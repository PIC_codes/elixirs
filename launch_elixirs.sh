#!/bin/sh
## SCRIPT DE LANCEMENT DE ELIXIRS
## --------------------------------------------------------
##
## Liste des parametres :
##    nb_pes : nombre de processeurs
##    nomcas : nom du cas
##    nomdon : nom du fichier de donnees
##    repris : reprise ou premier passage du cas
##

echo "lancement depuis $HOST"

nb_pes=$1
nomcas=$2
nomdon=$3
repris=${4-"n"}

USER=/home/drouinm/codes
PROJECT=$USER/elixirs_project

#launchargs: location of launch params logs
launchargs=$PROJECT/elixirs/launchargs
# exec_dir: location of elixirs executable
exec_dir=$PROJECT/elixirs/build_debug
# indata_dir: where test cases are defined
indata_dir=$PROJECT/cas_elixirs
# prod_dir: where output of the code is produced
prod_dir=$USER/elixirs_prod

if [ ! -d $launchargs ]
then
  mkdir -p $launchargs
fi

nomfic=kelix$$
echo "nb_pes="$nb_pes   >> $launchargs/$nomfic
echo "nomcas="$nomcas   >> $launchargs/$nomfic
echo "nomdon="$nomdon   >> $launchargs/$nomfic
echo "repris="$repris   >> $launchargs/$nomfic

if [ $nb_pes -ne 0 ]
then
## creation du repertoire de travail, menage, copie de lexecutable
  if [ -d $prod_dir/$nomcas ]
  then
    :
  else
    mkdir -p $prod_dir/$nomcas
  fi
  rm $prod_dir/$nomcas/debug.*
  rm $prod_dir/$nomcas/*.t
  rm $prod_dir/$nomcas/chmp_*
  rm $prod_dir/$nomcas/f??_?d_??
  cp $indata_dir/$nomdon  $prod_dir/$nomcas/data
  cp $exec_dir/elixirs  $prod_dir/$nomcas/
fi


if [ $nb_pes -ne 0 ]
then
  cd $prod_dir/$nomcas
  chmod 700 ./elixirs
  if [ $nb_pes -ne 1 ]
  then
   :
   ##  srun -n $nb_pes ./kexe"$nomver" > $nomcas.out
  else
    ## lancement en local
    ./elixirs 2> warning.out &
  fi

  echo "Cas $nomdon en cours d execution dans le repertoire $prod_dir/$nomcas "
fi
