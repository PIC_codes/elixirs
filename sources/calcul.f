

 !********************************************************************c
 !                                                                    c
 !                  ELIXIRS                                           c
 !                                                                    c
 !     code implicite electromagnetique bidimensionnel                c
 !     methode directe                                                c
 !     conditions aux limites semi periodiques                        c
 !     paroi conductrice en x=0 et x=xlong                            c
 !     ===> e<y=e<z=b<x=0 sur les parois                              c
 !                                                                    c
 !                                                                    c
 !  Born : 1987                                                       c
 !  sleeping : until 2007
 !
 !  Extended as relativistic ELIXIRS : 2009
 !
 !********************************************************************c


       PROGRAM Implicite_2d
! ======================================================================
!
!
!
!
!
! ======================================================================
       USE domaines
       USE domaines_klder
       USE champs
       USE champs_iter
       USE particules
       USE particules_klder
       USE temps
       USE collisions
       USE diagnostics
       USE erreurs

       IMPLICIT NONE

        REAL(kind=kr), DIMENSION(:,:), ALLOCATABLE    ::
     &     sauv, sauv2

       INTEGER    ::    i, j, ip, iesp, icol

       INTEGER    ::
     &       imax, jmax, ispot, istep, itime=0

       INTEGER    ::
     &        irho, jrho, ng, inc, ismax

       REAL(kind=kr)    ::   testc
       REAL(kind=kr)    ::  testq, tests, testp, amax

       CHARACTER(LEN=9)      :: f_debug
       CHARACTER(LEN=17)     :: f_out

       REAL(kind=kr)    ::    rjx_max, rjy_max, rjz_max,
     &     rjx_min, rjy_min, rjz_min

       REAL(kind=kr)    ::
     &         max_x, min_x, max_y, min_y,
     &         max_px, min_px, max_py, min_py

       REAL(kind=kr)    ::   maxdivE
       REAL(kind=kr)    ::   meanphi


! ***************************************************************

      f_debug = 'debug.002'
      OPEN(UNIT=fdbg, FILE=f_debug)

      f_out = 'impl2d_cald02.out'
      OPEN(UNIT=fmsgo, FILE=f_out )

! --- Lecture du fichier de donnees ou reprise, et premier traitement
      CALL init_donn
      write(fdbg,"('fin init_donn')")

! --- Traitements des parametres du cas
      CALL calc_donn
      write(fdbg,"('fin calc_donn')")

! --- Initialisation des variables du code implicite
      CALL init_implicite
      write(fdbg,"('fin init_implicite')")

! --- Dimensionnement des tableaux
      CALL decl_tabl
      write(fdbg,"('fin decl_tabl')")

!      IF(debug.GE.2)
      CALL verif_tabl
      write(fdbg,"('fin verif_tabl')")

! --- Initialisation du plasma ou reprise des tableaux
      CALL init_calc
      write(fdbg,"('fin init_calc')")

! --- Initialisation de grandeurs li�es aux particules
!
      CALL init_partic
      write(fdbg,"('fin init_partic')")

! --- Ouverture des fichiers de diagnostics
      CALL init_diag
      write(fdbg,"('fin init_diag')")

! --- d�termination des cstes les plus r�currentes dans les calculs
      CALL cstes

!      DO iesp=1,nesp_p
!        CALL tri_part(iesp)
!      ENDDO

! ***************************************************************


      ALLOCATE(sauv(0:n1x,0:n1y))
      ALLOCATE(sauv2(0:n1x,0:n1y))

! --- GOTO vers la fin du code
!      GOTO 991

       call zero

       anmax=0.5/float(ny)
       anpot=0.5/float(ny)

! ----- Sur la grille primale (griuni)
       IF(harmonicfield.EQ.1) THEN
         CALL harmonic
       ENDIF

       iter=0

! ---- On remet � z�ro les tenseur chi et zeta
       xxx(:,:) = 0. ; xxy(:,:) = 0. ; xxz(:,:) = 0. ;
       xyx(:,:) = 0. ; xyy(:,:) = 0. ; xyz(:,:) = 0. ;
       xzx(:,:) = 0. ; xzy(:,:) = 0. ; xzz(:,:) = 0. ;

       zxx(:,:) = 0. ; zxy(:,:) = 0. ; zxz(:,:) = 0. ;
       zyx(:,:) = 0. ; zyy(:,:) = 0. ; zyz(:,:) = 0. ;
       zzx(:,:) = 0. ; zzy(:,:) = 0. ; zzz(:,:) = 0. ;

 !
 !     prepush initial(les autres sont faits dans pushe et pushi)
 !     ---------------------------------------------------------
 !
 !     Boucle 1 sur les esp�ces de particules
 ! --------------------------------------------------------------
       DO iesp=1, nesp_p

       IF(freeze(iesp).GT.pasdt*iter) CYCLE

       CALL diag_tp_impl2d(iesp)

       IF(nbpa(iref).EQ.-1) THEN
         rho_esp_2(:,:,iesp) = 0.

         cjx_esp_2(:,:,iesp) = 0.
         cjy_esp_2(:,:,iesp) = 0.
         cjz_esp_2(:,:,iesp) = 0.

         dgx_esp_2(:,:,iesp) = 0.
         dgy_esp_2(:,:,iesp) = 0.
         dgz_esp_2(:,:,iesp) = 0.
       ELSE
!         IF(nbpa(iesp).EQ.80) THEN
! ------ projection de la charge de iesp
!           call projpa_rho(iesp)

         IF (dynam(iesp).EQ.2) THEN
! ------- cas relativiste
           IF(tta_def.EQ.1) THEN
             call prpush_rel_thetad(iesp)
           ELSEIF(friction_tta.EQ.0) THEN
             call prpush_rel_ttafix(iesp)
!           call prpush_rel_vay(iesp)
           ELSE
             call prpush_rel(iesp)
           ENDIF
         ELSE 
! ------- cas classique
           call prpush(iesp)
         ENDIF
! ------- normalisation des sources � cause de partic(a_po_ip,iesp)	 
         rho_esp_2(:,:,iesp) = rho_esp_2(:,:,iesp)*dxi*dyi*charge(iesp)

         cjx_esp_2(:,:,iesp) = cjx_esp_2(:,:,iesp)*dxi*dyi*charge(iesp)
         cjy_esp_2(:,:,iesp) = cjy_esp_2(:,:,iesp)*dxi*dyi*charge(iesp)
         cjz_esp_2(:,:,iesp) = cjz_esp_2(:,:,iesp)*dxi*dyi*charge(iesp)

         dgx_esp_2(:,:,iesp) = dgx_esp_2(:,:,iesp)*dxi*dyi*charge(iesp)
         dgy_esp_2(:,:,iesp) = dgy_esp_2(:,:,iesp)*dxi*dyi*charge(iesp)
         dgz_esp_2(:,:,iesp) = dgz_esp_2(:,:,iesp)*dxi*dyi*charge(iesp)
       ENDIF

       IF(debug.GE.3) THEN
         write(fmsgo,*) 'apres prepush, espece = ', iesp
         write(fmsgo,*) 'nbpa(iesp) = ', nbpa(iesp)
         max_x = maxval(partic(a_qx,1:nbpa(iesp),iesp))
         min_x = minval(partic(a_qx,1:nbpa(iesp),iesp))
         write(fmsgo,*) 'min_x,max_x =',min_x,max_x

         max_y = maxval(partic(a_qy,1:nbpa(iesp),iesp))
         min_y = minval(partic(a_qy,1:nbpa(iesp),iesp))
         write(fmsgo,*) 'min_y,max_y =',min_y,max_y

         max_px = maxval(partic(a_px,1:nbpa(iesp),iesp))
         min_px = minval(partic(a_px,1:nbpa(iesp),iesp))
         write(fmsgo,*) 'min_px,max_px =',min_px,max_px

         max_py = maxval(partic(a_py,1:nbpa(iesp),iesp))
         min_py = minval(partic(a_py,1:nbpa(iesp),iesp))
         write(fmsgo,*) 'min_py,max_py =',min_py,max_py
       ENDIF

       testc=0.
       DO j=0,ny
         DO i=0,n1x
          testc=testc+rho_esp_2(i,j,iesp)
         ENDDO
       ENDDO
       write(fmsgo,*) 'Init avant periodiq rho_esp_2(iesp) = ',testc
 !
 !     Fin boucle 1 sur les esp�ces de particules
 ! --------------------------------------------------------------
       ENDDO


!
!      Agglom�ration des tenseurs dans le cas o� une des
!      esp�ces est sub_cycl�e
!
! -----------------------------------------------------
       IF(max_cycles.GT.1) THEN
         DO  j=0,n1y
           DO  i=0,n1x
             DO iesp=1,nesp_p
               xxx(i,j)=xxx(i,j)+ xxx_esp(i,j,iesp)
               xxy(i,j)=xxy(i,j)+ xxy_esp(i,j,iesp)
               xxz(i,j)=xxz(i,j)+ xxz_esp(i,j,iesp)

               xyx(i,j)=xyx(i,j)+ xyx_esp(i,j,iesp)
               xyy(i,j)=xyy(i,j)+ xyy_esp(i,j,iesp)
               xyz(i,j)=xyz(i,j)+ xyz_esp(i,j,iesp)

               xzx(i,j)=xzx(i,j)+ xzx_esp(i,j,iesp)
               xzy(i,j)=xzy(i,j)+ xzy_esp(i,j,iesp)
               xzz(i,j)=xzz(i,j)+ xzz_esp(i,j,iesp)

               zxx(i,j)=zxx(i,j)+ zxx_esp(i,j,iesp)
               zxy(i,j)=zxy(i,j)+ zxy_esp(i,j,iesp)
               zxz(i,j)=zxz(i,j)+ zxz_esp(i,j,iesp)

               zyx(i,j)=zyx(i,j)+ zyx_esp(i,j,iesp)
               zyy(i,j)=zyy(i,j)+ zyy_esp(i,j,iesp)
               zyz(i,j)=zyz(i,j)+ zyz_esp(i,j,iesp)

               zzx(i,j)=zzx(i,j)+ zzx_esp(i,j,iesp)
               zzy(i,j)=zzy(i,j)+ zzy_esp(i,j,iesp)
               zzz(i,j)=zzz(i,j)+ zzz_esp(i,j,iesp)
             ENDDO
           ENDDO
         ENDDO
       ENDIF

 !
 !     normalisation des rho et de la charge totale
 !     --------------------------------------------
 !
 !     application de la periodicite
 !
       CALL edge_sources
       IF(ALL(dynam(1:nesp_p).EQ.2)) THEN
         CALL edge_kizetarel
         CALL period_kizetarel
       ENDIF

 !
 !     Boucle 2 sur les esp�ces de particules
 ! --------------------------------------------------------------
       DO iesp=1, nesp_p
         IF(MOD(iter,nbscycles(iesp)).NE.0)  CYCLE

         CALL period_rho(iesp)
         CALL period_cj(iesp)
         CALL period_dg(iesp)

         testc=0.
         DO j=0,ny
           DO i=0,n1x
            testc=testc+rho_esp_2(i,j,iesp)
           ENDDO
         ENDDO
         write(fmsgo,*) 'Init apres periodiq rho_esp_2(iesp) = ',testc
 !
 !     Fin boucle 2 sur les esp�ces de particules
 ! --------------------------------------------------------------
       ENDDO

        jxtot =0. ; jytot =0. ; jztot =0.

        rhomax=0.
        s(:,:)=0.
        DO  j=0,n1y
          DO  i=0,n1x
            DO iesp=1,nesp_p
             jxtot(i,j)=jxtot(i,j) +
     &         cjx_esp_2(i,j,iesp)+ dgx_esp_2(i,j,iesp)
             jytot(i,j)=jytot(i,j) +
     &         cjy_esp_2(i,j,iesp)+ dgy_esp_2(i,j,iesp)
             jztot(i,j)=jztot(i,j) +
     &         cjz_esp_2(i,j,iesp)+ dgz_esp_2(i,j,iesp)
 !
             s(i,j)= s(i,j) + rho_esp_2(i,j,iesp)
           ENDDO
           if(rhomax.lt.abs(s(i,j))) then
            irho=i
            jrho=j
            rhomax=abs(s(i,j))
           endif
	  ENDDO
        ENDDO

        jxtot(:,:) = 0.5*jxtot(:,:)
        jytot(:,:) = 0.5*jytot(:,:)
        jztot(:,:) = 0.5*jztot(:,:)

!       write(fmsgo,*) '***irho = ',irho,'   rhomax = ',rhomax
       testc=0.
       DO j=1,ny
         DO i=0,nx
          testc=testc+s(i,j)
         ENDDO
       ENDDO
       write(fmsgo,*) '*apres les prpushs testc = ',testc,'***'

       rjx_max=maxval(jxtot)
       rjy_max=maxval(jytot)
       rjz_max=maxval(jztot)

       rjx_min=minval(jxtot)
       rjy_min=minval(jytot)
       rjz_min=minval(jztot)

 !
 !     trace de la charge totale
 !     -------------------------
       write(fmsgo,*) 'rjx_max,rjy_max,rjz_max = ',
     &    rjx_max,rjy_max,rjz_max
       write(fmsgo,*) 'rjx_min,rjy_min,rjz_min = ',
     &    rjx_min,rjy_min,rjz_min

       ng=n2x*ny
       inc=n2x

 !
 !     calcul de la suceptibilite du plasma
 !     ------------------------------------
       IF(ALL(dynam(1:nesp_p).EQ.1)) THEN
! ----- toutes especes particules classiques
         call kizeta
       ELSEIF(ALL(dynam(1:nesp_p).EQ.2)) THEN
! ----- toutes esp�ces particules relativistes
         CALL kizeta_rel_means
	 IF(debug.GE.4) THEN
	   CALL kizeta_mean_components
	 ENDIF
       ELSE
!       dynamique mixte non prise en charge pour le moment :
!         arret de l execution
! ---------------------------------------------------------------
         WRITE(fmsgo,"('Les deux especes de particules n ont ',
     &           'pas la meme dynamique : arret de l execution')")
         STOP
       ENDIF
       write(fmsgo,*) 'apres appel kizeta'
 !

       IF(debug.GE.4) THEN
         DO i=1,n1x
           WRITE(IDchi ,wforma2)
     &       xxx0(i), xxy0(i), xxz0(i),
     &       xyx0(i), xyy0(i), xyz0(i),
     &       xzx0(i), xzy0(i), xzz0(i)

           WRITE(IDzeta,wforma2)
     &       zxx0(i), zxy0(i), zxz0(i),
     &       zyx0(i), zyy0(i), zyz0(i),
     &       zzx0(i), zzy0(i), zzz0(i)

           IF(ALL(dynam(1:nesp_p).EQ.2)) THEN
!           WRITE(IDchirel ,wforma2)
!     &       xxx0(i)-xxx0r1(i)-xxx0r2(i),
!     &       xxy0(i)-xxy0r1(i)-xxy0r2(i),
!     &       xxz0(i)-xxz0r1(i)-xxz0r2(i),
!     &       xyx0(i)-xyx0r1(i)-xyx0r2(i),
!     &       xyy0(i)-xyy0r1(i)-xyy0r2(i),
!     &       xyz0(i)-xyz0r1(i)-xyz0r2(i),
!     &       xzx0(i)-xzx0r1(i)-xzx0r2(i),
!     &       xzy0(i)-xzy0r1(i)-xzy0r2(i),
!     &       xzz0(i)-xzz0r1(i)-xzz0r2(i)

           WRITE(IDchirel ,wforma2)
     &       xxx0r1(i)+xxx0r2(i),
     &       xxy0r1(i)+xxy0r2(i),
     &       xxz0r1(i)+xxz0r2(i),
     &       xyx0r1(i)+xyx0r2(i),
     &       xyy0r1(i)+xyy0r2(i),
     &       xyz0r1(i)+xyz0r2(i),
     &       xzx0r1(i)+xzx0r2(i),
     &       xzy0r1(i)+xzy0r2(i),
     &       xzz0r1(i)+xzz0r2(i)

           WRITE(IDzetarel,wforma2)
     &       zxx0r1(i),
     &       zxy0r1(i),
     &       zxz0r1(i),
     &       zyx0r1(i),
     &       zyy0r1(i),
     &       zyz0r1(i),
     &       zzx0r1(i),
     &       zzy0r1(i),
     &       zzz0r1(i)
           ENDIF
         ENDDO
       ENDIF


       IF(col_test.EQ.0) THEN

       call grimax
       write(fmsgo,*) 'apres appel grimax'

       ismax=0
       IF(ismax.eq.0) THEN
 !
 !     resolution de l equation de maxwell
 !     ----------------------------------
       call calq
       write(fmsgo,*) 'apres appel calq'
       call maxwel
       write(fmsgo,*) 'apres appel maxwel'

       ELSE
         ex(:,:)=0.
         ey(:,:)=0.
         ez(:,:)=0.
       ENDIF

       ispot=0
       IF(ispot.eq.0) THEN
         IF(iter_es.EQ.1) THEN
           CALL Init_iter
           CALL pot2d_iter

         ELSE
           IF(ismax.EQ.0) THEN
 !
 !     calcul de la correction electrostatique
 !     ---------------------------------------
             testq=0.
             tests=0.
             DO j=1,ny
               DO i=1,nxm1
                 sauv(i,j)=s(i,j)

                 sauv2(i,j)=  -dxi*((1.+xxx(i+1,j))*ex(i+1,j)
     &                    -(1.+xxx(i,j))*ex(i,j))
     &           -dxf4i*( xxy(i+1,j) *(ey(i+1,j+1)+ey(i+1,j))
     &               -xxy(i-1,j) *(ey(i-1,j+1)+ey(i-1,j)) )
     &           -dxf2i*(xxz(i+1,j)*ez(i+1,j)-xxz(i-1,j)*ez(i-1,j))
     &           -dyf4i*( xyx(i,j+1) *(ex(i+1,j+1)+ex(i,j+1))
     &               -xyx(i,j-1) *(ex(i+1,j-1)+ex(i,j-1)) )
     &           -dyf2i*(xyz(i,j+1)*ez(i,j+1)-xyz(i,j-1)*ez(i,j-1))
     &       -dyi *((1.+xyy(i,j+1))*ey(i,j+1)-(1.+xyy(i,j))*ey(i,j))

                 s(i,j)=s(i,j)+sauv2(i,j)
               ENDDO
             ENDDO
           ENDIF

           testp=0.
           amax=0.
           DO i=1,nxm1
             DO j=1,ny
               testp=testp+s(i,j)
               if(amax.lt.abs(s(i,j))) then
                 amax=abs(s(i,j))
                 imax=i
                 jmax=j
               endif
             ENDDO
           ENDDO
           write(fmsgo,*) 'erreur a poisson apres le 1er maxwel testp=',
     &        testp
!       write(fmsgo,*) 'imax,jmax =',imax,jmax,'amax=',amax

           call pot2di

           DO j=1,ny
             DO i=1,nx
              ex(i,j)=ex(i,j)-dxi*(phi(i,j)-phi(i-1,j))
              ey(i,j)=ey(i,j)-dyi*(phi(i,j)-phi(i,j-1))
             ENDDO
           ENDDO

! -------- fin IF(ispot.EQ.0)	   
         ENDIF

         IF(debug.GE.7) THEN
           DO i=1,n1x
             meanphi= 0.
             DO j=1,n1y
               meanphi = meanphi + phi(i,j)
             ENDDO
             meanphi = meanphi/real(n1y)
             WRITE(IDchmpex_x,wforma) meanphi
           ENDDO
         ENDIF

 !
 !     conditions de periodicite des elements modifies
         ex(0:nx,0)  = ex(0:nx,ny)
         ex(0:nx,n1y)= ex(0:nx,1)
         ey(0:nx,0)  = ey(0:nx,ny)
         ey(0:nx,n1y)= ey(0:nx,1)
 ! ------ fin col_test
       ENDIF

 !
 !     extrapolation de ex by et bz sur les parois x=0. et x=xlong
 !     on stocke provisoirement l element nx en n1x (cf griuni)
 !
       IF(ibndc(1).EQ.-3) THEN
         ex(0  ,0:n1y) = 1.5*ex(1, 0:n1y)-0.5*ex(2   ,0:n1y)
         by(0  ,0:n1y) = 1.5*by(1, 0:n1y)-0.5*by(2   ,0:n1y)
         bz(0  ,0:n1y) = 1.5*bz(1, 0:n1y)-0.5*bz(2   ,0:n1y)
       ELSEIF(ibndc(1).EQ.-2 .OR. ibndc(1).EQ.0) THEN
         ex(0  ,0:n1y) = ex(1, 0:n1y)
         by(0  ,0:n1y) = by(1, 0:n1y)
         bz(0  ,0:n1y) = bz(1, 0:n1y)
       ENDIF

       IF(ibndc(2).EQ.-3) THEN
         ex(n1x,0:n1y) = 1.5*ex(nx,0:n1y)-0.5*ex(nxm1,0:n1y)
         by(n1x,0:n1y) = 1.5*by(nx,0:n1y)-0.5*by(nxm1,0:n1y)
         bz(n1x,0:n1y) = 1.5*bz(nx,0:n1y)-0.5*bz(nxm1,0:n1y)
       ELSEIF(ibndc(2).EQ.-2 .OR. ibndc(2).EQ.0) THEN
         ex(n1x,0:n1y) = ex(nx,0:n1y)
         by(n1x,0:n1y) = by(nx,0:n1y)
         bz(n1x,0:n1y) = bz(nx,0:n1y)
       ENDIF

       ENDIF   !  IF(col_test.EQ.0)


 !
 !     mise sur la grille des particules
 !
       write(fmsgo,*) 'avant appel griuni'
       call griuni
       write(fmsgo,*) 'apres appel griuni'

! --- + Diagnostics spatiaux a chaque pas de temps : diag_sp
        CALL diag_sp
! --- + Diagnostics espace des phases a frequence variable : diag_ph
        CALL diag_ph

 !
 ! *********************************************************************
 !     fin de l initialisation
 ! *********************************************************************
 !
 !     debut de la boucle principale sur les pas de temps
 !     --------------------------------------------------

       DO istep=1,itfin

! ---- on met � jour iter pour les diagnostics
       iter=istep

       write(fmsgo,*) 'itime=',itime,'istep=',istep

!
!     **********************

       IF(harmonicfield.EQ.1) THEN
         CALL harmonic
       ENDIF

 !
 !     on avance les ions
 !     ------------------

 ! --- On remet � z�ro les tenseur chi et zeta
       xxx(:,:) = 0. ; xxy(:,:) = 0. ; xxz(:,:) = 0. ;
       xyx(:,:) = 0. ; xyy(:,:) = 0. ; xyz(:,:) = 0. ;
       xzx(:,:) = 0. ; xzy(:,:) = 0. ; xzz(:,:) = 0. ;

       zxx(:,:) = 0. ; zxy(:,:) = 0. ; zxz(:,:) = 0. ;
       zyx(:,:) = 0. ; zyy(:,:) = 0. ; zyz(:,:) = 0. ;
       zzx(:,:) = 0. ; zzy(:,:) = 0. ; zzz(:,:) = 0. ;

 !
 !     Boucle 1 sur les esp�ces de particules
 ! --------------------------------------------------------------
       DO iesp=1, nesp_p

         IF(freeze(iesp).GT.pasdt*iter) CYCLE

         IF(MOD(iter,nbscycles(iesp)).NE.0)  CYCLE

         write(fmsgo,*) 'On pousse l espece : ', iesp


         CALL diag_tp_impl2d(iesp)

! ----- Test dip�le oscillant
       IF(nbpa(iref).EQ.-1) THEN
         rho_esp_2(:,:,iesp) = 0.

         cjx_esp_2(:,:,iesp) = 0.
         cjy_esp_2(:,:,iesp) = 0.
         cjz_esp_2(:,:,iesp) = 0.

         dgx_esp_2(:,:,iesp) = 0.
         dgy_esp_2(:,:,iesp) = 0.
         dgz_esp_2(:,:,iesp) = 0.
       ELSE
!         IF(nbpa(iesp).EQ.80) THEN
! ------ projection de la charge de iesp
!           call projpa_rho(iesp)

         IF(dynam(iesp).EQ.2) THEN
! ------ cas relativiste
           IF(tta_def.EQ.1) THEN
             call push_rel_thetad(iesp)
           ELSEIF(friction_tta.EQ.0) THEN
	     CALL push_rel_ttafix(iesp)
!	   CALL push_rel_vay(iesp)
           ELSE
	     CALL push_rel(iesp)
	   ENDIF
         ELSE
! ------ cas classique
	   call push(iesp)
	 ENDIF

! ------- normalisation des sources � cause de partic(a_po_ip,iesp)
         rho_esp_2(:,:,iesp) = rho_esp_2(:,:,iesp)*dxi*dyi*charge(iesp)

         cjx_esp_2(:,:,iesp) = cjx_esp_2(:,:,iesp)*dxi*dyi*charge(iesp)
         cjy_esp_2(:,:,iesp) = cjy_esp_2(:,:,iesp)*dxi*dyi*charge(iesp)
         cjz_esp_2(:,:,iesp) = cjz_esp_2(:,:,iesp)*dxi*dyi*charge(iesp)

         dgx_esp_2(:,:,iesp) = dgx_esp_2(:,:,iesp)*dxi*dyi*charge(iesp)
         dgy_esp_2(:,:,iesp) = dgy_esp_2(:,:,iesp)*dxi*dyi*charge(iesp)
         dgz_esp_2(:,:,iesp) = dgz_esp_2(:,:,iesp)*dxi*dyi*charge(iesp)

       testc=0.
       DO j=0,ny
         DO i=0,n1x
          testc=testc+rho_esp_2(i,j,iesp)
         ENDDO
       ENDDO
       write(fmsgo,*) 'rho_esp_2(iesp) = ',testc
       ENDIF

       IF(debug.GE.3) THEN
         write(fmsgo,*) 'apres push, espece = ', iesp
         write(fmsgo,*) 'nbpa(iesp) = ', nbpa(iesp)
         max_x = maxval(partic(a_qx,1:nbpa(iesp),iesp))
         min_x = minval(partic(a_qx,1:nbpa(iesp),iesp))
         write(fmsgo,*) 'min_x,max_x =',min_x,max_x

         max_y = maxval(partic(a_qy,1:nbpa(iesp),iesp))
         min_y = minval(partic(a_qy,1:nbpa(iesp),iesp))
         write(fmsgo,*) 'min_y,max_y =',min_y,max_y

         max_px = maxval(partic(a_px,1:nbpa(iesp),iesp))
         min_px = minval(partic(a_px,1:nbpa(iesp),iesp))
         write(fmsgo,*) 'min_px,max_px =',min_px,max_px

         max_py = maxval(partic(a_py,1:nbpa(iesp),iesp))
         min_py = minval(partic(a_py,1:nbpa(iesp),iesp))
         write(fmsgo,*) 'min_py,max_py =',min_py,max_py
       ENDIF

 !
 !     Fin boucle 1 sur les esp�ces de particules
 ! --------------------------------------------------------------
       ENDDO

!
!      Agglom�ration des tenseurs dans le cas o� une des
!      esp�ces est sub_cycl�e
!
! -----------------------------------------------------
       IF(max_cycles.GT.1) THEN
         DO  j=0,n1y
           DO  i=0,n1x
             DO iesp=1,nesp_p
               xxx(i,j)=xxx(i,j)+ xxx_esp(i,j,iesp)
               xxy(i,j)=xxy(i,j)+ xxy_esp(i,j,iesp)
               xxz(i,j)=xxz(i,j)+ xxz_esp(i,j,iesp)

               xyx(i,j)=xyx(i,j)+ xyx_esp(i,j,iesp)
               xyy(i,j)=xyy(i,j)+ xyy_esp(i,j,iesp)
               xyz(i,j)=xyz(i,j)+ xyz_esp(i,j,iesp)

               xzx(i,j)=xzx(i,j)+ xzx_esp(i,j,iesp)
               xzy(i,j)=xzy(i,j)+ xzy_esp(i,j,iesp)
               xzz(i,j)=xzz(i,j)+ xzz_esp(i,j,iesp)

               zxx(i,j)=zxx(i,j)+ zxx_esp(i,j,iesp)
               zxy(i,j)=zxy(i,j)+ zxy_esp(i,j,iesp)
               zxz(i,j)=zxz(i,j)+ zxz_esp(i,j,iesp)

               zyx(i,j)=zyx(i,j)+ zyx_esp(i,j,iesp)
               zyy(i,j)=zyy(i,j)+ zyy_esp(i,j,iesp)
               zyz(i,j)=zyz(i,j)+ zyz_esp(i,j,iesp)

               zzx(i,j)=zzx(i,j)+ zzx_esp(i,j,iesp)
               zzy(i,j)=zzy(i,j)+ zzy_esp(i,j,iesp)
               zzz(i,j)=zzz(i,j)+ zzz_esp(i,j,iesp)
             ENDDO
           ENDDO
         ENDDO
       ENDIF

! ----- application de la periodicite
	CALL edge_sources
        IF(ALL(dynam(1:nesp_p).EQ.2)) THEN
	  CALL edge_kizetarel
          CALL period_kizetarel
        ENDIF

        DO iesp=1, nesp_p
 !
 !     Boucle 2 sur les esp�ces de particules
 ! --------------------------------------------------------------
          IF(MOD(iter,nbscycles(iesp)).NE.0) CYCLE

          CALL period_rho(iesp)
          CALL period_cj(iesp)
          CALL period_dg(iesp)

          testc=0.
          DO j=0,ny
            DO i=0,n1x
             testc=testc+rho_esp_2(i,j,iesp)
            ENDDO
          ENDDO
          write(fmsgo,*) 'Periodique rho_esp_2( ',iesp,')=',testc
 !
 !     Fin boucle 2 sur les esp�ces de particules
 ! --------------------------------------------------------------
        ENDDO


        jxtot =0. ; jytot =0. ; jztot =0.

        rhomax=0.
        s(:,:)=0.
        DO  j=0,n1y
          DO  i=0,n1x
            DO iesp=1,nesp_p
             jxtot(i,j)=jxtot(i,j) +
     &         cjx_esp_2(i,j,iesp)+ dgx_esp_2(i,j,iesp)
             jytot(i,j)=jytot(i,j) +
     &         cjy_esp_2(i,j,iesp)+ dgy_esp_2(i,j,iesp)
             jztot(i,j)=jztot(i,j) +
     &         cjz_esp_2(i,j,iesp)+ dgz_esp_2(i,j,iesp)
 !
             s(i,j)= s(i,j) + rho_esp_2(i,j,iesp)
            ENDDO
	  ENDDO
        ENDDO

        jxtot(:,:) = 0.5*jxtot(:,:)
        jytot(:,:) = 0.5*jytot(:,:)
        jztot(:,:) = 0.5*jztot(:,:)

!    Mise � jour de diagnostics densites de charge et courant
       CALL update_moments

 !
       testc=0.
       DO j=1,ny
         DO i=0,nx
          testc=testc+s(i,j)
         ENDDO
       ENDDO
       write(fmsgo,*) 'testc = ',testc
 !
 !     calcul de la suceptibilite du plasma
 !     ------------------------------------
       IF(ALL(dynam(1:nesp_p).EQ.1)) THEN
! ----- toutes especes particules classiques
         call kizeta
       ELSEIF(ALL(dynam(1:nesp_p).EQ.2)) THEN
! ----- toutes esp�ces particules relativistes
         CALL kizeta_rel_means
         IF(debug.GE.4) THEN
           CALL kizeta_mean_components 
         ENDIF
       ELSE
!       dynamique mixte non prise en charge pour le moment :
!         arret de l execution
! ---------------------------------------------------------------
         WRITE(fmsgo,"('Les deux especes de particules n ont ',
     &           'pas la meme dynamique : arret de l execution')")
         STOP
       ENDIF

 !
       IF(col_test.EQ.0) THEN

       call grimax

       if(ismax.eq.0) then
 !
 !     resolution des equations de maxwell
 !     -----------------------------------
         call calq
         call maxwel

         else
           ex(:,:)=0.
           ey(:,:)=0.
           ez(:,:)=0.
         endif
         testp=0.

         DO i=1,nxm1
           DO j=1,ny
             testp=testp+s(i,j)
           ENDDO
         ENDDO

       write(fmsgo,*) 'testp=',testp

       if(ispot.eq.0) then
         IF(iter_es.EQ.1) THEN

           CALL Init_iter
           CALL pot2d_iter


         ELSE
           IF(ismax.EQ.0) THEN
 !
 !     correction du champ electrostatique par poisson
 !     -----------------------------------------------

             DO j=1,ny
               DO i=1,nxm1
               s(i,j)=s(i,j)-dxi*((1.+xxx(i+1,j))*ex(i+1,j)
     &                    -(1.+xxx(i,j))*ex(i,j))
     &         -dxf4i*( xxy(i+1,j) *(ey(i+1,j+1)+ey(i+1,j))
     &               -xxy(i-1,j) *(ey(i-1,j+1)+ey(i-1,j)) )
     &         -dxf2i*(xxz(i+1,j)*ez(i+1,j)-xxz(i-1,j)*ez(i-1,j))
     &         -dyf4i*( xyx(i,j+1) *(ex(i+1,j+1)+ex(i,j+1))
     &               -xyx(i,j-1) *(ex(i+1,j-1)+ex(i,j-1)) )
     &         -dyf2i*(xyz(i,j+1)*ez(i,j+1)-xyz(i,j-1)*ez(i,j-1))
     &         -dyi *((1.+xyy(i,j+1))*ey(i,j+1)-(1.+xyy(i,j))*ey(i,j))
               ENDDO
             ENDDO
           ENDIF

           call pot2di

           DO j=1,ny
             DO i=1,nx
               ex(i,j)=ex(i,j)-dxi*(phi(i,j)-phi(i-1,j))
               ey(i,j)=ey(i,j)-dyi*(phi(i,j)-phi(i,j-1))
             ENDDO
           ENDDO

! -------- fin else iter_es
         ENDIF

 !
         IF(debug.GE.7) THEN
           DO i=1,n1x
             meanphi= 0.
             DO j=1,n1y
               meanphi = meanphi + phi(i,j)
             ENDDO
             meanphi = meanphi/real(n1y)
             WRITE(IDchmpex_x,wforma) meanphi
           ENDDO
         ENDIF

!
!     conditions de periodicite
!
         ex(0:nx,0)  =ex(0:nx,ny)
         ex(0:nx,n1y)=ex(0:nx,1)
         ey(0:nx,0)  =ey(0:nx,ny)
         ey(0:nx,n1y)=ey(0:nx,1)

         maxdivE = MAXVAL( dxi*(ex(1:nx,1:ny)-ex(0:nx-1,1:ny))
     &     + dyi*(ey(1:nx,1:ny)-ey(1:nx,0:ny-1)) )

         write(fmsgo,*) 'divergence de E(n+1) : ', maxdivE

       endif
 !
 !     extrapolation de ex by et bz sur les parois x=0. et x=xlong
 !     on stocke provisoirement l element nx en n1x (cf griuni)
 !
       IF(ibndc(1).EQ.-3) THEN
         ex(0  ,0:n1y) = 1.5*ex(1, 0:n1y)-0.5*ex(2   ,0:n1y)
         by(0  ,0:n1y) = 1.5*by(1, 0:n1y)-0.5*by(2   ,0:n1y)
         bz(0  ,0:n1y) = 1.5*bz(1, 0:n1y)-0.5*bz(2   ,0:n1y)
       ELSEIF(ibndc(1).EQ.-2 .OR. ibndc(1).EQ.0) THEN
         ex(0  ,0:n1y) = ex(1, 0:n1y)
         by(0  ,0:n1y) = by(1, 0:n1y)
         bz(0  ,0:n1y) = bz(1, 0:n1y)
       ENDIF

       IF(ibndc(2).EQ.-3) THEN
         ex(n1x,0:n1y) = 1.5*ex(nx,0:n1y)-0.5*ex(nxm1,0:n1y)
         by(n1x,0:n1y) = 1.5*by(nx,0:n1y)-0.5*by(nxm1,0:n1y)
         bz(n1x,0:n1y) = 1.5*bz(nx,0:n1y)-0.5*bz(nxm1,0:n1y)
       ELSEIF(ibndc(2).EQ.-2 .OR. ibndc(2).EQ.0) THEN
         ex(n1x,0:n1y) = ex(nx,0:n1y)
         by(n1x,0:n1y) = by(nx,0:n1y)
         bz(n1x,0:n1y) = bz(nx,0:n1y)
       ENDIF

       ENDIF  ! IF(col_test.EQ.0)

! ------------------------------------------
! --- Boucle 4 sur les especes de particules
!     - Collisions
! ------------------------------------------

      IF((col_nb.GE.1).AND.(ALL(freeze.LE.pasdt*iter))) THEN

  ! ----- + La longueur de Debye n'est calculee que si necessaire
        IF(ANY(MOD(iter,col_per(:)).EQ.0)) THEN
          IF(ANY(col_log.EQ.0.)) CALL calcul_debye
        ENDIF  
        DO icol=1,col_nb
          IF(MOD(iter,col_per(icol)).GT.0) CYCLE 
          IF(col_p1(icol).EQ.col_p2(icol)) THEN
  ! ----- + Collisions entre particules de meme espece
            IF (col_regime.EQ.0) THEN
              CALL intra_collisions(icol)
            ELSE
              CALL intra_collisions_relat(icol)
            ENDIF
          ELSE
  ! ----- + Collisions entre particules d'especes differentes
            IF (col_regime.EQ.0) THEN
             CALL inter_collisions(icol)
            ELSE
              CALL inter_collisions_relat(icol)
            ENDIF
          ENDIF
  ! ----- Fin de la Boucle 4 sur les especes de particules
        ENDDO
  
      ENDIF       
      
!
!     interpolations sur la grille des particules
!
       call griuni

!    Mise � jour de diagnostics champs E et B
       CALL update_fields

!
! --- Diagnostics
! --- + Diagnostics temporels a chaque pas de temps : diag_tp_impl2d
! --- diags de impl2d
!        CALL diag_tp_impl2d


! --- + Diagnostics spatiaux a chaque pas de temps : diag_sp
        CALL diag_sp
! --- + Diagnostics temporels a chaque pas de temps : diag_tp
        CALL diag_tp(pasdt)
! --- + Diagnostics espace des phases a frequence variable : diag_ph
        CALL diag_ph

 
 !     fin de la boucle principale
 !     ---------------------------
       ENDDO

991    continue

 
      DEALLOCATE(sauv)
      DEALLOCATE(sauv2) 
 
 
       STOP
9000   format(1h1)
9002   format(1x,i5,6(1x,e9.4),4(1x,e10.3))
9010   format(1x,'itime',4x,'wcinx(1)', 4x,'wcinx(2)', 4x,'wciny(1)', 4x
     &  ,'wciny(2)',4x,'wcinz(1)',4x,'wcinz(2)',7x,'esq',7x,'bsq',7x,
     &  'enert',5x,'testc')
9021   format(1x,///)

!      ________________________
       END PROGRAM Implicite_2d
