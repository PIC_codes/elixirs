! ======================================================================
! collisions_relat.f
! ------------------
!
!  SUBROUTINE coll_relat_init
!  SUBROUTINE intra_collisions_relat
!  SUBROUTINE inter_collisions_relat
!  SUBROUTINE collision_relat
!  SUBROUTINE inverse_nanbu
!
! ======================================================================

      SUBROUTINE coll_relat_init(col_i)
! ======================================================================
! En fonction des parametres fournis, calcul du lorarithme Coulombien.
! Identique au calcul classique pour l'instant.
! Calcul de col_s0 et col_s1
! ======================================================================
      USE domaines
      USE particules_klder
      USE collisions
      USE laser
      USE temps
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)       :: col_i

!      REAL(kind=kr), PARAMETER   :: PI   = 3.14159265359
      REAL(kind=kr), PARAMETER   :: Qe   = 1.6022E-19
      REAL(kind=kr), PARAMETER   :: Me   = 9.1094E-31
      REAL(kind=kr), PARAMETER   :: eps0 = 8.8542E-12

      REAL(kind=kr)              :: temp1, temp2, tempe, tempi
      REAL(kind=kr)              :: kDe, kD2, kD, kmax
      REAL(kind=kr)              :: m12, V2
      INTEGER                   :: i, i1, i2, ielec, ndl

      i1 = col_p1(col_i) ; i2 = col_p2(col_i)
      per_tri(i1)=1      ; per_tri(i2)=1

! --- Temperatures des electrons (en keV)
! --- Temperatures des deux especes collisionnantes (en keV)
      DO i=1,nesp
         IF ((masse(i).EQ.1.).AND.(charge(i).EQ.-1.)) ielec=i
      ENDDO
      tempe = 0._8 ; temp1 = 0._8 ; temp2 = 0._8 ; ndl = 1
      IF((dim_cas.GE.1).AND.(dim_cas.LE.6)) THEN
        tempe=tempe+vthx(ielec)
        temp1=temp1+vthx(i1)
        temp2=temp2+vthx(i2)
        ndl  =1
      ENDIF
      IF((dim_cas.GE.2).AND.(dim_cas.LE.6)) THEN
        tempe=tempe+vthy(ielec)
        temp1=temp1+vthy(i1)
        temp2=temp2+vthy(i2)
        ndl  =2
      ENDIF
      IF((dim_cas.EQ.3).OR.(dim_cas.EQ.5).OR.(dim_cas.EQ.6)) THEN
        tempe=tempe+vthz(ielec)
        temp1=temp1+vthz(i1)
        temp2=temp2+vthz(i2)
        ndl  =3
      ENDIF
      tempe=tempe/ndl ; temp1=temp1/ndl ; temp2=temp2/ndl

! --- Inverse de la longueur de Debye electronique
      kDe = SQRT( densite(ielec)*Me*9.E16_8*4._8*PI**2_8
     &           /(tempe*1.E3_8*Qe) )   / (lambda(1)*1.E-6_8)

! --- kD par la formule de Decoster ("Modeling of Collisions")
      kD2 = 0.
      do i=1,nesp_p
        SELECT CASE (dim_cas)
          CASE(1    ) ; tempi =  vthx(i)
          CASE(2,4  ) ; tempi = (vthx(i)+vthy(i)        )/2.
          CASE(3,5,6) ; tempi = (vthx(i)+vthy(i)+vthz(i))/3.
        END SELECT
        kD2 = kD2
     &       +   densite(i)*ABS(charge(i))*(2.*PI)**2
     &        / (tempi + masse(i)/(masse(i1)/temp1+masse(i2)/temp2))
     &        * 511. / (lambda(1)*1.E-6_8)**2
      enddo
      kD = SQRT(kD2)

      m12 = masse(i1)*masse(i2)/(masse(i1)+masse(i2))
      V2  = 2.*( temp1/masse(i1) + temp2/masse(i2) )*(1.E3_8*Qe/Me)
      kmax = MIN(  4._8*PI*eps0*m12*Me*V2
     &                 /(ABS(charge(i1)*charge(i2))*Qe**2_8) ,
     &             2._8*m12*Me*SQRT(V2)/1.06E-34_8 )

! Rappel : formule donnant kmax dans le cas ou les deux particules sont
! du meme type.
!     kmax = MIN( 8.*PI*eps0*temp1*1e3 * Qe/(charge(i1)*Qe)**2 ,
!    &            2.*SQRT(Me*masse(i1)*temp1*1e3 * Qe)/1.06E-34 )

      WRITE(fdbg,"('---------------------------------------------')")
      WRITE(fdbg,"('Temperature electrons : ',E9.3,' keV')") tempe
      WRITE(fdbg,"('Temperature espece 1  : ',E9.3,' keV')") temp1
      WRITE(fdbg,"('Temperature espece 2  : ',E9.3,' keV')") temp2
      WRITE(fdbg,"('Long. Debye elect. : ',E9.3,' micron')") 1.E6_8/kDe
      WRITE(fdbg,"('1./kmax            : ',E9.3,' micron')") 1.E6_8/kmax
      WRITE(fdbg,"('1./kD (Decoster)   : ',E9.3,' micron')") 1.E6_8/kD

! --- Calcul du logarithme coulombien
      IF(col_log(col_i).LT.0.) THEN
        col_log(col_i) = LOG(kmax/kD)
        WRITE(fdbg,"('Log. Coulombien calcule pour les coll. de ',
     &               I2,' et ',I2,' : ',E9.3)") i1,i2,col_log(col_i)
      ELSEIF(col_log(col_i).EQ.0.) THEN
        WRITE(fdbg,"('Log. Coulombien calcule pour les coll. de ',
     &               I2,' et ',I2,' a chaque pasdt ')") i1,i2
      ELSE
        WRITE(fdbg,"('Log. Coulombien calcule pour les coll. de ',
     &               I2,' et ',I2,' : ',E9.3)") i1,i2,LOG(kmax/kD)
        WRITE(fdbg,"('mais fixe par le fichier de donnees a : ',
     &               E9.3)") col_log(col_i)
      ENDIF

! --- Facteurs col_s0 et col_s1 intervenant dans les sections efficaces     
      col_s0(col_i)=(charge(i1)*charge(i2))**2
     &               *(col_per(col_i)*pasdt/lambda(1))*1.771E-8_8
      IF(col_log(col_i).GT.0.) THEN 
        col_s0(col_i)=col_log(col_i)*col_s0(col_i)
      ENDIF

      col_s1(col_i)=col_per(col_i)*pasdt*(lambda(1))**(1./3.)*266._8
      
!     ______________________________
      END SUBROUTINE coll_relat_init



      SUBROUTINE calcul_debye
! ======================================================================
! Calcul de la longueur de debye au carre dans chaque cellule
! ======================================================================
      USE precisions
      USE domaines
      USE particules_klder
      USE collisions
      USE erreurs
      USE laser

      IMPLICIT NONE
      
      INTEGER        :: tot, iesp, nbpicell, n0, icell, ncell
      REAL(KIND=kr)  :: dens, densmax, temp, long, lmoy, voli

      tot=(nulpx-nulmx)*(nulpy-nulmy)*(nulpz-nulmz)
      voli = pasdxi*pasdyi*pasdzi

      lmoy = 0.
      ncell = 0

      DO icell = 1,tot
      
        long = 0.
        densmax = 0.

        DO iesp=1,nesp_p
          IF(nbpa(iesp).LT.1) CYCLE
          IF(icell.NE.tot) THEN
            nbpicell=compteur(icell+1,iesp)-compteur(icell,iesp)
          ELSE
            nbpicell=nbpa(iesp)+1-compteur(tot,iesp)
          ENDIF
          IF(nbpicell.LT.1) CYCLE
	  n0=compteur(icell,iesp)-1
	  dens=SUM(partic(a_po,n0+1:n0+nbpicell,iesp))
	  temp=DOT_PRODUCT(partic(a_po,n0+1:n0+nbpicell,iesp),
     &                  partic(a_ga,n0+1:n0+nbpicell,iesp)
     &                  -1./partic(a_ga,n0+1:n0+nbpicell,iesp) )
     &      * 1./3.*masse(iesp)/dens
	  dens=dens*voli
	  IF(dens.GT.densmax) densmax=dens
          IF(nbpicell.GT.0) long=long+charge(iesp)**2*dens/temp
        ENDDO
	
	ldebye2(icell)=0.
	IF(long.GT.0.) THEN
	  ldebye2(icell)=MAX(1._8/long, 1.41E-5/lambda(1)
     &     /densmax**(2./3.))
	  ncell = ncell + 1
	  lmoy = lmoy + ldebye2(icell)
	ENDIF

      ENDDO
      
      WRITE(fdbg,*) ' ldebye = ',SQRT(Lmoy)/REAL(ncell)

!     _____________________________________
      END SUBROUTINE calcul_debye



      SUBROUTINE intra_collisions_relat(col_i)
! ======================================================================
! Fonctionne uniquement en 3D, avec particules relativistes
! ======================================================================
      USE precisions
      USE domaines
      USE particules_klder
      USE collisions
      USE erreurs
      USE laser

      IMPLICIT NONE

      INTEGER, INTENT(IN)    :: col_i

      REAL(KIND=kr) :: s,scoll,logc,logcoll
      INTEGER :: ncoll
      
!=====Variables apparaissant dans 1
      INTEGER         :: i1, icell, nbpicell,nbpaires ,n0,tot
      REAL(KIND=kr)   :: B,C,D,E,n1, n11, voli
 
!=====Variables apparaissant dans 2
      INTEGER                                  :: ip1, ip2
      INTEGER(KIND=kr)                          :: provisoire
      INTEGER(KIND=kr),DIMENSION(:),ALLOCATABLE :: dummy
      INTEGER(kind=kr)                          :: graine
      REAL(KIND=kr),DIMENSION(:),ALLOCATABLE   :: moisson,moisson1

!=====Variables apparaissant dans 3
      INTEGER :: ipaire, ipairebis, iscoll1, iscoll2
      REAL(KIND=kr)  :: p1,p2

      INTERFACE
         SUBROUTINE aleat_unif(gerbe,methode,graine)
          USE precisions
          REAL(KIND=kr), DIMENSION(:), INTENT(OUT)   :: gerbe
          INTEGER                    , INTENT(IN)    :: methode
          INTEGER(kind=kr)            , INTENT(INOUT) :: graine
         END SUBROUTINE aleat_unif
      END INTERFACE

! --- Selection de l'espece qui subit les collisions
      i1 = col_p1(col_i)
! --- S'il n'y a pas assez de particules, on quitte la routine
      IF(nbpa(i1).LE.1) RETURN
          
      scoll = 0._8
      logcoll = 0._8
      ncoll = 0
      tot=(nulpx-nulmx)*(nulpy-nulmy)*(nulpz-nulmz)
      voli = pasdxi*pasdyi*pasdzi

! --- Selection de l'espece qui subit les collisions
      i1 = col_p1(col_i)

! --- Boucle sur l'ensemble des cellules
      DO icell = 1,tot

! -1--- Calcul du nombre de particules dans la cellule
        IF(icell.NE.tot) THEN
          nbpicell=compteur(icell+1,i1)-compteur(icell,i1)
        ELSE
          nbpicell=nbpa(i1)+1-compteur(tot,i1)
        ENDIF
!       S'il n'y a pas de quoi collisionner, on saute la cellule
        IF(nbpicell.LE.1) CYCLE
!       Si il n'y a pas assez de place dans les tableaux, on avertit
!        IF(nbpicell.GT.300)
!     &    print*, 'Depassement de taille : nbpicell=',nbpicell
     
        n0=compteur(icell,i1)-1

        nbpaires=INT(REAL(nbpicell+1)*0.5)
        ALLOCATE (moisson(nbpaires*2))
        ALLOCATE (moisson1(nbpaires))
        ALLOCATE (dummy(nbpicell))

!       Calcul de la densite de particules reelles dans la cellule
        n1=SUM(partic(a_po,n0+1:n0+nbpicell,i1))*voli


! -2--- Creation d'un tableau d'indices aleatoires afin de simuler un 
!       tirage aleatoire des paires de particules
        CALL aleat_unif(moisson,1,graine)
        DO ip1=1,nbpicell
          dummy(ip1)=ip1
        ENDDO
        DO ip1=1,nbpicell-1
          ip2=INT(moisson(ip1)*REAL(nbpicell-ip1+1))+ip1
          provisoire=dummy(ip2)
          dummy(ip2)=dummy(ip1)
          dummy(ip1)=provisoire
        ENDDO

!       Calcul de la densite 'croisee' n11 (cf.Nanbu)
        n11=0.
        DO ip1=1,nbpaires
          ipaire=2*ip1-1
          ipairebis=ipaire+1
          IF (ipairebis.GT.nbpicell) ipairebis=1
          n11=n11+MIN( partic(a_po,n0+dummy(ipaire),i1),
     &  	     partic(a_po,n0+dummy(ipairebis),i1) )
        ENDDO
	n11=2.*n11*voli
        
!       Calcul de différents facteurs
        B=col_s0(col_i)*n1*n1/(n11*masse(i1)**2)
        C=col_s1(col_i)*n1**(4./3.)/n11
        D=2.25E-8_8*charge(i1)**2/(masse(i1)*lambda(1))
        E=2.43E-6_8/(masse(i1)*lambda(1))
        

! -3--- Boucle sur toutes les paires de particules ipaire/ipairebis
        CALL aleat_unif(moisson,1,graine)
        CALL aleat_unif(moisson1,1,graine)
        DO ip1=1,nbpaires
          
          ipaire=2*ip1-1
          ipairebis=ipaire+1
          IF (ipairebis.GT.nbpicell) ipairebis=1
          
!         determination de la(les) particules qui vont effectivement
!          changer d'impulsion
          iscoll1=1
          iscoll2=1
          p1=partic(a_po,n0+dummy(ipaire),i1)
          p2=partic(a_po,n0+dummy(ipairebis),i1)
          IF(p1.GT.p2.AND.moisson1(ip1).GT.p2/p1) iscoll1=0
          IF(p2.GT.p1.AND.moisson1(ip1).GT.p1/p2) iscoll2=0

!         transformation des impulsions
          CALL collision_relat(col_log(col_i),ldebye2(icell),
     &                         1._8,B,C,D,E,iscoll1,iscoll2,
     &  		       partic(a_ga,n0+dummy(ipaire),i1),
     &  		       partic(a_ga,n0+dummy(ipairebis),i1),
     &  		       partic(a_pxtld,n0+dummy(ipaire),i1),
     &  		       partic(a_pytld,n0+dummy(ipaire),i1),
     &  		       partic(a_pztld,n0+dummy(ipaire),i1),
     &  		       partic(a_pxtld,n0+dummy(ipairebis),i1),
     &  		       partic(a_pytld,n0+dummy(ipairebis),i1),
     &  		       partic(a_pztld,n0+dummy(ipairebis),i1),
     &  		       moisson(ip1),moisson(2*ip1),
     &  		       s,logc)
           scoll=scoll+s
           ncoll=ncoll+1
	   IF(col_log(col_i).EQ.0.) logcoll=logcoll+logc
        ENDDO
	  
        DEALLOCATE (moisson)
        DEALLOCATE (moisson1)
        DEALLOCATE (dummy)
        
      ENDDO
      IF(ncoll.EQ.0) THEN
        WRITE(fdbg,*) 'coll #',col_i, ' : no collisions'
      ELSE
	WRITE(fdbg,*) 'coll #',col_i,' : log = ',logcoll/ncoll
        WRITE(fdbg,*) 'coll #',col_i,' : s = ',scoll/ncoll
      ENDIF
!     _____________________________________
      END SUBROUTINE intra_collisions_relat




      SUBROUTINE inter_collisions_relat(col_i)
! ======================================================================
! Fonctionne uniquement en 3D, avec particules relativistes
! ======================================================================
      USE precisions
      USE domaines
      USE particules_klder
      USE collisions
      USE erreurs
      USE laser

      IMPLICIT NONE

      INTEGER, INTENT(IN)    :: col_i

!=====Variables apparaissant dans 1
      INTEGER  :: i1, i2, itmp, icell, nbpicell1, nbpicell2,
     &           n01, n02,tot
      REAL(KIND=kr) :: B, C, D, E, rapport, n1, n2, n12, voli
 
!=====Variables apparaissant dans 2
      INTEGER                 :: ip1, ip2
      INTEGER(KIND=kr)                          :: provisoire
 
!=====Variables apparaissant dans 3 et 4
      REAL(KIND=kr)::s,scoll,logc,logcoll,p1,p2
      INTEGER :: ncoll,ipaire1, ipaire2, iscoll1,iscoll2

!=====Nombres aléatoires
      INTEGER(kind=kr)                          :: graine
      INTEGER(KIND=kr),DIMENSION(:),ALLOCATABLE :: dummy
      REAL(KIND=kr),DIMENSION(:),ALLOCATABLE::moisson,moisson2,moisson3
      INTERFACE
         SUBROUTINE aleat_unif(gerbe,methode,graine)
          USE precisions
          REAL(KIND=kr), DIMENSION(:), INTENT(OUT)   :: gerbe
          INTEGER                    , INTENT(IN)    :: methode
          INTEGER(kind=kr)            , INTENT(INOUT) :: graine
         END SUBROUTINE aleat_unif
      END INTERFACE
      
      
! --- Selection de l'espece qui subit les collisions
      i1 = col_p1(col_i) ; i2 = col_p2(col_i)
! --- S'il n'y a pas assez de particules, on quitte la routine
      IF((nbpa(i1).LT.1  ).OR.(nbpa(i2).LT.1  )) RETURN

      scoll = 0._8
      logcoll = 0._8
      ncoll = 0
      tot = (nulpx-nulmx)*(nulpy-nulmy)*(nulpz-nulmz)
      voli = pasdxi*pasdyi*pasdzi

! --- Boucle sur l'ensemble des cellules
      DO icell = 1,tot

! -1--- Calcul du nombre de particules dans la cellule
        IF(icell.NE.tot) THEN
          nbpicell1=compteur(icell+1,i1)-compteur(icell,i1)
          nbpicell2=compteur(icell+1,i2)-compteur(icell,i2)
        ELSE
          nbpicell1=nbpa(i1)+1-compteur(tot,i1)
          nbpicell2=nbpa(i2)+1-compteur(tot,i2)
        ENDIF
	
!       Si il n'y a pas de quoi collisionner, on saute la cellule
        IF((nbpicell1.LT.1  ).OR.(nbpicell2.LT.1  )) CYCLE

!       On fait en sorte que l'esp. 2 soit la plus nombreuse
        IF (nbpicell1.GT.nbpicell2) THEN
          itmp=i2        ; i2=i1               ; i1=itmp
          itmp=nbpicell2 ; nbpicell2=nbpicell1 ; nbpicell1=itmp
        ENDIF
        n01=compteur(icell,i1)-1
        n02=compteur(icell,i2)-1

!       calcul du rapport des masses
        rapport=masse(i1)/masse(i2)

        ALLOCATE (moisson(nbpicell2))
        ALLOCATE (moisson2(nbpicell2))
        ALLOCATE (moisson3(nbpicell2))
        ALLOCATE (dummy(nbpicell2))

!       Calcul de la densite de particules reelles dans la cellule
        n1=SUM(partic(a_po,n01+1:n01+nbpicell1,i1))*voli
        n2=SUM(partic(a_po,n02+1:n02+nbpicell2,i2))*voli

! -2--- Creation d'un tableau d'indices aleatoires afin de simuler un 
!       tirage aleatoire des paires de particules
        CALL aleat_unif(moisson,1,graine)
        DO ip1=1,nbpicell2
          dummy(ip1)=ip1
        ENDDO
        DO ip1=1,nbpicell2-1
          ip2=INT(moisson(ip1)*REAL(nbpicell2-ip1+1))+ip1
          provisoire=dummy(ip2)
          dummy(ip2)=dummy(ip1)
          dummy(ip1)=provisoire
        ENDDO

! -3--- Calcul de la densite 'croisee' n12 (cf.Nanbu)
        n12=0.

        DO ipaire2=1,nbpicell2
          ipaire1=MOD(ipaire2-1,nbpicell1)+1
          n12=n12+MIN( partic(a_po,n01+ipaire1,i1),
     &       partic(a_po,n02+dummy(ipaire2),i2) )
        ENDDO
	n12=n12*voli

!       Calcul de différents facteurs
        B=col_s0(col_i)*n1*n2/n12/masse(i1)**2
        C=col_s1(col_i)*n1*n2/(n12*MAX(n1,n2)**(2./3.))
        D=1.77E-8_8*charge(i1)*charge(i2)/(masse(i1)*lambda(1))
        E=7.6E-6_8/(masse(i1)*lambda(1))
	
        
! -4--- Boucle sur toutes les paires de particules ipaire1/ipaire2
        CALL aleat_unif(moisson,1,graine)
        CALL aleat_unif(moisson2,1,graine)
        CALL aleat_unif(moisson3,1,graine)
        DO ipaire2=1,nbpicell2
        
          ipaire1=MOD(ipaire2-1,nbpicell1)+1

!         determination de la(les) particules qui vont effectivement
!          changer d'impulsion
          iscoll1=1
          iscoll2=1
          p1=partic(a_po,n01+ipaire1,i1)
          p2=partic(a_po,n02+dummy(ipaire2),i2)
          IF(p1.GT.p2.AND.moisson3(ipaire2).GT.p2/p1) iscoll1=0
          IF(p2.GT.p1.AND.moisson3(ipaire2).GT.p1/p2) iscoll2=0


!         Transformation des impulsions

          CALL collision_relat(col_log(col_i),ldebye2(icell),
     &                         rapport,B,C,D,E,
     &                         iscoll1,iscoll2,
     &                         partic(a_ga,n01+ipaire1,i1),
     &                         partic(a_ga,n02+dummy(ipaire2),i2),
     &                         partic(a_pxtld,n01+ipaire1,i1),
     &                         partic(a_pytld,n01+ipaire1,i1),
     &                         partic(a_pztld,n01+ipaire1,i1),
     &                         partic(a_pxtld,n02+dummy(ipaire2),i2),
     &                         partic(a_pytld,n02+dummy(ipaire2),i2),
     &                         partic(a_pztld,n02+dummy(ipaire2),i2),
     &                         moisson(ipaire2),moisson2(ipaire2),
     &                         s,logc)
           scoll=scoll+s
           ncoll=ncoll+1
	   IF(col_log(col_i).EQ.0.) logcoll=logcoll+logc
        ENDDO

        DEALLOCATE (moisson)
        DEALLOCATE (moisson2)
        DEALLOCATE (moisson3)
        DEALLOCATE (dummy)
      
      ENDDO
      
     
      IF(ncoll.EQ.0) THEN
	WRITE(fdbg,*) 'coll #',col_i, ' : no collisions'
      ELSE
	WRITE(fdbg,*) 'coll #',col_i,' : log = ',logcoll/ncoll
	WRITE(fdbg,*) 'coll #',col_i,' : s = ',scoll/ncoll
      ENDIF
!     _____________________________________
      END SUBROUTINE inter_collisions_relat



      SUBROUTINE collision_relat(logc,L,r,B,C,D,E,coll1,coll2,
     &                           gamma1,gamma2,
     &                           p1x,p1y,p1z,p2x,p2y,p2z,
     &                           alea1,alea2,
     &                           s,logcoul)
! ======================================================================
! Permet de calculer les impulsions de deux particules apres collision
! rapport est le rapport des masses m1/m2, p1 et p2 sont les impulsions
! B intervient dans la distribution aleatoire de l'angle (cf. NANBU)
! ======================================================================
      USE precisions
      USE erreurs
      USE collisions

      IMPLICIT NONE

      REAL(KIND=kr), INTENT(IN) :: logc,L,r,B,C,D,E,alea1,alea2
      INTEGER, INTENT(IN) :: coll1,coll2
      REAL(KIND=kr), INTENT(INOUT) :: gamma1,gamma2,
     &                                p1x,p1y,p1z,p2x,p2y,p2z
      REAL(KIND=kr), INTENT(OUT) :: s,logcoul
    
      REAL(kind=kr), PARAMETER   :: PI   = 3.14159265359
      REAL(KIND=kr) :: gammaCM,
     &                 v1x,v1y,v1z,
     &                 v2x,v2y,v2z,
     &                 vCMx,vCMy,vCMz,vCM2,
     &                 costheta,sintheta,phi,s1,bmin,
     &                 pCM1x,pCM1y,pCM1z,pCMorth,pCM,
     &                 pCM1xbis,pCM1ybis,pCM1zbis,
     &                 tmpa,tmpb,tmpc,tmp1,tmp2,tmp3,tmpx,tmpy,tmpz
     
      costheta=1._8

! --- Vitesses des deux particules
      v1x=p1x/gamma1
      v2x=p2x/gamma2
      v1y=p1y/gamma1
      v2y=p2y/gamma2
      v1z=p1z/gamma1
      v2z=p2z/gamma2

! --- Vitesse et facteur de Lorentz du centre de masse
      tmpa=gamma1*r+gamma2
      vCMx=(p1x*r+p2x)/tmpa
      vCMy=(p1y*r+p2y)/tmpa
      vCMz=(p1z*r+p2z)/tmpa
      vCM2=vCMx**2+vCMy**2+vCMz**2
      gammaCM=1._8/SQRT(1._8-vCM2)
      
! --- Impulsion de la particule 1 dans le centre de masse
!       (Celle de la seconde particule lui est forcement opposee)
      tmpc=(gammaCM-1._8)/vCM2
      tmp1=vCMx*v1x + vCMy*v1y + vCMz*v1z
      tmp2=vCMx*v2x + vCMy*v2y + vCMz*v2z
      tmpb=(tmpc*tmp1-gammaCM)*gamma1
      
      pCM1x=p1x+tmpb*vCMx
      pCM1y=p1y+tmpb*vCMy
      pCM1z=p1z+tmpb*vCMz
      pCMorth=SQRT(pCM1x**2+pCM1y**2)
      pCM=SQRT(pCM1x**2+pCM1y**2+pCM1z**2)

      tmpa=tmpa/gammaCM
      tmpb=gammaCM**2*gamma1*gamma2*(1._8-tmp1)*(1._8-tmp2)
      tmp3=r+tmpb/pCM**2

! --- Calcul du parametre d'impact minimum bmin
      bmin=MAX(ABS(D*tmp3/tmpa),ABS(E/pCM))
! --- Calcul du logarithme coulombien
      logcoul=1.
      IF(logc.EQ.0.) logcoul=MAX(2.,LOG(1.+L/bmin**2)/2.)

! --- Calcul de s (NANBU) et de la version modifiee s1 (Lee&More)
      s=logcoul*B* pCM/(gamma1*gamma2*tmpa)*tmp3**2
      s1=C* (tmpa/tmpb) *pCM

      s=MIN(s,s1)

! --- Tirage aleatoire des angles theta et phi (cf. NANBU)
      CALL inverse_nanbu(s,costheta,alea1)
      sintheta=SQRT(1._8-costheta**2)
      phi=2._8*PI*alea2
      
! --- Impulsion de la particule 1 dans le CM apres collision
      tmpx=sintheta*COS(phi)
      tmpy=sintheta*SIN(phi)
      tmpz=costheta
!     si l'impulsion n'est pas parallele a z
      IF (pCMorth.GT .TINY(pCMorth)) THEN
        pCM1xbis=(pCM1x*pCM1z*tmpx-pCM1y*pCM*tmpy)/pCMorth+pCM1x*tmpz
        pCM1ybis=(pCM1y*pCM1z*tmpx+pCM1x*pCM*tmpy)/pCMorth+pCM1y*tmpz
        pCM1zbis=-tmpx*pCMorth+pCM1z*tmpz
      ELSE
        pCM1xbis=pCM*tmpx
        pCM1ybis=pCM*tmpy
        pCM1zbis=pCM*tmpz
      ENDIF

! --- Impulsion des particules dans le repere du labo apres collision
      tmpb=(vCMx*pCM1xbis+vCMy*pCM1ybis+vCMz*pCM1zbis)*tmpc
      tmp1=gammaCM**2*gamma1*(1._8-tmp1)+tmpb
      tmp2=gammaCM**2*gamma2*(1._8-tmp2)-tmpb*r
      
      IF(coll1.EQ.1) THEN
         p1x= tmp1*vCMx + PCM1xbis
         p1y= tmp1*vCMy + PCM1ybis
         p1z= tmp1*vCMz + PCM1zbis
         gamma1=SQRT(1._8+p1x**2+p1y**2+p1z**2)
      ENDIF
      IF(coll2.EQ.1) THEN
        p2x= tmp2*vCMx - PCM1xbis*r
        p2y= tmp2*vCMy - PCM1ybis*r
        p2z= tmp2*vCMz - PCM1zbis*r
        gamma2=SQRT(1._8+p2x**2+p2y**2+p2z**2)
      ENDIF
      
!     ______________________________
      END SUBROUTINE collision_relat




      SUBROUTINE inverse_nanbu(s,costheta,alea)
! ======================================================================
! Permet de calculer la solution A de l'equation cothA - 1/A = exp(-s)
! puis l'angle costheta = ( ln( exp(-A)+2U*sh(A) ) )/A
! cf. NANBU
! ======================================================================
      USE precisions

      IMPLICIT NONE

      REAL(KIND=kr), INTENT(IN) :: s,alea
      REAL(KIND=kr), INTENT(OUT) :: costheta

      REAL(KIND=kr) A,s2,s3,s4,s5,newalea

      newalea=alea
      IF (newalea.LT.0.01_8) newalea=0.01_8

      IF (s.LE.0.1_8) THEN
        costheta=1+s*LOG(newalea)

      ELSEIF (s.LE.3._8) THEN
        s2=s*s
        s3=s2*s
        s4=s3*s
        s5=s4*s
        A=1/ (0.00569578_8 +0.95602019_8*s -0.50813899_8*s2
     &        +0.4791390611_8*s3 -0.12788975_8*s4 +0.02389567_8*s5 )
        costheta=( LOG( EXP(-A)+2._8*newalea*SINH(A) ) )/A

      ELSEIF (s.LE.6._8) THEN
        A=3.0_8*EXP(-s)
        costheta=( LOG( EXP(-A)+2._8*newalea*SINH(A) ) )/A

      ELSE
        costheta=2._8*alea-1._8
 
      END IF

!     ____________________________
      END SUBROUTINE inverse_nanbu
