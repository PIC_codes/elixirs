! ======================================================================
! tableaux.f
! ----------
!
!     SUBROUTINE decl_tabl
!     SUBROUTINE verif_tabl
!
!
! ======================================================================



      SUBROUTINE decl_tabl
! ======================================================================
! Allocation dynamique des differents (gros) tableaux
! ======================================================================
      USE domaines
      USE domaines_klder
      USE champs
      USE champs_iter
      USE particules
      USE particules_klder
      USE temps
      USE collisions
      USE diagnostics
      USE diagnostics_klder
      USE diag_ptest
      USE erreurs
      

      IMPLICIT NONE
      
     
      INTEGER    :: iesp, ok, a_courant
      

       ALLOCATE( bz0(0:n1x) )
 
      
! ---- Allocations code implicite
! ---- allocation des tableaux de champ électrique et magnétique
!      ALLOCATE( bx_test(0:n1x,0:n1y) )

      ALLOCATE( bx(0:n1x,0:n1y) )
      ALLOCATE( by(0:n1x,0:n1y) ) 
      ALLOCATE( bz(0:n1x,0:n1y) )
      
      ALLOCATE( ex(0:n1x,0:n1y) ) 
      ALLOCATE( ey(0:n1x,0:n1y) )
      ALLOCATE( ez(0:n1x,0:n1y) )
      
      IF(tta_def.EQ.1) THEN
        ALLOCATE( thetad_2(0:n1x,0:n1y) )
      ENDIF

      IF(simplified.EQ.2) THEN
        ALLOCATE( dexdx(0:n1x,0:n1y) ) 
        ALLOCATE( dexdy(0:n1x,0:n1y) )
        ALLOCATE( deydx(0:n1x,0:n1y) ) 
        ALLOCATE( deydy(0:n1x,0:n1y) )
        ALLOCATE( dezdx(0:n1x,0:n1y) ) 
        ALLOCATE( dezdy(0:n1x,0:n1y) )
      ENDIF

      ALLOCATE( extm1(0:n1x,0:n1y) ) 
      ALLOCATE( eytm1(0:n1x,0:n1y) )
      ALLOCATE( eztm1(0:n1x,0:n1y) )

      ALLOCATE( extm1m(0:n1y)  )
      ALLOCATE( eytm1m(0:n1x)  )

      ALLOCATE( exm(0:n1y)  )
      ALLOCATE( bym(0:n1y)  )
      ALLOCATE( bzm1(0:n1y) )
      
      ALLOCATE( eym(0:n1x)  )
      ALLOCATE( bxm(0:n1x)  )
      ALLOCATE( bzm2(0:n1x) )

! ---- Allocation tableaux moyennés suivant y
      ALLOCATE( bx_1d(0:n1x) )
      ALLOCATE( by_1d(0:n1x) ) 
      ALLOCATE( bz_1d(0:n1x) )
      
      ALLOCATE( ex_1d(0:n1x) ) 
      ALLOCATE( ey_1d(0:n1x) )
      ALLOCATE( ez_1d(0:n1x) )
   
! ---- allocation des tableaux de densité et de courant
! ---- i et e désignent respectivement les ions et les électrons
      IF(ordre_interp.EQ.1) THEN
        ALLOCATE( rho_esp_2(0:n1x, 0:n1y, 1:nesp_p) )
        ALLOCATE( cjx_esp_2(0:n1x, 0:n1y, 1:nesp_p) )                  
        ALLOCATE( cjy_esp_2(0:n1x, 0:n1y, 1:nesp_p) )
        ALLOCATE( cjz_esp_2(0:n1x, 0:n1y, 1:nesp_p) )                  
        ALLOCATE( dgx_esp_2(0:n1x, 0:n1y, 1:nesp_p) )
        ALLOCATE( dgy_esp_2(0:n1x, 0:n1y, 1:nesp_p) )                  
        ALLOCATE( dgz_esp_2(0:n1x, 0:n1y, 1:nesp_p) ) 

        IF(iter_es.EQ.1) THEN
          ALLOCATE( rho_esp2_ik(0:n1x, 0:n1y, 1:nesp_p) )
        ENDIF
      ELSEIF(ordre_interp.EQ.2 .OR. ordre_interp.EQ.3) THEN
        ALLOCATE( rho_esp_2(0:n1x, -1:n1y, 1:nesp_p) )
        ALLOCATE( cjx_esp_2(0:n1x, -1:n1y, 1:nesp_p) )                  
        ALLOCATE( cjy_esp_2(0:n1x, -1:n1y, 1:nesp_p) )
        ALLOCATE( cjz_esp_2(0:n1x, -1:n1y, 1:nesp_p) )                  
        ALLOCATE( dgx_esp_2(0:n1x, -1:n1y, 1:nesp_p) )
        ALLOCATE( dgy_esp_2(0:n1x, -1:n1y, 1:nesp_p) )                  
        ALLOCATE( dgz_esp_2(0:n1x, -1:n1y, 1:nesp_p) ) 

        IF(iter_es.EQ.1) THEN
          ALLOCATE( rho_esp2_ik(0:n1x, -1:n1y, 1:nesp_p) )
        ENDIF
      ELSEIF(ordre_interp.EQ.4) THEN
        ALLOCATE( rho_esp_2(0:n1x, -2:ny+2, 1:nesp_p) )
        ALLOCATE( cjx_esp_2(0:n1x, -2:ny+2, 1:nesp_p) )
        ALLOCATE( cjy_esp_2(0:n1x, -2:ny+2, 1:nesp_p) )
        ALLOCATE( cjz_esp_2(0:n1x, -2:ny+2, 1:nesp_p) )
        ALLOCATE( dgx_esp_2(0:n1x, -2:ny+2, 1:nesp_p) )
        ALLOCATE( dgy_esp_2(0:n1x, -2:ny+2, 1:nesp_p) )
        ALLOCATE( dgz_esp_2(0:n1x, -2:ny+2, 1:nesp_p) ) 

        IF(iter_es.EQ.1) THEN
          ALLOCATE( rho_esp2_ik(0:n1x, -2:ny+2, 1:nesp_p) )
        ENDIF	
      ENDIF

      ALLOCATE( jxtot(0:n1x, 0:n1y) )
      ALLOCATE( jytot(0:n1x, 0:n1y) )                  
      ALLOCATE( jztot(0:n1x, 0:n1y) )

      ALLOCATE( phi(0:n1x,0:n1y) )
      ALLOCATE( s(0:n1x,0:n1y)   )

      IF(iter_es.EQ.1) THEN
        ALLOCATE( src_max(0:n1x,0:n1y) )
      ENDIF

! --- composantes du tenseur de susceptibilité ki 
! --- xxx pour x11, xxz pour x13
      IF(ordre_interp.EQ.1) THEN
        ALLOCATE( xxx(0:n1x,0:n1y) )
        ALLOCATE( xxy(0:n1x,0:n1y) )
        ALLOCATE( xxz(0:n1x,0:n1y) )
        ALLOCATE( xyx(0:n1x,0:n1y) )
        ALLOCATE( xyy(0:n1x,0:n1y) )
        ALLOCATE( xyz(0:n1x,0:n1y) )
        ALLOCATE( xzx(0:n1x,0:n1y) )
        ALLOCATE( xzy(0:n1x,0:n1y) )
        ALLOCATE( xzz(0:n1x,0:n1y) )
        IF(max_cycles.GT.1) THEN
          ALLOCATE( xxx_esp(0:n1x,0:n1y, 1:nesp_p) )
          ALLOCATE( xxy_esp(0:n1x,0:n1y, 1:nesp_p) )
          ALLOCATE( xxz_esp(0:n1x,0:n1y, 1:nesp_p) )
          ALLOCATE( xyx_esp(0:n1x,0:n1y, 1:nesp_p) )
          ALLOCATE( xyy_esp(0:n1x,0:n1y, 1:nesp_p) )
          ALLOCATE( xyz_esp(0:n1x,0:n1y, 1:nesp_p) )
          ALLOCATE( xzx_esp(0:n1x,0:n1y, 1:nesp_p) )
          ALLOCATE( xzy_esp(0:n1x,0:n1y, 1:nesp_p) )
          ALLOCATE( xzz_esp(0:n1x,0:n1y, 1:nesp_p) )	
	  
          ALLOCATE( zxx_esp(0:n1x,0:n1y, 1:nesp_p) )
          ALLOCATE( zxy_esp(0:n1x,0:n1y, 1:nesp_p) )
          ALLOCATE( zxz_esp(0:n1x,0:n1y, 1:nesp_p) )           
          ALLOCATE( zyx_esp(0:n1x,0:n1y, 1:nesp_p) )
          ALLOCATE( zyy_esp(0:n1x,0:n1y, 1:nesp_p) )
          ALLOCATE( zyz_esp(0:n1x,0:n1y, 1:nesp_p) )      
          ALLOCATE( zzx_esp(0:n1x,0:n1y, 1:nesp_p) )
          ALLOCATE( zzy_esp(0:n1x,0:n1y, 1:nesp_p) )
          ALLOCATE( zzz_esp(0:n1x,0:n1y, 1:nesp_p) )
	ENDIF	
	
        IF(iter_es.EQ.1 .AND. max_cycles.GT.1) THEN
          WRITE(fmsgo,"('le sub_cycling n est pas pris en charge'
     &         'avec les itérations de Newton du solveur '  
     &         'électrostatique : ERREUR FATALE !')")	 
          STOP 
	ELSEIF(iter_es.EQ.1) THEN
          ALLOCATE( xxx_ik(0:n1x,0:n1y) )
          ALLOCATE( xxy_ik(0:n1x,0:n1y) )
          ALLOCATE( xxz_ik(0:n1x,0:n1y) )
          ALLOCATE( xyx_ik(0:n1x,0:n1y) )
          ALLOCATE( xyy_ik(0:n1x,0:n1y) )
          ALLOCATE( xyz_ik(0:n1x,0:n1y) )
          ALLOCATE( xzx_ik(0:n1x,0:n1y) )
          ALLOCATE( xzy_ik(0:n1x,0:n1y) )
          ALLOCATE( xzz_ik(0:n1x,0:n1y) )
        ENDIF
      ELSEIF(ordre_interp.EQ.2 .OR. ordre_interp.EQ.3) THEN
        ALLOCATE( xxx(0:n1x,-1:n1y) )
        ALLOCATE( xxy(0:n1x,-1:n1y) )
        ALLOCATE( xxz(0:n1x,-1:n1y) )
        ALLOCATE( xyx(0:n1x,-1:n1y) )
        ALLOCATE( xyy(0:n1x,-1:n1y) )
        ALLOCATE( xyz(0:n1x,-1:n1y) )
        ALLOCATE( xzx(0:n1x,-1:n1y) )
        ALLOCATE( xzy(0:n1x,-1:n1y) )
        ALLOCATE( xzz(0:n1x,-1:n1y) )
        IF(max_cycles.GT.1) THEN
          ALLOCATE( xxx_esp(0:n1x,-1:n1y, 1:nesp_p) )
          ALLOCATE( xxy_esp(0:n1x,-1:n1y, 1:nesp_p) )
          ALLOCATE( xxz_esp(0:n1x,-1:n1y, 1:nesp_p) )
          ALLOCATE( xyx_esp(0:n1x,-1:n1y, 1:nesp_p) )
          ALLOCATE( xyy_esp(0:n1x,-1:n1y, 1:nesp_p) )
          ALLOCATE( xyz_esp(0:n1x,-1:n1y, 1:nesp_p) )
          ALLOCATE( xzx_esp(0:n1x,-1:n1y, 1:nesp_p) )
          ALLOCATE( xzy_esp(0:n1x,-1:n1y, 1:nesp_p) )
          ALLOCATE( xzz_esp(0:n1x,-1:n1y, 1:nesp_p) )
	  
          ALLOCATE( zxx_esp(0:n1x,-1:n1y, 1:nesp_p) )
          ALLOCATE( zxy_esp(0:n1x,-1:n1y, 1:nesp_p) )
          ALLOCATE( zxz_esp(0:n1x,-1:n1y, 1:nesp_p) )           
          ALLOCATE( zyx_esp(0:n1x,-1:n1y, 1:nesp_p) )
          ALLOCATE( zyy_esp(0:n1x,-1:n1y, 1:nesp_p) )
          ALLOCATE( zyz_esp(0:n1x,-1:n1y, 1:nesp_p) )      
          ALLOCATE( zzx_esp(0:n1x,-1:n1y, 1:nesp_p) )
          ALLOCATE( zzy_esp(0:n1x,-1:n1y, 1:nesp_p) )
          ALLOCATE( zzz_esp(0:n1x,-1:n1y, 1:nesp_p) )
	ENDIF	
	
        IF(iter_es.EQ.1 .AND. max_cycles.GT.1) THEN
          WRITE(fmsgo,"('tableaux.f : le sub_cycling n est '
     &         'pas pris en charge'
     &         'avec les itérations de Newton du solveur '  
     &         'électrostatique ERREUR FATALE !')")
          STOP	  
	ELSEIF(iter_es.EQ.1) THEN
          ALLOCATE( xxx_ik(0:n1x,-1:n1y) )
          ALLOCATE( xxy_ik(0:n1x,-1:n1y) )
          ALLOCATE( xxz_ik(0:n1x,-1:n1y) )
          ALLOCATE( xyx_ik(0:n1x,-1:n1y) )
          ALLOCATE( xyy_ik(0:n1x,-1:n1y) )
          ALLOCATE( xyz_ik(0:n1x,-1:n1y) )
          ALLOCATE( xzx_ik(0:n1x,-1:n1y) )
          ALLOCATE( xzy_ik(0:n1x,-1:n1y) )
          ALLOCATE( xzz_ik(0:n1x,-1:n1y) )
        ENDIF
	
      ELSEIF(ordre_interp.EQ.4) THEN
        ALLOCATE( xxx(0:n1x,-2:ny+2) )
        ALLOCATE( xxy(0:n1x,-2:ny+2) )
        ALLOCATE( xxz(0:n1x,-2:ny+2) )
        ALLOCATE( xyx(0:n1x,-2:ny+2) )
        ALLOCATE( xyy(0:n1x,-2:ny+2) )
        ALLOCATE( xyz(0:n1x,-2:ny+2) )
        ALLOCATE( xzx(0:n1x,-2:ny+2) )
        ALLOCATE( xzy(0:n1x,-2:ny+2) )
        ALLOCATE( xzz(0:n1x,-2:ny+2) )
        IF(max_cycles.GT.1) THEN
          ALLOCATE( xxx_esp(0:n1x,-2:ny+2, 1:nesp_p) )
          ALLOCATE( xxy_esp(0:n1x,-2:ny+2, 1:nesp_p) )
          ALLOCATE( xxz_esp(0:n1x,-2:ny+2, 1:nesp_p) )
          ALLOCATE( xyx_esp(0:n1x,-2:ny+2, 1:nesp_p) )
          ALLOCATE( xyy_esp(0:n1x,-2:ny+2, 1:nesp_p) )
          ALLOCATE( xyz_esp(0:n1x,-2:ny+2, 1:nesp_p) )
          ALLOCATE( xzx_esp(0:n1x,-2:ny+2, 1:nesp_p) )
          ALLOCATE( xzy_esp(0:n1x,-2:ny+2, 1:nesp_p) )
          ALLOCATE( xzz_esp(0:n1x,-2:ny+2, 1:nesp_p) )
	  
          ALLOCATE( zxx_esp(0:n1x,-2:ny+2, 1:nesp_p) )
          ALLOCATE( zxy_esp(0:n1x,-2:ny+2, 1:nesp_p) )
          ALLOCATE( zxz_esp(0:n1x,-2:ny+2, 1:nesp_p) )           
          ALLOCATE( zyx_esp(0:n1x,-2:ny+2, 1:nesp_p) )
          ALLOCATE( zyy_esp(0:n1x,-2:ny+2, 1:nesp_p) )
          ALLOCATE( zyz_esp(0:n1x,-2:ny+2, 1:nesp_p) )      
          ALLOCATE( zzx_esp(0:n1x,-2:ny+2, 1:nesp_p) )
          ALLOCATE( zzy_esp(0:n1x,-2:ny+2, 1:nesp_p) )
          ALLOCATE( zzz_esp(0:n1x,-2:ny+2, 1:nesp_p) )
	ENDIF	
	
        IF(iter_es.EQ.1 .AND. max_cycles.GT.1) THEN
          WRITE(fmsgo,"('tableaux.f : le sub_cycling n est '
     &         'pas pris en charge'
     &         'avec les itérations de Newton du solveur '  
     &         'électrostatique ERREUR FATALE !')")
          STOP	  
	ELSEIF(iter_es.EQ.1) THEN
          ALLOCATE( xxx_ik(0:n1x,-2:ny+2) )
          ALLOCATE( xxy_ik(0:n1x,-2:ny+2) )
          ALLOCATE( xxz_ik(0:n1x,-2:ny+2) )
          ALLOCATE( xyx_ik(0:n1x,-2:ny+2) )
          ALLOCATE( xyy_ik(0:n1x,-2:ny+2) )
          ALLOCATE( xyz_ik(0:n1x,-2:ny+2) )
          ALLOCATE( xzx_ik(0:n1x,-2:ny+2) )
          ALLOCATE( xzy_ik(0:n1x,-2:ny+2) )
          ALLOCATE( xzz_ik(0:n1x,-2:ny+2) )
        ENDIF	
      ENDIF
      
    
      IF(debug.GE.4) THEN
        ALLOCATE( xxxr1(0:n1x,0:n1y) )
        ALLOCATE( xxyr1(0:n1x,0:n1y) )
        ALLOCATE( xxzr1(0:n1x,0:n1y) )
        ALLOCATE( xyxr1(0:n1x,0:n1y) )
        ALLOCATE( xyyr1(0:n1x,0:n1y) )
        ALLOCATE( xyzr1(0:n1x,0:n1y) )
        ALLOCATE( xzxr1(0:n1x,0:n1y) )
        ALLOCATE( xzyr1(0:n1x,0:n1y) )
        ALLOCATE( xzzr1(0:n1x,0:n1y) )
      
        ALLOCATE( xxxr2(0:n1x,0:n1y) )
        ALLOCATE( xxyr2(0:n1x,0:n1y) )
        ALLOCATE( xxzr2(0:n1x,0:n1y) )
        ALLOCATE( xyxr2(0:n1x,0:n1y) )
        ALLOCATE( xyyr2(0:n1x,0:n1y) )
        ALLOCATE( xyzr2(0:n1x,0:n1y) )
        ALLOCATE( xzxr2(0:n1x,0:n1y) )
        ALLOCATE( xzyr2(0:n1x,0:n1y) )
        ALLOCATE( xzzr2(0:n1x,0:n1y) )

        ALLOCATE( zxxr1(0:n1x,0:n1y) )
        ALLOCATE( zxyr1(0:n1x,0:n1y) )
        ALLOCATE( zxzr1(0:n1x,0:n1y) )           
        ALLOCATE( zyxr1(0:n1x,0:n1y) )
        ALLOCATE( zyyr1(0:n1x,0:n1y) )
        ALLOCATE( zyzr1(0:n1x,0:n1y) )      
        ALLOCATE( zzxr1(0:n1x,0:n1y) )
        ALLOCATE( zzyr1(0:n1x,0:n1y) )
        ALLOCATE( zzzr1(0:n1x,0:n1y) )
	
        ALLOCATE( xxx0r1(0:n1x) )
        ALLOCATE( xxy0r1(0:n1x) ) 
        ALLOCATE( xxz0r1(0:n1x) )                  
        ALLOCATE( xyx0r1(0:n1x) )
        ALLOCATE( xyy0r1(0:n1x) )
        ALLOCATE( xyz0r1(0:n1x) )                   
        ALLOCATE( xzx0r1(0:n1x) )
        ALLOCATE( xzy0r1(0:n1x) )
        ALLOCATE( xzz0r1(0:n1x) )
      
        ALLOCATE( xxx0r2(0:n1x) )
        ALLOCATE( xxy0r2(0:n1x) ) 
        ALLOCATE( xxz0r2(0:n1x) )                  
        ALLOCATE( xyx0r2(0:n1x) )
        ALLOCATE( xyy0r2(0:n1x) )
        ALLOCATE( xyz0r2(0:n1x) )                   
        ALLOCATE( xzx0r2(0:n1x) )
        ALLOCATE( xzy0r2(0:n1x) )
        ALLOCATE( xzz0r2(0:n1x) )
	
        ALLOCATE( zxx0r1(0:n1x) )
        ALLOCATE( zxy0r1(0:n1x) )
        ALLOCATE( zxz0r1(0:n1x) )               
        ALLOCATE( zyx0r1(0:n1x) )
        ALLOCATE( zyy0r1(0:n1x) )
        ALLOCATE( zyz0r1(0:n1x) )               
        ALLOCATE( zzx0r1(0:n1x) )
        ALLOCATE( zzy0r1(0:n1x) )
        ALLOCATE( zzz0r1(0:n1x) )      
      ENDIF
    
! --- composantes du tenseur de susceptibilité ki 
! --- moyennées suivant y 
      ALLOCATE( xxx0(0:n1x) )
      ALLOCATE( xxy0(0:n1x) ) 
      ALLOCATE( xxz0(0:n1x) )                  
      ALLOCATE( xyx0(0:n1x) )
      ALLOCATE( xyy0(0:n1x) )
      ALLOCATE( xyz0(0:n1x) )                   
      ALLOCATE( xzx0(0:n1x) )
      ALLOCATE( xzy0(0:n1x) )
      ALLOCATE( xzz0(0:n1x) )
      IF(iter_es.EQ.1) THEN
        ALLOCATE( xxx0_ik(0:n1x) )
        ALLOCATE( xxy0_ik(0:n1x) ) 
        ALLOCATE( xxz0_ik(0:n1x) )                  
        ALLOCATE( xyx0_ik(0:n1x) )
        ALLOCATE( xyy0_ik(0:n1x) )
        ALLOCATE( xyz0_ik(0:n1x) )                   
        ALLOCATE( xzx0_ik(0:n1x) )
        ALLOCATE( xzy0_ik(0:n1x) )
        ALLOCATE( xzz0_ik(0:n1x) )
      ENDIF     
 
      ALLOCATE( zrx(0:n1x,0:n1y) )
      ALLOCATE( zry(0:n1x,0:n1y) )                 
      ALLOCATE( zrz(0:n1x,0:n1y) )    
      
! --- composantes du tenseur de susceptibilité zeta 
! --- zxx pour z11, zxz pour z13
      IF(ordre_interp.EQ.1) THEN
        ALLOCATE( zxx(0:n1x,0:n1y) )
        ALLOCATE( zxy(0:n1x,0:n1y) )
        ALLOCATE( zxz(0:n1x,0:n1y) )           
        ALLOCATE( zyx(0:n1x,0:n1y) )
        ALLOCATE( zyy(0:n1x,0:n1y) )
        ALLOCATE( zyz(0:n1x,0:n1y) )      
        ALLOCATE( zzx(0:n1x,0:n1y) )
        ALLOCATE( zzy(0:n1x,0:n1y) )
        ALLOCATE( zzz(0:n1x,0:n1y) )
      ELSEIF(ordre_interp.EQ.2 .OR. ordre_interp.EQ.3) THEN
        ALLOCATE( zxx(0:n1x,-1:n1y) )
        ALLOCATE( zxy(0:n1x,-1:n1y) )
        ALLOCATE( zxz(0:n1x,-1:n1y) )           
        ALLOCATE( zyx(0:n1x,-1:n1y) )
        ALLOCATE( zyy(0:n1x,-1:n1y) )
        ALLOCATE( zyz(0:n1x,-1:n1y) )      
        ALLOCATE( zzx(0:n1x,-1:n1y) )
        ALLOCATE( zzy(0:n1x,-1:n1y) )
        ALLOCATE( zzz(0:n1x,-1:n1y) )
      ELSEIF(ordre_interp.EQ.4) THEN
        ALLOCATE( zxx(0:n1x,-2:ny+2) )
        ALLOCATE( zxy(0:n1x,-2:ny+2) )
        ALLOCATE( zxz(0:n1x,-2:ny+2) )           
        ALLOCATE( zyx(0:n1x,-2:ny+2) )
        ALLOCATE( zyy(0:n1x,-2:ny+2) )
        ALLOCATE( zyz(0:n1x,-2:ny+2) )      
        ALLOCATE( zzx(0:n1x,-2:ny+2) )
        ALLOCATE( zzy(0:n1x,-2:ny+2) )
        ALLOCATE( zzz(0:n1x,-2:ny+2) )	
      ENDIF
     
! --- composantes du tenseur de susceptibilité zeta 
! --- moyennées suivant y 
      ALLOCATE( zxx0(0:n1x) )
      ALLOCATE( zxy0(0:n1x) )
      ALLOCATE( zxz0(0:n1x) )               
      ALLOCATE( zyx0(0:n1x) )
      ALLOCATE( zyy0(0:n1x) )
      ALLOCATE( zyz0(0:n1x) )               
      ALLOCATE( zzx0(0:n1x) )
      ALLOCATE( zzy0(0:n1x) )
      ALLOCATE( zzz0(0:n1x) )
     
     
      ALLOCATE( zeta(0:n1x,0:n1y) )
      ALLOCATE( z(0:n1x,0:n1y) )
     
! --- bx, by, bz à tn
      ALLOCATE( bx_tn(0:n1x,0:n1y) ) 
      ALLOCATE( by_tn(0:n1x,0:n1y) )
      ALLOCATE( bz_tn(0:n1x,0:n1y) ) 

      ALLOCATE( qxbound(0:n1y) ) 
      ALLOCATE( qybound(0:n1y) )
      ALLOCATE( qzbound(0:n1y) )            

      ALLOCATE( qxnx(0:n1y) ) 
      ALLOCATE( qynxm1(0:n1y) )
      ALLOCATE( qznxm1(0:n1y) ) 
     
      ALLOCATE( qx(0:n1x,0:n1y) ) 
      ALLOCATE( qy(0:n1x,0:n1y) )
      ALLOCATE( qz(0:n1x,0:n1y) ) 
                
      ALLOCATE( emoyx(0:n1x,0:n1y) )
      ALLOCATE( emoyy(0:n1x,0:n1y) )
      ALLOCATE( emoyz(0:n1x,0:n1y) )         
      
      ALLOCATE( ebx(0:n1x,0:n1y) )
      ALLOCATE( eby(0:n1x,0:n1y) )
      ALLOCATE( ebz(0:n1x,0:n1y) ) 

      ALLOCATE( ebbx(0:n1x,0:n1y) )
      ALLOCATE( ebby(0:n1x,0:n1y) )
      ALLOCATE( ebbz(0:n1x,0:n1y) ) 
      
      ALLOCATE( zetax(0:n1x,0:n1y) )
      ALLOCATE( zetay(0:n1x,0:n1y) )             
      ALLOCATE( zetaz(0:n1x,0:n1y) )
     
      ALLOCATE( csym1(ny) )
      ALLOCATE( csyp1(ny) ) 
      ALLOCATE( sny(ny)   )
      ALLOCATE( cosy(ny)   )
      ALLOCATE( cwk(n2y)  )

! --- Allocation des tableau de diag de impl2d
      ALLOCATE( welec(itfin) )
      ALLOCATE( wmagn(itfin) )
      ALLOCATE( enert(itfin) )
      ALLOCATE( dcin(itfin)  )

! --- Allocations variables Klder
      IF(nesp_p.GT.0) THEN
! Allocation des tableaux comptant les particules par espece
! ----------------------------------------------------------------------
      ALLOCATE(nbpa(nesp_p))
      ALLOCATE(nbpa_gl(nesp_p,nproc))

! Allocation du tableau de particules et definition des alias
! ----------------------------------------------------------------------
      a_po   = 0
      a_qx   = 0 ; a_qy   = 0 ; a_qz   = 0
      a_qxm1 = 0 ; a_qym1 = 0 ; a_qzm1 = 0
      a_px   = 0 ; a_py   = 0 ; a_pz   = 0
      a_wx   = 0 ; a_wy   = 0 ; a_wz   = 0
      a_pt   = 0
      
      a_bxp   = 0 ; a_byp     = 0 ; a_bzp   = 0
      a_pxtld = 0 ; a_pytld   = 0 ; a_pztld = 0
      a_gatld = 0 ; a_gatld12 = 0
      a_gaxe  = 0 ; a_gaye    = 0 ; a_gaze = 0
      a_paxe  = 0 ; a_paye    = 0 ; a_paze = 0
      a_ttapushs2 = 0
     
      SELECT CASE(dim_cas)
      CASE(6)
        a_po = 1
        a_qx = 2 ; a_qy = 3 ; a_qz = 4
        a_px = 5 ; a_py = 6 ; a_pz = 7

        a_bxp   = 8  ; a_byp     = 9 ; a_bzp   = 10
        a_pxtld = 11 ; a_pytld  = 12 ; a_pztld = 13
        a_gatld = 14 ; a_gatld12 = 15
        a_gaxe  = 16 ; a_gaye    = 17 ; a_gaze = 18
        a_paxe  = 19 ; a_paye    = 20 ; a_paze = 21
        a_courant = a_paze
!!! --- MODE friction pour les particules
        IF(friction_tta.EQ.1) THEN
	  a_ttapushs2 = a_courant+1
	  a_courant = a_ttapushs2
        ENDIF
!!! --- SONDE
        IF(esir_ok.EQ.1) THEN
          a_qxm1 = a_courant+1 ; a_qym1 = a_courant+2  
          a_qzm1 = a_courant+3
          a_courant = a_qzm1
        ENDIF
!!! --- FAURE
        IF(ANY(dph(:)%axe(1)(1:1).EQ.'w')) THEN
          a_wx = a_courant+1 ; a_wy = a_courant+2
          a_courant = a_wy
        ENDIF
!!! --- PARTICULES-TEST
        IF(dpt_ok.GT.0) THEN
          a_pt = a_courant+1
          a_courant = a_pt
        ENDIF
	WRITE(fdbg,"('Dans tableaux_sun : a_courant =',I2,1X,I2)")
     &     a_pt,a_courant
        ALLOCATE(partic(a_courant,nbpamax,nesp_p), stat=ok)
!!! --- FAURE
        IF(ANY(dph(:)%axe(1)(1:1).EQ.'w')) THEN
          partic(a_wx:a_wy,:,:) = 0.
          print*, 'CALDER 3D EN MODE *FAURE* !!!'
        ENDIF

      CASE(5)
        a_po = 1
        a_qx = 2 ; a_qy = 3
        a_px = 4 ; a_py = 5 ; a_pz = 6

        a_bxp   = 7  ; a_byp     = 8 ; a_bzp   = 9
        a_pxtld = 10 ; a_pytld  = 11 ; a_pztld = 12
        a_gatld = 13 ; a_gatld12 = 14
        a_gaxe  = 15 ; a_gaye    = 16 ; a_gaze = 17
        a_paxe  = 18 ; a_paye    = 19 ; a_paze = 20
        a_courant = a_paze
!!! --- MODE friction pour les particules
        IF(friction_tta.EQ.1) THEN
	  a_ttapushs2 = a_courant+1
	  a_courant = a_ttapushs2
        ENDIF
!!! --- SONDE
        IF(esir_ok.EQ.1) THEN
          a_qxm1 = a_courant+1 ; a_qym1 = a_courant+2
          a_courant = a_qym1
        ENDIF
!!! --- FAURE
        IF(ANY(dph(:)%axe(1)(1:1).EQ.'w')) THEN
          a_wx = a_courant+1 ; a_wy = a_courant+2
          a_courant = a_wy
        ENDIF
!!! --- PARTICULES-TEST
        IF(dpt_ok.GT.0) THEN
          a_pt = a_courant+1
          a_courant = a_pt
        ENDIF
	WRITE(fdbg,"('Dans tableaux_sun : a_pt,a_courant =',I2,1X,I2)")
     &     a_pt,a_courant
        ALLOCATE(partic(a_courant,nbpamax,nesp_p), stat=ok)
!!! --- FAURE
        IF(ANY(dph(:)%axe(1)(1:1).EQ.'w')) THEN
          partic(a_wx:a_wy,:,:) = 0.
          print*, 'CALDER 2.5D EN MODE *FAURE* !!!'
        ENDIF

      CASE(4)
        a_po = 1
        a_qx = 2 ; a_qy = 3
        a_px = 4 ; a_py = 5

        a_bzp   = 6
        a_pxtld = 7 ; a_pytld  = 8 
        a_gatld = 9 ; a_gatld12 = 10
        a_gaxe  = 11 ; a_gaye    = 12 
        a_paxe  = 13 ; a_paye    = 14 
        a_courant = a_paye
!!! --- MODE friction pour les particules
        IF(friction_tta.EQ.1) THEN
	  a_ttapushs2 = a_courant+1
	  a_courant = a_ttapushs2
        ENDIF
!!! --- SONDE
        IF(esir_ok.EQ.1) THEN
          a_qxm1 = a_courant+1 ; a_qym1 = a_courant+2
          a_courant = a_qym1
        ENDIF
!!! --- FAURE
        IF(ANY(dph(:)%axe(1)(1:1).EQ.'w')) THEN
          a_wx = a_courant+1 ; a_wy = a_courant+2
          a_courant = a_wy
        ENDIF
!!! --- PARTICULES-TEST
        IF(dpt_ok.GT.0) THEN
          a_pt = a_courant+1
          a_courant = a_pt
        ENDIF
	WRITE(fdbg,"('Dans tableaux_sun : a_pt,a_courant =',I2,1X,I2)")
     &     a_pt,a_courant
        ALLOCATE(partic(a_courant,nbpamax,nesp_p), stat=ok)
!!! --- FAURE
        IF(ANY(dph(:)%axe(1)(1:1).EQ.'w')) THEN
          partic(a_wx:a_wy,:,:) = 0.
          print*, 'CALDER 2D EN MODE *FAURE* !!!'
        ENDIF

      CASE(3)
        a_po = 1
        a_qx = 2
        a_px = 3 ; a_py = 4 ; a_pz = 5

        a_bxp   = 6 ; a_byp    = 7 ; a_bzp   = 8
        a_pxtld = 9 ; a_pytld  = 10 ; a_pztld = 11
        a_gatld = 12 ; a_gatld12 = 13
        a_gaxe  = 14 ; a_gaye   = 15 ; a_gaze = 16
        a_paxe  = 17 ; a_paye   = 18 ; a_paze = 19
        a_courant = a_paze
!!! --- MODE friction pour les particules
        IF(friction_tta.EQ.1) THEN
	  a_ttapushs2 = a_courant+1
	  a_courant = a_ttapushs2
        ENDIF
!!! --- SONDE
        IF(esir_ok.EQ.1) THEN
          a_qxm1 = a_courant+1
          a_courant = a_qxm1
        ENDIF
!!! --- PARTICULES-TEST
        IF(dpt_ok.GT.0) THEN
          a_pt = a_courant+1
          a_courant = a_pt
        ENDIF
	WRITE(fdbg,"('Dans tableaux_sun : a_courant =',I2,1X,I2)")
     &     a_pt,a_courant
        ALLOCATE(partic(a_courant,nbpamax,nesp_p), stat=ok)

      CASE(2)
        a_po = 1
        a_qx = 2
        a_px = 3 ; a_py = 4

        a_bzp   = 5
        a_pxtld = 6 ; a_pytld  = 7 
        a_gatld = 8 ; a_gatld12 = 9
        a_gaxe  = 10 ; a_gaye   = 11 
        a_paxe  = 12 ; a_paye   = 13 
        a_courant = a_paye
!!! --- MODE friction pour les particules
        IF(friction_tta.EQ.1) THEN
	  a_ttapushs2 = a_courant+1
	  a_courant = a_ttapushs2
        ENDIF
!!! --- SONDE
        IF(esir_ok.EQ.1) THEN
          a_qxm1 = a_courant+1
          a_courant = a_qxm1
        ENDIF
!!! --- PARTICULES-TEST
        IF(dpt_ok.GT.0) THEN
          a_pt = a_courant+1
          a_courant = a_pt
        ENDIF
	WRITE(fdbg,"('Dans tableaux_sun : a_courant =',I2,1X,I2)")
     &     a_pt,a_courant
        ALLOCATE(partic(a_courant,nbpamax,nesp_p), stat=ok)

      CASE(1)
        a_po = 1
        a_qx = 2
        a_px = 3
	
        a_pxtld = 4 
        a_gatld = 5 ; a_gatld12 = 6
        a_gaxe  = 7 
        a_paxe  = 8 
        a_courant = a_paxe
!!! --- MODE friction pour les particules
        IF(friction_tta.EQ.1) THEN
	  a_ttapushs2 = a_courant+1
	  a_courant = a_ttapushs2
        ENDIF
!!! --- SONDE
        IF(esir_ok.EQ.1) THEN
          a_qxm1 = a_courant+1
          a_courant = a_qxm1
        ENDIF
!!! --- PARTICULES-TEST
        IF(dpt_ok.GT.0) THEN
          a_pt = a_courant+1
          a_courant = a_pt
        ENDIF
	WRITE(fdbg,"('Dans tableaux_sun : a_courant =',I2,1X,I2)")
     &     a_pt,a_courant
        ALLOCATE(partic(a_courant,nbpamax,nesp_p), stat=ok)

      END SELECT       
      
      IF(friction_tta.EQ.1) THEN
        ALLOCATE(cnt_fast(nesp_p))      
      ENDIF
      
! ----- variable fictive utile pour les diags      
      a_ga = a_gatld12

!
! Allocation du tableau de particules et definition des alias
!  pour les iterations en electrostatique
! ----------------------------------------------------------------------
      a_qx_ik   = 0 ; a_qy_ik   = 0 ; a_qz_ik   = 0
      a_dvx_ik   = 0 ; a_dvy_ik   = 0 ; a_dvz_ik   = 0
     
      SELECT CASE(dim_cas)
      CASE(6)
        a_qx_ik = 1 ; a_qy_ik = 2 ; a_qz_ik = 3
        a_dvx_ik = 4 ; a_dvy_ik = 5 ; a_dvz_ik = 6
        a_crt_ik = a_dvz_ik

        ALLOCATE(partic_ik(a_crt_ik,nbpamax,nesp_p), stat=ok)

      CASE(5)
        a_qx_ik = 1 ; a_qy_ik = 2 
        a_dvx_ik = 3 ; a_dvy_ik = 4 ; a_dvz_ik = 5
        a_crt_ik = a_dvz_ik

        ALLOCATE(partic_ik(a_crt_ik,nbpamax,nesp_p), stat=ok)

      CASE(4)
        a_qx_ik = 1 ; a_qy_ik = 2 
        a_dvx_ik = 3 ; a_dvy_ik = 4 
        a_crt_ik = a_dvy_ik

        ALLOCATE(partic_ik(a_crt_ik,nbpamax,nesp_p), stat=ok)

      CASE(3)
        a_qx_ik = 1 
        a_dvx_ik = 2 ; a_dvy_ik = 3 ; a_dvz_ik = 4
        a_crt_ik = a_dvz_ik

        ALLOCATE(partic_ik(a_crt_ik,nbpamax,nesp_p), stat=ok)

      CASE(2)
        a_qx_ik = 1 
        a_dvx_ik = 2 ; a_dvy_ik = 3 
        a_crt_ik = a_dvy_ik

        ALLOCATE(partic_ik(a_crt_ik,nbpamax,nesp_p), stat=ok)

      CASE(1)
        a_qx_ik = 1 
        a_dvx_ik = 2 
        a_crt_ik = a_dvx_ik

        ALLOCATE(partic_ik(a_crt_ik,nbpamax,nesp_p), stat=ok)

      END SELECT       

      
      IF(ok.NE.0) THEN
        WRITE(6,"('Subroutine decl_tabl : erreur ',I2)") ok
        WRITE(6,"('a l''allocation du tableau de particules')")
        itesterr(numproc) = 1
      ENDIF
      ENDIF   ! --- nesp_p.GT.0

! Definition des alias pour le tableau buf_part(:,:)
! Allocation des tableaux de champs
! Allocation des tableaux de champs pour sub_cycling
! ----------------------------------------------------------------------
!      a_ex = 0 ; a_ey = 0 ; a_ez = 0
!      a_bx = 0 ; a_by = 0 ; a_bz = 0
      SELECT CASE(dim_cas)
      CASE(6)
!        a_ex = 1 ; a_ey = 2 ; a_ez = 3
!        a_bx = 4 ; a_by = 5 ; a_bz = 6
        dim_bufp = 6
        ALLOCATE(ex_3(nulmx-l_oset:nulpx+r_oset, 
     &  nulmy-c_oset:nulpy+c_oset,
     &  nulmz-c_oset:nulpz+c_oset  ), stat=ok)
        ALLOCATE(ey_3(nulmx-c_oset:nulpx+c_oset, 
     &  nulmy-l_oset:nulpy+r_oset,
     &  nulmz-c_oset:nulpz+c_oset  ), stat=ok)
        ALLOCATE(ez_3(nulmx-c_oset:nulpx+c_oset, 
     &  nulmy-c_oset:nulpy+c_oset,
     &  nulmz-l_oset:nulpz+r_oset), stat=ok)
        ALLOCATE(bx_3(nulmx-c_oset:nulpx+c_oset ,
     &  nulmy-l_oset:nulpy+r_oset,
     &  nulmz-l_oset:nulpz+r_oset), stat=ok)
        ALLOCATE(by_3(nulmx-l_oset:nulpx+r_oset,
     &  nulmy-c_oset:nulpy+c_oset  ,
     &  nulmz-l_oset:nulpz+r_oset),  stat=ok)
        ALLOCATE(bz_3(nulmx-l_oset:nulpx+r_oset,
     &  nulmy-l_oset:nulpy+r_oset,
     &  nulmz-c_oset:nulpz+c_oset  ), stat=ok)
        ALLOCATE(bx_3p(nulmx-c_oset:nulpx+c_oset ,
     &  nulmy-l_oset:nulpy+r_oset,
     &  nulmz-l_oset:nulpz+r_oset), stat=ok)
        ALLOCATE(by_3p(nulmx-l_oset:nulpx+r_oset,
     &  nulmy-c_oset:nulpy+c_oset  ,
     &  nulmz-l_oset:nulpz+r_oset),  stat=ok)
        ALLOCATE(bz_3p(nulmx-l_oset:nulpx+r_oset,
     &  nulmy-l_oset:nulpy+r_oset,
     &  nulmz-c_oset:nulpz+c_oset  ), stat=ok)
        ex_3(:,:,:) = 0.
        ey_3(:,:,:) = 0.
        ez_3(:,:,:) = 0.
        bx_3(:,:,:) = 0. ; bx_3p(:,:,:) = 0.
        by_3(:,:,:) = 0. ; by_3p(:,:,:) = 0.
        bz_3(:,:,:) = 0. ; bz_3p(:,:,:) = 0.

      CASE(5)
!        a_ex = 1 ; a_ey = 2 ; a_ez = 3
!        a_bx = 4 ; a_by = 5 ; a_bz = 6
        dim_bufp = 6
        ALLOCATE(ex_2(nulmx-l_oset:nulpx+r_oset, 
     &  nulmy-c_oset:nulpy+c_oset ), stat=ok)
        ALLOCATE(ey_2(nulmx-c_oset:nulpx+c_oset, 
     &  nulmy-l_oset:nulpy+r_oset ), stat=ok)
        ALLOCATE(ez_2(nulmx-c_oset:nulpx+c_oset, 
     &  nulmy-c_oset:nulpy+c_oset ), stat=ok)
        ALLOCATE(bx_2(nulmx-c_oset:nulpx+c_oset ,
     &  nulmy-l_oset:nulpy+r_oset ), stat=ok)
        ALLOCATE(by_2(nulmx-l_oset:nulpx+r_oset,
     &  nulmy-c_oset:nulpy+c_oset ),  stat=ok)
        ALLOCATE(bz_2(nulmx-l_oset:nulpx+r_oset,
     &  nulmy-l_oset:nulpy+r_oset ), stat=ok)
        ALLOCATE(bx_2p(nulmx-c_oset:nulpx+c_oset ,
     &  nulmy-l_oset:nulpy+r_oset ), stat=ok)
        ALLOCATE(by_2p(nulmx-l_oset:nulpx+r_oset,
     &  nulmy-c_oset:nulpy+c_oset ),  stat=ok)
        ALLOCATE(bz_2p(nulmx-l_oset:nulpx+r_oset,
     &  nulmy-l_oset:nulpy+r_oset ), stat=ok)    
    
        ex_2(:,:) = 0.
        ey_2(:,:) = 0.
        ez_2(:,:) = 0.
        bx_2(:,:) = 0. ; bx_2p(:,:) = 0.
        by_2(:,:) = 0. ; by_2p(:,:) = 0.
        bz_2(:,:) = 0. ; bz_2p(:,:) = 0.

      CASE(4)
!        a_ex = 1 ; a_ey = 2
!        a_bz = 3
        dim_bufp = 3
        ALLOCATE(ex_2(nulmx-l_oset:nulpx+r_oset, 
     &  nulmy-c_oset:nulpy+c_oset ), stat=ok)
        ALLOCATE(ey_2(nulmx-c_oset:nulpx+c_oset, 
     &  nulmy-l_oset:nulpy+r_oset ), stat=ok)
        ALLOCATE(bz_2(nulmx-l_oset:nulpx+r_oset,
     &  nulmy-l_oset:nulpy+r_oset ), stat=ok)
        ALLOCATE(bz_2p(nulmx-l_oset:nulpx+r_oset,
     &  nulmy-l_oset:nulpy+r_oset ), stat=ok)    
    
        ex_2(:,:) = 0.
        ey_2(:,:) = 0.
        bz_2(:,:) = 0. ; bz_2p(:,:) = 0.

      CASE(3)
!        a_ex = 1 ; a_ey = 2 ; a_ez = 3
!        a_bx = 4 ; a_by = 5 ; a_bz = 6
        dim_bufp = 6
        ALLOCATE(ex_1(nulmx-l_oset:nulpx+r_oset), stat=ok)
        ALLOCATE(ey_1(nulmx-c_oset:nulpx+c_oset), stat=ok)
        ALLOCATE(ez_1(nulmx-c_oset:nulpx+c_oset), stat=ok)
        ALLOCATE(bx_1(nulmx-c_oset:nulpx+c_oset), stat=ok)
        ALLOCATE(by_1(nulmx-l_oset:nulpx+r_oset), stat=ok)
        ALLOCATE(bz_1(nulmx-l_oset:nulpx+r_oset), stat=ok)
        ALLOCATE(bx_1p(nulmx-c_oset:nulpx+c_oset), stat=ok)
        ALLOCATE(by_1p(nulmx-l_oset:nulpx+r_oset), stat=ok)
        ALLOCATE(bz_1p(nulmx-l_oset:nulpx+r_oset), stat=ok)
        ex_1(:) = 0.
        ey_1(:) = 0.
        ez_1(:) = 0.
        bx_1(:) = 0. ; bx_1p(:) = 0.
        by_1(:) = 0. ; by_1p(:) = 0.
        bz_1(:) = 0. ; bz_1p(:) = 0.

      CASE(2)
!        a_ex = 1 ; a_ey = 2
!        a_bz = 3
        dim_bufp = 3
        ALLOCATE(ex_1(nulmx-l_oset:nulpx+r_oset), stat=ok)
        ALLOCATE(ey_1(nulmx-c_oset:nulpx+c_oset), stat=ok)
        ALLOCATE(bz_1(nulmx-l_oset:nulpx+r_oset), stat=ok)
        ALLOCATE(bz_1p(nulmx-l_oset:nulpx+r_oset), stat=ok)
        ex_1(:) = 0.
        ey_1(:) = 0.
        bz_1(:) = 0. ; bz_1p(:) = 0.

      CASE(1)
!        a_ex = 1
        dim_bufp = 1
        ALLOCATE(ex_1(nulmx-l_oset:nulpx+r_oset), stat=ok)
        ex_1(:) = 0.

      END SELECT


! Allocation des tableaux de champs
! pour les iterations
! ----------------------------------------------------------------------
      SELECT CASE(dim_cas)
      CASE(6)
        ALLOCATE(ex3_ik(nulmx-l_oset:nulpx+r_oset, 
     &  nulmy-c_oset:nulpy+c_oset,
     &  nulmz-c_oset:nulpz+c_oset  ), stat=ok)
        ALLOCATE(ey3_ik(nulmx-c_oset:nulpx+c_oset, 
     &  nulmy-l_oset:nulpy+r_oset,
     &  nulmz-c_oset:nulpz+c_oset  ), stat=ok)
        ALLOCATE(ez3_ik(nulmx-c_oset:nulpx+c_oset, 
     &  nulmy-c_oset:nulpy+c_oset,
     &  nulmz-l_oset:nulpz+r_oset), stat=ok)

        ex3_ik(:,:,:) = 0.
        ey3_ik(:,:,:) = 0.
        ez3_ik(:,:,:) = 0.

      CASE(4,5)
!        ALLOCATE(ex2_ik(nulmx-l_oset:nulpx+r_oset, 
!     &  nulmy-c_oset:nulpy+c_oset ), stat=ok)
!        ALLOCATE(ey2_ik(nulmx-c_oset:nulpx+c_oset, 
!     &  nulmy-l_oset:nulpy+r_oset ), stat=ok)
    
        ALLOCATE( ex2_ik(0:n1x,0:n1y) ) 
        ALLOCATE( ey2_ik(0:n1x,0:n1y) )
    
        ex2_ik(:,:) = 0.
        ey2_ik(:,:) = 0.

      CASE(1,2,3)

        ALLOCATE(ex1_ik(nulmx-l_oset:nulpx+r_oset), stat=ok)
        ex1_ik(:) = 0.

      END SELECT


!       allocation du tableau buf_part, 
!       Utile pour le tri periodique des particules
!       Version de tri_part differente decelle utilisee dans Klder
! -----------------------------------
       ALLOCATE(buf_part(dim_bufp,nbpamax), stat=ok)     
       buf_part(:,:) = 0.

      IF(ok.NE.0) THEN
        WRITE(6,"('Subroutine decl_tabl : erreur ',I2)") ok
        WRITE(6,"('a l''allocation des tableaux de champs')")
        itesterr(numproc) = 1
      ENDIF

! Allocation des tableaux de sources
! ----------------------------------------------------------------------
      SELECT CASE(dim_cas)
      CASE(6)
!       ALLOCATE(rh_esp_3(nulmx-c_oset:nulpx+c_oset, 
!     &  nulmy-c_oset:nulpy+c_oset, 
!     &  nulmz-c_oset:nulpz+c_oset,   nesp), stat=ok)
!       ALLOCATE(cx_esp_3(nulmx-l_oset:nulpx+r_oset, 
!     &  nulmy-c_oset:nulpy+c_oset,
!     &  nulmz-c_oset:nulpz+c_oset, nesp), stat=ok)
!       ALLOCATE(cy_esp_3(nulmx-c_oset:nulpx+c_oset, 
!     &  nulmy-l_oset:nulpy+r_oset,
!     &  nulmz-c_oset:nulpz+c_oset, nesp), stat=ok)
!       ALLOCATE(cz_esp_3(nulmx-c_oset:nulpx+c_oset, 
!     &  nulmy-c_oset:nulpy+c_oset,
!     &  nulmz-l_oset:nulpz+r_oset, nesp),  stat=ok)
!       ALLOCATE(rh_3(nulmx-c_oset:nulpx+c_oset, 
!     &  nulmy-c_oset:nulpy+c_oset  ,
!     &  nulmz-c_oset:nulpz+c_oset  ),stat=ok)
!       ALLOCATE(cx_3(nulmx-l_oset:nulpx+r_oset,
!     &  nulmy-c_oset:nulpy+c_oset  ,
!     &  nulmz-c_oset:nulpz+c_oset  ),stat=ok)
!       ALLOCATE(cy_3(nulmx-c_oset:nulpx+c_oset  ,
!     &  nulmy-l_oset:nulpy+r_oset,
!     &  nulmz-c_oset:nulpz+c_oset  ),stat=ok)
!       ALLOCATE(cz_3(nulmx-c_oset:nulpx+c_oset  ,
!     &  nulmy-c_oset:nulpy+c_oset  ,
!     &  nulmz-l_oset:nulpz+r_oset),stat=ok)

      CASE(5)
!       ALLOCATE(rh_esp_2(nulmx-c_oset:nulpx+c_oset, 
!     &  nulmy-c_oset:nulpy+c_oset,   nesp), stat=ok)
!       ALLOCATE(cx_esp_2(nulmx-l_oset:nulpx+r_oset, 
!     &  nulmy-c_oset:nulpy+c_oset, nesp), stat=ok)
!       ALLOCATE(cy_esp_2(nulmx-c_oset:nulpx+c_oset, 
!     &  nulmy-l_oset:nulpy+r_oset, nesp), stat=ok)
!       ALLOCATE(cz_esp_2(nulmx-c_oset:nulpx+c_oset, 
!     &  nulmy-c_oset:nulpy+c_oset, nesp),  stat=ok)
       ALLOCATE(rh_2(nulmx-c_oset:nulpx+c_oset, 
     &  nulmy-c_oset:nulpy+c_oset  ),stat=ok)
       ALLOCATE(cx_2(nulmx-l_oset:nulpx+r_oset,
     &  nulmy-c_oset:nulpy+c_oset  ),stat=ok)
       ALLOCATE(cy_2(nulmx-c_oset:nulpx+c_oset  ,
     &  nulmy-l_oset:nulpy+r_oset  ),stat=ok)
       ALLOCATE(cz_2(nulmx-c_oset:nulpx+c_oset  ,
     &  nulmy-c_oset:nulpy+c_oset  ),stat=ok)

      CASE(4)                  
!         ALLOCATE(rh_esp_2(nulmx-c_oset:nulpx+c_oset, 
!     &    nulmy-c_oset:nulpy+c_oset,   nesp), stat=ok)     
!         ALLOCATE(cx_esp_2(nulmx-l_oset:nulpx+r_oset, 
!     &    nulmy-c_oset:nulpy+c_oset, nesp), stat=ok)
!         ALLOCATE(cy_esp_2(nulmx-c_oset:nulpx+c_oset, 
!     &    nulmy-l_oset:nulpy+r_oset, nesp), stat=ok)     
!         ALLOCATE(rh_2(nulmx-c_oset:nulpx+c_oset, 
!     &    nulmy-c_oset:nulpy+c_oset  ),stat=ok)
!         ALLOCATE(rh_2_prec(nulmx-c_oset:nulpx+c_oset, 
!     &    nulmy-c_oset:nulpy+c_oset  ),stat=ok)     
!         ALLOCATE(cx_2(nulmx-l_oset:nulpx+r_oset,
!     &    nulmy-c_oset:nulpy+c_oset  ),stat=ok)
!         ALLOCATE(cy_2(nulmx-c_oset:nulpx+c_oset  ,
!     &    nulmy-l_oset:nulpy+r_oset  ),stat=ok)
       ALLOCATE(rh_2(nulmx-c_oset:nulpx+c_oset, 
     &  nulmy-c_oset:nulpy+c_oset  ),stat=ok)
       ALLOCATE(cx_2(nulmx-l_oset:nulpx+r_oset,
     &  nulmy-c_oset:nulpy+c_oset  ),stat=ok)
       ALLOCATE(cy_2(nulmx-c_oset:nulpx+c_oset  ,
     &  nulmy-l_oset:nulpy+r_oset  ),stat=ok)
       
      CASE(3)
!       ALLOCATE(rh_esp_1(nulmx-c_oset:nulpx+c_oset, 
!     &  nesp), stat=ok)
!       ALLOCATE(cx_esp_1(nulmx-l_oset:nulpx+r_oset,
!     &  nesp), stat=ok)
!       ALLOCATE(cy_esp_1(nulmx-c_oset:nulpx+c_oset, 
!     &  nesp), stat=ok)
!       ALLOCATE(cz_esp_1(nulmx-c_oset:nulpx+c_oset, 
!     &  nesp),  stat=ok)
!       ALLOCATE(rh_1(nulmx-c_oset:nulpx+c_oset),stat=ok)
!       ALLOCATE(cx_1(nulmx-l_oset:nulpx+r_oset),stat=ok)
!       ALLOCATE(cy_1(nulmx-c_oset:nulpx+c_oset),stat=ok)
!       ALLOCATE(cz_1(nulmx-c_oset:nulpx+c_oset),stat=ok)

      CASE(2)
!       ALLOCATE(rh_esp_1(nulmx-c_oset:nulpx+c_oset, 
!     &  nesp), stat=ok)
!       ALLOCATE(cx_esp_1(nulmx-l_oset:nulpx+r_oset,
!     &  nesp), stat=ok)
!       ALLOCATE(cy_esp_1(nulmx-c_oset:nulpx+c_oset, 
!     &  nesp), stat=ok)
!       ALLOCATE(rh_1(nulmx-c_oset:nulpx+c_oset),stat=ok)
!       ALLOCATE(cx_1(nulmx-l_oset:nulpx+r_oset),stat=ok)
!       ALLOCATE(cy_1(nulmx-c_oset:nulpx+c_oset),stat=ok)

      CASE(1)
!       ALLOCATE(rh_esp_1(nulmx-c_oset:nulpx+c_oset, 
!     &  nesp), stat=ok)
!       ALLOCATE(rh_1(nulmx-c_oset:nulpx+c_oset),stat=ok)

      END SELECT
      IF(ok.NE.0) THEN
        WRITE(6,"('Subroutine decl_tabl : erreur ',I2)") ok
        WRITE(6,"('a l''allocation des tableaux de sources')")
        itesterr(numproc) = 1
      ENDIF

! Allocation des tableaux pour les collisions
! ----------------------------------------------------------------------
      IF(col_nb.GE.1) 
     & ALLOCATE(ldebye2((nulpx-nulmx)*(nulpy-nulmy)*(nulpz-nulmz)))

! Allocation des tableaux de diags et definition des alias
! ----------------------------------------------------------------------
! --- poids et energies cinetiques
      ALLOCATE(d_po(nesp), d_ki(nesp))
      DO iesp=1,nesp
        d_po(iesp) = iesp
        d_ki(iesp) = iesp + nesp
      ENDDO      
      d1d_nb = d_ki(nesp)     
! --- Impulsion en x,y,z
      IF (a_px.NE.0) THEN
        ALLOCATE(d_px(nesp))
        DO iesp=1,nesp
          d_px(iesp) = d1d_nb + iesp
        ENDDO  
	d1d_nb = d_px(nesp)
      ENDIF      
      IF (a_py.NE.0) THEN
        ALLOCATE(d_py(nesp))
        DO iesp=1,nesp
          d_py(iesp) = d1d_nb + iesp
        ENDDO  
	d1d_nb = d_py(nesp)
      ENDIF        
      IF (a_pz.NE.0) THEN
        ALLOCATE(d_pz(nesp))
        DO iesp=1,nesp
          d_pz(iesp) = d1d_nb + iesp
        ENDDO  
	d1d_nb = d_pz(nesp)
      ENDIF      
! --- Flux d'impulsion en x,y,z      
      IF (a_px.NE.0) THEN
        ALLOCATE(d_fpx(nesp))
        DO iesp=1,nesp
          d_fpx(iesp) = d1d_nb + iesp
        ENDDO  
	d1d_nb = d_fpx(nesp)
      ENDIF      
      IF (a_py.NE.0) THEN
        ALLOCATE(d_fpy(nesp))
        DO iesp=1,nesp
          d_fpy(iesp) = d1d_nb + iesp
        ENDDO  
	d1d_nb = d_fpy(nesp)
      ENDIF        
      IF (a_pz.NE.0) THEN
        ALLOCATE(d_fpz(nesp))
        DO iesp=1,nesp
          d_fpz(iesp) = d1d_nb + iesp
        ENDDO  
	d1d_nb = d_fpz(nesp)
      ENDIF    
! --- poids et energies cinetiques perdus par absorption
      ALLOCATE(d_pof(nesp), d_kif(nesp))
      d_pof(:) = 0 ; d_kif(:) = 0
      IF( (ANY(ibndp(:,:).EQ.-2)).OR.(ANY(ibndp(:,:).EQ.-5)) ) THEN
!     &                           .OR.(gliss_ok.EQ.1) ) THEN
        DO iesp=1,nesp
          d_pof(iesp) = d1d_nb + iesp
          d_kif(iesp) = d1d_nb + iesp + nesp
        ENDDO
        d1d_nb = d_kif(nesp)
      ENDIF
! --- energies cinetiques perdues par reinjection
      ALLOCATE(d_kir(nesp))
      d_kir(:) = 0
      IF( (ANY(ibndp(:,:).EQ.-4)).OR.(ANY(ibndp(:,:).EQ.-5)) ) THEN
        DO iesp=1,nesp
          d_kir(iesp) = d1d_nb + iesp
        ENDDO
        d1d_nb = d_kir(nesp)
      ENDIF
! --- energies electromagnetiques
      IF(d_ex.NE.0) THEN ; d1d_nb = d1d_nb + 1 ; d_ex = d1d_nb ; ENDIF
      IF(d_ey.NE.0) THEN ; d1d_nb = d1d_nb + 1 ; d_ey = d1d_nb ; ENDIF
      IF(d_ez.NE.0) THEN ; d1d_nb = d1d_nb + 1 ; d_ez = d1d_nb ; ENDIF
      IF(d_bx.NE.0) THEN ; d1d_nb = d1d_nb + 1 ; d_bx = d1d_nb ; ENDIF
      IF(d_by.NE.0) THEN ; d1d_nb = d1d_nb + 1 ; d_by = d1d_nb ; ENDIF
      IF(d_bz.NE.0) THEN ; d1d_nb = d1d_nb + 1 ; d_bz = d1d_nb ; ENDIF

! --- champs locaux
      IF(clo_nb.NE.0) THEN
        d1d_nb = d1d_nb + 1 ; d_clo = d1d_nb
        d1d_nb = d1d_nb + clo_nb - 1
      ENDIF
! --- flux de Poynting source
      d1d_nb = d1d_nb + 1 ; d_psr = d1d_nb
! --- flux de Poynting aux bords
      d_pmx=0 ; d_ppx=0 ; d_pmy=0 ; d_ppy=0 ; d_pmz=0 ; d_ppz=0
      SELECT CASE(dim_cas)
      CASE(6)
        d_pmx = d1d_nb+1 ; d_ppx = d1d_nb+2
        d_pmy = d1d_nb+3 ; d_ppy = d1d_nb+4
        d_pmz = d1d_nb+5 ; d_ppz = d1d_nb+6
        d1d_nb = d1d_nb+6
        d_ptg  = d1d_nb
      CASE(5,4)
        d_pmx = d1d_nb+1 ; d_ppx = d1d_nb+2
        d_pmy = d1d_nb+3 ; d_ppy = d1d_nb+4
        d1d_nb = d1d_nb+4
        d_ptg  = d1d_nb
      CASE(3,2)
        d_pmx = d1d_nb+1 ; d_ppx = d1d_nb+2
        d1d_nb = d1d_nb+2
        d_ptg  = d1d_nb
      CASE DEFAULT
        d_ptg  = d1d_nb
      END SELECT
! --- ecarts a Poisson et Gauss
      IF(pois_dia) THEN
        d1d_nb = d1d_nb + 1 ; d_psn = d1d_nb
        IF(dim_cas.GE.5) THEN
          d1d_nb = d1d_nb + 1 ; d_gau = d1d_nb
        ELSE
          d_gau = d_psn
        ENDIF
      ENDIF
! --- champs max
      IF(cmx_nb.NE.0) THEN
        d1d_nb = d1d_nb + 1 ; d_cmx = d1d_nb
        d1d_nb = d1d_nb + cmx_nb - 1
      ENDIF
      ALLOCATE(d1d_loc(d1d_nb), d1d_som(d1d_nb), stat=ok)
      IF(ok.NE.0) THEN
        WRITE(6,"('Subroutine decl_tabl : erreur ',I2)") ok
        WRITE(6,"('a l''allocation des tableaux de diagnostics')")
        itesterr(numproc) = 1
      ENDIF
      d1d_loc(:) = 0. ; d1d_som(:) = 0.

      RETURN

!     _______________________________
      END SUBROUTINE decl_tabl
      
      
      
      SUBROUTINE verif_tabl
! ======================================================================
! Verification de l'etat des tableaux alloues dynamiquement
! ======================================================================

      USE champs
      USE particules
      USE particules_klder
      USE diagnostics_klder
      USE erreurs
      
      
      IMPLICIT NONE


! Etat du tableau de particules
! ----------------------------------------------------------------------
      IF(ALLOCATED(partic)) THEN
        WRITE(fdbg,"('Le tableau de particules est alloue, et son ',
     &               'profil est ',I2,1X,I7,1X,I2)") SHAPE(partic)
      ELSE
        WRITE(fdbg,"('Le tableau de particules n''est pas alloue')") 
      ENDIF

! Etat des tableaux de champs
! ----------------------------------------------------------------------
      WRITE(fdbg,"('Tableau du champ Ex : ')",ADVANCE='NO') 
      IF(ALLOCATED(ex_3)) THEN
        WRITE(fdbg,"('alloue 3d, profil = ',3(I4,1X))") SHAPE(ex_3)
      ELSEIF(ALLOCATED(ex_2)) THEN
        WRITE(fdbg,"('alloue 2d, profil = ',2(I4,1X))") SHAPE(ex_2)
      ELSEIF(ALLOCATED(ex_1)) THEN
        WRITE(fdbg,"('alloue 1d, profil = ',1(I4,1X))") SHAPE(ex_1)
      ELSE
        WRITE(fdbg,"('pas alloue')")
      ENDIF

      WRITE(fdbg,"('Tableau du champ Ey : ')",ADVANCE='NO') 
      IF(ALLOCATED(ey_3)) THEN
        WRITE(fdbg,"('alloue 3d, profil = ',3(I4,1X))") SHAPE(ey_3)
      ELSEIF(ALLOCATED(ey_2)) THEN
        WRITE(fdbg,"('alloue 2d, profil = ',2(I4,1X))") SHAPE(ey_2)
      ELSEIF(ALLOCATED(ey_1)) THEN
        WRITE(fdbg,"('alloue 1d, profil = ',1(I4,1X))") SHAPE(ey_1)
      ELSE
        WRITE(fdbg,"('pas alloue')")
      ENDIF

      WRITE(fdbg,"('Tableau du champ Ez : ')",ADVANCE='NO') 
      IF(ALLOCATED(ez_3)) THEN
        WRITE(fdbg,"('alloue 3d, profil = ',3(I4,1X))") SHAPE(ez_3)
      ELSEIF(ALLOCATED(ez_2)) THEN
        WRITE(fdbg,"('alloue 2d, profil = ',2(I4,1X))") SHAPE(ez_2)
      ELSEIF(ALLOCATED(ez_1)) THEN
        WRITE(fdbg,"('alloue 1d, profil = ',1(I4,1X))") SHAPE(ez_1)
      ELSE
        WRITE(fdbg,"('pas alloue')")
      ENDIF

      WRITE(fdbg,"('Tableau du champ Bx : ')",ADVANCE='NO') 
      IF(ALLOCATED(bx_3)) THEN
        WRITE(fdbg,"('alloue 3d, profil = ',3(I4,1X))") SHAPE(bx_3)
      ELSEIF(ALLOCATED(bx_2)) THEN
        WRITE(fdbg,"('alloue 2d, profil = ',2(I4,1X))") SHAPE(bx_2)
      ELSEIF(ALLOCATED(bx_1)) THEN
        WRITE(fdbg,"('alloue 1d, profil = ',1(I4,1X))") SHAPE(bx_1)
      ELSE
        WRITE(fdbg,"('pas alloue')")
      ENDIF

      WRITE(fdbg,"('Tableau du champ By : ')",ADVANCE='NO') 
      IF(ALLOCATED(by_3)) THEN
        WRITE(fdbg,"('alloue 3d, profil = ',3(I4,1X))") SHAPE(by_3)
      ELSEIF(ALLOCATED(by_2)) THEN
        WRITE(fdbg,"('alloue 2d, profil = ',2(I4,1X))") SHAPE(by_2)
      ELSEIF(ALLOCATED(by_1)) THEN
        WRITE(fdbg,"('alloue 1d, profil = ',1(I4,1X))") SHAPE(by_1)
      ELSE
        WRITE(fdbg,"('pas alloue')")
      ENDIF

      WRITE(fdbg,"('Tableau du champ Bz : ')",ADVANCE='NO') 
      IF(ALLOCATED(bz_3)) THEN
        WRITE(fdbg,"('alloue 3d, profil = ',3(I4,1X))") SHAPE(bz_3)
      ELSEIF(ALLOCATED(bz_2)) THEN
        WRITE(fdbg,"('alloue 2d, profil = ',2(I4,1X))") SHAPE(bz_2)
      ELSEIF(ALLOCATED(bz_1)) THEN
        WRITE(fdbg,"('alloue 1d, profil = ',1(I4,1X))") SHAPE(bz_1)
      ELSE
        WRITE(fdbg,"('pas alloue')")
      ENDIF


! Etat des tableaux de sources
! ----------------------------------------------------------------------
!      WRITE(fdbg,"('Tableau des densites Rh_esp : ')",ADVANCE='NO') 
!      IF(ALLOCATED(rh_esp_3)) THEN
!        WRITE(fdbg,"('alloue 3d, profil = ',4(I4,1X))") SHAPE(rh_esp_3)
!      ELSEIF(ALLOCATED(rh_esp_2)) THEN
!        WRITE(fdbg,"('alloue 2d, profil = ',3(I4,1X))") SHAPE(rh_esp_2)
!      ELSEIF(ALLOCATED(rh_esp_1)) THEN
!        WRITE(fdbg,"('alloue 1d, profil = ',2(I4,1X))") SHAPE(rh_esp_1)
!      ELSE
!        WRITE(fdbg,"('pas alloue')")
!      ENDIF

      WRITE(fdbg,"('Tableau de la densite Rh    : ')",ADVANCE='NO') 
      IF(ALLOCATED(rh_3)) THEN
        WRITE(fdbg,"('alloue 3d, profil = ',3(I4,1X))") SHAPE(rh_3)
      ELSEIF(ALLOCATED(rh_2)) THEN
        WRITE(fdbg,"('alloue 2d, profil = ',2(I4,1X))") SHAPE(rh_2)
      ELSEIF(ALLOCATED(rh_1)) THEN
        WRITE(fdbg,"('alloue 1d, profil = ',1(I4,1X))") SHAPE(rh_1)
      ELSE
        WRITE(fdbg,"('pas alloue')")
      ENDIF

!      WRITE(fdbg,"('Tableau des courants Cx_esp : ')",ADVANCE='NO') 
!      IF(ALLOCATED(cx_esp_3)) THEN
!        WRITE(fdbg,"('alloue 3d, profil = ',4(I4,1X))") SHAPE(cx_esp_3)
!      ELSEIF(ALLOCATED(cx_esp_2)) THEN
!        WRITE(fdbg,"('alloue 2d, profil = ',3(I4,1X))") SHAPE(cx_esp_2)
!      ELSEIF(ALLOCATED(cx_esp_1)) THEN
!        WRITE(fdbg,"('alloue 1d, profil = ',2(I4,1X))") SHAPE(cx_esp_1)
!      ELSE
!        WRITE(fdbg,"('pas alloue')")
!      ENDIF

      WRITE(fdbg,"('Tableau du courant Cx       : ')",ADVANCE='NO') 
      WRITE(fdbg,"('non teste pour cause de bug compilateur')") 
      IF(ALLOCATED(cx_3)) THEN
        WRITE(fdbg,"('alloue 3d, profil = ',3(I4,1X))") SHAPE(cx_3)
      ELSEIF(ALLOCATED(cx_2)) THEN
        WRITE(fdbg,"('alloue 2d, profil = ',2(I4,1X))") SHAPE(cx_2)
      ELSEIF(ALLOCATED(cx_1)) THEN
        WRITE(fdbg,"('alloue 1d, profil = ',1(I4,1X))") SHAPE(cx_1)
      ELSE
        WRITE(fdbg,"('pas alloue')")
      ENDIF

!      WRITE(fdbg,"('Tableau des courants Cy_esp : ')",ADVANCE='NO') 
!      IF(ALLOCATED(cy_esp_3)) THEN
!        WRITE(fdbg,"('alloue 3d, profil = ',4(I4,1X))") SHAPE(cy_esp_3)
!      ELSEIF(ALLOCATED(cy_esp_2)) THEN
!        WRITE(fdbg,"('alloue 2d, profil = ',3(I4,1X))") SHAPE(cy_esp_2)
!      ELSEIF(ALLOCATED(cy_esp_1)) THEN
!        WRITE(fdbg,"('alloue 1d, profil = ',2(I4,1X))") SHAPE(cy_esp_1)
!      ELSE
!        WRITE(fdbg,"('pas alloue')")
!      ENDIF

      WRITE(fdbg,"('Tableau du courant Cy       : ')",ADVANCE='NO') 
      WRITE(fdbg,"('non teste pour cause de bug compilateur')") 
      IF(ALLOCATED(cy_3)) THEN
        WRITE(fdbg,"('alloue 3d, profil = ',3(I4,1X))") SHAPE(cy_3)
      ELSEIF(ALLOCATED(cy_2)) THEN
        WRITE(fdbg,"('alloue 2d, profil = ',2(I4,1X))") SHAPE(cy_2)
      ELSEIF(ALLOCATED(cy_1)) THEN
        WRITE(fdbg,"('alloue 1d, profil = ',1(I4,1X))") SHAPE(cy_1)
      ELSE
        WRITE(fdbg,"('pas alloue')")
      ENDIF

!      WRITE(fdbg,"('Tableau des courants Cz_esp : ')",ADVANCE='NO') 
!      IF(ALLOCATED(cz_esp_3)) THEN
!        WRITE(fdbg,"('alloue 3d, profil = ',4(I4,1X))") SHAPE(cz_esp_3)
!      ELSEIF(ALLOCATED(cz_esp_2)) THEN
!        WRITE(fdbg,"('alloue 2d, profil = ',3(I4,1X))") SHAPE(cz_esp_2)
!      ELSEIF(ALLOCATED(cz_esp_1)) THEN
!        WRITE(fdbg,"('alloue 1d, profil = ',2(I4,1X))") SHAPE(cz_esp_1)
!      ELSE
!        WRITE(fdbg,"('pas alloue')")
!      ENDIF

      WRITE(fdbg,"('Tableau du courant Cz       : ')",ADVANCE='NO') 
      WRITE(fdbg,"('non teste pour cause de bug compilateur')") 
      IF(ALLOCATED(cz_3)) THEN
        WRITE(fdbg,"('alloue 3d, profil = ',3(I4,1X))") SHAPE(cz_3)
      ELSEIF(ALLOCATED(cz_2)) THEN
        WRITE(fdbg,"('alloue 2d, profil = ',2(I4,1X))") SHAPE(cz_2)
      ELSEIF(ALLOCATED(cz_1)) THEN
        WRITE(fdbg,"('alloue 1d, profil = ',1(I4,1X))") SHAPE(cz_1)
      ELSE
        WRITE(fdbg,"('pas alloue')")
      ENDIF

! Etat des tableaux de diags
! ----------------------------------------------------------------------
      IF(ALLOCATED(d1d_loc).AND.ALLOCATED(d1d_som)) THEN
        WRITE(fdbg,"('Les tableaux de diagnostics 1d sont alloues,')") 
        WRITE(fdbg,"('et leur profil est ',(I4,1X))") SHAPE(d1d_loc)
      ELSE
        WRITE(fdbg,"('Les tableaux de diags 1d ne sont pas alloues')")
      ENDIF
      WRITE(fdbg,"(' ')") 

      RETURN
!     _________________________
      END SUBROUTINE verif_tabl
      
      
      
      
