! ======================================================================
! pousseurs.f
! ----------
!
!
!     SUBROUTINE push_rel
!     SUBROUTINE prpush_rel
!
!     SUBROUTINE push_rel_ttafix
!     SUBROUTINE prpush_rel_ttafix
!
!     SUBROUTINE push_rel_thetad
!     SUBROUTINE prpush_rel_thetad
!
!     SUBROUTINE push
!     SUBROUTINE prpush
!
!
! ======================================================================




       SUBROUTINE prpush_rel(iesp)
! ======================================================================
!
!
!
!
!
! ======================================================================

       USE domaines
       USE domaines_klder
       USE particules
       USE particules_klder
       USE champs
       USE collisions
       USE temps
       USE erreurs

       IMPLICIT NONE


       INTEGER    ::   i, j
       INTEGER    ::   ip

!       REAL(kind=kr)     ::  xint, yint

       REAL(kind=kr)   ::  ex1, ey1, ez1

       INTEGER(KIND=kr), DIMENSION(1:ordre_interp+1)  :: ipx, ipy
       REAL(KIND=kr), DIMENSION(1:ordre_interp+1)     :: ponpx, ponpy

       REAL(kind=kr), DIMENSION(1:3,1:3)     ::
     &  rotmat, identity

       REAL(kind=kr), DIMENSION(1:3,1:3)     ::
     &  Umatstar, chimat, zetamat, Mrelat

       REAL(kind=kr), DIMENSION(1:3)     ::
     &  vtempe, gave, utild

       REAL(kind=kr)   ::  vxe, vye, vze
       REAL(kind=kr)   ::  rqm, aux_rh
       REAL(kind=kr)   ::  dt_eff

       INTEGER, INTENT(IN)     ::   iesp

       REAL(kind=kr)   :: tbx, tby, tbz, teta2

! ---- matrices pour diagnostics tenseurs relativistes
       REAL(kind=kr), DIMENSION(1:3,1:3)     ::
     &   chir1, chir2, zetar1


       identity(1,1)=1. ; identity(1,2)=0. ; identity(1,3)=0.
       identity(2,1)=0. ; identity(2,2)=1. ; identity(2,3)=0.
       identity(3,1)=0. ; identity(3,2)=0. ; identity(3,3)=1.

! --- On remet � z�ro les courants rj[xyz]e sur la grille
       cjx_esp_2(:,:,iesp) = 0.
       cjy_esp_2(:,:,iesp) = 0.
       cjz_esp_2(:,:,iesp) = 0.
! --- On remet � z�ro les courants rj[xyz]e sur la grille
       rho_esp_2(:,:,iesp) = 0.
       dgx_esp_2(:,:,iesp) = 0.
       dgy_esp_2(:,:,iesp) = 0.
       dgz_esp_2(:,:,iesp) = 0.

       IF(max_cycles.GT.1) THEN
         xxx_esp(:,:,iesp)=0.  
         xxy_esp(:,:,iesp)=0.  
         xxz_esp(:,:,iesp)=0.
         xyx_esp(:,:,iesp)=0.  
         xyy_esp(:,:,iesp)=0.  
         xyz_esp(:,:,iesp)=0.
         xzx_esp(:,:,iesp)=0.  
         xzy_esp(:,:,iesp)=0.  
         xzz_esp(:,:,iesp)=0.
         
         zxx_esp(:,:,iesp)=0.  
         zxy_esp(:,:,iesp)=0.  
         zxz_esp(:,:,iesp)=0.
         zyx_esp(:,:,iesp)=0.  
         zyy_esp(:,:,iesp)=0.  
         zyz_esp(:,:,iesp)=0.
         zzx_esp(:,:,iesp)=0.  
         zzy_esp(:,:,iesp)=0.  
         zzz_esp(:,:,iesp)=0.            
       ENDIF

       rqm = charge(iesp)/masse(iesp)
       dt_eff = dt*nbscycles(iesp)
       WRITE(fmsgo,*) 'dt_eff : ', dt_eff

! ----- Calcul du gradient du champ electrique a l instant (n*dt)
       IF(simplified.EQ.2 .AND. iesp.EQ.1) THEN
         CALL calcgradE
       ENDIF


       cnt_fast(iesp) = 0.

       IF(col_test.EQ.0) THEN

 !     prepush initial
 !     ----------------------
       DO ip=1,nbpa(iesp)

! ----- determination de gammatild(t0) de la particule n
        vtempe(1) = partic(a_px,ip,iesp) +
     &     0.25*dt_eff*partic(a_gaxe,ip,iesp)
        vtempe(2) = partic(a_py,ip,iesp) +
     &     0.25*dt_eff*partic(a_gaye,ip,iesp)
        vtempe(3) = partic(a_pz,ip,iesp) +
     &     0.25*dt_eff*partic(a_gaze,ip,iesp)

        partic(a_gatld,ip,iesp)= sqrt(1 + vtempe(1)**2
     &     + vtempe(2)**2 + vtempe(3)**2)

        SELECT CASE(ordre_interp)
        CASE(1)
         CALL ponder_ordre1(ip, iesp, ponpx, ponpy, ipx, ipy)

        CASE(2)
         CALL ponder_ordre2(ip, iesp, ponpx, ponpy, ipx, ipy)

        CASE(3)
         CALL ponder_ordre3(ip, iesp, ponpx, ponpy, ipx, ipy)

        CASE(4)
         CALL ponder_ordre4(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE DEFAULT
         WRITE(fmsgo,*) 'pousseurs.f : mauvaise valeur ordre_interp'
       END SELECT

         CALL champa_2d_exyz1(ex1, ey1, ez1,
     &                ponpx, ponpy, ipx, ipy)
         CALL champa_2dx3dv_bxyz(ip, iesp,
     &                ponpx, ponpy, ipx, ipy)


 !
 !     initialisation de gaxe,gaye,gaze
 !
        partic(a_gaxe,ip,iesp) = rqm*partic(a_ttapushs2,ip,iesp)*ex1
     &    + (1-partic(a_ttapushs2,ip,iesp))*partic(a_paxe,ip,iesp)
        partic(a_gaye,ip,iesp) = rqm*partic(a_ttapushs2,ip,iesp)*ey1
     &    + (1-partic(a_ttapushs2,ip,iesp))*partic(a_paye,ip,iesp)
        partic(a_gaze,ip,iesp) = rqm*partic(a_ttapushs2,ip,iesp)*ez1
     &    + (1-partic(a_ttapushs2,ip,iesp))*partic(a_paze,ip,iesp)


! ----- variables matrice de rotation
       tbx = 0.5*rqm*dt_eff*partic(a_bxp,ip,iesp)
     &    /partic(a_gatld,ip,iesp)
       tby = 0.5*rqm*dt_eff*partic(a_byp,ip,iesp)
     &    /partic(a_gatld,ip,iesp)
       tbz = 0.5*rqm*dt_eff*partic(a_bzp,ip,iesp)
     &    /partic(a_gatld,ip,iesp)

       teta2 = tbx**2 + tby**2 + tbz**2

       rotmat(1,1) = 1-teta2+2*tbx**2
       rotmat(1,2) = 2*tbz + 2*tbx*tby
       rotmat(1,3) = -2*tby +2*tbx*tbz

       rotmat(2,1) = 2*tbx*tby - 2*tbz
       rotmat(2,2) = 1-teta2 + 2*tby**2
       rotmat(2,3) = 2*tby*tbz + 2*tbx

       rotmat(3,1) = 2*tby + 2*tbx*tbz
       rotmat(3,2) = 2*tby*tbz - 2*tbx
       rotmat(3,3) = 1-teta2 + 2*tbz**2

       rotmat(:,:) = 1/(1+teta2) * rotmat(:,:)

! ---- determination de utild(n+1/2)
       vtempe(1)= partic(a_px,ip,iesp)
     &    + 0.25*dt_eff*partic(a_gaxe,ip,iesp)
       vtempe(2)= partic(a_py,ip,iesp)
     &    + 0.25*dt_eff*partic(a_gaye,ip,iesp)
       vtempe(3)= partic(a_pz,ip,iesp)
     &    + 0.25*dt_eff*partic(a_gaze,ip,iesp)

       gave(1)=partic(a_gaxe,ip,iesp)
       gave(2)=partic(a_gaye,ip,iesp)
       gave(3)=partic(a_gaze,ip,iesp)

       utild(1:3) = matmul(rotmat,vtempe) + 0.25*dt_eff*gave(1:3)

! ---- definition de gammatild(n+1/2)
       partic(a_gatld12,ip,iesp) =
     &   SQRT(1+utild(1)**2+utild(2)**2+utild(3)**2)


! ---- Promotion en particule rapide, dont le mouvement n'est pas amorti
!      Crit�re arbitraire : (\gamma -1)> Efast
!      Avec E_fast de l'ordre de quelques m_e c^2
!      rappel :   a_gatld12  equiv a_ga
!
       IF(partic(a_qx,ip,iesp).LE.x_damp1 .OR.
     &    partic(a_qx,ip,iesp).GT.x_damp2 .OR.
     &    (partic(a_ga,ip,iesp)-1).GE.Efast) THEN
         partic(a_ttapushs2,ip,iesp)= 0.5*thetad   	   
         cnt_fast(iesp)= cnt_fast(iesp)+1
       ELSE
         partic(a_ttapushs2,ip,iesp)= ttapushs2
       ENDIF


! ---- determination de vtild(n+1/2)
       partic(a_pxtld,ip,iesp)=utild(1)
       partic(a_pytld,ip,iesp)=utild(2)
       partic(a_pztld,ip,iesp)=utild(3)

       vxe = partic(a_pxtld,ip,iesp)/partic(a_gatld12,ip,iesp)
       vye = partic(a_pytld,ip,iesp)/partic(a_gatld12,ip,iesp)
       vze = partic(a_pztld,ip,iesp)/partic(a_gatld12,ip,iesp)

!
!      Matrice Mrelat pour le calcul de chi et zeta
!     ---------------------------------------------
       Mrelat(1,1) = partic(a_pxtld,ip,iesp)**2
       Mrelat(1,2) = partic(a_pxtld,ip,iesp)*partic(a_pytld,ip,iesp)
       Mrelat(1,3) = partic(a_pxtld,ip,iesp)*partic(a_pztld,ip,iesp)

       Mrelat(2,1) = partic(a_pytld,ip,iesp)*partic(a_pxtld,ip,iesp)
       Mrelat(2,2) = partic(a_pytld,ip,iesp)**2
       Mrelat(2,3) = partic(a_pytld,ip,iesp)*partic(a_pztld,ip,iesp)

       Mrelat(3,1) = partic(a_pztld,ip,iesp)*partic(a_pxtld,ip,iesp)
       Mrelat(3,2) = partic(a_pztld,ip,iesp)*partic(a_pytld,ip,iesp)
       Mrelat(3,3) = partic(a_pztld,ip,iesp)**2

       Mrelat(:,:) = Mrelat(:,:)/partic(a_gatld12,ip,iesp)**2

       Mrelat(:,:) = (1/partic(a_gatld12,ip,iesp))*(identity - Mrelat)

       aux_rh = partic(a_po,ip,iesp)

       CALL projpa_2dx3dv_rjxyz(iesp, ponpx, ponpy,
     &              ipx, ipy, vxe, vye, vze, aux_rh)

 !
 !     calcul de xtild(n+1)
 !     -------------------------
       partic(a_qx,ip,iesp)=partic(a_qx,ip,iesp)+dt_eff*vxe
       partic(a_qy,ip,iesp)=partic(a_qy,ip,iesp)+dt_eff*vye

       IF(partic(a_qy,ip,iesp).GE.bord(4,numproc)) THEN
         partic(a_qy,ip,iesp) =
     &        partic(a_qy,ip,iesp) -ylong
       ELSEIF(partic(a_qy,ip,iesp).LE.bord(3,numproc)) THEN
         partic(a_qy,ip,iesp) =
     &        partic(a_qy,ip,iesp) +ylong
       ENDIF

 !
 !     calcul de rhoe en (x n+1) et j(x n+1,v n+1/2)
 !     ---------------------------------------------
       SELECT CASE(ordre_interp)
         CASE(1)
         CALL ponder_ordre1(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(2)
         CALL ponder_ordre2(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(3)
         CALL ponder_ordre3(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(4)
         CALL ponder_ordre4(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE DEFAULT
         WRITE(fmsgo,*) 'pousseurs.f : ordre_interp incorrecte'
       END SELECT

       CALL projpa_2dx3dv_dgxyz(iesp, ponpx, ponpy,
     &              ipx, ipy, vxe, vye, vze, aux_rh)

       CALL champa_2d_exyz1(ex1, ey1, ez1,
     &                ponpx, ponpy, ipx, ipy)
       CALL champa_2dx3dv_bxyz(ip, iesp,
     &                ponpx, ponpy, ipx, ipy)


! ----- variables matrice de rotation
       tbx = 0.5*rqm*dt_eff*partic(a_bxp,ip,iesp)
     &    /partic(a_gatld,ip,iesp)
       tby = 0.5*rqm*dt_eff*partic(a_byp,ip,iesp)
     &    /partic(a_gatld,ip,iesp)
       tbz = 0.5*rqm*dt_eff*partic(a_bzp,ip,iesp)
     &    /partic(a_gatld,ip,iesp)

       teta2 = tbx**2 + tby**2 + tbz**2

       rotmat(1,1) = 1-teta2+2*tbx**2
       rotmat(1,2) = 2*tbz + 2*tbx*tby
       rotmat(1,3) = -2*tby +2*tbx*tbz

       rotmat(2,1) = 2*tbx*tby - 2*tbz
       rotmat(2,2) = 1-teta2 + 2*tby**2
       rotmat(2,3) = 2*tby*tbz + 2*tbx

       rotmat(3,1) = 2*tby + 2*tbx*tbz
       rotmat(3,2) = 2*tby*tbz - 2*tbx
       rotmat(3,3) = 1-teta2 + 2*tbz**2

       rotmat(:,:) = 1/(1+teta2) * rotmat(:,:)

!
!     definition des coefficients de la matrice Umatstar pour electron
! ----------------------------------------------------------
       IF(simplified.EQ.0) THEN
       Umatstar(1,1) = ((partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))
     &   *partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*
     &  (partic(a_px,ip,iesp)+0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umatstar(1,2) = ((partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))
     &   *partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*
     &  (partic(a_py,ip,iesp)+0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umatstar(1,3) = ((partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))
     &   *partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*
     &  (partic(a_pz,ip,iesp)+0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umatstar(2,1) = ((partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))
     &  *partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*
     &  (partic(a_px,ip,iesp)+0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umatstar(2,2) = ((partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))
     &  *partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*
     &  (partic(a_py,ip,iesp)+0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umatstar(2,3) = ((partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))
     &  *partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*
     &  (partic(a_pz,ip,iesp)+0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umatstar(3,1) = ((partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))
     &  *partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*
     &  (partic(a_px,ip,iesp)+0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umatstar(3,2) = ((partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))
     &  *partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*
     &  (partic(a_py,ip,iesp)+0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umatstar(3,3) = ((partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))
     &  *partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*
     &  (partic(a_pz,ip,iesp)+0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umatstar(:,:) = 0.25*rqm
     &    *(dt_eff/partic(a_gatld,ip,iesp)**3)*Umatstar(:,:)
       Umatstar(:,:) = identity(:,:) - Umatstar(:,:)

! ------ Forme simplifiee (gamma_n approxime a gamma_tild_n)
       ELSE
         Umatstar(:,:) = identity(:,:)
       ENDIF


! ----- d�termination des tenseurs de susceptibilit� pour la paricule n
       chimat(1:3,1:3) = matmul(Mrelat, Identity+rotmat)
       chimat(1:3,1:3) = matmul(chimat,Umatstar)
       chimat(:,:) = 0.25*charge(iesp)**2/masse(iesp)*
     &      dt_eff**2 *chimat(:,:)

       zetamat(1,1) = vye*rotmat(3,1)     - vze*rotmat(2,1)
       zetamat(1,2) = vye*rotmat(3,2)     - vze*(1+rotmat(2,2))
       zetamat(1,3) = vye*(1+rotmat(3,3)) - vze*rotmat(2,3)

       zetamat(2,1) = vze*(1+rotmat(1,1)) - vxe*rotmat(3,1)
       zetamat(2,2) = vze*rotmat(1,2)     - vxe*rotmat(3,2)
       zetamat(2,3) = vze*rotmat(1,3)     - vxe*(1+rotmat(3,3))

       zetamat(3,1) = vxe*rotmat(2,1)     - vye*(1+rotmat(1,1))
       zetamat(3,2) = vxe*(1+rotmat(2,2)) - vye*rotmat(1,2)
       zetamat(3,3) = vxe*rotmat(2,3)     - vye*rotmat(1,3)

       zetamat(:,:) = matmul(zetamat, Umatstar)
       zetamat(:,:) = 0.125*charge(iesp)**2/masse(iesp)*dt_eff**2
     &    /partic(a_gatld12,ip,iesp)*zetamat(:,:)


       IF(max_cycles.GT.1) THEN
          CALL kizeta_rel_ordn_sub(ponpx, ponpy, ipx, ipy,
     &            chimat, zetamat, iesp, partic(a_po,ip,iesp))
       ELSE
          CALL kizeta_rel_ordn(ponpx, ponpy, ipx, ipy,
     &            chimat, zetamat, iesp, partic(a_po,ip,iesp))
       ENDIF


       IF(debug.GE.4) THEN
         SELECT CASE(ordre_interp)
           CASE(1)
!           CALL kizeta_components(sd1, sd2, sd3, sd4, ipmx, ipmy,
!     &                  chir1, chir2, zetar1, iesp)
           CASE(2)

           CASE DEFAULT
           WRITE(fmsgo,*) 'pousseurs.f : ordre_interp incorrect',
     &         'appel kizeta_rel'
         END SELECT
       ENDIF

       ENDDO

! --- Pourcentage de particules rapides par esp�ce
       write(fmsgo,*) 'Esp�ce : ', iesp
       write(fmsgo,*) '% part. rapides : ',
     &        (REAL(cnt_fast(iesp))*100./REAL(nbpa(iesp)))

       ENDIF !  IF(col_test.EQ.0)


       return
!      ______________________
       END SUBROUTINE prpush_rel




       SUBROUTINE push_rel(iesp)
! ======================================================================
!
!
!
! ======================================================================

       USE domaines
       USE domaines_klder
       USE particules
       USE particules_klder
       USE champs
       USE champs_iter
       USE collisions
       USE temps
       USE diagnostics
       USE erreurs

       IMPLICIT NONE


       INTEGER    ::    i, j
       INTEGER   ::  ip

       INTEGER, INTENT(IN)    ::   iesp

       INTEGER(KIND=kr), DIMENSION(1:ordre_interp+1)  :: ipx, ipy
       REAL(KIND=kr), DIMENSION(1:ordre_interp+1)     :: ponpx, ponpy

       REAL(kind=kr), DIMENSION(1:3,1:3)     ::
     &  rotmat, Umat, Mrelat

       REAL(kind=kr), DIMENSION(1:3,1:3)     ::  VoV

       REAL(kind=kr), DIMENSION(1:3,1:3)     ::
     &  Umatstar, chimat, zetamat

       REAL(kind=kr), DIMENSION(1:3,1:3)     ::
     &  identity

       REAL(kind=kr), DIMENSION(1:3)     ::
     &  vtempe, gave, utild, Evect

       REAL(kind=kr), DIMENSION(1:3)     ::
     &  deltaU, deltaX

       REAL(kind=kr)   :: tbx, tby, tbz, teta2,
     &  gam_p1s2

       REAL(kind=kr)   :: rqm, aux_rh
       REAL(kind=kr)   ::  dt_eff

       REAL(kind=kr)   ::  vxe, vye, vze

       REAL(kind=kr)        ::
     &   sumx, sumy, sumz, dwx, dwy, dwz,
     &   ex1, ey1, ez1

! ---- matrices pour diagnostics tenseurs relativistes
       REAL(kind=kr), DIMENSION(1:3,1:3)     ::
     &   chir1, chir2, zetar1

       REAL(kind=kr), DIMENSION(1:3,1:3)    ::   gradE, Am33

       REAL(kind=kr)  ::   total


       identity(1,1)=1. ; identity(1,2)=0. ; identity(1,3)=0.
       identity(2,1)=0. ; identity(2,2)=1. ; identity(2,3)=0.
       identity(3,1)=0. ; identity(3,2)=0. ; identity(3,3)=1.

       rqm = charge(iesp)/masse(iesp)
       dt_eff = dt*nbscycles(iesp)

       sumx=0.
       sumy=0.
       sumz=0.
       dwx=0.
       dwy=0.
       dwz=0.


       cnt_fast(iesp) = 0.

       IF(col_test.EQ.0) THEN

       DO ip=1,nbpa(iesp)
!     final push
!     ----------
!     on fait les corrections aux valeurs predites de x et v

!
!     interpolation du champ electrique t=(n+1)*dt au centre des particules
!     interpolation du gradient du champ electrique t=n*dt au centre des particules
!
       IF(iter_es.EQ.1) THEN
         SELECT CASE(ordre_interp)
           CASE(1)
           CALL ponder_ordre1_ik(ip, iesp, ponpx, ponpy, ipx, ipy)

           CASE(2)
           CALL ponder_ordre2_ik(ip, iesp, ponpx, ponpy, ipx, ipy)

           CASE(3)
           CALL ponder_ordre3_ik(ip, iesp, ponpx, ponpy, ipx, ipy)

           CASE(4)
           CALL ponder_ordre4_ik(ip, iesp, ponpx, ponpy, ipx, ipy)

           CASE DEFAULT
           write(fmsgo,*) 'pousseurs.f : valeur de ordre_interp ',
     &      'incorrecte '
         END SELECT
       ELSE
         SELECT CASE(ordre_interp)
           CASE(1)
           CALL ponder_ordre1(ip, iesp, ponpx, ponpy, ipx, ipy)

           CASE(2)
           CALL ponder_ordre2(ip, iesp, ponpx, ponpy, ipx, ipy)

           CASE(3)
           CALL ponder_ordre3(ip, iesp, ponpx, ponpy, ipx, ipy)

           CASE(4)
           CALL ponder_ordre4(ip, iesp, ponpx, ponpy, ipx, ipy)

           CASE DEFAULT
           write(fmsgo,*) 'pousseurs.f : valeur de ordre_interp ',
     &       'incorrecte '
         END SELECT
       ENDIF

       CALL champa_2d_exyz1(ex1, ey1, ez1,
     &                ponpx, ponpy, ipx, ipy)
!      on tient compte des it�rations pour calculer le champ magn�tique
!      vu par les particules
! -------------------
       IF(iter_es.EQ.1) THEN
         CALL champa_2dx3dv_bxyz(ip, iesp, ponpx, ponpy, ipx, ipy)
       ENDIF
       IF(simplified.EQ.2) THEN
         CALL champa_2d_gradE(gradE, ponpx, ponpy, ipx, ipy)
       ENDIF

       Evect(1)=ex1 ; Evect(2)=ey1 ; Evect(3)=ez1

! ----- variables matrice de rotation
       tbx = 0.5*rqm*dt_eff*
     &    partic(a_bxp,ip,iesp)/partic(a_gatld,ip,iesp)
       tby = 0.5*rqm*dt_eff*
     &    partic(a_byp,ip,iesp)/partic(a_gatld,ip,iesp)
       tbz = 0.5*rqm*dt_eff*
     &    partic(a_bzp,ip,iesp)/partic(a_gatld,ip,iesp)

       teta2 = tbx**2 + tby**2 + tbz**2

       rotmat(1,1) = 1-teta2+2*tbx**2
       rotmat(1,2) = 2*tbz + 2*tbx*tby
       rotmat(1,3) = -2*tby +2*tbx*tbz

       rotmat(2,1) = 2*tbx*tby - 2*tbz
       rotmat(2,2) = 1-teta2 + 2*tby**2
       rotmat(2,3) = 2*tby*tbz + 2*tbx

       rotmat(3,1) = 2*tby + 2*tbx*tbz
       rotmat(3,2) = 2*tby*tbz - 2*tbx
       rotmat(3,3) = 1-teta2 + 2*tbz**2

       rotmat(:,:) = 1/(1+teta2) * rotmat(:,:)

!
!     definition des coefficients de la matrice Umat
! ----------------------------------------------------------
       IF(simplified.EQ.0) THEN
       Umat(1,1) = ((partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*(partic(a_px,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umat(1,2) = ((partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*(partic(a_py,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umat(1,3) = ((partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*(partic(a_pz,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umat(2,1) = ((partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*(partic(a_px,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umat(2,2) = ((partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*(partic(a_py,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umat(2,3) = ((partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*(partic(a_pz,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umat(3,1) = ((partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*(partic(a_px,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umat(3,2) = ((partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*(partic(a_py,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umat(3,3) = ((partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*(partic(a_pz,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umat(:,:) = 0.25*rqm
     &              *(dt_eff/partic(a_gatld,ip,iesp)**3)*Umat(:,:)
       Umat(:,:) = identity(:,:) - Umat(:,:)
       Umat(:,:) = 0.25*rqm
     &              *dt_eff*matmul((identity+rotmat),Umat)
! ---------------------------
!        Forme simplifiee (gamma_n approxime a gamma_tild_n)
!        avec prise en compte des gradients du champ electrique
! ---------------------------
       ELSEIF(simplified.EQ.2) THEN
         Am33(:,:)= 0.25*rqm*dt_eff*matmul((identity+rotmat),gradE)
         Am33(:,:)= identity - Am33(:,:)
         CALL invmat33(Am33)

         Umat(1:3,1:3) = 0.
         Umat(:,:) = 0.25*rqm*dt_eff*(identity+rotmat)

         Umat(:,:) = matmul(Am33,Umat)

!      Forme simplifiee LSP atention � ne tester que pour une particule dans
!      un vide pouss�, les tenseur chi et zeta sont traites comme dans le cas
!      simplified==1
! ------------------------------------------------------
       ELSEIF(simplified.EQ.5) THEN
         Umat(1:3,1:3) = 0.
         VoV(1:3,1:3) = 0.
         Umat(:,:) = 0.25*rqm*dt_eff*(identity+rotmat)

         vxe = partic(a_pxtld,ip,iesp)/partic(a_gatld12,ip,iesp)
         vye = partic(a_pytld,ip,iesp)/partic(a_gatld12,ip,iesp)
         vze = partic(a_pztld,ip,iesp)/partic(a_gatld12,ip,iesp)

         VoV(1,1) = vxe*vxe
         VoV(1,2) = vxe*vye
         VoV(1,3) = vxe*vze

         VoV(2,1) = vye*vxe
         VoV(2,2) = vye*vye
         VoV(2,3) = vye*vze
         
         VoV(3,1) = vze*vxe
         VoV(3,2) = vze*vye
         VoV(3,3) = vze*vze
         
         Umat(:,:) = Umat - 0.5*rqm*dt_eff*VoV

!        Forme simplifiee (gamma_n approxime a gamma_tild_n)
       ELSE
         Umat(1:3,1:3) = 0.
         Umat(:,:) = 0.25*rqm*dt_eff*(identity+rotmat)
       ENDIF

       deltaU(:) = matmul(Umat,Evect)

!
!      Matrice Mrelat pour le calcul de deltaX
!     ---------------------------------------------
       Mrelat(1,1) = partic(a_pxtld,ip,iesp)**2
       Mrelat(1,2) = partic(a_pxtld,ip,iesp)*partic(a_pytld,ip,iesp)
       Mrelat(1,3) = partic(a_pxtld,ip,iesp)*partic(a_pztld,ip,iesp)

       Mrelat(2,1) = partic(a_pytld,ip,iesp)*partic(a_pxtld,ip,iesp)
       Mrelat(2,2) = partic(a_pytld,ip,iesp)**2
       Mrelat(2,3) = partic(a_pytld,ip,iesp)*partic(a_pztld,ip,iesp)

       Mrelat(3,1) = partic(a_pztld,ip,iesp)*partic(a_pxtld,ip,iesp)
       Mrelat(3,2) = partic(a_pztld,ip,iesp)*partic(a_pytld,ip,iesp)
       Mrelat(3,3) = partic(a_pztld,ip,iesp)**2

       Mrelat(:,:) = Mrelat(:,:)/partic(a_gatld12,ip,iesp)**2

       Mrelat(:,:) = (1/partic(a_gatld12,ip,iesp))*(identity - Mrelat)

       IF(simplified.EQ.5) THEN
         Mrelat(:,:) = (1/partic(a_gatld12,ip,iesp))*identity
       ENDIF

!
!      Calcul de deltaX
!     --------------------------------------
       deltaX(:) = dt_eff*matmul(Mrelat(:,:),deltaU(:))

!
!      Mise � jour des impulsions, des vitesses et des coordonnees
!      de la particule n
!     --------------------------------------
       partic(a_px,ip,iesp) = partic(a_pxtld,ip,iesp)+deltaU(1)
       partic(a_py,ip,iesp) = partic(a_pytld,ip,iesp)+deltaU(2)
       partic(a_pz,ip,iesp) = partic(a_pztld,ip,iesp)+deltaU(3)

       gam_p1s2 = SQRT(1+ partic(a_px,ip,iesp)**2 +
     &    partic(a_py,ip,iesp)**2 + partic(a_pz,ip,iesp)**2)

!       vxe(n) = vxe(n) + deltaX(1)/dt
!       vye(n) = vye(n) + deltaX(2)/dt
!       vze(n) = vze(n) + deltaX(3)/dt

       vxe = partic(a_px,ip,iesp)/gam_p1s2
       vye = partic(a_py,ip,iesp)/gam_p1s2
       vze = partic(a_pz,ip,iesp)/gam_p1s2

 !     calcul de x au temps n+1
 !     -------------------------
       partic(a_qx,ip,iesp)=partic(a_qx,ip,iesp)+deltaX(1)
       partic(a_qy,ip,iesp)=partic(a_qy,ip,iesp)+deltaX(2)

!
!     calcul de l acceleration au temps n+1
!     -------------------------------------

         partic(a_gaxe,ip,iesp) =
     &     rqm*partic(a_ttapushs2,ip,iesp)*ex1 +
     &    (1-partic(a_ttapushs2,ip,iesp))*partic(a_paxe,ip,iesp)
         partic(a_gaye,ip,iesp) =
     &     rqm*partic(a_ttapushs2,ip,iesp)*ey1 +
     &    (1-partic(a_ttapushs2,ip,iesp))*partic(a_paye,ip,iesp)
         partic(a_gaze,ip,iesp) =
     &     rqm*partic(a_ttapushs2,ip,iesp)*ez1 +
     &    (1-partic(a_ttapushs2,ip,iesp))*partic(a_paze,ip,iesp)

         partic(a_paxe,ip,iesp) =
     &     rqm*(1-partic(a_ttapushs2,ip,iesp))*ex1 +
     &     partic(a_ttapushs2,ip,iesp)*partic(a_paxe,ip,iesp)
         partic(a_paye,ip,iesp) =
     &     rqm*(1-partic(a_ttapushs2,ip,iesp))*ey1 +
     &     partic(a_ttapushs2,ip,iesp)*partic(a_paye,ip,iesp)
         partic(a_paze,ip,iesp) =
     &     rqm*(1-partic(a_ttapushs2,ip,iesp))*ez1 +
     &     partic(a_ttapushs2,ip,iesp)*partic(a_paze,ip,iesp)


 !
 !     energie cinetique au temps n+1
 !     ------------------------------
       sumx=sumx+vxe*vxe
       sumy=sumy+vye*vye
       sumz=sumz+vze*vze

       ENDDO

       ENDIF   ! IF(col_test.EQ.0)


! --- + Diagnostics temporels a chaque pas de temps : diag_tp_impl2d
! --- diags de impl2d
!        CALL diag_tp_impl2d(iesp)

!      CALL cndlim_reinje_relat(dwx,dwy,dwz)
       CALL cndlim_part(iesp)


! --- + Tri spatial des particules
       IF(per_tri(iesp).NE.0) THEN
          IF(MOD(iter,per_tri(iesp)).EQ.0) THEN
            CALL tri_part(iesp)
          ENDIF
       ENDIF

!
!**********************************************************************
!

 ! --- On remet � z�ro les courants rj[xyz]e sur la grille
       cjx_esp_2(:,:,iesp) = 0.
       cjy_esp_2(:,:,iesp) = 0.
       cjz_esp_2(:,:,iesp) = 0.
! --- On remet � z�ro les courants rj[xyz]e sur la grille
       rho_esp_2(:,:,iesp) = 0.
       dgx_esp_2(:,:,iesp) = 0.
       dgy_esp_2(:,:,iesp) = 0.
       dgz_esp_2(:,:,iesp) = 0.

       IF(max_cycles.GT.1) THEN
         xxx_esp(:,:,iesp)=0.  
         xxy_esp(:,:,iesp)=0.  
         xxz_esp(:,:,iesp)=0.
         xyx_esp(:,:,iesp)=0.  
         xyy_esp(:,:,iesp)=0.  
         xyz_esp(:,:,iesp)=0.
         xzx_esp(:,:,iesp)=0.  
         xzy_esp(:,:,iesp)=0.  
         xzz_esp(:,:,iesp)=0.

         zxx_esp(:,:,iesp)=0.  
         zxy_esp(:,:,iesp)=0.  
         zxz_esp(:,:,iesp)=0.
         zyx_esp(:,:,iesp)=0.  
         zyy_esp(:,:,iesp)=0.  
         zyz_esp(:,:,iesp)=0.
         zzx_esp(:,:,iesp)=0.  
         zzy_esp(:,:,iesp)=0.  
         zzz_esp(:,:,iesp)=0.                        
       ENDIF

! ----- Calcul du gradient du champ electrique a l instant (n*dt)
       IF(simplified.EQ.2 .AND. iesp.EQ.1) THEN
         CALL calcgradE
       ENDIF

       IF(col_test.EQ.0) THEN

 !     prepush du pas suivant
 !     ----------------------
       DO ip=1,nbpa(iesp)

! ----- determination de gammatild(t0) de la particule n
        vtempe(1) = partic(a_px,ip,iesp) + 0.25*dt_eff*
     &    partic(a_gaxe,ip,iesp)
        vtempe(2) = partic(a_py,ip,iesp) + 0.25*dt_eff*
     &    partic(a_gaye,ip,iesp)
        vtempe(3) = partic(a_pz,ip,iesp) + 0.25*dt_eff*
     &    partic(a_gaze,ip,iesp)

        partic(a_gatld,ip,iesp)= sqrt(1 + vtempe(1)**2
     &     + vtempe(2)**2 + vtempe(3)**2)

        SELECT CASE(ordre_interp)
         CASE(1)
         CALL ponder_ordre1(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(2)
         CALL ponder_ordre2(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(3)
         CALL ponder_ordre3(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(4)
         CALL ponder_ordre4(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE DEFAULT
         WRITE(fmsgo,*) 'pousseurs.f : mauvaise valeur ordre_interp'
       END SELECT

         CALL champa_2d_exyz1(ex1, ey1, ez1,
     &                ponpx, ponpy, ipx, ipy)
         CALL champa_2dx3dv_bxyz(ip, iesp,
     &                ponpx, ponpy, ipx, ipy)

! ----- variables matrice de rotation
       tbx = 0.5*rqm*dt_eff*partic(a_bxp,ip,iesp)
     &  /partic(a_gatld,ip,iesp)
       tby = 0.5*rqm*dt_eff*partic(a_byp,ip,iesp)
     &  /partic(a_gatld,ip,iesp)
       tbz = 0.5*rqm*dt_eff*partic(a_bzp,ip,iesp)
     &  /partic(a_gatld,ip,iesp)

       teta2 = tbx**2 + tby**2 + tbz**2

       rotmat(1,1) = 1-teta2+2*tbx**2
       rotmat(1,2) = 2*tbz + 2*tbx*tby
       rotmat(1,3) = -2*tby +2*tbx*tbz

       rotmat(2,1) = 2*tbx*tby - 2*tbz
       rotmat(2,2) = 1-teta2 + 2*tby**2
       rotmat(2,3) = 2*tby*tbz + 2*tbx

       rotmat(3,1) = 2*tby + 2*tbx*tbz
       rotmat(3,2) = 2*tby*tbz - 2*tbx
       rotmat(3,3) = 1-teta2 + 2*tbz**2

       rotmat(:,:) = 1/(1+teta2) * rotmat(:,:)

! ---- determination de utild(n+1/2)
       vtempe(1)= partic(a_px,ip,iesp)
     &    + 0.25*dt_eff*partic(a_gaxe,ip,iesp)
       vtempe(2)= partic(a_py,ip,iesp)
     &    + 0.25*dt_eff*partic(a_gaye,ip,iesp)
       vtempe(3)= partic(a_pz,ip,iesp)
     &    + 0.25*dt_eff*partic(a_gaze,ip,iesp)

       gave(1)=partic(a_gaxe,ip,iesp);
       gave(2)=partic(a_gaye,ip,iesp);
       gave(3)=partic(a_gaze,ip,iesp)

       utild(1:3) = matmul(rotmat,vtempe) + 0.25*dt_eff*gave(1:3)

! ---- definition de gammatild(n+1/2)
       partic(a_gatld12,ip,iesp) =
     &   SQRT(1+utild(1)**2+utild(2)**2+utild(3)**2)

! ---- Promotion en particule rapide, dont le mouvement n'est pas amorti
!      Crit�re arbitraire : (\gamma -1)> Efast
!      Avec E_fast de l'ordre de quelques m_e c^2
!      rappel :   a_gatld12  equiv a_ga
!
       IF(partic(a_qx,ip,iesp).LE.x_damp1 .OR.
     &    partic(a_qx,ip,iesp).GT.x_damp2 .OR.
     &    (partic(a_ga,ip,iesp)-1).GE.Efast) THEN
         partic(a_ttapushs2,ip,iesp)= 0.5*thetad
	 cnt_fast(iesp)= cnt_fast(iesp)+1
       ELSE
         partic(a_ttapushs2,ip,iesp)= ttapushs2
       ENDIF


! ---- determination de vtild(n+1/2)
       partic(a_pxtld,ip,iesp)=utild(1)
       partic(a_pytld,ip,iesp)=utild(2)
       partic(a_pztld,ip,iesp)=utild(3)

       vxe = partic(a_pxtld,ip,iesp)/partic(a_gatld12,ip,iesp)
       vye = partic(a_pytld,ip,iesp)/partic(a_gatld12,ip,iesp)
       vze = partic(a_pztld,ip,iesp)/partic(a_gatld12,ip,iesp)

!
!      Matrice Mrelat pour le calcul de chi et zeta
!     ---------------------------------------------
       Mrelat(1,1) = partic(a_pxtld,ip,iesp)**2
       Mrelat(1,2) = partic(a_pxtld,ip,iesp)*partic(a_pytld,ip,iesp)
       Mrelat(1,3) = partic(a_pxtld,ip,iesp)*partic(a_pztld,ip,iesp)

       Mrelat(2,1) = partic(a_pytld,ip,iesp)*partic(a_pxtld,ip,iesp)
       Mrelat(2,2) = partic(a_pytld,ip,iesp)**2
       Mrelat(2,3) = partic(a_pytld,ip,iesp)*partic(a_pztld,ip,iesp)

       Mrelat(3,1) = partic(a_pztld,ip,iesp)*partic(a_pxtld,ip,iesp)
       Mrelat(3,2) = partic(a_pztld,ip,iesp)*partic(a_pytld,ip,iesp)
       Mrelat(3,3) = partic(a_pztld,ip,iesp)**2

       Mrelat(:,:) = Mrelat(:,:)/partic(a_gatld12,ip,iesp)**2

       Mrelat(:,:) = (1/partic(a_gatld12,ip,iesp))*(identity - Mrelat)

       aux_rh = partic(a_po,ip,iesp)

       CALL projpa_2dx3dv_rjxyz(iesp, ponpx, ponpy,
     &                   ipx, ipy, vxe, vye, vze, aux_rh)

 !
 !     calcul de x au temps n+1
 !     -------------------------
       partic(a_qx,ip,iesp)=partic(a_qx,ip,iesp)+dt_eff*vxe
       partic(a_qy,ip,iesp)=partic(a_qy,ip,iesp)+dt_eff*vye

       IF(partic(a_qy,ip,iesp).GE.bord(4,numproc)) THEN
         partic(a_qy,ip,iesp) =
     &        partic(a_qy,ip,iesp) -ylong
       ELSEIF(partic(a_qy,ip,iesp).LE.bord(3,numproc)) THEN
         partic(a_qy,ip,iesp) =
     &        partic(a_qy,ip,iesp) +ylong
       ENDIF

 !
 !     calcul de rhoe en (x n+1) et j(x n+1,v n+1/2)
 !     ---------------------------------------------
       SELECT CASE(ordre_interp)
         CASE(1)
         CALL ponder_ordre1(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(2)
         CALL ponder_ordre2(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(3)
         CALL ponder_ordre3(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(4)
         CALL ponder_ordre4(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE DEFAULT
         WRITE(fmsgo,*) 'pousseurs.f : ordre_interp incorrecte'
       END SELECT

       CALL projpa_2dx3dv_dgxyz(iesp, ponpx, ponpy,
     &                ipx, ipy, vxe, vye, vze, aux_rh)

       CALL champa_2d_exyz1(ex1, ey1, ez1,
     &                ponpx, ponpy, ipx, ipy)
       CALL champa_2dx3dv_bxyz(ip, iesp,
     &                ponpx, ponpy, ipx, ipy)
       IF(simplified.EQ.2) THEN
          CALL champa_2d_gradE(gradE, ponpx, ponpy, ipx, ipy)
       ENDIF

! ----- variables matrice de rotation
       tbx = 0.5*rqm*dt_eff*partic(a_bxp,ip,iesp)
     &  /partic(a_gatld,ip,iesp)
       tby = 0.5*rqm*dt_eff*partic(a_byp,ip,iesp)
     &  /partic(a_gatld,ip,iesp)
       tbz = 0.5*rqm*dt_eff*partic(a_bzp,ip,iesp)
     &  /partic(a_gatld,ip,iesp)

       teta2 = tbx**2 + tby**2 + tbz**2

       rotmat(1,1) = 1-teta2+2*tbx**2
       rotmat(1,2) = 2*tbz + 2*tbx*tby
       rotmat(1,3) = -2*tby +2*tbx*tbz

       rotmat(2,1) = 2*tbx*tby - 2*tbz
       rotmat(2,2) = 1-teta2 + 2*tby**2
       rotmat(2,3) = 2*tby*tbz + 2*tbx

       rotmat(3,1) = 2*tby + 2*tbx*tbz
       rotmat(3,2) = 2*tby*tbz - 2*tbx
       rotmat(3,3) = 1-teta2 + 2*tbz**2

       rotmat(:,:) = 1/(1+teta2) * rotmat(:,:)

!
!     definition des coefficients de la matrice Umatstar pour electron
! ----------------------------------------------------------
       IF(simplified.EQ.0) THEN
       Umatstar(1,1) = ((partic(a_py,ip,iesp)+
     &   partic(a_pytld,ip,iesp))*partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*(partic(a_px,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umatstar(1,2) = ((partic(a_py,ip,iesp)+
     &   partic(a_pytld,ip,iesp))*partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*(partic(a_py,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umatstar(1,3) = ((partic(a_py,ip,iesp)+
     &   partic(a_pytld,ip,iesp))*partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*(partic(a_pz,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umatstar(2,1) = ((partic(a_pz,ip,iesp)+
     &   partic(a_pztld,ip,iesp))*partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*(partic(a_px,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umatstar(2,2) = ((partic(a_pz,ip,iesp)+
     &   partic(a_pztld,ip,iesp))*partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*(partic(a_py,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umatstar(2,3) = ((partic(a_pz,ip,iesp)+
     &   partic(a_pztld,ip,iesp))*partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*(partic(a_pz,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umatstar(3,1) = ((partic(a_px,ip,iesp)+
     &   partic(a_pxtld,ip,iesp))*partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*(partic(a_px,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umatstar(3,2) = ((partic(a_px,ip,iesp)+
     &   partic(a_pxtld,ip,iesp))*partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*(partic(a_py,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umatstar(3,3) = ((partic(a_px,ip,iesp)+
     &   partic(a_pxtld,ip,iesp))*partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*(partic(a_pz,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umatstar(:,:)= 0.25*rqm
     &    *(dt_eff/partic(a_gatld,ip,iesp)**3) *Umatstar(:,:)
       Umatstar(:,:)= identity(:,:) - Umatstar(:,:)
! ---------------------------
!        Forme simplifiee (gamma_n approxime a gamma_tild_n)
!        avec prise en compte des gradients du champ electrique
! ---------------------------
       ELSEIF(simplified.EQ.2) THEN
         Am33(:,:)= 0.25*rqm*dt_eff*matmul((identity+rotmat),gradE)
         Am33(:,:)= identity - Am33(:,:)
         CALL invmat33(Am33)

         Umatstar(:,:) = identity(:,:)

!        Forme simplifiee (gamma_n approxime a gamma_tild_n)
       ELSE
         Umatstar(:,:)= identity(:,:)
       ENDIF

       IF(simplified.EQ.2) THEN
         Mrelat(:,:) = matmul(Am33,Mrelat)
       ENDIF
       chimat(1:3,1:3) = matmul(Mrelat, Identity+rotmat)
       chimat(1:3,1:3) = matmul(chimat,Umatstar)
       chimat(:,:)=0.25*charge(iesp)**2/masse(iesp)*
     &     dt_eff**2 *chimat(:,:)

       zetamat(1,1) = vye*rotmat(3,1)     - vze*rotmat(2,1)
       zetamat(1,2) = vye*rotmat(3,2)     - vze*(1+rotmat(2,2))
       zetamat(1,3) = vye*(1+rotmat(3,3)) - vze*rotmat(2,3)

       zetamat(2,1) = vze*(1+rotmat(1,1)) - vxe*rotmat(3,1)
       zetamat(2,2) = vze*rotmat(1,2)     - vxe*rotmat(3,2)
       zetamat(2,3) = vze*rotmat(1,3)     - vxe*(1+rotmat(3,3))

       zetamat(3,1) = vxe*rotmat(2,1)     - vye*(1+rotmat(1,1))
       zetamat(3,2) = vxe*(1+rotmat(2,2)) - vye*rotmat(1,2)
       zetamat(3,3) = vxe*rotmat(2,3)     - vye*rotmat(1,3)

       zetamat(:,:) = matmul(zetamat, Umatstar)
       zetamat(:,:) = 0.125*charge(iesp)**2/masse(iesp)*
     &        dt_eff**2/partic(a_gatld12,ip,iesp)*zetamat(:,:)


!         CALL kizeta_rel(sd1, sd2, sd3, sd4, ipmx, ipmy,
!     &                       chimat, zetamat, iesp)
       IF(max_cycles.GT.1) THEN
         CALL kizeta_rel_ordn_sub(ponpx, ponpy, ipx, ipy,
     &          chimat, zetamat, iesp, partic(a_po,ip,iesp))
       ELSE
         CALL kizeta_rel_ordn(ponpx, ponpy, ipx, ipy,
     &          chimat, zetamat, iesp, partic(a_po,ip,iesp))
       ENDIF


       IF(debug.GE.4) THEN
         SELECT CASE(ordre_interp)
           CASE(1)
!           CALL kizeta_components(sd1, sd2, sd3, sd4, ipmx, ipmy,
!     &                  chir1, chir2, zetar1, iesp)
           CASE(2)

           CASE DEFAULT
           WRITE(fmsgo,*) 'pousseurs.f : ordre_interp incorrect',
     &         'appel kizeta_rel'
         END SELECT
       ENDIF

       ENDDO

! --- Pourcentage de particules rapides par esp�ce
       write(fmsgo,*) 'Esp�ce : ', iesp
       write(fmsgo,*) '% part. rapides : ',
     &        (REAL(cnt_fast(iesp))*100./REAL(nbpa(iesp)))

       ENDIF  ! IF(col_test.EQ.0)


       return
!      ____________________
       END SUBROUTINE push_rel



       SUBROUTINE prpush_rel_thetad(iesp)
! ======================================================================
!
!
!
!
!
! ======================================================================

       USE domaines
       USE domaines_klder
       USE particules
       USE particules_klder
       USE champs
       USE collisions
       USE temps
       USE erreurs

       IMPLICIT NONE


       INTEGER    ::   i, j
       INTEGER    ::   ip

!       REAL(kind=kr)     ::  xint, yint

       REAL(kind=kr)   ::  ex1, ey1, ez1

       INTEGER(KIND=kr), DIMENSION(1:ordre_interp+1)  :: ipx, ipy
       REAL(KIND=kr), DIMENSION(1:ordre_interp+1)     :: ponpx, ponpy

       REAL(kind=kr), DIMENSION(1:3,1:3)     ::
     &  rotmat, identity

       REAL(kind=kr), DIMENSION(1:3,1:3)     ::
     &  Umatstar, chimat, zetamat, Mrelat

       REAL(kind=kr), DIMENSION(1:3)     ::
     &  vtempe, gave, utild

       REAL(kind=kr)   ::  vxe, vye, vze
       REAL(kind=kr)   ::  rqm, aux_rh
       REAL(kind=kr)   ::  dt_eff

       INTEGER, INTENT(IN)     ::   iesp

       REAL(kind=kr)   :: tbx, tby, tbz, teta2

! ---- matrices pour diagnostics tenseurs relativistes
       REAL(kind=kr), DIMENSION(1:3,1:3)     ::
     &   chir1, chir2, zetar1


       identity(1,1)=1. ; identity(1,2)=0. ; identity(1,3)=0.
       identity(2,1)=0. ; identity(2,2)=1. ; identity(2,3)=0.
       identity(3,1)=0. ; identity(3,2)=0. ; identity(3,3)=1.

! --- On remet � z�ro les courants rj[xyz]e sur la grille
       cjx_esp_2(:,:,iesp) = 0.
       cjy_esp_2(:,:,iesp) = 0.
       cjz_esp_2(:,:,iesp) = 0.
! --- On remet � z�ro les courants rj[xyz]e sur la grille
       rho_esp_2(:,:,iesp) = 0.
       dgx_esp_2(:,:,iesp) = 0.
       dgy_esp_2(:,:,iesp) = 0.
       dgz_esp_2(:,:,iesp) = 0.

       IF(max_cycles.GT.1) THEN
         xxx_esp(:,:,iesp)=0.  
         xxy_esp(:,:,iesp)=0.  
         xxz_esp(:,:,iesp)=0.
         xyx_esp(:,:,iesp)=0.  
         xyy_esp(:,:,iesp)=0.  
         xyz_esp(:,:,iesp)=0.
         xzx_esp(:,:,iesp)=0.  
         xzy_esp(:,:,iesp)=0.  
         xzz_esp(:,:,iesp)=0.

         zxx_esp(:,:,iesp)=0.  
         zxy_esp(:,:,iesp)=0.  
         zxz_esp(:,:,iesp)=0.
         zyx_esp(:,:,iesp)=0.  
         zyy_esp(:,:,iesp)=0.  
         zyz_esp(:,:,iesp)=0.
         zzx_esp(:,:,iesp)=0.  
         zzy_esp(:,:,iesp)=0.  
         zzz_esp(:,:,iesp)=0.            
       ENDIF


       rqm = charge(iesp)/masse(iesp)
       dt_eff = dt*nbscycles(iesp)

! ----- Calcul du gradient du champ electrique a l instant (n*dt)
       IF(simplified.EQ.2 .AND. iesp.EQ.1) THEN
         CALL calcgradE
       ENDIF


       IF(col_test.EQ.0) THEN

 !     prepush initial
 !     ----------------------
       DO ip=1,nbpa(iesp)

! ----- determination de gammatild(t0) de la particule n
        vtempe(1) = partic(a_px,ip,iesp) +
     &     0.25*dt_eff*partic(a_gaxe,ip,iesp)
        vtempe(2) = partic(a_py,ip,iesp) +
     &     0.25*dt_eff*partic(a_gaye,ip,iesp)
        vtempe(3) = partic(a_pz,ip,iesp) +
     &     0.25*dt_eff*partic(a_gaze,ip,iesp)

        partic(a_gatld,ip,iesp)= sqrt(1 + vtempe(1)**2
     &     + vtempe(2)**2 + vtempe(3)**2)

        SELECT CASE(ordre_interp)
          CASE(1)
            CALL ponder_ordre1(ip, iesp, ponpx, ponpy, ipx, ipy)

          CASE(2)
            CALL ponder_ordre2(ip, iesp, ponpx, ponpy, ipx, ipy)

          CASE(3)
            CALL ponder_ordre3(ip, iesp, ponpx, ponpy, ipx, ipy)

          CASE(4)
            CALL ponder_ordre4(ip, iesp, ponpx, ponpy, ipx, ipy)

          CASE DEFAULT
            WRITE(fmsgo,*) 'pousseurs.f : mauvaise valeur ordre_interp'
        END SELECT

         CALL champa_2d_exyz1(ex1, ey1, ez1,
     &                ponpx, ponpy, ipx, ipy)
         CALL champa_2dx3dv_bxyz(ip, iesp,
     &                ponpx, ponpy, ipx, ipy)

 !
 !     initialisation de gaxe,gaye,gaze
 !
       partic(a_gaxe,ip,iesp) =
     &    rqm*thetad_2(ipx(iptarf),ipy(iptarf))*ex1
     &   + (1-thetad_2(ipx(iptarf),ipy(iptarf)))*partic(a_paxe,ip,iesp)
       partic(a_gaye,ip,iesp) =
     &     rqm*thetad_2(ipx(iptarf),ipy(iptarf))*ey1
     &   + (1-thetad_2(ipx(iptarf),ipy(iptarf)))*partic(a_paye,ip,iesp)
       partic(a_gaze,ip,iesp) =
     &     rqm*thetad_2(ipx(iptarf),ipy(iptarf))*ez1
     &   + (1-thetad_2(ipx(iptarf),ipy(iptarf)))*partic(a_paze,ip,iesp)

! ----- variables matrice de rotation
       tbx = 0.5*rqm*dt_eff*partic(a_bxp,ip,iesp)
     &    /partic(a_gatld,ip,iesp)
       tby = 0.5*rqm*dt_eff*partic(a_byp,ip,iesp)
     &    /partic(a_gatld,ip,iesp)
       tbz = 0.5*rqm*dt_eff*partic(a_bzp,ip,iesp)
     &    /partic(a_gatld,ip,iesp)

       teta2 = tbx**2 + tby**2 + tbz**2

       rotmat(1,1) = 1-teta2+2*tbx**2
       rotmat(1,2) = 2*tbz + 2*tbx*tby
       rotmat(1,3) = -2*tby +2*tbx*tbz

       rotmat(2,1) = 2*tbx*tby - 2*tbz
       rotmat(2,2) = 1-teta2 + 2*tby**2
       rotmat(2,3) = 2*tby*tbz + 2*tbx

       rotmat(3,1) = 2*tby + 2*tbx*tbz
       rotmat(3,2) = 2*tby*tbz - 2*tbx
       rotmat(3,3) = 1-teta2 + 2*tbz**2

       rotmat(:,:) = 1/(1+teta2) * rotmat(:,:)

! ---- determination de utild(n+1/2)
       vtempe(1)= partic(a_px,ip,iesp)
     &    + 0.25*dt_eff*partic(a_gaxe,ip,iesp)
       vtempe(2)= partic(a_py,ip,iesp)
     &    + 0.25*dt_eff*partic(a_gaye,ip,iesp)
       vtempe(3)= partic(a_pz,ip,iesp)
     &    + 0.25*dt_eff*partic(a_gaze,ip,iesp)

       gave(1)=partic(a_gaxe,ip,iesp)
       gave(2)=partic(a_gaye,ip,iesp)
       gave(3)=partic(a_gaze,ip,iesp)

       utild(1:3) = matmul(rotmat,vtempe) + 0.25*dt_eff*gave(1:3)

! ---- definition de gammatild(n+1/2)
       partic(a_gatld12,ip,iesp) =
     &   SQRT(1+utild(1)**2+utild(2)**2+utild(3)**2)

! ---- determination de vtild(n+1/2)
       partic(a_pxtld,ip,iesp)=utild(1)
       partic(a_pytld,ip,iesp)=utild(2)
       partic(a_pztld,ip,iesp)=utild(3)

       vxe = partic(a_pxtld,ip,iesp)/partic(a_gatld12,ip,iesp)
       vye = partic(a_pytld,ip,iesp)/partic(a_gatld12,ip,iesp)
       vze = partic(a_pztld,ip,iesp)/partic(a_gatld12,ip,iesp)

!
!      Matrice Mrelat pour le calcul de chi et zeta
!     ---------------------------------------------
       Mrelat(1,1) = partic(a_pxtld,ip,iesp)**2
       Mrelat(1,2) = partic(a_pxtld,ip,iesp)*partic(a_pytld,ip,iesp)
       Mrelat(1,3) = partic(a_pxtld,ip,iesp)*partic(a_pztld,ip,iesp)

       Mrelat(2,1) = partic(a_pytld,ip,iesp)*partic(a_pxtld,ip,iesp)
       Mrelat(2,2) = partic(a_pytld,ip,iesp)**2
       Mrelat(2,3) = partic(a_pytld,ip,iesp)*partic(a_pztld,ip,iesp)

       Mrelat(3,1) = partic(a_pztld,ip,iesp)*partic(a_pxtld,ip,iesp)
       Mrelat(3,2) = partic(a_pztld,ip,iesp)*partic(a_pytld,ip,iesp)
       Mrelat(3,3) = partic(a_pztld,ip,iesp)**2

       Mrelat(:,:) = Mrelat(:,:)/partic(a_gatld12,ip,iesp)**2

       Mrelat(:,:) = (1/partic(a_gatld12,ip,iesp))*(identity - Mrelat)

       aux_rh = partic(a_po,ip,iesp)

       CALL projpa_2dx3dv_rjxyz(iesp, ponpx, ponpy,
     &              ipx, ipy, vxe, vye, vze, aux_rh)

 !
 !     calcul de xtild(n+1)
 !     -------------------------
       partic(a_qx,ip,iesp)=partic(a_qx,ip,iesp)+dt_eff*vxe
       partic(a_qy,ip,iesp)=partic(a_qy,ip,iesp)+dt_eff*vye

       IF(partic(a_qy,ip,iesp).GE.bord(4,numproc)) THEN
         partic(a_qy,ip,iesp) =
     &        partic(a_qy,ip,iesp) -ylong
       ELSEIF(partic(a_qy,ip,iesp).LE.bord(3,numproc)) THEN
         partic(a_qy,ip,iesp) =
     &        partic(a_qy,ip,iesp) +ylong
       ENDIF

 !
 !     calcul de rhoe en (x n+1) et j(x n+1,v n+1/2)
 !     ---------------------------------------------
       SELECT CASE(ordre_interp)
         CASE(1)
         CALL ponder_ordre1(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(2)
         CALL ponder_ordre2(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(3)
         CALL ponder_ordre3(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(4)
         CALL ponder_ordre4(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE DEFAULT
         WRITE(fmsgo,*) 'pousseurs.f : ordre_interp incorrecte'
       END SELECT

       CALL projpa_2dx3dv_dgxyz(iesp, ponpx, ponpy,
     &                ipx, ipy, vxe, vye, vze, aux_rh)

       CALL champa_2d_exyz1(ex1, ey1, ez1,
     &                ponpx, ponpy, ipx, ipy)
       CALL champa_2dx3dv_bxyz(ip, iesp,
     &                ponpx, ponpy, ipx, ipy)

! ----- variables matrice de rotation
       tbx = 0.5*rqm*dt_eff*partic(a_bxp,ip,iesp)
     &    /partic(a_gatld,ip,iesp)
       tby = 0.5*rqm*dt_eff*partic(a_byp,ip,iesp)
     &    /partic(a_gatld,ip,iesp)
       tbz = 0.5*rqm*dt_eff*partic(a_bzp,ip,iesp)
     &    /partic(a_gatld,ip,iesp)

       teta2 = tbx**2 + tby**2 + tbz**2

       rotmat(1,1) = 1-teta2+2*tbx**2
       rotmat(1,2) = 2*tbz + 2*tbx*tby
       rotmat(1,3) = -2*tby +2*tbx*tbz

       rotmat(2,1) = 2*tbx*tby - 2*tbz
       rotmat(2,2) = 1-teta2 + 2*tby**2
       rotmat(2,3) = 2*tby*tbz + 2*tbx

       rotmat(3,1) = 2*tby + 2*tbx*tbz
       rotmat(3,2) = 2*tby*tbz - 2*tbx
       rotmat(3,3) = 1-teta2 + 2*tbz**2

       rotmat(:,:) = 1/(1+teta2) * rotmat(:,:)

!
!     definition des coefficients de la matrice Umatstar pour electron
! ----------------------------------------------------------
       IF(simplified.EQ.0) THEN
       Umatstar(1,1) = ((partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))
     &   *partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*
     &  (partic(a_px,ip,iesp)+0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umatstar(1,2) = ((partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))
     &   *partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*
     &  (partic(a_py,ip,iesp)+0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umatstar(1,3) = ((partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))
     &   *partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*
     &  (partic(a_pz,ip,iesp)+0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umatstar(2,1) = ((partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))
     &  *partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*
     &  (partic(a_px,ip,iesp)+0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umatstar(2,2) = ((partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))
     &  *partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*
     &  (partic(a_py,ip,iesp)+0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umatstar(2,3) = ((partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))
     &  *partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*
     &  (partic(a_pz,ip,iesp)+0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umatstar(3,1) = ((partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))
     &  *partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*
     &  (partic(a_px,ip,iesp)+0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umatstar(3,2) = ((partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))
     &  *partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*
     &  (partic(a_py,ip,iesp)+0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umatstar(3,3) = ((partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))
     &  *partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*
     &  (partic(a_pz,ip,iesp)+0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umatstar(:,:) = 0.25*rqm
     &    *(dt_eff/partic(a_gatld,ip,iesp)**3)*Umatstar(:,:)
       Umatstar(:,:) = identity(:,:) - Umatstar(:,:)

! ------ Forme simplifiee (gamma_n approxime a gamma_tild_n)
       ELSE
         Umatstar(:,:) = identity(:,:)
       ENDIF


! ----- d�termination des tenseurs de susceptibilit� pour la paricule n
       chimat(1:3,1:3) = matmul(Mrelat, Identity+rotmat)
       chimat(1:3,1:3) = matmul(chimat,Umatstar)
       chimat(:,:) = 0.25*charge(iesp)**2/masse(iesp)*
     &      dt_eff**2 *chimat(:,:)

       zetamat(1,1) = vye*rotmat(3,1)     - vze*rotmat(2,1)
       zetamat(1,2) = vye*rotmat(3,2)     - vze*(1+rotmat(2,2))
       zetamat(1,3) = vye*(1+rotmat(3,3)) - vze*rotmat(2,3)

       zetamat(2,1) = vze*(1+rotmat(1,1)) - vxe*rotmat(3,1)
       zetamat(2,2) = vze*rotmat(1,2)     - vxe*rotmat(3,2)
       zetamat(2,3) = vze*rotmat(1,3)     - vxe*(1+rotmat(3,3))

       zetamat(3,1) = vxe*rotmat(2,1)     - vye*(1+rotmat(1,1))
       zetamat(3,2) = vxe*(1+rotmat(2,2)) - vye*rotmat(1,2)
       zetamat(3,3) = vxe*rotmat(2,3)     - vye*rotmat(1,3)

       zetamat(:,:) = matmul(zetamat, Umatstar)
       zetamat(:,:) = 0.125*charge(iesp)**2/masse(iesp)*dt_eff**2
     &    /partic(a_gatld12,ip,iesp)*zetamat(:,:)


       IF(max_cycles.GT.1) THEN
         CALL kizeta_rel_ordn_sub(ponpx, ponpy, ipx, ipy,
     &          chimat, zetamat, iesp, partic(a_po,ip,iesp))
       ELSE
         CALL kizeta_rel_ordn(ponpx, ponpy, ipx, ipy,
     &          chimat, zetamat, iesp, partic(a_po,ip,iesp))
       ENDIF


       IF(debug.GE.4) THEN
         SELECT CASE(ordre_interp)
           CASE(1)
!           CALL kizeta_components(sd1, sd2, sd3, sd4, ipmx, ipmy,
!     &                  chir1, chir2, zetar1, iesp)
           CASE(2)

           CASE DEFAULT
           WRITE(fmsgo,*) 'pousseurs.f : ordre_interp incorrect',
     &         'appel kizeta_rel'
         END SELECT
       ENDIF

       ENDDO

       ENDIF !  IF(col_test.EQ.0)


       return
!      ______________________
       END SUBROUTINE prpush_rel_thetad




       SUBROUTINE push_rel_thetad(iesp)
! ======================================================================
!
!
!
! ======================================================================

       USE domaines
       USE domaines_klder
       USE particules
       USE particules_klder
       USE champs
       USE champs_iter
       USE collisions
       USE temps
       USE diagnostics
       USE erreurs

       IMPLICIT NONE


       INTEGER    ::    i, j
       INTEGER   ::  ip

       INTEGER, INTENT(IN)    ::   iesp

       INTEGER(KIND=kr), DIMENSION(1:ordre_interp+1)  :: ipx, ipy
       REAL(KIND=kr), DIMENSION(1:ordre_interp+1)     :: ponpx, ponpy

       REAL(kind=kr), DIMENSION(1:3,1:3)     ::
     &  rotmat, Umat, Mrelat

       REAL(kind=kr), DIMENSION(1:3,1:3)     ::
     &  Umatstar, chimat, zetamat

       REAL(kind=kr), DIMENSION(1:3,1:3)     ::
     &  identity

       REAL(kind=kr), DIMENSION(1:3)     ::
     &  vtempe, gave, utild, Evect

       REAL(kind=kr), DIMENSION(1:3)     ::
     &  deltaU, deltaX

       REAL(kind=kr)   :: tbx, tby, tbz, teta2,
     &  gam_p1s2

       REAL(kind=kr)   :: rqm, aux_rh
       REAL(kind=kr)   ::  dt_eff

       REAL(kind=kr)   ::  vxe, vye, vze

       REAL(kind=kr)        ::
     &   sumx, sumy, sumz, dwx, dwy, dwz,
     &   ex1, ey1, ez1

! ---- matrices pour diagnostics tenseurs relativistes
       REAL(kind=kr), DIMENSION(1:3,1:3)     ::
     &   chir1, chir2, zetar1

       REAL(kind=kr), DIMENSION(1:3,1:3)    ::   gradE, Am33

       REAL(kind=kr)  ::   total


       identity(1,1)=1. ; identity(1,2)=0. ; identity(1,3)=0.
       identity(2,1)=0. ; identity(2,2)=1. ; identity(2,3)=0.
       identity(3,1)=0. ; identity(3,2)=0. ; identity(3,3)=1.

       rqm = charge(iesp)/masse(iesp)
       dt_eff = dt*nbscycles(iesp)

       sumx=0.
       sumy=0.
       sumz=0.
       dwx=0.
       dwy=0.
       dwz=0.


       IF(col_test.EQ.0) THEN

       DO ip=1,nbpa(iesp)
!     final push
!     ----------
!     on fait les corrections aux valeurs predites de x et v

!
!     interpolation du champ electrique t=(n+1)*dt au centre des particules
!     interpolation du gradient du champ electrique t=n*dt au centre des particules
!
       IF(iter_es.EQ.1) THEN
         SELECT CASE(ordre_interp)
           CASE(1)
           CALL ponder_ordre1_ik(ip, iesp, ponpx, ponpy, ipx, ipy)

           CASE(2)
           CALL ponder_ordre2_ik(ip, iesp, ponpx, ponpy, ipx, ipy)

           CASE(3)
           CALL ponder_ordre3_ik(ip, iesp, ponpx, ponpy, ipx, ipy)

           CASE(4)
           CALL ponder_ordre4_ik(ip, iesp, ponpx, ponpy, ipx, ipy)

           CASE DEFAULT
           write(fmsgo,*) 'pousseurs.f : valeur de ordre_interp ',
     &      'incorrecte '
         END SELECT
       ELSE
         SELECT CASE(ordre_interp)
           CASE(1)
           CALL ponder_ordre1(ip, iesp, ponpx, ponpy, ipx, ipy)

           CASE(2)
           CALL ponder_ordre2(ip, iesp, ponpx, ponpy, ipx, ipy)

           CASE(3)
           CALL ponder_ordre3(ip, iesp, ponpx, ponpy, ipx, ipy)

           CASE(4)
           CALL ponder_ordre4(ip, iesp, ponpx, ponpy, ipx, ipy)

           CASE DEFAULT
           write(fmsgo,*) 'pousseurs.f : valeur de ordre_interp ',
     &       'incorrecte '
         END SELECT
       ENDIF

       CALL champa_2d_exyz1(ex1, ey1, ez1,
     &                ponpx, ponpy, ipx, ipy)
!      on tient compte des it�rations pour calculer le champ magn�tique
!      vu par les particules
! -------------------
       IF(iter_es.EQ.1) THEN
         CALL champa_2dx3dv_bxyz(ip, iesp, ponpx, ponpy, ipx, ipy)
       ENDIF
       IF(simplified.EQ.2) THEN
         CALL champa_2d_gradE(gradE, ponpx, ponpy, ipx, ipy)
       ENDIF

       Evect(1)=ex1 ; Evect(2)=ey1 ; Evect(3)=ez1

! ----- variables matrice de rotation
       tbx = 0.5*rqm*dt_eff*
     &    partic(a_bxp,ip,iesp)/partic(a_gatld,ip,iesp)
       tby = 0.5*rqm*dt_eff*
     &    partic(a_byp,ip,iesp)/partic(a_gatld,ip,iesp)
       tbz = 0.5*rqm*dt_eff*
     &    partic(a_bzp,ip,iesp)/partic(a_gatld,ip,iesp)

       teta2 = tbx**2 + tby**2 + tbz**2

       rotmat(1,1) = 1-teta2+2*tbx**2
       rotmat(1,2) = 2*tbz + 2*tbx*tby
       rotmat(1,3) = -2*tby +2*tbx*tbz

       rotmat(2,1) = 2*tbx*tby - 2*tbz
       rotmat(2,2) = 1-teta2 + 2*tby**2
       rotmat(2,3) = 2*tby*tbz + 2*tbx

       rotmat(3,1) = 2*tby + 2*tbx*tbz
       rotmat(3,2) = 2*tby*tbz - 2*tbx
       rotmat(3,3) = 1-teta2 + 2*tbz**2

       rotmat(:,:) = 1/(1+teta2) * rotmat(:,:)

!
!     definition des coefficients de la matrice Umat
! ----------------------------------------------------------
       IF(simplified.EQ.0) THEN
       Umat(1,1) = ((partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*(partic(a_px,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umat(1,2) = ((partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*(partic(a_py,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umat(1,3) = ((partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*(partic(a_pz,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umat(2,1) = ((partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*(partic(a_px,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umat(2,2) = ((partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*(partic(a_py,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umat(2,3) = ((partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*(partic(a_pz,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umat(3,1) = ((partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*(partic(a_px,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umat(3,2) = ((partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*(partic(a_py,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umat(3,3) = ((partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*(partic(a_pz,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umat(:,:) = 0.25*rqm
     &              *(dt_eff/partic(a_gatld,ip,iesp)**3)*Umat(:,:)
       Umat(:,:) = identity(:,:) - Umat(:,:)
       Umat(:,:) = 0.25*rqm
     &              *dt_eff*matmul((identity+rotmat),Umat)
! ---------------------------
!        Forme simplifiee (gamma_n approxime a gamma_tild_n)
!        avec prise en compte des gradients du champ electrique
! ---------------------------
       ELSEIF(simplified.EQ.2) THEN
         Am33(:,:)= 0.25*rqm*dt_eff*matmul((identity+rotmat),gradE)
         Am33(:,:)= identity - Am33(:,:)
         CALL invmat33(Am33)

         Umat(1:3,1:3) = 0.
         Umat(:,:) = 0.25*rqm*dt_eff*(identity+rotmat)

         Umat(:,:) = matmul(Am33,Umat)

!        Forme simplifiee (gamma_n approxime a gamma_tild_n)
       ELSE
         Umat(1:3,1:3) = 0.
         Umat(:,:) = 0.25*rqm*dt_eff*(identity+rotmat)
       ENDIF

       deltaU(:) = matmul(Umat,Evect)

!
!      Matrice Mrelat pour le calcul de deltaX
!     ---------------------------------------------
       Mrelat(1,1) = partic(a_pxtld,ip,iesp)**2
       Mrelat(1,2) = partic(a_pxtld,ip,iesp)*partic(a_pytld,ip,iesp)
       Mrelat(1,3) = partic(a_pxtld,ip,iesp)*partic(a_pztld,ip,iesp)

       Mrelat(2,1) = partic(a_pytld,ip,iesp)*partic(a_pxtld,ip,iesp)
       Mrelat(2,2) = partic(a_pytld,ip,iesp)**2
       Mrelat(2,3) = partic(a_pytld,ip,iesp)*partic(a_pztld,ip,iesp)

       Mrelat(3,1) = partic(a_pztld,ip,iesp)*partic(a_pxtld,ip,iesp)
       Mrelat(3,2) = partic(a_pztld,ip,iesp)*partic(a_pytld,ip,iesp)
       Mrelat(3,3) = partic(a_pztld,ip,iesp)**2

       Mrelat(:,:) = Mrelat(:,:)/partic(a_gatld12,ip,iesp)**2

       Mrelat(:,:) = (1/partic(a_gatld12,ip,iesp))*(identity - Mrelat)

!
!      Calcul de deltaX
!     --------------------------------------
       deltaX(:) = dt_eff*matmul(Mrelat(:,:),deltaU(:))

!
!      Mise � jour des impulsions, des vitesses et des coordonnees
!      de la particule n
!     --------------------------------------
       partic(a_px,ip,iesp) = partic(a_pxtld,ip,iesp)+deltaU(1)
       partic(a_py,ip,iesp) = partic(a_pytld,ip,iesp)+deltaU(2)
       partic(a_pz,ip,iesp) = partic(a_pztld,ip,iesp)+deltaU(3)

       gam_p1s2 = SQRT(1+ partic(a_px,ip,iesp)**2 +
     &    partic(a_py,ip,iesp)**2 + partic(a_pz,ip,iesp)**2)

!       vxe(n) = vxe(n) + deltaX(1)/dt
!       vye(n) = vye(n) + deltaX(2)/dt
!       vze(n) = vze(n) + deltaX(3)/dt

       vxe = partic(a_px,ip,iesp)/gam_p1s2
       vye = partic(a_py,ip,iesp)/gam_p1s2
       vze = partic(a_pz,ip,iesp)/gam_p1s2

 !     calcul de x au temps n+1
 !     -------------------------
       partic(a_qx,ip,iesp)=partic(a_qx,ip,iesp)+deltaX(1)
       partic(a_qy,ip,iesp)=partic(a_qy,ip,iesp)+deltaX(2)

!
!     calcul de l acceleration au temps n+1
!     -------------------------------------
       partic(a_gaxe,ip,iesp) =
     &     rqm*thetad_2(ipx(iptarf),ipy(iptarf))*ex1 +
     &    (1-thetad_2(ipx(iptarf),ipy(iptarf)))*partic(a_paxe,ip,iesp)
       partic(a_gaye,ip,iesp) =
     &     rqm*thetad_2(ipx(iptarf),ipy(iptarf))*ey1 +
     &    (1-thetad_2(ipx(iptarf),ipy(iptarf)))*partic(a_paye,ip,iesp)
       partic(a_gaze,ip,iesp) =
     &     rqm*thetad_2(ipx(iptarf),ipy(iptarf))*ez1 +
     &    (1-thetad_2(ipx(iptarf),ipy(iptarf)))*partic(a_paze,ip,iesp)

       partic(a_paxe,ip,iesp) =
     &     rqm*(1-thetad_2(ipx(iptarf),ipy(iptarf)))*ex1 +
     &     thetad_2(ipx(iptarf),ipy(iptarf))*partic(a_paxe,ip,iesp)
       partic(a_paye,ip,iesp) =
     &     rqm*(1-thetad_2(ipx(iptarf),ipy(iptarf)))*ey1 +
     &     thetad_2(ipx(iptarf),ipy(iptarf))*partic(a_paye,ip,iesp)
       partic(a_paze,ip,iesp) =
     &     rqm*(1-thetad_2(ipx(iptarf),ipy(iptarf)))*ez1 +
     &     thetad_2(ipx(iptarf),ipy(iptarf))*partic(a_paze,ip,iesp)


 !
 !     energie cinetique au temps n+1
 !     ------------------------------
       sumx=sumx+vxe*vxe
       sumy=sumy+vye*vye
       sumz=sumz+vze*vze

       ENDDO

       ENDIF   ! IF(col_test.EQ.0)


! --- + Diagnostics temporels a chaque pas de temps : diag_tp_impl2d
! --- diags de impl2d
!        CALL diag_tp_impl2d(iesp)

!      CALL cndlim_reinje_relat(dwx,dwy,dwz)
       CALL cndlim_part(iesp)


! --- + Tri spatial des particules
       IF(per_tri(iesp).NE.0) THEN
          IF(MOD(iter,per_tri(iesp)).EQ.0) THEN
            CALL tri_part(iesp)
          ENDIF
       ENDIF

!
!**********************************************************************
!

 ! --- On remet � z�ro les courants rj[xyz]e sur la grille
       cjx_esp_2(:,:,iesp) = 0.
       cjy_esp_2(:,:,iesp) = 0.
       cjz_esp_2(:,:,iesp) = 0.
! --- On remet � z�ro les courants rj[xyz]e sur la grille
       rho_esp_2(:,:,iesp) = 0.
       dgx_esp_2(:,:,iesp) = 0.
       dgy_esp_2(:,:,iesp) = 0.
       dgz_esp_2(:,:,iesp) = 0.

       IF(max_cycles.GT.1) THEN
         xxx_esp(:,:,iesp)=0.  
         xxy_esp(:,:,iesp)=0.  
         xxz_esp(:,:,iesp)=0.
         xyx_esp(:,:,iesp)=0.  
         xyy_esp(:,:,iesp)=0.  
         xyz_esp(:,:,iesp)=0.
         xzx_esp(:,:,iesp)=0.  
         xzy_esp(:,:,iesp)=0.  
         xzz_esp(:,:,iesp)=0.

         zxx_esp(:,:,iesp)=0.  
         zxy_esp(:,:,iesp)=0.  
         zxz_esp(:,:,iesp)=0.
         zyx_esp(:,:,iesp)=0.  
         zyy_esp(:,:,iesp)=0.  
         zyz_esp(:,:,iesp)=0.
         zzx_esp(:,:,iesp)=0.  
         zzy_esp(:,:,iesp)=0.  
         zzz_esp(:,:,iesp)=0.            
       ENDIF

! ----- Calcul du gradient du champ electrique a l instant (n*dt)
       IF(simplified.EQ.2 .AND. iesp.EQ.1) THEN
         CALL calcgradE
       ENDIF

       IF(col_test.EQ.0) THEN

 !     prepush du pas suivant
 !     ----------------------
       DO ip=1,nbpa(iesp)

! ----- determination de gammatild(t0) de la particule n
        vtempe(1) = partic(a_px,ip,iesp) + 0.25*dt_eff*
     &    partic(a_gaxe,ip,iesp)
        vtempe(2) = partic(a_py,ip,iesp) + 0.25*dt_eff*
     &    partic(a_gaye,ip,iesp)
        vtempe(3) = partic(a_pz,ip,iesp) + 0.25*dt_eff*
     &    partic(a_gaze,ip,iesp)

        partic(a_gatld,ip,iesp)= sqrt(1 + vtempe(1)**2
     &     + vtempe(2)**2 + vtempe(3)**2)

        SELECT CASE(ordre_interp)
         CASE(1)
         CALL ponder_ordre1(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(2)
         CALL ponder_ordre2(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(3)
         CALL ponder_ordre3(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(4)
         CALL ponder_ordre4(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE DEFAULT
         WRITE(fmsgo,*) 'pousseurs.f : mauvaise valeur ordre_interp'
       END SELECT

         CALL champa_2d_exyz1(ex1, ey1, ez1,
     &                ponpx, ponpy, ipx, ipy)
         CALL champa_2dx3dv_bxyz(ip, iesp,
     &                ponpx, ponpy, ipx, ipy)

! ----- variables matrice de rotation
       tbx = 0.5*rqm*dt_eff*partic(a_bxp,ip,iesp)
     &  /partic(a_gatld,ip,iesp)
       tby = 0.5*rqm*dt_eff*partic(a_byp,ip,iesp)
     &  /partic(a_gatld,ip,iesp)
       tbz = 0.5*rqm*dt_eff*partic(a_bzp,ip,iesp)
     &  /partic(a_gatld,ip,iesp)

       teta2 = tbx**2 + tby**2 + tbz**2

       rotmat(1,1) = 1-teta2+2*tbx**2
       rotmat(1,2) = 2*tbz + 2*tbx*tby
       rotmat(1,3) = -2*tby +2*tbx*tbz

       rotmat(2,1) = 2*tbx*tby - 2*tbz
       rotmat(2,2) = 1-teta2 + 2*tby**2
       rotmat(2,3) = 2*tby*tbz + 2*tbx

       rotmat(3,1) = 2*tby + 2*tbx*tbz
       rotmat(3,2) = 2*tby*tbz - 2*tbx
       rotmat(3,3) = 1-teta2 + 2*tbz**2

       rotmat(:,:) = 1/(1+teta2) * rotmat(:,:)

! ---- determination de utild(n+1/2)
       vtempe(1)= partic(a_px,ip,iesp)
     &    + 0.25*dt_eff*partic(a_gaxe,ip,iesp)
       vtempe(2)= partic(a_py,ip,iesp)
     &    + 0.25*dt_eff*partic(a_gaye,ip,iesp)
       vtempe(3)= partic(a_pz,ip,iesp)
     &    + 0.25*dt_eff*partic(a_gaze,ip,iesp)

       gave(1)=partic(a_gaxe,ip,iesp);
       gave(2)=partic(a_gaye,ip,iesp);
       gave(3)=partic(a_gaze,ip,iesp)

       utild(1:3) = matmul(rotmat,vtempe) + 0.25*dt_eff*gave(1:3)

! ---- definition de gammatild(n+1/2)
       partic(a_gatld12,ip,iesp) =
     &   SQRT(1+utild(1)**2+utild(2)**2+utild(3)**2)

! ---- determination de vtild(n+1/2)
       partic(a_pxtld,ip,iesp)=utild(1)
       partic(a_pytld,ip,iesp)=utild(2)
       partic(a_pztld,ip,iesp)=utild(3)

       vxe = partic(a_pxtld,ip,iesp)/partic(a_gatld12,ip,iesp)
       vye = partic(a_pytld,ip,iesp)/partic(a_gatld12,ip,iesp)
       vze = partic(a_pztld,ip,iesp)/partic(a_gatld12,ip,iesp)

!
!      Matrice Mrelat pour le calcul de chi et zeta
!     ---------------------------------------------
       Mrelat(1,1) = partic(a_pxtld,ip,iesp)**2
       Mrelat(1,2) = partic(a_pxtld,ip,iesp)*partic(a_pytld,ip,iesp)
       Mrelat(1,3) = partic(a_pxtld,ip,iesp)*partic(a_pztld,ip,iesp)

       Mrelat(2,1) = partic(a_pytld,ip,iesp)*partic(a_pxtld,ip,iesp)
       Mrelat(2,2) = partic(a_pytld,ip,iesp)**2
       Mrelat(2,3) = partic(a_pytld,ip,iesp)*partic(a_pztld,ip,iesp)

       Mrelat(3,1) = partic(a_pztld,ip,iesp)*partic(a_pxtld,ip,iesp)
       Mrelat(3,2) = partic(a_pztld,ip,iesp)*partic(a_pytld,ip,iesp)
       Mrelat(3,3) = partic(a_pztld,ip,iesp)**2

       Mrelat(:,:) = Mrelat(:,:)/partic(a_gatld12,ip,iesp)**2

       Mrelat(:,:) = (1/partic(a_gatld12,ip,iesp))*(identity - Mrelat)

       aux_rh = partic(a_po,ip,iesp)


       CALL projpa_2dx3dv_rjxyz(iesp, ponpx, ponpy,
     &                ipx, ipy, vxe, vye, vze, aux_rh)

 !
 !     calcul de x au temps n+1
 !     -------------------------
       partic(a_qx,ip,iesp)=partic(a_qx,ip,iesp)+dt_eff*vxe
       partic(a_qy,ip,iesp)=partic(a_qy,ip,iesp)+dt_eff*vye

       IF(partic(a_qy,ip,iesp).GE.bord(4,numproc)) THEN
         partic(a_qy,ip,iesp) =
     &        partic(a_qy,ip,iesp) -ylong
       ELSEIF(partic(a_qy,ip,iesp).LE.bord(3,numproc)) THEN
         partic(a_qy,ip,iesp) =
     &        partic(a_qy,ip,iesp) +ylong
       ENDIF

 !
 !     calcul de rhoe en (x n+1) et j(x n+1,v n+1/2)
 !     ---------------------------------------------
       SELECT CASE(ordre_interp)
         CASE(1)
         CALL ponder_ordre1(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(2)
         CALL ponder_ordre2(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(3)
         CALL ponder_ordre3(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(4)
         CALL ponder_ordre4(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE DEFAULT
         WRITE(fmsgo,*) 'pousseurs.f : ordre_interp incorrecte'
       END SELECT

       CALL projpa_2dx3dv_dgxyz(iesp, ponpx, ponpy,
     &                ipx, ipy, vxe, vye, vze, aux_rh)

       CALL champa_2d_exyz1(ex1, ey1, ez1,
     &                ponpx, ponpy, ipx, ipy)
       CALL champa_2dx3dv_bxyz(ip, iesp,
     &                ponpx, ponpy, ipx, ipy)
       IF(simplified.EQ.2) THEN
         CALL champa_2d_gradE(gradE, ponpx, ponpy, ipx, ipy)
       ENDIF

! ----- variables matrice de rotation
       tbx = 0.5*rqm*dt_eff*partic(a_bxp,ip,iesp)
     &  /partic(a_gatld,ip,iesp)
       tby = 0.5*rqm*dt_eff*partic(a_byp,ip,iesp)
     &  /partic(a_gatld,ip,iesp)
       tbz = 0.5*rqm*dt_eff*partic(a_bzp,ip,iesp)
     &  /partic(a_gatld,ip,iesp)

       teta2 = tbx**2 + tby**2 + tbz**2

       rotmat(1,1) = 1-teta2+2*tbx**2
       rotmat(1,2) = 2*tbz + 2*tbx*tby
       rotmat(1,3) = -2*tby +2*tbx*tbz

       rotmat(2,1) = 2*tbx*tby - 2*tbz
       rotmat(2,2) = 1-teta2 + 2*tby**2
       rotmat(2,3) = 2*tby*tbz + 2*tbx

       rotmat(3,1) = 2*tby + 2*tbx*tbz
       rotmat(3,2) = 2*tby*tbz - 2*tbx
       rotmat(3,3) = 1-teta2 + 2*tbz**2

       rotmat(:,:) = 1/(1+teta2) * rotmat(:,:)

!
!     definition des coefficients de la matrice Umatstar pour electron
! ----------------------------------------------------------
       IF(simplified.EQ.0) THEN
       Umatstar(1,1) = ((partic(a_py,ip,iesp)+
     &   partic(a_pytld,ip,iesp))*partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*(partic(a_px,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umatstar(1,2) = ((partic(a_py,ip,iesp)+
     &   partic(a_pytld,ip,iesp))*partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*(partic(a_py,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umatstar(1,3) = ((partic(a_py,ip,iesp)+
     &   partic(a_pytld,ip,iesp))*partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*(partic(a_pz,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umatstar(2,1) = ((partic(a_pz,ip,iesp)+
     &   partic(a_pztld,ip,iesp))*partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*(partic(a_px,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umatstar(2,2) = ((partic(a_pz,ip,iesp)+
     &   partic(a_pztld,ip,iesp))*partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*(partic(a_py,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umatstar(2,3) = ((partic(a_pz,ip,iesp)+
     &   partic(a_pztld,ip,iesp))*partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*(partic(a_pz,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umatstar(3,1) = ((partic(a_px,ip,iesp)+
     &   partic(a_pxtld,ip,iesp))*partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*(partic(a_px,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umatstar(3,2) = ((partic(a_px,ip,iesp)+
     &   partic(a_pxtld,ip,iesp))*partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*(partic(a_py,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umatstar(3,3) = ((partic(a_px,ip,iesp)+
     &   partic(a_pxtld,ip,iesp))*partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*(partic(a_pz,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umatstar(:,:)= 0.25*rqm
     &    *(dt_eff/partic(a_gatld,ip,iesp)**3) *Umatstar(:,:)
       Umatstar(:,:)= identity(:,:) - Umatstar(:,:)
! ---------------------------
!        Forme simplifiee (gamma_n approxime a gamma_tild_n)
!        avec prise en compte des gradients du champ electrique
! ---------------------------
       ELSEIF(simplified.EQ.2) THEN
         Am33(:,:)= 0.25*rqm*dt_eff*matmul((identity+rotmat),gradE)
         Am33(:,:)= identity - Am33(:,:)
         CALL invmat33(Am33)

         Umatstar(:,:) = identity(:,:)

!        Forme simplifiee (gamma_n approxime a gamma_tild_n)
       ELSE
         Umatstar(:,:)= identity(:,:)
       ENDIF

       IF(simplified.EQ.2) THEN
         Mrelat(:,:) = matmul(Am33,Mrelat)
       ENDIF
       chimat(1:3,1:3) = matmul(Mrelat, Identity+rotmat)
       chimat(1:3,1:3) = matmul(chimat,Umatstar)
       chimat(:,:)=0.25*charge(iesp)**2/masse(iesp)*
     &     dt_eff**2 *chimat(:,:)

       zetamat(1,1) = vye*rotmat(3,1)     - vze*rotmat(2,1)
       zetamat(1,2) = vye*rotmat(3,2)     - vze*(1+rotmat(2,2))
       zetamat(1,3) = vye*(1+rotmat(3,3)) - vze*rotmat(2,3)

       zetamat(2,1) = vze*(1+rotmat(1,1)) - vxe*rotmat(3,1)
       zetamat(2,2) = vze*rotmat(1,2)     - vxe*rotmat(3,2)
       zetamat(2,3) = vze*rotmat(1,3)     - vxe*(1+rotmat(3,3))

       zetamat(3,1) = vxe*rotmat(2,1)     - vye*(1+rotmat(1,1))
       zetamat(3,2) = vxe*(1+rotmat(2,2)) - vye*rotmat(1,2)
       zetamat(3,3) = vxe*rotmat(2,3)     - vye*rotmat(1,3)

       zetamat(:,:) = matmul(zetamat, Umatstar)
       zetamat(:,:) = 0.125*charge(iesp)**2/masse(iesp)*
     &        dt_eff**2/partic(a_gatld12,ip,iesp)*zetamat(:,:)


!         CALL kizeta_rel(sd1, sd2, sd3, sd4, ipmx, ipmy,
!     &                       chimat, zetamat, iesp)
       IF(max_cycles.GT.1) THEN
         CALL kizeta_rel_ordn_sub(ponpx, ponpy, ipx, ipy,
     &          chimat, zetamat, iesp, partic(a_po,ip,iesp))
       ELSE
         CALL kizeta_rel_ordn(ponpx, ponpy, ipx, ipy,
     &          chimat, zetamat, iesp, partic(a_po,ip,iesp))
       ENDIF

       IF(debug.GE.4) THEN
         SELECT CASE(ordre_interp)
           CASE(1)
!           CALL kizeta_components(sd1, sd2, sd3, sd4, ipmx, ipmy,
!     &                  chir1, chir2, zetar1, iesp)
           CASE(2)

           CASE DEFAULT
           WRITE(fmsgo,*) 'pousseurs.f : ordre_interp incorrect',
     &         'appel kizeta_rel'
         END SELECT
       ENDIF

       ENDDO

       ENDIF  ! IF(col_test.EQ.0)


       return
!      ____________________
       END SUBROUTINE push_rel_thetad


       SUBROUTINE prpush_rel_ttafix(iesp)
! ======================================================================
!
!
!
!
!
! ======================================================================

       USE domaines
       USE domaines_klder
       USE particules
       USE particules_klder
       USE champs
       USE collisions
       USE temps
       USE erreurs

       IMPLICIT NONE


       INTEGER    ::   i, j
       INTEGER    ::   ip

!       REAL(kind=kr)     ::  xint, yint

       REAL(kind=kr)   ::  ex1, ey1, ez1

       INTEGER(KIND=kr), DIMENSION(1:ordre_interp+1)  :: ipx, ipy
       REAL(KIND=kr), DIMENSION(1:ordre_interp+1)     :: ponpx, ponpy

       REAL(kind=kr), DIMENSION(1:3,1:3)     ::
     &  rotmat, identity

       REAL(kind=kr), DIMENSION(1:3,1:3)     ::
     &  Umatstar, chimat, zetamat, Mrelat

       REAL(kind=kr), DIMENSION(1:3)     ::
     &  vtempe, gave, utild

       REAL(kind=kr)   ::  vxe, vye, vze
       REAL(kind=kr)   ::  rqm, aux_rh
       REAL(kind=kr)   ::  dt_eff

       INTEGER, INTENT(IN)     ::   iesp

       REAL(kind=kr)   :: tbx, tby, tbz, teta2

! ---- matrices pour diagnostics tenseurs relativistes
       REAL(kind=kr), DIMENSION(1:3,1:3)     ::
     &   chir1, chir2, zetar1


       identity(1,1)=1. ; identity(1,2)=0. ; identity(1,3)=0.
       identity(2,1)=0. ; identity(2,2)=1. ; identity(2,3)=0.
       identity(3,1)=0. ; identity(3,2)=0. ; identity(3,3)=1.

! --- On remet � z�ro les courants rj[xyz]e sur la grille
       cjx_esp_2(:,:,iesp) = 0.
       cjy_esp_2(:,:,iesp) = 0.
       cjz_esp_2(:,:,iesp) = 0.
! --- On remet � z�ro les courants rj[xyz]e sur la grille
       rho_esp_2(:,:,iesp) = 0.
       dgx_esp_2(:,:,iesp) = 0.
       dgy_esp_2(:,:,iesp) = 0.
       dgz_esp_2(:,:,iesp) = 0.

       IF(max_cycles.GT.1) THEN
         xxx_esp(:,:,iesp)=0.  
         xxy_esp(:,:,iesp)=0.  
         xxz_esp(:,:,iesp)=0.
         xyx_esp(:,:,iesp)=0.  
         xyy_esp(:,:,iesp)=0.  
         xyz_esp(:,:,iesp)=0.
         xzx_esp(:,:,iesp)=0.  
         xzy_esp(:,:,iesp)=0.  
         xzz_esp(:,:,iesp)=0.
         
         zxx_esp(:,:,iesp)=0.  
         zxy_esp(:,:,iesp)=0.  
         zxz_esp(:,:,iesp)=0.
         zyx_esp(:,:,iesp)=0.  
         zyy_esp(:,:,iesp)=0.  
         zyz_esp(:,:,iesp)=0.
         zzx_esp(:,:,iesp)=0.  
         zzy_esp(:,:,iesp)=0.  
         zzz_esp(:,:,iesp)=0.            
       ENDIF


       rqm = charge(iesp)/masse(iesp)
       dt_eff = dt*nbscycles(iesp)

! ----- Calcul du gradient du champ electrique a l instant (n*dt)
       IF(simplified.EQ.2 .AND. iesp.EQ.1) THEN
         CALL calcgradE
       ENDIF


       IF(col_test.EQ.0) THEN

 !     prepush initial
 !     ----------------------
       DO ip=1,nbpa(iesp)

! ----- determination de gammatild(t0) de la particule n
        vtempe(1) = partic(a_px,ip,iesp) +
     &     0.25*dt_eff*partic(a_gaxe,ip,iesp)
        vtempe(2) = partic(a_py,ip,iesp) +
     &     0.25*dt_eff*partic(a_gaye,ip,iesp)
        vtempe(3) = partic(a_pz,ip,iesp) +
     &     0.25*dt_eff*partic(a_gaze,ip,iesp)

        partic(a_gatld,ip,iesp)= sqrt(1 + vtempe(1)**2
     &     + vtempe(2)**2 + vtempe(3)**2)

        SELECT CASE(ordre_interp)
          CASE(1)
            CALL ponder_ordre1(ip, iesp, ponpx, ponpy, ipx, ipy)

          CASE(2)
            CALL ponder_ordre2(ip, iesp, ponpx, ponpy, ipx, ipy)

          CASE(3)
            CALL ponder_ordre3(ip, iesp, ponpx, ponpy, ipx, ipy)

          CASE(4)
            CALL ponder_ordre4(ip, iesp, ponpx, ponpy, ipx, ipy)

          CASE DEFAULT
            WRITE(fmsgo,*) 'pousseurs.f : mauvaise valeur ordre_interp'
        END SELECT

         CALL champa_2d_exyz1(ex1, ey1, ez1,
     &                ponpx, ponpy, ipx, ipy)
         CALL champa_2dx3dv_bxyz(ip, iesp,
     &                ponpx, ponpy, ipx, ipy)

 !
 !     initialisation de gaxe,gaye,gaze
 !
       IF(partic(a_qx,ip,iesp).LE.x_damp1 .OR.
     &    partic(a_qx,ip,iesp).GT.x_damp2 ) THEN
         partic(a_gaxe,ip,iesp) = rqm*thetads2*ex1
     &    + (1-thetads2)*partic(a_paxe,ip,iesp)
         partic(a_gaye,ip,iesp) = rqm*thetads2*ey1
     &    + (1-thetads2)*partic(a_paye,ip,iesp)
         partic(a_gaze,ip,iesp) = rqm*thetads2*ez1
     &    + (1-thetads2)*partic(a_paze,ip,iesp)
       ELSE
         partic(a_gaxe,ip,iesp) = rqm*ttapushs2*ex1
     &    + (1-ttapushs2)*partic(a_paxe,ip,iesp)
         partic(a_gaye,ip,iesp) = rqm*ttapushs2*ey1
     &    + (1-ttapushs2)*partic(a_paye,ip,iesp)
         partic(a_gaze,ip,iesp) = rqm*ttapushs2*ez1
     &    + (1-ttapushs2)*partic(a_paze,ip,iesp)
       ENDIF


! ----- variables matrice de rotation
       tbx = 0.5*rqm*dt_eff*partic(a_bxp,ip,iesp)
     &    /partic(a_gatld,ip,iesp)
       tby = 0.5*rqm*dt_eff*partic(a_byp,ip,iesp)
     &    /partic(a_gatld,ip,iesp)
       tbz = 0.5*rqm*dt_eff*partic(a_bzp,ip,iesp)
     &    /partic(a_gatld,ip,iesp)

       teta2 = tbx**2 + tby**2 + tbz**2

       rotmat(1,1) = 1-teta2+2*tbx**2
       rotmat(1,2) = 2*tbz + 2*tbx*tby
       rotmat(1,3) = -2*tby +2*tbx*tbz

       rotmat(2,1) = 2*tbx*tby - 2*tbz
       rotmat(2,2) = 1-teta2 + 2*tby**2
       rotmat(2,3) = 2*tby*tbz + 2*tbx

       rotmat(3,1) = 2*tby + 2*tbx*tbz
       rotmat(3,2) = 2*tby*tbz - 2*tbx
       rotmat(3,3) = 1-teta2 + 2*tbz**2

       rotmat(:,:) = 1/(1+teta2) * rotmat(:,:)

! ---- determination de utild(n+1/2)
       vtempe(1)= partic(a_px,ip,iesp)
     &    + 0.25*dt_eff*partic(a_gaxe,ip,iesp)
       vtempe(2)= partic(a_py,ip,iesp)
     &    + 0.25*dt_eff*partic(a_gaye,ip,iesp)
       vtempe(3)= partic(a_pz,ip,iesp)
     &    + 0.25*dt_eff*partic(a_gaze,ip,iesp)

       gave(1)=partic(a_gaxe,ip,iesp)
       gave(2)=partic(a_gaye,ip,iesp)
       gave(3)=partic(a_gaze,ip,iesp)

       utild(1:3) = matmul(rotmat,vtempe) + 0.25*dt_eff*gave(1:3)

! ---- definition de gammatild(n+1/2)
       partic(a_gatld12,ip,iesp) =
     &   SQRT(1+utild(1)**2+utild(2)**2+utild(3)**2)

! ---- determination de vtild(n+1/2)
       partic(a_pxtld,ip,iesp)=utild(1)
       partic(a_pytld,ip,iesp)=utild(2)
       partic(a_pztld,ip,iesp)=utild(3)

       vxe = partic(a_pxtld,ip,iesp)/partic(a_gatld12,ip,iesp)
       vye = partic(a_pytld,ip,iesp)/partic(a_gatld12,ip,iesp)
       vze = partic(a_pztld,ip,iesp)/partic(a_gatld12,ip,iesp)

!
!      Matrice Mrelat pour le calcul de chi et zeta
!     ---------------------------------------------
       Mrelat(1,1) = partic(a_pxtld,ip,iesp)**2
       Mrelat(1,2) = partic(a_pxtld,ip,iesp)*partic(a_pytld,ip,iesp)
       Mrelat(1,3) = partic(a_pxtld,ip,iesp)*partic(a_pztld,ip,iesp)

       Mrelat(2,1) = partic(a_pytld,ip,iesp)*partic(a_pxtld,ip,iesp)
       Mrelat(2,2) = partic(a_pytld,ip,iesp)**2
       Mrelat(2,3) = partic(a_pytld,ip,iesp)*partic(a_pztld,ip,iesp)

       Mrelat(3,1) = partic(a_pztld,ip,iesp)*partic(a_pxtld,ip,iesp)
       Mrelat(3,2) = partic(a_pztld,ip,iesp)*partic(a_pytld,ip,iesp)
       Mrelat(3,3) = partic(a_pztld,ip,iesp)**2

       Mrelat(:,:) = Mrelat(:,:)/partic(a_gatld12,ip,iesp)**2

       Mrelat(:,:) = (1/partic(a_gatld12,ip,iesp))*(identity - Mrelat)

       aux_rh = partic(a_po,ip,iesp)

       CALL projpa_2dx3dv_rjxyz(iesp, ponpx, ponpy,
     &              ipx, ipy, vxe, vye, vze, aux_rh)

 !
 !     calcul de xtild(n+1)
 !     -------------------------
       partic(a_qx,ip,iesp)=partic(a_qx,ip,iesp)+dt_eff*vxe
       partic(a_qy,ip,iesp)=partic(a_qy,ip,iesp)+dt_eff*vye

       IF(partic(a_qy,ip,iesp).GE.bord(4,numproc)) THEN
         partic(a_qy,ip,iesp) =
     &        partic(a_qy,ip,iesp) -ylong
       ELSEIF(partic(a_qy,ip,iesp).LE.bord(3,numproc)) THEN
         partic(a_qy,ip,iesp) =
     &        partic(a_qy,ip,iesp) +ylong
       ENDIF

 !
 !     calcul de rhoe en (x n+1) et j(x n+1,v n+1/2)
 !     ---------------------------------------------
       SELECT CASE(ordre_interp)
         CASE(1)
         CALL ponder_ordre1(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(2)
         CALL ponder_ordre2(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(3)
         CALL ponder_ordre3(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(4)
         CALL ponder_ordre4(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE DEFAULT
         WRITE(fmsgo,*) 'pousseurs.f : ordre_interp incorrecte'
       END SELECT

       CALL projpa_2dx3dv_dgxyz(iesp, ponpx, ponpy,
     &                ipx, ipy, vxe, vye, vze, aux_rh)

       CALL champa_2d_exyz1(ex1, ey1, ez1,
     &                ponpx, ponpy, ipx, ipy)
       CALL champa_2dx3dv_bxyz(ip, iesp,
     &                ponpx, ponpy, ipx, ipy)

! ----- variables matrice de rotation
       tbx = 0.5*rqm*dt_eff*partic(a_bxp,ip,iesp)
     &    /partic(a_gatld,ip,iesp)
       tby = 0.5*rqm*dt_eff*partic(a_byp,ip,iesp)
     &    /partic(a_gatld,ip,iesp)
       tbz = 0.5*rqm*dt_eff*partic(a_bzp,ip,iesp)
     &    /partic(a_gatld,ip,iesp)

       teta2 = tbx**2 + tby**2 + tbz**2

       rotmat(1,1) = 1-teta2+2*tbx**2
       rotmat(1,2) = 2*tbz + 2*tbx*tby
       rotmat(1,3) = -2*tby +2*tbx*tbz

       rotmat(2,1) = 2*tbx*tby - 2*tbz
       rotmat(2,2) = 1-teta2 + 2*tby**2
       rotmat(2,3) = 2*tby*tbz + 2*tbx

       rotmat(3,1) = 2*tby + 2*tbx*tbz
       rotmat(3,2) = 2*tby*tbz - 2*tbx
       rotmat(3,3) = 1-teta2 + 2*tbz**2

       rotmat(:,:) = 1/(1+teta2) * rotmat(:,:)

!
!     definition des coefficients de la matrice Umatstar pour electron
! ----------------------------------------------------------
       IF(simplified.EQ.0) THEN
       Umatstar(1,1) = ((partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))
     &   *partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*
     &  (partic(a_px,ip,iesp)+0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umatstar(1,2) = ((partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))
     &   *partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*
     &  (partic(a_py,ip,iesp)+0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umatstar(1,3) = ((partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))
     &   *partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*
     &  (partic(a_pz,ip,iesp)+0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umatstar(2,1) = ((partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))
     &  *partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*
     &  (partic(a_px,ip,iesp)+0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umatstar(2,2) = ((partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))
     &  *partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*
     &  (partic(a_py,ip,iesp)+0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umatstar(2,3) = ((partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))
     &  *partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*
     &  (partic(a_pz,ip,iesp)+0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umatstar(3,1) = ((partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))
     &  *partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*
     &  (partic(a_px,ip,iesp)+0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umatstar(3,2) = ((partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))
     &  *partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*
     &  (partic(a_py,ip,iesp)+0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umatstar(3,3) = ((partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))
     &  *partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*
     &  (partic(a_pz,ip,iesp)+0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umatstar(:,:) = 0.25*rqm
     &    *(dt_eff/partic(a_gatld,ip,iesp)**3)*Umatstar(:,:)
       Umatstar(:,:) = identity(:,:) - Umatstar(:,:)

! ------ Forme simplifiee (gamma_n approxime a gamma_tild_n)
       ELSE
         Umatstar(:,:) = identity(:,:)
       ENDIF


! ----- d�termination des tenseurs de susceptibilit� pour la paricule n
       chimat(1:3,1:3) = matmul(Mrelat, Identity+rotmat)
       chimat(1:3,1:3) = matmul(chimat,Umatstar)
       chimat(:,:) = 0.25*charge(iesp)**2/masse(iesp)*
     &      dt_eff**2 *chimat(:,:)

       zetamat(1,1) = vye*rotmat(3,1)     - vze*rotmat(2,1)
       zetamat(1,2) = vye*rotmat(3,2)     - vze*(1+rotmat(2,2))
       zetamat(1,3) = vye*(1+rotmat(3,3)) - vze*rotmat(2,3)

       zetamat(2,1) = vze*(1+rotmat(1,1)) - vxe*rotmat(3,1)
       zetamat(2,2) = vze*rotmat(1,2)     - vxe*rotmat(3,2)
       zetamat(2,3) = vze*rotmat(1,3)     - vxe*(1+rotmat(3,3))

       zetamat(3,1) = vxe*rotmat(2,1)     - vye*(1+rotmat(1,1))
       zetamat(3,2) = vxe*(1+rotmat(2,2)) - vye*rotmat(1,2)
       zetamat(3,3) = vxe*rotmat(2,3)     - vye*rotmat(1,3)

       zetamat(:,:) = matmul(zetamat, Umatstar)
       zetamat(:,:) = 0.125*charge(iesp)**2/masse(iesp)*dt_eff**2
     &    /partic(a_gatld12,ip,iesp)*zetamat(:,:)


       IF(max_cycles.GT.1) THEN
         CALL kizeta_rel_ordn_sub(ponpx, ponpy, ipx, ipy,
     &          chimat, zetamat, iesp, partic(a_po,ip,iesp))
       ELSE
         CALL kizeta_rel_ordn(ponpx, ponpy, ipx, ipy,
     &          chimat, zetamat, iesp, partic(a_po,ip,iesp))
       ENDIF


       IF(debug.GE.4) THEN
         SELECT CASE(ordre_interp)
           CASE(1)
!           CALL kizeta_components(sd1, sd2, sd3, sd4, ipmx, ipmy,
!     &                  chir1, chir2, zetar1, iesp)
           CASE(2)

           CASE DEFAULT
           WRITE(fmsgo,*) 'pousseurs.f : ordre_interp incorrect',
     &         'appel kizeta_rel'
         END SELECT
       ENDIF

       ENDDO

       ENDIF !  IF(col_test.EQ.0)


       return
!      ______________________
       END SUBROUTINE prpush_rel_ttafix




       SUBROUTINE push_rel_ttafix(iesp)
! ======================================================================
!
!
!
! ======================================================================

       USE domaines
       USE domaines_klder
       USE particules
       USE particules_klder
       USE champs
       USE champs_iter
       USE collisions
       USE temps
       USE diagnostics
       USE erreurs

       IMPLICIT NONE


       INTEGER    ::    i, j
       INTEGER   ::  ip

       INTEGER, INTENT(IN)    ::   iesp

       INTEGER(KIND=kr), DIMENSION(1:ordre_interp+1)  :: ipx, ipy
       REAL(KIND=kr), DIMENSION(1:ordre_interp+1)     :: ponpx, ponpy

       REAL(kind=kr), DIMENSION(1:3,1:3)     ::
     &  rotmat, Umat, Mrelat

       REAL(kind=kr), DIMENSION(1:3,1:3)     ::
     &  Umatstar, chimat, zetamat

       REAL(kind=kr), DIMENSION(1:3,1:3)     ::
     &  identity

       REAL(kind=kr), DIMENSION(1:3)     ::
     &  vtempe, gave, utild, Evect

       REAL(kind=kr), DIMENSION(1:3)     ::
     &  deltaU, deltaX

       REAL(kind=kr)   :: tbx, tby, tbz, teta2,
     &  gam_p1s2

       REAL(kind=kr)   :: rqm, aux_rh
       REAL(kind=kr)   ::  dt_eff

       REAL(kind=kr)   ::  vxe, vye, vze

       REAL(kind=kr)        ::
     &   sumx, sumy, sumz, dwx, dwy, dwz,
     &   ex1, ey1, ez1

! ---- matrices pour diagnostics tenseurs relativistes
       REAL(kind=kr), DIMENSION(1:3,1:3)     ::
     &   chir1, chir2, zetar1

       REAL(kind=kr), DIMENSION(1:3,1:3)    ::   gradE, Am33

       REAL(kind=kr)  ::   total


       identity(1,1)=1. ; identity(1,2)=0. ; identity(1,3)=0.
       identity(2,1)=0. ; identity(2,2)=1. ; identity(2,3)=0.
       identity(3,1)=0. ; identity(3,2)=0. ; identity(3,3)=1.

       rqm = charge(iesp)/masse(iesp)
       dt_eff = dt*nbscycles(iesp)

       sumx=0.
       sumy=0.
       sumz=0.
       dwx=0.
       dwy=0.
       dwz=0.


       IF(col_test.EQ.0) THEN

       DO ip=1,nbpa(iesp)
!     final push
!     ----------
!     on fait les corrections aux valeurs predites de x et v

!
!     interpolation du champ electrique t=(n+1)*dt au centre des particules
!     interpolation du gradient du champ electrique t=n*dt au centre des particules
!
       IF(iter_es.EQ.1) THEN
         SELECT CASE(ordre_interp)
           CASE(1)
           CALL ponder_ordre1_ik(ip, iesp, ponpx, ponpy, ipx, ipy)

           CASE(2)
           CALL ponder_ordre2_ik(ip, iesp, ponpx, ponpy, ipx, ipy)

           CASE(3)
           CALL ponder_ordre3_ik(ip, iesp, ponpx, ponpy, ipx, ipy)

           CASE(4)
           CALL ponder_ordre4_ik(ip, iesp, ponpx, ponpy, ipx, ipy)

           CASE DEFAULT
           write(fmsgo,*) 'pousseurs.f : valeur de ordre_interp ',
     &      'incorrecte '
         END SELECT
       ELSE
         SELECT CASE(ordre_interp)
           CASE(1)
           CALL ponder_ordre1(ip, iesp, ponpx, ponpy, ipx, ipy)

           CASE(2)
           CALL ponder_ordre2(ip, iesp, ponpx, ponpy, ipx, ipy)

           CASE(3)
           CALL ponder_ordre3(ip, iesp, ponpx, ponpy, ipx, ipy)

           CASE(4)
           CALL ponder_ordre4(ip, iesp, ponpx, ponpy, ipx, ipy)

           CASE DEFAULT
           write(fmsgo,*) 'pousseurs.f : valeur de ordre_interp ',
     &       'incorrecte '
         END SELECT
       ENDIF

       CALL champa_2d_exyz1(ex1, ey1, ez1,
     &                ponpx, ponpy, ipx, ipy)
!      on tient compte des it�rations pour calculer le champ magn�tique
!      vu par les particules
! -------------------
       IF(iter_es.EQ.1) THEN
         CALL champa_2dx3dv_bxyz(ip, iesp, ponpx, ponpy, ipx, ipy)
       ENDIF
       IF(simplified.EQ.2) THEN
         CALL champa_2d_gradE(gradE, ponpx, ponpy, ipx, ipy)
       ENDIF

       Evect(1)=ex1 ; Evect(2)=ey1 ; Evect(3)=ez1

! ----- variables matrice de rotation
       tbx = 0.5*rqm*dt_eff*
     &    partic(a_bxp,ip,iesp)/partic(a_gatld,ip,iesp)
       tby = 0.5*rqm*dt_eff*
     &    partic(a_byp,ip,iesp)/partic(a_gatld,ip,iesp)
       tbz = 0.5*rqm*dt_eff*
     &    partic(a_bzp,ip,iesp)/partic(a_gatld,ip,iesp)

       teta2 = tbx**2 + tby**2 + tbz**2

       rotmat(1,1) = 1-teta2+2*tbx**2
       rotmat(1,2) = 2*tbz + 2*tbx*tby
       rotmat(1,3) = -2*tby +2*tbx*tbz

       rotmat(2,1) = 2*tbx*tby - 2*tbz
       rotmat(2,2) = 1-teta2 + 2*tby**2
       rotmat(2,3) = 2*tby*tbz + 2*tbx

       rotmat(3,1) = 2*tby + 2*tbx*tbz
       rotmat(3,2) = 2*tby*tbz - 2*tbx
       rotmat(3,3) = 1-teta2 + 2*tbz**2

       rotmat(:,:) = 1/(1+teta2) * rotmat(:,:)

!
!     definition des coefficients de la matrice Umat
! ----------------------------------------------------------
       IF(simplified.EQ.0) THEN
       Umat(1,1) = ((partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*(partic(a_px,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umat(1,2) = ((partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*(partic(a_py,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umat(1,3) = ((partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*(partic(a_pz,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umat(2,1) = ((partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*(partic(a_px,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umat(2,2) = ((partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*(partic(a_py,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umat(2,3) = ((partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*(partic(a_pz,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umat(3,1) = ((partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*(partic(a_px,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umat(3,2) = ((partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*(partic(a_py,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umat(3,3) = ((partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*(partic(a_pz,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umat(:,:) = 0.25*rqm
     &              *(dt_eff/partic(a_gatld,ip,iesp)**3)*Umat(:,:)
       Umat(:,:) = identity(:,:) - Umat(:,:)
       Umat(:,:) = 0.25*rqm
     &              *dt_eff*matmul((identity+rotmat),Umat)
! ---------------------------
!        Forme simplifiee (gamma_n approxime a gamma_tild_n)
!        avec prise en compte des gradients du champ electrique
! ---------------------------
       ELSEIF(simplified.EQ.2) THEN
         Am33(:,:)= 0.25*rqm*dt_eff*matmul((identity+rotmat),gradE)
         Am33(:,:)= identity - Am33(:,:)
         CALL invmat33(Am33)

         Umat(1:3,1:3) = 0.
         Umat(:,:) = 0.25*rqm*dt_eff*(identity+rotmat)

         Umat(:,:) = matmul(Am33,Umat)

!        Forme simplifiee (gamma_n approxime a gamma_tild_n)
       ELSE
         Umat(1:3,1:3) = 0.
         Umat(:,:) = 0.25*rqm*dt_eff*(identity+rotmat)
       ENDIF

       deltaU(:) = matmul(Umat,Evect)

!
!      Matrice Mrelat pour le calcul de deltaX
!     ---------------------------------------------
       Mrelat(1,1) = partic(a_pxtld,ip,iesp)**2
       Mrelat(1,2) = partic(a_pxtld,ip,iesp)*partic(a_pytld,ip,iesp)
       Mrelat(1,3) = partic(a_pxtld,ip,iesp)*partic(a_pztld,ip,iesp)

       Mrelat(2,1) = partic(a_pytld,ip,iesp)*partic(a_pxtld,ip,iesp)
       Mrelat(2,2) = partic(a_pytld,ip,iesp)**2
       Mrelat(2,3) = partic(a_pytld,ip,iesp)*partic(a_pztld,ip,iesp)

       Mrelat(3,1) = partic(a_pztld,ip,iesp)*partic(a_pxtld,ip,iesp)
       Mrelat(3,2) = partic(a_pztld,ip,iesp)*partic(a_pytld,ip,iesp)
       Mrelat(3,3) = partic(a_pztld,ip,iesp)**2

       Mrelat(:,:) = Mrelat(:,:)/partic(a_gatld12,ip,iesp)**2

       Mrelat(:,:) = (1/partic(a_gatld12,ip,iesp))*(identity - Mrelat)

!
!      Calcul de deltaX
!     --------------------------------------
       deltaX(:) = dt_eff*matmul(Mrelat(:,:),deltaU(:))

!
!      Mise � jour des impulsions, des vitesses et des coordonnees
!      de la particule n
!     --------------------------------------
       partic(a_px,ip,iesp) = partic(a_pxtld,ip,iesp)+deltaU(1)
       partic(a_py,ip,iesp) = partic(a_pytld,ip,iesp)+deltaU(2)
       partic(a_pz,ip,iesp) = partic(a_pztld,ip,iesp)+deltaU(3)

       gam_p1s2 = SQRT(1+ partic(a_px,ip,iesp)**2 +
     &    partic(a_py,ip,iesp)**2 + partic(a_pz,ip,iesp)**2)

!       vxe(n) = vxe(n) + deltaX(1)/dt
!       vye(n) = vye(n) + deltaX(2)/dt
!       vze(n) = vze(n) + deltaX(3)/dt

       vxe = partic(a_px,ip,iesp)/gam_p1s2
       vye = partic(a_py,ip,iesp)/gam_p1s2
       vze = partic(a_pz,ip,iesp)/gam_p1s2

 !     calcul de x au temps n+1
 !     -------------------------
       partic(a_qx,ip,iesp)=partic(a_qx,ip,iesp)+deltaX(1)
       partic(a_qy,ip,iesp)=partic(a_qy,ip,iesp)+deltaX(2)

!
!     calcul de l acceleration au temps n+1
!     -------------------------------------

       IF(partic(a_qx,ip,iesp).LE.x_damp1 .OR.
     &    partic(a_qx,ip,iesp).GT.x_damp2 ) THEN
         partic(a_gaxe,ip,iesp) =
     &     rqm*thetads2*ex1 +
     &    (1-thetads2)*partic(a_paxe,ip,iesp)
         partic(a_gaye,ip,iesp) =
     &     rqm*thetads2*ey1 +
     &    (1-thetads2)*partic(a_paye,ip,iesp)
         partic(a_gaze,ip,iesp) =
     &     rqm*thetads2*ez1 +
     &    (1-thetads2)*partic(a_paze,ip,iesp)

         partic(a_paxe,ip,iesp) =
     &     rqm*(1-thetads2)*ex1 +
     &     thetads2*partic(a_paxe,ip,iesp)
         partic(a_paye,ip,iesp) =
     &     rqm*(1-thetads2)*ey1 +
     &     thetads2*partic(a_paye,ip,iesp)
         partic(a_paze,ip,iesp) =
     &     rqm*(1-thetads2)*ez1 +
     &     thetads2*partic(a_paze,ip,iesp)
       ELSE
         partic(a_gaxe,ip,iesp) =
     &     rqm*ttapushs2*ex1 +
     &    (1-ttapushs2)*partic(a_paxe,ip,iesp)
         partic(a_gaye,ip,iesp) =
     &     rqm*ttapushs2*ey1 +
     &    (1-ttapushs2)*partic(a_paye,ip,iesp)
         partic(a_gaze,ip,iesp) =
     &     rqm*ttapushs2*ez1 +
     &    (1-ttapushs2)*partic(a_paze,ip,iesp)

         partic(a_paxe,ip,iesp) =
     &     rqm*(1-ttapushs2)*ex1 +
     &     ttapushs2*partic(a_paxe,ip,iesp)
         partic(a_paye,ip,iesp) =
     &     rqm*(1-ttapushs2)*ey1 +
     &     ttapushs2*partic(a_paye,ip,iesp)
         partic(a_paze,ip,iesp) =
     &     rqm*(1-ttapushs2)*ez1 +
     &     ttapushs2*partic(a_paze,ip,iesp)
       ENDIF

 !
 !     energie cinetique au temps n+1
 !     ------------------------------
       sumx=sumx+vxe*vxe
       sumy=sumy+vye*vye
       sumz=sumz+vze*vze

       ENDDO

       ENDIF   ! IF(col_test.EQ.0)


! --- + Diagnostics temporels a chaque pas de temps : diag_tp_impl2d
! --- diags de impl2d
!        CALL diag_tp_impl2d(iesp)

!      CALL cndlim_reinje_relat(dwx,dwy,dwz)
       CALL cndlim_part(iesp)


! --- + Tri spatial des particules
       IF(per_tri(iesp).NE.0) THEN
          IF(MOD(iter,per_tri(iesp)).EQ.0) THEN
            CALL tri_part(iesp)
          ENDIF
       ENDIF

!
!**********************************************************************
!

 ! --- On remet � z�ro les courants rj[xyz]e sur la grille
       cjx_esp_2(:,:,iesp) = 0.
       cjy_esp_2(:,:,iesp) = 0.
       cjz_esp_2(:,:,iesp) = 0.
! --- On remet � z�ro les courants rj[xyz]e sur la grille
       rho_esp_2(:,:,iesp) = 0.
       dgx_esp_2(:,:,iesp) = 0.
       dgy_esp_2(:,:,iesp) = 0.
       dgz_esp_2(:,:,iesp) = 0.

       IF(max_cycles.GT.1) THEN
         xxx_esp(:,:,iesp)=0.  
         xxy_esp(:,:,iesp)=0.  
         xxz_esp(:,:,iesp)=0.
         xyx_esp(:,:,iesp)=0.  
         xyy_esp(:,:,iesp)=0.  
         xyz_esp(:,:,iesp)=0.
         xzx_esp(:,:,iesp)=0.  
         xzy_esp(:,:,iesp)=0.  
         xzz_esp(:,:,iesp)=0.

         zxx_esp(:,:,iesp)=0.  
         zxy_esp(:,:,iesp)=0.  
         zxz_esp(:,:,iesp)=0.
         zyx_esp(:,:,iesp)=0.  
         zyy_esp(:,:,iesp)=0.  
         zyz_esp(:,:,iesp)=0.
         zzx_esp(:,:,iesp)=0.  
         zzy_esp(:,:,iesp)=0.  
         zzz_esp(:,:,iesp)=0.            
       ENDIF

! ----- Calcul du gradient du champ electrique a l instant (n*dt)
       IF(simplified.EQ.2 .AND. iesp.EQ.1) THEN
         CALL calcgradE
       ENDIF

       IF(col_test.EQ.0) THEN

 !     prepush du pas suivant
 !     ----------------------
       DO ip=1,nbpa(iesp)

! ----- determination de gammatild(t0) de la particule n
        vtempe(1) = partic(a_px,ip,iesp) + 0.25*dt_eff*
     &    partic(a_gaxe,ip,iesp)
        vtempe(2) = partic(a_py,ip,iesp) + 0.25*dt_eff*
     &    partic(a_gaye,ip,iesp)
        vtempe(3) = partic(a_pz,ip,iesp) + 0.25*dt_eff*
     &    partic(a_gaze,ip,iesp)

        partic(a_gatld,ip,iesp)= sqrt(1 + vtempe(1)**2
     &     + vtempe(2)**2 + vtempe(3)**2)

        SELECT CASE(ordre_interp)
         CASE(1)
         CALL ponder_ordre1(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(2)
         CALL ponder_ordre2(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(3)
         CALL ponder_ordre3(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(4)
         CALL ponder_ordre4(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE DEFAULT
         WRITE(fmsgo,*) 'pousseurs.f : mauvaise valeur ordre_interp'
       END SELECT

         CALL champa_2d_exyz1(ex1, ey1, ez1,
     &                ponpx, ponpy, ipx, ipy)
         CALL champa_2dx3dv_bxyz(ip, iesp,
     &                ponpx, ponpy, ipx, ipy)

! ----- variables matrice de rotation
       tbx = 0.5*rqm*dt_eff*partic(a_bxp,ip,iesp)
     &  /partic(a_gatld,ip,iesp)
       tby = 0.5*rqm*dt_eff*partic(a_byp,ip,iesp)
     &  /partic(a_gatld,ip,iesp)
       tbz = 0.5*rqm*dt_eff*partic(a_bzp,ip,iesp)
     &  /partic(a_gatld,ip,iesp)

       teta2 = tbx**2 + tby**2 + tbz**2

       rotmat(1,1) = 1-teta2+2*tbx**2
       rotmat(1,2) = 2*tbz + 2*tbx*tby
       rotmat(1,3) = -2*tby +2*tbx*tbz

       rotmat(2,1) = 2*tbx*tby - 2*tbz
       rotmat(2,2) = 1-teta2 + 2*tby**2
       rotmat(2,3) = 2*tby*tbz + 2*tbx

       rotmat(3,1) = 2*tby + 2*tbx*tbz
       rotmat(3,2) = 2*tby*tbz - 2*tbx
       rotmat(3,3) = 1-teta2 + 2*tbz**2

       rotmat(:,:) = 1/(1+teta2) * rotmat(:,:)

! ---- determination de utild(n+1/2)
       vtempe(1)= partic(a_px,ip,iesp)
     &    + 0.25*dt_eff*partic(a_gaxe,ip,iesp)
       vtempe(2)= partic(a_py,ip,iesp)
     &    + 0.25*dt_eff*partic(a_gaye,ip,iesp)
       vtempe(3)= partic(a_pz,ip,iesp)
     &    + 0.25*dt_eff*partic(a_gaze,ip,iesp)

       gave(1)=partic(a_gaxe,ip,iesp);
       gave(2)=partic(a_gaye,ip,iesp);
       gave(3)=partic(a_gaze,ip,iesp)

       utild(1:3) = matmul(rotmat,vtempe) + 0.25*dt_eff*gave(1:3)

! ---- definition de gammatild(n+1/2)
       partic(a_gatld12,ip,iesp) =
     &   SQRT(1+utild(1)**2+utild(2)**2+utild(3)**2)

! ---- determination de vtild(n+1/2)
       partic(a_pxtld,ip,iesp)=utild(1)
       partic(a_pytld,ip,iesp)=utild(2)
       partic(a_pztld,ip,iesp)=utild(3)

       vxe = partic(a_pxtld,ip,iesp)/partic(a_gatld12,ip,iesp)
       vye = partic(a_pytld,ip,iesp)/partic(a_gatld12,ip,iesp)
       vze = partic(a_pztld,ip,iesp)/partic(a_gatld12,ip,iesp)

!
!      Matrice Mrelat pour le calcul de chi et zeta
!     ---------------------------------------------
       Mrelat(1,1) = partic(a_pxtld,ip,iesp)**2
       Mrelat(1,2) = partic(a_pxtld,ip,iesp)*partic(a_pytld,ip,iesp)
       Mrelat(1,3) = partic(a_pxtld,ip,iesp)*partic(a_pztld,ip,iesp)

       Mrelat(2,1) = partic(a_pytld,ip,iesp)*partic(a_pxtld,ip,iesp)
       Mrelat(2,2) = partic(a_pytld,ip,iesp)**2
       Mrelat(2,3) = partic(a_pytld,ip,iesp)*partic(a_pztld,ip,iesp)

       Mrelat(3,1) = partic(a_pztld,ip,iesp)*partic(a_pxtld,ip,iesp)
       Mrelat(3,2) = partic(a_pztld,ip,iesp)*partic(a_pytld,ip,iesp)
       Mrelat(3,3) = partic(a_pztld,ip,iesp)**2

       Mrelat(:,:) = Mrelat(:,:)/partic(a_gatld12,ip,iesp)**2

       Mrelat(:,:) = (1/partic(a_gatld12,ip,iesp))*(identity - Mrelat)

       aux_rh = partic(a_po,ip,iesp)


       CALL projpa_2dx3dv_rjxyz(iesp, ponpx, ponpy,
     &                ipx, ipy, vxe, vye, vze, aux_rh)

 !
 !     calcul de x au temps n+1
 !     -------------------------
       partic(a_qx,ip,iesp)=partic(a_qx,ip,iesp)+dt_eff*vxe
       partic(a_qy,ip,iesp)=partic(a_qy,ip,iesp)+dt_eff*vye

       IF(partic(a_qy,ip,iesp).GE.bord(4,numproc)) THEN
         partic(a_qy,ip,iesp) =
     &        partic(a_qy,ip,iesp) -ylong
       ELSEIF(partic(a_qy,ip,iesp).LE.bord(3,numproc)) THEN
         partic(a_qy,ip,iesp) =
     &        partic(a_qy,ip,iesp) +ylong
       ENDIF

 !
 !     calcul de rhoe en (x n+1) et j(x n+1,v n+1/2)
 !     ---------------------------------------------
       SELECT CASE(ordre_interp)
         CASE(1)
         CALL ponder_ordre1(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(2)
         CALL ponder_ordre2(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(3)
         CALL ponder_ordre3(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(4)
         CALL ponder_ordre4(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE DEFAULT
         WRITE(fmsgo,*) 'pousseurs.f : ordre_interp incorrecte'
       END SELECT

       CALL projpa_2dx3dv_dgxyz(iesp, ponpx, ponpy,
     &                ipx, ipy, vxe, vye, vze, aux_rh)

       CALL champa_2d_exyz1(ex1, ey1, ez1,
     &                ponpx, ponpy, ipx, ipy)
       CALL champa_2dx3dv_bxyz(ip, iesp,
     &                ponpx, ponpy, ipx, ipy)
       IF(simplified.EQ.2) THEN
         CALL champa_2d_gradE(gradE, ponpx, ponpy, ipx, ipy)
       ENDIF

! ----- variables matrice de rotation
       tbx = 0.5*rqm*dt_eff*partic(a_bxp,ip,iesp)
     &  /partic(a_gatld,ip,iesp)
       tby = 0.5*rqm*dt_eff*partic(a_byp,ip,iesp)
     &  /partic(a_gatld,ip,iesp)
       tbz = 0.5*rqm*dt_eff*partic(a_bzp,ip,iesp)
     &  /partic(a_gatld,ip,iesp)

       teta2 = tbx**2 + tby**2 + tbz**2

       rotmat(1,1) = 1-teta2+2*tbx**2
       rotmat(1,2) = 2*tbz + 2*tbx*tby
       rotmat(1,3) = -2*tby +2*tbx*tbz

       rotmat(2,1) = 2*tbx*tby - 2*tbz
       rotmat(2,2) = 1-teta2 + 2*tby**2
       rotmat(2,3) = 2*tby*tbz + 2*tbx

       rotmat(3,1) = 2*tby + 2*tbx*tbz
       rotmat(3,2) = 2*tby*tbz - 2*tbx
       rotmat(3,3) = 1-teta2 + 2*tbz**2

       rotmat(:,:) = 1/(1+teta2) * rotmat(:,:)

!
!     definition des coefficients de la matrice Umatstar pour electron
! ----------------------------------------------------------
       IF(simplified.EQ.0) THEN
       Umatstar(1,1) = ((partic(a_py,ip,iesp)+
     &   partic(a_pytld,ip,iesp))*partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*(partic(a_px,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umatstar(1,2) = ((partic(a_py,ip,iesp)+
     &   partic(a_pytld,ip,iesp))*partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*(partic(a_py,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umatstar(1,3) = ((partic(a_py,ip,iesp)+
     &   partic(a_pytld,ip,iesp))*partic(a_bzp,ip,iesp) -
     &  (partic(a_pz,ip,iesp)+partic(a_pztld,ip,iesp))*
     &   partic(a_byp,ip,iesp))*(partic(a_pz,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umatstar(2,1) = ((partic(a_pz,ip,iesp)+
     &   partic(a_pztld,ip,iesp))*partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*(partic(a_px,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umatstar(2,2) = ((partic(a_pz,ip,iesp)+
     &   partic(a_pztld,ip,iesp))*partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*(partic(a_py,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umatstar(2,3) = ((partic(a_pz,ip,iesp)+
     &   partic(a_pztld,ip,iesp))*partic(a_bxp,ip,iesp) -
     &  (partic(a_px,ip,iesp)+partic(a_pxtld,ip,iesp))*
     &   partic(a_bzp,ip,iesp))*(partic(a_pz,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umatstar(3,1) = ((partic(a_px,ip,iesp)+
     &   partic(a_pxtld,ip,iesp))*partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*(partic(a_px,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaxe,ip,iesp))
       Umatstar(3,2) = ((partic(a_px,ip,iesp)+
     &   partic(a_pxtld,ip,iesp))*partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*(partic(a_py,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaye,ip,iesp))
       Umatstar(3,3) = ((partic(a_px,ip,iesp)+
     &   partic(a_pxtld,ip,iesp))*partic(a_byp,ip,iesp) -
     &  (partic(a_py,ip,iesp)+partic(a_pytld,ip,iesp))*
     &   partic(a_bxp,ip,iesp))*(partic(a_pz,ip,iesp)+
     &   0.25*dt_eff*partic(a_gaze,ip,iesp))

       Umatstar(:,:)= 0.25*rqm
     &    *(dt_eff/partic(a_gatld,ip,iesp)**3) *Umatstar(:,:)
       Umatstar(:,:)= identity(:,:) - Umatstar(:,:)
! ---------------------------
!        Forme simplifiee (gamma_n approxime a gamma_tild_n)
!        avec prise en compte des gradients du champ electrique
! ---------------------------
       ELSEIF(simplified.EQ.2) THEN
         Am33(:,:)= 0.25*rqm*dt_eff*matmul((identity+rotmat),gradE)
         Am33(:,:)= identity - Am33(:,:)
         CALL invmat33(Am33)

         Umatstar(:,:) = identity(:,:)

!        Forme simplifiee (gamma_n approxime a gamma_tild_n)
       ELSE
         Umatstar(:,:)= identity(:,:)
       ENDIF

       IF(simplified.EQ.2) THEN
         Mrelat(:,:) = matmul(Am33,Mrelat)
       ENDIF
       chimat(1:3,1:3) = matmul(Mrelat, Identity+rotmat)
       chimat(1:3,1:3) = matmul(chimat,Umatstar)
       chimat(:,:)=0.25*charge(iesp)**2/masse(iesp)*
     &     dt_eff**2 *chimat(:,:)

       zetamat(1,1) = vye*rotmat(3,1)     - vze*rotmat(2,1)
       zetamat(1,2) = vye*rotmat(3,2)     - vze*(1+rotmat(2,2))
       zetamat(1,3) = vye*(1+rotmat(3,3)) - vze*rotmat(2,3)

       zetamat(2,1) = vze*(1+rotmat(1,1)) - vxe*rotmat(3,1)
       zetamat(2,2) = vze*rotmat(1,2)     - vxe*rotmat(3,2)
       zetamat(2,3) = vze*rotmat(1,3)     - vxe*(1+rotmat(3,3))

       zetamat(3,1) = vxe*rotmat(2,1)     - vye*(1+rotmat(1,1))
       zetamat(3,2) = vxe*(1+rotmat(2,2)) - vye*rotmat(1,2)
       zetamat(3,3) = vxe*rotmat(2,3)     - vye*rotmat(1,3)

       zetamat(:,:) = matmul(zetamat, Umatstar)
       zetamat(:,:) = 0.125*charge(iesp)**2/masse(iesp)*
     &        dt_eff**2/partic(a_gatld12,ip,iesp)*zetamat(:,:)


!         CALL kizeta_rel(sd1, sd2, sd3, sd4, ipmx, ipmy,
!     &                       chimat, zetamat, iesp)
       IF(max_cycles.GT.1) THEN
         CALL kizeta_rel_ordn_sub(ponpx, ponpy, ipx, ipy,
     &          chimat, zetamat, iesp, partic(a_po,ip,iesp))
       ELSE
         CALL kizeta_rel_ordn(ponpx, ponpy, ipx, ipy,
     &          chimat, zetamat, iesp, partic(a_po,ip,iesp))
       ENDIF

       IF(debug.GE.4) THEN
         SELECT CASE(ordre_interp)
           CASE(1)
!           CALL kizeta_components(sd1, sd2, sd3, sd4, ipmx, ipmy,
!     &                  chir1, chir2, zetar1, iesp)
           CASE(2)

           CASE DEFAULT
           WRITE(fmsgo,*) 'pousseurs.f : ordre_interp incorrect',
     &         'appel kizeta_rel'
         END SELECT
       ENDIF

       ENDDO

       ENDIF  ! IF(col_test.EQ.0)


       return
!      ____________________
       END SUBROUTINE push_rel_ttafix


       SUBROUTINE prpush(iesp)
! ======================================================================
!
!
!
!
!
! ======================================================================

       USE domaines
       USE domaines_klder
       USE particules
       USE particules_klder
       USE champs
       USE collisions
       USE temps
       USE erreurs

       IMPLICIT NONE


       INTEGER    ::   i, j

       REAL(kind=kr)     ::  teta2,
     &    aux1, aux2, vb,
     &    vx1, vy1, vz1, vx2, vy2, vz2

       REAL(kind=kr)   ::  ex1, ey1, ez1

       REAL(kind=kr)   ::  rqm, aux_rh

       INTEGER    ::   ip
       INTEGER, INTENT(IN)    ::  iesp

       INTEGER(KIND=kr), DIMENSION(1:ordre_interp+1)  :: ipx, ipy
       REAL(KIND=kr), DIMENSION(1:ordre_interp+1)     :: ponpx, ponpy



! --- On remet � z�ro les courants rj[xyz]e sur la grille
       cjx_esp_2(:,:,iesp) = 0.
       cjy_esp_2(:,:,iesp) = 0.
       cjz_esp_2(:,:,iesp) = 0.
! --- On remet � z�ro les courants rj[xyz]e sur la grille
       rho_esp_2(:,:,iesp) = 0.
       dgx_esp_2(:,:,iesp) = 0.
       dgy_esp_2(:,:,iesp) = 0.
       dgz_esp_2(:,:,iesp) = 0.

       rqm = charge(iesp)/masse(iesp)

! ----- Calcul du gradient du champ electrique a l instant (n*dt)
       IF(simplified.EQ.2 .AND. iesp.EQ.1) THEN
         CALL calcgradE
       ENDIF


      IF(col_test.EQ.0) THEN

!     prepush du pas suivant
!     ----------------------
       DO ip=1,nbpa(iesp)
!     on definit les particules par leur centre.
!
        SELECT CASE(ordre_interp)
         CASE(1)
         CALL ponder_ordre1(ip, iesp, ponpx, ponpy, ipx, ipy)

        CASE(2)
         CALL ponder_ordre2(ip, iesp, ponpx, ponpy, ipx, ipy)

        CASE(3)
         CALL ponder_ordre3(ip, iesp, ponpx, ponpy, ipx, ipy)

        CASE(4)
         CALL ponder_ordre4(ip, iesp, ponpx, ponpy, ipx, ipy)

        CASE DEFAULT
        WRITE(fmsgo,*) 'pousseurs.f : mauvaise valeur ordre_interp'
       END SELECT

       CALL champa_2d_exyz1(ex1, ey1, ez1,
     &                ponpx, ponpy, ipx, ipy)
       CALL champa_2dx3dv_bxyz(ip, iesp,
     &                ponpx, ponpy, ipx, ipy)

 !
 !     initialisation de gaxe,gaye,gaze
 !
        partic(a_gaxe,ip,iesp) = rqm*partic(a_ttapushs2,ip,iesp)*ex1 +
     &    (1-partic(a_ttapushs2,ip,iesp))*partic(a_paxe,ip,iesp)
        partic(a_gaye,ip,iesp) = rqm*partic(a_ttapushs2,ip,iesp)*ey1 +
     &    (1-partic(a_ttapushs2,ip,iesp))*partic(a_paye,ip,iesp)
        partic(a_gaze,ip,iesp) = rqm*partic(a_ttapushs2,ip,iesp)*ez1 +
     &    (1-partic(a_ttapushs2,ip,iesp))*partic(a_paze,ip,iesp)

 !
 !     calcul de v avant correction.
 !     -----------------------------
       vx1=partic(a_px,ip,iesp)+dts4*partic(a_gaxe,ip,iesp)
       vy1=partic(a_py,ip,iesp)+dts4*partic(a_gaye,ip,iesp)
       vz1=partic(a_pz,ip,iesp)+dts4*partic(a_gaze,ip,iesp)
 !
       teta2=dt2s4*rqm**2 *(partic(a_bxp,ip,iesp)**2
     &    +partic(a_byp,ip,iesp)**2 +partic(a_bzp,ip,iesp)**2)
       aux1=1.-teta2
       aux2=1./(1.+teta2)
       vb=dt2s4*rqm**2 *(vx1*partic(a_bxp,ip,iesp)
     &    +vy1*partic(a_byp,ip,iesp) +vz1*partic(a_bzp,ip,iesp))
 !
 !     application de la matrice de rotation
       vx2=aux2*(vx1*aux1 + dt*rqm*(vy1*partic(a_bzp,ip,iesp)
     &    -vz1*partic(a_byp,ip,iesp))+2*vb*partic(a_bxp,ip,iesp))
       vy2=aux2*(vy1*aux1 + dt*rqm*(vz1*partic(a_bxp,ip,iesp)
     &    -vx1*partic(a_bzp,ip,iesp))+2*vb*partic(a_byp,ip,iesp))
       vz2=aux2*(vz1*aux1 + dt*rqm*(vx1*partic(a_byp,ip,iesp)
     &    -vy1*partic(a_bxp,ip,iesp))+2*vb*partic(a_bzp,ip,iesp))

 !
       partic(a_px,ip,iesp)=vx2+dts4*partic(a_gaxe,ip,iesp)
       partic(a_py,ip,iesp)=vy2+dts4*partic(a_gaye,ip,iesp)
       partic(a_pz,ip,iesp)=vz2+dts4*partic(a_gaze,ip,iesp)

       aux_rh = partic(a_po,ip,iesp)

       CALL projpa_2dx3dv_rjxyz(iesp, ponpx, ponpy,
     &                ipx, ipy, partic(a_px,ip,iesp),
     &         partic(a_py,ip,iesp), partic(a_pz,ip,iesp), aux_rh)


 !
 !     calcul de x au temps n+1
 !     -------------------------
       partic(a_qx,ip,iesp)=partic(a_qx,ip,iesp)
     &    +dt*partic(a_px,ip,iesp)
       partic(a_qy,ip,iesp)=partic(a_qy,ip,iesp)
     &    +dt*partic(a_py,ip,iesp)

       IF(partic(a_qy,ip,iesp).GE.bord(4,numproc)) THEN
         partic(a_qy,ip,iesp) =
     &        partic(a_qy,ip,iesp) -ylong
       ELSEIF(partic(a_qy,ip,iesp).LE.bord(3,numproc)) THEN
         partic(a_qy,ip,iesp) =
     &        partic(a_qy,ip,iesp) +ylong
       ENDIF

 !
 !     calcul de rhoe en (x n+1) et j(x n+1,v n+1/2)
 !     ---------------------------------------------
       SELECT CASE(ordre_interp)
        CASE(1)
         CALL ponder_ordre1(ip, iesp, ponpx, ponpy, ipx, ipy)

        CASE(2)
         CALL ponder_ordre2(ip, iesp, ponpx, ponpy, ipx, ipy)

        CASE(3)
         CALL ponder_ordre3(ip, iesp, ponpx, ponpy, ipx, ipy)

        CASE(4)
         CALL ponder_ordre4(ip, iesp, ponpx, ponpy, ipx, ipy)

        CASE DEFAULT
         WRITE(fmsgo,*) 'pousseurs.f : ordre_interp incorrecte'
       END SELECT

         CALL projpa_2dx3dv_dgxyz(iesp, ponpx, ponpy,
     &                ipx, ipy, partic(a_px,ip,iesp),
     &      partic(a_py,ip,iesp),
     &      partic(a_pz,ip,iesp), aux_rh)


       ENDDO

       ENDIF   ! IF(col_test.EQ.0)


       return
!      ______________________
       END SUBROUTINE prpush




       SUBROUTINE push(iesp)
! ======================================================================
!
!
!
! ======================================================================

       USE domaines
       USE domaines_klder
       USE particules
       USE particules_klder
       USE champs
       USE collisions
       USE temps
       USE diagnostics
       USE erreurs

       IMPLICIT NONE


       INTEGER    ::   i, j

       REAL(kind=kr)     ::  teta2,
     &    aux1, aux2, vb,
     &    vx1, vy1, vz1, vx2, vy2, vz2

       INTEGER, INTENT(IN)    ::    iesp

       INTEGER     ::   ip

       INTEGER(KIND=kr), DIMENSION(1:ordre_interp+1)  :: ipx, ipy
       REAL(KIND=kr), DIMENSION(1:ordre_interp+1)     :: ponpx, ponpy

       REAL(KIND=kr), DIMENSION(1:3,1:3)  ::  gradE, Am33
       REAL(KIND=kr), DIMENSION(1:3,1:3)  ::  identity, rotmat

       REAL(kind=kr)     ::   rqm, aux_rh

       REAL(kind=kr)        ::
     &   sumx, sumy, sumz, dwx, dwy, dwz,
     &   ex1, ey1, ez1, eb, dvx2, dvy2, dvz2


!       INTEGER, DIMENSION(1:npsort)   ::    ise1, ise2

 !

       sumx=0.
       sumy=0.
       sumz=0.
       dwx=0.
       dwy=0.
       dwz=0.

       rqm = charge(iesp)/masse(iesp)

       identity(1,1)=1. ; identity(1,2)=0. ; identity(1,3)=0.
       identity(2,1)=0. ; identity(2,2)=1. ; identity(2,3)=0.
       identity(3,1)=0. ; identity(3,2)=0. ; identity(3,3)=1.


       cnt_fast(iesp)= 0

       IF(col_test.EQ.0) THEN

       DO ip=1,nbpa(iesp)
!     final push
!     ----------
!     on fait les corrections aux valeurs predites de x et v
!     on definit les particules par leur centre.
!
!     interpolation du champ electrique t=(n+1)*dt au centre des particules
!     interpolation du gradient du champ electrique t=n*dt au centre des particules
!
       SELECT CASE(ordre_interp)
         CASE(1)
         CALL ponder_ordre1(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(2)
         CALL ponder_ordre2(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(3)
         CALL ponder_ordre3(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(4)
         CALL ponder_ordre4(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE DEFAULT
         write(fmsgo,*) 'pousseurs.f : valeur de ordre_interp ',
     &     'incorrecte '

       END SELECT

       CALL champa_2d_exyz1(ex1, ey1, ez1,
     &                ponpx, ponpy, ipx, ipy)
       IF(simplified.EQ.2) THEN
         CALL champa_2d_gradE(gradE, ponpx, ponpy, ipx, ipy)
       ENDIF

 !
 !     calcul de la correction deltav
 !
       teta2=dt2s4*rqm**2 *(partic(a_bxp,ip,iesp)**2+
     &    partic(a_byp,ip,iesp)**2+partic(a_bzp,ip,iesp)**2)
       aux2=rqm*dts2/(1.+teta2)
       eb=dt2s4*rqm**2 *(ex1*partic(a_bxp,ip,iesp)+
     &   ey1*partic(a_byp,ip,iesp)+ez1*partic(a_bzp,ip,iesp))
 !
 !     application de la matrice de rotation
       dvx2=aux2*(ex1 + rqm*dts2*(ey1*partic(a_bzp,ip,iesp)-
     &    ez1*partic(a_byp,ip,iesp)) +eb*partic(a_bxp,ip,iesp))
       dvy2=aux2*(ey1 + rqm*dts2*(ez1*partic(a_bxp,ip,iesp)-
     &    ex1*partic(a_bzp,ip,iesp)) +eb*partic(a_byp,ip,iesp))
       dvz2=aux2*(ez1 + rqm*dts2*(ex1*partic(a_byp,ip,iesp)-
     &    ey1*partic(a_bxp,ip,iesp)) +eb*partic(a_bzp,ip,iesp))

! ---------------------------
!        Prise en compte des gradients du champ electrique
! ---------------------------
       IF(simplified.EQ.2) THEN
         rotmat= identity
         Am33(:,:)= 0.25*rqm*dt*matmul((identity+rotmat),gradE)
         Am33(:,:)= identity - Am33(:,:)
         CALL invmat33(Am33)

         dvx2= Am33(1,1)*dvx2 + Am33(1,2)*dvy2 + Am33(1,3)*dvz2
         dvy2= Am33(2,1)*dvx2 + Am33(2,2)*dvy2 + Am33(2,3)*dvz2
         dvz2= Am33(3,1)*dvx2 + Am33(3,2)*dvy2 + Am33(3,3)*dvz2
       ENDIF

 !
 !     calcul de l acceleration au temps n+1
 !     -------------------------------------
         partic(a_gaxe,ip,iesp) = rqm*partic(a_ttapushs2,ip,iesp)*ex1 +
     &     (1-partic(a_ttapushs2,ip,iesp))*partic(a_paxe,ip,iesp)
         partic(a_gaye,ip,iesp) = rqm*partic(a_ttapushs2,ip,iesp)*ey1 +
     &     (1-partic(a_ttapushs2,ip,iesp))*partic(a_paye,ip,iesp)
         partic(a_gaze,ip,iesp) = rqm*partic(a_ttapushs2,ip,iesp)*ez1 +
     &     (1-partic(a_ttapushs2,ip,iesp))*partic(a_paze,ip,iesp)

         partic(a_paxe,ip,iesp) =
     &     rqm*(1-partic(a_ttapushs2,ip,iesp))*ex1 +
     &     partic(a_ttapushs2,ip,iesp)*partic(a_paxe,ip,iesp)
         partic(a_paye,ip,iesp) =
     &     rqm*(1-partic(a_ttapushs2,ip,iesp))*ey1 +
     &     partic(a_ttapushs2,ip,iesp)*partic(a_paye,ip,iesp)
         partic(a_paze,ip,iesp) =
     &     rqm*(1-partic(a_ttapushs2,ip,iesp))*ez1 +
     &     partic(a_ttapushs2,ip,iesp)*partic(a_paze,ip,iesp)

 !
 !     calcul de v au temps n+1
 !     ------------------------
       partic(a_px,ip,iesp)=partic(a_px,ip,iesp)+dvx2
       partic(a_py,ip,iesp)=partic(a_py,ip,iesp)+dvy2
       partic(a_pz,ip,iesp)=partic(a_pz,ip,iesp)+dvz2

 !
 !     energie cinetique au temps n+1
 !     ------------------------------
       sumx=sumx+partic(a_px,ip,iesp)*partic(a_px,ip,iesp)
       sumy=sumy+partic(a_py,ip,iesp)*partic(a_py,ip,iesp)
       sumz=sumz+partic(a_pz,ip,iesp)*partic(a_pz,ip,iesp)
 !
 !     calcul de x au temps n+1
 !     -------------------------
       partic(a_qx,ip,iesp)=partic(a_qx,ip,iesp)+dt*dvx2
       partic(a_qy,ip,iesp)=partic(a_qy,ip,iesp)+dt*dvy2

!       IF(npart.EQ.1) THEN
! ------- ndebe=ndebi=npart=1
!         WRITE(IDxe_t,wforma) (xe(ndebe)-0.5*dt*dvx2)
!       ENDIF

       ENDDO

       ENDIF   ! IF(col_test.EQ.0)

! --- + Diagnostics temporels a chaque pas de temps : diag_tp_impl2d
! --- diags de impl2d
!        CALL diag_tp_impl2d(iesp)

! --- Calcul de l energie cinetique
       partic(a_ga, 1:nbpa(iesp), iesp) = 0._8
       IF(a_px.NE.0) partic(a_ga, 1:nbpa(iesp),iesp)
     &          = partic(a_ga, 1:nbpa(iesp),iesp)
     &          + partic(a_px, 1:nbpa(iesp),iesp)**2
       IF(a_py.NE.0) partic(a_ga, 1:nbpa(iesp),iesp)
     &          = partic(a_ga, 1:nbpa(iesp),iesp)
     &          + partic(a_py, 1:nbpa(iesp),iesp)**2
       IF(a_pz.NE.0) partic(a_ga, 1:nbpa(iesp),iesp)
     &          = partic(a_ga, 1:nbpa(iesp),iesp)
     &          + partic(a_pz, 1:nbpa(iesp),iesp)**2

        partic(a_ga, 1:nbpa(iesp), iesp) =
     &        0.5*partic(a_ga, 1:nbpa(iesp), iesp)


       CALL cndlim_part(iesp)

! --- + Tri spatial des particules
       IF(per_tri(iesp).NE.0) THEN
          IF(MOD(iter,per_tri(iesp)).EQ.0) THEN
            CALL tri_part(iesp)
          ENDIF
       ENDIF

 !**********************************************************************
 !

 ! --- On remet � z�ro les courants rj[xyz]e sur la grille
       cjx_esp_2(:,:,iesp) = 0.
       cjy_esp_2(:,:,iesp) = 0.
       cjz_esp_2(:,:,iesp) = 0.
! --- On remet � z�ro les courants rj[xyz]e sur la grille
       rho_esp_2(:,:,iesp) = 0.
       dgx_esp_2(:,:,iesp) = 0.
       dgy_esp_2(:,:,iesp) = 0.
       dgz_esp_2(:,:,iesp) = 0.

 ! ----- Calcul du gradient du champ electrique a l instant (n*dt)
       IF(simplified.EQ.2 .AND. iesp.EQ.1) THEN
         CALL calcgradE
       ENDIF

       IF(col_test.EQ.0) THEN
 !     prepush du pas suivant
 !     ----------------------
       DO ip=1,nbpa(iesp)
 !
 !     on definit les particules par leur centre.
 !
        SELECT CASE(ordre_interp)
        CASE(1)
         CALL ponder_ordre1(ip, iesp, ponpx, ponpy, ipx, ipy)

        CASE(2)
         CALL ponder_ordre2(ip, iesp, ponpx, ponpy, ipx, ipy)

        CASE(3)
         CALL ponder_ordre3(ip, iesp, ponpx, ponpy, ipx, ipy)

        CASE(4)
         CALL ponder_ordre4(ip, iesp, ponpx, ponpy, ipx, ipy)

        CASE DEFAULT
        WRITE(fmsgo,*) 'pousseurs.f : mauvaise valeur ordre_interp'
       END SELECT

       CALL champa_2dx3dv_bxyz(ip, iesp, ponpx, ponpy, ipx, ipy)

 !
 !     calcul de v avant correction.
 !     -----------------------------
       vx1=partic(a_px,ip,iesp)+dts4*partic(a_gaxe,ip,iesp)
       vy1=partic(a_py,ip,iesp)+dts4*partic(a_gaye,ip,iesp)
       vz1=partic(a_pz,ip,iesp)+dts4*partic(a_gaze,ip,iesp)
 !
       teta2=dt2s4*rqm**2 *(partic(a_bxp,ip,iesp)**2
     &    +partic(a_byp,ip,iesp)**2 +partic(a_bzp,ip,iesp)**2)
       aux1=1.-teta2
       aux2=1./(1.+teta2)
       vb=dt2s4*rqm**2 *(vx1*partic(a_bxp,ip,iesp)
     &    +vy1*partic(a_byp,ip,iesp) +vz1*partic(a_bzp,ip,iesp))
 !
 !     application de la matrice de rotation
       vx2=aux2*(vx1*aux1 + dt*rqm*(vy1*partic(a_bzp,ip,iesp)
     &    -vz1*partic(a_byp,ip,iesp))+2*vb*partic(a_bxp,ip,iesp))
       vy2=aux2*(vy1*aux1 + dt*rqm*(vz1*partic(a_bxp,ip,iesp)
     &    -vx1*partic(a_bzp,ip,iesp))+2*vb*partic(a_byp,ip,iesp))
       vz2=aux2*(vz1*aux1 + dt*rqm*(vx1*partic(a_byp,ip,iesp)
     &    -vy1*partic(a_bxp,ip,iesp))+2*vb*partic(a_bzp,ip,iesp))

 !
       partic(a_px,ip,iesp)=vx2+dts4*partic(a_gaxe,ip,iesp)
       partic(a_py,ip,iesp)=vy2+dts4*partic(a_gaye,ip,iesp)
       partic(a_pz,ip,iesp)=vz2+dts4*partic(a_gaze,ip,iesp)

! ---- Promotion en particule rapide, dont le mouvement n'est pas amorti
!      Crit�re arbitraire : (\gamma -1)> Efast
!      Avec E_fast de l'ordre de quelques m_e c^2
!      rappel :   a_gatld12  equiv a_ga
!
       IF(partic(a_qx,ip,iesp).LE.x_damp1 .OR.
     &    partic(a_qx,ip,iesp).GT.x_damp2 .OR.
     &    partic(a_ga,ip,iesp).GE.Efast) THEN
         partic(a_ttapushs2,ip,iesp)= 0.5*thetad
	 cnt_fast(iesp)= cnt_fast(iesp)+1
       ELSE
         partic(a_ttapushs2,ip,iesp)= ttapushs2
       ENDIF

       aux_rh = partic(a_po,ip,iesp)

       CALL projpa_2dx3dv_rjxyz(iesp, ponpx, ponpy,
     &         ipx, ipy, partic(a_px,ip,iesp),
     &         partic(a_py,ip,iesp),
     &         partic(a_pz,ip,iesp), aux_rh)

 !
 !     calcul de x au temps n+1
 !     -------------------------
       partic(a_qx,ip,iesp)=partic(a_qx,ip,iesp)
     &    +dt*partic(a_px,ip,iesp)
       partic(a_qy,ip,iesp)=partic(a_qy,ip,iesp)
     &    +dt*partic(a_py,ip,iesp)

       IF(partic(a_qy,ip,iesp).GE.bord(4,numproc)) THEN
         partic(a_qy,ip,iesp) =
     &        partic(a_qy,ip,iesp) -ylong
       ELSEIF(partic(a_qy,ip,iesp).LE.bord(3,numproc)) THEN
         partic(a_qy,ip,iesp) =
     &        partic(a_qy,ip,iesp) +ylong
       ENDIF

 !
 !     calcul de rhoe en (x n+1) et j(x n+1,v n+1/2)
 !     ---------------------------------------------
       SELECT CASE(ordre_interp)
        CASE(1)
         CALL ponder_ordre1(ip, iesp, ponpx, ponpy, ipx, ipy)

        CASE(2)
         CALL ponder_ordre2(ip, iesp, ponpx, ponpy, ipx, ipy)

        CASE(3)
         CALL ponder_ordre3(ip, iesp, ponpx, ponpy, ipx, ipy)

        CASE(4)
         CALL ponder_ordre4(ip, iesp, ponpx, ponpy, ipx, ipy)

        CASE DEFAULT
         WRITE(fmsgo,*) 'pousseurs.f : ordre_interp incorrecte'
       END SELECT

         CALL projpa_2dx3dv_dgxyz(iesp, ponpx, ponpy,
     &                ipx, ipy, partic(a_px,ip,iesp),
     &       partic(a_py,ip,iesp),
     &       partic(a_pz,ip,iesp), aux_rh)


       ENDDO

       ENDIF   ! IF(col_test.EQ.0)


       return
!      ____________________
       END SUBROUTINE push
