! ======================================================================
! utilitaires.f
! -------------
!
!    SUBROUTINE tri_part
!
!    SUBROUTINE projpa_rho
!
!    SUBROUTINE partic_rho
!    SUBROUTINE partic_cj
!    SUBROUTINE partic_dg
!
!    SUBROUTINE period_rho
!    SUBROUTINE period_cj
!    SUBROUTINE period_dg
!
!    SUBROUTINE edge_sources
!    SUBROUTINE edge_kizetarel
!
!    SUBROUTINE period_kizetarel
!
!    SUBROUTINE edge_rho_ik
!    SUBROUTINE edge_chi_ik_rel
!    SUBROUTINE period_rho_ik
!    SUBROUTINE period_chi_ik_rel
!
!    SUBROUTINE  leqt1v
!    SUBROUTINE  banbks       
!    SUBROUTINE  bandec
!    SUBROUTINE  tridia
!    SUBROUTINE  out3v
!
!
! ======================================================================





      SUBROUTINE tri_part(iesp)
! ======================================================================
! Tri des particules de l'espece iesp en fonction de leurs positions
! dans le domaine local
!  | * Dans le tableau cell(numero de la particule), on range le numero
!  | de la cellule ou se trouve chaque particule, puis le numero qu'elle
!  | occupera dans le tableau reorganise
!  | * Dans compteur(numero de la cellule), on compte d'abord le nombre 
!  | de particules se trouvant dans chaque cellule, puis l'indice
!  | de la premiere particule de cette cellule dans le tableau de
!  | particules reorganise
! ======================================================================
      USE domaines
      USE champs
      USE temps
      USE particules_klder
      USE erreurs
       
      IMPLICIT NONE

      INTEGER, INTENT(IN)                         :: iesp

      INTEGER                                 :: i, icell, ix, iy, iz,
     &                                           nbcells, nbp,
     &                                           d1_min, d1_max
      INTEGER, DIMENSION(nbpamax)             :: cell


      IF(.NOT.ALLOCATED(compteur))
     &  ALLOCATE(compteur( (nulpx-nulmx)*(nulpy-nulmy)
     &                                  *(nulpz-nulmz),nesp_p) )

      nbp = nbpa(iesp)

      IF(nbp.GT.0) THEN
        compteur(:,iesp) = 0
        IF(a_qz.NE.0) THEN
          DO i=1,nbp
            ix = (partic(a_qx,i,iesp)-xmin)*pasdxi
            iy = (partic(a_qy,i,iesp)-ymin)*pasdyi
            iz = (partic(a_qz,i,iesp)-zmin)*pasdzi
            icell = ix+1 + (nulpx-nulmx)*(iy + (nulpy-nulmy)*iz )
            cell(i) = icell
            compteur(icell,iesp) = compteur(icell,iesp) + 1
          ENDDO

        ELSEIF(a_qy.NE.0) THEN
          DO i=1,nbp
            ix = (partic(a_qx,i,iesp)-xmin)*pasdxi
            iy = (partic(a_qy,i,iesp)-ymin)*pasdyi
            icell = ix+1 + (nulpx-nulmx)*iy
            cell(i) = icell
            compteur(icell,iesp) = compteur(icell,iesp) + 1
          ENDDO

        ELSEIF(a_qx.NE.0) THEN
          DO i=1,nbp
            ix = (partic(a_qx,i,iesp)-xmin)*pasdxi
            icell = ix+1
            cell(i) = icell
            compteur(icell,iesp) = compteur(icell,iesp) + 1
          ENDDO

        ENDIF
! --- a ce stade, compteur contient le nombre de particules dans
!     chaque cellule

        nbcells = (nulpx-nulmx)*(nulpy-nulmy)*(nulpz-nulmz)

        compteur(nbcells,iesp) = nbp + 1 - compteur(nbcells,iesp)
        DO i=nbcells-1,1,-1
          compteur(i,iesp) = compteur(i+1,iesp) - compteur(i,iesp)
        ENDDO
        IF(compteur(1,iesp).NE.1) THEN
          print*, 'Erreur dans tri_part : compteur(1)=',compteur(1,iesp)
        ENDIF
! --- a ce stade, compteur contient le numero de la premiere
!     particule de chaque cellule

        DO i=1,nbp
          icell = cell(i)
          cell(i) = compteur(icell,iesp)
          compteur(icell,iesp) = compteur(icell,iesp) + 1
        ENDDO
! --- a ce stade, compteur contient le numero de la premiere 
!     particule de la cellule suivante

        compteur(:,iesp) = EOSHIFT(compteur(:,iesp),-1,BOUNDARY=1)
! --- a ce stade, compteur contient a nouveau le numero de la
!     premiere particule de chaque cellule

        d1_min = a_po
        d1_max = MIN(SIZE(partic,1),SIZE(buf_part,1))
        DO
          buf_part(1:d1_max-d1_min+1,1:nbp) 
     &     = partic(d1_min:d1_max,1:nbp,iesp)
          DO i=1,nbp
                 partic(d1_min:d1_max, cell(i) ,iesp)
     &      = buf_part(1:d1_max-d1_min+1,      i)
          ENDDO
          IF(d1_max.EQ.SIZE(partic,1)) EXIT
          d1_min = d1_max + 1
          d1_max = MIN(SIZE(partic,1),d1_max+SIZE(buf_part,1))
        ENDDO
      ENDIF

      RETURN
!     _____________________       
      END SUBROUTINE tri_part




       SUBROUTINE edge_kizetarel
! ======================================================================
!
!
!
! ======================================================================
       USE domaines
       USE champs
       USE temps
       USE particules_klder
       USE erreurs
       
       IMPLICIT NONE
       
               

         xxx(0:n1x,ny) = xxx(:,0) + xxx(:,ny)
         xxy(0:n1x,ny) = xxy(:,0) + xxy(:,ny)
         xxz(0:n1x,ny) = xxz(:,0) + xxz(:,ny)       

         xyx(0:n1x,ny) = xyx(:,0) + xyx(:,ny)
         xyy(0:n1x,ny) = xyy(:,0) + xyy(:,ny)
         xyz(0:n1x,ny) = xyz(:,0) + xyz(:,ny)   
	 
         xzx(0:n1x,ny) = xzx(:,0) + xzx(:,ny)
         xzy(0:n1x,ny) = xzy(:,0) + xzy(:,ny)
         xzz(0:n1x,ny) = xzz(:,0) + xzz(:,ny)   

         zxx(0:n1x,ny) = zxx(:,0) + zxx(:,ny)
         zxy(0:n1x,ny) = zxy(:,0) + zxy(:,ny)
         zxz(0:n1x,ny) = zxz(:,0) + zxz(:,ny)       

         zyx(0:n1x,ny) = zyx(:,0) + zyx(:,ny)
         zyy(0:n1x,ny) = zyy(:,0) + zyy(:,ny)
         zyz(0:n1x,ny) = zyz(:,0) + zyz(:,ny)   
	 
         zzx(0:n1x,ny) = zzx(:,0) + zzx(:,ny)
         zzy(0:n1x,ny) = zzy(:,0) + zzy(:,ny)
         zzz(0:n1x,ny) = zzz(:,0) + zzz(:,ny)  	        
       
       IF(ordre_interp.GE.2) THEN
! ----------------------------------------------------------

         xxx(0:n1x,ny-1) = xxx(0:n1x,-1) + xxx(0:n1x,ny-1)
         xxy(0:n1x,ny-1) = xxy(0:n1x,-1) + xxy(0:n1x,ny-1)
         xxz(0:n1x,ny-1) = xxz(0:n1x,-1) + xxz(0:n1x,ny-1)       

         xyx(0:n1x,ny-1) = xyx(0:n1x,-1) + xyx(0:n1x,ny-1)
         xyy(0:n1x,ny-1) = xyy(0:n1x,-1) + xyy(0:n1x,ny-1)
         xyz(0:n1x,ny-1) = xyz(0:n1x,-1) + xyz(0:n1x,ny-1)       
	 
         xzx(0:n1x,ny-1) = xzx(0:n1x,-1) + xzx(0:n1x,ny-1)
         xzy(0:n1x,ny-1) = xzy(0:n1x,-1) + xzy(0:n1x,ny-1)
         xzz(0:n1x,ny-1) = xzz(0:n1x,-1) + xzz(0:n1x,ny-1)   

         zxx(0:n1x,ny-1) = zxx(0:n1x,-1) + zxx(0:n1x,ny-1)
         zxy(0:n1x,ny-1) = zxy(0:n1x,-1) + zxy(0:n1x,ny-1)
         zxz(0:n1x,ny-1) = zxz(0:n1x,-1) + zxz(0:n1x,ny-1)       

         zyx(0:n1x,ny-1) = zyx(0:n1x,-1) + zyx(0:n1x,ny-1)
         zyy(0:n1x,ny-1) = zyy(0:n1x,-1) + zyy(0:n1x,ny-1)
         zyz(0:n1x,ny-1) = zyz(0:n1x,-1) + zyz(0:n1x,ny-1)       
	 
         zzx(0:n1x,ny-1) = zzx(0:n1x,-1) + zzx(0:n1x,ny-1)
         zzy(0:n1x,ny-1) = zzy(0:n1x,-1) + zzy(0:n1x,ny-1)
         zzz(0:n1x,ny-1) = zzz(0:n1x,-1) + zzz(0:n1x,ny-1)   

! ----------------------------------------------------------
         xxx(0:n1x,1) = xxx(0:n1x,1) + xxx(0:n1x,ny+1)
         xxy(0:n1x,1) = xxy(0:n1x,1) + xxy(0:n1x,ny+1)
         xxz(0:n1x,1) = xxz(0:n1x,1) + xxz(0:n1x,ny+1)

         xyx(0:n1x,1) = xyx(0:n1x,1) + xyx(0:n1x,ny+1)
         xyy(0:n1x,1) = xyy(0:n1x,1) + xyy(0:n1x,ny+1)
         xyz(0:n1x,1) = xyz(0:n1x,1) + xyz(0:n1x,ny+1)
	 
         xzx(0:n1x,1) = xzx(0:n1x,1) + xzx(0:n1x,ny+1)
         xzy(0:n1x,1) = xzy(0:n1x,1) + xzy(0:n1x,ny+1)
         xzz(0:n1x,1) = xzz(0:n1x,1) + xzz(0:n1x,ny+1) 
	 
         zxx(0:n1x,1) = zxx(0:n1x,1) + zxx(0:n1x,ny+1)
         zxy(0:n1x,1) = zxy(0:n1x,1) + zxy(0:n1x,ny+1)
         zxz(0:n1x,1) = zxz(0:n1x,1) + zxz(0:n1x,ny+1)

         zyx(0:n1x,1) = zyx(0:n1x,1) + zyx(0:n1x,ny+1)
         zyy(0:n1x,1) = zyy(0:n1x,1) + zyy(0:n1x,ny+1)
         zyz(0:n1x,1) = zyz(0:n1x,1) + zyz(0:n1x,ny+1)
	 
         zzx(0:n1x,1) = zzx(0:n1x,1) + zzx(0:n1x,ny+1)
         zzy(0:n1x,1) = zzy(0:n1x,1) + zzy(0:n1x,ny+1)
         zzz(0:n1x,1) = zzz(0:n1x,1) + zzz(0:n1x,ny+1)
	 
         IF(ordre_interp.EQ.4) THEN
! ----------------------------------------------------------

           xxx(0:n1x,ny-2) = xxx(0:n1x,-2) + xxx(0:n1x,ny-2)
           xxy(0:n1x,ny-2) = xxy(0:n1x,-2) + xxy(0:n1x,ny-2)
           xxz(0:n1x,ny-2) = xxz(0:n1x,-2) + xxz(0:n1x,ny-2)       

           xyx(0:n1x,ny-2) = xyx(0:n1x,-2) + xyx(0:n1x,ny-2)
           xyy(0:n1x,ny-2) = xyy(0:n1x,-2) + xyy(0:n1x,ny-2)
           xyz(0:n1x,ny-2) = xyz(0:n1x,-2) + xyz(0:n1x,ny-2)       
	 
           xzx(0:n1x,ny-2) = xzx(0:n1x,-2) + xzx(0:n1x,ny-2)
           xzy(0:n1x,ny-2) = xzy(0:n1x,-2) + xzy(0:n1x,ny-2)
           xzz(0:n1x,ny-2) = xzz(0:n1x,-2) + xzz(0:n1x,ny-2)   

           zxx(0:n1x,ny-2) = zxx(0:n1x,-2) + zxx(0:n1x,ny-2)
           zxy(0:n1x,ny-2) = zxy(0:n1x,-2) + zxy(0:n1x,ny-2)
           zxz(0:n1x,ny-2) = zxz(0:n1x,-2) + zxz(0:n1x,ny-2)       

           zyx(0:n1x,ny-2) = zyx(0:n1x,-2) + zyx(0:n1x,ny-2)
           zyy(0:n1x,ny-2) = zyy(0:n1x,-2) + zyy(0:n1x,ny-2)
           zyz(0:n1x,ny-2) = zyz(0:n1x,-2) + zyz(0:n1x,ny-2)       
	 
           zzx(0:n1x,ny-2) = zzx(0:n1x,-2) + zzx(0:n1x,ny-2)
           zzy(0:n1x,ny-2) = zzy(0:n1x,-2) + zzy(0:n1x,ny-2)
           zzz(0:n1x,ny-2) = zzz(0:n1x,-2) + zzz(0:n1x,ny-2)   

! ----------------------------------------------------------
           xxx(0:n1x,2) = xxx(0:n1x,2) + xxx(0:n1x,ny+2)
           xxy(0:n1x,2) = xxy(0:n1x,2) + xxy(0:n1x,ny+2)
           xxz(0:n1x,2) = xxz(0:n1x,2) + xxz(0:n1x,ny+2)

           xyx(0:n1x,2) = xyx(0:n1x,2) + xyx(0:n1x,ny+2)
           xyy(0:n1x,2) = xyy(0:n1x,2) + xyy(0:n1x,ny+2)
           xyz(0:n1x,2) = xyz(0:n1x,2) + xyz(0:n1x,ny+2)
	 
           xzx(0:n1x,2) = xzx(0:n1x,2) + xzx(0:n1x,ny+2)
           xzy(0:n1x,2) = xzy(0:n1x,2) + xzy(0:n1x,ny+2)
           xzz(0:n1x,2) = xzz(0:n1x,2) + xzz(0:n1x,ny+2) 
	 
           zxx(0:n1x,2) = zxx(0:n1x,2) + zxx(0:n1x,ny+2)
           zxy(0:n1x,2) = zxy(0:n1x,2) + zxy(0:n1x,ny+2)
           zxz(0:n1x,2) = zxz(0:n1x,2) + zxz(0:n1x,ny+2)

           zyx(0:n1x,2) = zyx(0:n1x,2) + zyx(0:n1x,ny+2)
           zyy(0:n1x,2) = zyy(0:n1x,2) + zyy(0:n1x,ny+2)
           zyz(0:n1x,2) = zyz(0:n1x,2) + zyz(0:n1x,ny+2)
	 
           zzx(0:n1x,2) = zzx(0:n1x,2) + zzx(0:n1x,ny+2)
           zzy(0:n1x,2) = zzy(0:n1x,2) + zzy(0:n1x,ny+2)
           zzz(0:n1x,2) = zzz(0:n1x,2) + zzz(0:n1x,ny+2)
         ENDIF
       ENDIF



       return
!      _____________________       
       END SUBROUTINE edge_kizetarel



       SUBROUTINE period_kizetarel
! ======================================================================
!
!
!
! ======================================================================
       USE domaines
       USE champs
       USE temps
       USE particules_klder
       USE erreurs
       
       IMPLICIT NONE
       


       xxx(0:n1x,0)   = xxx(0:n1x,ny) 
       xxx(0:n1x,n1y) = xxx(0:n1x,1) 

       xxy(0:n1x,0)   = xxy(0:n1x,ny) 
       xxy(0:n1x,n1y) = xxy(0:n1x,1) 
	 
       xxz(0:n1x,0)   = xxz(0:n1x,ny) 
       xxz(0:n1x,n1y) = xxz(0:n1x,1) 
! ------------------------------------

       xyx(0:n1x,0)   = xyx(0:n1x,ny) 
       xyx(0:n1x,n1y) = xyx(0:n1x,1) 

       xyy(0:n1x,0)   = xyy(0:n1x,ny) 
       xyy(0:n1x,n1y) = xyy(0:n1x,1) 
	 
       xyz(0:n1x,0)   = xyz(0:n1x,ny) 
       xyz(0:n1x,n1y) = xyz(0:n1x,1) 
! ------------------------------------

       xzx(0:n1x,0)   = xzx(0:n1x,ny) 
       xzx(0:n1x,n1y) = xzx(0:n1x,1) 

       xzy(0:n1x,0)   = xzy(0:n1x,ny) 
       xzy(0:n1x,n1y) = xzy(0:n1x,1) 
	 
       xzz(0:n1x,0)   = xzz(0:n1x,ny) 
       xzz(0:n1x,n1y) = xzz(0:n1x,1) 
! ------------------------------------

       zxx(0:n1x,0)   = zxx(0:n1x,ny) 
       zxx(0:n1x,n1y) = zxx(0:n1x,1) 

       zxy(0:n1x,0)   = zxy(0:n1x,ny) 
       zxy(0:n1x,n1y) = zxy(0:n1x,1) 
	 
       zxz(0:n1x,0)   = zxz(0:n1x,ny) 
       zxz(0:n1x,n1y) = zxz(0:n1x,1) 
! ------------------------------------

       zyx(0:n1x,0)   = zyx(0:n1x,ny) 
       zyx(0:n1x,n1y) = zyx(0:n1x,1) 

       zyy(0:n1x,0)   = zyy(0:n1x,ny) 
       zyy(0:n1x,n1y) = zyy(0:n1x,1) 
	 
       zyz(0:n1x,0)   = zyz(0:n1x,ny) 
       zyz(0:n1x,n1y) = zyz(0:n1x,1) 
! ------------------------------------

       zzx(0:n1x,0)   = zzx(0:n1x,ny) 
       zzx(0:n1x,n1y) = zzx(0:n1x,1) 

       zzy(0:n1x,0)   = zzy(0:n1x,ny) 
       zzy(0:n1x,n1y) = zzy(0:n1x,1) 
	 
       zzz(0:n1x,0)   = zzz(0:n1x,ny) 
       zzz(0:n1x,n1y) = zzz(0:n1x,1) 
! ------------------------------------    

       IF(ordre_interp.GE.2) THEN
         xxx(0:n1x,-1)   = xxx(0:n1x,ny-1) 
         xxy(0:n1x,-1)   = xxy(0:n1x,ny-1) 
         xxz(0:n1x,-1)   = xxz(0:n1x,ny-1) 

         xyx(0:n1x,-1)   = xyx(0:n1x,ny-1) 
         xyy(0:n1x,-1)   = xyy(0:n1x,ny-1) 
         xyz(0:n1x,-1)   = xyz(0:n1x,ny-1) 
	 
         xzx(0:n1x,-1)   = xzx(0:n1x,ny-1) 
         xzy(0:n1x,-1)   = xzy(0:n1x,ny-1) 
         xzz(0:n1x,-1)   = xzz(0:n1x,ny-1) 
	 
         zxx(0:n1x,-1)   = zxx(0:n1x,ny-1) 
         zxy(0:n1x,-1)   = zxy(0:n1x,ny-1) 
         zxz(0:n1x,-1)   = zxz(0:n1x,ny-1) 

         zyx(0:n1x,-1)   = zyx(0:n1x,ny-1) 
         zyy(0:n1x,-1)   = zyy(0:n1x,ny-1) 
         zyz(0:n1x,-1)   = zyz(0:n1x,ny-1) 
	 
         zzx(0:n1x,-1)   = zzx(0:n1x,ny-1) 
         zzy(0:n1x,-1)   = zzy(0:n1x,ny-1) 
         zzz(0:n1x,-1)   = zzz(0:n1x,ny-1)          

         IF(ordre_interp.EQ.4) THEN
           xxx(0:n1x,-2)   = xxx(0:n1x,ny-2) 
           xxy(0:n1x,-2)   = xxy(0:n1x,ny-2) 
           xxz(0:n1x,-2)   = xxz(0:n1x,ny-2) 

           xyx(0:n1x,-2)   = xyx(0:n1x,ny-2) 
           xyy(0:n1x,-2)   = xyy(0:n1x,ny-2) 
           xyz(0:n1x,-2)   = xyz(0:n1x,ny-2) 
	 
           xzx(0:n1x,-2)   = xzx(0:n1x,ny-2) 
           xzy(0:n1x,-2)   = xzy(0:n1x,ny-2) 
           xzz(0:n1x,-2)   = xzz(0:n1x,ny-2) 
	 
           zxx(0:n1x,-2)   = zxx(0:n1x,ny-2) 
           zxy(0:n1x,-2)   = zxy(0:n1x,ny-2) 
           zxz(0:n1x,-2)   = zxz(0:n1x,ny-2) 

           zyx(0:n1x,-2)   = zyx(0:n1x,ny-2) 
           zyy(0:n1x,-2)   = zyy(0:n1x,ny-2) 
           zyz(0:n1x,-2)   = zyz(0:n1x,ny-2) 
	 
           zzx(0:n1x,-2)   = zzx(0:n1x,ny-2) 
           zzy(0:n1x,-2)   = zzy(0:n1x,ny-2) 
           zzz(0:n1x,-2)   = zzz(0:n1x,ny-2)   
	   
! ---------------------------------------------------------	   
           xxx(0:n1x,ny+2)   = xxx(0:n1x,2) 
           xxy(0:n1x,ny+2)   = xxy(0:n1x,2) 
           xxz(0:n1x,ny+2)   = xxz(0:n1x,2) 

           xyx(0:n1x,ny+2)   = xyx(0:n1x,2) 
           xyy(0:n1x,ny+2)   = xyy(0:n1x,2) 
           xyz(0:n1x,ny+2)   = xyz(0:n1x,2) 
	 
           xzx(0:n1x,ny+2)   = xzx(0:n1x,2) 
           xzy(0:n1x,ny+2)   = xzy(0:n1x,2) 
           xzz(0:n1x,ny+2)   = xzz(0:n1x,2) 
	 
           zxx(0:n1x,ny+2)   = zxx(0:n1x,2) 
           zxy(0:n1x,ny+2)   = zxy(0:n1x,2) 
           zxz(0:n1x,ny+2)   = zxz(0:n1x,2) 

           zyx(0:n1x,ny+2)   = zyx(0:n1x,2) 
           zyy(0:n1x,ny+2)   = zyy(0:n1x,2) 
           zyz(0:n1x,ny+2)   = zyz(0:n1x,2) 
	 
           zzx(0:n1x,ny+2)   = zzx(0:n1x,2) 
           zzy(0:n1x,ny+2)   = zzy(0:n1x,2) 
           zzz(0:n1x,ny+2)   = zzz(0:n1x,2)          
         ENDIF
       ENDIF       
       
       
!       WRITE(fmsgo,*) 'utilitaires.f : erreur period_kizetarel'  	       
       return
!      _____________________       
       END SUBROUTINE period_kizetarel


       SUBROUTINE edge_chi_ik_rel
! ======================================================================
!
!
!
! ======================================================================
       USE domaines
       USE champs_iter
       USE temps
       USE particules_klder
       USE erreurs
       
       IMPLICIT NONE
       
               

         xxx_ik(0:n1x,ny) = xxx_ik(:,0) + xxx_ik(:,ny)
         xxy_ik(0:n1x,ny) = xxy_ik(:,0) + xxy_ik(:,ny)
         xxz_ik(0:n1x,ny) = xxz_ik(:,0) + xxz_ik(:,ny)       

         xyx_ik(0:n1x,ny) = xyx_ik(:,0) + xyx_ik(:,ny)
         xyy_ik(0:n1x,ny) = xyy_ik(:,0) + xyy_ik(:,ny)
         xyz_ik(0:n1x,ny) = xyz_ik(:,0) + xyz_ik(:,ny)   
	 
         xzx_ik(0:n1x,ny) = xzx_ik(:,0) + xzx_ik(:,ny)
         xzy_ik(0:n1x,ny) = xzy_ik(:,0) + xzy_ik(:,ny)
         xzz_ik(0:n1x,ny) = xzz_ik(:,0) + xzz_ik(:,ny)   
       
       IF(ordre_interp.GE.2) THEN
! ----------------------------------------------------------
         xxx_ik(0:n1x,ny-1) = xxx_ik(0:n1x,-1) + xxx_ik(0:n1x,ny-1)
         xxy_ik(0:n1x,ny-1) = xxy_ik(0:n1x,-1) + xxy_ik(0:n1x,ny-1)
         xxz_ik(0:n1x,ny-1) = xxz_ik(0:n1x,-1) + xxz_ik(0:n1x,ny-1) 

         xyx_ik(0:n1x,ny-1) = xyx_ik(0:n1x,-1) + xyx_ik(0:n1x,ny-1)
         xyy_ik(0:n1x,ny-1) = xyy_ik(0:n1x,-1) + xyy_ik(0:n1x,ny-1)
         xyz_ik(0:n1x,ny-1) = xyz_ik(0:n1x,-1) + xyz_ik(0:n1x,ny-1) 
	 
         xzx_ik(0:n1x,ny-1) = xzx_ik(0:n1x,-1) + xzx_ik(0:n1x,ny-1)
         xzy_ik(0:n1x,ny-1) = xzy_ik(0:n1x,-1) + xzy_ik(0:n1x,ny-1)
         xzz_ik(0:n1x,ny-1) = xzz_ik(0:n1x,-1) + xzz_ik(0:n1x,ny-1) 

! ----------------------------------------------------------
         xxx_ik(0:n1x,1) = xxx_ik(0:n1x,1) + xxx_ik(0:n1x,ny+1)
         xxy_ik(0:n1x,1) = xxy_ik(0:n1x,1) + xxy_ik(0:n1x,ny+1)
         xxz_ik(0:n1x,1) = xxz_ik(0:n1x,1) + xxz_ik(0:n1x,ny+1)

         xyx_ik(0:n1x,1) = xyx_ik(0:n1x,1) + xyx_ik(0:n1x,ny+1)
         xyy_ik(0:n1x,1) = xyy_ik(0:n1x,1) + xyy_ik(0:n1x,ny+1)
         xyz_ik(0:n1x,1) = xyz_ik(0:n1x,1) + xyz_ik(0:n1x,ny+1)
	 
         xzx_ik(0:n1x,1) = xzx_ik(0:n1x,1) + xzx_ik(0:n1x,ny+1)
         xzy_ik(0:n1x,1) = xzy_ik(0:n1x,1) + xzy_ik(0:n1x,ny+1)
         xzz_ik(0:n1x,1) = xzz_ik(0:n1x,1) + xzz_ik(0:n1x,ny+1) 	 
         IF(ordre_interp.EQ.4) THEN
! ----------------------------------------------------------
           xxx_ik(0:n1x,ny-2) = xxx_ik(0:n1x,-2) + xxx_ik(0:n1x,ny-2)
           xxy_ik(0:n1x,ny-2) = xxy_ik(0:n1x,-2) + xxy_ik(0:n1x,ny-2)
           xxz_ik(0:n1x,ny-2) = xxz_ik(0:n1x,-2) + xxz_ik(0:n1x,ny-2) 

           xyx_ik(0:n1x,ny-2) = xyx_ik(0:n1x,-2) + xyx_ik(0:n1x,ny-2)
           xyy_ik(0:n1x,ny-2) = xyy_ik(0:n1x,-2) + xyy_ik(0:n1x,ny-2)
           xyz_ik(0:n1x,ny-2) = xyz_ik(0:n1x,-2) + xyz_ik(0:n1x,ny-2) 
	 
           xzx_ik(0:n1x,ny-2) = xzx_ik(0:n1x,-2) + xzx_ik(0:n1x,ny-2)
           xzy_ik(0:n1x,ny-2) = xzy_ik(0:n1x,-2) + xzy_ik(0:n1x,ny-2)
           xzz_ik(0:n1x,ny-2) = xzz_ik(0:n1x,-2) + xzz_ik(0:n1x,ny-2)   

! ----------------------------------------------------------
           xxx_ik(0:n1x,2) = xxx_ik(0:n1x,2) + xxx_ik(0:n1x,ny+2)
           xxy_ik(0:n1x,2) = xxy_ik(0:n1x,2) + xxy_ik(0:n1x,ny+2)
           xxz_ik(0:n1x,2) = xxz_ik(0:n1x,2) + xxz_ik(0:n1x,ny+2)

           xyx_ik(0:n1x,2) = xyx_ik(0:n1x,2) + xyx_ik(0:n1x,ny+2)
           xyy_ik(0:n1x,2) = xyy_ik(0:n1x,2) + xyy_ik(0:n1x,ny+2)
           xyz_ik(0:n1x,2) = xyz_ik(0:n1x,2) + xyz_ik(0:n1x,ny+2)
	 
           xzx_ik(0:n1x,2) = xzx_ik(0:n1x,2) + xzx_ik(0:n1x,ny+2)
           xzy_ik(0:n1x,2) = xzy_ik(0:n1x,2) + xzy_ik(0:n1x,ny+2)
           xzz_ik(0:n1x,2) = xzz_ik(0:n1x,2) + xzz_ik(0:n1x,ny+2) 	 
         ENDIF
       ENDIF


       return
!      _____________________       
       END SUBROUTINE edge_chi_ik_rel



       SUBROUTINE period_chi_ik_rel
! ======================================================================
!
!
!
! ======================================================================
       USE domaines
       USE champs_iter
       USE temps
       USE particules_klder
       USE erreurs
       
       IMPLICIT NONE
       


       xxx_ik(0:n1x,0)   = xxx_ik(0:n1x,ny) 
       xxx_ik(0:n1x,n1y) = xxx_ik(0:n1x,1) 

       xxy_ik(0:n1x,0)   = xxy_ik(0:n1x,ny) 
       xxy_ik(0:n1x,n1y) = xxy_ik(0:n1x,1) 
	 
       xxz_ik(0:n1x,0)   = xxz_ik(0:n1x,ny) 
       xxz_ik(0:n1x,n1y) = xxz_ik(0:n1x,1) 
! ------------------------------------

       xyx_ik(0:n1x,0)   = xyx_ik(0:n1x,ny) 
       xyx_ik(0:n1x,n1y) = xyx_ik(0:n1x,1) 

       xyy_ik(0:n1x,0)   = xyy_ik(0:n1x,ny) 
       xyy_ik(0:n1x,n1y) = xyy_ik(0:n1x,1) 
	 
       xyz_ik(0:n1x,0)   = xyz_ik(0:n1x,ny) 
       xyz_ik(0:n1x,n1y) = xyz_ik(0:n1x,1) 
! ------------------------------------

       xzx_ik(0:n1x,0)   = xzx_ik(0:n1x,ny) 
       xzx_ik(0:n1x,n1y) = xzx_ik(0:n1x,1) 

       xzy_ik(0:n1x,0)   = xzy_ik(0:n1x,ny) 
       xzy_ik(0:n1x,n1y) = xzy_ik(0:n1x,1) 
	 
       xzz_ik(0:n1x,0)   = xzz_ik(0:n1x,ny) 
       xzz_ik(0:n1x,n1y) = xzz_ik(0:n1x,1) 
! ------------------------------------

       IF(ordre_interp.GE.2) THEN
         xxx_ik(0:n1x,-1)   = xxx_ik(0:n1x,ny-1) 
         xxy_ik(0:n1x,-1)   = xxy_ik(0:n1x,ny-1) 
         xxz_ik(0:n1x,-1)   = xxz_ik(0:n1x,ny-1) 

         xyx_ik(0:n1x,-1)   = xyx_ik(0:n1x,ny-1) 
         xyy_ik(0:n1x,-1)   = xyy_ik(0:n1x,ny-1) 
         xyz_ik(0:n1x,-1)   = xyz_ik(0:n1x,ny-1) 
	 
         xzx_ik(0:n1x,-1)   = xzx_ik(0:n1x,ny-1) 
         xzy_ik(0:n1x,-1)   = xzy_ik(0:n1x,ny-1) 
         xzz_ik(0:n1x,-1)   = xzz_ik(0:n1x,ny-1) 
         IF(ordre_interp.EQ.4) THEN
           xxx_ik(0:n1x,-2)   = xxx_ik(0:n1x,ny-2) 
           xxy_ik(0:n1x,-2)   = xxy_ik(0:n1x,ny-2) 
           xxz_ik(0:n1x,-2)   = xxz_ik(0:n1x,ny-2) 

           xyx_ik(0:n1x,-2)   = xyx_ik(0:n1x,ny-2) 
           xyy_ik(0:n1x,-2)   = xyy_ik(0:n1x,ny-2) 
           xyz_ik(0:n1x,-2)   = xyz_ik(0:n1x,ny-2) 
	 
           xzx_ik(0:n1x,-2)   = xzx_ik(0:n1x,ny-2) 
           xzy_ik(0:n1x,-2)   = xzy_ik(0:n1x,ny-2) 
           xzz_ik(0:n1x,-2)   = xzz_ik(0:n1x,ny-2)       
	   
! ------------------------------------
           xxx_ik(0:n1x,ny+2)   = xxx_ik(0:n1x,2) 
           xxy_ik(0:n1x,ny+2)   = xxy_ik(0:n1x,2) 
           xxz_ik(0:n1x,ny+2)   = xxz_ik(0:n1x,2) 

           xyx_ik(0:n1x,ny+2)   = xyx_ik(0:n1x,2) 
           xyy_ik(0:n1x,ny+2)   = xyy_ik(0:n1x,2) 
           xyz_ik(0:n1x,ny+2)   = xyz_ik(0:n1x,2) 
	 
           xzx_ik(0:n1x,ny+2)   = xzx_ik(0:n1x,2) 
           xzy_ik(0:n1x,ny+2)   = xzy_ik(0:n1x,2) 
           xzz_ik(0:n1x,ny+2)   = xzz_ik(0:n1x,2)       
         ENDIF          
       ENDIF
!       WRITE(fmsgo,*) 'utilitaires.f : erreur period_kizetarel'  
	       

       return
!      _____________________       
       END SUBROUTINE period_chi_ik_rel




       SUBROUTINE projpa_rho(iesp)
! ======================================================================
!
!
!
! ======================================================================
       USE domaines
       USE champs
       USE temps
       USE particules_klder
       
       IMPLICIT NONE
       
       INTEGER, INTENT(IN)     ::   iesp
       
       INTEGER    ::   i, j, ip 

       REAL(kind=kr)        :: 
     &  sd1, sd2, sd3, sd4

       REAL(kind=kr)     ::    aux_rh

       REAL(kind=kr)   ::  rpx, rpy
       REAL(kind=kr)   ::  ponpmx, ponppx, ponpmy, ponppy       
       
       INTEGER  :: ipmx, ippx, ipmy, ippy


       rho_esp_2(:,:,iesp) = 0.

 
       DO ip=1, nbpa(iesp)
 !     calcul de rhoe en (x n+1) et j(x n+1,v n+1/2)
 !     ---------------------------------------------
         aux_rh = partic(a_po,ip,iesp)
       
! ----- on d�termine ds quelle maille est la particule
         rpx = (partic(a_qx,ip,iesp)-xmin_gl)*dxi
         rpy = (partic(a_qy,ip,iesp)-ymin_gl)*dyi
! ----- coordonn�es enti�res, lignes encadrant la particule
         ipmx = INT(rpx)
         ippx = ipmx+1
         ipmy = INT(rpy)
         ippy = ipmy+1
         i=ipmx ; j=ipmy
	
         ponpmx = REAl(ippx,8) - rpx
         ponppx = 1._8 - ponpmx
         ponpmy = REAl(ippy,8) - rpy
         ponppy = 1._8 - ponpmy
	
         sd1 = ponpmx*ponpmy
         sd2 = ponppx*ponpmy
         sd3 = ponpmx*ponppy
         sd4 = ponppx*ponppy
       
         sd1 = sd1*aux_rh 
         sd2 = sd2*aux_rh
         sd3 = sd3*aux_rh
         sd4 = sd4*aux_rh

         rho_esp_2(i  ,j  ,iesp) = rho_esp_2(i  ,j  ,iesp) + sd1 
         rho_esp_2(i+1,j  ,iesp) = rho_esp_2(i+1,j  ,iesp) + sd2 
         rho_esp_2(i  ,j+1,iesp) = rho_esp_2(i  ,j+1,iesp) + sd3 
         rho_esp_2(i+1,j+1,iesp) = rho_esp_2(i+1,j+1,iesp) + sd4
       
       ENDDO

       return
!      _____________________       
       END SUBROUTINE projpa_rho



       SUBROUTINE partic_rho(ip,iesp)
! ======================================================================
!
!
!
! ======================================================================
       USE domaines
       USE champs
       USE temps
       USE particules_klder
       
       IMPLICIT NONE
       
       INTEGER, INTENT(IN)     ::   ip, iesp
       
       INTEGER    ::   i, j  

       REAL(kind=kr)        :: 
     &   sd1, sd2, sd3, sd4

       REAL(kind=kr)     ::    aux_rh

       REAL(kind=kr)   ::  rpx, rpy
       REAL(kind=kr)   ::  ponpmx, ponppx, ponpmy, ponppy       
       
       INTEGER  :: ipmx, ippx, ipmy, ippy

       aux_rh = partic(a_po,ip,iesp)

 

 
 !     calcul de rhoe en (x n+1) et j(x n+1,v n+1/2)
 !     ---------------------------------------------
       
! ----- on d�termine ds quelle maille est la particule
       rpx = (partic(a_qx,ip,iesp)-xmin_gl)*dxi
       rpy = (partic(a_qy,ip,iesp)-ymin_gl)*dyi
! ----- coordonn�es enti�res, lignes encadrant la particule
       ipmx = INT(rpx)
       ippx = ipmx+1
       ipmy = INT(rpy)
       ippy = ipmy+1
       i=ipmx ; j=ipmy
	
       ponpmx = REAl(ippx,8) - rpx
       ponppx = 1._8 - ponpmx
       ponpmy = REAl(ippy,8) - rpy
       ponppy = 1._8 - ponpmy
	
       sd1 = ponpmx*ponpmy
       sd2 = ponppx*ponpmy
       sd3 = ponpmx*ponppy
       sd4 = ponppx*ponppy
       
       sd1 = sd1*aux_rh 
       sd2 = sd2*aux_rh
       sd3 = sd3*aux_rh
       sd4 = sd4*aux_rh

 !
 !     calcul de j(x n,v n+1/2)
 !     ------------------------
       rho_esp_2(i  ,j  ,iesp) = rho_esp_2(i  ,j  ,iesp) + sd1 
       rho_esp_2(i+1,j  ,iesp) = rho_esp_2(i+1,j  ,iesp) + sd2 
       rho_esp_2(i  ,j+1,iesp) = rho_esp_2(i  ,j+1,iesp) + sd3 
       rho_esp_2(i+1,j+1,iesp) = rho_esp_2(i+1,j+1,iesp) + sd4        
       
       

       return
!      _____________________       
       END SUBROUTINE partic_rho
       


       SUBROUTINE partic_cj(ip,iesp)
! ======================================================================
!
!
!
! ======================================================================
       USE domaines
       USE champs
       USE temps
       USE particules_klder
       
       IMPLICIT NONE
       
       INTEGER, INTENT(IN)     ::   ip, iesp
       
       INTEGER    ::   i, j  

       REAL(kind=kr)        :: 
     &   sd1, sd2, sd3, sd4

       REAL(kind=kr)     ::    aux_rh

       REAL(kind=kr)   ::  rpx, rpy
       REAL(kind=kr)   ::  ponpmx, ponppx, ponpmy, ponppy       
       
       INTEGER  :: ipmx, ippx, ipmy, ippy

       aux_rh = partic(a_po,ip,iesp)

 

 
 !     calcul de rhoe en (x n+1) et j(x n+1,v n+1/2)
 !     ---------------------------------------------
       
! ----- on d�termine ds quelle maille est la particule
       rpx = (partic(a_qx,ip,iesp)-xmin_gl)*dxi
       rpy = (partic(a_qy,ip,iesp)-ymin_gl)*dyi
! ----- coordonn�es enti�res, lignes encadrant la particule
       ipmx = INT(rpx)
       ippx = ipmx+1
       ipmy = INT(rpy)
       ippy = ipmy+1
       i=ipmx ; j=ipmy
	
       ponpmx = REAl(ippx,8) - rpx
       ponppx = 1._8 - ponpmx
       ponpmy = REAl(ippy,8) - rpy
       ponppy = 1._8 - ponpmy
	
       sd1 = ponpmx*ponpmy
       sd2 = ponppx*ponpmy
       sd3 = ponpmx*ponppy
       sd4 = ponppx*ponppy
       
       sd1 = sd1*aux_rh 
       sd2 = sd2*aux_rh
       sd3 = sd3*aux_rh
       sd4 = sd4*aux_rh

 !
 !     calcul de j(x n,v n+1/2)
 !     ------------------------
       cjx_esp_2(i  ,j  ,iesp) = cjx_esp_2(i  ,j  ,iesp) 
     &   + sd1*partic(a_px,ip,iesp) 
       cjx_esp_2(i+1,j  ,iesp) = cjx_esp_2(i+1,j  ,iesp) 
     &   + sd2*partic(a_px,ip,iesp) 
       cjx_esp_2(i  ,j+1,iesp) = cjx_esp_2(i  ,j+1,iesp) 
     &   + sd3*partic(a_px,ip,iesp) 
       cjx_esp_2(i+1,j+1,iesp) = cjx_esp_2(i+1,j+1,iesp) 
     &   + sd4*partic(a_px,ip,iesp)        
       
       cjy_esp_2(i  ,j  ,iesp) = cjy_esp_2(i  ,j  ,iesp)
     &   + sd1*partic(a_py,ip,iesp) 
       cjy_esp_2(i+1,j  ,iesp) = cjy_esp_2(i+1,j  ,iesp) 
     &   + sd2*partic(a_py,ip,iesp) 
       cjy_esp_2(i  ,j+1,iesp) = cjy_esp_2(i  ,j+1,iesp) 
     &   + sd3*partic(a_py,ip,iesp) 
       cjy_esp_2(i+1,j+1,iesp) = cjy_esp_2(i+1,j+1,iesp) 
     &   + sd4*partic(a_py,ip,iesp)     
              
       cjz_esp_2(i  ,j  ,iesp) = cjz_esp_2(i  ,j  ,iesp) 
     &   + sd1*partic(a_pz,ip,iesp) 
       cjz_esp_2(i+1,j  ,iesp) = cjz_esp_2(i+1,j  ,iesp) 
     &   + sd2*partic(a_pz,ip,iesp) 
       cjz_esp_2(i  ,j+1,iesp) = cjz_esp_2(i  ,j+1,iesp) 
     &   + sd3*partic(a_pz,ip,iesp) 
       cjz_esp_2(i+1,j+1,iesp) = cjz_esp_2(i+1,j+1,iesp) 
     &   + sd4*partic(a_pz,ip,iesp) 
       


       return
!      _____________________       
       END SUBROUTINE partic_cj



       SUBROUTINE partic_dg(ip,iesp)
! ======================================================================
!
!
!
! ======================================================================
       USE domaines
       USE champs
       USE temps
       USE particules_klder
       
       IMPLICIT NONE
       
       INTEGER, INTENT(IN)     ::   ip, iesp
       
       INTEGER    ::   i, j 

       REAL(kind=kr)        :: 
     &   sd1, sd2, sd3, sd4

       REAL(kind=kr)     ::    aux_rh

       REAL(kind=kr)   ::  rpx, rpy
       REAL(kind=kr)   ::  ponpmx, ponppx, ponpmy, ponppy       
       
       INTEGER  :: ipmx, ippx, ipmy, ippy

       aux_rh = partic(a_po,ip,iesp)

 

 
 !     calcul de rhoe en (x n+1) et j(x n+1,v n+1/2)
 !     ---------------------------------------------
       
! ----- on d�termine ds quelle maille est la particule
       rpx = (partic(a_qx,ip,iesp)-xmin_gl)*dxi
       rpy = (partic(a_qy,ip,iesp)-ymin_gl)*dyi
! ----- coordonn�es enti�res, lignes encadrant la particule
       ipmx = INT(rpx)
       ippx = ipmx+1
       ipmy = INT(rpy)
       ippy = ipmy+1
       i=ipmx ; j=ipmy
	
       ponpmx = REAl(ippx,8) - rpx
       ponppx = 1._8 - ponpmx
       ponpmy = REAl(ippy,8) - rpy
       ponppy = 1._8 - ponpmy
	
       sd1 = ponpmx*ponpmy
       sd2 = ponppx*ponpmy
       sd3 = ponpmx*ponppy
       sd4 = ponppx*ponppy
       
       sd1 = sd1*aux_rh 
       sd2 = sd2*aux_rh
       sd3 = sd3*aux_rh
       sd4 = sd4*aux_rh

       dgx_esp_2(i  ,j  ,iesp) = dgx_esp_2(i  ,j  ,iesp) 
     &   + sd1*partic(a_px,ip,iesp) 
       dgx_esp_2(i+1,j  ,iesp) = dgx_esp_2(i+1,j  ,iesp) 
     &   + sd2*partic(a_px,ip,iesp) 
       dgx_esp_2(i  ,j+1,iesp) = dgx_esp_2(i  ,j+1,iesp) 
     &   + sd3*partic(a_px,ip,iesp) 
       dgx_esp_2(i+1,j+1,iesp) = dgx_esp_2(i+1,j+1,iesp) 
     &   + sd4*partic(a_px,ip,iesp)   
 !
       dgy_esp_2(i  ,j  ,iesp) = dgy_esp_2(i  ,j  ,iesp) 
     &   + sd1*partic(a_py,ip,iesp) 
       dgy_esp_2(i+1,j  ,iesp) = dgy_esp_2(i+1,j  ,iesp) 
     &   + sd2*partic(a_py,ip,iesp) 
       dgy_esp_2(i  ,j+1,iesp) = dgy_esp_2(i  ,j+1,iesp) 
     &   + sd3*partic(a_py,ip,iesp) 
       dgy_esp_2(i+1,j+1,iesp) = dgy_esp_2(i+1,j+1,iesp) 
     &   + sd4*partic(a_py,ip,iesp)    
 !
       dgz_esp_2(i  ,j  ,iesp) = dgz_esp_2(i  ,j  ,iesp) 
     &   + sd1*partic(a_pz,ip,iesp) 
       dgz_esp_2(i+1,j  ,iesp) = dgz_esp_2(i+1,j  ,iesp) 
     &   + sd2*partic(a_pz,ip,iesp) 
       dgz_esp_2(i  ,j+1,iesp) = dgz_esp_2(i  ,j+1,iesp) 
     &   + sd3*partic(a_pz,ip,iesp) 
       dgz_esp_2(i+1,j+1,iesp) = dgz_esp_2(i+1,j+1,iesp) 
     &   + sd4*partic(a_pz,ip,iesp)    
       


       return
!      _____________________       
       END SUBROUTINE partic_dg



       SUBROUTINE edge_rho_ik
! ======================================================================
!
!
!
! ======================================================================
       USE domaines
       USE champs_iter
       USE particules_klder
       USE erreurs
       
       IMPLICIT NONE
       
       INTEGER     ::   iesp


       SELECT CASE(ordre_interp)
       CASE(1)
         DO iesp=1, nesp_p
          rho_esp2_ik(0:n1x,ny,iesp) = rho_esp2_ik(:,0,iesp)
     &          +rho_esp2_ik(:,ny,iesp)  
         ENDDO
       CASE(2,3)
         DO iesp=1, nesp_p
          rho_esp2_ik(0:n1x,ny,iesp) = rho_esp2_ik(:,0,iesp)
     &          +rho_esp2_ik(:,ny,iesp)
          rho_esp2_ik(0:n1x,ny-1,iesp) = rho_esp2_ik(:,-1,iesp)
     &          +rho_esp2_ik(:,ny-1,iesp)
          rho_esp2_ik(0:n1x,1,iesp) = rho_esp2_ik(:,1,iesp)
     &          +rho_esp2_ik(:,ny+1,iesp)
         ENDDO
       CASE(4)
         DO iesp=1, nesp_p
          rho_esp2_ik(0:n1x,ny,iesp) = rho_esp2_ik(:,0,iesp)
     &          +rho_esp2_ik(:,ny,iesp)
          rho_esp2_ik(0:n1x,ny-1,iesp) = rho_esp2_ik(:,-1,iesp)
     &          +rho_esp2_ik(:,ny-1,iesp)
          rho_esp2_ik(0:n1x,1,iesp) = rho_esp2_ik(:,1,iesp)
     &          +rho_esp2_ik(:,ny+1,iesp)

          rho_esp2_ik(0:n1x,ny-2,iesp) = rho_esp2_ik(:,-2,iesp)
     &          +rho_esp2_ik(:,ny-2,iesp)     
          rho_esp2_ik(0:n1x,2,iesp) = rho_esp2_ik(:,2,iesp)
     &          +rho_esp2_ik(:,ny+2,iesp)         
         ENDDO 	 

       CASE DEFAULT
         WRITE(fmsgo,*) 'utilitaires.f : erreur edge_sources' 
       END SELECT

       return
!      _____________________       
       END SUBROUTINE edge_rho_ik



       SUBROUTINE period_rho_ik(iesp)
! ======================================================================
!
!
!
! ======================================================================
       USE domaines
       USE champs_iter
       
       IMPLICIT NONE
       
       INTEGER, INTENT(IN)    ::   iesp


       rho_esp2_ik(0:n1x,0  ,iesp)= rho_esp2_ik(:,ny  ,iesp)
       rho_esp2_ik(0:n1x,n1y,iesp)= rho_esp2_ik(:,1   ,iesp)
       IF(ordre_interp.GE.2) THEN
         rho_esp2_ik(0:n1x,-1 ,iesp)= rho_esp2_ik(:,ny-1,iesp)
         IF(ordre_interp.EQ.4) THEN
           rho_esp2_ik(0:n1x,-2 ,iesp)= rho_esp2_ik(:,ny-2,iesp)
	 
           rho_esp2_ik(0:n1x,ny+2,iesp)= rho_esp2_ik(:,2,iesp)       
         ENDIF
       ENDIF

       return
!      _____________________       
       END SUBROUTINE period_rho_ik
       
       
       SUBROUTINE edge_sources
! ======================================================================
!
!
!
! ======================================================================
       USE domaines
       USE champs
       USE particules_klder
       USE temps
       USE erreurs
       
       IMPLICIT NONE
       
       INTEGER     ::   iesp



         DO iesp=1, nesp_p
          IF(MOD(iter,nbscycles(iesp)).NE.0) CYCLE
	 
          rho_esp_2(0:n1x,ny,iesp) = rho_esp_2(:,0,iesp)
     &          +rho_esp_2(:,ny,iesp)
          cjx_esp_2(0:n1x,ny,iesp) = cjx_esp_2(:,0,iesp)
     &          +cjx_esp_2(:,ny,iesp)
          cjy_esp_2(0:n1x,ny,iesp) = cjy_esp_2(:,0,iesp)
     &          +cjy_esp_2(:,ny,iesp)
          cjz_esp_2(0:n1x,ny,iesp) = cjz_esp_2(:,0,iesp)
     &          +cjz_esp_2(:,ny,iesp)

          dgx_esp_2(0:n1x,ny,iesp) = dgx_esp_2(:,0,iesp)
     &          +dgx_esp_2(:,ny,iesp)
          dgy_esp_2(0:n1x,ny,iesp) = dgy_esp_2(:,0,iesp)
     &          +dgy_esp_2(:,ny,iesp)
          dgz_esp_2(0:n1x,ny,iesp) = dgz_esp_2(:,0,iesp)
     &          +dgz_esp_2(:,ny,iesp)     
	 
          IF(ordre_interp.GE.2) THEN
          rho_esp_2(0:n1x,ny-1,iesp) = rho_esp_2(:,-1,iesp)
     &          +rho_esp_2(:,ny-1,iesp)
          cjx_esp_2(0:n1x,ny-1,iesp) = cjx_esp_2(:,-1,iesp)
     &          +cjx_esp_2(:,ny-1,iesp)
          cjy_esp_2(0:n1x,ny-1,iesp) = cjy_esp_2(:,-1,iesp)
     &          +cjy_esp_2(:,ny-1,iesp)
          cjz_esp_2(0:n1x,ny-1,iesp) = cjz_esp_2(:,-1,iesp)
     &          +cjz_esp_2(:,ny-1,iesp)

          dgx_esp_2(0:n1x,ny-1,iesp) = dgx_esp_2(:,-1,iesp)
     &          +dgx_esp_2(:,ny-1,iesp)
          dgy_esp_2(0:n1x,ny-1,iesp) = dgy_esp_2(:,-1,iesp)
     &          +dgy_esp_2(:,ny-1,iesp)
          dgz_esp_2(0:n1x,ny-1,iesp) = dgz_esp_2(:,-1,iesp)
     &          +dgz_esp_2(:,ny-1,iesp) 

          rho_esp_2(0:n1x,1,iesp) = rho_esp_2(:,1,iesp)
     &          +rho_esp_2(:,ny+1,iesp)
          cjx_esp_2(0:n1x,1,iesp) = cjx_esp_2(:,1,iesp)
     &          +cjx_esp_2(:,ny+1,iesp)
          cjy_esp_2(0:n1x,1,iesp) = cjy_esp_2(:,1,iesp)
     &          +cjy_esp_2(:,ny+1,iesp)
          cjz_esp_2(0:n1x,1,iesp) = cjz_esp_2(:,1,iesp)
     &          +cjz_esp_2(:,ny+1,iesp)

          dgx_esp_2(0:n1x,1,iesp) = dgx_esp_2(:,1,iesp)
     &          +dgx_esp_2(:,ny+1,iesp)
          dgy_esp_2(0:n1x,1,iesp) = dgy_esp_2(:,1,iesp)
     &          +dgy_esp_2(:,ny+1,iesp)
          dgz_esp_2(0:n1x,1,iesp) = dgz_esp_2(:,1,iesp)
     &          +dgz_esp_2(:,ny+1,iesp) 
     
            IF(ordre_interp.EQ.4) THEN
            rho_esp_2(0:n1x,ny-2,iesp) = rho_esp_2(:,-2,iesp)
     &          +rho_esp_2(:,ny-2,iesp)
            cjx_esp_2(0:n1x,ny-2,iesp) = cjx_esp_2(:,-2,iesp)
     &          +cjx_esp_2(:,ny-2,iesp)
            cjy_esp_2(0:n1x,ny-2,iesp) = cjy_esp_2(:,-2,iesp)
     &          +cjy_esp_2(:,ny-2,iesp)
            cjz_esp_2(0:n1x,ny-2,iesp) = cjz_esp_2(:,-2,iesp)
     &          +cjz_esp_2(:,ny-2,iesp)

            dgx_esp_2(0:n1x,ny-2,iesp) = dgx_esp_2(:,-2,iesp)
     &          +dgx_esp_2(:,ny-2,iesp)
            dgy_esp_2(0:n1x,ny-2,iesp) = dgy_esp_2(:,-2,iesp)
     &          +dgy_esp_2(:,ny-2,iesp)
            dgz_esp_2(0:n1x,ny-2,iesp) = dgz_esp_2(:,-2,iesp)
     &          +dgz_esp_2(:,ny-2,iesp) 

            rho_esp_2(0:n1x,2,iesp) = rho_esp_2(:,2,iesp)
     &          +rho_esp_2(:,ny+2,iesp)
            cjx_esp_2(0:n1x,2,iesp) = cjx_esp_2(:,2,iesp)
     &          +cjx_esp_2(:,ny+2,iesp)
            cjy_esp_2(0:n1x,2,iesp) = cjy_esp_2(:,2,iesp)
     &          +cjy_esp_2(:,ny+2,iesp)
            cjz_esp_2(0:n1x,2,iesp) = cjz_esp_2(:,2,iesp)
     &          +cjz_esp_2(:,ny+2,iesp)

            dgx_esp_2(0:n1x,2,iesp) = dgx_esp_2(:,2,iesp)
     &          +dgx_esp_2(:,ny+2,iesp)
            dgy_esp_2(0:n1x,2,iesp) = dgy_esp_2(:,2,iesp)
     &          +dgy_esp_2(:,ny+2,iesp)
            dgz_esp_2(0:n1x,2,iesp) = dgz_esp_2(:,2,iesp)
     &          +dgz_esp_2(:,ny+2,iesp)      
            ENDIF   
          ENDIF
        ENDDO



       return
!      _____________________       
       END SUBROUTINE edge_sources



       SUBROUTINE period_rho(iesp)
! ======================================================================
!
!
!
! ======================================================================
       USE domaines
       USE champs
       
       IMPLICIT NONE
       
       INTEGER, INTENT(IN)    ::   iesp


       rho_esp_2(0:n1x,0  ,iesp)= rho_esp_2(:,ny  ,iesp)
       rho_esp_2(0:n1x,n1y,iesp)= rho_esp_2(:,1   ,iesp)
       IF(ordre_interp.GE.2) THEN
         rho_esp_2(0:n1x,-1 ,iesp)= rho_esp_2(:,ny-1,iesp)
         IF(ordre_interp.EQ.4) THEN	 
           rho_esp_2(0:n1x,-2 ,iesp)= rho_esp_2(:,ny-2,iesp)	 
           rho_esp_2(0:n1x,ny+2 ,iesp)= rho_esp_2(:,2,iesp)	 
	 ENDIF
       ENDIF

       return
!      _____________________       
       END SUBROUTINE period_rho


       
       SUBROUTINE period_cj(iesp)
! ======================================================================
!
!
!
! ======================================================================
       USE domaines
       USE champs
       
       IMPLICIT NONE
       
       INTEGER, INTENT(IN)    ::   iesp
       

       cjx_esp_2(0:n1x,0  ,iesp) =cjx_esp_2(:,ny  ,iesp)
       cjx_esp_2(0:n1x,n1y,iesp) =cjx_esp_2(:,1   ,iesp)
 !
       cjy_esp_2(0:n1x,0  ,iesp) =cjy_esp_2(:,ny  ,iesp)
       cjy_esp_2(0:n1x,n1y,iesp) =cjy_esp_2(:,1   ,iesp)
 !
       cjz_esp_2(0:n1x,0  ,iesp) =cjz_esp_2(:,ny  ,iesp)
       cjz_esp_2(0:n1x,n1y,iesp) =cjz_esp_2(:,1   ,iesp)

       IF(ordre_interp.GE.2) THEN
         cjx_esp_2(0:n1x,-1 ,iesp) =cjx_esp_2(:,ny-1,iesp)
         cjy_esp_2(0:n1x,-1 ,iesp) =cjy_esp_2(:,ny-1,iesp)
         cjz_esp_2(0:n1x,-1 ,iesp) =cjz_esp_2(:,ny-1,iesp)
         IF(ordre_interp.EQ.4) THEN	
           cjx_esp_2(0:n1x,-2 ,iesp) =cjx_esp_2(:,ny-2,iesp)
           cjy_esp_2(0:n1x,-2 ,iesp) =cjy_esp_2(:,ny-2,iesp)
           cjz_esp_2(0:n1x,-2 ,iesp) =cjz_esp_2(:,ny-2,iesp)	

           cjx_esp_2(0:n1x,ny+2 ,iesp) =cjx_esp_2(:,2,iesp)
           cjy_esp_2(0:n1x,ny+2 ,iesp) =cjy_esp_2(:,2,iesp)
           cjz_esp_2(0:n1x,ny+2 ,iesp) =cjz_esp_2(:,2,iesp)		
	 ENDIF
       ENDIF

       return
!      _____________________       
       END SUBROUTINE period_cj


       SUBROUTINE period_dg(iesp)
! ======================================================================
!
!
!
! ======================================================================
       USE domaines
       USE champs
       
       IMPLICIT NONE
       
       INTEGER, INTENT(IN)    ::   iesp
       

       dgx_esp_2(0:n1x,0  ,iesp) =dgx_esp_2(:,ny  ,iesp)
       dgx_esp_2(0:n1x,n1y,iesp) =dgx_esp_2(:,1   ,iesp)
 !
       dgy_esp_2(0:n1x,0  ,iesp) =dgy_esp_2(:,ny  ,iesp)
       dgy_esp_2(0:n1x,n1y,iesp) =dgy_esp_2(:,1   ,iesp)
 !
       dgz_esp_2(0:n1x,0  ,iesp) =dgz_esp_2(:,ny  ,iesp)
       dgz_esp_2(0:n1x,n1y,iesp) =dgz_esp_2(:,1   ,iesp)

       IF(ordre_interp.GE.2) THEN
         dgx_esp_2(0:n1x,-1 ,iesp) =dgx_esp_2(:,ny-1,iesp)
         dgy_esp_2(0:n1x,-1 ,iesp) =dgy_esp_2(:,ny-1,iesp)
         dgz_esp_2(0:n1x,-1 ,iesp) =dgz_esp_2(:,ny-1,iesp)
         IF(ordre_interp.EQ.4) THEN	
           dgx_esp_2(0:n1x,-2 ,iesp) =dgx_esp_2(:,ny-2,iesp)
           dgy_esp_2(0:n1x,-2 ,iesp) =dgy_esp_2(:,ny-2,iesp)
           dgz_esp_2(0:n1x,-2 ,iesp) =dgz_esp_2(:,ny-2,iesp)
	
           dgx_esp_2(0:n1x,ny+2 ,iesp) =dgx_esp_2(:,2,iesp)
           dgy_esp_2(0:n1x,ny+2 ,iesp) =dgy_esp_2(:,2,iesp)
           dgz_esp_2(0:n1x,ny+2 ,iesp) =dgz_esp_2(:,2,iesp)
	 ENDIF
       ENDIF

       return
!      _____________________       
       END SUBROUTINE period_dg


	SUBROUTINE banbks(a,n,m1,m2,mp,al,mpl,indx,b)
! ======================================================================
!
!   Stockage inverse par rapport a la doc      
!
! ======================================================================
	
	

	IMPLICIT NONE
	 
	integer m1,m2,mp,mpl,n,indx(n)
	real(kind=8) a(mp,n),al(mpl,n),b(n)
	integer i,k,l,mm
	real(kind=8) dum
	mm=m1+m2+1
	if(mm.gt.mp.or.m1.gt.mpl) then
	write(6,*) 'bad args in banbks'
	stop
	endif
	l=m1
	DO k=1,n
	  i=indx(k)
	  if(i.ne.k)then
	   dum=b(k)
	   b(k)=b(i)
	   b(i)=dum
	  endif
	  if(l.lt.n)l=l+1
          DO i=k+1,l
	    b(i)=b(i)-al(i-k,k)*b(k)
	  ENDDO
	ENDDO
	l=1
	DO i=n,1,-1
	  dum=b(i)
	  DO k=2,l
	    dum=dum-a(k,i)*b(k+i-1)
	  ENDDO
	  b(i)=dum/a(1,i)
	  if(l.lt.mm) l=l+1
	ENDDO
	
	return
!       _____________________
	END SUBROUTINE banbks
       
       
       
	SUBROUTINE bandec(a,n,m1,m2,mp,al,mpl,indx,d)
! ======================================================================
!
!   Stockage inverse par rapport a la doc       
!
! ======================================================================

          

        IMPLICIT NONE

	! scotckage inverse par rapport a la doc       
	integer m1,m2,mp,mpl,n,indx(n)
	real(kind=8) d,a(mp,n),al(mpl,n),tiny
	parameter (tiny=1.e-20)
	integer i,j,k,l,mm
	real(kind=8) dum
	mm=m1+m2+1
	
	if(mm.gt.mp.or.m1.gt.mpl) Then
	  write(6,*)  'bad args in bandec'
          stop
	endif
	l=m1
	
	DO i=1,m1
	  DO j=m1+2-i,mm
            a(j-l,i)=a(j,i)
          ENDDO
	  l=l-1
	  DO j=mm-l,mm
	    a(j,i)=0.
          ENDDO
        ENDDO
        d=1.
        l=m1
	
        DO k=1,n
          dum=a(1,k)
          i=k
          if(l.lt.n)l=l+1

          DO j=k+1,l
            if(abs(a(1,j)).gt.abs(dum))then
              dum=a(1,j)
              i=j
            endif
          ENDDO

          indx(k)=i
          if(dum.eq.0.) a(1,k)=tiny
	
          if(i.ne.k)then
          d=-d
          DO j=1,mm
            dum=a(j,k)
            a(j,k)=a(j,i)
            a(j,i)=dum
          ENDDO
          endif
	  
          DO i=k+1,l
          dum=a(1,i)/a(1,k)
          al(i-k,k)=dum
            DO j=2,mm
              a(j-1,i)=a(j,i)-dum*a(j,k)
            ENDDO
          a(mm,i)=0.
          ENDDO

        ENDDO
	
	return
	
!       _____________________	
	END SUBROUTINE bandec
       
       
       
	
      SUBROUTINE tridia(ctri,atri,phitri,stri)
! ======================================================================
!
!
!
! ======================================================================
      
       USE domaines
 
 
       IMPLICIT NONE
       
       INTEGER    ::   i

!       real(kind=8) alfa(nx), beta(nx)
!       real(kind=8) stri(nx), ctri(nx), atri(nx)
!       real(kind=8) phitri(nx)

       REAL(kind=kr), DIMENSION(1:nx)    :: 
     &    alfa, beta    
       REAL(kind=kr), DIMENSION(1:nx), INTENT(IN)    :: 
     &    stri, ctri, atri  
       REAL(kind=kr), DIMENSION(1:nx), INTENT(INOUT)    :: 
     &    phitri     

 !
 !     calcul de la decomposition l u
 !     -------------------------------
       alfa(1)=1./atri(1)
       DO i=2,nxm1
         beta(i)=-ctri(i)*alfa(i-1)
         alfa(i)=1./(atri(i)+ctri(i)*beta(i))
       ENDDO
 
 !
 !     inversion
 !     ---------
       phitri(1)=stri(1)
       DO i=2,nxm1
         phitri(i)=stri(i)-beta(i)*phitri(i-1)
       ENDDO
    
       phitri(nxm1)=phitri(nxm1)*alfa(nxm1)
       
       DO i=nx-2,1,-1
         phitri(i)=(phitri(i)+ctri(i+1)*phitri(i+1))*alfa(i)
       ENDDO
    
       return       
!      ______________________
       END  SUBROUTINE tridia
       
	
	
      SUBROUTINE minmax_gl(nbp, tableau, min, max)
! ======================================================================
! Calcul des min et max de "tableau" sur l'ensemble des PE
! et redistribution des resultats
! ======================================================================

      USE domaines
      USE erreurs

      IMPLICIT NONE
!!      INCLUDE 'mpif.h'

      INTEGER, INTENT(IN)                       :: nbp
      REAL(KIND=kr), DIMENSION(nbp), INTENT(IN) :: tableau
      REAL(KIND=kr), INTENT(OUT)                :: min, max

      REAL(KIND=kr)                  :: min_loc, max_loc

      min_loc = MINVAL(tableau)
      max_loc = MAXVAL(tableau)

      IF(nproc.GT.1) THEN
!        CALL MPI_ALLREDUCE(min_loc, min, 1, MPI_CALDER_REAL,
!     &                     MPI_MIN, MPI_COMM_WORLD, info)
!        CALL MPI_ALLREDUCE(max_loc, max, 1, MPI_CALDER_REAL,
!     &                     MPI_MAX, MPI_COMM_WORLD, info)
      ELSE
        min = min_loc
        max = max_loc
      ENDIF
      
      RETURN
!     ________________________
      END SUBROUTINE minmax_gl
   
   
   
      SUBROUTINE inverse(tab,nbp,xmax,yval,xval)
! ======================================================================
! Inversion d'un tableau : donne xval a partir de yval fourni
! On suppose tab(0)=0 et xval compris sur (0,xmax)
! ======================================================================
      USE precisions
      IMPLICIT NONE

      INTEGER                      , INTENT(IN)   :: nbp
      REAL(KIND=kr), DIMENSION(nbp), INTENT(IN)   :: tab
      REAL(KIND=kr)                , INTENT(IN)   :: xmax, yval
      REAL(KIND=kr)                , INTENT(OUT)  :: xval
      INTEGER       :: i

      DO i=1,nbp
        IF(tab(i).GE.yval) EXIT
      ENDDO
      IF(i.GT.nbp) THEN
        xval = xmax
      ELSEIF(i.EQ.1) THEN
        xval = xmax/nbp*( yval/tab(i) )
      ELSE
        xval = xmax/nbp*(i - 1. + (yval-tab(i-1))/(tab(i)-tab(i-1)) )
      ENDIF
      RETURN
!     ______________________
      END SUBROUTINE inverse
   

      SUBROUTINE ecrit_tab1d(num_fi, taillx, tableau)
! ======================================================================
! Ecrit sur num_fi le tableau local tableau
! ======================================================================
      USE precisions
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: num_fi, taillx
      REAL(KIND=kr), DIMENSION(taillx), INTENT(IN) :: tableau

      WRITE(num_fi,'(8(1X,1PE10.3))') tableau(:)
      RETURN
!     __________________________
      END SUBROUTINE ecrit_tab1d


      SUBROUTINE ecrit_tab2d(num_fi, taillx, tailly, tableau)
! ======================================================================
! Ecrit sur num_fi le tableau local tableau
! ======================================================================
      USE precisions
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: num_fi, taillx, tailly
      REAL(KIND=kr), DIMENSION(taillx,tailly), INTENT(IN) :: tableau

      INTEGER  :: iy

      DO iy=1,tailly
        WRITE(num_fi,'(8(1X,1PE10.3))') tableau(:,iy)
      ENDDO
      RETURN
!     __________________________
      END SUBROUTINE ecrit_tab2d


      SUBROUTINE ecrit_tab3d(num_fi, taillx, tailly, taillz, tableau)
! ======================================================================
! Ecrit sur num_fi le tableau local tableau
! ======================================================================
      USE precisions
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: num_fi, taillx, tailly, taillz
      REAL(KIND=kr), DIMENSION(taillx,tailly,taillz), INTENT(IN) 
     &                    :: tableau

      INTEGER  :: iy, iz

      DO iz=1,taillz
        DO iy=1,tailly
          WRITE(num_fi,'(8(1X,1PE10.3))') tableau(:,iy,iz)
        ENDDO
      ENDDO
      RETURN
!     __________________________
      END SUBROUTINE ecrit_tab3d
      
	
	
              
