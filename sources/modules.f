! ======================================================================
! modules.f
! ---------
!
!      MODULE champs
!      MODULE champs_iter
!      MODULE diagnostics
!      MODULE diagnostics_klder
!      MODULE diag_moyen
!      MODULE diag_ptest
!      MODULE domaines
!      MODULE domaines_klder
!      MODULE erreurs
!      MODULE laser
!      MODULE particules
!      MODULE particules_klder
!      MODULE precisions
!      MODULE repar
!      MODULE temps
!
! ======================================================================


      MODULE precisions
! ======================================================================
!
!
!
! ======================================================================
      IMPLICIT NONE
!      INCLUDE 'mpif.h'

      INTEGER, PARAMETER :: kr = 8


!     _____________________
      END MODULE precisions


       MODULE domaines
! ========================================================
!
! Taille de la maille en x, y et z, et inverses
! Taille en mailles du domaine global puis local
! Premiere et derniere mailles du domaine local en x, y, z
!
! Positions des frontieres du domaine global
! Conditions aux bords des differents domaines (particules, champs)
! Positions des frontieres des domaines
!
!
! ========================================================
       USE precisions

       IMPLICIT NONE


       INTEGER, SAVE      ::
     &          nx, ny,
     &             n1x, n1y,
     &             nxm1, n2x, n2y, n2xf2,
     &             nxs2, nys2, nxm1s2,
     &             nxm1f2, nxm1f3,
     &             nys2m1, nys2p1, nys2p2,
     &          nvect,
     &             nvf28,
     &             nxm1f6, ndima, nxny,
     &             ngmax,
     &          nfreq,
     &             nda1d,
     &             ndimam, ndim1d,
     &             ngf4, ngf7, ngf14

       INTEGER, PARAMETER     ::    numproc=1, nproc=1
       INTEGER, PARAMETER     ::    nprocmax=1

       INTEGER, PARAMETER    ::
     &  nprocx=1, nprocy=1, nprocz=1,
     &  numprocx=1, numprocy=1, numprocz=1

       INTEGER, SAVE    ::
     &   nulmx, nulpx, nulmy, nulpy, nulmz, nulpz

       REAL(kind=8),PARAMETER ::  pi = 3.14159265359

       INTEGER, SAVE  ::  isti1,isti2,ndebi,iste1,iste2,ndebe,istop1,
     &              istop2

       INTEGER, SAVE       :: dim_cas

       INTEGER, DIMENSION(2), PARAMETER ::  ifaz=(/ 10000,12 /)

       REAL(KIND=kr), SAVE ::
     &               irh1,irh2,irh3,irh4,irh5,xgrad,
     &               rho1,rho2,rho3,rho4,rho5

       INTEGER, SAVE       :: nbmx,     nbmy,     nbmz
       INTEGER, SAVE       :: nbmx_gl,  nbmy_gl,  nbmz_gl

       REAL(KIND=kr), SAVE :: pasdx, pasdy, pasdz, xvide, yvide,
     &             pasdxi, pasdyi, pasdzi
       REAL(KIND=kr), SAVE :: xmin,  xmax,  ymin, ymax, zmin, zmax
       REAL(KIND=kr), SAVE :: xmin_gl,xmax_gl, ymin_gl,ymax_gl,
     &                      zmin_gl,zmax_gl

       INTEGER, SAVE    ::   tta_def, iptarf

       REAL(KIND=kr), SAVE   ::   thetad, thetads2

       REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE, SAVE ::
     &           thetad_2

       CHARACTER(LEN=5), DIMENSION(:), ALLOCATABLE, SAVE ::
     &          tta_x_profn, tta_y_profn, tta_z_profn
       REAL(KIND=kr),    DIMENSION(:), ALLOCATABLE, SAVE ::
     &          tta_centrex, tta_centrey, tta_centrez
       REAL(KIND=kr),    DIMENSION(:), ALLOCATABLE, SAVE ::
     &          tta_largx, tta_lgrmx, tta_lgrpx, tta_nrmx, tta_nrpx
       REAL(KIND=kr),    DIMENSION(:), ALLOCATABLE, SAVE ::
     &          tta_largy, tta_lgrmy, tta_lgrpy, tta_nrmy, tta_nrpy
       REAL(KIND=kr),    DIMENSION(:), ALLOCATABLE, SAVE ::
     &          tta_largz, tta_lgrmz, tta_lgrpz, tta_nrmz, tta_nrpz

       INTEGER, SAVE     ::   sizeabs,sizebuf

       INTEGER      , DIMENSION(6,nprocmax), SAVE :: ibord
       REAL(KIND=kr), DIMENSION(6,nprocmax), SAVE ::  bord

       INTEGER, SAVE :: nb_vidx, nb_vidy, nb_vidz

       INTEGER, DIMENSION(6)  , SAVE   :: ibndc
       INTEGER, DIMENSION(6,1), SAVE   :: ibndp

       REAL(KIND=kr), SAVE ::     nbm_plas

       INTEGER, SAVE       :: ordre_interp
       INTEGER, SAVE :: l_oset, c_oset, r_oset

 !     ___________________
       END MODULE domaines



      MODULE domaines_klder
! ======================================================================
!
! Coordonnees du centre du plasma
! Taille du plasma et des rampes de densite dans les 3 directions
!
! Flag, amplitude et parametres de la modulation de densite
! Flag, x de debut et fin, et vitesses de la derive du plasma
! Flag, instants de debut et fin, et vitesse de la "moving window"
!
! Defaut de charge electrique et flux de charge electronique aux bords
!
! Nombre de mailles vides au bout du domaine
!
! Nombre de mailles de d�passement, on utilise c_oset pour les grandeurs centr�es
! sur les mailles primales telles que la densit� de charge
! et l_oset, r_oset pour les grandeurs calcul�es sur des mailles duales
!
! Creation ---  07-98 --- E. Lefebvre
! ======================================================================
      USE precisions

      IMPLICIT NONE


!
      INTEGER, SAVE        :: nprof
      CHARACTER(LEN=5), DIMENSION(:), ALLOCATABLE, SAVE ::
     &                            x_profn, y_profn, z_profn
      REAL(KIND=kr),    DIMENSION(:), ALLOCATABLE, SAVE ::
     &                            centrex, centrey, centrez
      REAL(KIND=kr),    DIMENSION(:), ALLOCATABLE, SAVE ::
     &                            largx, lgrmx, lgrpx, nrmx, nrpx
      REAL(KIND=kr),    DIMENSION(:), ALLOCATABLE, SAVE ::
     &                            largy, lgrmy, lgrpy, nrmy, nrpy
      REAL(KIND=kr),    DIMENSION(:), ALLOCATABLE, SAVE ::
     &                            largz, lgrmz, lgrpz, nrmz, nrpz
!

      REAL(KIND=kr),    DIMENSION(:,:), ALLOCATABLE, SAVE ::
     &                            poids3_bordmx,poids3_bordpx,
     &                            poids3_bordmy,poids3_bordpy,
     &                            poids3_bordmz,poids3_bordpz
      REAL(KIND=kr),    DIMENSION(:), ALLOCATABLE, SAVE ::
     &                            poids2_bordmx,poids2_bordpx,
     &                            poids2_bordmy,poids2_bordpy
      REAL(KIND=kr), SAVE :: poids1_bordmx,poids1_bordpx

!
      INTEGER,       SAVE :: der_ok
      REAL(KIND=kr), DIMENSION(2), SAVE :: der_x, der_vx, der_vy, der_vz

      INTEGER, PARAMETER   :: gliss_ok=0
!
      REAL(KIND=kr), DIMENSION(6), SAVE :: def_charg     = 0.,
     &                                     def_elec      = 0.,
     &                                     def_charg_tot = 0.


!     ___________________
      END MODULE domaines_klder



       MODULE diagnostics
! ========================================================
!
!
!
!
! ========================================================
       USE precisions

       IMPLICIT NONE


       INTEGER, SAVE  ::  IDecin2, IDeemg2
       INTEGER, SAVE  ::  IDchmpex_x, IDchi, IDzeta
       INTEGER, SAVE  ::  IDchirel, IDzetarel

       INTEGER, SAVE  ::  IDxe_t, IDye_t,
     &               IDvxe_t, IDvye_t, IDvze_t

       INTEGER, SAVE  ::  IDxi_t, IDyi_t,
     &               IDvxi_t, IDvyi_t, IDvzi_t

       INTEGER, SAVE  ::  IDgae_t, IDgai_t

       INTEGER, SAVE  ::  IDxrel, IDyrel, IDzrel, IDtrel

       CHARACTER(LEN=8), SAVE   :: f_chmpex_x, f_chi, f_zeta
       CHARACTER(LEN=8), SAVE   :: f_chirel, f_zetarel

       CHARACTER(LEN=*), PARAMETER   :: wforma ='(1E17.10)'
       CHARACTER(LEN=*), PARAMETER   :: wforma2='( 9(1PE12.4,1X))'

       CHARACTER(LEN=8), SAVE   ::
     &   f_xet, f_yet, f_vxet, f_vyet, f_vzet,
     &   f_xit, f_yit, f_vxit, f_vyit, f_vzit,
     &   f_gaet, f_gait

       CHARACTER(LEN=8), SAVE   ::
     &   f_xrel, f_yrel, f_zrel, f_trel

       REAL(KIND=kr), SAVE    ::  esqx, esqy, esqz,
     &     bsqx, bsqy, bsqz

       REAL(KIND=kr), DIMENSION(:), ALLOCATABLE, SAVE ::
     &         welec, wmagn, enert, dcin

!      ________________
       END MODULE diagnostics



      MODULE diag_ptest
! ======================================================================
! Parametres pour le diagnostic des particules-test
! ======================================================================
       USE precisions
       IMPLICIT NONE

! --- flags de choix
       INTEGER, SAVE        :: dpt_ok
! --- numeros du fichier de particules-test
       INTEGER, SAVE        :: dpt_fic
! --- nombre de particules-test de chaque espece
       INTEGER, DIMENSION(:), ALLOCATABLE, SAVE      :: dpt_nbpt
! --- nombre de blocs de particules-test a selectionner
       INTEGER, SAVE        :: dpt_nblocs
! -- Parametres de chaque bloc
!    Numero d'espece et Probabilite de selection
       INTEGER, DIMENSION(:), ALLOCATABLE, SAVE       :: dpt_bl_esp
       REAL(KIND=kr), DIMENSION(:), ALLOCATABLE, SAVE :: dpt_bl_proba
       REAL(KIND=kr), DIMENSION(:), ALLOCATABLE, SAVE ::
     &                            dpt_bl_xcn, dpt_bl_ycn, dpt_bl_zcn,
     &                            dpt_bl_xrn, dpt_bl_yrn, dpt_bl_zrn,
     &                            dpt_bl_emn, dpt_bl_emx
!     _____________________
      END MODULE diag_ptest



       MODULE diagnostics_klder
! ======================================================================
! Type de format des diagnostics
! Nombre de diagnostics (1d_t, champs, espaces des phases 2d)
! Parametres des diagnostics spatiaux
! Parametres des diagnostics esp_phase 2d
! Alias pour diagnostics 1d
! Tableau de diagnostics 1d
!
! Creation ---  08-98 --- E. Lefebvre
! Modif    ---  07-05 --- L. Gremillet (dph_struct,d_fp[xyz],d_p[xyz])
! ======================================================================
      USE precisions
      IMPLICIT NONE

! --- type d'ecriture des fichiers de diagnostics
      CHARACTER(LEN=3), SAVE       :: diag_typ
! --- nombres de diagnostics
      INTEGER, SAVE                :: d1d_nb, dsp_nb, dsp_nbmoy,
     &                                        dph_nb

! --- type structure et declaration : diagnostic spatial 1D/2D/3D
      TYPE dsp_struct
        INTEGER                     :: typ, per, moy
        REAL(KIND=kr), DIMENSION(3) :: min, max
        REAL(KIND=kr)               :: xmin_ref
        INTEGER, DIMENSION(3)       :: imn, imx, siz
      END TYPE dsp_struct
      TYPE (dsp_struct), DIMENSION(:), ALLOCATABLE, SAVE ::  dsp

! --- nombres de types differents de diagnostics spatiaux
      INTEGER, PARAMETER           :: dsp_typ_nb = 11
      CHARACTER(LEN=2), DIMENSION(dsp_typ_nb), PARAMETER :: dsp_typ_map=
     & (/ 'ex','ey','ez','bx','by','bz','cx','cy','cz','rh','ta' /)
      INTEGER,          DIMENSION(dsp_typ_nb), PARAMETER :: dsp_typ_fic=
     & (/ 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50 /)

! --- type structure et declaration : espace des phases 2D/3D
      TYPE dph_struct
        CHARACTER(LEN=2), DIMENSION(3)  :: axe
        LOGICAL                         :: ok
        INTEGER                         :: fic, per
        REAL(KIND=kr), DIMENSION(3)     :: min, max
        INTEGER, DIMENSION(3)           :: siz
	REAL(KIND=kr), DIMENSION(2)     :: ener   ! ajout LG 19/07/05
      END TYPE dph_struct
      TYPE (dph_struct), DIMENSION(:), ALLOCATABLE, SAVE ::  dph

! --- champs locaux
      INTEGER,                                     SAVE :: clo_nb
      CHARACTER(LEN=3), DIMENSION(:), ALLOCATABLE, SAVE :: clo_ch
      REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE, SAVE  :: clo_pos
      INTEGER,      DIMENSION(:,:), ALLOCATABLE, SAVE   :: clo_ind

! --- champs max
      INTEGER,                                     SAVE :: cmx_nb
      CHARACTER(LEN=3), DIMENSION(:), ALLOCATABLE, SAVE :: cmx_ch

! --- numeros des fichiers (et numeros du 1er et dernier diag)
! impulsions
! Flux d'impulsion en x,y,z (LG 01/06)
      INTEGER, DIMENSION(3), SAVE :: d1d_poid,  d1d_ecin,
     &                               d1d_impx,  d1d_impy,  d1d_impz,
     &                               d1d_fimpx, d1d_fimpy, d1d_fimpz,
     &                               d1d_poidf,
     &                               d1d_ecinf, d1d_ecinr,
     &                               d1d_eemg,  d1d_eemgg,
     &                               d1d_clo,   d1d_eptg,  d1d_pois,
     &                               d1d_cmx

! --- d_fp[xyz] Flux d'impulsion en x,y,z (LG 01/06)
      INTEGER, DIMENSION(:), ALLOCATABLE, SAVE ::
     &                d_po,  d_ki,
     &                d_px,  d_py,  d_pz,
     &                d_fpx, d_fpy, d_fpz,
     &                d_pof, d_kif, d_kir

      INTEGER, SAVE   :: d_ex,  d_ey,  d_ez,  d_bx,  d_by,  d_bz
      INTEGER, SAVE   :: d_exg, d_eyg, d_ezg, d_bxg, d_byg, d_bzg
      INTEGER, SAVE   :: d_clo        ! - diagnostics de champs locaux
      INTEGER, SAVE   :: d_pmx, d_ppx, d_pmy, d_ppy, d_pmz, d_ppz
      INTEGER, SAVE   :: d_psr, d_ptg ! - d_ptg=numero dernier diag Ptng
      INTEGER, SAVE   :: d_psn, d_gau ! - numero diags Poisson et Gauss
      INTEGER, SAVE   :: d_cmx        ! - diagnostics de champs max

      REAL(KIND=kr), DIMENSION(:), ALLOCATABLE        :: d1d_som
      REAL(KIND=kr), DIMENSION(:), ALLOCATABLE, SAVE  :: d1d_loc

!      ____________________________
       END MODULE diagnostics_klder


      MODULE diag_moyen
c ======================================================================
c Tableau des champs a memoriser pour les diagnostics moyens
c - champs (E, B, rho...)
c - espaces des phases (ou assimiles) 1D
c
c Nombre de pas de temps pour les moyennes
c ======================================================================
      USE precisions
      IMPLICIT NONE

      REAL(KIND=kr), DIMENSION(:,:,:,:), ALLOCATABLE, SAVE :: dsp_mem
      REAL(KIND=kr), DIMENSION(:,:,:),   ALLOCATABLE, SAVE :: dph1d_mem
!
      INTEGER, SAVE  :: dsp_moy, dph_moy
!     _____________________
      END MODULE diag_moyen


      MODULE laser
! ======================================================================
! Parametres de la source laser introduite dans la boite
! *_prof : nom de code du profil
! *_ord  : ordre du profil
! *_deb  : coordonnee du profil
! *_lrg  : extension caracteristique du profil
! *_max  : coordonnee du point culminant du profil
! *_ang  : direction de propagation de l'onde incidente
!
! Creation ---  08-98 --- E. Lefebvre
! ======================================================================
      USE precisions
      IMPLICIT NONE

      INTEGER, SAVE     ::    nulx_inj

      REAL(KIND=kr), SAVE    ::  scat_angle
      REAL(KIND=kr), SAVE    ::  in_angle

! --- Cas particulier pour une source avec fonction de hermite
      REAL(KIND=kr), DIMENSION(3), SAVE     ::
     &                a1_her_y, a2_her_y,
     &                a1_her_z, a2_her_z

! --- On prevoit jusqu'a trois sources
      INTEGER, DIMENSION(3), SAVE           :: inj_laser
      REAL(KIND=kr), DIMENSION(3), SAVE     :: a_0, lambda, polar
      CHARACTER(LEN=5), DIMENSION(3), SAVE  :: t_prof, y_prof, z_prof
      INTEGER, DIMENSION(3), SAVE           :: t_ord,  y_ord,  z_ord
      REAL(KIND=kr), DIMENSION(3), SAVE     :: t_lrg,  y_lrg,  z_lrg
      REAL(KIND=kr), DIMENSION(3), SAVE     :: t_deb,  y_deb,  z_deb
      REAL(KIND=kr), DIMENSION(3), SAVE     :: t_max,  y_max,  z_max
      REAL(KIND=kr), DIMENSION(3), SAVE     ::         y_ang,  z_ang
      REAL(KIND=kr), DIMENSION(3), SAVE     :: y_dpeak, z_dpeak
      REAL(KIND=kr), DIMENSION(3), SAVE     :: usak,   y_pol,  z_pol
!     ________________
      END MODULE laser



       MODULE temps
! ========================================================
!
!
!
!
! ========================================================
       USE precisions

       IMPLICIT NONE

          REAL(KIND=kr), SAVE ::
     &          dt,dx,dy,dt2,dx2,dy2,dxdy,
     &          dxi,dyi,dx2i,dy2i,dxdyi,dxdyi4,
     &          dxf2i,dxf4i,dyf2i,dyf4i,dts2,dts4,
     &          dt2s2,dt2s4,dt2s8,
     &          dtsdx2,dtsdy2,
     &          dtsdx,dtsdy,dts2dx,dts2dy,dts4dx,dts4dy

       REAL(KIND=kr), SAVE ::    d3, d11, d12, d13


! ---- constantes pour le sch�ma de base E= -B
! ---- au bord gauche
       REAL(KIND=kr), SAVE :: keb1, keb2, keb3, keb4, keb5
       REAL(KIND=kr), SAVE :: keb_a, keb_b
       REAL(KIND=kr), SAVE :: kebey1, kebey2, kebey3, kebey4
       REAL(KIND=kr), SAVE :: kebez1, kebez2, kebez3

       REAL(KIND=kr), SAVE :: kebqx1, kebqx2, kebqx3, kebqx4
       REAL(KIND=kr), SAVE :: kebqy1, kebqy2, kebqy3, kebqy4
       REAL(KIND=kr), SAVE :: kebqz1, kebqz2, kebqz3

! ---- Sch�ma � la Klder (version 1) au bord gauche

       REAL(KIND=kr), SAVE :: vex1, vex2, vqx1, vqx2, vqx3
       REAL(KIND=kr), SAVE :: vex1tild, vex2tild
       REAL(KIND=kr), SAVE :: vqx1tild, vqx2tild,
     &                        vqx3tild, vqx4tild

       REAL(KIND=kr), SAVE :: vey1tild, vey2tild
       REAL(KIND=kr), SAVE :: vqy1tild, vqy2tild,
     &                        vqy3tild, vqy4tild

       REAL(KIND=kr), SAVE :: deltakeb
       REAL(KIND=kr), SAVE :: vkebz1, vkebz2, vkebz3, vkebz4
       REAL(KIND=kr), SAVE :: vkebz1tild, vkebz2tild,
     &                        vkebz3tild, vkebz4tild

! ---- Sch�ma � la Klder (version 1) au bord droit

       REAL(KIND=kr), SAVE :: veyr1, veyr2
       REAL(KIND=kr), SAVE :: vqxr1, vqxr2, vqxr3,
     &        vqxr1tild, vqxr2tild, vqxr3tild
       REAL(KIND=kr), SAVE :: vexr1tild, vexr2tild
       REAL(KIND=kr), SAVE :: veyr1tild, veyr2tild
       REAL(KIND=kr), SAVE ::
     &        vqyr1tild, vqyr2tild, vqyr3tild
       REAL(KIND=kr), SAVE :: vezr1, vqzr1, vqzr2,
     &    vezr1tild, vqzr1tild, vqzr2tild


! ---- Sch�ma � la Klder (version 2) au bord gauche

       REAL(KIND=kr), SAVE :: deltaf
       REAL(KIND=kr), SAVE :: fkebz1, fkebz2, fkebz3
       REAL(KIND=kr), SAVE :: fkebz1tild, fkebz2tild,
     &                        fkebz3tild

       REAL(KIND=kr), SAVE :: deltaff
       REAL(KIND=kr), SAVE :: ffkebz1, ffkebz2, ffkebz3, ffkebz4
       REAL(KIND=kr), SAVE :: ffkebz1tild, ffkebz2tild,
     &                        ffkebz3tild, ffkebz4tild

! ---- constantes pour le sch�ma de base E= -B
! ---- au bord droit
       REAL(KIND=kr), SAVE :: kebr1, kebr2

       REAL(KIND=kr), SAVE :: kebqxr1, kebqxr2, kebqxr3, kebqxr4
       REAL(KIND=kr), SAVE :: kebqyr1, kebqyr2, kebqyr3, kebqyr4
       REAL(KIND=kr), SAVE :: kebqzr1, kebqzr2, kebqzr3

       REAL(KIND=kr), SAVE :: kebrex1, kebrex2
       REAL(KIND=kr), SAVE :: kebrey1, kebrey2
       REAL(KIND=kr), SAVE :: kebrez1


       INTEGER, SAVE         :: itdeb, itfin, iter
       REAL(KIND=kr), SAVE   :: pasdt, pasdts2
!      ______________
       END MODULE temps



       MODULE champs
! ========================================================
!
!
!
!
! ========================================================
       USE precisions

        IMPLICIT NONE


        REAL(kind=kr), SAVE   ::   fpsr = 0.

        REAL(KIND=kr), SAVE   ::   anmax,epsmax,bx0,by0

        REAL(KIND=kr), SAVE   ::   cb_45, ce_45

        REAL(kind=kr), SAVE   ::   ones2

        REAL(KIND=kr), SAVE   ::   ttapushs2

	INTEGER, SAVE   ::  harmonicfield
	INTEGER, SAVE   ::  simplified, friction_tta

        REAL(KIND=kr), SAVE   ::   x_damp1, x_damp2
	INTEGER, SAVE    :: i_damp1, i_damp2

! ---- couplage des conditions aux limites
!      1 pour implicite complet, 3 pour explicite/implicite
	INTEGER, SAVE      :: boundaries

        REAL(KIND=kr), PARAMETER     ::  x_dipol=2.0d0

        REAL(KIND=kr), DIMENSION(:), ALLOCATABLE, SAVE ::   bz0(:)

        REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE, SAVE ::
     &              bx(:,:),by(:,:),bz(:,:),
     &              ex(:,:),ey(:,:),ez(:,:)

        REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE, SAVE ::
     &      dexdx(:,:), dexdy(:,:),
     &      deydx(:,:), deydy(:,:),
     &      dezdx(:,:), dezdy(:,:)

        REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE, SAVE ::
     &              extm1(:,:),eytm1(:,:),eztm1(:,:)

        REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE, SAVE ::
     &              bx_1d(:),by_1d(:),bz_1d(:),
     &              ex_1d(:),ey_1d(:),ez_1d(:)

        REAL(KIND=kr), DIMENSION(:), ALLOCATABLE, SAVE ::
     &               exm(:),bym(:),bzm1(:),
     &               eym(:),bxm(:),bzm2(:)

        REAL(KIND=kr), DIMENSION(:), ALLOCATABLE, SAVE ::
     &      extm1m(:), eytm1m(:)


        REAL(kind=kr), DIMENSION(:,:), ALLOCATABLE, SAVE ::
     &     bx_tn(:,:), by_tn(:,:), bz_tn(:,:)

        REAL(KIND=kr), DIMENSION(:,:,:), ALLOCATABLE, SAVE ::
     &          rho_esp_2(:,:,:),cjx_esp_2(:,:,:),cjy_esp_2(:,:,:),
     &          cjz_esp_2(:,:,:),dgx_esp_2(:,:,:),dgy_esp_2(:,:,:),
     &          dgz_esp_2(:,:,:)

        REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE, SAVE ::
     &        jxtot(:,:), jytot(:,:), jztot(:,:)

        REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE, SAVE ::
     &           phi(:,:),s(:,:)

        REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE, SAVE  ::
     &                xxx(:,:),xxy(:,:),xxz(:,:),
     &               xyx(:,:),xyy(:,:),xyz(:,:),
     &               xzx(:,:),xzy(:,:),xzz(:,:)

        REAL(KIND=kr), DIMENSION(:,:,:), ALLOCATABLE, SAVE  ::
     &       xxx_esp(:,:,:),xxy_esp(:,:,:),xxz_esp(:,:,:),
     &       xyx_esp(:,:,:),xyy_esp(:,:,:),xyz_esp(:,:,:),
     &       xzx_esp(:,:,:),xzy_esp(:,:,:),xzz_esp(:,:,:)

! ---- tenseurs utiles pour diags relativistes
! ---- d�but diags relativistes
	    REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE, SAVE  ::
     &               xxxr1(:,:),xxyr1(:,:),xxzr1(:,:),
     &               xyxr1(:,:),xyyr1(:,:),xyzr1(:,:),
     &               xzxr1(:,:),xzyr1(:,:),xzzr1(:,:)

	    REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE, SAVE  ::
     &               xxxr2(:,:),xxyr2(:,:),xxzr2(:,:),
     &               xyxr2(:,:),xyyr2(:,:),xyzr2(:,:),
     &               xzxr2(:,:),xzyr2(:,:),xzzr2(:,:)

	    REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE, SAVE  ::
     &               zxxr1(:,:),zxyr1(:,:),zxzr1(:,:),
     &               zyxr1(:,:),zyyr1(:,:),zyzr1(:,:),
     &               zzxr1(:,:),zzyr1(:,:),zzzr1(:,:)

	    REAL(KIND=kr), DIMENSION(:), ALLOCATABLE, SAVE  ::
     &               xxx0r1(:),xxy0r1(:),xxz0r1(:),
     &               xyx0r1(:),xyy0r1(:),xyz0r1(:),
     &               xzx0r1(:),xzy0r1(:),xzz0r1(:)

	    REAL(KIND=kr), DIMENSION(:), ALLOCATABLE, SAVE  ::
     &               xxx0r2(:),xxy0r2(:),xxz0r2(:),
     &               xyx0r2(:),xyy0r2(:),xyz0r2(:),
     &               xzx0r2(:),xzy0r2(:),xzz0r2(:)

	    REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE, SAVE  ::
     &               zxx0r1(:),zxy0r1(:),zxz0r1(:),
     &               zyx0r1(:),zyy0r1(:),zyz0r1(:),
     &               zzx0r1(:),zzy0r1(:),zzz0r1(:)

! ---- tenseurs utiles pour diags relativistes
! ---- fin diags relativistes

	    REAL(KIND=kr), DIMENSION(:), ALLOCATABLE, SAVE  ::
     &               xxx0(:),xxy0(:),xxz0(:),
     &               xyx0(:),xyy0(:),xyz0(:),
     &               xzx0(:),xzy0(:),xzz0(:)

	    REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE, SAVE  ::
     &                zrx(:,:),zry(:,:),
     &                zrz(:,:),
     &                zxx(:,:),zxy(:,:),zxz(:,:),
     &                zyx(:,:),zyy(:,:),zyz(:,:),
     &                zzx(:,:),zzy(:,:),zzz(:,:)

	    REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE, SAVE  ::
     &         zxx_esp(:,:,:),zxy_esp(:,:,:),zxz_esp(:,:,:),
     &         zyx_esp(:,:,:),zyy_esp(:,:,:),zyz_esp(:,:,:),
     &         zzx_esp(:,:,:),zzy_esp(:,:,:),zzz_esp(:,:,:)

            REAL(KIND=kr), DIMENSION(:), ALLOCATABLE, SAVE  ::
     &                zxx0(:),zxy0(:),zxz0(:),
     &                zyx0(:),zyy0(:),zyz0(:),
     &                zzx0(:),zzy0(:),zzz0(:)

            REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE, SAVE ::
     &        zeta(:,:),z(:,:)

       REAL(KIND=kr), DIMENSION(:), ALLOCATABLE, SAVE ::
     &                qxbound(:),qybound(:),qzbound(:)

       REAL(KIND=kr), DIMENSION(:), ALLOCATABLE, SAVE ::
     &                qxnx(:),qynxm1(:),qznxm1(:)

       REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE, SAVE ::
     &                qx(:,:),qy(:,:),qz(:,:),
     &               emoyx(:,:),emoyy(:,:),emoyz(:,:),
     &               zetax(:,:),zetay(:,:),
     &               zetaz(:,:)

       REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE, SAVE ::
     &                ebx(:,:),eby(:,:),ebz(:,:),
     &                ebbx(:,:),ebby(:,:),ebbz(:,:)

       REAL(KIND=kr), DIMENSION(:), ALLOCATABLE, SAVE ::
     &               csym1(:),csyp1(:),
     &               sny(:),cosy(:),cwk(:)

       REAL(KIND=kr), SAVE ::
     &               anpot,
     &               epspot,rhomax

       INTEGER, SAVE   ::   nbpot, nbmax

! --- pour le 1d, 1d1/2, 1d2/2
      REAL(KIND=kr), DIMENSION(:), ALLOCATABLE, SAVE ::
     &                        ex_1, ey_1, ez_1,
     &                        bx_1, by_1, bz_1, bx_1p, by_1p, bz_1p,
     &                        rh_1,
     &                        cx_1, cy_1, cz_1

!      REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE, SAVE ::
!     &                        rh_esp_1,
!     &                        cx_esp_1, cy_esp_1, cz_esp_1
! --- pour le 2d, 2d1/2
      REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE, SAVE ::
     &                        ex_2, ey_2, ez_2,
     &                        bx_2, by_2, bz_2, bx_2p, by_2p, bz_2p,
     &                        rh_2, rh_2_prec,
     &                        cx_2, cy_2, cz_2

!      REAL(KIND=kr), DIMENSION(:,:,:), ALLOCATABLE, SAVE ::
!     &                        rh_esp_2,
!     &                        cx_esp_2, cy_esp_2, cz_esp_2
! --- pour le 3d
      REAL(KIND=kr), DIMENSION(:,:,:), ALLOCATABLE, SAVE ::
     &                        ex_3, ey_3, ez_3,
     &                        bx_3, by_3, bz_3, bx_3p, by_3p, bz_3p,
     &                        rh_3,
     &                        cx_3, cy_3, cz_3

!      REAL(KIND=kr), DIMENSION(:,:,:,:), ALLOCATABLE, SAVE ::
!     &                        rh_esp_3,
!     &                        cx_esp_3, cy_esp_3, cz_esp_3

! --- Champ magnetique statique
       REAL(KIND=kr), SAVE :: b0x, b0y, b0z
       INTEGER, SAVE :: mag_ok

! --- pour la correction de Poisson
       LOGICAL, SAVE :: pois_cor, pois_dia
!      _________________
       END MODULE champs


       MODULE champs_iter
! ========================================================
!
!
!
!
! ========================================================
       USE precisions

       IMPLICIT NONE


! --- pour le 3d
      REAL(KIND=kr), DIMENSION(:,:,:), ALLOCATABLE, SAVE ::
     &                        ex3_ik, ey3_ik, ez3_ik

! --- pour le 2d, 2d1/2
      REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE, SAVE ::
     &                        ex2_ik, ey2_ik

! --- pour le 1d, 1d1/2, 1d3/2
      REAL(KIND=kr), DIMENSION(:), ALLOCATABLE, SAVE ::
     &                        ex1_ik

! --- tableau partic pour les iterations
      REAL(KIND=kr), DIMENSION(:,:,:), ALLOCATABLE, SAVE :: partic_ik

      REAL(KIND=kr), DIMENSION(:,:,:), ALLOCATABLE, SAVE ::
     &          rho_esp2_ik(:,:,:)

      REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE, SAVE  ::
     &           xxx_ik(:,:),xxy_ik(:,:),xxz_ik(:,:),
     &           xyx_ik(:,:),xyy_ik(:,:),xyz_ik(:,:),
     &           xzx_ik(:,:),xzy_ik(:,:),xzz_ik(:,:)

      REAL(KIND=kr), DIMENSION(:), ALLOCATABLE, SAVE  ::
     &           xxx0_ik(:),xxy0_ik(:),xxz0_ik(:),
     &           xyx0_ik(:),xyy0_ik(:),xyz0_ik(:),
     &           xzx0_ik(:),xzy0_ik(:),xzz0_ik(:)

      REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE, SAVE  ::  src_max


      INTEGER, SAVE    ::    a_qx_ik, a_qy_ik, a_qz_ik
      INTEGER, SAVE    ::    a_dvx_ik, a_dvy_ik, a_dvz_ik

      INTEGER, SAVE    ::    a_crt_ik


      INTEGER, SAVE    ::    iter_es, nik0

      REAL(KIND=kr)    ::    epsik0



!      _________________
       END MODULE champs_iter


       MODULE particules
! ========================================================
!
!
!
!
! ========================================================
       USE precisions

       IMPLICIT NONE



       REAL(KIND=kr), SAVE ::
     &               xlong,ylong,
     &               rhmx


       REAL(KIND=kr), SAVE   ::   omega1
       REAL(KIND=kr), SAVE   ::   boost


!      _____________________
       END MODULE particules


       MODULE particules_klder
! ======================================================================
! Nombre d'especes de particules
! Taille des tableaux de particules
! Nombre local de particules (espece)
! Nombres globaux de particules par (espece, PE)
! Tableaux de particules (coordonnee, numero, espece)
!
! Creation ---  07-98 --- E. Lefebvre
! ======================================================================
      USE precisions

      IMPLICIT NONE

      INTEGER, SAVE :: iref, max_cycles
      INTEGER, SAVE :: nesp_f, nesp_p, nesp

      INTEGER, DIMENSION(:), ALLOCATABLE, SAVE ::
     &             meth_inj,  nbppm,
     &             dynam, nbscycles, per_tri,
     &             odml,
     &             typ_distx,typ_disty,typ_distz
      REAL(KIND=kr), DIMENSION(:), ALLOCATABLE, SAVE ::
     &               charge, masse, densite,
     &               vthx, vthy, vthz,
     &               vderx, vdery, vderz,
     &               polyt,  limmx, limpx, freeze

!
      REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE, SAVE ::
     &                 inj_distx,inj_disty,inj_distz
!
      INTEGER, SAVE       :: esir_ok

      INTEGER, SAVE :: dim_bufp, nbpamax
      INTEGER, SAVE :: a_po, a_qx, a_qy, a_qz, a_px, a_py, a_pz
      INTEGER, SAVE :: a_qxm1, a_qym1, a_qzm1
      INTEGER, SAVE :: a_wx, a_wy, a_wz

      INTEGER, SAVE :: a_bxp, a_byp, a_bzp
      INTEGER, SAVE :: a_pxtld, a_pytld, a_pztld
      INTEGER, SAVE :: a_gatld, a_gatld12, a_ga
      INTEGER, SAVE :: a_gaxe, a_gaye, a_gaze
      INTEGER, SAVE :: a_paxe, a_paye, a_paze
      INTEGER, SAVE :: a_ttapushs2

      REAL(KIND=kr), SAVE :: Efast
      INTEGER, DIMENSION(:), ALLOCATABLE, SAVE ::
     &      cnt_fast


      INTEGER, SAVE :: a_pt
!      INTEGER, SAVE :: a_ex, a_ey, a_ez, a_bx, a_by, a_bz
      INTEGER, DIMENSION(:),   ALLOCATABLE, SAVE :: nbpa
      INTEGER, DIMENSION(:,:), ALLOCATABLE, SAVE :: nbpa_gl
!
      REAL(KIND=kr), DIMENSION(:,:,:), ALLOCATABLE, SAVE :: partic

      REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE  :: buf_part

      INTEGER,       DIMENSION(:,:),   ALLOCATABLE, SAVE :: compteur
!      _____________________
       END MODULE particules_klder


      MODULE collisions
! ======================================================================
!
! Grandeurs utilisees pour parametrer et calculer les collisions
!
! Creation ---  05-00 --- J. Caillard, E. Lefebvre
! Modification ---  08-08 --- F. Perez, L. Gremillet
! ======================================================================
      USE precisions
      IMPLICIT NONE

! --- collisions classiques (=0) ou relativistes (=1)
      INTEGER, SAVE :: col_regime
! --- pour les tests : calculer seulement les collisions =1 sinon =0
      INTEGER, SAVE :: col_test
! --- nombre de types de collisions differentes
      INTEGER, SAVE :: col_nb
! --- especes prenant part a la collision
      INTEGER, DIMENSION(:),       ALLOCATABLE, SAVE  :: col_p1, col_p2
! --- nombre de "rounds de collision" dans un pas de temps
      INTEGER, DIMENSION(:),       ALLOCATABLE, SAVE  :: col_rnd
! --- logarithme coulombien calcule ou impose
      REAL(KIND=kr), DIMENSION(:), ALLOCATABLE, SAVE  :: col_log
! --- periode de calcul de la collision
      INTEGER, DIMENSION(:),       ALLOCATABLE, SAVE  :: col_per
! --- constantes pour les collisions relativistes (sans unite)
      REAL(KIND=kr), DIMENSION(:), ALLOCATABLE, SAVE  :: col_s0, col_s1
! --- angle caracteristique de la collision
      REAL(KIND=kr), DIMENSION(:), ALLOCATABLE, SAVE  :: col_dlt
! --- longueur de Debye au carre
      REAL(KIND=kr), DIMENSION(:), ALLOCATABLE, SAVE  :: ldebye2
!     _____________________
      END MODULE collisions


       MODULE repar
! ========================================================
!
!
!
!
! ========================================================
       IMPLICIT NONE

       INTEGER, SAVE ::  nint,hinit
       INTEGER, DIMENSION(1:1000), SAVE  ::   np

!      ________________
       END MODULE repar


      MODULE erreurs
! ======================================================================
!
!
! ======================================================================

      IMPLICIT NONE

      INTEGER, SAVE :: debug, fdbg = 19, fmsgo = 190
      INTEGER, DIMENSION(1), SAVE :: itesterr
!     __________________
      END MODULE erreurs
