! ======================================================================
! diag_ph.f
! ---------
!
!     SUBROUTINE diag_ph
!     SUBROUTINE proj1d_phase
!     SUBROUTINE proj2d_phase
!     SUBROUTINE proj3d_phase
!     SUBROUTINE proj1d_currt
!     SUBROUTINE proj2d_currt
!     SUBROUTINE proj3d_currt
!
!
! -- Passage a NINT(..-0.5) dans les proj?d_phase en juillet 2002 - EL -
!    Resolution en energie pour chaque projection - Projection des courants - LG 07/05 
! ======================================================================

      SUBROUTINE diag_ph
! ======================================================================
! Diagnostics de l'espace des phases effectues periodiquement
!    + Projections 1D de l'espace des phases
!    + Projections 2D de l'espace des phases
!    + Projections 3D de l'espace des phases
!    + Projection et calcul de pression en 1D
!    - Projection et calcul de densite effective
! 
! ======================================================================

      USE domaines
      USE domaines_klder
      USE temps
      USE particules_klder
      USE diagnostics_klder
      USE diag_moyen
      USE erreurs

      IMPLICIT NONE

      INTEGER                           :: iesp,iax,dph_i,ific
      INTEGER                           :: a_cx
      REAL(KIND=kr)                     :: marge,emin,emax
      INTEGER,      DIMENSION(3)        :: taill, a_ax
      REAL(KIND=kr), DIMENSION(3)       :: rmin, rmax
      REAL(KIND=kr), DIMENSION(:),     ALLOCATABLE  :: projection_1d
      REAL(KIND=kr), DIMENSION(:,:),   ALLOCATABLE  :: projection_2d
      REAL(KIND=kr), DIMENSION(:,:,:), ALLOCATABLE  :: projection_3d
      LOGICAL                           :: diag_moy, ecrit_ok
      LOGICAL, DIMENSION(3)             :: period
      CHARACTER(LEN=2)                  :: toto
      
!      REAL(KIND=kr)                :: moyenne, variance
!      REAL(KIND=kr)                :: moyenne_verif, variance_verif

!      INTEGER                      :: i_temp, j_temp, k_temp
            
      INTERFACE
        SUBROUTINE minmax_gl(nbp, tableau, min, max)
          USE precisions
          INTEGER, INTENT(IN)                       :: nbp
          REAL(KIND=kr), DIMENSION(nbp), INTENT(IN) :: tableau
          REAL(KIND=kr), INTENT(OUT)                :: min, max
        END SUBROUTINE minmax_gl
!
        SUBROUTINE proj3d_phase(nbp, taillx, tailly, taillz,
     &                          minx,maxx, miny,maxy, minz,maxz,
     &                          iesp,a_ax,ener_min,ener_max,resultat)
          USE precisions
          USE particules_klder
	  
          INTEGER, INTENT(IN)       :: nbp, taillx, tailly, taillz
          REAL(KIND=kr), INTENT(IN) :: minx,maxx, miny,maxy, minz,maxz,
     &                                 ener_min,ener_max
          INTEGER, DIMENSION(3), INTENT(IN)        ::  a_ax
          INTEGER, INTENT(IN)       :: iesp
          REAL(KIND=kr), DIMENSION(0:taillx-1,0:tailly-1,0:taillz-1),
     &                   INTENT(INOUT) :: resultat
        END SUBROUTINE proj3d_phase


!        SUBROUTINE som_PE1_tab3d(taillx,tailly,taillz,tableau)
!          USE precisions
!          INTEGER, INTENT(IN)         :: taillx, tailly, taillz
!          REAL(KIND=kr), DIMENSION(taillx,tailly,taillz),
!     &             INTENT(INOUT)      :: tableau
!        END SUBROUTINE som_PE1_tab3d
        SUBROUTINE ecrit_tab3d(num_fi,taillx,tailly,taillz,tableau)
          USE precisions
          INTEGER, INTENT(IN)         :: num_fi,taillx,tailly,taillz
          REAL(KIND=kr), DIMENSION(taillx,tailly,taillz),
     &                   INTENT(IN)   :: tableau
        END SUBROUTINE ecrit_tab3d
!
        SUBROUTINE proj2d_phase(nbp,taillx,tailly,minx,maxx,
     &                          miny,maxy,iesp,a_ax,
     &                          ener_min,ener_max,resultat)
          USE precisions
          USE particules_klder
	  
          INTEGER, INTENT(IN)                  :: nbp,taillx, tailly
          REAL(KIND=kr), INTENT(IN)            :: minx,maxx,miny,maxy,
     &                                            ener_min,ener_max
          INTEGER, DIMENSION(3), INTENT(IN)        ::  a_ax
     
          INTEGER, INTENT(IN) :: iesp
          REAL(KIND=kr), DIMENSION(0:taillx-1,0:tailly-1),
     &                   INTENT(INOUT)              :: resultat
        END SUBROUTINE proj2d_phase
	
	
        SUBROUTINE proj2d_emitt(nbp,taillx,tailly,minx,maxx,miny,maxy,
     &                          poids,axex,axey,axeg,axepx,ener,
     &                          ener_min,ener_max,resultat)
          USE precisions
          INTEGER, INTENT(IN)                  :: nbp,taillx,tailly
          REAL(KIND=kr), INTENT(INOUT)         :: minx,maxx,miny,maxy,
     &                                            ener_min,ener_max
          REAL(KIND=kr), DIMENSION(nbp), INTENT(IN) :: poids,
     &                                                 axex,axey,
     &                                                 axeg,axepx,
     &                                                 ener
          REAL(KIND=kr), DIMENSION(0:taillx-1,0:tailly-1), INTENT(INOUT)
     &                                              :: resultat
        END SUBROUTINE proj2d_emitt
!        SUBROUTINE som_PE1_tab2d(taillx,tailly,tableau)
!          USE precisions
!          INTEGER, INTENT(IN)                     :: taillx, tailly
!          REAL(KIND=kr), DIMENSION(taillx,tailly),
!     &                   INTENT(INOUT)            :: tableau
!        END SUBROUTINE som_PE1_tab2d
        SUBROUTINE ecrit_tab2d(num_fi, taillx, tailly, tableau)
          USE precisions
          INTEGER, INTENT(IN) :: num_fi, taillx, tailly
          REAL(KIND=kr), DIMENSION(taillx,tailly), INTENT(IN) :: tableau
        END SUBROUTINE ecrit_tab2d
!
        SUBROUTINE proj1d_phase(nbp,taillx,minx,maxx,iesp,a_ax,
     &                          ener_min,ener_max,resultat)
          USE precisions
          USE particules_klder
          INTEGER, INTENT(IN)                       :: nbp, taillx
          REAL(KIND=kr), INTENT(IN)                 :: minx, maxx,
     &                                                 ener_min,ener_max
          INTEGER, DIMENSION(3), INTENT(IN)        ::  a_ax
      
          INTEGER, INTENT(IN)        :: iesp
          REAL(KIND=kr), DIMENSION(0:taillx-1), INTENT(INOUT) 
     &                                              :: resultat
        END SUBROUTINE proj1d_phase
	

	
        SUBROUTINE proj1d_press(nbp,taillx,minx,maxx,
     &                          poids,axex,impx,vitx,ener,
     &                          ener_min,ener_max,resultat)
          USE precisions
          INTEGER, INTENT(IN)                       :: nbp, taillx
          REAL(KIND=kr), INTENT(IN)                 :: minx, maxx,
     &                                                 ener_min,ener_max
          REAL(KIND=kr), DIMENSION(nbp), INTENT(IN) :: poids, axex,
     &                                                 impx,  vitx,
     &                                                 ener
          REAL(KIND=kr), DIMENSION(0:taillx-1), INTENT(INOUT) 
     &                                              :: resultat
        END SUBROUTINE proj1d_press
	
        SUBROUTINE proj1d_currt(nbp,taillx,minx,maxx,
     &                          toto, iesp, a_ax, a_cx,
     &                          ener_min,ener_max,resultat)
          USE precisions
          USE particules_klder
	  
          INTEGER, INTENT(IN)                       :: nbp, taillx
          REAL(KIND=kr), INTENT(IN)                 :: minx, maxx,
     &                                                 ener_min,ener_max
          CHARACTER, INTENT(IN)           :: toto
          INTEGER, INTENT(IN)             :: iesp, a_cx
          INTEGER, DIMENSION(3), INTENT(IN)        ::  a_ax
	  
          REAL(KIND=kr), DIMENSION(0:taillx-1), INTENT(INOUT) 
     &                                              :: resultat
        END SUBROUTINE proj1d_currt
	
        SUBROUTINE proj2d_currt(nbp, taillx,tailly, 
     &                          minx,maxx,miny,maxy,
     &                          toto,iesp,a_ax, a_cx, 
     &                          ener_min,ener_max,resultat)
          USE precisions
          USE particules_klder
	  
          INTEGER, INTENT(IN)             :: nbp, 
     &                                       taillx,tailly
          REAL(KIND=kr), INTENT(IN)       :: minx,maxx,
     &                                       miny,maxy,
     &                                       ener_min,ener_max
          INTEGER, DIMENSION(3), INTENT(IN)        ::  a_ax
          CHARACTER, INTENT(IN)           :: toto
          INTEGER, INTENT(IN)             :: iesp, a_cx
          REAL(KIND=kr), DIMENSION(0:taillx-1,0:tailly-1),
     &                   INTENT(INOUT)    :: resultat
        END SUBROUTINE proj2d_currt	        
	
	SUBROUTINE proj3d_currt(nbp,taillx,tailly,taillz,
     &                          minx,maxx,miny,maxy,minz,maxz,
     &                          toto, iesp, a_ax, a_cx, 
     &                          ener_min,ener_max,resultat)
          USE precisions
          USE particules_klder
	  
          INTEGER, INTENT(IN)              :: nbp, 
     &                                        taillx,tailly,taillz
          REAL(KIND=kr), INTENT(IN)        :: minx,maxx,
     &                                        miny,maxy,
     &                                        minz,maxz,
     &                                        ener_min,ener_max
          INTEGER, DIMENSION(3), INTENT(IN)        ::  a_ax
          CHARACTER, INTENT(IN)          :: toto
          INTEGER, INTENT(IN)            :: iesp, a_cx
          REAL(KIND=kr), DIMENSION(0:taillx-1,0:tailly-1,0:taillz-1),
     &                   INTENT(INOUT)     :: resultat
        END SUBROUTINE proj3d_currt
	
!        SUBROUTINE som_PE1_tab1d(taillx, tableau)
!          USE precisions
!          INTEGER, INTENT(IN) :: taillx
!          REAL(KIND=kr),DIMENSION(taillx),INTENT(INOUT) :: tableau	
!        END SUBROUTINE som_PE1_tab1d
        SUBROUTINE ecrit_tab1d(num_fi, taillx, tableau)
          USE precisions
          INTEGER, INTENT(IN) :: num_fi, taillx
          REAL(KIND=kr), DIMENSION(taillx), INTENT(IN) :: tableau
        END SUBROUTINE ecrit_tab1d
      END INTERFACE


      diag_moy = .FALSE. ; IF(dph_moy.GT.0) diag_moy = .TRUE.

! Diagnostics particulaires : projections de l'espace des phases
! ----------------------------------------------------------------------
      DO dph_i=1,dph_nb
! --- diag. non valide
        IF(.NOT.dph(dph_i)%ok) CYCLE
! --- diag 2D ou 3D : pas de moyenne en temps
        IF(     (dph(dph_i)%axe(2).NE.'00')
     &     .AND.(MOD(iter,dph(dph_i)%per).NE.0) ) CYCLE
! --- diag non moyenne en temps : mauvais pas de temps
        IF((.NOT.diag_moy).AND.(MOD(iter,dph(dph_i)%per).NE.0) ) CYCLE
! --- diag moyenne en temps : mauvais pas de temps
        IF(     (diag_moy)
     &     .AND.(MOD(iter+dph_moy,dph(dph_i)%per).GT.dph_moy) ) CYCLE

        IF(diag_moy) THEN
          ecrit_ok = MOD(iter+dph_moy,dph(dph_i)%per)+1 .EQ. dph_moy+1
          IF(.NOT.ALLOCATED(dph1d_mem)) THEN
            ALLOCATE( dph1d_mem(0:MAXVAL(dph(:)%siz(1))-1,nesp,dph_nb) )
          ENDIF
          IF(MOD(iter+dph_moy,dph(dph_i)%per).EQ.0) THEN
            dph1d_mem(:,:,dph_i) = 0.
          ENDIF
        ELSE
          ecrit_ok = .TRUE.
        ENDIF

      DO iesp=1,nesp_p

      WRITE(fdbg,*) 'Diag Phase ',dph_i,' espece ',iesp, nbpa(iesp)

! --- Parametres des axes x et y de la projection
      DO iax=1,3
        IF(dph(dph_i)%axe(iax).EQ.'00') CYCLE

        taill(iax) = dph(dph_i)%siz(iax)

        SELECT CASE(dph(dph_i)%axe(iax))
! indice associe a la coordonnee x
        CASE('qx') ; a_ax(iax) = a_qx   
! indice associe a la coordonnee y
        CASE('qy') ; a_ax(iax) = a_qy   
! indice associe a la coordonnee z
        CASE('qz') ; a_ax(iax) = a_qz   
! indice associe a l'impulsion px
        CASE('px') ; a_ax(iax) = a_px   
        CASE('py') ; a_ax(iax) = a_py
        CASE('pz') ; a_ax(iax) = a_pz
! indice associe a la composante x de la puissance
        CASE('wx') ; a_ax(iax) = a_wx   
        CASE('wy') ; a_ax(iax) = a_wy
        CASE('wz') ; a_ax(iax) = a_wz
! pour la pression
        CASE('fx') ; a_ax(iax) = a_qx      
! pour l'emittance
        CASE('ex') ; a_ax(iax) = a_px      
! pour l'emittance
        CASE('ey') ; a_ax(iax) = a_py      
! pour l'emittance
        CASE('ez') ; a_ax(iax) = a_pz      
        CASE('ix','jx','kx')
! pour le courant 
          a_ax(iax) = a_qx                 
        CASE('iy','jy','ky')
! pour le courant 
          a_ax(iax) = a_qy                 
        CASE('iz','jz','kz')
! pour le courant 
          a_ax(iax) = a_qz                 
	CASE('rx','sx','tx')
! pour le flux d'energie
          a_ax(iax) = a_qx                 
        CASE('ry','sy','ty')
 ! pour le flux d'energie
          a_ax(iax) = a_qy                
	CASE('rz','sz','tz')
! pour le flux d'energie
          a_ax(iax) = a_qz                 
! pour la densite d'energie
	CASE('gx') ; a_ax(iax) = a_qx      
! pour la densite d'energie
	CASE('gy') ; a_ax(iax) = a_qy     
! pour la densite d'energie
	CASE('gz') ; a_ax(iax) = a_qz     
	  	    
        END SELECT
        
! LG 02/07 
	IF(ABS(dph(dph_i)%ener(1)).LT.ABS(dph(dph_i)%ener(2))) THEN  
	  toto = dph(dph_i)%axe(1) 
          SELECT CASE(toto(1:1))
	  CASE('i','j','k','r','s','t')
	    emax=dph(dph_i)%ener(2)
	    IF(dph(dph_i)%ener(1)*dph(dph_i)%ener(2).LE.0.) THEN
	      emin = 0._8
	    ELSE
	      emin = dph(dph_i)%ener(1)
	    ENDIF
	  CASE DEFAULT
	    emin=ABS(dph(dph_i)%ener(1))
	    emax=ABS(dph(dph_i)%ener(2))
	  END SELECT
	ELSE
	  emin=0._8
	  emax=1.d10
	ENDIF
	
        IF(dph(dph_i)%min(iax).EQ.dph(dph_i)%max(iax)) THEN
          SELECT CASE(dph(dph_i)%axe(iax))
          CASE('qx','gx') ; rmin(iax)=xmin_gl;rmax(iax)=xmax_gl
          CASE('qy','gy') ; rmin(iax)=ymin_gl;rmax(iax)=ymax_gl
          CASE('qz','gz') ; rmin(iax)=zmin_gl;rmax(iax)=zmax_gl
          CASE('fx') ; rmin(iax) = xmin_gl ; rmax(iax) = xmax_gl
          CASE('ix','jx','kx')
            rmin(iax) = xmin_gl ; rmax(iax) = xmax_gl
          CASE('iy','jy','ky')
            rmin(iax) = ymin_gl ; rmax(iax) = ymax_gl
          CASE('iz','jz','kz')
            rmin(iax) = zmin_gl ; rmax(iax) = zmax_gl
	  CASE('rx','sx','tx')
            rmin(iax) = xmin_gl ; rmax(iax) = xmax_gl
          CASE('ry','sy','ty')
            rmin(iax) = ymin_gl ; rmax(iax) = ymax_gl
          CASE('rz','sz','tz')
            rmin(iax) = zmin_gl ; rmax(iax) = zmax_gl  
          CASE('ex') ; rmin(iax) = 0.      ; rmax(iax) = 0.
          CASE('ey') ; rmin(iax) = 0.      ; rmax(iax) = 0.
          CASE('ez') ; rmin(iax) = 0.      ; rmax(iax) = 0.
          CASE DEFAULT
            CALL minmax_gl(nbpa(iesp),
     &                     partic(a_ax(iax),1:nbpa(iesp),iesp),
     &                     rmin(iax),rmax(iax))
! --- + si jamais il n'y a plus aucune part. de cette espece,
!       minmax_gl retourne +HUGE(1._8) en min et - en max
            IF(rmin(iax).GT.rmax(iax)) THEN
              rmin(iax) = -1._8
              rmax(iax) =  1._8
            ELSE
              IF(rmin(iax).EQ.rmax(iax)) THEN
! ----- notamment axe px pour des ions immobiles et froids
                rmin(iax) = -0.01_8
                rmax(iax) =  0.01_8
              ENDIF
              marge = 0.05*(rmax(iax)-rmin(iax))
              rmin(iax) = rmin(iax)-marge
              rmax(iax) = rmax(iax)+marge
            ENDIF
          END SELECT
        ELSE
          rmin(iax) = dph(dph_i)%min(iax)
          rmax(iax) = dph(dph_i)%max(iax)
        ENDIF

! --- Y a-t-il des conditions periodiques sur certains axes ?
        period(iax) = .FALSE.
        SELECT CASE(dph(dph_i)%axe(iax))
        CASE('qx','fx','ix','jx','kx','rx','sx','tx','gx')
          IF(     (rmin(iax).EQ.xmin_gl).AND.(rmax(iax).EQ.xmax_gl)
     &       .AND.(ibndp(1,1).EQ.-1)) period(iax)=.TRUE.
        CASE('qy','iy','jy','ky','ry','sy','ty','gy')
          IF(     (rmin(iax).EQ.ymin_gl).AND.(rmax(iax).EQ.ymax_gl)
     &       .AND.(ibndp(3,1).EQ.-1)) period(iax)=.TRUE.
        CASE('qz','iz','jz','kz','rz','sz','tz','gz')
          IF(     (rmin(iax).EQ.zmin_gl).AND.(rmax(iax).EQ.zmax_gl)
     &       .AND.(ibndp(5,1).EQ.-1)) period(iax)=.TRUE.
        END SELECT

      ENDDO

! bof bof bof, oui bon
      toto = dph(dph_i)%axe(1) 
      SELECT CASE(toto(1:1))
! --- Si courant projet�, quel axe ?       
      CASE('i') ; a_cx = a_px    
      CASE('j') ; a_cx = a_py    
      CASE('k') ; a_cx = a_pz    
! si flux d'energie projete, quel axe ?
      CASE('r') ; a_cx = a_px 
      CASE('s') ; a_cx = a_py 
      CASE('t') ; a_cx = a_pz 
! si energie projetee, quel axe ?
      CASE('g') ; a_cx = a_ga   
      END SELECT

      IF(dph(dph_i)%axe(3).NE.'00') THEN
! --- Tous les axes sont definis : projection 3D
! --- Allocation et mise a zero du tableau resultat de la projection
        ALLOCATE(projection_3d(0:taill(1)-1,0:taill(2)-1,0:taill(3)-1))
        projection_3d = 0.
        
        IF(    (toto(1:1).EQ.'i').OR.(toto(1:1).EQ.'j')
     &      .OR.(toto(1:1).EQ.'k').OR.   
     &       (toto(1:1).EQ.'r').OR.(toto(1:1).EQ.'s')
     &      .OR.(toto(1:1).EQ.'t') .OR.(toto(1:1).EQ.'g') ) THEN
! --- Calcul d'un courant 
            CALL proj3d_currt(nbpa(iesp),
     &                        taill(1),taill(2),taill(3),	    
     &                        rmin(1),rmax(1),rmin(2),rmax(2),
     &                        rmin(3),rmax(3),     
     &                        toto(1:1),iesp, a_ax, a_cx, emin,emax,
     &                        projection_3d)
     
	ELSE
! --- Calcul de la projection
            CALL proj3d_phase(nbpa(iesp),taill(1),taill(2),taill(3),
     &                    rmin(1),rmax(1),rmin(2),rmax(2),
     &                    rmin(3),rmax(3),iesp, a_ax,                         
     &                    emin,emax,
     &                    projection_3d) 
        ENDIF
       
! --- Rassemblement des resultats et ecriture
!        CALL som_PE1_tab3d(taill(1), taill(2), taill(3), projection_3d)  ! mise en commun de l'ensemble des
!	procs sur le proc 1
        IF(numproc.EQ.1) THEN
          ific = dph(dph_i)%fic
	  
!!--- calcul de la moyenne	   
!	  moyenne = sum( sum( sum( projection_3d, dim=1 ), dim=1 ) )
!	  moyenne = moyenne / ( taill(1)*taill(2)*taill(3) )
!	  moyenne_verif = 0
!	  DO i_temp = 0, taill(1)-1
!	     DO j_temp = 0, taill(2)-1
!	        DO k_temp = 0, taill(3)-1
!	          moyenne_verif = moyenne_verif + 
!     &	          projection_3d(i_temp,j_temp,k_temp)
!		ENDDO
!	     ENDDO
!	  ENDDO	  
!	  moyenne_verif = moyenne_verif / ( taill(1)*taill(2)*taill(3) )
!	  
!!--- calcul de la variance 
!	  variance = sum( sum( sum( (projection_3d - moyenne)**2, 
!     &    dim=1 ), dim=1 ) ) 	  
!	  variance = variance / ( taill(1)*taill(2)*taill(3) )
!	  variance_verif = 0
!	  DO i_temp = 0, taill(1)-1
!	     DO j_temp = 0, taill(2)-1
!	        DO k_temp = 0, taill(3)-1
!	          variance_verif = variance_verif + 
!     &	          ( projection_3d(i_temp,j_temp,k_temp) - moyenne_verif )**2
!		ENDDO  
!	     ENDDO
!	  ENDDO	  
!	  variance_verif = variance_verif / ( taill(1)*taill(2)*taill(3) )	  
!	  
!	  WRITE(ific,'("MOYENNE ET VERIFICATION")')
!          WRITE(ific,'(10F12.8)')             moyenne	    	    
!          WRITE(ific,'(10F12.8)')             moyenne_verif	
!	  WRITE(ific,'("VARIANCE ET VERIFICATION")')
!          WRITE(ific,'(10F12.8)')             variance		
!	  WRITE(ific,'(10F12.8)')             variance_verif		  
	  
          WRITE(ific,'("PHASE_3D")')
          WRITE(ific,'(A2)')                  dph(dph_i)%axe(1)
          WRITE(ific,'(A2)')                  dph(dph_i)%axe(2)
          WRITE(ific,'(A2)')                  dph(dph_i)%axe(3)
	  WRITE(ific,'(2(1X,1PE10.3))')       emin, emax    ! Modif LG 07/05
          WRITE(ific,'(1X,1PE10.3)')          iter*pasdt
          WRITE(ific,'(1X,I4)')               iesp
          WRITE(ific,'(2(1X,1PE10.3),1X,I5)') rmin(1), rmax(1), taill(1)
          WRITE(ific,'(2(1X,1PE10.3),1X,I5)') rmin(2), rmax(2), taill(2)
          WRITE(ific,'(2(1X,1PE10.3),1X,I5)') rmin(3), rmax(3), taill(3)
! --- au besoin, tenir compte des conditions periodiques
          IF(period(1)) THEN
            projection_3d(0,:,:) =  projection_3d(0,:,:) 
     &                            + projection_3d(taill(1)-1,:,:)
            projection_3d(taill(1)-1,:,:) = projection_3d(0,:,:)
          ENDIF
          IF(period(2)) THEN
            projection_3d(:,0,:) =  projection_3d(:,0,:)
     &                            + projection_3d(:,taill(2)-1,:)
            projection_3d(:,taill(2)-1,:) = projection_3d(:,0,:)
          ENDIF
          IF(period(3)) THEN
            projection_3d(:,:,0) =  projection_3d(:,:,0)
     &                            + projection_3d(:,:,taill(3)-1)
            projection_3d(:,:,taill(3)-1) = projection_3d(:,:,0)
          ENDIF
          CALL ecrit_tab3d(ific, taill(1), taill(2), taill(3),
     &                     projection_3d)
        ENDIF
        DEALLOCATE( projection_3d )

      ELSEIF(dph(dph_i)%axe(2).NE.'00') THEN
! --- Le deuxieme axe est defini : projection 2D
! --- Allocation et mise a zero du tableau resultat de la projection
        ALLOCATE( projection_2d(0:taill(1)-1,0:taill(2)-1) )
        projection_2d = 0.
! --- Calcul de la projection
        IF(dph(dph_i)%axe(2)(1:1).EQ.'e') THEN
! Calcul de l'emittance
          IF(dynam(iesp).EQ.2) THEN     ! dynamique relativiste	  
            CALL proj2d_emitt(nbpa(iesp), taill(1), taill(2),
     &                      rmin(1),rmax(1), rmin(2),rmax(2),
     &                      partic(a_po   ,1:nbpa(iesp),iesp),
     &                      partic(a_ax(1),1:nbpa(iesp),iesp),
     &                      partic(a_ax(2),1:nbpa(iesp),iesp),
     &                      partic(a_ga   ,1:nbpa(iesp),iesp),
     &                      partic(a_px   ,1:nbpa(iesp),iesp),
     &                      (partic(a_ga   ,1:nbpa(iesp),iesp)-1._8)
     &                     *masse(iesp)*511._8,                       
     &                      emin,emax,
     &                      projection_2d) 
          ELSE
	    CALL proj2d_emitt(nbpa(iesp), taill(1), taill(2),
     &                      rmin(1),rmax(1), rmin(2),rmax(2),
     &                      partic(a_po   ,1:nbpa(iesp),iesp),
     &                      partic(a_ax(1),1:nbpa(iesp),iesp),
     &                      partic(a_ax(2),1:nbpa(iesp),iesp),
     &                      partic(a_ga   ,1:nbpa(iesp),iesp),
     &                      partic(a_px   ,1:nbpa(iesp),iesp),
     &                      partic(a_ga   ,1:nbpa(iesp),iesp)
     &                     *masse(iesp)*511._8,                       
     &                      emin,emax,
     &                      projection_2d)   
          ENDIF     
        ELSEIF(    (toto(1:1).EQ.'i').OR.(toto(1:1).EQ.'j')
     &      .OR.(toto(1:1).EQ.'k').OR.   
     &       (toto(1:1).EQ.'r').OR.(toto(1:1).EQ.'s')
     &      .OR.(toto(1:1).EQ.'t') .OR.(toto(1:1).EQ.'g') ) THEN

         CALL proj2d_currt(nbpa(iesp),taill(1),taill(2),
     &                     rmin(1),rmax(1), rmin(2),rmax(2), 
     &                     toto(1:1), iesp, a_ax, a_cx, 
     &                     emin,emax, projection_2d) 
         
	ELSE 
            CALL proj2d_phase(nbpa(iesp), taill(1), taill(2),
     &                      rmin(1),rmax(1), rmin(2),rmax(2), 
     &                      iesp, a_ax, emin,emax,
     &                      projection_2d)
        ENDIF
! --- Rassemblement des resultats et ecriture
!        CALL som_PE1_tab2d(taill(1), taill(2), projection_2d)
        IF(numproc.EQ.1) THEN
	
          ific = dph(dph_i)%fic
	
!--- calcul de la moyenne	   
!	  moyenne = sum( sum( projection_2d, dim=1 ) )
!	  moyenne = moyenne / ( taill(1)*taill(2) )	  	  
!	  moyenne_verif = 0
!	  DO i_temp = 0, taill(1)-1
!	     DO j_temp = 0, taill(2)-1
!	        moyenne_verif = moyenne_verif + 
!     &	        projection_2d(i_temp,j_temp)
!	     ENDDO
!	  ENDDO	  
!	  moyenne_verif = moyenne_verif / ( taill(1)*taill(2) )
!	  
!--- calcul de la variance 
!	  variance = sum( sum( (projection_2d - moyenne)**2, dim=1 ) ) 	  
!	  variance = variance / ( taill(1)*taill(2) )	    	  	  
!	  variance_verif = 0
!	  DO i_temp = 0, taill(1)-1
!	     DO j_temp = 0, taill(2)-1
!	        variance_verif = variance_verif + 
!     &	        ( projection_2d(i_temp,j_temp) - moyenne_verif )**2
!	     ENDDO
!	  ENDDO	  
!	  variance_verif = variance_verif / ( taill(1)*taill(2) )
!	            
!	  WRITE(ific,'("MOYENNE ET VERIFICATION")')
!          WRITE(ific,'(10F12.8)')             moyenne	    	    
!          WRITE(ific,'(10F12.8)')             moyenne_verif	
!	  WRITE(ific,'("VARIANCE ET VERIFICATION")')
!          WRITE(ific,'(10F12.8)')             variance		
!	  WRITE(ific,'(10F12.8)')             variance_verif	

          WRITE(ific,'("PHASE_2D")')
          WRITE(ific,'(A2)')                  dph(dph_i)%axe(1)
          WRITE(ific,'(A2)')                  dph(dph_i)%axe(2)
	  WRITE(ific,'(2(1X,1PE10.3))')       emin, emax    ! Modif LG 07/05
          WRITE(ific,'(1X,1PE10.3)')          iter*pasdt
          WRITE(ific,'(1X,I4)')               iesp
          WRITE(ific,'(2(1X,1PE10.3),1X,I5)') rmin(1), rmax(1), taill(1)
          WRITE(ific,'(2(1X,1PE10.3),1X,I5)') rmin(2), rmax(2), taill(2)
! --- au besoin, tenir compte des conditions periodiques
          IF(period(1)) THEN
            projection_2d(0,:) =  projection_2d(0,:) 
     &                          + projection_2d(taill(1)-1,:)
            projection_2d(taill(1)-1,:) = projection_2d(0,:)
          ENDIF
          IF(period(2)) THEN
            projection_2d(:,0) =  projection_2d(:,0)
     &                          + projection_2d(:,taill(2)-1)
            projection_2d(:,taill(2)-1) = projection_2d(:,0)
          ENDIF
          CALL ecrit_tab2d(ific, taill(1), taill(2), projection_2d)
        ENDIF
        DEALLOCATE( projection_2d )

      ELSEIF(dph(dph_i)%axe(1).NE.'00') THEN
! --- Seul le premier axe est defini : projection 1D
! --- Allocation et mise a zero du tableau resultat de la projection
        ALLOCATE( projection_1d(0:taill(1)-1) )
        projection_1d = 0.

        IF(dph(dph_i)%axe(1).EQ.'fx') THEN
! --- Calcul de la pression
          IF(dynam(iesp).EQ.2) THEN	    
            CALL proj1d_press(nbpa(iesp),taill(1),rmin(1),rmax(1),
     &                        partic(a_po,1:nbpa(iesp),iesp),
     &                        partic(a_qx,1:nbpa(iesp),iesp),
     &                        partic(a_px,1:nbpa(iesp),iesp),
     &                        partic(a_px,1:nbpa(iesp),iesp)
     &                       /partic(a_ga,1:nbpa(iesp),iesp),
     &                        (partic(a_ga   ,1:nbpa(iesp),iesp)-1._8)
     &                       *masse(iesp)*511._8,                       
     &                        emin,emax,
     &                        projection_1d)
          ELSE
            CALL proj1d_press(nbpa(iesp),taill(1),rmin(1),rmax(1),
     &                        partic(a_po,1:nbpa(iesp),iesp),
     &                        partic(a_qx,1:nbpa(iesp),iesp),
     &                        partic(a_px,1:nbpa(iesp),iesp),
     &                        partic(a_px,1:nbpa(iesp),iesp),
     &                        partic(a_ga   ,1:nbpa(iesp),iesp)
     &                       *masse(iesp)*511._8,
     &                        emin,emax,
     &                        projection_1d)
          ENDIF
        ELSEIF(   (toto(1:1).EQ.'i').OR.(toto(1:1).EQ.'j')
     &      .OR.(toto(1:1).EQ.'k').OR.   
     &       (toto(1:1).EQ.'r').OR.(toto(1:1).EQ.'s')
     &      .OR.(toto(1:1).EQ.'t') .OR.(toto(1:1).EQ.'g') ) THEN

            CALL proj1d_currt(nbpa(iesp),taill(1),rmin(1),rmax(1),                       
     &                        toto(1:1), iesp, a_ax, a_cx, 
     &                        emin, emax, projection_1d)   
     
        ELSE
! Calcul d'une projection			  	
	    CALL proj1d_phase(nbpa(iesp), taill(1), rmin(1),rmax(1),                     
     &                      iesp, a_ax, emin,emax,
     &                      projection_1d)    	  
        ENDIF
! --- Memorisation ou...
        IF(diag_moy) THEN
            dph1d_mem(0:taill(1)-1,iesp,dph_i) 
     &    = dph1d_mem(0:taill(1)-1,iesp,dph_i)
     &     +projection_1d(0:taill(1)-1)
          IF(ecrit_ok) THEN
              projection_1d(0:taill(1)-1)
     &      = dph1d_mem(0:taill(1)-1,iesp,dph_i)/REAL(dph_moy+1)
            IF(iesp.EQ.nesp) THEN
              print*, 'Fin diag_ph moyen (',dph_moy*pasdt,')'
            ENDIF
          ENDIF
        ENDIF
        IF(ecrit_ok) THEN
! --- ... rassemblement des resultats et ecriture
!          CALL som_PE1_tab1d(taill(1), projection_1d)
          IF(numproc.EQ.1) THEN
            ific = dph(dph_i)%fic	    
!--- calcul de la moyenne	   
!	     moyenne = sum( projection_1d(:) ) 	  
!	     moyenne = moyenne / taill(1)	  
!--- calcul de la variance 
!	     variance = sum( (projection_1d(:) - moyenne )**2 ) 	  
!	     variance = variance / taill(1)		  	    
!	     WRITE(ific,'("MOYENNE")')
!            WRITE(ific,'(10F12.8)')             moyenne	    	    
!	     WRITE(ific,'("VARIANCE")')
!            WRITE(ific,'(10F12.8)')             variance

            WRITE(ific,'("PHASE_1D")')
            WRITE(ific,'(A2)')                  dph(dph_i)%axe(1)	    
	    WRITE(ific,'(2(1X,1PE10.3))')       emin, emax    ! Modif LG 07/05
            WRITE(ific,'(1X,1PE10.3)')          iter*pasdt
            WRITE(ific,'(1X,I4)')               iesp
            WRITE(ific,'(2(1X,1PE10.3),1X,I5)') rmin(1),rmax(1),taill(1)
! --- au besoin, tenir compte des conditions periodiques
            IF(period(1)) THEN
              projection_1d(0) =  projection_1d(0) 
     &                          + projection_1d(taill(1)-1)
              projection_1d(taill(1)-1) = projection_1d(0)
            ENDIF
            CALL ecrit_tab1d(ific, taill(1), projection_1d)
          ENDIF
        ENDIF
        DEALLOCATE( projection_1d )

      ENDIF

      ENDDO   ! --- boucle sur les especes
      ENDDO   ! --- boucle sur les diagnostics

      RETURN
!     ______________________
      END SUBROUTINE diag_ph



      SUBROUTINE proj1d_phase(nbp,taillx,minx,maxx,
     &                        iesp,a_ax,ener_min,ener_max,
     &                        resultat)
! ======================================================================
! Projection de l'espace des phases sur une droite
! ------------------------------------------------
!
! INTENT(IN)
!    + nbp : nombre de particules dans les tableaux poids, axex
!    + taillx      : taille du tableau resultat
!    + minx, maxx  : bornes de la projection, determinees
!                    avant appel de la procedure
!    + poids, axex : poids et positions des particules
!    + ener   : energies de sparticules
!    + ener_min,ener_max  : bornes en energie
! INTENT(INOUT)
!    + resultat : tableau resultat incremente
! 
! ======================================================================
      USE precisions
      USE particules_klder
      
      IMPLICIT NONE

      INTEGER, INTENT(IN)                       :: nbp, taillx
      REAL(KIND=kr), INTENT(IN)                 :: minx, maxx,
     &                                             ener_min,ener_max
      INTEGER, INTENT(IN)    :: iesp
      INTEGER, DIMENSION(3), INTENT(IN)        ::  a_ax

      REAL(KIND=kr), DIMENSION(0:taillx-1), INTENT(INOUT) 
     &                                          :: resultat

      REAL(KIND=kr)   :: poids, axex,ener

      INTEGER         :: ip
      REAL(KIND=kr)   :: pasdxi, auxmx, auxpx
      REAL(KIND=kr)   :: rpx
      INTEGER         :: ipmx, ippx
      REAL(KIND=kr)   :: ponpmx, ponppx


      IF(nbp.EQ.0) RETURN

! --- Taille des mailles
      pasdxi = REAL(taillx-1,8)/(maxx-minx)

! Boucle sur les particules...
! ----------------------------------------------------------------------
      DO ip=1,nbp
      
      poids = partic(a_po   ,ip,iesp)
      axex  = partic(a_ax(1),ip,iesp)
      
! --------------------- Cas relativiste
      IF(dynam(iesp).EQ.2) THEN 
          ener  = (partic(a_ga, ip, iesp)-1._8)*masse(iesp)*511._8
! --------------------- Cas classique
      ELSE   
          ener  = partic(a_ga, ip, iesp)*masse(iesp)*511._8
      ENDIF   
      
      if ((ener.lt.ener_min).or.(ener.gt.ener_max)) cycle

! --- Coordonnees reelles en numero de maille (primale)
        rpx = (axex-minx)*pasdxi
! --- Coordonnees entieres primales : lignes encadrant la particule
        ipmx = NINT(rpx-0.5)
        ippx = ipmx+1

        if((ipmx.LT.0).OR.(ippx.GE.taillx)) then
          cycle
        endif  

! --- Ponderations (lineaires) des differents sommets primaux
        ponpmx = REAL(ippx,8) - rpx
        ponppx = 1._8 - ponpmx

! --- Projection sur le maillage de diagnostic
        auxmx = poids*ponpmx
        auxpx = poids*ponppx
        resultat(ipmx) = resultat(ipmx) + auxmx
        resultat(ippx) = resultat(ippx) + auxpx

      ENDDO

      resultat(:) = resultat(:)*pasdxi

      RETURN
!     ___________________________
      END SUBROUTINE proj1d_phase
      


      SUBROUTINE proj2d_phase(nbp,taillx,tailly,minx,maxx,
     &                        miny,maxy,iesp,a_ax,
     &                        ener_min,ener_max,resultat)
! ======================================================================
! Projection de l'espace des phases sur un plan
! ---------------------------------------------
!
! INTENT(IN)
!    + nbp : nombre de particules dans les tableaux poids, axex, axey
!    + taillx, tailly : taille du tableau resultat
!    + minx, maxx, miny, maxy : bornes de la projection, determinees
!                               avant appel de la procedure
!    + poids, axex, axey : poids et positions des particules
!    + ener : energies des particules
!    + ener_min,ener_max : bornes en energie
! INTENT(INOUT)
!    + resultat : tableau resultat incremente
! 
! ======================================================================
      USE precisions
      USE particules_klder
      
      IMPLICIT NONE

      INTEGER, INTENT(IN)                       :: nbp, taillx,tailly
      REAL(KIND=kr), INTENT(IN)                 :: minx,maxx,miny,maxy,
     &                                             ener_min,ener_max
      INTEGER, DIMENSION(3), INTENT(IN)        ::  a_ax
      INTEGER, INTENT(IN) :: iesp 
      
      REAL(KIND=kr), DIMENSION(0:taillx-1,0:tailly-1), INTENT(INOUT) 
     &                                          :: resultat

      REAL(KIND=kr)   ::  poids,axex,axey,ener

      INTEGER         :: ip
      REAL(KIND=kr)   :: pasdxi, pasdyi, auxmx, auxpx
      REAL(KIND=kr)   :: rpx, rpy
      INTEGER         :: ipmx, ippx, ipmy, ippy
      REAL(KIND=kr)   :: ponpmx, ponppx, ponpmy, ponppy


      IF(nbp.EQ.0) RETURN

! --- Taille des mailles
      pasdxi = REAL(taillx-1,8)/(maxx-minx)
      pasdyi = REAL(tailly-1,8)/(maxy-miny)

! Boucle sur les particules...
! ----------------------------------------------------------------------
      DO ip=1,nbp


      poids = partic(a_po   ,ip,iesp)
      axex  = partic(a_ax(1),ip,iesp)
      axey  = partic(a_ax(2),ip,iesp)
      
! --------------------- Cas relativiste
      IF(dynam(iesp).EQ.2) THEN 
          ener  = (partic(a_ga, ip, iesp)-1._8)*masse(iesp)*511._8
! --------------------- Cas classique
      ELSE   
          ener  = partic(a_ga, ip, iesp)*masse(iesp)*511._8
      ENDIF   


      if ((ener.lt.ener_min).or.(ener.gt.ener_max)) cycle

! --- Coordonnees reelles en numero de maille (primale)
        rpx = (axex-minx)*pasdxi
        rpy = (axey-miny)*pasdyi
! --- Coordonnees entieres primales : lignes encadrant la particule
        ipmx = NINT(rpx-0.5)
        ippx = ipmx+1
        ipmy = NINT(rpy-0.5)
        ippy = ipmy+1

        if((ipmx.LT.0).OR.(ippx.GE.taillx).OR.
     &     (ipmy.LT.0).OR.(ippy.GE.tailly)) then
          cycle
        endif

! --- Ponderations (lineaires) des differents sommets primaux
        ponpmx = REAL(ippx,8) - rpx
        ponppx = 1._8 - ponpmx
        ponpmy = REAL(ippy,8) - rpy
        ponppy = 1._8 - ponpmy

! --- Projection sur le maillage de diagnostic
        auxmx = poids*ponpmx
        auxpx = poids*ponppx
        resultat(ipmx,ipmy) = resultat(ipmx,ipmy) + auxmx*ponpmy
        resultat(ippx,ipmy) = resultat(ippx,ipmy) + auxpx*ponpmy
        resultat(ipmx,ippy) = resultat(ipmx,ippy) + auxmx*ponppy
        resultat(ippx,ippy) = resultat(ippx,ippy) + auxpx*ponppy

      ENDDO

      resultat(:,:) = resultat(:,:)*pasdxi*pasdyi

      RETURN
!     ___________________________
      END SUBROUTINE proj2d_phase



      SUBROUTINE proj3d_phase(nbp, taillx, tailly, taillz,
     &                        minx,maxx, miny,maxy, minz,maxz,
     &                        iesp,a_ax,ener_min,ener_max,resultat)
! ======================================================================
! Projection de l'espace des phases sur un volume
! -----------------------------------------------
!
! INTENT(IN)
!    + nbp : nombre de particules dans les tableaux poids, axex/y/z
!    + taillx, tailly, taillz  : taille du tableau resultat
!    + minx/y/z, maxx/y/z      : bornes de la projection, determinees
!                                avant appel de la procedure
!    + poids, axex, axey, axez : poids et positions des particules
!    + ener,ener_min,ener_max  : energies des particules 
!    + ener_min,ener_max       : bornes en energie
! INTENT(INOUT)
!    + resultat : tableau resultat incremente
! 
! ======================================================================
      USE precisions
      USE particules_klder
      
      IMPLICIT NONE

      INTEGER, INTENT(IN)             :: nbp,taillx,tailly,taillz
      REAL(KIND=kr), INTENT(IN)       :: minx,maxx,miny,maxy,minz,maxz,
     &                                   ener_min,ener_max
      INTEGER, INTENT(IN)             :: iesp
      INTEGER, DIMENSION(3), INTENT(IN)        ::  a_ax

      REAL(KIND=kr), DIMENSION(0:taillx-1,0:tailly-1,0:taillz-1),
     &               INTENT(INOUT)    :: resultat



      REAL(KIND=kr)   :: poids,axex,axey,axez,ener

      INTEGER         :: ip
      REAL(KIND=kr)   :: pasdxi,pasdyi,pasdzi,auxmx,auxpx
      REAL(KIND=kr)   :: rpx, rpy, rpz
      INTEGER         :: ipmx, ippx, ipmy, ippy, ipmz, ippz
      REAL(KIND=kr)   :: ponpmx, ponppx, ponpmy, ponppy, ponpmz, ponppz


      IF(nbp.EQ.0) RETURN

! --- Taille des mailles
      pasdxi = REAL(taillx-1,8)/(maxx-minx)
      pasdyi = REAL(tailly-1,8)/(maxy-miny)
      pasdzi = REAL(taillz-1,8)/(maxz-minz)

! Boucle sur les particules...
! ----------------------------------------------------------------------
      DO ip=1,nbp

      poids = partic(a_po   ,ip,iesp)
      axex  = partic(a_ax(1),ip,iesp)
      axey  = partic(a_ax(2),ip,iesp)
      axez  = partic(a_ax(3),ip,iesp)
      
! --------------------- Cas relativiste
      IF(dynam(iesp).EQ.2) THEN 
          ener  = (partic(a_ga, ip, iesp)-1._8)*masse(iesp)*511._8
! --------------------- Cas classique
      ELSE   
          ener  = partic(a_ga, ip, iesp)*masse(iesp)*511._8
      ENDIF   

      if ((ener.lt.ener_min).or.(ener.gt.ener_max)) cycle

! --- Coordonnees reelles en numero de maille (primale)
        rpx = (axex-minx)*pasdxi
        rpy = (axey-miny)*pasdyi
        rpz = (axez-minz)*pasdzi
! --- Coordonnees entieres primales : lignes encadrant la particule
        ipmx = NINT(rpx-0.5)
        ippx = ipmx+1
        ipmy = NINT(rpy-0.5)
        ippy = ipmy+1
        ipmz = NINT(rpz-0.5)
        ippz = ipmz+1

        if((ipmx.LT.0).OR.(ippx.GE.taillx).OR.(ippy.GE.tailly).OR.
     &     (ipmy.LT.0).OR.(ipmz.LT.0).OR.(ippz.GE.taillz)) then
          cycle
        endif

! --- Ponderations (lineaires) des differents sommets primaux
        ponpmx = REAL(ippx,8) - rpx
        ponppx = 1._8 - ponpmx
        ponpmy = REAL(ippy,8) - rpy
        ponppy = 1._8 - ponpmy
        ponpmz = REAL(ippz,8) - rpz
        ponppz = 1._8 - ponpmz

! --- Projection sur le maillage de diagnostic
        auxmx = poids*ponpmx
        auxpx = poids*ponppx
        resultat(ipmx,ipmy,ipmz) =  resultat(ipmx,ipmy,ipmz)
     &                            + auxmx*ponpmy*ponpmz
        resultat(ippx,ipmy,ipmz) =  resultat(ippx,ipmy,ipmz)
     &                            + auxpx*ponpmy*ponpmz
        resultat(ipmx,ippy,ipmz) =  resultat(ipmx,ippy,ipmz)
     &                            + auxmx*ponppy*ponpmz
        resultat(ippx,ippy,ipmz) =  resultat(ippx,ippy,ipmz)
     &                            + auxpx*ponppy*ponpmz
        resultat(ipmx,ipmy,ippz) =  resultat(ipmx,ipmy,ippz)
     &                            + auxmx*ponpmy*ponppz
        resultat(ippx,ipmy,ippz) =  resultat(ippx,ipmy,ippz)
     &                            + auxpx*ponpmy*ponppz
        resultat(ipmx,ippy,ippz) =  resultat(ipmx,ippy,ippz)
     &                            + auxmx*ponppy*ponppz
        resultat(ippx,ippy,ippz) =  resultat(ippx,ippy,ippz)
     &                            + auxpx*ponppy*ponppz

      ENDDO

      resultat(:,:,:) = resultat(:,:,:)*pasdxi*pasdyi*pasdzi

      RETURN
!     ___________________________
      END SUBROUTINE proj3d_phase



      SUBROUTINE proj1d_press(nbp,taillx,minx,maxx,
     &                        poids,axex,impx,vitx,
     &                        ener,ener_min,ener_max,
     &                        resultat)
! ======================================================================
! Calcul de la pression dans le plasma
! ------------------------------------
!
! INTENT(IN)
!    + nbp : nombre de particules dans les tableaux poids, axex
!    + taillx      : taille du tableau resultat
!    + minx, maxx  : bornes de la projection, determinees
!                    avant appel de la procedure
!    + poids, axex, impx, vitx : poids, positions, impulsion, vitesse
!                                des particules
!    + ener : energies des particules
!    + ener_min,ener_max : bornes en energies
! INTENT(INOUT)
!    + resultat : tableau resultat incremente
! 
! ======================================================================
      USE precisions
      IMPLICIT NONE

      INTEGER, INTENT(IN)                       :: nbp,taillx
      REAL(KIND=kr), INTENT(IN)                 :: minx, maxx,
     &                                             ener_min,ener_max
      REAL(KIND=kr), DIMENSION(nbp), INTENT(IN) :: poids,axex,impx,
     &                                             vitx,ener
      REAL(KIND=kr), DIMENSION(0:taillx-1), INTENT(INOUT) 
     &                                          :: resultat

      INTEGER         :: ip
      REAL(KIND=kr)   :: pasdxi,aux,auxmx,auxpx
      REAL(KIND=kr)   :: rpx
      INTEGER         :: ipmx,ippx
      REAL(KIND=kr)   :: ponpmx,ponppx


      IF(nbp.EQ.0) RETURN

! --- Taille des mailles
      pasdxi = REAL(taillx-1,8)/(maxx-minx)

! Boucle sur les particules...
! ----------------------------------------------------------------------
      DO ip=1,nbp

      if ((ener(ip).lt.ener_min).or.(ener(ip).gt.ener_max)) cycle

! --- Coordonnees reelles en numero de maille (primale)
        rpx = (axex(ip)-minx)*pasdxi
! --- Coordonnees entieres primales : lignes encadrant la particule
        ipmx = INT(rpx)
        ippx = ipmx+1
! --- Ponderations (lineaires) des differents sommets primaux
        ponpmx = REAL(ippx,8) - rpx
        ponppx = 1._8 - ponpmx

! --- Projection sur le maillage de diagnostic
        aux = poids(ip)*impx(ip)*vitx(ip)
        auxmx = aux*ponpmx
        auxpx = aux*ponppx

        if((ipmx.LT.0).OR.(ippx.GE.taillx)) then
          print*, 'partic. ',ip
          print*, 'x ', axex(ip)
          print*, 'minx ', minx
          print*, 'ipmx, ippx ', ipmx, ippx
	  cycle
        endif

        resultat(ipmx) = resultat(ipmx) + auxmx
        resultat(ippx) = resultat(ippx) + auxpx

      ENDDO

      resultat(:) = resultat(:)*pasdxi

      RETURN
!     ___________________________
      END SUBROUTINE proj1d_press



      SUBROUTINE proj2d_emitt(nbp, taillx,tailly,
     &                        minx,maxx,ming,maxg,
     &                        poids,axex,axey,axeg,axepx,
     &                        ener,ener_min,ener_max,
     &                        resultat)
! ======================================================================
! Projection de l'espace des phases sur un plan pour calcul emittance
! ---------------------------------------------
!
! INTENT(IN)
!    + nbp : nombre de particules dans les tableaux poids, axex, axey
!    + taillx, tailly : taille du tableau resultat
!    + minx, maxx : bornes spatiales de la projection, determinees
!                   avant appel de la procedure
!    + ming, maxg : bornes en gamma de la projection, fournies 
!                   dans le fichier de donnees
!    + poids, axex, axey, axeg, axepx : poids, position, impulsion, 
!                   gamma et impulsion longitudinale des particules
!    + ener : energies des particules 
!    + ener_min,ener_max : bornes en energies
! INTENT(INOUT)
!    + resultat : tableau resultat incremente
! 
! ======================================================================
      USE precisions
      IMPLICIT NONE

      INTEGER, INTENT(IN)                       :: nbp, taillx, tailly
      REAL(KIND=kr), INTENT(INOUT)              :: minx,maxx, ming,maxg,
     &                                             ener_min,ener_max
      REAL(KIND=kr), DIMENSION(nbp), INTENT(IN) :: poids,
     &                                             axex, axey,
     &                                             axeg, axepx,
     &                                             ener
      REAL(KIND=kr), DIMENSION(0:taillx-1,0:tailly-1), INTENT(INOUT) 
     &                                          :: resultat

      INTEGER         :: ip
      REAL(KIND=kr)   :: miny, maxy, bg, cos_coll
      REAL(KIND=kr)   :: pasdxi, pasdyi, auxmx, auxpx
      REAL(KIND=kr)   :: rpx, rpy
      INTEGER         :: ipmx, ippx, ipmy, ippy
      REAL(KIND=kr)   :: ponpmx, ponppx, ponpmy, ponppy


      IF(nbp.EQ.0) RETURN

      miny = -0.45
      maxy = +0.45
! ->- Collimation : cos_coll=1.-dOmega_collimateur/2Pi
      cos_coll = 1.-0.05/(2.*3.1415926)

! --- Taille des mailles
      pasdxi = REAL(taillx-1,8)/(maxx-minx)
      pasdyi = REAL(tailly-1,8)/(maxy-miny)

! Boucle sur les particules...
! ----------------------------------------------------------------------
      DO ip=1,nbp

! A revoir car redondant avec ce qui suit
      if ((ener(ip).lt.ener_min).or.(ener(ip).gt.ener_max)) cycle

! ->- Fenetre en gamma
        IF((axeg(ip).LT.ming).OR.(axeg(ip).GT.maxg)) CYCLE

! ->- Impulsion normalisee
        bg = SQRT(axeg(ip)**2-1.)

! ->- Collimation : cos_coll=1.-dOmega_coll/2Pi
        IF(axepx(ip).LT.cos_coll*bg) CYCLE

! --- Coordonnees reelles en numero de maille (primale)
        rpx = (axex(ip)   -minx)*pasdxi
        rpy = (axey(ip)/bg-miny)*pasdyi
! --- Coordonnees entieres primales : lignes encadrant la particule
        ipmx = INT(rpx)
        ippx = ipmx+1
        ipmy = INT(rpy)
        ippy = ipmy+1

! ->- Fenetre en espace
        IF((ipmx.LT.0).OR.(ippx.GE.taillx)) CYCLE

! --- Ponderations (lineaires) des differents sommets primaux
        ponpmx = REAL(ippx,8) - rpx
        ponppx = 1._8 - ponpmx
        ponpmy = REAL(ippy,8) - rpy
        ponppy = 1._8 - ponpmy

! --- Projection sur le maillage de diagnostic
        auxmx = poids(ip)*ponpmx
        auxpx = poids(ip)*ponppx
        resultat(ipmx,ipmy) = resultat(ipmx,ipmy) + auxmx*ponpmy
        resultat(ippx,ipmy) = resultat(ippx,ipmy) + auxpx*ponpmy
        resultat(ipmx,ippy) = resultat(ipmx,ippy) + auxmx*ponppy
        resultat(ippx,ippy) = resultat(ippx,ippy) + auxpx*ponppy

      ENDDO

      resultat(:,:) = resultat(:,:)*pasdxi*pasdyi

      ming = miny
      maxg = maxy

      RETURN
!     ___________________________
      END SUBROUTINE proj2d_emitt



      SUBROUTINE proj1d_currt(nbp, taillx, minx,maxx,
     &                        toto, iesp, a_ax, a_cx, 
     &                        ener_min,ener_max,
     &                        resultat)
! ======================================================================
! Calcul d'un courant projet� sur un maillage 1d dans le plasma
! ----------------------------------------------------------------
!
! INTENT(IN)
!    + nbp : nombre de particules dans les tableaux poids, axex
!    + taillx      : taille du tableau resultat
!    + minx, maxx  : bornes de la projection, determinees
!                    avant appel de la procedure
!    + poids, axex, vitx : poids, positions, vitesse des particules
!    + ener : energies des particules
!    + ener_min,ener_max : bornes en energies
! INTENT(INOUT)
!    + resultat : tableau(taillx) resultat incremente
! 
! ======================================================================
      USE precisions
      USE particules_klder
      
      IMPLICIT NONE

      INTEGER, INTENT(IN)                       :: nbp, taillx
      REAL(KIND=kr), INTENT(IN)                 :: minx, maxx,
     &                                             ener_min,ener_max
      INTEGER, DIMENSION(3), INTENT(IN)        ::  a_ax

      CHARACTER, INTENT(IN)          :: toto
      INTEGER, INTENT(IN)          :: iesp, a_cx
      REAL(KIND=kr), DIMENSION(0:taillx-1), INTENT(INOUT) 
     &                                          :: resultat

      REAL(KIND=kr)   ::    poids,axex,vitx,ener
      INTEGER         :: ip
      REAL(KIND=kr)   :: pasdxi, aux, auxmx, auxpx
      REAL(KIND=kr)   :: rpx
      INTEGER         :: ipmx, ippx
      REAL(KIND=kr)   :: ponpmx, ponppx


      IF(nbp.EQ.0) RETURN

! --- Taille des mailles
      pasdxi = REAL(taillx-1,8)/(maxx-minx)

! Boucle sur les particules...
! ----------------------------------------------------------------------
      DO ip=1,nbp

      poids = partic(a_po   ,ip,iesp)
      axex  = partic(a_ax(1),ip,iesp)
      
! --------------------- Cas relativiste
      IF(dynam(iesp).EQ.2) THEN 
! --- Calcul d'un courant
        IF(toto.EQ.'i' .OR. toto.EQ.'j' .OR. toto.EQ.'k') THEN          
          vitx  = partic(a_cx   ,ip,iesp)
     &                       /partic(a_ga   ,ip,iesp)
          ener  = (partic(a_ga   ,ip,iesp)-1._8)
     &                       *masse(iesp)*511._8
! --- Calcul du flux d'energie     
        ELSEIF(toto.EQ.'r' .OR. toto.EQ.'s' .OR. toto.EQ.'t') THEN
          vitx  = partic(a_cx   ,ip,iesp)
     &     *masse(iesp)*(1._8-1._8/partic(a_ga,ip,iesp))
          ener  = (partic(a_ga   ,ip,iesp)-1._8)
     &                       *masse(iesp)*511._8
! --- Calcul de la densite d'energie, i.e toto.EQ.'g'
        ELSE 
          vitx  = masse(iesp)*(partic(a_ga   ,ip,iesp)-1._8)
          ener  = (partic(a_ga   ,ip,iesp)-1._8)
     &                       *masse(iesp)*511._8
        ENDIF
! --------------------- Cas classique
      ELSE   
! --- Calcul d'un courant
        IF(toto.EQ.'i' .OR. toto.EQ.'j' .OR. toto.EQ.'k') THEN          
          vitx  = partic(a_cx   ,ip,iesp)
          ener  = partic(a_ga   ,ip,iesp)
     &                       *masse(iesp)*511._8
! --- Calcul du flux d'energie     
        ELSEIF(toto.EQ.'r' .OR. toto.EQ.'s' .OR. toto.EQ.'t') THEN
          vitx  =  partic(a_cx   ,ip,iesp)
     &           *masse(iesp)*partic(a_ga   ,ip,iesp)
          ener  = partic(a_ga   ,ip,iesp)
     &                       *masse(iesp)*511._8
! --- Calcul de la densite d'energie, i.e toto.EQ.'g'
        ELSE 
          vitx  = masse(iesp)*partic(a_ga   ,ip,iesp)
          ener  = partic(a_ga   ,ip,iesp)
     &                       *masse(iesp)*511._8
        ENDIF         
      ENDIF      

      IF ((ener.LT.ABS(ener_min)).OR.
     &    (ener.GT.ABS(ener_max))) CYCLE
! --- Direction ok ?     
        IF(ener_max*vitx.LT.0.) CYCLE
	
c        IF((a_dx.EQ.+1).AND.(vitx.LT.0.)) CYCLE
c	IF((a_dx.EQ.+1).AND.(vitx.LT.0.7)) CYCLE  ! Modif. LG 24/06/05
c        IF((a_dx.EQ.-1).AND.(vitx.GT.0.)) CYCLE

! --- Coordonnees reelles en numero de maille (primale)
        rpx = (axex-minx)*pasdxi
! --- Coordonnees entieres primales : lignes encadrant la particule
        ipmx = NINT(rpx-0.5)
        ippx = ipmx+1
	
        if((ipmx.LT.0).OR.(ippx.GE.taillx)) then
          print*, 'partic. ',ip
          print*, 'x ', axex
          print*, 'minx ', minx
          print*, 'ipmx, ippx ', ipmx, ippx
	  cycle
        endif	
	
! --- Ponderations (lineaires) des differents sommets primaux
        ponpmx = REAL(ippx,8) - rpx
        ponppx = 1._8 - ponpmx

! --- Projection sur le maillage de diagnostic
        aux = poids*vitx
        auxmx = aux*ponpmx
        auxpx = aux*ponppx

        resultat(ipmx) = resultat(ipmx) + auxmx
        resultat(ippx) = resultat(ippx) + auxpx

      ENDDO

      resultat(:) = resultat(:)*pasdxi

      RETURN
!     ___________________________
      END SUBROUTINE proj1d_currt
      
      
      SUBROUTINE proj2d_currt(nbp, taillx, tailly, 
     &                        minx,maxx, miny,maxy,
     &                        toto,iesp, a_ax, a_cx,
     &                        ener_min,ener_max,resultat)
! ======================================================================
! Calcul d'un courant projet� sur un maillage 2d dans le plasma
! ----------------------------------------------------------------
!
! INTENT(IN)
!    + nbp : nombre de particules dans les tableaux poids, axex
!    + taillx,tailly: tailles du tableau resultat
!    + minx, maxx, miny, maxy : bornes de la projection, determinees
!                               avant appel de la procedure
!    + poids, axex, axey, vitx : poids, positions, vitesse des particules
!    + ener : energies des particules
!    + ener_min,ener_max : bornes en energies
! INTENT(INOUT)
!    + resultat : tableau(taillx,tailly) resultat incremente
! 
! ======================================================================
      USE precisions
      USE particules_klder
      
      IMPLICIT NONE

      INTEGER, INTENT(IN)                :: nbp, taillx, tailly
      REAL(KIND=kr), INTENT(IN)          :: minx, maxx, miny,maxy,
     &                                      ener_min,ener_max
      INTEGER, DIMENSION(3), INTENT(IN)        ::  a_ax
     
      CHARACTER, INTENT(IN)          :: toto
      INTEGER, INTENT(IN)          :: iesp, a_cx
      
      REAL(KIND=kr), DIMENSION(0:taillx-1,0:tailly-1),
     &               INTENT(INOUT)       :: resultat

      REAL(KIND=kr)   ::    poids,axex,axey,vitx,ener

      INTEGER         :: ip
      REAL(KIND=kr)   :: pasdxi, pasdyi, auxmx, auxpx
      REAL(KIND=kr)   :: rpx, rpy
      INTEGER         :: ipmx, ippx, ipmy, ippy
      REAL(KIND=kr)   :: ponpmx, ponppx, ponpmy, ponppy


      IF(nbp.EQ.0) RETURN

! --- Taille des mailles
      pasdxi = REAL(taillx-1,8)/(maxx-minx)
      pasdyi = REAL(tailly-1,8)/(maxy-miny)           
      
! Boucle sur les particules...
! ----------------------------------------------------------------------
      DO ip=1,nbp
      
      poids = partic(a_po   ,ip,iesp)
      axex  = partic(a_ax(1),ip,iesp)
      axey  = partic(a_ax(2),ip,iesp)
      
! --------------------- Cas relativiste
      IF(dynam(iesp).EQ.2) THEN 
! --- Calcul d'un courant
        IF(toto.EQ.'i' .OR. toto.EQ.'j' .OR. toto.EQ.'k') THEN          
          vitx  = partic(a_cx   ,ip,iesp)
     &                       /partic(a_ga   ,ip,iesp)
          ener  = (partic(a_ga   ,ip,iesp)-1._8)
     &                       *masse(iesp)*511._8
! --- Calcul du flux d'energie     
        ELSEIF(toto.EQ.'r' .OR. toto.EQ.'s' .OR. toto.EQ.'t') THEN
          vitx  = partic(a_cx   ,ip,iesp)
     &     *masse(iesp)*(1._8-1._8/partic(a_ga,ip,iesp))
          ener  = (partic(a_ga   ,ip,iesp)-1._8)
     &                       *masse(iesp)*511._8
! --- Calcul de la densite d'energie, i.e toto.EQ.'g'
        ELSE 
          vitx  = masse(iesp)*(partic(a_ga   ,ip,iesp)-1._8)
          ener  = (partic(a_ga   ,ip,iesp)-1._8)
     &                       *masse(iesp)*511._8
        ENDIF
! --------------------- Cas classique
      ELSE   
! --- Calcul d'un courant
        IF(toto.EQ.'i' .OR. toto.EQ.'j' .OR. toto.EQ.'k') THEN          
          vitx  = partic(a_cx   ,ip,iesp)
          ener  = partic(a_ga   ,ip,iesp)
     &                       *masse(iesp)*511._8
! --- Calcul du flux d'energie     
        ELSEIF(toto.EQ.'r' .OR. toto.EQ.'s' .OR. toto.EQ.'t') THEN
          vitx  =  partic(a_cx   ,ip,iesp)
     &           *masse(iesp)*partic(a_ga   ,ip,iesp)
          ener  = partic(a_ga   ,ip,iesp)
     &                       *masse(iesp)*511._8
! --- Calcul de la densite d'energie, i.e toto.EQ.'g'
        ELSE 
          vitx  = masse(iesp)*partic(a_ga   ,ip,iesp)
          ener  = partic(a_ga   ,ip,iesp)
     &                       *masse(iesp)*511._8
        ENDIF         
      ENDIF           

      IF ((ener.LT.ABS(ener_min)).OR.
     &    (ener.GT.ABS(ener_max))) CYCLE
! Direction OK ?
        IF(ener_max*vitx.LT.0.) CYCLE

! --- Coordonnees reelles en numero de maille (primale)
        rpx = (axex-minx)*pasdxi
        rpy = (axey-miny)*pasdyi	
! --- Coordonnees entieres primales : lignes encadrant la particule
        ipmx = NINT(rpx-0.5)
        ippx = ipmx+1
        ipmy = NINT(rpy-0.5)
        ippy = ipmy+1        
	
	if((ipmx.LT.0).OR.(ippx.GE.taillx).OR.
     &     (ipmy.LT.0).OR.(ippy.GE.tailly)    ) then
          cycle
        endif	
	
! --- Ponderations (lineaires) des differents sommets primaux
        ponpmx = REAL(ippx,8) - rpx
        ponppx = 1._8 - ponpmx
        ponpmy = REAL(ippy,8) - rpy
        ponppy = 1._8 - ponpmy

! --- Projection sur le maillage de diagnostic        
        auxmx = poids*ponpmx*vitx
        auxpx = poids*ponppx*vitx
        resultat(ipmx,ipmy) = resultat(ipmx,ipmy) + auxmx*ponpmy
        resultat(ippx,ipmy) = resultat(ippx,ipmy) + auxpx*ponpmy
        resultat(ipmx,ippy) = resultat(ipmx,ippy) + auxmx*ponppy
        resultat(ippx,ippy) = resultat(ippx,ippy) + auxpx*ponppy

      ENDDO

      resultat = resultat*pasdxi*pasdyi

      RETURN
!     ___________________________
      END SUBROUTINE proj2d_currt      
      
      
      SUBROUTINE proj3d_currt(nbp, taillx, tailly, taillz,
     &                        minx,maxx, miny,maxy, minz,maxz,
     &                        toto, iesp, a_ax, a_cx,
     &                        ener_min,ener_max,
     &                        resultat)
! ======================================================================
! Projection de l'espace des phases sur un volume
! -----------------------------------------------
!
! INTENT(IN)
!    + nbp : nombre de particules dans les tableaux poids, axex/y/z
!    + taillx, tailly, taillz  : taille du tableau resultat
!    + minx/y/z, maxx/y/z      : bornes de la projection, determinees
!                                avant appel de la procedure
!    + poids, axex, axey, axez, vitx  : poids, positions, vitesse des particules
!    + ener : energies de particules
!    + ener_min,ener_max  : bornes min et max de la gamme en energie 
! INTENT(INOUT)
!    + resultat : tableau(taillx,tailly,taillz) resultat incremente
! 
! ======================================================================
      USE precisions
      USE particules_klder
      
      IMPLICIT NONE

      INTEGER, INTENT(IN)          :: nbp, taillx, tailly, taillz
      REAL(KIND=kr), INTENT(IN)    :: minx,maxx, miny,maxy, minz,maxz,
     &                                ener_min,ener_max
      INTEGER, DIMENSION(3), INTENT(IN)        ::  a_ax
     
      CHARACTER, INTENT(IN)          :: toto
      INTEGER, INTENT(IN)          :: iesp, a_cx
      
      REAL(KIND=kr), DIMENSION(0:taillx-1,0:tailly-1,0:taillz-1),
     &               INTENT(INOUT) :: resultat

      REAL(KIND=kr)   :: poids, axex, axey, axez, vitx, ener
      INTEGER         :: ip
      REAL(KIND=kr)   :: pasdxi, pasdyi, pasdzi, auxmx, auxpx
      REAL(KIND=kr)   :: rpx, rpy, rpz
      INTEGER         :: ipmx, ippx, ipmy, ippy, ipmz, ippz
      REAL(KIND=kr)   :: ponpmx, ponppx, ponpmy, ponppy, ponpmz,ponppz


      IF(nbp.EQ.0) RETURN

! --- Taille des mailles
      pasdxi = REAL(taillx-1,8)/(maxx-minx)
      pasdyi = REAL(tailly-1,8)/(maxy-miny)
      pasdzi = REAL(taillz-1,8)/(maxz-minz)

! Boucle sur les particules...
! ----------------------------------------------------------------------
      DO ip=1,nbp

      poids = partic(a_po   ,ip,iesp)
      axex  = partic(a_ax(1),ip,iesp)
      axey  = partic(a_ax(2),ip,iesp)
      axez  = partic(a_ax(3),ip,iesp)
      
! --------------------- Cas relativiste
      IF(dynam(iesp).EQ.2) THEN 
! --- Calcul d'un courant
        IF(toto.EQ.'i' .OR. toto.EQ.'j' .OR. toto.EQ.'k') THEN          
          vitx  = partic(a_cx   ,ip,iesp)
     &                       /partic(a_ga   ,ip,iesp)
          ener  = (partic(a_ga   ,ip,iesp)-1._8)
     &                       *masse(iesp)*511._8
! --- Calcul du flux d'energie     
        ELSEIF(toto.EQ.'r' .OR. toto.EQ.'s' .OR. toto.EQ.'t') THEN
          vitx  = partic(a_cx   ,ip,iesp)
     &     *masse(iesp)*(1._8-1._8/partic(a_ga,ip,iesp))
          ener  = (partic(a_ga   ,ip,iesp)-1._8)
     &                       *masse(iesp)*511._8
! --- Calcul de la densite d'energie, i.e toto.EQ.'g'
        ELSE 
          vitx  = masse(iesp)*(partic(a_ga   ,ip,iesp)-1._8)
          ener  = (partic(a_ga   ,ip,iesp)-1._8)
     &                       *masse(iesp)*511._8
        ENDIF
! --------------------- Cas classique
      ELSE   
! --- Calcul d'un courant
        IF(toto.EQ.'i' .OR. toto.EQ.'j' .OR. toto.EQ.'k') THEN          
          vitx  = partic(a_cx   ,ip,iesp)
          ener  = partic(a_ga   ,ip,iesp)
     &                       *masse(iesp)*511._8
! --- Calcul du flux d'energie     
        ELSEIF(toto.EQ.'r' .OR. toto.EQ.'s' .OR. toto.EQ.'t') THEN
          vitx  =  partic(a_cx   ,ip,iesp)
     &           *masse(iesp)*partic(a_ga   ,ip,iesp)
          ener  = partic(a_ga   ,ip,iesp)
     &                       *masse(iesp)*511._8
! --- Calcul de la densite d'energie, i.e toto.EQ.'g'
        ELSE 
          vitx  = masse(iesp)*partic(a_ga   ,ip,iesp)
          ener  = partic(a_ga   ,ip,iesp)
     &                       *masse(iesp)*511._8
        ENDIF         
      ENDIF      

      IF ((ener.lt.ABS(ener_min)).OR.
     &    (ener.gt.ABS(ener_max))) CYCLE      
c! --- Direction ok ?
        IF(ener_max*vitx.LT.0.) CYCLE
	
c        IF((a_dx.EQ.+1).AND.(vitx.LT.0.)) CYCLE
c	IF((a_dx.EQ.+1).AND.(vitx.LT.0.7)) CYCLE  ! modif LG 24/06/05
c        IF((a_dx.EQ.-1).AND.(vitx.GT.0.)) CYCLE
	
! --- Coordonnees reelles en numero de maille (primale)
        rpx = (axex-minx)*pasdxi
        rpy = (axey-miny)*pasdyi
        rpz = (axez-minz)*pasdzi
! --- Coordonnees entieres primales : lignes encadrant la particule
        ipmx = NINT(rpx-0.5)
        ippx = ipmx+1
        ipmy = NINT(rpy-0.5)
        ippy = ipmy+1
        ipmz = NINT(rpz-0.5)
        ippz = ipmz+1

        if((ipmx.LT.0).OR.(ippx.GE.taillx).OR.(ippy.GE.tailly).OR.
     &     (ipmy.LT.0).OR.(ipmz.LT.0).OR.(ippz.GE.taillz)) then
          cycle
        endif

! --- Ponderations (lineaires) des differents sommets primaux
        ponpmx = REAL(ippx,8) - rpx
        ponppx = 1._8 - ponpmx
        ponpmy = REAL(ippy,8) - rpy
        ponppy = 1._8 - ponpmy
        ponpmz = REAL(ippz,8) - rpz
        ponppz = 1._8 - ponpmz

! --- Projection sur le maillage de diagnostic
        auxmx = poids*ponpmx*vitx
        auxpx = poids*ponppx*vitx
        resultat(ipmx,ipmy,ipmz) =  resultat(ipmx,ipmy,ipmz)
     &                            + auxmx*ponpmy*ponpmz
        resultat(ippx,ipmy,ipmz) =  resultat(ippx,ipmy,ipmz)
     &                            + auxpx*ponpmy*ponpmz
        resultat(ipmx,ippy,ipmz) =  resultat(ipmx,ippy,ipmz)
     &                            + auxmx*ponppy*ponpmz
        resultat(ippx,ippy,ipmz) =  resultat(ippx,ippy,ipmz)
     &                            + auxpx*ponppy*ponpmz
        resultat(ipmx,ipmy,ippz) =  resultat(ipmx,ipmy,ippz)
     &                            + auxmx*ponpmy*ponppz
        resultat(ippx,ipmy,ippz) =  resultat(ippx,ipmy,ippz)
     &                            + auxpx*ponpmy*ponppz
        resultat(ipmx,ippy,ippz) =  resultat(ipmx,ippy,ippz)
     &                            + auxmx*ponppy*ponppz
        resultat(ippx,ippy,ippz) =  resultat(ippx,ippy,ippz)
     &                            + auxpx*ponppy*ponppz

      ENDDO

      resultat(:,:,:) = resultat(:,:,:)*pasdxi*pasdyi*pasdzi

      RETURN
!     ___________________________
      END SUBROUTINE proj3d_currt
      
      
