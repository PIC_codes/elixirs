! ======================================================================
! collisions.f
! ------------
!
!  SUBROUTINE coll_init
!  SUBROUTINE intra_collisions
!  SUBROUTINE incrementation
!  SUBROUTINE inter_collisions
!  SUBROUTINE incrementation2
!
! ======================================================================

      SUBROUTINE coll_init(col_i)
! ======================================================================
! En fonction des parametres fournis, calcul du lorarithme Coulombien,
! des frequences de collisions, et de l'angle caracteristique
! ======================================================================
      USE domaines
      USE particules_klder
      USE collisions
      USE laser
      USE temps
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)       :: col_i

!      REAL(kind=kr), PARAMETER   :: PI   = 3.14159265359
      REAL(kind=kr), PARAMETER   :: Qe   = 1.6022E-19
      REAL(kind=kr), PARAMETER   :: Me   = 9.1094E-31
      REAL(kind=kr), PARAMETER   :: eps0 = 8.8542E-12

      REAL(kind=kr)              :: temp1, temp2, tempe, tempi
      REAL(kind=kr)              :: kDe, kD2, kD, kmax
      REAL(kind=kr)              :: m12, nmin, V2
      REAL(kind=kr)              :: col_freq
      INTEGER                   :: i, i1, i2, ielec, ndl

      i1 = col_p1(col_i) ; i2 = col_p2(col_i)
      per_tri(i1)=1      ; per_tri(i2)=1

! --- Temperatures des electrons (en keV)
! --- Temperatures des deux especes collisionnantes (en keV)
      DO i=1,nesp
         IF ((masse(i).EQ.1.).AND.(charge(i).EQ.-1.)) ielec=i
      ENDDO
      tempe = 0._8 ; temp1 = 0._8 ; temp2 = 0._8 ; ndl = 1
      IF((dim_cas.GE.1).AND.(dim_cas.LE.6)) THEN
        tempe=tempe+vthx(ielec)
        temp1=temp1+vthx(i1)
        temp2=temp2+vthx(i2)
        ndl  =1
      ENDIF
      IF((dim_cas.GE.2).AND.(dim_cas.LE.6)) THEN
        tempe=tempe+vthy(ielec)
        temp1=temp1+vthy(i1)
        temp2=temp2+vthy(i2)
        ndl  =2
      ENDIF
      IF((dim_cas.EQ.3).OR.(dim_cas.EQ.5).OR.(dim_cas.EQ.6)) THEN
        tempe=tempe+vthz(ielec)
        temp1=temp1+vthz(i1)
        temp2=temp2+vthz(i2)
        ndl  =3
      ENDIF
      tempe=tempe/ndl ; temp1=temp1/ndl ; temp2=temp2/ndl

! --- Inverse de la longueur de Debye electronique
      kDe = SQRT( densite(ielec)*Me*9.E16_8*4._8*PI**2_8
     &           /(tempe*1.E3_8*Qe) )   / (lambda(1)*1.E-6_8)

! --- kD par la formule de Decoster ("Modeling of Collisions")
      kD2 = 0.
      do i=1,nesp_p
        SELECT CASE (dim_cas)
          CASE(1    ) ; tempi =  vthx(i)
          CASE(2,4  ) ; tempi = (vthx(i)+vthy(i)        )/2.
          CASE(3,5,6) ; tempi = (vthx(i)+vthy(i)+vthz(i))/3.
        END SELECT
        kD2 = kD2
     &       +   densite(i)*ABS(charge(i))*(2.*PI)**2
     &        / (tempi + masse(i)/(masse(i1)/temp1+masse(i2)/temp2))
     &        * 511. / (lambda(1)*1.E-6_8)**2
      enddo
      kD = SQRT(kD2)

      m12 = masse(i1)*masse(i2)/(masse(i1)+masse(i2))
      V2  = 2.*( temp1/masse(i1) + temp2/masse(i2) )*(1.E3_8*Qe/Me)
      kmax = MIN(  4._8*PI*eps0*m12*Me*V2
     &                 /(ABS(charge(i1)*charge(i2))*Qe**2_8) ,
     &             2._8*m12*Me*SQRT(V2)/1.06E-34_8 )

! Rappel : formule donnant kmax dans le cas ou les deux particules sont
! du meme type.
!     kmax = MIN( 8.*PI*eps0*temp1*1e3 * Qe/(charge(i1)*Qe)**2 ,
!    &            2.*SQRT(Me*masse(i1)*temp1*1e3 * Qe)/1.06E-34 )

      WRITE(fdbg,"('---------------------------------------------')")
      WRITE(fdbg,"('Temperature electrons : ',E9.3,' keV')") tempe
      WRITE(fdbg,"('Temperature espece 1  : ',E9.3,' keV')") temp1
      WRITE(fdbg,"('Temperature espece 2  : ',E9.3,' keV')") temp2
      WRITE(fdbg,"('Long. Debye elect. : ',E9.3,' micron')") 1.E6_8/kDe
      WRITE(fdbg,"('1./kmax            : ',E9.3,' micron')") 1.E6_8/kmax
      WRITE(fdbg,"('1./kD (Decoster)   : ',E9.3,' micron')") 1.E6_8/kD

! --- Calcul du logarithme coulombien
      IF(col_log(col_i).LT.0.) THEN
        col_log(col_i) = LOG(kmax/kD)
        WRITE(fdbg,"('Log. Coulombien calcule pour les coll. de ',
     &               I2,' et ',I2,' : ',E9.3)") i1,i2,col_log(col_i)
      ELSE
        WRITE(fdbg,"('Log. Coulombien calcule pour les coll. de ',
     &               I2,' et ',I2,' : ',E9.3)") i1,i2,LOG(kmax/kD)
        WRITE(fdbg,"('mais fixe par le fichier de donnees a : ',
     &               E9.3)") col_log(col_i)
      ENDIF

! --- Calcul des frequences de collision
! --- i1 -> i2
      col_freq = (4._8/3._8)*SQRT(PI)*densite(i2)*(charge(i1)**2_8)*
     &           ABS(charge(i2))*col_log(col_i)*Qe**2_8 /
     &           ( masse(i1)*m12*((V2/9E16_8)**1.5_8)*
     &           eps0*((lambda(1)*1E-6_8)**2_8)*Me*3E8_8 )
      WRITE(fdbg,"('Frequence de coll. pour l''impulsion de ',
     &             I2,' sur ',I2,' : ')") i1,i2
      WRITE(fdbg,"('- en Hz : ',E9.3)") col_freq
      WRITE(fdbg,"('- en w0 : ',E9.3)") col_freq*lambda(1)/(2.*PI*3.E14)
      IF (i1.EQ.i2) GOTO 100
      col_freq = 2._8*masse(i1)/(masse(i1)+masse(i2))*col_freq
      WRITE(fdbg,"('Frequence de coll. pour l''energie de ',
     &             I2,' sur ',I2,' : ')") i1,i2
      WRITE(fdbg,"('- en Hz : ',E9.3)") col_freq
      WRITE(fdbg,"('- en w0 : ',E9.3)") col_freq*lambda(1)/(2.*PI*3.E14)

! --- i2 -> i1
      col_freq = (4._8/3._8)*SQRT(PI)*densite(i1)*(charge(i2)**2_8)*
     &           ABS(charge(i1))*col_log(col_i)*Qe**2_8 /
     &           ( masse(i2)*m12*((V2/9E16_8)**1.5_8)*
     &           eps0*((lambda(1)*1E-6_8)**2_8)*Me*3E8_8 )
      WRITE(fdbg,"('Frequence de coll. pour l''impulsion de ',
     &             I2,' sur ',I2,' : ')") i2,i1
      WRITE(fdbg,"('- en Hz : ',E9.3)") col_freq
      WRITE(fdbg,"('- en w0 : ',E9.3)") col_freq*lambda(1)/(2.*PI*3.E14)
      col_freq = 2._8*masse(i2)/(masse(i2)+masse(i1))*col_freq
      WRITE(fdbg,"('Frequence de coll. pour l''energie de ',
     &             I2,' sur ',I2,' : ')") i2,i1
      WRITE(fdbg,"('- en Hz : ',E9.3)") col_freq
      WRITE(fdbg,"('- en w0 : ',E9.3)") col_freq*lambda(1)/(2.*PI*3.E14)

 100  CONTINUE

! --- Precalcul de l'angle caracteristique de la collision
!     (les intra-collisions ne sont qu'un cas particulier)
      nmin = MIN ( densite(i1)/ABS(charge(i1)),
     &             densite(i2)/ABS(charge(i2)) )
      col_dlt(col_i) = (Qe*charge(i1)*charge(i2))**2_8
     &                * nmin * col_per(col_i)*pasdt
     &                /(4._8*eps0*Me*9.E16_8*m12**2_8*lambda(1)*1.E-6_8)

      RETURN
!     ________________________
      END SUBROUTINE coll_init



      SUBROUTINE intra_collisions(col_i)
! ======================================================================
! Fonctionne uniquement en 3D dans le cadre de la mecanique classique
! pour l'instant.
! ======================================================================
      USE precisions
      USE domaines
      USE particules_klder
      USE collisions
      USE laser
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)    :: col_i

!=====Variables apparaissant dans 1
      INTEGER                                 :: i1, icell, nbpicell, n0
      REAL(KIND=kr),DIMENSION(:,:),ALLOCATABLE :: dummy

!=====Variables apparaissant dans 2
      INTEGER                                  :: iround, ip1, ip2
      INTEGER(kind=kr)                          :: graine
      REAL(KIND=kr),DIMENSION(:),ALLOCATABLE   :: moisson, provisoire

!=====Variables apparaissant dans 3
      REAL(KIND=kr),DIMENSION(:,:),ALLOCATABLE :: couple

      INTERFACE
         SUBROUTINE incrementation(col_i,logcou,tableau)
          USE precisions
          INTEGER, INTENT(IN)                          :: col_i
          REAL(KIND=kr), INTENT(IN)                    :: logcou
          REAL(KIND=kr), DIMENSION(:,:), INTENT(INOUT) :: tableau
          END SUBROUTINE incrementation
         SUBROUTINE aleat_unif(gerbe,methode,graine)
          USE precisions
          REAL(KIND=kr), DIMENSION(:), INTENT(OUT)   :: gerbe
          INTEGER                    , INTENT(IN)    :: methode
          INTEGER(kind=kr)            , INTENT(INOUT) :: graine
         END SUBROUTINE aleat_unif
      END INTERFACE

!=======================================================================

      i1 = col_p1(col_i)

      IF(.NOT.ALLOCATED(dummy))
     &        ALLOCATE (dummy(SIZE(partic,1),300))
      IF(.NOT.ALLOCATED(provisoire))
     &        ALLOCATE (provisoire(SIZE(partic,1)))
      IF(.NOT.ALLOCATED(couple))
     &        ALLOCATE (couple(SIZE(partic,1),2))

      DO icell = 1,(nulpx-nulmx)*(nulpy-nulmy)*(nulpz-nulmz)

! -1- Calcul du nombre de particules dans la cellule
!     et creation d'une copie, dummy

        IF(icell.NE.(nulpx-nulmx)*(nulpy-nulmy)
     &                           *(nulpz-nulmz)) THEN
          nbpicell=compteur(icell+1,i1)-compteur(icell,i1)
        ELSE
          nbpicell=nbpa(i1)+1-compteur( (nulpx-nulmx)*(nulpy-nulmy)
     &                                 *(nulpz-nulmz),i1)
        ENDIF
! ----- Si il n'y a pas de quoi collisionner, on saute la cellule
        IF(nbpicell.LE.1) CYCLE
! ----- Si il n'y a pas assez de place dans les tableaux, on avertit
        IF(nbpicell.GT.300)
     &    print*, 'Depassement de taille : nbpicell=',nbpicell
        ALLOCATE (moisson(nbpicell-1))
        n0=compteur(icell,i1)
        dummy(:,1:nbpicell)=partic(:,n0:n0+nbpicell-1,i1)


        DO iround = 1,col_rnd(col_i)

! -2- Permutation aleatoire des particules
! ----- Borne superieure theorique pour col_rnd(col_i) :
!       nbpicell*(nbpicell-1)/2

          CALL aleat_unif(moisson,1,graine)
          DO ip1=1,nbpicell-1
            ip2=INT(moisson(ip1)*(nbpicell-ip1+1))+1
            provisoire(:)=dummy(:,ip2)
            dummy(:,ip2)=dummy(:,nbpicell-ip1+1)
            dummy(:,nbpicell-ip1+1)=provisoire(:)
          ENDDO

! -3- Collisions entre les particules

! ----- Cas ou nbpicell est pair
          IF(MOD(nbpicell,2).EQ.0) THEN

            CALL incrementation(col_i,col_log(col_i)/col_rnd(col_i),
     &                          dummy(:,1:nbpicell))

! ----- Cas ou nbpicell est impair
          ELSE

! ------- Collisions entre les trois premieres particules
            CALL incrementation(col_i,0.5*col_log(col_i)/col_rnd(col_i),
     &                          dummy(:,1:2))
            CALL incrementation(col_i,0.5*col_log(col_i)/col_rnd(col_i),
     &                          dummy(:,2:3))
            couple(:,1)=dummy(:,1)
            couple(:,2)=dummy(:,3)
            CALL incrementation(col_i,0.5*col_log(col_i)/col_rnd(col_i),
     &                          couple)
            dummy(:,1)=couple(:,1)
            dummy(:,3)=couple(:,2)
! ------- Collisions entre les autres particules
            IF (nbpicell.GE.5)
     &        CALL incrementation(col_i,col_log(col_i)/col_rnd(col_i),
     &                            dummy(:,4:nbpicell))

          ENDIF

        ENDDO

        partic(:,n0:n0+nbpicell-1,i1)=dummy(:,1:nbpicell)
        DEALLOCATE (moisson)

      ENDDO
!     _______________________________
      END SUBROUTINE intra_collisions



      SUBROUTINE incrementation(col_i,logcou,tableau)
! ======================================================================
! Fonctionne uniquement en 3D dans le cadre de la mecanique classique
! pour l'instant.
! Les vitesses sont toutes adimensionnees par rapport a c.
! ======================================================================
      USE precisions
      USE domaines
      USE particules_klder
      USE collisions
      USE laser
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)       :: col_i
      REAL(KIND=kr), INTENT(IN) :: logcou
      REAL(KIND=kr), DIMENSION(:,:), INTENT(INOUT) :: tableau

      INTEGER                   :: ipaire
      INTEGER(kind=kr)           :: graine
      REAL(KIND=kr)             :: vax,vay,vaz,vbx,vby,vbz,ux,uy,uz,u,
     &                             uorth,phi,delta,theta,
     &                             dux,duy,duz
      REAL(KIND=kr), DIMENSION(1) :: moisson

!      REAL(kind=kr), PARAMETER    :: PI   = 3.14159265359

      INTERFACE
         SUBROUTINE aleat_unif(gerbe,methode,graine)
          USE precisions
          REAL(KIND=kr), DIMENSION(:), INTENT(OUT)   :: gerbe
          INTEGER                    , INTENT(IN)    :: methode
          INTEGER(kind=kr)            , INTENT(INOUT) :: graine
         END SUBROUTINE aleat_unif
         SUBROUTINE aleat_gaus(gerbe,methode,graine)
          USE precisions
          REAL(KIND=kr), DIMENSION(:), INTENT(OUT)   :: gerbe
          INTEGER                    , INTENT(IN)    :: methode
          INTEGER(kind=kr)            , INTENT(INOUT) :: graine
         END SUBROUTINE aleat_gaus
      END INTERFACE

      DO ipaire=1,SIZE(tableau,2)-1,2

! --- Calcul de la vitesse relative
         IF (a_px.NE.0) THEN
            vax=tableau(a_px,ipaire)
            vbx=tableau(a_px,ipaire+1)
         ELSE
            vax=0
            vbx=0
         ENDIF
         IF (a_py.NE.0) THEN
            vay=tableau(a_py,ipaire)
            vby=tableau(a_py,ipaire+1)
         ELSE
            vay=0
            vby=0
         ENDIF
         IF (a_pz.NE.0) THEN
            vaz=tableau(a_pz,ipaire)
            vbz=tableau(a_pz,ipaire+1)
         ELSE
            vaz=0
            vbz=0
         ENDIF
         ux=vax-vbx
         uy=vay-vby
         uz=vaz-vbz
         u=SQRT(ux**2_8+uy**2_8+uz**2_8)
         uorth=SQRT(ux**2_8+uy**2_8)

! --- Tirage des angles
         call aleat_unif(moisson,1,graine)
         phi=PI*moisson(1)
         call aleat_gaus(moisson,1,graine)
         delta=SQRT(col_dlt(col_i)*logcou/u**3_8)*moisson(1)
         theta=2._8*ATAN(delta)

! --- Incrementation des vitesses
         IF (uorth.LE.TINY(uorth)) THEN
            dux=u*SIN(theta)*COS(phi)
            duy=u*SIN(theta)*SIN(phi)
            duz=u*(COS(theta)-1.)
         ELSE   ! u n'est pas parallele a z
           dux=((ux*uz)/uorth)*SIN(theta)*COS(phi)
     &         -((uy*u/uorth))*SIN(theta)*SIN(phi)
     &         +ux*(COS(theta)-1.)
           duy=((uy*uz)/uorth)*SIN(theta)*COS(phi)
     &         +((ux*u/uorth))*SIN(theta)*SIN(phi)
     &         +uy*(COS(theta)-1.)
           duz=-uorth*SIN(theta)*COS(phi)+uz*(COS(theta)-1.)
         ENDIF
         IF (a_px.NE.0) THEN
            tableau(a_px,ipaire)  =vax+0.5_8*dux
            tableau(a_px,ipaire+1)=vbx-0.5_8*dux
         ENDIF
         IF (a_py.NE.0) THEN
            tableau(a_py,ipaire)  =vay+0.5_8*duy
            tableau(a_py,ipaire+1)=vby-0.5_8*duy
         ENDIF
         IF (a_pz.NE.0) THEN
            tableau(a_pz,ipaire)  =vaz+0.5_8*duz
            tableau(a_pz,ipaire+1)=vbz-0.5_8*duz
         ENDIF

      ENDDO
!     _____________________________
      END SUBROUTINE incrementation



      SUBROUTINE inter_collisions(col_i)
! ======================================================================
! Fonctionne uniquement en 3D dans le cadre de la mecanique classique
! pour l'instant.
! ======================================================================
      USE precisions
      USE domaines
      USE particules_klder
      USE collisions
      USE laser
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)    :: col_i

!=====Variables apparaissant dans 1
      INTEGER             :: i1, i2, icell, nbpicell1, nbpicell2,
     &                       n1, n2, itmp
      REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE :: dummy1
      REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE :: dummy2

!=====Variables apparaissant dans 2
      INTEGER                                 :: iround, J, r, q
      INTEGER(kind=kr)                         :: graine
      REAL(KIND=kr),DIMENSION(:),ALLOCATABLE  :: moisson1, moisson2,
     &                                                     provisoire

      INTERFACE
        SUBROUTINE incrementation2(i1,i2,col_i,logcou,tableau1,tableau2)
          USE precisions
          INTEGER, INTENT(IN)                          :: i1,i2,col_i
          REAL(KIND=kr), INTENT(IN)                    :: logcou
          REAL(KIND=kr), DIMENSION(:),   INTENT(INOUT) :: tableau1
          REAL(KIND=kr), DIMENSION(:,:), INTENT(INOUT) :: tableau2
        END SUBROUTINE incrementation2
        SUBROUTINE aleat_unif(gerbe,methode,graine)
          USE precisions
          REAL(KIND=kr), DIMENSION(:), INTENT(OUT)   :: gerbe
          INTEGER                    , INTENT(IN)    :: methode
          INTEGER(kind=kr)            , INTENT(INOUT) :: graine
        END SUBROUTINE aleat_unif
      END INTERFACE

!=======================================================================

      i1 = col_p1(col_i) ; i2 = col_p2(col_i)

      IF(.NOT.ALLOCATED(dummy1))
     &        ALLOCATE (dummy1(SIZE(partic,1),300))
      IF(.NOT.ALLOCATED(dummy2))
     &        ALLOCATE (dummy2(SIZE(partic,1),300))
      IF(.NOT.ALLOCATED(provisoire))
     &        ALLOCATE (provisoire(SIZE(partic,1)))

      DO icell = 1,(nulpx-nulmx)*(nulpy-nulmy)*(nulpz-nulmz)

! -1- Calcul du nombre de particules de chaque espece dans la cellule
!     et creation des copies dummy1 et dummy2

        IF(icell.NE.(nulpx-nulmx)*(nulpy-nulmy)
     &                           *(nulpz-nulmz)) THEN
          nbpicell1=compteur(icell+1,i1)-compteur(icell,i1)
          nbpicell2=compteur(icell+1,i2)-compteur(icell,i2)
        ELSE
          nbpicell1=nbpa(i1)+1-compteur( (nulpx-nulmx)*(nulpy-nulmy)
     &                                  *(nulpz-nulmz),i1)
          nbpicell2=nbpa(i2)+1-compteur( (nulpx-nulmx)*(nulpy-nulmy)
     &                                  *(nulpz-nulmz),i2)
        ENDIF
! ----- Si il n'y a pas de quoi collisionner, on saute la cellule
        IF((nbpicell1.LT.1  ).OR.(nbpicell2.LT.1  )) CYCLE
! ----- Si il n'y a pas assez de place dans les tableaux, on avertit
        IF((nbpicell1.GT.300).OR.(nbpicell2.GT.300))
     &    print*, 'Depassement de taille : nbpicell1=',nbpicell1,
     &                                ' et nbpicell2=',nbpicell2

! ----- On fait en sorte que l'esp. 2 soit la plus nombreuse
        IF (nbpicell1.GT.nbpicell2) THEN
          itmp=i2        ; i2=i1               ; i1=itmp
          itmp=nbpicell2 ; nbpicell2=nbpicell1 ; nbpicell1=itmp
        ENDIF

        ALLOCATE (moisson1(nbpicell1-1))
        ALLOCATE (moisson2(nbpicell2-1))
        n1=compteur(icell,i1)
        n2=compteur(icell,i2)
        dummy1(:,1:nbpicell1)=partic(:,n1:n1+nbpicell1-1,i1)
        dummy2(:,1:nbpicell2)=partic(:,n2:n2+nbpicell2-1,i2)


        DO iround = 1,col_rnd(col_i)

! -2- Permutation aleatoire des particules, especes 1 et 2

          CALL aleat_unif(moisson1,1,graine)
          DO itmp=1,nbpicell1-1
            J=INT(moisson1(itmp)*(nbpicell1-itmp+1))+1
            provisoire(:)=dummy1(:,J)
            dummy1(:,J)=dummy1(:,nbpicell1-itmp+1)
            dummy1(:,nbpicell1-itmp+1)=provisoire(:)
          ENDDO
          CALL aleat_unif(moisson2,1,graine)
          DO itmp=1,nbpicell2-1
            J=INT(moisson2(itmp)*(nbpicell2-itmp+1))+1
            provisoire(:)=dummy2(:,J)
            dummy2(:,J)=dummy2(:,nbpicell2-itmp+1)
            dummy2(:,nbpicell2-itmp+1)=provisoire(:)
          ENDDO

! -3- Collisions entre les particules

          r=MOD(nbpicell2,nbpicell1) ; q=nbpicell2/nbpicell1
          DO itmp=1,r
            CALL incrementation2(i1,i2,col_i,
     &                           col_log(col_i)/col_rnd(col_i),
     &                           dummy1(:,itmp),
     &                           dummy2(:,(itmp-1)*(q+1)+1:itmp*(q+1)))
          ENDDO
          DO itmp=r+1,nbpicell1
            CALL incrementation2(i1,i2,col_i,
     &                           col_log(col_i)/col_rnd(col_i),
     &                           dummy1(:,itmp),
     &                           dummy2(:,(itmp-1)*q+r+1:itmp*q+r))
          ENDDO

        ENDDO

        partic(:,n1:n1+nbpicell1-1,i1)=dummy1(:,1:nbpicell1)
        partic(:,n2:n2+nbpicell2-1,i2)=dummy2(:,1:nbpicell2)
        DEALLOCATE(moisson1) ; DEALLOCATE(moisson2)

      ENDDO
!     _______________________________
      END SUBROUTINE inter_collisions



      SUBROUTINE incrementation2(i1,i2,col_i,logcou,tableau1,tableau2)
! ======================================================================
! Fonctionne uniquement en 3D dans le cadre de la mecanique classique
! pour l'instant.
! Les vitesses sont toutes adimensionnees par rapport a c.
! ======================================================================
      USE precisions
      USE domaines
      USE particules_klder
      USE collisions
      USE laser
      USE erreurs

      IMPLICIT NONE

      INTEGER,                       INTENT(IN)    :: i1,i2,col_i
      REAL(KIND=kr),                 INTENT(IN)    :: logcou
      REAL(KIND=kr), DIMENSION(:),   INTENT(INOUT) :: tableau1
      REAL(KIND=kr), DIMENSION(:,:), INTENT(INOUT) :: tableau2

      INTEGER                  :: ipaire
      INTEGER(kind=kr)          :: graine
      REAL (KIND=kr)           :: vax,vay,vaz,vbx,vby,vbz,ux,uy,uz,u,
     &                            uorth,m12sm1,m12sm2,phi,
     &                            delta,theta,dux,duy,duz
      REAL(KIND=kr), DIMENSION(1) :: moisson

!      REAL(kind=kr), PARAMETER    :: PI   = 3.14159265359

      INTERFACE
         SUBROUTINE aleat_unif(gerbe,methode,graine)
          USE precisions
          REAL(KIND=kr), DIMENSION(:), INTENT(OUT)  :: gerbe
          INTEGER                   , INTENT(IN)    :: methode
          INTEGER(kind=kr)           , INTENT(INOUT) :: graine
         END SUBROUTINE aleat_unif
         SUBROUTINE aleat_gaus(gerbe,methode,graine)
          USE precisions
          REAL(KIND=kr), DIMENSION(:), INTENT(OUT)  :: gerbe
          INTEGER                   , INTENT(IN)    :: methode
          INTEGER(kind=kr)           , INTENT(INOUT) :: graine
         END SUBROUTINE aleat_gaus
      END INTERFACE

      DO ipaire=1,SIZE(tableau2,2)
! --- Calcul de la vitesse relative
        IF(a_px.NE.0) THEN
          vax=tableau1(a_px)
          vbx=tableau2(a_px,ipaire)
        ELSE
          vax=0.
          vbx=0.
        ENDIF
        IF(a_py.NE.0) THEN
          vay=tableau1(a_py)
          vby=tableau2(a_py,ipaire)
        ELSE
          vay=0.
          vby=0.
        ENDIF
         IF(a_pz.NE.0) THEN
          vaz=tableau1(a_pz)
          vbz=tableau2(a_pz,ipaire)
        ELSE
          vaz=0.
          vbz=0.
        ENDIF
        ux=vax-vbx
        uy=vay-vby
        uz=vaz-vbz
        u=SQRT(ux**2_8+uy**2_8+uz**2_8)
        uorth=SQRT(ux**2_8+uy**2_8)

! --- Tirage des angles
        CALL aleat_unif(moisson,1,graine)
        phi=PI*moisson(1)
        CALL aleat_gaus(moisson,1,graine)
        delta=SQRT(col_dlt(col_i)*logcou/u**3_8)*moisson(1)
        theta=2._8*ATAN(delta)

! --- Incrementation des vitesses
        IF(uorth.LE.TINY(uorth)) THEN
          dux=u*SIN(theta)*COS(phi)
          duy=u*SIN(theta)*SIN(phi)
          duz=u*(COS(theta)-1.)
        ELSE   ! u n'est pas parallele a z
          dux=  (ux*uz/uorth)*SIN(theta)*COS(phi)
     &         -(uy*u /uorth)*SIN(theta)*SIN(phi)
     &         + ux*(COS(theta)-1.)
          duy=  (uy*uz/uorth)*SIN(theta)*COS(phi)
     &         +(ux*u /uorth)*SIN(theta)*SIN(phi)
     &         + uy*(COS(theta)-1.)
          duz= -uorth*SIN(theta)*COS(phi)+uz*(COS(theta)-1.)
        ENDIF
! ----- Note : on ne peut pas utiliser col_p[12](col_i) ci-dessous
!       car les especes ont peut-etre ete permutees...
        m12sm1 =  masse(i2) / (masse(i1)+masse(i2))
        m12sm2 =  masse(i1) / (masse(i1)+masse(i2))
        IF(a_px.NE.0) THEN
          tableau1(a_px)       =vax+m12sm1*dux
          tableau2(a_px,ipaire)=vbx-m12sm2*dux
        ENDIF
        IF(a_py.NE.0) THEN
          tableau1(a_py)       =vay+m12sm1*duy
          tableau2(a_py,ipaire)=vby-m12sm2*duy
        ENDIF
        IF(a_pz.NE.0) THEN
          tableau1(a_pz)       =vaz+m12sm1*duz
          tableau2(a_pz,ipaire)=vbz-m12sm2*duz
        ENDIF

      ENDDO

!     ______________________________
      END SUBROUTINE incrementation2
