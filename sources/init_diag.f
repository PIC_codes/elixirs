! ======================================================================
! init_diag.f
! -----------
!
!     SUBROUTINE init_diag
!
! ======================================================================

      SUBROUTINE init_diag
! ======================================================================
! Encore des initialisations, et
! Ouverture sur le PE 1 des fichiers de diagnostics
! ======================================================================
      USE particules_klder
      USE diagnostics
      USE diagnostics_klder
      USE domaines
      USE particules
      USE diag_ptest

      USE erreurs

      IMPLICIT NONE

      INTEGER              :: numero_fi
      CHARACTER(LEN= 9)    :: nom_fi, mask_nom
      CHARACTER(LEN=11)    :: format_fi
      INTEGER              :: dsp_i, dph_i, lgc, i


! --- mode d'ecriture des resultats
      IF((diag_typ.EQ.'bin').OR.(diag_typ.EQ.'txt')) THEN
        IF(diag_typ.EQ.'bin') THEN
          format_fi = 'unformatted'
        ELSE
          format_fi =   'formatted'
        ENDIF
        numero_fi = 20

! Ouverture des fichiers pour les diagnostics temporels
! ----------------------------------------------------------------------
!        WRITE(fdbg,*) 'init_diag : nesp = ', nesp

	IDchmpex_x = 64
	IDchi  = 65 
	IDzeta = 66

	IDchirel  = 67 
	IDzetarel = 68	
	
! ----- Ces identifiants de fichier ne sont utiles que pour les tests 
! ----- � 2 particules (syst�me �lectron-ion)	
	IDxe_t  = 70 
	IDye_t  = 71
	IDvxe_t = 72
	IDvye_t = 73
	IDvze_t = 74

	IDxi_t  = 75 
	IDyi_t  = 76
	IDvxi_t = 77
	IDvyi_t = 78
	IDvzi_t = 79

	IDgae_t = 80
	IDgai_t = 81
	
	IDxrel = 82
	IDyrel = 83
	IDzrel = 84
	IDtrel = 85	

	IF(nbmy.LE.10) THEN
	  f_chmpex_x = 'chmpex_x' 
          OPEN(UNIT=IDchmpex_x, FILE=f_chmpex_x ) 
	ENDIF
	
! ------  fichiers de diag tenseurs chi et zeta
        IF(debug.GE.4) THEN
          f_chi = 'chi.t' 
          OPEN(UNIT=IDchi, FILE=f_chi ) 
          f_zeta = 'zeta.t' 
          OPEN(UNIT=IDzeta, FILE=f_zeta ) 

          f_chirel = 'chirel.t' 
          OPEN(UNIT=IDchirel, FILE=f_chirel ) 
          f_zetarel = 'zetrel.t' 
          OPEN(UNIT=IDzetarel, FILE=f_zetarel ) 
        ENDIF
	
	iref=1
	IF(nbpa(iref).EQ.1) THEN	
          f_xet = 'xe.t' 
          f_yet = 'ye.t' 
          f_vxet= 'vxe.t'
          f_vyet= 'vye.t'
          f_vzet= 'vze.t'
          f_xit = 'xi.t'
          f_yit = 'yi.t'
          f_vxit= 'vxi.t'
          f_vyit= 'vyi.t'
          f_vzit= 'vzi.t'  

          f_gait = 'gai.t'
	  f_gaet = 'gae.t'

          f_xrel = 'xrel.t'
          f_yrel = 'yrel.t'
          f_zrel = 'zrel.t'
          f_trel = 'trel.t'

          OPEN(UNIT=IDxe_t, FILE=f_xet ) 
          OPEN(UNIT=IDye_t, FILE=f_yet ) 
          OPEN(UNIT=IDvxe_t, FILE=f_vxet ) 
          OPEN(UNIT=IDvye_t, FILE=f_vyet ) 
          OPEN(UNIT=IDvze_t, FILE=f_vzet ) 
          OPEN(UNIT=IDxi_t, FILE=f_xit ) 
          OPEN(UNIT=IDyi_t, FILE=f_yit ) 
          OPEN(UNIT=IDvxi_t, FILE=f_vxit ) 
          OPEN(UNIT=IDvyi_t, FILE=f_vyit ) 
          OPEN(UNIT=IDvzi_t, FILE=f_vzit ) 
	  
          OPEN(UNIT=IDgae_t, FILE=f_gaet ) 
          OPEN(UNIT=IDgai_t, FILE=f_gait ) 

          OPEN(UNIT=IDxrel, FILE=f_xrel ) 
          OPEN(UNIT=IDyrel, FILE=f_yrel ) 
!          OPEN(UNIT=IDzrel, FILE=f_zrel ) 
          OPEN(UNIT=IDtrel, FILE=f_trel )  
	ENDIF
	
	IF(nesp.GT.0) THEN
! --- fichier de poids
          numero_fi = numero_fi + 1
          nom_fi    = 'poids.t'
          d1d_poid(1) = numero_fi
          d1d_poid(2) = d_po(   1)
          d1d_poid(3) = d_po(nesp)
          IF(numproc.EQ.1) THEN
!	    WRITE(fdbg,*) 'numero_fi, nom_fi =', numero_fi, nom_fi
            OPEN(UNIT=numero_fi, FILE=nom_fi, FORM=format_fi)
            IF(debug.GE.1)
     &      WRITE(fdbg,"('Ouverture du fichier ',I2,' pour les ',
     &                   'diagnostics temporels (poids)')") numero_fi
          ENDIF

! --- fichier d'energies cinetiques
          numero_fi = numero_fi + 1
          nom_fi    = 'e_cin.t'
          d1d_ecin(1) = numero_fi
          d1d_ecin(2) = d_ki(   1)
          d1d_ecin(3) = d_ki(nesp)
          IF(numproc.EQ.1) THEN
            OPEN(UNIT=numero_fi, FILE=nom_fi, FORM=format_fi)
            IF(debug.GE.1)
     &      WRITE(fdbg,"('Ouverture du fichier ',I2,' pour les ',
     &                   'diagnostics temporels (e_cin)')") numero_fi
          ENDIF

	  
! --- fichier d'impulsions (LG, 03/06)
          d1d_impx(:) = 0
          IF(a_px.NE.0) THEN
            WRITE(fdbg,"('Avant ouverture du fichier pour les ',
     &                   'diagnostics temporels (impx)')")	  
            numero_fi = numero_fi + 1
            nom_fi    = 'impx.t'
            d1d_impx(1) = numero_fi
            d1d_impx(2) = d_px(   1)
            d1d_impx(3) = d_px(nesp)
            IF(numproc.EQ.1) THEN
              OPEN(UNIT=numero_fi, FILE=nom_fi, FORM=format_fi)
              IF(debug.GE.1)
     &        WRITE(fdbg,"('Ouverture du fichier ',I2,' pour les ',
     &                   'diagnostics temporels (impx)')") numero_fi
            ENDIF
	  ENDIF
	  
	  d1d_impy(:) = 0
	  IF(a_py.NE.0) THEN
            numero_fi = numero_fi + 1
            nom_fi    = 'impy.t'
            d1d_impy(1) = numero_fi
            d1d_impy(2) = d_py(   1)
            d1d_impy(3) = d_py(nesp)
            IF(numproc.EQ.1) THEN
              OPEN(UNIT=numero_fi, FILE=nom_fi, FORM=format_fi)
              IF(debug.GE.1)
     &        WRITE(fdbg,"('Ouverture du fichier ',I2,' pour les ',
     &                   'diagnostics temporels (impy)')") numero_fi
            ENDIF
	  ENDIF
	  
	  d1d_impz(:) = 0
          IF(a_pz.NE.0) THEN
            numero_fi = numero_fi + 1
            nom_fi    = 'impz.t'
            d1d_impz(1) = numero_fi
            d1d_impz(2) = d_pz(   1)
            d1d_impz(3) = d_pz(nesp)
            IF(numproc.EQ.1) THEN
              OPEN(UNIT=numero_fi, FILE=nom_fi, FORM=format_fi)
              IF(debug.GE.1)
     &        WRITE(fdbg,"('Ouverture du fichier ',I2,' pour les ',
     &                   'diagnostics temporels (impz)')") numero_fi
            ENDIF
	  ENDIF

! --- fichier de flux d'impulsions (LG, 01/06)
          d1d_fimpx(:) = 0
          IF(a_px.NE.0) THEN
            numero_fi = numero_fi + 1
            nom_fi    = 'f_impx.t'
            d1d_fimpx(1) = numero_fi
            d1d_fimpx(2) = d_fpx(   1)
            d1d_fimpx(3) = d_fpx(nesp)
            IF(numproc.EQ.1) THEN
              OPEN(UNIT=numero_fi, FILE=nom_fi, FORM=format_fi)
              IF(debug.GE.1)
     &        WRITE(fdbg,"('Ouverture du fichier ',I2,' pour les ',
     &                   'diagnostics temporels (f_impx)')") numero_fi
            ENDIF
	  ENDIF
	  
	  d1d_fimpy(:) = 0
	  IF(a_py.NE.0) THEN
            numero_fi = numero_fi + 1
            nom_fi    = 'f_impy.t'
            d1d_fimpy(1) = numero_fi
            d1d_fimpy(2) = d_fpy(   1)
            d1d_fimpy(3) = d_fpy(nesp)
            IF(numproc.EQ.1) THEN
              OPEN(UNIT=numero_fi, FILE=nom_fi, FORM=format_fi)
              IF(debug.GE.1)
     &        WRITE(fdbg,"('Ouverture du fichier ',I2,' pour les ',
     &                   'diagnostics temporels (f_impy)')") numero_fi
            ENDIF
	  ENDIF
	  
	  d1d_fimpz(:) = 0
          IF(a_pz.NE.0) THEN
            numero_fi = numero_fi + 1
            nom_fi    = 'f_impz.t'
            d1d_fimpz(1) = numero_fi
            d1d_fimpz(2) = d_fpz(   1)
            d1d_fimpz(3) = d_fpz(nesp)
            IF(numproc.EQ.1) THEN
              OPEN(UNIT=numero_fi, FILE=nom_fi, FORM=format_fi)
              IF(debug.GE.1)
     &        WRITE(fdbg,"('Ouverture du fichier ',I2,' pour les ',
     &                   'diagnostics temporels (f_impz)')") numero_fi
            ENDIF
	  ENDIF
	  	  
! --- fichier de flux de poids (perdu par absorption)
          IF(d_pof(1).NE.0) THEN
            numero_fi = numero_fi + 1
            nom_fi    = 'poidsf.t'
            d1d_poidf(1) = numero_fi
            d1d_poidf(2) = d_pof(   1)
            d1d_poidf(3) = d_pof(nesp)
            IF(numproc.EQ.1) THEN
              OPEN(UNIT=numero_fi, FILE=nom_fi, FORM=format_fi)
              IF(debug.GE.1)
     &        WRITE(fdbg,"('Ouverture du fichier ',I2,' pour les ',
     &                     'diagnostics temporels (poidsf)')") numero_fi
            ENDIF
          ENDIF

! --- fichier de flux d'energies cinetiques (perdu par absorption)
          IF(d_kif(1).NE.0) THEN
            numero_fi = numero_fi + 1
            nom_fi    = 'e_cinf.t'
            d1d_ecinf(1) = numero_fi
            d1d_ecinf(2) = d_kif(   1)
            d1d_ecinf(3) = d_kif(nesp)
            IF(numproc.EQ.1) THEN
              OPEN(UNIT=numero_fi, FILE=nom_fi, FORM=format_fi)
              IF(debug.GE.1)
     &        WRITE(fdbg,"('Ouverture du fichier ',I2,' pour les ',
     &                     'diagnostics temporels (e_cinf)')") numero_fi
           ENDIF
         ENDIF

! --- fichier de flux d'energies cinetiques (perdu par reinjection)
          IF(d_kir(1).NE.0) THEN
            numero_fi = numero_fi + 1
            nom_fi    = 'e_cinr.t'
            d1d_ecinr(1) = numero_fi
            d1d_ecinr(2) = d_kir(   1)
            d1d_ecinr(3) = d_kir(nesp)
            IF(numproc.EQ.1) THEN
              OPEN(UNIT=numero_fi, FILE=nom_fi, FORM=format_fi)
              IF(debug.GE.1)
     &        WRITE(fdbg,"('Ouverture du fichier ',I2,' pour les ',
     &                     'diagnostics temporels (e_cinr)')") numero_fi
           ENDIF
         ENDIF
        ENDIF

! --- fichier d'energies electromagnetiques
        IF((d_ex+d_ey+d_ez+d_bx+d_by+d_bz).GT.0) THEN
          numero_fi = numero_fi + 1
          nom_fi    = 'e_emg.t'
          d1d_eemg(1) = numero_fi
          IF(d_bz.NE.0) d1d_eemg(2) = d_bz
          IF(d_by.NE.0) d1d_eemg(2) = d_by
          IF(d_bx.NE.0) d1d_eemg(2) = d_bx
          IF(d_ez.NE.0) d1d_eemg(2) = d_ez
          IF(d_ey.NE.0) d1d_eemg(2) = d_ey
          IF(d_ex.NE.0) d1d_eemg(2) = d_ex
          d1d_eemg(3) = MAX(d_ex,d_ey,d_ez,d_bx,d_by,d_bz)
          IF(numproc.EQ.1) THEN
            OPEN(UNIT=numero_fi, FILE=nom_fi, FORM=format_fi)
            IF(debug.GE.1)
     &      WRITE(fdbg,"('Ouverture du fichier ',I2,' pour les ',
     &                   'diagnostics temporels (e_emg)')") numero_fi
          ENDIF
        ENDIF

! --- fichier d'energies electromagnetiques perdues par glissement
        IF((d_exg+d_eyg+d_ezg+d_bxg+d_byg+d_bzg).GT.0) THEN
          numero_fi = numero_fi + 1
          nom_fi    = 'e_emgg.t'
          d1d_eemgg(1) = numero_fi
          IF(d_bzg.NE.0) d1d_eemgg(2) = d_bzg
          IF(d_byg.NE.0) d1d_eemgg(2) = d_byg
          IF(d_bxg.NE.0) d1d_eemgg(2) = d_bxg
          IF(d_ezg.NE.0) d1d_eemgg(2) = d_ezg
          IF(d_eyg.NE.0) d1d_eemgg(2) = d_eyg
          IF(d_exg.NE.0) d1d_eemgg(2) = d_exg
          d1d_eemgg(3) = MAX(d_exg,d_eyg,d_ezg,d_bxg,d_byg,d_bzg)
          IF(numproc.EQ.1) THEN
            OPEN(UNIT=numero_fi, FILE=nom_fi, FORM=format_fi)
            IF(debug.GE.1)
     &      WRITE(fdbg,"('Ouverture du fichier ',I2,' pour les ',
     &                   'diagnostics temporels (e_emgg)')") numero_fi
          ENDIF
        ENDIF

! --- fichier de champs en 1 point
        IF((d_clo).GT.0) THEN
          numero_fi = numero_fi + 1
          nom_fi    = 'c_loc.t'
          d1d_clo(1) = numero_fi
          d1d_clo(2) = d_clo
          d1d_clo(3) = d_clo+clo_nb-1
          IF(numproc.EQ.1) THEN
            OPEN(UNIT=numero_fi, FILE=nom_fi, FORM=format_fi)
            WRITE(numero_fi,*) clo_nb
            DO i=1,clo_nb
            WRITE(numero_fi,"(A2,5(1X,1PE10.3))") clo_ch(i),clo_pos(:,i)
            ENDDO
            IF(debug.GE.1)
     &      WRITE(fdbg,"('Ouverture du fichier ',I2,' pour les ',
     &                   'diagnostics temporels (c_loc)')") numero_fi
          ENDIF
        ENDIF

! --- fichier des flux du vecteur de Poynting
          numero_fi = numero_fi + 1
          nom_fi    = 'e_ptg.t'
          d1d_eptg(1) = numero_fi
          d1d_eptg(2) = d_psr
          d1d_eptg(3) = d_psr
          IF(d_pmx.NE.0) d1d_eptg(3) = d_ppx
          IF(d_pmy.NE.0) d1d_eptg(3) = d_ppy
          IF(d_pmz.NE.0) d1d_eptg(3) = d_ppz
          IF(numproc.EQ.1) THEN
            OPEN(UNIT=numero_fi, FILE=nom_fi, FORM=format_fi)
            IF(debug.GE.1)
     &      WRITE(fdbg,"('Ouverture du fichier ',I2,' pour les ',
     &                   'diagnostics temporels (e_ptg)')") numero_fi
          ENDIF

! --- fichier des ecarts a Poisson et Gauss
          numero_fi = numero_fi + 1
          nom_fi    = 'poiss.t'
          d1d_pois(1) = numero_fi
          d1d_pois(2) = d_psn
          IF(d_gau.NE.0) d1d_pois(3) = d_gau
          IF(numproc.EQ.1) THEN
            OPEN(UNIT=numero_fi, FILE=nom_fi, FORM=format_fi)
            IF(debug.GE.1)
     &      WRITE(fdbg,"('Ouverture du fichier ',I2,' pour les ',
     &                   'diagnostics temporels (poiss)')") numero_fi
          ENDIF

! --- fichier de champs max
        IF((d_cmx).GT.0) THEN
          numero_fi = numero_fi + 1
          nom_fi    = 'c_max.t'
          d1d_cmx(1) = numero_fi
          d1d_cmx(2) = d_cmx
          d1d_cmx(3) = d_cmx+cmx_nb-1
          IF(numproc.EQ.1) THEN
            OPEN(UNIT=numero_fi, FILE=nom_fi, FORM=format_fi)
            WRITE(numero_fi,*) cmx_nb
            DO i=1,cmx_nb
            WRITE(numero_fi,"(A2)") cmx_ch(i)
            ENDDO
            IF(debug.GE.1)
     &      WRITE(fdbg,"('Ouverture du fichier ',I2,' pour les ',
     &                   'diagnostics temporels (c_max)')") numero_fi
          ENDIF
        ENDIF


      IF(numproc.EQ.1) THEN

! Ouverture des fichiers pour les cartes de champs
! ----------------------------------------------------------------------
      mask_nom = 'chmp_zz'
      dsp_nbmoy = 0

      DO dsp_i=1,dsp_nb
        IF(dsp(dsp_i)%typ.EQ.0) CYCLE
        IF(dsp(dsp_i)%moy.NE.0) dsp_nbmoy = dsp_i
        nom_fi = mask_nom(1:5) // dsp_typ_map(dsp(dsp_i)%typ)
        OPEN(UNIT=dsp_typ_fic(dsp(dsp_i)%typ),
     &      FILE=nom_fi, FORM=format_fi)
        IF(debug.GE.1)
     &    WRITE(fdbg,"('Utilisation du fichier ',I2,' pour le diag.',
     &    'spatial ',I2,' du champ ',A2)") dsp_typ_fic(dsp(dsp_i)%typ),
     &    dsp_i, dsp_typ_map(dsp(dsp_i)%typ)
      ENDDO

      numero_fi = MAXVAL(dsp_typ_fic(:))

! Ouverture des fichiers pour les projections de l'espace des phases
! ----------------------------------------------------------------------
      mask_nom = 'faz_3d_00'

      DO dph_i=1,dph_nb
        IF(.NOT.dph(dph_i)%ok) CYCLE
        numero_fi = numero_fi + 1
        WRITE(nom_fi,'(i9)') dph_i
        lgc = LEN_TRIM(ADJUSTL(nom_fi))
        nom_fi = mask_nom(1:9-lgc) // nom_fi(10-lgc:9)
        IF(dph(dph_i)%axe(3).EQ.'00') THEN
          nom_fi(5:5)='2'
          IF(dph(dph_i)%axe(2).EQ.'00') nom_fi(5:5)='1'
        ENDIF
        OPEN(UNIT=numero_fi, FILE=nom_fi, FORM=format_fi)
        dph(dph_i)%fic = numero_fi
        IF(debug.GE.1)
     &    WRITE(fdbg,"('Ouverture du fichier ',I2,' pour la projection',
     &          1X,A,'d de l''espace des phases ',I2)")
     &          numero_fi,nom_fi(5:5),dph_i
      ENDDO


! Ouverture du fichier pour les particules-test
! ----------------------------------------------------------------------
      nom_fi = 'ptest.t'
      IF(dpt_ok.GT.0) THEN
        numero_fi = numero_fi + 1
        OPEN(UNIT=numero_fi, FILE=nom_fi, FORM=format_fi)
        dpt_fic = numero_fi
        IF(debug.GE.1)
     &    WRITE(fdbg,"('Ouverture du fichier ',I2,' pour les',
     &                 ' particules-test (ptest)')")
     &          numero_fi
      ENDIF

      IF(debug.GE.1) WRITE(fdbg,"(' ')")

      ENDIF   ! --- mode d'ecriture
      ENDIF   ! --- numproc = 1
      RETURN
!     ________________________
      END SUBROUTINE init_diag
