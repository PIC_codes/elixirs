! ======================================================================
! diag_sp.f
! ---------
!
!     SUBROUTINE diag_sp
!     SUBROUTINE diag_spatial
!     SUBROUTINE indices_sstab
!
! ======================================================================

      SUBROUTINE diag_sp
! ======================================================================
! Diagnostics spatiaux effectues periodiquement :
!    + Cartes des champs et sources (1, 2, 3D)
! ======================================================================
      USE temps
      USE diagnostics_klder
      USE diag_moyen
      USE domaines
      USE erreurs

      IMPLICIT NONE

      INTEGER                   :: dsp_i, it_mem, smx, smy, smz
      LOGICAL                   :: ecrit_ok, free_mem


      free_mem = .TRUE.
! Diagnostics spatiaux : cartes 1D / 2D / 3D des champs
! ----------------------------------------------------------------------
      DO dsp_i=1,dsp_nb

        IF (dsp(dsp_i)%typ.EQ.0)  CYCLE

        IF(       (         dsp(dsp_i)%moy .EQ.0)
     &      .AND. (MOD(iter,dsp(dsp_i)%per).EQ.0) ) THEN
          CALL diag_spatial(dsp_i, 0, .TRUE.)
        ENDIF

        IF(       (                dsp(dsp_i)%moy .NE.     0)
     &      .AND. (MOD(iter+dsp_moy,dsp(dsp_i)%per).LE.dsp_moy) ) THEN
          it_mem = MOD(iter+dsp_moy,dsp(dsp_i)%per) + 1
          ecrit_ok = it_mem .EQ. (dsp_moy+1)

          IF((numproc.EQ.1).AND.(.NOT.ALLOCATED(dsp_mem))) THEN
            smx = MAXVAL(dsp(1:dsp_nbmoy)%siz(1),
     &                   MASK=(dsp(1:dsp_nbmoy)%typ.NE.0))
            smy = MAXVAL(dsp(1:dsp_nbmoy)%siz(2),
     &                   MASK=(dsp(1:dsp_nbmoy)%typ.NE.0))
            smz = MAXVAL(dsp(1:dsp_nbmoy)%siz(3),
     &                   MASK=(dsp(1:dsp_nbmoy)%typ.NE.0))
            ALLOCATE( dsp_mem(smx,smy,smz,dsp_nbmoy) )
            dsp_mem(:,:,:,:) = 0.
          ENDIF
          CALL diag_spatial(dsp_i, it_mem, ecrit_ok)
          IF(.NOT.ecrit_ok) free_mem = .FALSE.
        ENDIF

      ENDDO

! Peut-on desallouer le tableau pour les diags moyens ?
! ----------------------------------------------------------------------
      IF( ALLOCATED(dsp_mem) .AND. free_mem ) THEN
        DEALLOCATE(dsp_mem)
        print*, 'Libere le tableau dsp_mem (',dsp_moy*pasdt,')'
      ENDIF

      RETURN
!     ______________________
      END SUBROUTINE diag_sp




      SUBROUTINE diag_spatial(dsp_i,it_mem,ecrit_ok)
! ======================================================================

      USE domaines
      USE domaines_klder
      USE temps
      USE champs
      USE diagnostics_klder
      USE diag_moyen

      USE erreurs

      IMPLICIT NONE
!!      INCLUDE 'mpif.h'

      INTEGER, INTENT(IN)   :: dsp_i, it_mem
      LOGICAL, INTENT(IN)   :: ecrit_ok

      REAL(KIND=kr), DIMENSION(:,:,:), ALLOCATABLE  :: dsp_tab, dsp_big
      INTEGER              :: deb_x, deb_y, deb_z, fin_x, fin_y, fin_z, 
     &                        siz_x, siz_y, siz_z, str_x, str_y, str_z
      INTEGER              :: nb_glis, aux_glis 
!      INTEGER              :: typ_xy, dsp_typ, aux
      INTEGER              :: iproc, ific

      INTERFACE
        SUBROUTINE ecrit_tab3d(num_fi, taillx, tailly, taillz, tableau)
          USE precisions
          INTEGER, INTENT(IN) :: num_fi, taillx, tailly, taillz
          REAL(KIND=kr), DIMENSION(taillx,tailly,taillz), INTENT(IN) 
     &                        :: tableau
        END SUBROUTINE ecrit_tab3d
      END INTERFACE


! --- allocation du tableau total par PE 1
      IF(numproc.EQ.1) THEN
        ALLOCATE( dsp_big( dsp(dsp_i)%siz(1),
     &                     dsp(dsp_i)%siz(2),dsp(dsp_i)%siz(3) ) )
        dsp_big(:,:,:) = 0.
      ENDIF

! --- recherche de la contribution locale du PE
      nb_glis = 0
      IF((gliss_ok.EQ.1).AND.(dsp(dsp_i)%moy.NE.0)) THEN
        IF(it_mem.EQ.1) dsp(dsp_i)%xmin_ref = xmin_gl
        nb_glis = INT((xmin_gl-dsp(dsp_i)%xmin_ref)*pasdxi+0.9)
      ENDIF
      CALL indices_sstab(nulmx, nulpx, dsp(dsp_i)%imn(1),
     &                   dsp(dsp_i)%imx(1), dsp(dsp_i)%siz(1),
     &                   deb_x, fin_x, aux_glis, str_x )
      CALL indices_sstab(nulmx, nulpx, dsp(dsp_i)%imn(1)-nb_glis,
     &                   dsp(dsp_i)%imx(1)-nb_glis, dsp(dsp_i)%siz(1),
     &                   deb_x, fin_x, siz_x, str_x )
      aux_glis = aux_glis - siz_x
      CALL indices_sstab(nulmy, nulpy, dsp(dsp_i)%imn(2),
     &                   dsp(dsp_i)%imx(2), dsp(dsp_i)%siz(2),
     &                   deb_y, fin_y, siz_y, str_y )
      CALL indices_sstab(nulmz, nulpz, dsp(dsp_i)%imn(3),
     &                   dsp(dsp_i)%imx(3), dsp(dsp_i)%siz(3),
     &                   deb_z, fin_z, siz_z, str_z )

        WRITE(fdbg,"('appel du diagnostic ',   I3 )") dsp_i
        WRITE(fdbg,"('on a glisse ',I4,' lignes de tableau et perdu ', 
     &               I4,' lignes de diag')") nb_glis,aux_glis
        WRITE(fdbg,"('intervalle sur x :',3(1X,I5))") deb_x,fin_x,siz_x
        WRITE(fdbg,"('intervalle sur y :',3(1X,I5))") deb_y,fin_y,siz_y
        WRITE(fdbg,"('intervalle sur z :',3(1X,I5))") deb_z,fin_z,siz_z

! --- Si la contribution du PE local au diagnostic n'est pas vide
!     on alloue et on remplit le tableau dsp_tab qui va la contenir
      IF(siz_x*siz_y*siz_z.GT.0) THEN
        ALLOCATE( dsp_tab(siz_x,siz_y,siz_z) )

        SELECT CASE(dsp(dsp_i)%typ)
        CASE(1)
          IF(dim_cas.EQ.6) dsp_tab(:,:,:) =
     &     ex_3(deb_x:fin_x:str_x, deb_y:fin_y:str_y, deb_z:fin_z:str_z)
          IF(dim_cas.EQ.5) dsp_tab(:,:,1) =
     &     ex_2(deb_x:fin_x:str_x, deb_y:fin_y:str_y)
          IF(dim_cas.EQ.4) dsp_tab(:,:,1) =
     &     ex_2(deb_x:fin_x:str_x, deb_y:fin_y:str_y)
          IF(dim_cas.EQ.3) dsp_tab(:,1,1) =
     &     ex_1(deb_x:fin_x:str_x)
          IF(dim_cas.EQ.2) dsp_tab(:,1,1) =
     &     ex_1(deb_x:fin_x:str_x)
          IF(dim_cas.EQ.1) dsp_tab(:,1,1) =
     &     ex_1(deb_x:fin_x:str_x)

        CASE(2)
          IF(dim_cas.EQ.6) dsp_tab(:,:,:) =
     &     ey_3(deb_x:fin_x:str_x, deb_y:fin_y:str_y, deb_z:fin_z:str_z)
          IF(dim_cas.EQ.5) dsp_tab(:,:,1) =
     &     ey_2(deb_x:fin_x:str_x, deb_y:fin_y:str_y)
          IF(dim_cas.EQ.4) dsp_tab(:,:,1) =
     &     ey_2(deb_x:fin_x:str_x, deb_y:fin_y:str_y)
          IF(dim_cas.EQ.3) dsp_tab(:,1,1) =
     &     ey_1(deb_x:fin_x:str_x)
          IF(dim_cas.EQ.2) dsp_tab(:,1,1) =
     &     ey_1(deb_x:fin_x:str_x)

        CASE(3)
          IF(dim_cas.EQ.6) dsp_tab(:,:,:) =
     &     ez_3(deb_x:fin_x:str_x, deb_y:fin_y:str_y, deb_z:fin_z:str_z)
          IF(dim_cas.EQ.5) dsp_tab(:,:,1) =
     &     ez_2(deb_x:fin_x:str_x, deb_y:fin_y:str_y)
          IF(dim_cas.EQ.3) dsp_tab(:,1,1) =
     &     ez_1(deb_x:fin_x:str_x)

        CASE(4)
          IF(dim_cas.EQ.6) dsp_tab(:,:,:) =
     &     bx_3(deb_x:fin_x:str_x, deb_y:fin_y:str_y, deb_z:fin_z:str_z)
          IF(dim_cas.EQ.5) dsp_tab(:,:,1) =
     &     bx_2(deb_x:fin_x:str_x, deb_y:fin_y:str_y)
          IF(dim_cas.EQ.3) dsp_tab(:,1,1) =
     &     bx_1(deb_x:fin_x:str_x)

        CASE(5)
          IF(dim_cas.EQ.6) dsp_tab(:,:,:) =
     &     by_3(deb_x:fin_x:str_x, deb_y:fin_y:str_y, deb_z:fin_z:str_z)
          IF(dim_cas.EQ.5) dsp_tab(:,:,1) =
     &     by_2(deb_x:fin_x:str_x, deb_y:fin_y:str_y)
          IF(dim_cas.EQ.3) dsp_tab(:,1,1) =
     &     by_1(deb_x:fin_x:str_x)

        CASE(6)
          IF(dim_cas.EQ.6) dsp_tab(:,:,:) =
     &     bz_3(deb_x:fin_x:str_x, deb_y:fin_y:str_y, deb_z:fin_z:str_z)
          IF(dim_cas.EQ.5) dsp_tab(:,:,1) =
     &     bz_2(deb_x:fin_x:str_x, deb_y:fin_y:str_y)
          IF(dim_cas.EQ.4) dsp_tab(:,:,1) =
     &     bz_2(deb_x:fin_x:str_x, deb_y:fin_y:str_y)
          IF(dim_cas.EQ.3) dsp_tab(:,1,1) =
     &     bz_1(deb_x:fin_x:str_x)
          IF(dim_cas.EQ.2) dsp_tab(:,1,1) =
     &     bz_1(deb_x:fin_x:str_x)

        CASE(7)
          IF(dim_cas.EQ.6) dsp_tab(:,:,:) =
     &     cx_3(deb_x:fin_x:str_x, deb_y:fin_y:str_y, deb_z:fin_z:str_z)
          IF(dim_cas.EQ.5) dsp_tab(:,:,1) =
     &     cx_2(deb_x:fin_x:str_x, deb_y:fin_y:str_y)
          IF(dim_cas.EQ.4) dsp_tab(:,:,1) =
     &     cx_2(deb_x:fin_x:str_x, deb_y:fin_y:str_y)
          IF(dim_cas.EQ.3) dsp_tab(:,1,1) =
     &     cx_1(deb_x:fin_x:str_x)
          IF(dim_cas.EQ.2) dsp_tab(:,1,1) =
     &     cx_1(deb_x:fin_x:str_x)

        CASE(8)
          IF(dim_cas.EQ.6) dsp_tab(:,:,:) =
     &     cy_3(deb_x:fin_x:str_x, deb_y:fin_y:str_y, deb_z:fin_z:str_z)
          IF(dim_cas.EQ.5) dsp_tab(:,:,1) =
     &     cy_2(deb_x:fin_x:str_x, deb_y:fin_y:str_y)
          IF(dim_cas.EQ.4) dsp_tab(:,:,1) =
     &     cy_2(deb_x:fin_x:str_x, deb_y:fin_y:str_y)
          IF(dim_cas.EQ.3) dsp_tab(:,1,1) =
     &     cy_1(deb_x:fin_x:str_x)
          IF(dim_cas.EQ.2) dsp_tab(:,1,1) =
     &     cy_1(deb_x:fin_x:str_x)

        CASE(9)
          IF(dim_cas.EQ.6) dsp_tab(:,:,:) =
     &     cz_3(deb_x:fin_x:str_x, deb_y:fin_y:str_y, deb_z:fin_z:str_z)
          IF(dim_cas.EQ.5) dsp_tab(:,:,1) =
     &     cz_2(deb_x:fin_x:str_x, deb_y:fin_y:str_y)
          IF(dim_cas.EQ.3) dsp_tab(:,1,1) =
     &     cz_1(deb_x:fin_x:str_x)

        CASE(10)
          IF(dim_cas.EQ.6) dsp_tab(:,:,:) =
     &     rh_3(deb_x:fin_x:str_x, deb_y:fin_y:str_y, deb_z:fin_z:str_z)
          IF(dim_cas.EQ.5) dsp_tab(:,:,1) =
     &     rh_2(deb_x:fin_x:str_x, deb_y:fin_y:str_y)
          IF(dim_cas.EQ.4) dsp_tab(:,:,1) =
     &     rh_2(deb_x:fin_x:str_x, deb_y:fin_y:str_y)
          IF(dim_cas.EQ.3) dsp_tab(:,1,1) =
     &     rh_1(deb_x:fin_x:str_x)
          IF(dim_cas.EQ.2) dsp_tab(:,1,1) =
     &     rh_1(deb_x:fin_x:str_x)
          IF(dim_cas.EQ.1) dsp_tab(:,1,1) =
     &     rh_1(deb_x:fin_x:str_x)
     
! ----  diagnostic du profil d amortissement     
        CASE(11)
!          IF(dim_cas.EQ.6) dsp_tab(:,:,:) =
!     &     thetad_3(deb_x:fin_x:str_x, deb_y:fin_y:str_y, deb_z:fin_z:str_z)
          IF(dim_cas.EQ.5) dsp_tab(:,:,1) =
     &     thetad_2(deb_x:fin_x:str_x, deb_y:fin_y:str_y)
          IF(dim_cas.EQ.4) dsp_tab(:,:,1) =
     &     thetad_2(deb_x:fin_x:str_x, deb_y:fin_y:str_y)
          IF(dim_cas.EQ.3) dsp_tab(:,1,1) =
     &     thetad_2(deb_x:fin_x:str_x, nulmy)
          IF(dim_cas.EQ.2) dsp_tab(:,1,1) =
     &     thetad_2(deb_x:fin_x:str_x, nulmy)
          IF(dim_cas.EQ.1) dsp_tab(:,1,1) =
     &     thetad_2(deb_x:fin_x:str_x, nulmy)
     
        END SELECT

        IF(numproc.NE.1) THEN
!          CALL MPI_SEND(dsp_tab(1,1,1), siz_x*siz_y*siz_z,
!     &                  MPI_CALDER_REAL, itids(1), 110+dsp_i,
!     &                  MPI_COMM_WORLD, info)
          write(6,"('envoi par ',I3,' de taille',3(1X,I3))")
     &             numproc,siz_x,siz_y,siz_z
        ELSE
          dsp_big(1+aux_glis:siz_x+aux_glis, 1:siz_y, 1:siz_z)
     &  = dsp_tab(:,:,:)
        ENDIF

        DEALLOCATE( dsp_tab )
      ENDIF

      IF(numproc.EQ.1) THEN
! --- boucle sur les autres PE
        IF(nproc.GE.2) THEN
        DO iproc=2,nproc
! ------ determine la contribution du PE distant iproc
          CALL indices_sstab(ibord(1,iproc), ibord(2,iproc),
     &                       dsp(dsp_i)%imn(1),
     &                       dsp(dsp_i)%imx(1), 
     &                       dsp(dsp_i)%siz(1),
     &                       deb_x, fin_x, aux_glis, str_x )
          CALL indices_sstab(ibord(1,iproc), ibord(2,iproc),
     &                       dsp(dsp_i)%imn(1)-nb_glis,
     &                       dsp(dsp_i)%imx(1)-nb_glis, 
     &                       dsp(dsp_i)%siz(1),
     &                       deb_x, fin_x, siz_x, str_x )
          aux_glis = aux_glis - siz_x
          deb_x = (deb_x-dsp(dsp_i)%imn(1))/str_x + 1 + aux_glis
          CALL indices_sstab(ibord(3,iproc), ibord(4,iproc),
     &                       dsp(dsp_i)%imn(2),
     &                       dsp(dsp_i)%imx(2), dsp(dsp_i)%siz(2),
     &                       deb_y, fin_y, siz_y, str_y )
          deb_y = (deb_y-dsp(dsp_i)%imn(2))/str_y + 1
          CALL indices_sstab(ibord(5,iproc), ibord(6,iproc),
     &                       dsp(dsp_i)%imn(3),
     &                       dsp(dsp_i)%imx(3), dsp(dsp_i)%siz(3),
     &                       deb_z, fin_z, siz_z, str_z )
          deb_z = (deb_z-dsp(dsp_i)%imn(3))/str_z + 1
          IF(siz_x*siz_y*siz_z.NE.0) THEN
! ------ contruit le type adapte a la reception
!        reels double precision supposes codes sur 8 octets
!            CALL MPI_TYPE_VECTOR(siz_y, siz_x, dsp(dsp_i)%siz(1),
!     &                           MPI_CALDER_REAL, typ_xy, info)
!            aux = 8*dsp(dsp_i)%siz(1)*dsp(dsp_i)%siz(2)
!            CALL MPI_TYPE_HVECTOR(siz_z, 1, aux, typ_xy, dsp_typ, info)
!            CALL MPI_TYPE_COMMIT(dsp_typ, info)
!            write(6,"('attente de ',I3,' de taille',3(1X,I3))")
!     &                iproc,siz_x,siz_y,siz_z
!            CALL MPI_RECV(dsp_big(deb_x,deb_y,deb_z), 1, dsp_typ,
!     &                    itids(iproc), 110+dsp_i,
!     &                    MPI_COMM_WORLD, status, info)
!            CALL MPI_TYPE_FREE(dsp_typ, info)
!	    CALL MPI_TYPE_FREE(typ_xy,  info)
          ENDIF
        ENDDO
        ENDIF

! --- memorisation en cas de diag moyen en temps
        IF(dsp(dsp_i)%moy.EQ.1) THEN
          dsp_mem(1:dsp(dsp_i)%siz(1),1:dsp(dsp_i)%siz(2),
     &            1:dsp(dsp_i)%siz(3),dsp_i)
     &  = dsp_mem(1:dsp(dsp_i)%siz(1),1:dsp(dsp_i)%siz(2),
     &            1:dsp(dsp_i)%siz(3),dsp_i)
     &  + dsp_big(:,:,:)
          IF(ecrit_ok) THEN
            dsp_big(:,:,:)
     &    = dsp_mem(1:dsp(dsp_i)%siz(1),1:dsp(dsp_i)%siz(2),
     &              1:dsp(dsp_i)%siz(3),dsp_i) / it_mem
          ENDIF
        ENDIF
        IF(dsp(dsp_i)%moy.EQ.2) THEN
          dsp_mem(1:dsp(dsp_i)%siz(1),1:dsp(dsp_i)%siz(2),
     &            1:dsp(dsp_i)%siz(3),dsp_i)
     &  = dsp_mem(1:dsp(dsp_i)%siz(1),1:dsp(dsp_i)%siz(2),
     &            1:dsp(dsp_i)%siz(3),dsp_i)
     &  + dsp_big(:,:,:)**2
          IF(ecrit_ok) THEN
            dsp_big(:,:,:)
     &    = SQRT(dsp_mem(1:dsp(dsp_i)%siz(1),1:dsp(dsp_i)%siz(2),
     &                   1:dsp(dsp_i)%siz(3),dsp_i) / it_mem)
          ENDIF
        ENDIF

! --- ecriture de dsp_big
        IF(ecrit_ok) THEN

          IF(gliss_ok.EQ.1) THEN
            CALL indices_sstab(ibord(1,1), ibord(2,nproc),
     &                         dsp(dsp_i)%imn(1),
     &                         dsp(dsp_i)%imx(1),
     &                         dsp(dsp_i)%siz(1),
     &                         deb_x, fin_x, aux_glis, str_x )
            CALL indices_sstab(ibord(1,1), ibord(2,nproc),
     &                         dsp(dsp_i)%imn(1)-nb_glis,
     &                         dsp(dsp_i)%imx(1)-nb_glis,
     &                         dsp(dsp_i)%siz(1),
     &                         deb_x, fin_x, siz_x, str_x )
              aux_glis = aux_glis - siz_x
            IF(aux_glis.GT.0) THEN
              WRITE(fdbg,"('On met a zero ',I3,' colonnes')") aux_glis
              dsp_big(1:aux_glis,:,:) = 0.
            ENDIF
          ENDIF

          ific = dsp_typ_fic(dsp(dsp_i)%typ)
          WRITE(ific,'("CHAMP ",A2)')     dsp_typ_map(dsp(dsp_i)%typ)
          WRITE(ific,'(1X,1PE10.3,1X,I1)') iter*pasdt,dsp(dsp_i)%moy
          IF((gliss_ok.EQ.1).AND.(dsp(dsp_i)%moy.NE.0)) THEN
            WRITE(ific,'(3(I5,1X),1PE10.3)')
     &                           dsp(dsp_i)%imn(1),dsp(dsp_i)%imx(1),
     &                           dsp(dsp_i)%siz(1),dsp(dsp_i)%xmin_ref
          ELSE
            WRITE(ific,'(3(I5,1X),1PE10.3)')
     &                           dsp(dsp_i)%imn(1),dsp(dsp_i)%imx(1),
     &                           dsp(dsp_i)%siz(1),xmin_gl
          ENDIF
          WRITE(ific,'(3(I5,1X),1PE10.3)')
     &                              dsp(dsp_i)%imn(2),dsp(dsp_i)%imx(2),
     &                              dsp(dsp_i)%siz(2),ymin_gl
          WRITE(ific,'(3(I5,1X),1PE10.3)')
     &                              dsp(dsp_i)%imn(3),dsp(dsp_i)%imx(3),
     &                              dsp(dsp_i)%siz(3),zmin_gl

          WHERE(ABS(dsp_big).LT.1.e-99_8) dsp_big=0.
          CALL ecrit_tab3d(ific, dsp(dsp_i)%siz(1), dsp(dsp_i)%siz(2),
     &                           dsp(dsp_i)%siz(3), dsp_big)
        ENDIF

        DEALLOCATE( dsp_big )
      ENDIF

      RETURN
!     ___________________________
      END SUBROUTINE diag_spatial




      SUBROUTINE indices_sstab(deb_dom_loc, fin_dom_loc,
     &                         deb_tab, fin_tab, siz_tab,
     &                         deb_loc, fin_loc, siz_loc, str_tab )
! ======================================================================
      IMPLICIT NONE

      INTEGER, INTENT(IN)  :: deb_dom_loc, fin_dom_loc,
     &                        deb_tab, fin_tab, siz_tab
      INTEGER, INTENT(OUT) :: deb_loc, fin_loc, siz_loc, str_tab
      INTEGER              :: siz_x, aux

      siz_x = siz_tab
      IF(siz_x.NE.1) THEN
        str_tab = (fin_tab-deb_tab)/(siz_x-1)
      ELSE
        str_tab = 1
      ENDIF
      IF(deb_tab.GT.fin_dom_loc) THEN
        siz_x = 0
      ELSEIF(deb_tab.GE.deb_dom_loc) THEN
        deb_loc = deb_tab
      ELSE
        aux = (deb_dom_loc-deb_tab+str_tab-1) / str_tab
        deb_loc = deb_tab + aux*str_tab
        IF(deb_loc.GT.fin_dom_loc) siz_x = 0
      ENDIF
      IF(siz_x.NE.0) THEN
        IF(fin_tab.LT.deb_dom_loc) THEN
          siz_x = 0
        ELSEIF(fin_tab.LE.fin_dom_loc) THEN
          fin_loc = fin_tab
        ELSE
          aux = (fin_dom_loc-deb_tab) / str_tab
          fin_loc = deb_tab + aux*str_tab
          IF(fin_loc.LT.deb_dom_loc) siz_x = 0
        ENDIF
      ENDIF
      IF(siz_x.NE.0) siz_x = (fin_loc-deb_loc)/str_tab + 1

      siz_loc = siz_x
      RETURN
!     ____________________________
      END SUBROUTINE indices_sstab
