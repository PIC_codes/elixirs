! ======================================================================
! diag_tp.f
! ---------
!
!     SUBROUTINE diag_tp
!     SUBROUTINE diag_tp_impl2d
!
! ======================================================================




      SUBROUTINE diag_tp_impl2d(iesp)
! ======================================================================
! Diagnostics scalaires effectues a chaque pas de temps
! Ce sont des doublons obtenus directement � partir de impl2d
!    + Energies cinetiques         UNIT=61
!    + Energies electromagnetiques UNIT=62
!
! ======================================================================

       USE domaines
       USE diagnostics
       USE particules
       USE particules_klder
       USE temps
       USE erreurs

      
       IMPLICIT NONE

       INTEGER, INTENT(IN)    ::   iesp
  
  
       REAL(kind=kr)   ::  xboost, yboost, tboost     
       INTEGER        ::   ip
       
       ip = 1
      
       IF(nbpa(iref).EQ.1) THEN
         IF(iesp==1) THEN    
           WRITE(IDxe_t,wforma) partic(a_qx,ip,iesp) 
           WRITE(IDye_t,wforma) partic(a_qy,ip,iesp) 
           WRITE(IDvxe_t,wforma) partic(a_px,ip,iesp)
           WRITE(IDvye_t,wforma) partic(a_py,ip,iesp) 
           WRITE(IDvze_t,wforma) partic(a_pz,ip,iesp) 
           WRITE(IDgae_t,wforma) partic(a_gatld12,ip,iesp)

           IF(dynam(iesp).EQ.2) THEN     
	     xboost = (partic(a_qx,ip,iesp) - 
     &                 boost*iter*dt)/sqrt(1-boost**2)
	     yboost = partic(a_qy,ip,iesp)
	     tboost = (iter*dt - boost*partic(a_qx,ip,iesp))
     &                /sqrt(1-boost**2) 
	   
             WRITE(IDxrel,wforma) xboost 
             WRITE(IDyrel,wforma) yboost 
             WRITE(IDtrel,wforma) tboost 	   
	   ENDIF
         ENDIF

         IF(iesp==2) THEN
           WRITE(IDxi_t,wforma) partic(a_qx,ip,iesp)  
           WRITE(IDyi_t,wforma) partic(a_qy,ip,iesp)  
           WRITE(IDvxi_t,wforma) partic(a_px,ip,iesp) 
           WRITE(IDvyi_t,wforma) partic(a_py,ip,iesp)  
           WRITE(IDvzi_t,wforma) partic(a_pz,ip,iesp)  
           WRITE(IDgai_t,wforma) partic(a_gatld12,ip,iesp)   
	 ENDIF
       ENDIF
       
       
       IF(debug.GE.4 .AND. iter.EQ.0) THEN
         IF(iesp==1) THEN
          DO ip=1, nbpa(iesp)  
           WRITE(IDxe_t,wforma) partic(a_qx,ip,iesp) 
           WRITE(IDye_t,wforma) partic(a_qy,ip,iesp) 
           WRITE(IDvxe_t,wforma) partic(a_px,ip,iesp)
           WRITE(IDvye_t,wforma) partic(a_py,ip,iesp) 
           WRITE(IDvze_t,wforma) partic(a_pz,ip,iesp) 
           WRITE(IDgae_t,wforma) partic(a_gatld12,ip,iesp)
          ENDDO
	 ELSEIF(iesp==2) THEN
          DO ip=1, nbpa(iesp)  
           WRITE(IDxi_t,wforma) partic(a_qx,ip,iesp) 
           WRITE(IDyi_t,wforma) partic(a_qy,ip,iesp) 
           WRITE(IDvxi_t,wforma) partic(a_px,ip,iesp)
           WRITE(IDvyi_t,wforma) partic(a_py,ip,iesp) 
           WRITE(IDvzi_t,wforma) partic(a_pz,ip,iesp) 
           WRITE(IDgai_t,wforma) partic(a_gatld12,ip,iesp)
          ENDDO	 
	 ENDIF
       ENDIF
       

      RETURN
!     _____________________________
      END SUBROUTINE diag_tp_impl2d



      SUBROUTINE diag_tp(pasdt)
! ======================================================================
! Diagnostics scalaires effectues a chaque pas de temps
!    + Poids des differentes especes
!    + Energies cinetiques
!    + Flux d'impulsions
!    + Energies electromagnetiques
!    + Flux du vecteur de Poynting
!    + 
! Entree : fpsr = flux de Poynting source
! ======================================================================

      USE domaines
      USE domaines_klder      
      USE particules_klder
      USE champs
      USE diagnostics_klder
      USE erreurs

      IMPLICIT NONE
!!      INCLUDE 'mpif.h'

      REAL(KIND=kr), INTENT(IN) :: pasdt 
      INTEGER                   :: iesp, i
      REAL(KIND=kr)             :: vol_s2
      REAL(KIND=kr), DIMENSION(:,:,:), ALLOCATABLE :: tp_3
      REAL(KIND=kr), DIMENSION(:,:),   ALLOCATABLE :: tp_2
      REAL(KIND=kr), DIMENSION(:),     ALLOCATABLE :: tp_1

           

! Diagnostics particulaires : boucle sur les especes de particules
! ----------------------------------------------------------------------
      IF(nesp_p.GT.0) THEN
        DO iesp=1,nesp_p
	
!   nbm_plas repr�sente le nombre de mailles de plasma
! --- Poids des differentes especes  *nbm_plas
          d1d_loc(d_po(iesp)) = SUM(partic(a_po,1:nbpa(iesp),
     &         iesp))

! --- Energie cinetique des differentes especes
          d1d_loc(d_ki(iesp)) =  masse(iesp) 
     &                          *SUM( partic(a_ga,1:nbpa(iesp),iesp)
     &                               *partic(a_po,1:nbpa(iesp),iesp))
          IF(dynam(iesp).EQ.2) 
     &      d1d_loc(d_ki(iesp)) = d1d_loc(d_ki(iesp))
     &                           -d1d_loc(d_po(iesp))*masse(iesp)
     
! --- Impulsions des differentes especes (LG, 03/06)     
          IF(a_px.NE.0) THEN
	      d1d_loc(d_px(iesp)) = masse(iesp)
     &	                   *SUM(partic(a_po,1:nbpa(iesp),iesp)
     &                     *partic(a_px,1:nbpa(iesp),iesp))
	  ENDIF
	  IF(a_py.NE.0) THEN
	      d1d_loc(d_py(iesp)) = masse(iesp)
     &	                   *SUM(partic(a_po,1:nbpa(iesp),iesp)
     &                     *partic(a_py,1:nbpa(iesp),iesp)) 
	  ENDIF
	  IF(a_pz.NE.0) THEN
	      d1d_loc(d_pz(iesp)) = masse(iesp)
     &	                   *SUM(partic(a_po,1:nbpa(iesp),iesp)
     &                     *partic(a_pz,1:nbpa(iesp),iesp))   	  
	  ENDIF
     
! --- Flux d'impulsions des differentes especes (LG, 01/06)     
          IF(a_px.NE.0) THEN
	    IF(dynam(iesp).EQ.2) THEN
	      d1d_loc(d_fpx(iesp)) = 
     &            SUM(partic(a_po,1:nbpa(iesp),iesp)
     &                           *partic(a_px,1:nbpa(iesp),iesp)**2
     &                           /partic(a_ga,1:nbpa(iesp),iesp))
     &                           /masse(iesp)                           
	    ELSE
	      d1d_loc(d_fpx(iesp)) = masse(iesp)
     &                     *SUM(partic(a_po,1:nbpa(iesp),iesp)
     &                     *partic(a_px,1:nbpa(iesp),iesp)**2)
	    ENDIF	  
	  ENDIF
	  IF(a_py.NE.0) THEN
	    IF(dynam(iesp).EQ.2) THEN
	      d1d_loc(d_fpy(iesp)) = 
     &            SUM(partic(a_po,1:nbpa(iesp),iesp)
     &                           *partic(a_py,1:nbpa(iesp),iesp)**2
     &                           /partic(a_ga,1:nbpa(iesp),iesp))
     &                           /masse(iesp)     
	    ELSE
	      d1d_loc(d_fpy(iesp)) = masse(iesp)
     &                     *SUM(partic(a_po,1:nbpa(iesp),iesp)
     &                     *partic(a_py,1:nbpa(iesp),iesp)**2)	    
	    ENDIF	  	  
	  ENDIF
	  IF(a_pz.NE.0) THEN
	    IF(dynam(iesp).EQ.2) THEN
	      d1d_loc(d_fpz(iesp)) = 
     &           SUM(partic(a_po,1:nbpa(iesp),iesp)
     &                           *partic(a_pz,1:nbpa(iesp),iesp)**2
     &                           /partic(a_ga,1:nbpa(iesp),iesp))
     &                           /masse(iesp)   	    
	    ELSE
	      d1d_loc(d_fpz(iesp)) = masse(iesp)
     &                     *SUM(partic(a_po,1:nbpa(iesp),iesp)
     &                     *partic(a_pz,1:nbpa(iesp),iesp)**2)		    
	    ENDIF	  	  
	  ENDIF
        ENDDO
      ENDIF


! Diagnostics electromagnetiques :
! Energies des champs 
! (on compte positivement l'energie qui SORT du domaine)
! On centre les energies sur les points interieurs du maillage dual,
! et les vecteurs de Poynting sur les centres des mailles de sortie :
! cela garantit qu'il n'y a pas de recouvrements entre PEs
! ----------------------------------------------------------------------
      SELECT CASE(dim_cas)
      CASE(6)
        vol_s2 = 0.5_8*pasdx*pasdy*pasdz
        ALLOCATE(tp_3(nulmx+1:nulpx, nulmy+1:nulpy, nulmz+1:nulpz))
        IF(d_ex.NE.0) THEN
       tp_3(:,:,:) = ex_3(nulmx+1:nulpx, nulmy:nulpy-1, nulmz:nulpz-1)
     &             + ex_3(nulmx+1:nulpx, nulmy+1:nulpy, nulmz:nulpz-1)
     &             + ex_3(nulmx+1:nulpx, nulmy:nulpy-1, nulmz+1:nulpz)
     &             + ex_3(nulmx+1:nulpx, nulmy+1:nulpy, nulmz+1:nulpz)
         d1d_loc(d_ex) = 0.0625_8 * SUM( tp_3(:,:,:)**2 ) * vol_s2
        ENDIF
        IF(d_ey.NE.0) THEN
       tp_3(:,:,:) = ey_3(nulmx:nulpx-1, nulmy+1:nulpy, nulmz:nulpz-1)
     &             + ey_3(nulmx+1:nulpx, nulmy+1:nulpy, nulmz:nulpz-1)
     &             + ey_3(nulmx:nulpx-1, nulmy+1:nulpy, nulmz+1:nulpz)
     &             + ey_3(nulmx+1:nulpx, nulmy+1:nulpy, nulmz+1:nulpz)
         d1d_loc(d_ey) = 0.0625_8 * SUM( tp_3(:,:,:)**2 ) * vol_s2
        ENDIF
        IF(d_ez.NE.0) THEN
       tp_3(:,:,:) = ez_3(nulmx:nulpx-1, nulmy:nulpy-1, nulmz+1:nulpz)
     &             + ez_3(nulmx+1:nulpx, nulmy:nulpy-1, nulmz+1:nulpz)
     &             + ez_3(nulmx:nulpx-1, nulmy+1:nulpy, nulmz+1:nulpz)
     &             + ez_3(nulmx+1:nulpx, nulmy+1:nulpy, nulmz+1:nulpz)
         d1d_loc(d_ez) = 0.0625_8 * SUM( tp_3(:,:,:)**2 ) * vol_s2
        ENDIF
        IF(d_bx.NE.0) THEN
       tp_3(:,:,:) = bx_3(nulmx:nulpx-1, nulmy+1:nulpy, nulmz+1:nulpz)
     &             + bx_3(nulmx+1:nulpx, nulmy+1:nulpy, nulmz+1:nulpz)
         d1d_loc(d_bx) = 0.25_8 * SUM( tp_3(:,:,:)**2 ) * vol_s2
        ENDIF
        IF(d_by.NE.0) THEN
       tp_3(:,:,:) = by_3(nulmx+1:nulpx, nulmy:nulpy-1, nulmz+1:nulpz)
     &             + by_3(nulmx+1:nulpx, nulmy+1:nulpy, nulmz+1:nulpz)
         d1d_loc(d_by) = 0.25_8 * SUM( tp_3(:,:,:)**2 ) * vol_s2
        ENDIF
        IF(d_bz.NE.0) THEN
       tp_3(:,:,:) = bz_3(nulmx+1:nulpx, nulmy+1:nulpy, nulmz:nulpz-1)
     &             + bz_3(nulmx+1:nulpx, nulmy+1:nulpy, nulmz+1:nulpz)
         d1d_loc(d_bz) = 0.25_8 * SUM( tp_3(:,:,:)**2 ) * vol_s2
        ENDIF
        DEALLOCATE(tp_3)

      CASE(5)
        vol_s2 = 0.5_8*pasdx*pasdy
        ALLOCATE(tp_2(nulmx:nulpx-2, nulmy+1:nulpy-1))
        IF(d_ex.NE.0) THEN
          tp_2(:,:) = 0.5*
     &        ( extm1(nulmx+1:nulpx-1, nulmy+1:nulpy-1)
     &        + ex_2 (nulmx+1:nulpx-1, nulmy+1:nulpy-1) )

          d1d_loc(d_ex) = SUM( tp_2(:,:)**2 ) * vol_s2
        ENDIF
        IF(d_ey.NE.0) THEN
          tp_2(:,:) = 0.5*
     &        ( eytm1(nulmx+1:nulpx-1, nulmy+1:nulpy-1)
     &        + ey_2 (nulmx+1:nulpx-1, nulmy+1:nulpy-1) )

          d1d_loc(d_ey) = SUM( tp_2(:,:)**2 ) * vol_s2
        ENDIF
        IF(d_ez.NE.0) THEN
          tp_2(:,:) = 0.5*
     &        ( eztm1(nulmx+1:nulpx-1, nulmy+1:nulpy-1)
     &        + ez_2 (nulmx+1:nulpx-1, nulmy+1:nulpy-1) )

          d1d_loc(d_ez) = SUM( tp_2(:,:)**2 ) * vol_s2
        ENDIF
        IF(d_bx.NE.0) THEN
          tp_2(:,:) = 0.5*
     &      ( zetax(nulmx+1:nulpx-1, nulmy+1:nulpy-1)
     &      + zetax(nulmx+1:nulpx-1, nulmy+2:nulpy) )

          d1d_loc(d_bx) = SUM( tp_2(:,:)**2 ) * vol_s2
        ENDIF
        IF(d_by.NE.0) THEN
          tp_2(:,:) = 0.5*
     &      ( zetay(nulmx+1:nulpx-1, nulmy+1:nulpy-1)
     &      + zetay(nulmx+2:nulpx, nulmy+1:nulpy-1) )

          d1d_loc(d_by) = SUM( tp_2(:,:)**2 ) * vol_s2
        ENDIF
        IF(d_bz.NE.0) THEN
          tp_2(:,:) = 0.25*
     &       ( zetaz(nulmx+1:nulpx-1, nulmy+1:nulpy-1)
     &       + zetaz(nulmx+2:nulpx  , nulmy+1:nulpy-1)
     &       + zetaz(nulmx+1:nulpx-1, nulmy+2:nulpy)
     &       + zetaz(nulmx+2:nulpx  , nulmy+2:nulpy) )

          d1d_loc(d_bz) = SUM( tp_2(:,:)**2 ) * vol_s2
        ENDIF
        DEALLOCATE(tp_2)

      CASE(4)
        vol_s2 = 0.5_8*pasdx*pasdy
        ALLOCATE(tp_2(nulmx+1:nulpx, nulmy+1:nulpy))
        IF(d_ex.NE.0) THEN
          tp_2(:,:) = ex_2(nulmx+1:nulpx, nulmy  :nulpy-1) 
     &              + ex_2(nulmx+1:nulpx, nulmy+1:nulpy  )
          d1d_loc(d_ex) = 0.25_8 * SUM( tp_2(:,:)**2 ) * vol_s2
        ENDIF
        IF(d_ey.NE.0) THEN
          tp_2(:,:) = ey_2(nulmx  :nulpx-1, nulmy+1:nulpy) 
     &              + ey_2(nulmx+1:nulpx  , nulmy+1:nulpy)
          d1d_loc(d_ey) = 0.25_8 * SUM( tp_2(:,:)**2 ) * vol_s2
        ENDIF
        IF(d_bz.NE.0) THEN
          d1d_loc(d_bz) = SUM( bz_2(nulmx+1:nulpx,nulmy+1:nulpy)**2 )
     &                                                 * vol_s2
        ENDIF
        DEALLOCATE(tp_2)

      CASE(3)
        vol_s2 = 0.5_8*pasdx
        ALLOCATE(tp_1(nulmx+1:nulpx))
        IF(d_ex.NE.0) THEN
          d1d_loc(d_ex) = SUM( ex_1(nulmx+1:nulpx)**2 ) * vol_s2
        ENDIF
        IF(d_ey.NE.0) THEN
          tp_1(:) = ey_1(nulmx:nulpx-1) + ey_1(nulmx+1:nulpx)
          d1d_loc(d_ey) = 0.25_8 * SUM( tp_1(:)**2 ) * vol_s2
        ENDIF
        IF(d_ez.NE.0) THEN
          tp_1(:) = ez_1(nulmx:nulpx-1) + ez_1(nulmx+1:nulpx)
          d1d_loc(d_ez) = 0.25_8 * SUM( tp_1(:)**2 ) * vol_s2
        ENDIF
        IF(d_bx.NE.0) THEN
          tp_1(:) = bx_1(nulmx:nulpx-1) + bx_1(nulmx+1:nulpx)
          d1d_loc(d_bx) = 0.25_8 * SUM( tp_1(:)**2 ) * vol_s2
        ENDIF
        IF(d_by.NE.0) THEN
          d1d_loc(d_by) = SUM( by_1(nulmx+1:nulpx)**2 ) * vol_s2
        ENDIF
        IF(d_bz.NE.0) THEN
          d1d_loc(d_bz) = SUM( bz_1(nulmx+1:nulpx)**2 ) * vol_s2
        ENDIF
        DEALLOCATE(tp_1)

      CASE(2,1)
        vol_s2 = 0.5_8*pasdx
        ALLOCATE(tp_1(nulmx+1:nulpx))
        IF(d_ex.NE.0) THEN
          d1d_loc(d_ex) = SUM( ex_1(nulmx+1:nulpx)**2 ) * vol_s2
        ENDIF
        IF(d_ey.NE.0) THEN
          tp_1(:) = ey_1(nulmx:nulpx-1) + ey_1(nulmx+1:nulpx)
          d1d_loc(d_ey) = 0.25_8 * SUM( tp_1(:)**2 ) * vol_s2
        ENDIF
        IF(d_bz.NE.0) THEN
          d1d_loc(d_bz) = SUM( bz_1(nulmx+1:nulpx)**2 ) * vol_s2
        ENDIF
        DEALLOCATE(tp_1)

      END SELECT

! Diagnostics :
! Champs en 1 point au cours du temps
! ----------------------------------------------------------------------
      d1d_loc(d_clo:d_clo+clo_nb-1) = 0.
      DO i=1,clo_nb

        IF((clo_ind(1,i).GE.nulmx).AND.(clo_ind(1,i).LE.nulpx)) THEN
        IF((clo_ind(2,i).GE.nulmy).AND.(clo_ind(2,i).LE.nulpy)) THEN
        IF((clo_ind(3,i).GE.nulmz).AND.(clo_ind(3,i).LE.nulpz)) THEN

        SELECT CASE(clo_ch(i))
          CASE('ex3') ; d1d_loc(d_clo+i-1)
     &                  = ex_3(clo_ind(1,i),clo_ind(2,i),clo_ind(3,i))
          CASE('ey3') ; d1d_loc(d_clo+i-1)
     &                  = ey_3(clo_ind(1,i),clo_ind(2,i),clo_ind(3,i))
          CASE('ez3') ; d1d_loc(d_clo+i-1)
     &                  = ez_3(clo_ind(1,i),clo_ind(2,i),clo_ind(3,i))
          CASE('bx3') ; d1d_loc(d_clo+i-1)
     &                  = bx_3(clo_ind(1,i),clo_ind(2,i),clo_ind(3,i))
          CASE('by3') ; d1d_loc(d_clo+i-1)
     &                  = by_3(clo_ind(1,i),clo_ind(2,i),clo_ind(3,i))
          CASE('bz3') ; d1d_loc(d_clo+i-1)
     &                  = bz_3(clo_ind(1,i),clo_ind(2,i),clo_ind(3,i))
          CASE('cx3') ; d1d_loc(d_clo+i-1)
     &                  = cx_3(clo_ind(1,i),clo_ind(2,i),clo_ind(3,i))
          CASE('cy3') ; d1d_loc(d_clo+i-1)
     &                  = cy_3(clo_ind(1,i),clo_ind(2,i),clo_ind(3,i))
          CASE('cz3') ; d1d_loc(d_clo+i-1)
     &                  = cz_3(clo_ind(1,i),clo_ind(2,i),clo_ind(3,i))
          CASE('rh3') ; d1d_loc(d_clo+i-1)
     &                  = rh_3(clo_ind(1,i),clo_ind(2,i),clo_ind(3,i))
        END SELECT

        ENDIF

        SELECT CASE(clo_ch(i))
          CASE('ex2') ; d1d_loc(d_clo+i-1) 
     &                  = ex_2(clo_ind(1,i),clo_ind(2,i))
          CASE('ey2') ; d1d_loc(d_clo+i-1)
     &                  = ey_2(clo_ind(1,i),clo_ind(2,i))
          CASE('ez2') ; d1d_loc(d_clo+i-1)
     &                  = ez_2(clo_ind(1,i),clo_ind(2,i))
          CASE('bz2') ; d1d_loc(d_clo+i-1)
     &                  = bz_2(clo_ind(1,i),clo_ind(2,i))
          CASE('cx2') ; d1d_loc(d_clo+i-1)
     &                  = cx_2(clo_ind(1,i),clo_ind(2,i))
          CASE('cy2') ; d1d_loc(d_clo+i-1)
     &                  = cy_2(clo_ind(1,i),clo_ind(2,i))
          CASE('rh2') ; d1d_loc(d_clo+i-1)
     &                  = rh_2(clo_ind(1,i),clo_ind(2,i))
        END SELECT

        ENDIF

        SELECT CASE(clo_ch(i))
          CASE('ex1') ; d1d_loc(d_clo+i-1) = ex_1(clo_ind(1,i))
          CASE('ey1') ; d1d_loc(d_clo+i-1) = ey_1(clo_ind(1,i))
          CASE('ez1') ; d1d_loc(d_clo+i-1) = ez_1(clo_ind(1,i))
          CASE('bx1') ; d1d_loc(d_clo+i-1) = bx_1(clo_ind(1,i))
          CASE('by1') ; d1d_loc(d_clo+i-1) = by_1(clo_ind(1,i))
          CASE('bz1') ; d1d_loc(d_clo+i-1) = bz_1(clo_ind(1,i))

!!          CASE('cx1') ; d1d_loc(d_clo+i-1) = cx_1(clo_ind(1,i))
          CASE('cx1') ; d1d_loc(d_clo+i-1) = MAXVAL(partic(a_ga,:,1))-1.

          CASE('cy1') ; d1d_loc(d_clo+i-1) = cy_1(clo_ind(1,i))
          CASE('rh1') ; d1d_loc(d_clo+i-1) = rh_1(clo_ind(1,i))
        END SELECT

        ENDIF

      ENDDO

! Les calculs de d1d_loc(d_pof(:)), d1d_loc(d_kif(:)), d1d_loc(d_kir(:)) 
! sont faits dans cndlim_part...
! ----------------------------------------------------------------------

! Les calculs de d1d_loc(d_gex:d_gbz) sont faits dans glisse_champs...
! ----------------------------------------------------------------------

! Diagnostics :
! Champs max au cours du temps
! ----------------------------------------------------------------------
      d1d_loc(d_cmx:d_cmx+cmx_nb-1) = 0.
      DO i=1,cmx_nb

        SELECT CASE(cmx_ch(i))
          CASE('ex3') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(ex_3))
          CASE('ey3') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(ey_3))
          CASE('ez3') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(ez_3))
          CASE('bx3') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(bx_3))
          CASE('by3') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(by_3))
          CASE('bz3') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(bz_3))
          CASE('cx3') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(cx_3))
          CASE('cy3') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(cy_3))
          CASE('cz3') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(cz_3))
          CASE('rh3') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(rh_3))
          CASE('ex2') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(ex_2))
          CASE('ey2') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(ey_2))
          CASE('bz2') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(bz_2))
          CASE('cx2') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(cx_2))
          CASE('cy2') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(cy_2))
          CASE('rh2') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(rh_2))
          CASE('ex1') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(ex_1))
          CASE('ey1') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(ey_1))
          CASE('ez1') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(ez_1))
          CASE('bx1') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(bx_1))
          CASE('by1') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(by_1))
          CASE('bz1') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(bz_1))
          CASE('cx1') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(cx_1))
          CASE('cy1') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(cy_1))
          CASE('cz1') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(cz_1))
          CASE('rh1') ; d1d_loc(d_cmx+i-1) = MAXVAL(ABS(rh_1))
        END SELECT

      ENDDO

! Les calculs de d1d_loc(d_psn:d_gau) sont faits dans maxpois...
! ----------------------------------------------------------------------

      d1d_som(:) = d1d_loc(:)


! Mise a zero des grandeurs qui ne sont pas integrees au cours du temps
! ----------------------------------------------------------------------
      IF(d1d_poidf(1).NE.0) d1d_loc(d1d_poidf(2):d1d_poidf(3)) = 0.
      IF(d1d_ecinf(1).NE.0) d1d_loc(d1d_ecinf(2):d1d_ecinf(3)) = 0.
      IF(d1d_ecinr(1).NE.0) d1d_loc(d1d_ecinr(2):d1d_ecinr(3)) = 0.
      IF(d1d_eemgg(1).NE.0) d1d_loc(d1d_eemgg(2):d1d_eemgg(3)) = 0.

! Ecriture des resultats par le PE 1 
! ----------------------------------------------------------------------
      IF(numproc.EQ.1) THEN
      WHERE(ABS(d1d_som).LE.1.E-100_8) d1d_som = 0.
      SELECT CASE(diag_typ)
!      CASE('hdf')

      CASE('bin')
        IF(d1d_poid (1).NE.0)
     &    WRITE(d1d_poid (1)) d1d_som(d1d_poid (2):d1d_poid (3))
        IF(d1d_ecin (1).NE.0)
     &    WRITE(d1d_ecin (1)) d1d_som(d1d_ecin (2):d1d_ecin (3))
        IF(d1d_impx(1).NE.0)
     &    WRITE(d1d_impx(1)) d1d_som(d1d_impx(2):d1d_impx(3))     
        IF(d1d_impy(1).NE.0)
     &    WRITE(d1d_impy(1)) d1d_som(d1d_impy(2):d1d_impy(3)) 
        IF(d1d_impz(1).NE.0)
     &    WRITE(d1d_impz(1)) d1d_som(d1d_impz(2):d1d_impz(3)) 
        IF(d1d_fimpx(1).NE.0)
     &    WRITE(d1d_fimpx(1)) d1d_som(d1d_fimpx(2):d1d_fimpx(3))     
        IF(d1d_fimpy(1).NE.0)
     &    WRITE(d1d_fimpy(1)) d1d_som(d1d_fimpy(2):d1d_fimpy(3)) 
        IF(d1d_fimpz(1).NE.0)
     &    WRITE(d1d_fimpz(1)) d1d_som(d1d_fimpz(2):d1d_fimpz(3)) 
        IF(d1d_poidf(1).NE.0)	
     &    WRITE(d1d_poidf(1)) d1d_som(d1d_poidf(2):d1d_poidf(3))
        IF(d1d_ecinf(1).NE.0)
     &    WRITE(d1d_ecinf(1)) d1d_som(d1d_ecinf(2):d1d_ecinf(3))
        IF(d1d_ecinr(1).NE.0)
     &    WRITE(d1d_ecinr(1)) d1d_som(d1d_ecinr(2):d1d_ecinr(3))
        IF(d1d_eemg (1).NE.0)
     &    WRITE(d1d_eemg (1)) d1d_som(d1d_eemg (2):d1d_eemg (3))
        IF(d1d_eemgg(1).NE.0)
     &    WRITE(d1d_eemgg(1)) d1d_som(d1d_eemgg(2):d1d_eemgg(3))
        IF(d1d_clo  (1).NE.0)
     &    WRITE(d1d_clo  (1)) d1d_som(d1d_clo  (2):d1d_clo  (3))
        IF(d1d_eptg (1).NE.0)
     &    WRITE(d1d_eptg (1)) d1d_som(d1d_eptg (2):d1d_eptg (3))
        IF(d1d_pois (1).NE.0)
     &    WRITE(d1d_pois (1)) d1d_som(d1d_pois (2):d1d_pois (3))
        IF(d1d_cmx  (1).NE.0)
     &    WRITE(d1d_cmx  (1)) d1d_som(d1d_cmx  (2):d1d_cmx  (3))

      CASE('txt')
        IF(d1d_poid (1).NE.0)  WRITE(d1d_poid (1),"( 5(1PE12.4,1X))")
     &          d1d_som(d1d_poid (2):d1d_poid (3))
        IF(d1d_ecin (1).NE.0)  WRITE(d1d_ecin (1),"( 5(1PE12.4,1X))")
     &          d1d_som(d1d_ecin (2):d1d_ecin (3))
        IF(d1d_impx(1).NE.0)  WRITE(d1d_impx(1),"( 5(1PE12.4,1X))")
     &          d1d_som(d1d_impx(2):d1d_impx(3))     
        IF(d1d_impy(1).NE.0)  WRITE(d1d_impy(1),"( 5(1PE12.4,1X))")
     &          d1d_som(d1d_impy(2):d1d_impy(3))     
        IF(d1d_impz(1).NE.0)  WRITE(d1d_impz(1),"( 5(1PE12.4,1X))")
     &          d1d_som(d1d_impz(2):d1d_impz(3))
        IF(d1d_fimpx(1).NE.0)  WRITE(d1d_fimpx(1),"( 5(1PE12.4,1X))")
     &          d1d_som(d1d_fimpx(2):d1d_fimpx(3))     
        IF(d1d_fimpy(1).NE.0)  WRITE(d1d_fimpy(1),"( 5(1PE12.4,1X))")
     &          d1d_som(d1d_fimpy(2):d1d_fimpy(3))     
        IF(d1d_fimpz(1).NE.0)  WRITE(d1d_fimpz(1),"( 5(1PE12.4,1X))")
     &          d1d_som(d1d_fimpz(2):d1d_fimpz(3))     
        IF(d1d_poidf(1).NE.0)  WRITE(d1d_poidf(1),"( 5(1PE12.4,1X))")
     &          d1d_som(d1d_poidf(2):d1d_poidf(3))
        IF(d1d_ecinf(1).NE.0)  WRITE(d1d_ecinf(1),"( 5(1PE12.4,1X))")
     &          d1d_som(d1d_ecinf(2):d1d_ecinf(3))
        IF(d1d_ecinr(1).NE.0)  WRITE(d1d_ecinr(1),"( 5(1PE12.4,1X))")
     &          d1d_som(d1d_ecinr(2):d1d_ecinr(3))
        IF(d1d_eemg (1).NE.0)  WRITE(d1d_eemg (1),"( 6(1PE12.4,1X))")
     &          d1d_som(d1d_eemg (2):d1d_eemg (3))
        IF(d1d_eemgg(1).NE.0)  WRITE(d1d_eemgg(1),"( 6(1PE12.4,1X))")
     &          d1d_som(d1d_eemgg(2):d1d_eemgg(3))
        IF(d1d_clo  (1).NE.0)  WRITE(d1d_clo  (1),"( 6(1PE12.4,1X))")
     &          d1d_som(d1d_clo  (2):d1d_clo  (3))
        IF(d1d_eptg (1).NE.0)  WRITE(d1d_eptg (1),"( 7(1PE12.4,1X))")
     &          d1d_som(d1d_eptg (2):d1d_eptg (3))
        IF(d1d_pois (1).NE.0)  WRITE(d1d_pois (1),"( 2(1PE12.4,1X))")
     &          d1d_som(d1d_pois (2):d1d_pois (3))
        IF(d1d_cmx  (1).NE.0)  WRITE(d1d_cmx  (1),"( 6(1PE12.4,1X))")
     &          d1d_som(d1d_cmx  (2):d1d_cmx  (3))

      CASE DEFAULT
        IF(d1d_poid (1).NE.0)  WRITE(6,"('poids  ', 5(1PE12.4,1X))")
     &    d1d_som(d1d_poid (2):d1d_poid (3))
        IF(d1d_ecin (1).NE.0)  WRITE(6,"('e_cin  ', 5(1PE12.4,1X))")
     &    d1d_som(d1d_ecin (2):d1d_ecin (3))
        IF(d1d_impx(1).NE.0)  WRITE(6,"('impx', 5(1PE12.4,1X))")
     &    d1d_som(d1d_impx(2):d1d_impx(3))     
        IF(d1d_impy(1).NE.0)  WRITE(6,"('impy', 5(1PE12.4,1X))")
     &    d1d_som(d1d_impy(2):d1d_impy(3)) 
        IF(d1d_impz(1).NE.0)  WRITE(6,"('impz', 5(1PE12.4,1X))")
     &    d1d_som(d1d_impz(2):d1d_impz(3))
        IF(d1d_fimpx(1).NE.0)  WRITE(6,"('e_fimpx', 5(1PE12.4,1X))")
     &    d1d_som(d1d_fimpx(2):d1d_fimpx(3))     
        IF(d1d_fimpy(1).NE.0)  WRITE(6,"('e_fimpy', 5(1PE12.4,1X))")
     &    d1d_som(d1d_fimpy(2):d1d_fimpy(3)) 
        IF(d1d_fimpz(1).NE.0)  WRITE(6,"('e_fimpz', 5(1PE12.4,1X))")
     &    d1d_som(d1d_fimpz(2):d1d_fimpz(3))          
        IF(d1d_poidf(1).NE.0)  WRITE(6,"('poidsf ', 5(1PE12.4,1X))")
     &    d1d_som(d1d_poidf(2):d1d_poidf(3))
        IF(d1d_ecinf(1).NE.0)  WRITE(6,"('e_cinf ', 5(1PE12.4,1X))")
     &    d1d_som(d1d_ecinf(2):d1d_ecinf(3))
        IF(d1d_ecinr(1).NE.0)  WRITE(6,"('e_cinr ', 5(1PE12.4,1X))")
     &    d1d_som(d1d_ecinr(2):d1d_ecinr(3))
        IF(d1d_eemg (1).NE.0)  WRITE(6,"('e_emg  ', 6(1PE12.4,1X))")
     &    d1d_som(d1d_eemg (2):d1d_eemg (3))
        IF(d1d_eemgg(1).NE.0)  WRITE(6,"('e_emgg ', 6(1PE12.4,1X))")
     &    d1d_som(d1d_eemgg(2):d1d_eemgg(3))
        IF(d1d_clo  (1).NE.0)  WRITE(6,"('c_loc  ', 6(1PE12.4,1X))")
     &    d1d_som(d1d_clo  (2):d1d_clo  (3))
        IF(d1d_eptg (1).NE.0)  WRITE(6,"('e_ptg  ', 7(1PE12.4,1X))")
     &    d1d_som(d1d_eptg (2):d1d_eptg (3))
        IF(d1d_pois (1).NE.0)  WRITE(6,"('poiss  ', 2(1PE12.4,1X))")
     &    d1d_som(d1d_pois (2):d1d_pois (3))
        IF(d1d_cmx  (1).NE.0)  WRITE(6,"('c_max  ', 6(1PE12.4,1X))")
     &    d1d_som(d1d_cmx  (2):d1d_cmx  (3))

      END SELECT
      ENDIF

      RETURN
!     ______________________
      END SUBROUTINE diag_tp



      SUBROUTINE diag_poynting(pasdt,bxn,byn,bzn)
! ======================================================================
! Diagnostics scalaires effectues a chaque pas de temps
!    + Poids des differentes especes
!    + Energies cinetiques
!    + Flux d'impulsions
!    + Energies electromagnetiques
!    + Flux du vecteur de Poynting
!    + 
! Entree : fpsr = flux de Poynting source
! ======================================================================

      USE domaines
      USE domaines_klder      
      USE particules_klder
      USE champs
      USE diagnostics_klder
      USE erreurs

      IMPLICIT NONE
!!      INCLUDE 'mpif.h'

      REAL(KIND=kr), INTENT(IN) :: pasdt 
      INTEGER                   :: iesp, i
      REAL(KIND=kr)             :: vol_s2
      REAL(KIND=kr), DIMENSION(:,:,:), ALLOCATABLE :: tp_3
      REAL(KIND=kr), DIMENSION(:,:),   ALLOCATABLE :: tp_2
      REAL(KIND=kr), DIMENSION(:),     ALLOCATABLE :: tp_1

      REAL(KIND=kr), DIMENSION(nulmx:nulpx+1,nulmy:nulpy+1), 
     &      INTENT(IN) :: bxn, byn, bzn


! Diagnostics electromagnetiques :
! flux des vecteurs de Poynting
! (on compte positivement l'energie qui SORT du domaine)
! On centre les energies sur les points interieurs du maillage dual,
! et les vecteurs de Poynting sur les centres des mailles de sortie :
! cela garantit qu'il n'y a pas de recouvrements entre PEs
! ----------------------------------------------------------------------
      SELECT CASE(dim_cas)
      CASE(6)

        IF(d_psr.NE.0) d1d_loc(d_psr) =  d1d_loc(d_psr)
     &                                 + fpsr*pasdt*pasdy*pasdz
        IF((ibndc(1).LT.-1).OR.(ibndc(1).EQ.0)) d1d_loc(d_pmx)
     &  = d1d_loc(d_pmx) + 0.125_8*pasdt*pasdy*pasdz
     &   *SUM( ( ez_3 (nulmx  ,nulmy:nulpy-1,nulmz+1:nulpz)
     &          +ez_3 (nulmx  ,nulmy+1:nulpy,nulmz+1:nulpz) )
     &        *( by_3p(nulmx  ,nulmy:nulpy-1,nulmz+1:nulpz)
     &          +by_3p(nulmx  ,nulmy+1:nulpy,nulmz+1:nulpz) 
     &          +by_3p(nulmx+1,nulmy:nulpy-1,nulmz+1:nulpz)
     &          +by_3p(nulmx+1,nulmy+1:nulpy,nulmz+1:nulpz) )
     &        -( ey_3 (nulmx  ,nulmy+1:nulpy,nulmz:nulpz-1)
     &          +ey_3 (nulmx  ,nulmy+1:nulpy,nulmz+1:nulpz) )
     &        *( bz_3p(nulmx  ,nulmy+1:nulpy,nulmz:nulpz-1)
     &          +bz_3p(nulmx  ,nulmy+1:nulpy,nulmz+1:nulpz) 
     &          +bz_3p(nulmx+1,nulmy+1:nulpy,nulmz:nulpz-1)
     &          +bz_3p(nulmx+1,nulmy+1:nulpy,nulmz+1:nulpz) )
     &       )
        IF((ibndc(2).LT.-1).OR.(ibndc(2).EQ.0)) d1d_loc(d_ppx)
     &  = d1d_loc(d_ppx) + 0.125_8*pasdt*pasdy*pasdz
     &   *SUM(-( ez_3 (nulpx  ,nulmy:nulpy-1,nulmz+1:nulpz)
     &          +ez_3 (nulpx  ,nulmy+1:nulpy,nulmz+1:nulpz) )
     &        *( by_3p(nulpx  ,nulmy:nulpy-1,nulmz+1:nulpz)
     &          +by_3p(nulpx  ,nulmy+1:nulpy,nulmz+1:nulpz) 
     &          +by_3p(nulpx+1,nulmy:nulpy-1,nulmz+1:nulpz)
     &          +by_3p(nulpx+1,nulmy+1:nulpy,nulmz+1:nulpz) )
     &        +( ey_3 (nulpx  ,nulmy+1:nulpy,nulmz:nulpz-1)
     &          +ey_3 (nulpx  ,nulmy+1:nulpy,nulmz+1:nulpz) )
     &        *( bz_3p(nulpx  ,nulmy+1:nulpy,nulmz:nulpz-1)
     &          +bz_3p(nulpx  ,nulmy+1:nulpy,nulmz+1:nulpz) 
     &          +bz_3p(nulpx+1,nulmy+1:nulpy,nulmz:nulpz-1)
     &          +bz_3p(nulpx+1,nulmy+1:nulpy,nulmz+1:nulpz) )
     &       )
        IF(ibndc(3).LT.-1)                      d1d_loc(d_pmy)
     &  = d1d_loc(d_pmy) + 0.125_8*pasdt*pasdx*pasdz
     &   *SUM( ( ex_3 (nulmx+1:nulpx,nulmy  ,nulmz:nulpz-1)
     &          +ex_3 (nulmx+1:nulpx,nulmy  ,nulmz+1:nulpz) )
     &        *( bz_3p(nulmx+1:nulpx,nulmy  ,nulmz:nulpz-1)
     &          +bz_3p(nulmx+1:nulpx,nulmy  ,nulmz+1:nulpz) 
     &          +bz_3p(nulmx+1:nulpx,nulmy+1,nulmz:nulpz-1)
     &          +bz_3p(nulmx+1:nulpx,nulmy+1,nulmz+1:nulpz) )
     &        -( ez_3 (nulmx:nulpx-1,nulmy  ,nulmz+1:nulpz)
     &          +ez_3 (nulmx+1:nulpx,nulmy  ,nulmz+1:nulpz) )
     &        *( bx_3p(nulmx:nulpx-1,nulmy  ,nulmz+1:nulpz)
     &          +bx_3p(nulmx+1:nulpx,nulmy  ,nulmz+1:nulpz) 
     &          +bx_3p(nulmx:nulpx-1,nulmy+1,nulmz+1:nulpz)
     &          +bx_3p(nulmx+1:nulpx,nulmy+1,nulmz+1:nulpz) )
     &       )
        IF(ibndc(4).LT.-1)                      d1d_loc(d_ppy)
     &  = d1d_loc(d_ppy) + 0.125_8*pasdt*pasdx*pasdz
     &   *SUM(-( ex_3 (nulmx+1:nulpx,nulpy  ,nulmz:nulpz-1)
     &          +ex_3 (nulmx+1:nulpx,nulpy  ,nulmz+1:nulpz) )
     &        *( bz_3p(nulmx+1:nulpx,nulpy  ,nulmz:nulpz-1)
     &          +bz_3p(nulmx+1:nulpx,nulpy  ,nulmz+1:nulpz) 
     &          +bz_3p(nulmx+1:nulpx,nulpy+1,nulmz:nulpz-1)
     &          +bz_3p(nulmx+1:nulpx,nulpy+1,nulmz+1:nulpz) )
     &        +( ez_3 (nulmx:nulpx-1,nulpy  ,nulmz+1:nulpz)
     &          +ez_3 (nulmx+1:nulpx,nulpy  ,nulmz+1:nulpz) )
     &        *( bx_3p(nulmx:nulpx-1,nulpy  ,nulmz+1:nulpz)
     &          +bx_3p(nulmx+1:nulpx,nulpy  ,nulmz+1:nulpz) 
     &          +bx_3p(nulmx:nulpx-1,nulpy+1,nulmz+1:nulpz)
     &          +bx_3p(nulmx+1:nulpx,nulpy+1,nulmz+1:nulpz) )
     &       )
        IF(ibndc(5).LT.-1)                      d1d_loc(d_pmz)
     &  = d1d_loc(d_pmz) + 0.125_8*pasdt*pasdx*pasdy
     &   *SUM( ( ey_3 (nulmx:nulpx-1,nulmy+1:nulpy,nulmz  )
     &          +ey_3 (nulmx+1:nulpx,nulmy+1:nulpy,nulmz  ) )
     &        *( bx_3p(nulmx:nulpx-1,nulmy+1:nulpy,nulmz  )
     &          +bx_3p(nulmx+1:nulpx,nulmy+1:nulpy,nulmz  ) 
     &          +bx_3p(nulmx:nulpx-1,nulmy+1:nulpy,nulmz+1)
     &          +bx_3p(nulmx+1:nulpx,nulmy+1:nulpy,nulmz+1) )
     &        -( ex_3 (nulmx+1:nulpx,nulmy:nulpy-1,nulmz  )
     &          +ex_3 (nulmx+1:nulpx,nulmy+1:nulpy,nulmz  ) )
     &        *( by_3p(nulmx+1:nulpx,nulmy:nulpy-1,nulmz  )
     &          +by_3p(nulmx+1:nulpx,nulmy+1:nulpy,nulmz  ) 
     &          +by_3p(nulmx+1:nulpx,nulmy:nulpy-1,nulmz+1)
     &          +by_3p(nulmx+1:nulpx,nulmy+1:nulpy,nulmz+1) )
     &       )
        IF(ibndc(6).LT.-1)                      d1d_loc(d_ppz)
     &  = d1d_loc(d_ppz) + 0.125_8*pasdt*pasdx*pasdy
     &   *SUM(-( ey_3 (nulmx:nulpx-1,nulmy+1:nulpy,nulpz  )
     &          +ey_3 (nulmx+1:nulpx,nulmy+1:nulpy,nulpz  ) )
     &        *( bx_3p(nulmx:nulpx-1,nulmy+1:nulpy,nulpz  )
     &          +bx_3p(nulmx+1:nulpx,nulmy+1:nulpy,nulpz  ) 
     &          +bx_3p(nulmx:nulpx-1,nulmy+1:nulpy,nulpz+1)
     &          +bx_3p(nulmx+1:nulpx,nulmy+1:nulpy,nulpz+1) )
     &        +( ex_3 (nulmx+1:nulpx,nulmy:nulpy-1,nulpz  )
     &          +ex_3 (nulmx+1:nulpx,nulmy+1:nulpy,nulpz  ) )
     &        *( by_3p(nulmx+1:nulpx,nulmy:nulpy-1,nulpz  )
     &          +by_3p(nulmx+1:nulpx,nulmy+1:nulpy,nulpz  ) 
     &          +by_3p(nulmx+1:nulpx,nulmy:nulpy-1,nulpz+1)
     &          +by_3p(nulmx+1:nulpx,nulmy+1:nulpy,nulpz+1) )
     &       )

      CASE(5)

        IF(d_psr.NE.0) d1d_loc(d_psr) =  d1d_loc(d_psr)
     &                                 + fpsr*pasdt*pasdy


        IF((ibndc(1).LT.-1).OR.(ibndc(1).EQ.0)) d1d_loc(d_pmx)
     &  = d1d_loc(d_pmx) + pasdt*pasdy
     &   *SUM( -0.125*( eytm1(nulmx  ,nulmy+1:nulpy-1)
     &                + eytm1(nulmx+1,nulmy+1:nulpy-1)
     &                + eytm1(nulmx  ,nulmy+2:nulpy  )
     &                + eytm1(nulmx+1,nulmy+2:nulpy  ) )*
     &        ( bzn(nulmx+1,nulmy+1:nulpy-1)
     &        + bzn(nulmx+1,nulmy+2:nulpy  ) )
     &   + 0.25*( eztm1(nulmx  ,nulmy+1:nulpy-1)
     &          + eztm1(nulmx+1,nulmy+1:nulpy-1)
     &          + eztm1(nulmx  ,nulmy+2:nulpy  )
     &          + eztm1(nulmx+1,nulmy+2:nulpy  ) )*
     &          byn(nulmx+1,nulmy+1:nulpy-1)
     &       )

        IF((ibndc(2).LT.-1).OR.(ibndc(2).EQ.0)) d1d_loc(d_ppx)
     &  = d1d_loc(d_ppx) + pasdt*pasdy
     &   *SUM( 0.125*( eytm1(nulpx-1,nulmy+1:nulpy-1)
     &               + eytm1(nulpx  ,nulmy+1:nulpy-1)
     &               + eytm1(nulpx-1,nulmy+2:nulpy  )
     &               + eytm1(nulpx  ,nulmy+2:nulpy  ) )*
     &        ( bzn(nulpx,nulmy+1:nulpy-1)
     &        + bzn(nulpx,nulmy+2:nulpy  ) )
     &   - 0.25*( eztm1(nulpx-1,nulmy+1:nulpy-1)
     &          + eztm1(nulpx  ,nulmy+1:nulpy-1)
     &          + eztm1(nulpx-1,nulmy+2:nulpy  )
     &          + eztm1(nulpx  ,nulmy+2:nulpy  ) )*
     &          byn(nulpx,nulmy+1:nulpy-1)
     &       )

        IF(ibndc(3).LT.-1)                      d1d_loc(d_pmy)
     &  = d1d_loc(d_pmy) + pasdt*pasdx
     &  *SUM( -0.5*(  eztm1(nulmx+1:nulpx-1,nulmy  )
     &              + eztm1(nulmx+1:nulpx-1,nulmy+1)  )*
     &           bxn(nulmx+1:nulpx-1,nulmy+1)
     &      + 0.125*( extm1(nulmx+1:nulpx-1,nulmy  )    
     &              + extm1(nulmx+1:nulpx-1,nulmy+1)
     &              + extm1(nulmx+2:nulpx  ,nulmy  )    
     &              + extm1(nulmx+2:nulpx  ,nulmy+1) )*
     &         ( bzn(nulmx+1:nulpx-1,nulmy+1)
     &         + bzn(nulmx+2:nulpx  ,nulmy+1)) )

        IF(ibndc(4).LT.-1)                      d1d_loc(d_ppy)
     &  = d1d_loc(d_ppy) + pasdt*pasdx
     &  *SUM( 0.5*(  eztm1(nulmx+1:nulpx-1,nulpy-1)
     &             + eztm1(nulmx+1:nulpx-1,nulpy  )  )*
     &           bxn(nulmx+1:nulpx-1,nulpy )
     &      - 0.125*( extm1(nulmx+1:nulpx-1,nulpy-1)    
     &              + extm1(nulmx+1:nulpx-1,nulpy  )
     &              + extm1(nulmx+2:nulpx  ,nulpy-1)    
     &              + extm1(nulmx+2:nulpx  ,nulpy  ) )*
     &         ( bzn(nulmx+1:nulpx-1,nulpy)
     &         + bzn(nulmx+2:nulpx  ,nulpy) ) 
     &       )

      CASE(4)

        IF(d_psr.NE.0) d1d_loc(d_psr) =  d1d_loc(d_psr)
     &                                 + fpsr*pasdt*pasdy
        IF((ibndc(1).LT.-1).OR.(ibndc(1).EQ.0)) d1d_loc(d_pmx)
     &  = d1d_loc(d_pmx) - 0.5_8*pasdt*pasdy
     &    *SUM(   ey_2 (nulmx,nulmy+1:nulpy)
     &         *( bz_2p(nulmx,nulmy+1:nulpy)
     &           +bz_2p(nulmx+1,nulmy+1:nulpy)))
        IF((ibndc(2).LT.-1).OR.(ibndc(2).EQ.0)) d1d_loc(d_ppx)
     &  = d1d_loc(d_ppx) + 0.5_8*pasdt*pasdy
     &    *SUM(   ey_2 (nulpx,nulmy+1:nulpy)
     &         *( bz_2p(nulpx,nulmy+1:nulpy)
     &           +bz_2p(nulpx+1,nulmy+1:nulpy)))
        IF(ibndc(3).LT.-1)                      d1d_loc(d_pmy)
     &  = d1d_loc(d_pmy) + 0.5_8*pasdt*pasdx
     &    *SUM(   ex_2 (nulmx+1:nulpx,nulmy)
     &         *( bz_2p(nulmx+1:nulpx,nulmy)
     &           +bz_2p(nulmx+1:nulpx,nulmy+1)))
        IF((ibndc(4).LT.-1))                    d1d_loc(d_ppy)
     &  = d1d_loc(d_ppy) - 0.5_8*pasdt*pasdx
     &    *SUM(   ex_2 (nulmx+1:nulpx,nulpy)
     &         *( bz_2p(nulmx+1:nulpx,nulpy)
     &           +bz_2p(nulmx+1:nulpx,nulpy+1)))

      CASE(3)

        IF(d_psr.NE.0) d1d_loc(d_psr) =  d1d_loc(d_psr)
     &                                 + fpsr*pasdt
        IF((ibndc(1).LT.-1).OR.(ibndc(1).EQ.0)) d1d_loc(d_pmx)
     &  = d1d_loc(d_pmx) + 0.5_8*pasdt
     &   *( ez_1(nulmx) * ( by_1p(nulmx)+by_1p(nulmx+1) )
     &     -ey_1(nulmx) * ( bz_1p(nulmx)+bz_1p(nulmx+1) )  )
        IF((ibndc(2).LT.-1).OR.(ibndc(2).EQ.0)) d1d_loc(d_ppx)
     &  = d1d_loc(d_ppx) + 0.5_8*pasdt
     &   *(-ez_1(nulpx) * ( by_1p(nulpx)+by_1p(nulpx+1) )
     &     +ey_1(nulpx) * ( bz_1p(nulpx)+bz_1p(nulpx+1) )  )

      CASE(2,1)

        IF(d_psr.NE.0) d1d_loc(d_psr) =  d1d_loc(d_psr)
     &                                 + fpsr*pasdt
        IF((d_pmx.NE.0).AND.((ibndc(1).LT.-1).OR.(ibndc(1).EQ.0)))
     &    d1d_loc(d_pmx) = d1d_loc(d_pmx) - 
     &    0.5_8*pasdt * ey_1(nulmx) * (bz_1p(nulmx)+bz_1p(nulmx+1))
        IF((d_pmx.NE.0).AND.((ibndc(2).LT.-1).OR.(ibndc(2).EQ.0)))
     &    d1d_loc(d_ppx) = d1d_loc(d_ppx) +
     &    0.5_8*pasdt * ey_1(nulpx) * (bz_1p(nulpx)+bz_1p(nulpx+1))
      END SELECT


      RETURN
!     ______________________
      END SUBROUTINE diag_poynting
      
      

