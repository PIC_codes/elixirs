! ======================================================================
! grille.f
! ----------
!
!     SUBROUTINE grimax
!     SUBROUTINE griuni
!
!
! ======================================================================


       SUBROUTINE grimax
 ! ==================================================================
 !     on remet les champs  sur le maillage de maxwell
 !     c est a dire on defait le travail de griuni
 !
 ! ==================================================================

       USE domaines
       USE champs
       USE erreurs

       IMPLICIT NONE


       INTEGER     ::   i, j

 !     en (i,j+1/2)
 !     ------------
!       DO i=0,nx
         ey(0:nx,n1y)=eym (0:nx)
         bx(0:nx,n1y)=bxm (0:nx)
         bz(0:nx,n1y)=bzm2(0:nx)

         eytm1(0:nx,n1y)=eytm1m (0:nx)
!       ENDDO

       DO j=ny,0,-1
!         write(fmsgo,"('j : ', I4)") j
!         write(fmsgo,"('ey(0:100, j  ) : ', 101(F16.4,1X))") ey(0:nx, j)
!         write(fmsgo,"('ey(0:100, j+1) : ',
!     &                    101(F16.4,1X))") ey(0:nx, j+1)
!         write(fmsgo,"('avant')")
          ey(0:nx,j)=2.*ey(0:nx,j)-ey(0:nx,j+1)
          bx(0:nx,j)=2.*bx(0:nx,j)-bx(0:nx,j+1)
          bz(0:nx,j)=2.*bz(0:nx,j)-bz(0:nx,j+1)

          eytm1(0:nx,j)=2.*eytm1(0:nx,j)-eytm1(0:nx,j+1)
!         write(fmsgo,"('apres grimax')")
!         write(fmsgo,"('ey(0:100, j  ) : ', 101(F16.4,1X))") ey(0:nx, j)
       ENDDO

 !     en (i+1/2,j)
 !     ------------
       DO j=0,n1y
         ex(nx,j)=exm(j)
         by(nx,j)=bym(j)
         bz(nx,j)=bzm1(j)

         extm1(nx,j)=extm1m(j)
       ENDDO

       DO i=nxm1,1,-1
         DO j=0,n1y
          ex(i,j)=2.*ex(i,j)-ex(i+1,j)
          by(i,j)=2.*by(i,j)-by(i+1,j)
          bz(i,j)=2.*bz(i,j)-bz(i+1,j)

          extm1(i,j)=2.*extm1(i,j)-extm1(i+1,j)
         ENDDO
       ENDDO
 !
 !     mise des courants sur la grille de maxwel
 !
       DO j=0,n1y
         DO i=nx,1,-1
          jxtot(i,j)=0.5*(jxtot(i,j)+jxtot(i-1,j))
         ENDDO
       ENDDO
 !
       DO i=0,nx
         DO j=n1y,1,-1
          jytot(i,j)=0.5*(jytot(i,j)+jytot(i,j-1))
         ENDDO
         jytot(i,0)=jytot(i,ny)
       ENDDO
 !
       return

!      _____________________
       END SUBROUTINE grimax



      SUBROUTINE griuni
 ! ==================================================================
 !     On remet les champs sur la grille primale (i,j)
 !
 ! ==================================================================
       USE domaines
       USE champs

       IMPLICIT NONE

       INTEGER     ::   i, j


       DO j=0,n1y
         DO i=1,nxm1
          ex(i,j)   =0.5*(ex(i,j)   +ex(i+1,j))
          extm1(i,j)=0.5*(extm1(i,j)+extm1(i+1,j))
          by(i,j)   =0.5*(by(i,j)   +by(i+1,j))
          bz(i,j)   =0.5*(bz(i,j)   +bz(i+1,j))
         ENDDO
       ENDDO
 !
 !     stockage de la valeur en n1x et periodicite
 !
       DO j=0,n1y
         exm(j)  =ex(nx,j)
         bym(j)  =by(nx,j)
         bzm1(j) =bz(nx,j)
         extm1m(j)=extm1(nx,j)
 !
 !     on remet les champs extrapoles en nx
 !
         ex(nx,j)=ex(n1x,j)
         by(nx,j)=by(n1x,j)
         bz(nx,j)=bz(n1x,j)
         extm1(nx,j)=extm1(n1x,j)
       ENDDO
 !
 !
       DO i=0,nx
         DO j=0,ny
          ey(i,j)   =0.5*(ey(i,j)   +ey(i,j+1))
          eytm1(i,j)=0.5*(eytm1(i,j)+eytm1(i,j+1))
          bx(i,j)   =0.5*(bx(i,j)   +bx(i,j+1))
          bz(i,j)   =0.5*(bz(i,j)   +bz(i,j+1))
         ENDDO
       ENDDO

!       DO i=0,nx
         eym (0:nx)  = ey(0:nx,n1y)
         bxm (0:nx)  = bx(0:nx,n1y)
         bzm2(0:nx)  = bz(0:nx,n1y)
         ey(0:nx,n1y)= ey(0:nx,1)
         bx(0:nx,n1y)= bx(0:nx,1)
         bz(0:nx,n1y)= bz(0:nx,1)

         eytm1m (0:nx)  = eytm1(0:nx,n1y)
         eytm1(0:nx,n1y)= eytm1(0:nx,1)

!       ENDDO

       return
!      _____________________
       END SUBROUTINE griuni
