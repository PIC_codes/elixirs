! ======================================================================
! maxwell_iter.f
! ----------
!
!
!     SUBROUTINE Init_iter
!     SUBROUTINE pot2d_iter
!
!
! ======================================================================






       SUBROUTINE Init_iter
! ======================================================================
! 
! 
!
!
!
! ======================================================================       
       
       USE champs
       USE champs_iter
       USE domaines
       USE temps
       USE particules
       USE particules_klder
       USE erreurs
 
 
       IMPLICIT NONE
 
       INTEGER    ::   i, j, iesp
       INTEGER    ::   ip

       INTEGER(KIND=kr), DIMENSION(1:ordre_interp+1)  :: 
     &    ipx, ipy, idx, idy
       REAL(KIND=kr), DIMENSION(1:ordre_interp+1)     :: 
     &    ponpx, ponpy, pondx, pondy

       REAL(kind=kr), DIMENSION(1:3,1:3)     :: 
     &  rotmat, identity, chimat
     
       REAL(kind=kr), DIMENSION(1:3)     :: 
     &   dv0, vtempe

       REAL(kind=kr)   ::  rqm, aux_rh       
       REAL(kind=kr)   :: tbx, tby, tbz, teta2


       identity(1,1)=1. ; identity(1,2)=0. ; identity(1,3)=0.
       identity(2,1)=0. ; identity(2,2)=1. ; identity(2,3)=0.       
       identity(3,1)=0. ; identity(3,2)=0. ; identity(3,3)=1. 


       rho_esp2_ik(:,:,:)=0.
       xxx_ik(:,:)=0.; xxy_ik(:,:)=0.; xxz_ik(:,:)=0.
       xyx_ik(:,:)=0.; xyy_ik(:,:)=0.; xyz_ik(:,:)=0.
       xzx_ik(:,:)=0.; xzy_ik(:,:)=0.; xzz_ik(:,:)=0.       

! ---- Debut boucle sur les especes 
       DO iesp=1, nesp_p

       rqm = charge(iesp)/masse(iesp)

       DO ip=1,nbpa(iesp)  

! ----- champ corrige a t_n
!       Pond�rations et indices calcul�s � partir de \tildeX(n+1), 
!       on utilise le tableau partic(:,:,:)
! ----------------------------------
        SELECT CASE(ordre_interp)
        CASE(1)
         CALL ponder_ordre1(ip, iesp, ponpx, ponpy, ipx, ipy)
         CALL ponder_ordre1_dual(ip, iesp, pondx, pondy, idx, idy)

        CASE(2)
         CALL ponder_ordre2(ip, iesp, ponpx, ponpy, ipx, ipy)
         CALL ponder_ordre2_dual(ip, iesp, pondx, pondy, idx, idy)

        CASE(3)
         CALL ponder_ordre3(ip, iesp, ponpx, ponpy, ipx, ipy)
         CALL ponder_ordre3_dual(ip, iesp, pondx, pondy, idx, idy)

        CASE(4)
         CALL ponder_ordre4(ip, iesp, ponpx, ponpy, ipx, ipy)
         CALL ponder_ordre4_dual(ip, iesp, pondx, pondy, idx, idy)

         CASE DEFAULT
         WRITE(fmsgo,*) 'pousseurs.f : mauvaise valeur ordre_interp'
       END SELECT

       CALL champa_2d_exyztm1_gridmax(vtempe(1), vtempe(2), vtempe(3),  
     &            ponpx, ponpy, pondx, pondy, ipx, ipy, idx, idy)

! ----- variables matrice de rotation
       tbx = 0.5*rqm*dt*partic(a_bxp,ip,iesp)
     &    /partic(a_gatld,ip,iesp)
       tby = 0.5*rqm*dt*partic(a_byp,ip,iesp)
     &    /partic(a_gatld,ip,iesp)
       tbz = 0.5*rqm*dt*partic(a_bzp,ip,iesp)
     &    /partic(a_gatld,ip,iesp)
         
       teta2 = tbx**2 + tby**2 + tbz**2       

       rotmat(1,1) = 1-teta2+2*tbx**2
       rotmat(1,2) = 2*tbz + 2*tbx*tby
       rotmat(1,3) = -2*tby +2*tbx*tbz

       rotmat(2,1) = 2*tbx*tby - 2*tbz
       rotmat(2,2) = 1-teta2 + 2*tby**2 
       rotmat(2,3) = 2*tby*tbz + 2*tbx

       rotmat(3,1) = 2*tby + 2*tbx*tbz
       rotmat(3,2) = 2*tby*tbz - 2*tbx 
       rotmat(3,3) = 1-teta2 + 2*tbz**2
       
       rotmat(:,:) = 1/(1+teta2) * rotmat(:,:)

!     determination de deltaV(0)
! -----------------------------------
       dv0(1:3) = matmul(identity+rotmat,vtempe) 

!     Initialisation positions it�ration 0
! --------------------------------
       partic_ik(a_qx_ik,ip,iesp) = partic(a_qx,ip,iesp)
     &                          + 0.25*rqm*dt**2*dv0(1)

       partic_ik(a_qy_ik,ip,iesp) = partic(a_qy,ip,iesp)
     &                          + 0.25*rqm*dt**2*dv0(2)

       aux_rh = partic(a_po,ip,iesp)
       
!
!      conditions aux limites simplifi�es, on se contente d assurer  
!        la p�riodicit� suivant y
!      ------------------------------------------
       IF(partic_ik(a_qy_ik,ip,iesp).GE.bord(4,numproc)) THEN
         partic_ik(a_qy_ik,ip,iesp) =  
     &        partic_ik(a_qy_ik,ip,iesp) -ylong
       ELSEIF(partic_ik(a_qy_ik,ip,iesp).LE.bord(3,numproc)) THEN
         partic_ik(a_qy_ik,ip,iesp) =  
     &        partic_ik(a_qy_ik,ip,iesp) +ylong	 
       ENDIF  

 !
 !     calcul de rhoe en x (k+1) 
 !     atention : on utilise le tableau partic_ik, les routines 
 !     ponder_ordre1 et ponder_ordre2 sont remplac�es par 
 !     ponder_ordre1_ik et ponder_ordre2_ik
 !     ---------------------------------------------
       SELECT CASE(ordre_interp)
         CASE(1)  
         CALL ponder_ordre1_ik(ip, iesp, ponpx, ponpy, ipx, ipy)
       
         CASE(2)
         CALL ponder_ordre2_ik(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(3)
         CALL ponder_ordre3_ik(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(4)
         CALL ponder_ordre4_ik(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE DEFAULT
         WRITE(fmsgo,*) 'pousseurs.f : ordre_interp incorrecte'
       END SELECT
       
!      projection de la densite de charge de l espece iesp dans le tableau 
!      rho_esp2_ik(:,:,iesp)
! ------------------------------------       
       CALL projpa_2d_rho_ik(iesp, ponpx, ponpy,   
     &                ipx, ipy, aux_rh)
      
! ----- détermination des tenseurs de susceptibilité pour la paricule n
       chimat(1:3,1:3) = Identity+rotmat
       chimat(:,:) = 0.25*charge(iesp)**2/masse(iesp)*dt**2 *chimat(:,:)
     
! ----- on projette la contribution de la particule dans les tableaux :
!       xxx_ik(:,:), xxy_ik(:,:), xxz_ik(:,:)
!       xyx_ik(:,:), xyy_ik(:,:), xyz_ik(:,:)
!       xzx_ik(:,:), xzy_ik(:,:), xzz_ik(:,:)
!     
       CALL chi_ik(ponpx, ponpy, ipx, ipy, 
     &            chimat, iesp, partic(a_po,ip,iesp))     
       
! ---- Fin boucle sur les particules
       ENDDO       
! ------- normalisation provenant de la définition de partic(a_po_ip,iesp)	 
       rho_esp2_ik(:,:,iesp) = rho_esp2_ik(:,:,iesp)
     &                         *dxi*dyi*charge(iesp)

! ---- Fin boucle sur les especes 
       ENDDO


!      Sommation des contributions sur les bords et 
!      conditions aux limites périodiques
! --------------------------------------------------
       CALL edge_rho_ik
       IF(ALL(dynam(1:nesp_p).EQ.2)) THEN
         CALL edge_chi_ik_rel
         CALL period_chi_ik_rel
       ENDIF

       DO iesp=1,nesp_p
         CALL period_rho_ik(iesp)
       ENDDO

!      On initialise le terme source avec la densite 
!      issue de l iteration 0
! ---------------------------------------------------
        s(:,:)=0.
        DO  j=0,n1y
          DO  i=0,n1x
            DO iesp=1,nesp_p
             s(i,j)= s(i,j) + rho_esp2_ik(i,j,iesp)
           ENDDO
	  ENDDO
        ENDDO       

!
!     calcul de la suceptibilite du plasma
!     ------------------------------------
       IF(ALL(dynam(1:nesp_p).EQ.1)) THEN       
! ----- toutes especes particules classiques
         WRITE(fmsgo,"('Dynamique classique non ', 
     &           'prise en charge : arret de l execution')")  
         STOP
       ELSEIF(ALL(dynam(1:nesp_p).EQ.2)) THEN
! ----- toutes espèces particules relativistes
         CALL chi_ik_rel_means
       ELSE
!       dynamique mixte non prise en charge pour le moment : 
!         arret de l execution 
! ---------------------------------------------------------------
         WRITE(fmsgo,"('Les deux especes de particules n ont ', 
     &           'pas la meme dynamique : arret de l execution')")  
         STOP
       ENDIF   

!
!      Calcul de -div[(I+chi)E_{n+1}]
! -----------------------------------------
       src_max(:,:)=0.
       DO j=1,ny
         DO i=1,nxm1

           src_max(i,j)=  -dxi*((1.+xxx_ik(i+1,j))*ex(i+1,j)                             
     &                    -(1.+xxx_ik(i,j))*ex(i,j))                                
     &       -dxf4i*( xxy_ik(i+1,j) *(ey(i+1,j+1)+ey(i+1,j))           
     &               -xxy_ik(i-1,j) *(ey(i-1,j+1)+ey(i-1,j)) )                 
     &       -dxf2i*( xxz_ik(i+1,j)*ez(i+1,j)-xxz_ik(i-1,j)*ez(i-1,j) )                 
     &       -dyf4i*( xyx_ik(i,j+1) *(ex(i+1,j+1)+ex(i,j+1))          
     &               -xyx_ik(i,j-1) *(ex(i+1,j-1)+ex(i,j-1)) ) 
     &       -dyf2i*( xyz_ik(i,j+1)*ez(i,j+1)-xyz_ik(i,j-1)*ez(i,j-1) )                 
     &       -dyi *((1.+xyy_ik(i,j+1))*ey(i,j+1)
     &             -(1.+xyy_ik(i,j))*ey(i,j))

         s(i,j)= s(i,j)+src_max(i,j)

         ENDDO
       ENDDO

!
!      Initialisation de e[xy]2_ik(:,:), deux composantes �lectrostatiques en 
!      deux dimensions d espace
! --------------------------------------------------------
       ex2_ik(:,:)=extm1(:,:)
       ey2_ik(:,:)=eytm1(:,:)
!       ez2_ik(:,:)=ez(:,:)


       
       return       
!      _____________________       
       END SUBROUTINE Init_iter


       
       SUBROUTINE pot2d_iter
! ======================================================================
! 
! 
!
!
!
! ======================================================================       
       
       USE champs
       USE champs_iter
       USE domaines
       USE particules
       USE particules_klder
       USE temps
       USE erreurs
 
 
       IMPLICIT NONE
 
 !     version avec des conditions aux limites
 !     ouverte en x periodique en y
 !     en x phi(0)=phi(nx)=0.(phi=0. sur les parois)
 

       REAL(kind=kr), DIMENSION(nxm1f2)    ::  aux
       REAL(kind=kr), DIMENSION(7,nxm1f2)  ::  a
       REAL(kind=kr), DIMENSION(3,nxm1f2)  ::  al
       REAL(kind=kr), DIMENSION(nx,2)      ::  axx
       REAL(kind=kr), DIMENSION(nxm1f2)    ::  in
      
       REAL(kind=kr), DIMENSION(1:n1x)    ::   c1, c2, c3, c4 
       REAL(kind=kr), DIMENSION(0:n1x, 0:n1y, 1:nesp_p)    ::   
     &            rho_ik_save

       REAL(kind=4), DIMENSION(0:n1x,0:n1y)  ::   worktab1

!       REAL(kind=4), DIMENSION(1:n1x)   ::  wkin1 
!       REAL(kind=4), DIMENSION(1:nx,2)  ::  wkin2
!       REAL(kind=4), DIMENSION(0:n1x, 0:n1y)  ::  wkin3, wkout

       REAL(kind=kr)     :: dband_a, sr, tz
 
       INTEGER    :: ky, iter_golub, jk, il, ik, m1, m2, i7
!       INTEGER    ::  npx

       INTEGER    ::   i, j, iesp
       INTEGER    ::   ip

       INTEGER(KIND=kr), DIMENSION(1:ordre_interp+1)  :: 
     &    ipx, ipy, idx, idy 
       REAL(KIND=kr), DIMENSION(1:ordre_interp+1)     :: 
     &    ponpx, ponpy, pondx, pondy  

       REAL(kind=kr), DIMENSION(1:3,1:3)     :: 
     &  rotmat, identity, chimat
     
       REAL(kind=kr), DIMENSION(1:3)     :: 
     &   dv0, vtempe

       REAL(kind=kr)   ::  rqm, aux_rh       
       REAL(kind=kr)   :: tbx, tby, tbz, teta2

       REAL(kind=kr)   ::    epsik, max_rho_ik
       INTEGER      ::   nik


       identity(1,1)=1. ; identity(1,2)=0. ; identity(1,3)=0.
       identity(2,1)=0. ; identity(2,2)=1. ; identity(2,3)=0.       
       identity(3,1)=0. ; identity(3,2)=0. ; identity(3,3)=1. 

              
       write(fmsgo,*) ' pot : xxx0,xxy0,xyx0,xyy0 = ',       
     &       xxx0_ik(nxs2),xxy0_ik(nxs2),xyx0_ik(nxs2),xyy0_ik(nxs2)
 
 
       nik=1
       epsik=1.

!      d�but de la m�thode it�rative
!
! --------------------------------------
       DO WHILE(epsik.GT.epsik0 .AND. nik.LE.nik0)


       rho_ik_save(0:n1x,0:n1y,1:nesp_p)=
     &                  rho_esp2_ik(0:n1x,0:n1y,1:nesp_p)

 
       phi=0.
       write(fmsgo,*) 'pot : nxm1f2,nxm1s2 : ',nxm1f2,nxm1s2
 !     calcul des coefficients de la matrice
 !
       DO i=1,nx
         c1(i)=(1.+xxx0_ik(i))*dx2i
         c2(i)=(1.+xyy0_ik(i))*dy2i*2
         c3(i)=(xxy0_ik(i+1)+xyx0_ik(i))*dxdyi4*2.
         c4(i)=(xxy0_ik(i-1)+xyx0_ik(i))*dxdyi4*2.
       ENDDO
 !
 !     calcul du maximun de s
 !
       sr=0.
       DO j=1,ny
         DO i=1,nxm1
         sr=max(sr,abs(s(i,j)))
         ENDDO
       ENDDO
 !
 !     calcul du terme source de la methode iterative pour le demarrage
 
       DO  j=1,ny
         DO  i=1,nxm1
        z(i,j)=+dx2i*((xxx_ik(i+1,j)-xxx0_ik(i+1))*(phi(i+1,j)-phi(i,j))         
     &               -(xxx_ik(i,j)-xxx0_ik(i))*(phi(i,j)-phi(i-1,j)))            
     &         +dy2i*((xyy_ik(i,j+1)-xyy0_ik(i))*(phi(i,j+1)-phi(i,j))           
     &               -(xyy_ik(i,j)-xyy0_ik(i))*(phi(i,j)-phi(i,j-1)))            
     &    +dxdyi4*
     &       ( (xxy_ik(i+1,j)-xxy0_ik(i+1))*(phi(i+1,j+1)-phi(i+1,j-1))      
     &        -(xxy_ik(i-1,j)-xxy0_ik(i-1))*(phi(i-1,j+1)-phi(i-1,j-1))      
     &        +(xyx_ik(i,j+1)-xyx0_ik(i))*(phi(i+1,j+1)-phi(i-1,j+1))        
     &        -(xyx_ik(i,j-1)-xyx0_ik(i))*(phi(i+1,j-1)-phi(i-1,j-1)))   
     &        +dxi*(xxx_ik(i+1,j)*ex2_ik(i+1,j)-xxx_ik(i,j)*ex2_ik(i,j)) 
     &        +dxf4i*( xxy_ik(i+1,j) *(ey2_ik(i+1,j+1)+ey2_ik(i+1,j))           
     &                 -xxy_ik(i-1,j) *(ey2_ik(i-1,j+1)+ey2_ik(i-1,j)) )  
     &          +dyf4i*(xyx_ik(i,j+1)*(ex2_ik(i+1,j+1)+ex2_ik(i,j+1))          
     &                 -xyx_ik(i,j-1) *(ex2_ik(i+1,j-1)+ex2_ik(i,j-1)) )               
     &    +dyi*(xyy_ik(i,j+1)*ey2_ik(i,j+1)-xyy_ik(i,j)*ey2_ik(i,j))    
     &         +s(i,j)
         ENDDO
       ENDDO

!     transformee de fourier du z
!       call cpftv(z(1,1),z(2,1),ny,n2x,-1.,2,nxm1s2,cwk)
!       call rpft2v(z(1,1),z(2,1),ny,n2x,2,nxm1s2)

       worktab1=real(z,4) 
       call cpftv(worktab1(1,1),worktab1(2,1),ny,n2x,-1.,2,nxm1s2,cwk)
       call rpft2v(worktab1(1,1),worktab1(2,1),ny,n2x,2,nxm1s2)
       z=0.
       z=real(worktab1,8)
       
       DO iter_golub=1,nbpot  !     boucle sur les iterations
      
 !     calcul du mode 0 et nys2 par resolution d'une matrice
 !     tridiagonale

       DO  i=1,nxm1
         axx(i,1)=c1(i)+c1(i+1)
         axx(i,2)=c1(i)+c1(i+1)-c2(i)*csym1(nys2p1)
       ENDDO

! --- affichage phi mode 0, phi mode nys2 avant tridia
       call tridia(c1,axx(1,1),phi(1,1),z(1,1))
       call tridia(c1,axx(1,2),phi(1,nys2p1),z(1,nys2p1))
       
 !     on range les coefficients de la matrice dans a sous forme bande
 
 !     la premiere inconnue est phi(1)
 !     les quatres premieres lignes sont differentes des autres
  
       jk=n1y

       DO ky=2,nys2   
         jk=jk-1
         a=0.
         al=0.
 
 !     les deux premieres lignes (phi(0)=0.)
         il=1
         a(4,1)= c1(il+1)+c1(il)-c2(il)*csym1(ky)
         a(6,1)=-c1(il+1)
         a(7,1)= c3(il)*sny(ky)
         a(4,2)= a(4,1)
         a(5,2)=-a(7,1)
         a(6,2)= a(6,1)
 !
 !     les deux dernieres lignes (phi(nx)=0.))
         il=nxm1
         a(2,nxm1f2-1)=-c1(il)
         a(3,nxm1f2-1)=-c4(il)*sny(ky)
         a(4,nxm1f2-1)= c1(il)+c1(il+1)-c2(il)*csym1(ky)
         a(1,nxm1f2  )=-a(3,nxm1f2-1)
         a(2,nxm1f2  )=-c1(il)
         a(4,nxm1f2  )= a(4,nxm1f2-1)
 
         il=1
         DO i=3,nxm1f2-2,2    !     lignes generales
           il=il+1
           a(2,i)=-c1(il)
           a(3,i)=-c4(il)*sny(ky)
           a(4,i)= c1(il)+c1(il+1)-c2(il)*csym1(ky)
           a(6,i)=-c1(il+1)
           a(7,i)= c3(il)*sny(ky)
 
           a(1,i+1)=-a(3,i)
           a(2,i+1)=-c1(il)
           a(4,i+1)= a(4,i)
           a(5,i+1)=-a(7,i)
           a(6,i+1)=-c1(il+1)     
         ENDDO

 !     terme source dans aux
 !     --------------------
         ik=-1
         DO  i=1,nxm1
           ik=ik+2
           aux(ik)=z(i,ky)
           aux(ik+1)=z(i,jk)
         ENDDO

         m1=3
         m2=3
         i7=7
	 
         call bandec(a,nxm1f2,m1,m2,i7,al,m1,in,dband_a)
         call banbks(a,nxm1f2,m1,m2,i7,al,m1,in,aux)
      
 !     on range phi au bon point 
         ik=-1
         DO i=1,nxm1
           ik=ik+2
           phi(i,ky)=aux(ik)
           phi(i,jk)=aux(ik+1)
         ENDDO
 
 ! --- fin de la boucle sur les modes ky
       ENDDO

 !     transformee fourier inverse de phi
!       call rpf2iv(phi(1,1),phi(2,1),ny,n2x,2,nxm1s2)
!       call cpftv(phi(1,1),phi(2,1),ny,n2x,1.,2,nxm1s2,cwk)
 
       worktab1=real(phi,4)
       call rpf2iv(worktab1(1,1),worktab1(2,1),ny,n2x,2,nxm1s2)
       call cpftv(worktab1(1,1),worktab1(2,1),ny,n2x,1.,2,nxm1s2,cwk)
       phi= 0.
       phi=real(worktab1,8)
 
 !     normalisation du phi et applications des conditions aux limites

       phi=phi*anpot

 !     conditions aux limites
 
       phi(0,1:ny)=0.
       phi(nx,1:ny)=0.

 
 !     periodicite du potentiel en y
 !     ------------------------
       phi(0:nx,0)=phi(0:nx,ny)
       phi(0:nx,n1y)=phi(0:nx,1)

 !     calcul du residu
 !
       DO j=1,ny
         DO i=1,nxm1
         zeta(i,j)=+dx2i*((1.+xxx_ik(i+1,j))*(phi(i+1,j)-phi(i,j))           
     &                   -(1.+xxx_ik(i  ,j))*(phi(i,j)-phi(i-1,j)))            
     &            +dy2i*((1.+xyy_ik(i,j+1))*(phi(i,j+1)-phi(i,j))            
     &                 -(1. +xyy_ik(i,j  ))*(phi(i,j)-phi(i,j-1)))           
     &            +dxdyi4*(  xxy_ik(i+1,j )*(phi(i+1,j+1)-phi(i+1,j-1))      
     &                      -xxy_ik(i-1,j )*(phi(i-1,j+1)-phi(i-1,j-1))      
     &                      +xyx_ik(i ,j+1)*(phi(i+1,j+1)-phi(i-1,j+1))      
     &                      -xyx_ik(i ,j-1)*(phi(i+1,j-1)-phi(i-1,j-1)))   
     &        +dxi*(xxx_ik(i+1,j)*ex2_ik(i+1,j)-xxx_ik(i,j)*ex2_ik(i,j)) 
     &        +dxf4i*( xxy_ik(i+1,j) *(ey2_ik(i+1,j+1)+ey2_ik(i+1,j))           
     &                -xxy_ik(i-1,j) *(ey2_ik(i-1,j+1)+ey2_ik(i-1,j)) )                 
     &      +dyf4i*(xyx_ik(i,j+1)*(ex2_ik(i+1,j+1)+ex2_ik(i,j+1))          
     &               -xyx_ik(i,j-1) *(ex2_ik(i+1,j-1)+ex2_ik(i,j-1)) ) 
     &      +dyi*(xyy_ik(i,j+1)*ey2_ik(i,j+1)-xyy_ik(i,j)*ey2_ik(i,j)) 
     &         +s(i,j)
         ENDDO
       ENDDO


       tz=maxval(abs(zeta(1:nxm1,1:ny)))
       IF(sr.GT.1e-8) tz=tz/sr

       if(tz.lt.epspot) goto 2000
       write(fmsgo,*) iter_golub,'   tz = ',tz,'   sr = ',sr
 
 !     calcul du terme source pour l iteration suivante
 !
 !
 !     transformee de fourier du z
 !
!       call cpftv (zeta(1,1),zeta(2,1),ny,n2x,-1.,2,nxm1s2,cwk)
!       call rpft2v(zeta(1,1),zeta(2,1),ny,n2x,2,nxm1s2)

       worktab1=real(zeta,4)
       call cpftv (worktab1(1,1),worktab1(2,1),ny,n2x,-1.,2,nxm1s2,cwk)
       call rpft2v(worktab1(1,1),worktab1(2,1),ny,n2x,2,nxm1s2)
       zeta= 0.
       zeta=real(worktab1,8)
 
       DO ky=1,ny
         z(1:nxm1,ky)=z(1:nxm1,ky)+zeta(1:nxm1,ky)
       ENDDO

! --- fin de la boucle itérative Concus et Golub   
       ENDDO

       write(fmsgo,*) 'pas'
       
2000   continue
       write(fmsgo,*) ' '
       write(fmsgo,*) 'convergence en ',iter_golub,
     &     ' iterations, eps=',tz
       write(fmsgo,*) ' '
       
! --- Mise à jour ex2_ik(:,:)
       DO j=1,ny
         DO i=1,nx
           ex2_ik(i,j)=-dxi*(phi(i,j)-phi(i-1,j))
           ey2_ik(i,j)=-dyi*(phi(i,j)-phi(i,j-1))
         ENDDO
       ENDDO

 !
 !     conditions de periodicite des elements modifies
         ex2_ik(1:nx,0)  = ex2_ik(1:nx,ny)
         ex2_ik(1:nx,n1y)= ex2_ik(1:nx,1)
         ey2_ik(1:nx,0)  = ey2_ik(1:nx,ny)
         ey2_ik(1:nx,n1y)= ey2_ik(1:nx,1)


       rho_esp2_ik(:,:,:)=0.
       xxx_ik(:,:)=0.; xxy_ik(:,:)=0.; xxz_ik(:,:)=0.
       xyx_ik(:,:)=0.; xyy_ik(:,:)=0.; xyz_ik(:,:)=0.
       xzx_ik(:,:)=0.; xzy_ik(:,:)=0.; xzz_ik(:,:)=0.    

! ---- mise à jour des positions des particules
! ---- mise à jour de rho_esp_ik et chi_ik

! ---- Debut boucle sur les especes 
       DO iesp=1, nesp_p

       rqm = charge(iesp)/masse(iesp)

       DO ip=1,nbpa(iesp)  

! ----- Indices et ponderations pour la paritucle en X(k), on utilise 
!       le tableau partic_ik(:,:,:)
! 
        SELECT CASE(ordre_interp)
        CASE(1)
         CALL ponder_ordre1_ik(ip, iesp, ponpx, ponpy, ipx, ipy)
         CALL ponder_ordre1_ik_dual(ip, iesp, pondx, pondy, idx, idy)

        CASE(2)
         CALL ponder_ordre2_ik(ip, iesp, ponpx, ponpy, ipx, ipy)
         CALL ponder_ordre2_ik_dual(ip, iesp, pondx, pondy, idx, idy)

        CASE(3)
         CALL ponder_ordre3_ik(ip, iesp, ponpx, ponpy, ipx, ipy)
         CALL ponder_ordre3_ik_dual(ip, iesp, pondx, pondy, idx, idy)

        CASE(4)
         CALL ponder_ordre4_ik(ip, iesp, ponpx, ponpy, ipx, ipy)
         CALL ponder_ordre4_ik_dual(ip, iesp, pondx, pondy, idx, idy)

         CASE DEFAULT
         WRITE(fmsgo,*) 'pousseurs.f : mauvaise valeur ordre_interp'
       END SELECT

! ---- On calcule le champ vu par la particule en X(k)
       CALL champa_2d_epart_ik_gridmax(vtempe(1), vtempe(2), vtempe(3),  
     &      ponpx, ponpy, pondx, pondy, ipx, ipy, idx, idy)

! ----- variables matrice de rotation
       tbx = 0.5*rqm*dt*partic(a_bxp,ip,iesp)
     &    /partic(a_gatld,ip,iesp)
       tby = 0.5*rqm*dt*partic(a_byp,ip,iesp)
     &    /partic(a_gatld,ip,iesp)
       tbz = 0.5*rqm*dt*partic(a_bzp,ip,iesp)
     &    /partic(a_gatld,ip,iesp)
         
       teta2 = tbx**2 + tby**2 + tbz**2       

       rotmat(1,1) = 1-teta2+2*tbx**2
       rotmat(1,2) = 2*tbz + 2*tbx*tby
       rotmat(1,3) = -2*tby +2*tbx*tbz

       rotmat(2,1) = 2*tbx*tby - 2*tbz
       rotmat(2,2) = 1-teta2 + 2*tby**2 
       rotmat(2,3) = 2*tby*tbz + 2*tbx

       rotmat(3,1) = 2*tby + 2*tbx*tbz
       rotmat(3,2) = 2*tby*tbz - 2*tbx 
       rotmat(3,3) = 1-teta2 + 2*tbz**2
       
       rotmat(:,:) = 1/(1+teta2) * rotmat(:,:)

!     determination de deltaV(0)
! -----------------------------------
       dv0(1:3) = matmul(identity+rotmat,vtempe) 

       partic_ik(a_qx_ik,ip,iesp) = partic(a_qx,ip,iesp)
     &                          + 0.25*rqm*dt**2*dv0(1)

       partic_ik(a_qy_ik,ip,iesp) = partic(a_qy,ip,iesp)
     &                          + 0.25*rqm*dt**2*dv0(2)

       aux_rh = partic(a_po,ip,iesp)
       
!
!      conditions aux limites simplifi�es, on se contente d assurer  
!        la p�riodicit� suivant y
!      ------------------------------------------
       IF(partic_ik(a_qy_ik,ip,iesp).GE.bord(4,numproc)) THEN
         partic_ik(a_qy_ik,ip,iesp) =  
     &        partic_ik(a_qy_ik,ip,iesp) -ylong
       ELSEIF(partic_ik(a_qy_ik,ip,iesp).LE.bord(3,numproc)) THEN
         partic_ik(a_qy_ik,ip,iesp) =  
     &        partic_ik(a_qy_ik,ip,iesp) +ylong	 
       ENDIF    

 !
 !     calcul de rhoe en x (k+1) 
 !     ---------------------------------------------
       SELECT CASE(ordre_interp)
         CASE(1)  
         CALL ponder_ordre1_ik(ip, iesp, ponpx, ponpy, ipx, ipy)
       
         CASE(2)
         CALL ponder_ordre2_ik(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(3)
         CALL ponder_ordre3_ik(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE(4)
         CALL ponder_ordre4_ik(ip, iesp, ponpx, ponpy, ipx, ipy)

         CASE DEFAULT
         WRITE(fmsgo,*) 'pousseurs.f : ordre_interp incorrecte'
       END SELECT
       
       CALL projpa_2d_rho_ik(iesp, ponpx, ponpy,   
     &                ipx, ipy, aux_rh)
      
! ----- tensuer de susceptibilite particule n
       chimat(1:3,1:3) = Identity+rotmat
       chimat(:,:) = 0.25*charge(iesp)**2/masse(iesp)*dt**2 *chimat(:,:)
     
! ----- Contribution de la particule aux tableaux :
!       xxx(:,:), xxy(:,:), xxz(:,:)
!       xyx(:,:), xyy(:,:), xyz(:,:)
!       xzx(:,:), xzy(:,:), xzz(:,:)
!
       CALL chi_ik(ponpx, ponpy, ipx, ipy, 
     &            chimat, iesp, partic(a_po,ip,iesp))     
       
! ---- Fin boucle sur les particules
       ENDDO       
! ------- normalisation provenant de la définition de partic(a_po_ip,iesp)	 
       rho_esp2_ik(:,:,iesp) = rho_esp2_ik(:,:,iesp)
     &                         *dxi*dyi*charge(iesp)

! ---- Fin boucle sur les especes 
       ENDDO


!      Sommation des contributions sur les bords et 
!      conditions aux limites périodiques
! --------------------------------------------------
       CALL edge_rho_ik
       IF(ALL(dynam(1:nesp_p).EQ.2)) THEN
         CALL edge_chi_ik_rel
         CALL period_chi_ik_rel
       ENDIF

       DO iesp=1,nesp_p
         CALL period_rho_ik(iesp)
       ENDDO

!      On initialise le terme source avec la densite 
!      issue de l iteration 0
! ---------------------------------------------------
        s(:,:)=0.
        DO  j=0,n1y
          DO  i=0,n1x
            DO iesp=1,nesp_p
              s(i,j)= s(i,j) + rho_esp2_ik(i,j,iesp)
            ENDDO
            s(i,j)= s(i,j)+src_max(i,j)
	 
	  ENDDO
        ENDDO       

!
!     calcul de la suceptibilite du plasma
!     ------------------------------------
       IF(ALL(dynam(1:nesp_p).EQ.1)) THEN       
! ----- toutes especes particules classiques
         WRITE(fmsgo,"('Dynamique classique non ', 
     &           'prise en charge : arret de l execution')")  
         STOP
       ELSEIF(ALL(dynam(1:nesp_p).EQ.2)) THEN
! ----- toutes espèces particules relativistes
         CALL chi_ik_rel_means
       ELSE
!       dynamique mixte non prise en charge pour le moment : 
!         arret de l execution 
! ---------------------------------------------------------------
         WRITE(fmsgo,"('Les deux especes de particules n ont ', 
     &           'pas la meme dynamique : arret de l execution')")  
         STOP
       ENDIF

! ----- Calcul epsilon de convergence des it�rations
       max_rho_ik=MAXVAL(ABS(rho_ik_save(:,:,:)))
       
       
       epsik=MAXVAL(ABS(rho_esp2_ik(:,:,:)-rho_ik_save(:,:,:)))
       epsik=epsik/max_rho_ik
       

       write(fmsgo,*) 'iteration : ',nik       
       write(fmsgo,*) 'Convergence ? epsik = ',epsik,
     &      ' pour epsik0 = ',epsik0    
                 
       write(fmsgo,*) '--------- FIN ITERATION ---------- '
       
       nik=nik+1

! ---- Fin de la m�thode it�rative
!    - nombre max d iterations atteint ou
!    - critere de convergence satisfait
       ENDDO


! ---- mise � jour des densit�s
       rho_esp_2(:,:,:) = rho_esp2_ik(:,:,:)

! ---- Mise � jour des susceptibilit�s
       xxx(:,:)=xxx_ik(:,:); xxy(:,:)=xxy_ik(:,:); xxz(:,:)=xxz_ik(:,:)
       xyx(:,:)=xyx_ik(:,:); xyy(:,:)=xyy_ik(:,:); xyz(:,:)=xyz_ik(:,:)
       xzx(:,:)=xzx_ik(:,:); xzy(:,:)=xzy_ik(:,:); xzz(:,:)=xzz_ik(:,:)


! ---- D�termination du champ corrig�
       ex(:,:)=ex(:,:)+ex2_ik(:,:)
       ey(:,:)=ey(:,:)+ey2_ik(:,:)




       return       
!      _____________________       
       END SUBROUTINE pot2d_iter
       
      
     
     
                   
       
