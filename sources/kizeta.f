! ======================================================================
! kizeta.f
! ----------
!
!     SUBROUTINE kizeta
!     SUBROUTINE kizeta_rel
!     SUBROUTINE kizeta_rel_ordn
!     SUBROUTINE kizeta_rel_ordn_sub
!
!     SUBROUTINE kizeta_components
!     SUBROUTINE kizeta_mean_components
!
!     SUBROUTINE kizeta_rel_means
!     SUBROUTINE kizeta_rel_means2
!
!     SUBROUTINE chi_ik
!     SUBROUTINE chi_ik_rel_means
!
! ======================================================================




       SUBROUTINE kizeta_rel(s1, s2, s3, s4, ipmx, ipmy,
     &                       chimat, zetamat, iesp)
! ======================================================================
! Dans cette routine on calcule la discr�tisation de chaque
! composante des tenseurs ki et zeta sur la grille 2d,
! et on calcul les composantes des tenseurs ki et zeta sur la grille 1d (suivant x)
! apr�s avoir moyenn� suivant y
!
! ======================================================================
       USE domaines
       USE champs
       USE particules
       USE particules_klder
       USE temps
       USE erreurs

       IMPLICIT NONE

       INTEGER, INTENT(IN)   ::   ipmx, ipmy, iesp

       REAL(kind=kr), INTENT(IN)  ::  s1, s2, s3, s4
       REAL(kind=kr), DIMENSION(1:3,1:3), INTENT(IN)  ::
     &    chimat, zetamat

       REAL(kind=kr)     ::  sd1, sd2, sd3, sd4

       INTEGER   ::  i, j


       sd1 = s1*dxi*dyi*abs(charge(iesp))
       sd2 = s2*dxi*dyi*abs(charge(iesp))
       sd3 = s3*dxi*dyi*abs(charge(iesp))
       sd4 = s4*dxi*dyi*abs(charge(iesp))
!       sd1 = s1
!       sd2 = s2
!       sd3 = s3
!       sd4 = s4

       i=ipmx ; j=ipmy

       xxx(i  ,j  ) = xxx(i  ,j  ) + sd1*chimat(1,1)
       xxx(i+1,j  ) = xxx(i+1,j  ) + sd2*chimat(1,1)
       xxx(i  ,j+1) = xxx(i  ,j+1) + sd3*chimat(1,1)
       xxx(i+1,j+1) = xxx(i+1,j+1) + sd4*chimat(1,1)

       xxy(i  ,j  ) = xxy(i  ,j  ) + sd1*chimat(1,2)
       xxy(i+1,j  ) = xxy(i+1,j  ) + sd2*chimat(1,2)
       xxy(i  ,j+1) = xxy(i  ,j+1) + sd3*chimat(1,2)
       xxy(i+1,j+1) = xxy(i+1,j+1) + sd4*chimat(1,2)

       xxz(i  ,j  ) = xxz(i  ,j  ) + sd1*chimat(1,3)
       xxz(i+1,j  ) = xxz(i+1,j  ) + sd2*chimat(1,3)
       xxz(i  ,j+1) = xxz(i  ,j+1) + sd3*chimat(1,3)
       xxz(i+1,j+1) = xxz(i+1,j+1) + sd4*chimat(1,3)

       xyx(i  ,j  ) = xyx(i  ,j  ) + sd1*chimat(2,1)
       xyx(i+1,j  ) = xyx(i+1,j  ) + sd2*chimat(2,1)
       xyx(i  ,j+1) = xyx(i  ,j+1) + sd3*chimat(2,1)
       xyx(i+1,j+1) = xyx(i+1,j+1) + sd4*chimat(2,1)

       xyy(i  ,j  ) = xyy(i  ,j  ) + sd1*chimat(2,2)
       xyy(i+1,j  ) = xyy(i+1,j  ) + sd2*chimat(2,2)
       xyy(i  ,j+1) = xyy(i  ,j+1) + sd3*chimat(2,2)
       xyy(i+1,j+1) = xyy(i+1,j+1) + sd4*chimat(2,2)

       xyz(i  ,j  ) = xyz(i  ,j  ) + sd1*chimat(2,3)
       xyz(i+1,j  ) = xyz(i+1,j  ) + sd2*chimat(2,3)
       xyz(i  ,j+1) = xyz(i  ,j+1) + sd3*chimat(2,3)
       xyz(i+1,j+1) = xyz(i+1,j+1) + sd4*chimat(2,3)

       xzx(i  ,j  ) = xzx(i  ,j  ) + sd1*chimat(3,1)
       xzx(i+1,j  ) = xzx(i+1,j  ) + sd2*chimat(3,1)
       xzx(i  ,j+1) = xzx(i  ,j+1) + sd3*chimat(3,1)
       xzx(i+1,j+1) = xzx(i+1,j+1) + sd4*chimat(3,1)

       xzy(i  ,j  ) = xzy(i  ,j  ) + sd1*chimat(3,2)
       xzy(i+1,j  ) = xzy(i+1,j  ) + sd2*chimat(3,2)
       xzy(i  ,j+1) = xzy(i  ,j+1) + sd3*chimat(3,2)
       xzy(i+1,j+1) = xzy(i+1,j+1) + sd4*chimat(3,2)

       xzz(i  ,j  ) = xzz(i  ,j  ) + sd1*chimat(3,3)
       xzz(i+1,j  ) = xzz(i+1,j  ) + sd2*chimat(3,3)
       xzz(i  ,j+1) = xzz(i  ,j+1) + sd3*chimat(3,3)
       xzz(i+1,j+1) = xzz(i+1,j+1) + sd4*chimat(3,3)


       zxx(i  ,j  ) = zxx(i  ,j  ) + sd1*zetamat(1,1)
       zxx(i+1,j  ) = zxx(i+1,j  ) + sd2*zetamat(1,1)
       zxx(i  ,j+1) = zxx(i  ,j+1) + sd3*zetamat(1,1)
       zxx(i+1,j+1) = zxx(i+1,j+1) + sd4*zetamat(1,1)

       zxy(i  ,j  ) = zxy(i  ,j  ) + sd1*zetamat(1,2)
       zxy(i+1,j  ) = zxy(i+1,j  ) + sd2*zetamat(1,2)
       zxy(i  ,j+1) = zxy(i  ,j+1) + sd3*zetamat(1,2)
       zxy(i+1,j+1) = zxy(i+1,j+1) + sd4*zetamat(1,2)

       zxz(i  ,j  ) = zxz(i  ,j  ) + sd1*zetamat(1,3)
       zxz(i+1,j  ) = zxz(i+1,j  ) + sd2*zetamat(1,3)
       zxz(i  ,j+1) = zxz(i  ,j+1) + sd3*zetamat(1,3)
       zxz(i+1,j+1) = zxz(i+1,j+1) + sd4*zetamat(1,3)

       zyx(i  ,j  ) = zyx(i  ,j  ) + sd1*zetamat(2,1)
       zyx(i+1,j  ) = zyx(i+1,j  ) + sd2*zetamat(2,1)
       zyx(i  ,j+1) = zyx(i  ,j+1) + sd3*zetamat(2,1)
       zyx(i+1,j+1) = zyx(i+1,j+1) + sd4*zetamat(2,1)

       zyy(i  ,j  ) = zyy(i  ,j  ) + sd1*zetamat(2,2)
       zyy(i+1,j  ) = zyy(i+1,j  ) + sd2*zetamat(2,2)
       zyy(i  ,j+1) = zyy(i  ,j+1) + sd3*zetamat(2,2)
       zyy(i+1,j+1) = zyy(i+1,j+1) + sd4*zetamat(2,2)

       zyz(i  ,j  ) = zyz(i  ,j  ) + sd1*zetamat(2,3)
       zyz(i+1,j  ) = zyz(i+1,j  ) + sd2*zetamat(2,3)
       zyz(i  ,j+1) = zyz(i  ,j+1) + sd3*zetamat(2,3)
       zyz(i+1,j+1) = zyz(i+1,j+1) + sd4*zetamat(2,3)

       zzx(i  ,j  ) = zzx(i  ,j  ) + sd1*zetamat(3,1)
       zzx(i+1,j  ) = zzx(i+1,j  ) + sd2*zetamat(3,1)
       zzx(i  ,j+1) = zzx(i  ,j+1) + sd3*zetamat(3,1)
       zzx(i+1,j+1) = zzx(i+1,j+1) + sd4*zetamat(3,1)

       zzy(i  ,j  ) = zzy(i  ,j  ) + sd1*zetamat(3,2)
       zzy(i+1,j  ) = zzy(i+1,j  ) + sd2*zetamat(3,2)
       zzy(i  ,j+1) = zzy(i  ,j+1) + sd3*zetamat(3,2)
       zzy(i+1,j+1) = zzy(i+1,j+1) + sd4*zetamat(3,2)

       zzz(i  ,j  ) = zzz(i  ,j  ) + sd1*zetamat(3,3)
       zzz(i+1,j  ) = zzz(i+1,j  ) + sd2*zetamat(3,3)
       zzz(i  ,j+1) = zzz(i  ,j+1) + sd3*zetamat(3,3)
       zzz(i+1,j+1) = zzz(i+1,j+1) + sd4*zetamat(3,3)


 !
       return
!      _____________________
       END SUBROUTINE kizeta_rel


       SUBROUTINE chi_ik(pponpx, pponpy, ipx, ipy,
     &                   chimat, iesp, aux_rh)
! ======================================================================
! Dans cette routine on calcule la discr�tisation de chaque
! composante des tenseurs ki et zeta sur la grille 2d,
! et on calcul les composantes des tenseurs ki et zeta sur la grille 1d (suivant x)
! apr�s avoir moyenn� suivant y
!
! ======================================================================
       USE domaines
       USE champs_iter
       USE particules
       USE particules_klder
       USE temps
       USE erreurs

       IMPLICIT NONE



       INTEGER(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN) ::
     &        ipx, ipy
       REAL(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN)  ::
     &        pponpx, pponpy

       REAL(kind=kr), DIMENSION(1:3,1:3), INTENT(IN)  ::
     &    chimat

       REAL(kind=kr), INTENT(IN)   ::    aux_rh

       INTEGER, INTENT(IN)   ::    iesp

       REAL(kind=kr)    ::   aux

       INTEGER   ::  i, j, ord



      aux = aux_rh*dxi*dyi*abs(charge(iesp))

      ord= ordre_interp+1


       DO j=1,ord
         DO i=1,ord
           xxx_ik(ipx(i),ipy(j)) = xxx_ik(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*chimat(1,1)*aux
           xxy_ik(ipx(i),ipy(j)) = xxy_ik(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*chimat(1,2)*aux
           xxz_ik(ipx(i),ipy(j)) = xxz_ik(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*chimat(1,3)*aux

           xyx_ik(ipx(i),ipy(j)) = xyx_ik(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*chimat(2,1)*aux
           xyy_ik(ipx(i),ipy(j)) = xyy_ik(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*chimat(2,2)*aux
           xyz_ik(ipx(i),ipy(j)) = xyz_ik(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*chimat(2,3)*aux

           xzx_ik(ipx(i),ipy(j)) = xzx_ik(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*chimat(3,1)*aux
           xzy_ik(ipx(i),ipy(j)) = xzy_ik(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*chimat(3,2)*aux
           xzz_ik(ipx(i),ipy(j)) = xzz_ik(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*chimat(3,3)*aux
         ENDDO
       ENDDO

 !
       return
!      _____________________
       END SUBROUTINE chi_ik


       SUBROUTINE chi_ik_rel_means
! ======================================================================
! Dans cette routine
! on calcul les composantes des tenseurs ki et zeta sur la grille 1d (suivant x)
! apr�s avoir moyenn� suivant y
!
! ======================================================================
       USE domaines
       USE champs_iter
       USE particules
       USE particules_klder
       USE temps
       USE erreurs

       IMPLICIT NONE

       INTEGER   ::  i, j

       REAL(kind=kr), DIMENSION(0:n1x)    ::
     &      rhomn, rhomx, rhoaux

       REAL(kind=kr)                      ::
     &    xaux



 !
 !     calcul des valeurs moyennes
 !

       rhomn(:)=1.e38
       rhomx(:)=0.
       rhoaux(:)=0.
       xxx0_ik(:)=0.
       xxy0_ik(:)=0.
       xxz0_ik(:)=0.
       xyx0_ik(:)=0.
       xyy0_ik(:)=0.
       xyz0_ik(:)=0.
       xzx0_ik(:)=0.
       xzy0_ik(:)=0.
       xzz0_ik(:)=0.

       DO j=1,ny
         DO i=0,n1x
         rhomn(i)=min(rhomn(i),rho_esp2_ik(i,j,iref))
         rhomx(i)=max(rhomx(i),rho_esp2_ik(i,j,iref))
         xxx0_ik(i)=xxx0_ik(i)+xxx_ik(i,j)
         xxy0_ik(i)=xxy0_ik(i)+xxy_ik(i,j)
         xxz0_ik(i)=xxz0_ik(i)+xxz_ik(i,j)
         xyx0_ik(i)=xyx0_ik(i)+xyx_ik(i,j)
         xyy0_ik(i)=xyy0_ik(i)+xyy_ik(i,j)
         xyz0_ik(i)=xyz0_ik(i)+xyz_ik(i,j)
         xzx0_ik(i)=xzx0_ik(i)+xzx_ik(i,j)
         xzy0_ik(i)=xzy0_ik(i)+xzy_ik(i,j)
         xzz0_ik(i)=xzz0_ik(i)+xzz_ik(i,j)

         rhoaux(i)=rhoaux(i)+rho_esp2_ik(i,j,iref)
 !
         ENDDO
       ENDDO

       xaux = 1./ny

       DO i=0,n1x
         xxx0_ik(i)=xaux*xxx0_ik(i)
         xxy0_ik(i)=xaux*xxy0_ik(i)
         xxz0_ik(i)=xaux*xxz0_ik(i)
         xyx0_ik(i)=xaux*xyx0_ik(i)
         xyy0_ik(i)=xaux*xyy0_ik(i)
         xyz0_ik(i)=xaux*xyz0_ik(i)
         xzx0_ik(i)=xaux*xzx0_ik(i)
         xzy0_ik(i)=xaux*xzy0_ik(i)
         xzz0_ik(i)=xaux*xzz0_ik(i)
       ENDDO

 !
 !
 !     on va positionner les ki et zeta a leur position
 !     pour la resolution de maxwell
 !
 !     xxx,zyx,zzx en (i+1/2,j).
 !     -------------------------
       DO i=n1x,1,-1
           xxx_ik(i,:)=0.5*(xxx_ik(i,:)+xxx_ik(i-1,:))
       ENDDO

         xxx_ik(0,:)=0.

       DO i=n1x,1,-1
         xxx0_ik(i)=0.5*(xxx0_ik(i)+xxx0_ik(i-1))
       ENDDO

       xxx0_ik(0)=0.
 !
 !     xyy,zxy,zzy en (i,j+1/2)
 !     ------------------------
       DO j=n1y,1,-1
         xyy_ik(:,j)=0.5*(xyy_ik(:,j)+xyy_ik(:,j-1))
       ENDDO

         xyy_ik(:,0)=xyy_ik(:,ny)

!
       return
!      _____________________
       END SUBROUTINE chi_ik_rel_means



       SUBROUTINE kizeta_rel_ordn(pponpx, pponpy, ipx, ipy,
     &                       chimat, zetamat, iesp, aux_rh)
! ======================================================================
! Dans cette routine on calcule la discr�tisation de chaque
! composante des tenseurs ki et zeta sur la grille 2d,
! et on calcul les composantes des tenseurs ki et zeta sur la grille 1d (suivant x)
! apr�s avoir moyenn� suivant y
!
! ======================================================================
       USE domaines
       USE champs
       USE particules
       USE particules_klder
       USE temps
       USE erreurs

       IMPLICIT NONE



       INTEGER(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN) ::
     &     ipx, ipy
       REAL(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN)  ::
     &     pponpx, pponpy

       REAL(kind=kr), DIMENSION(1:3,1:3), INTENT(IN)  ::
     &    chimat, zetamat

       REAL(kind=kr), INTENT(IN)   ::    aux_rh

       INTEGER, INTENT(IN)   ::    iesp

       REAL(kind=kr)    ::   aux

       INTEGER   ::  i, j, ord


       ord= ordre_interp+1

      aux = aux_rh*dxi*dyi*abs(charge(iesp))
!       aux = 1.


       DO j=1,ord
         DO i=1,ord
           xxx(ipx(i),ipy(j)) = xxx(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*chimat(1,1)*aux
           xxy(ipx(i),ipy(j)) = xxy(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*chimat(1,2)*aux
           xxz(ipx(i),ipy(j)) = xxz(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*chimat(1,3)*aux

           xyx(ipx(i),ipy(j)) = xyx(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*chimat(2,1)*aux
           xyy(ipx(i),ipy(j)) = xyy(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*chimat(2,2)*aux
           xyz(ipx(i),ipy(j)) = xyz(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*chimat(2,3)*aux

           xzx(ipx(i),ipy(j)) = xzx(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*chimat(3,1)*aux
           xzy(ipx(i),ipy(j)) = xzy(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*chimat(3,2)*aux
           xzz(ipx(i),ipy(j)) = xzz(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*chimat(3,3)*aux

           zxx(ipx(i),ipy(j)) = zxx(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*zetamat(1,1)*aux
           zxy(ipx(i),ipy(j)) = zxy(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*zetamat(1,2)*aux
           zxz(ipx(i),ipy(j)) = zxz(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*zetamat(1,3)*aux

           zyx(ipx(i),ipy(j)) = zyx(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*zetamat(2,1)*aux
           zyy(ipx(i),ipy(j)) = zyy(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*zetamat(2,2)*aux
           zyz(ipx(i),ipy(j)) = zyz(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*zetamat(2,3)*aux

           zzx(ipx(i),ipy(j)) = zzx(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*zetamat(3,1)*aux
           zzy(ipx(i),ipy(j)) = zzy(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*zetamat(3,2)*aux
           zzz(ipx(i),ipy(j)) = zzz(ipx(i),ipy(j)) +
     &         (pponpx(i)*pponpy(j))*zetamat(3,3)*aux
         ENDDO
       ENDDO


 !
       return
!      _____________________
       END SUBROUTINE kizeta_rel_ordn



       SUBROUTINE kizeta_rel_ordn_sub(pponpx, pponpy, ipx, ipy,
     &                       chimat, zetamat, iesp, aux_rh)
! ======================================================================
! Dans cette routine on calcule la discr�tisation de chaque
! composante des tenseurs ki et zeta sur la grille 2d,
! et on calcul les composantes des tenseurs ki et zeta sur la grille 1d (suivant x)
! apr�s avoir moyenn� suivant y
!
! ======================================================================
       USE domaines
       USE champs
       USE particules
       USE particules_klder
       USE temps
       USE erreurs

       IMPLICIT NONE



       INTEGER(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN) ::
     &   ipx, ipy
       REAL(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN)  ::
     &   pponpx, pponpy

       REAL(kind=kr), DIMENSION(1:3,1:3), INTENT(IN)  ::
     &    chimat, zetamat

       REAL(kind=kr), INTENT(IN)   ::    aux_rh

       INTEGER, INTENT(IN)   ::    iesp

       REAL(kind=kr)    ::   aux

       INTEGER   ::  i, j, ord


       ord= ordre_interp+1


      aux = aux_rh*dxi*dyi*abs(charge(iesp))
!       aux = 1.


       DO j=1,ord
         DO i=1,ord
           xxx_esp(ipx(i),ipy(j),iesp) = xxx_esp(ipx(i),ipy(j),iesp) +
     &         (pponpx(i)*pponpy(j))*chimat(1,1)*aux
           xxy_esp(ipx(i),ipy(j),iesp) = xxy_esp(ipx(i),ipy(j),iesp) +
     &         (pponpx(i)*pponpy(j))*chimat(1,2)*aux
           xxz_esp(ipx(i),ipy(j),iesp) = xxz_esp(ipx(i),ipy(j),iesp) +
     &         (pponpx(i)*pponpy(j))*chimat(1,3)*aux

           xyx_esp(ipx(i),ipy(j),iesp) = xyx_esp(ipx(i),ipy(j),iesp) +
     &         (pponpx(i)*pponpy(j))*chimat(2,1)*aux
           xyy_esp(ipx(i),ipy(j),iesp) = xyy_esp(ipx(i),ipy(j),iesp) +
     &         (pponpx(i)*pponpy(j))*chimat(2,2)*aux
           xyz_esp(ipx(i),ipy(j),iesp) = xyz_esp(ipx(i),ipy(j),iesp) +
     &         (pponpx(i)*pponpy(j))*chimat(2,3)*aux

           xzx_esp(ipx(i),ipy(j),iesp) = xzx_esp(ipx(i),ipy(j),iesp) +
     &         (pponpx(i)*pponpy(j))*chimat(3,1)*aux
           xzy_esp(ipx(i),ipy(j),iesp) = xzy_esp(ipx(i),ipy(j),iesp) +
     &         (pponpx(i)*pponpy(j))*chimat(3,2)*aux
           xzz_esp(ipx(i),ipy(j),iesp) = xzz_esp(ipx(i),ipy(j),iesp) +
     &         (pponpx(i)*pponpy(j))*chimat(3,3)*aux

           zxx_esp(ipx(i),ipy(j),iesp) = zxx_esp(ipx(i),ipy(j),iesp) +
     &         (pponpx(i)*pponpy(j))*zetamat(1,1)*aux
           zxy_esp(ipx(i),ipy(j),iesp) = zxy_esp(ipx(i),ipy(j),iesp) +
     &         (pponpx(i)*pponpy(j))*zetamat(1,2)*aux
           zxz_esp(ipx(i),ipy(j),iesp) = zxz_esp(ipx(i),ipy(j),iesp) +
     &         (pponpx(i)*pponpy(j))*zetamat(1,3)*aux

           zyx_esp(ipx(i),ipy(j),iesp) = zyx_esp(ipx(i),ipy(j),iesp) +
     &         (pponpx(i)*pponpy(j))*zetamat(2,1)*aux
           zyy_esp(ipx(i),ipy(j),iesp) = zyy_esp(ipx(i),ipy(j),iesp) +
     &         (pponpx(i)*pponpy(j))*zetamat(2,2)*aux
           zyz_esp(ipx(i),ipy(j),iesp) = zyz_esp(ipx(i),ipy(j),iesp) +
     &         (pponpx(i)*pponpy(j))*zetamat(2,3)*aux

           zzx_esp(ipx(i),ipy(j),iesp) = zzx_esp(ipx(i),ipy(j),iesp) +
     &         (pponpx(i)*pponpy(j))*zetamat(3,1)*aux
           zzy_esp(ipx(i),ipy(j),iesp) = zzy_esp(ipx(i),ipy(j),iesp) +
     &         (pponpx(i)*pponpy(j))*zetamat(3,2)*aux
           zzz_esp(ipx(i),ipy(j),iesp) = zzz_esp(ipx(i),ipy(j),iesp) +
     &         (pponpx(i)*pponpy(j))*zetamat(3,3)*aux
         ENDDO
       ENDDO


 !
       return
!      _____________________
       END SUBROUTINE kizeta_rel_ordn_sub



       SUBROUTINE kizeta_rel_means
! ======================================================================
! Dans cette routine
! on calcul les composantes des tenseurs ki et zeta sur la grille 1d (suivant x)
! apr�s avoir moyenn� suivant y
!
! ======================================================================
       USE domaines
       USE champs
       USE particules
       USE particules_klder
       USE temps
       USE erreurs

       IMPLICIT NONE

       INTEGER   ::  i, j

       REAL(kind=kr), DIMENSION(0:n1x)    ::
     &      rhomn, rhomx, rhoaux

       REAL(kind=kr)                      ::
     &    xaux



 !
 !     calcul des valeurs moyennes
 !

       rhomn(:)=1.e38
       rhomx(:)=0.
       rhoaux(:)=0.
       xxx0(:)=0.
       xxy0(:)=0.
       xxz0(:)=0.
       xyx0(:)=0.
       xyy0(:)=0.
       xyz0(:)=0.
       xzx0(:)=0.
       xzy0(:)=0.
       xzz0(:)=0.
       zxx0(:)=0.
       zxy0(:)=0.
       zxz0(:)=0.
       zyx0(:)=0.
       zyy0(:)=0.
       zyz0(:)=0.
       zzx0(:)=0.
       zzy0(:)=0.
       zzz0(:)=0.


       DO j=1,ny
         DO i=0,n1x
         rhomn(i)=min(rhomn(i),rho_esp_2(i,j,iref))
         rhomx(i)=max(rhomx(i),rho_esp_2(i,j,iref))
         xxx0(i)=xxx0(i)+xxx(i,j)
         xxy0(i)=xxy0(i)+xxy(i,j)
         xxz0(i)=xxz0(i)+xxz(i,j)
         xyx0(i)=xyx0(i)+xyx(i,j)
         xyy0(i)=xyy0(i)+xyy(i,j)
         xyz0(i)=xyz0(i)+xyz(i,j)
         xzx0(i)=xzx0(i)+xzx(i,j)
         xzy0(i)=xzy0(i)+xzy(i,j)
         xzz0(i)=xzz0(i)+xzz(i,j)
 !
         zxx0(i)=zxx0(i)+zxx(i,j)
         zxy0(i)=zxy0(i)+zxy(i,j)
         zxz0(i)=zxz0(i)+zxz(i,j)
         zyx0(i)=zyx0(i)+zyx(i,j)
         zyy0(i)=zyy0(i)+zyy(i,j)
         zyz0(i)=zyz0(i)+zyz(i,j)
         zzx0(i)=zzx0(i)+zzx(i,j)
         zzy0(i)=zzy0(i)+zzy(i,j)
         zzz0(i)=zzz0(i)+zzz(i,j)
         rhoaux(i)=rhoaux(i)+rho_esp_2(i,j,iref)
 !
         ENDDO
       ENDDO

       xaux = 1./ny

       DO i=0,n1x
         xxx0(i)=xaux*xxx0(i)
         xxy0(i)=xaux*xxy0(i)
         xxz0(i)=xaux*xxz0(i)
         xyx0(i)=xaux*xyx0(i)
         xyy0(i)=xaux*xyy0(i)
         xyz0(i)=xaux*xyz0(i)
         xzx0(i)=xaux*xzx0(i)
         xzy0(i)=xaux*xzy0(i)
         xzz0(i)=xaux*xzz0(i)
 !
         zxx0(i)=xaux*zxx0(i)
         zxy0(i)=xaux*zxy0(i)
         zxz0(i)=xaux*zxz0(i)
         zyx0(i)=xaux*zyx0(i)
         zyy0(i)=xaux*zyy0(i)
         zyz0(i)=xaux*zyz0(i)
         zzx0(i)=xaux*zzx0(i)
         zzy0(i)=xaux*zzy0(i)
         zzz0(i)=xaux*zzz0(i)
       ENDDO

 !
 !
 !     on va positionner les ki et zeta a leur position
 !     pour la resolution de maxwell
 !
 !     xxx,zyx,zzx en (i+1/2,j).
 !     -------------------------
       DO i=n1x,1,-1
           xxx(i,:)=0.5*(xxx(i,:)+xxx(i-1,:))
           zxx(i,:)=0.5*(zxx(i,:)+zxx(i-1,:))
           zyx(i,:)=0.5*(zyx(i,:)+zyx(i-1,:))
           zzx(i,:)=0.5*(zzx(i,:)+zzx(i-1,:))
       ENDDO

         xxx(0,:)=0.
         zxx(0,:)=0.
         zyx(0,:)=0.
         zzx(0,:)=0.

       DO i=n1x,1,-1
         xxx0(i)=0.5*(xxx0(i)+xxx0(i-1))
         zxx0(i)=0.5*(zxx0(i)+zxx0(i-1))
         zyx0(i)=0.5*(zyx0(i)+zyx0(i-1))
         zzx0(i)=0.5*(zzx0(i)+zzx0(i-1))
       ENDDO

       xxx0(0)=0.
       zxx0(0)=0.
       zxy0(0)=0.
       zxz0(0)=0.
 !
 !     xyy,zxy,zzy en (i,j+1/2)
 !     ------------------------
       DO j=n1y,1,-1
         xyy(:,j)=0.5*(xyy(:,j)+xyy(:,j-1))
         zxy(:,j)=0.5*(zxy(:,j)+zxy(:,j-1))
         zyy(:,j)=0.5*(zyy(:,j)+zyy(:,j-1))
         zzy(:,j)=0.5*(zzy(:,j)+zzy(:,j-1))
       ENDDO

         xyy(:,0)=xyy(:,ny)
         zxy(:,0)=zxy(:,ny)
         zyy(:,0)=zyy(:,ny)
         zzy(:,0)=zzy(:,ny)

 !
       return
!      _____________________
       END SUBROUTINE kizeta_rel_means


       SUBROUTINE kizeta_rel_means2
! ======================================================================
! Dans cette routine on calcule la discr�tisation de chaque
! composante des tenseurs ki et zeta sur la grille 2d,
! et on calcul les composantes des tenseurs ki et zeta sur la grille 1d (suivant x)
! apr�s avoir moyenn� suivant y
!
! ======================================================================
       USE domaines
       USE champs
       USE particules
       USE particules_klder
       USE temps
       USE erreurs

       IMPLICIT NONE

       INTEGER   ::  i, j

       REAL(kind=kr), DIMENSION(0:n1x)    ::
     &      rhomn, rhomx, rhoaux

       REAL(kind=kr)                      ::
     &    xaux



 !
 !     calcul des valeurs moyennes
 !

       rhomn(:)=1.e38
       rhomx(:)=0.
       rhoaux(:)=0.
       xxx0(:)=0.
       xxy0(:)=0.
       xxz0(:)=0.
       xyx0(:)=0.
       xyy0(:)=0.
       xyz0(:)=0.
       xzx0(:)=0.
       xzy0(:)=0.
       xzz0(:)=0.
       zxx0(:)=0.
       zxy0(:)=0.
       zxz0(:)=0.
       zyx0(:)=0.
       zyy0(:)=0.
       zyz0(:)=0.
       zzx0(:)=0.
       zzy0(:)=0.
       zzz0(:)=0.


       DO j=1,ny
         DO i=0,n1x
         rhomn(i)=min(rhomn(i),rho_esp_2(i,j,iref))
         rhomx(i)=max(rhomx(i),rho_esp_2(i,j,iref))
         xxx0(i)=xxx0(i)+xxx(i,j)
         xxy0(i)=xxy0(i)+xxy(i,j)
         xxz0(i)=xxz0(i)+xxz(i,j)
         xyx0(i)=xyx0(i)+xyx(i,j)
         xyy0(i)=xyy0(i)+xyy(i,j)
         xyz0(i)=xyz0(i)+xyz(i,j)
         xzx0(i)=xzx0(i)+xzx(i,j)
         xzy0(i)=xzy0(i)+xzy(i,j)
         xzz0(i)=xzz0(i)+xzz(i,j)
 !
         zxx0(i)=zxx0(i)+zxx(i,j)
         zxy0(i)=zxy0(i)+zxy(i,j)
         zxz0(i)=zxz0(i)+zxz(i,j)
         zyx0(i)=zyx0(i)+zyx(i,j)
         zyy0(i)=zyy0(i)+zyy(i,j)
         zyz0(i)=zyz0(i)+zyz(i,j)
         zzx0(i)=zzx0(i)+zzx(i,j)
         zzy0(i)=zzy0(i)+zzy(i,j)
         zzz0(i)=zzz0(i)+zzz(i,j)
         rhoaux(i)=rhoaux(i)+rho_esp_2(i,j,iref)
 !
         ENDDO
       ENDDO

       DO i=0,n1x
        if(rhoaux(i).gt.0.) then
         xaux=0.5*(rhomn(i)+rhomx(i))/rhoaux(i)
         xxx0(i)=xaux*xxx0(i)
         xxy0(i)=xaux*xxy0(i)
         xxz0(i)=xaux*xxz0(i)
         xyx0(i)=xaux*xyx0(i)
         xyy0(i)=xaux*xyy0(i)
         xyz0(i)=xaux*xyz0(i)
         xzx0(i)=xaux*xzx0(i)
         xzy0(i)=xaux*xzy0(i)
         xzz0(i)=xaux*xzz0(i)
 !
         zxx0(i)=xaux*zxx0(i)
         zxy0(i)=xaux*zxy0(i)
         zxz0(i)=xaux*zxz0(i)
         zyx0(i)=xaux*zyx0(i)
         zyy0(i)=xaux*zyy0(i)
         zyz0(i)=xaux*zyz0(i)
         zzx0(i)=xaux*zzx0(i)
         zzy0(i)=xaux*zzy0(i)
         zzz0(i)=xaux*zzz0(i)
        else
         xxx0(i)=0.
         xxy0(i)=0.
         xxz0(i)=0.
         xyx0(i)=0.
         xyy0(i)=0.
         xyz0(i)=0.
         xzx0(i)=0.
         xzy0(i)=0.
         xzz0(i)=0.
 !
         zxx0(i)=0.
         zxy0(i)=0.
         zxz0(i)=0.
         zyx0(i)=0.
         zyy0(i)=0.
         zyz0(i)=0.
         zzx0(i)=0.
         zzy0(i)=0.
         zzz0(i)=0.
        endif
       ENDDO
 !
 !
 !     on va positionner les ki et zeta a leur position
 !     pour la resolution de maxwell
 !
 !     xxx,zyx,zzx en (i+1/2,j).
 !     -------------------------
       DO i=n1x,1,-1
           xxx(i,:)=0.5*(xxx(i,:)+xxx(i-1,:))
           zxx(i,:)=0.5*(zxx(i,:)+zxx(i-1,:))
           zyx(i,:)=0.5*(zyx(i,:)+zyx(i-1,:))
           zzx(i,:)=0.5*(zzx(i,:)+zzx(i-1,:))
       ENDDO

         xxx(0,:)=0.
         zxx(0,:)=0.
         zyx(0,:)=0.
         zzx(0,:)=0.

       DO i=n1x,1,-1
         xxx0(i)=0.5*(xxx0(i)+xxx0(i-1))
         zxx0(i)=0.5*(zxx0(i)+zxx0(i-1))
         zyx0(i)=0.5*(zyx0(i)+zyx0(i-1))
         zzx0(i)=0.5*(zzx0(i)+zzx0(i-1))
       ENDDO

       xxx0(0)=0.
       zxx0(0)=0.
       zxy0(0)=0.
       zxz0(0)=0.
 !
 !     xyy,zxy,zzy en (i,j+1/2)
 !     ------------------------
       DO j=n1y,1,-1
         xyy(:,j)=0.5*(xyy(:,j)+xyy(:,j-1))
         zxy(:,j)=0.5*(zxy(:,j)+zxy(:,j-1))
         zyy(:,j)=0.5*(zyy(:,j)+zyy(:,j-1))
         zzy(:,j)=0.5*(zzy(:,j)+zzy(:,j-1))
       ENDDO

         xyy(:,0)=xyy(:,ny)
         zxy(:,0)=zxy(:,ny)
         zyy(:,0)=zyy(:,ny)
         zzy(:,0)=zzy(:,ny)

 !
       return
!      _____________________
       END SUBROUTINE kizeta_rel_means2




       SUBROUTINE kizeta
! ======================================================================
! Dans cette routine on calcule la discr�tisation de chaque
! composante des tenseurs ki et zeta sur la grille 2d,
! et on calcul les composantes des tenseurs ki et zeta sur la grille 1d (suivant x)
! apr�s avoir moyenn� suivant y
!
! ======================================================================
       USE domaines
       USE champs
       USE particules
       USE particules_klder
       USE temps
       USE erreurs

       IMPLICIT NONE

       INTEGER   ::  i, j, iesp

       REAL(kind=kr), DIMENSION(0:n1x)    ::
     &      rhomn, rhomx, rhoaux

       REAL(kind=kr)     ::    xaux

       REAL(kind=kr)     ::    rki1b, rki1, aux1, aux2
       REAL(kind=kr)     ::    rzeta1, rqm

       REAL(kind=kr)     ::
     &       lzxx, lzxy, lzxz,
     &       lzyx, lzyy, lzyz,
     &       lzzx, lzzy, lzzz



       DO iesp=1, nesp_p

       rqm = charge(iesp)/masse(iesp)

       rki1b = dt2s4*rqm

       aux1 = dts2*rqm
       aux2 = dt2s4*rqm*rqm

         DO j=0,n1y
           DO i=0,n1x

 !
 !     calcul des 9 elements de la matrice ki.
 !     ---------------------------------------

 ! ------ mise � jour rki1
           rki1 = rki1b*2./(1.+aux2*(bx(i,j)**2+by(i,j)**2+bz(i,j)**2))

 !
           xxx(i,j)= xxx(i,j)+ rki1*rho_esp_2(i,j,iesp)*
     &          (1 + aux2*bx(i,j)**2)
           xxy(i,j)= xyx(i,j)+ rki1*rho_esp_2(i,j,iesp)*
     &          (aux2*bx(i,j)*by(i,j) + aux1*bz(i,j))
           xxz(i,j)= xzx(i,j)+ rki1*rho_esp_2(i,j,iesp)*
     &          (aux2*bx(i,j)*bz(i,j) - aux1*by(i,j))
 !
           xyx(i,j)= xyx(i,j)+ rki1*rho_esp_2(i,j,iesp)*
     &          (aux2*by(i,j)*bx(i,j) - aux1*bz(i,j))
           xyy(i,j)= xyy(i,j)+ rki1*rho_esp_2(i,j,iesp)*
     &          (1 + aux2*by(i,j)**2)
           xyz(i,j)= xyz(i,j)+ rki1*rho_esp_2(i,j,iesp)*
     &          (aux2*by(i,j)*bz(i,j) + aux1*bx(i,j))
 !
           xzx(i,j)= xzx(i,j)+ rki1*rho_esp_2(i,j,iesp)*
     &          (aux2*bz(i,j)*bx(i,j) + aux1*by(i,j))
           xzy(i,j)= xzy(i,j)+ rki1*rho_esp_2(i,j,iesp)*
     &          (aux2*bz(i,j)*by(i,j) - aux1*bx(i,j))
           xzz(i,j)= xzz(i,j)+ rki1*rho_esp_2(i,j,iesp)*
     &          (1 + aux2*bz(i,j)**2)
 !
 !
 !     calcul de zeta.
 !     ---------------
           rzeta1 = 0.5*rki1
 !
           lzxx =
     &      dgy_esp_2(i,j,iesp)*(aux2*bz(i,j)*bx(i,j)+aux1*by(i,j)) -
     &      dgz_esp_2(i,j,iesp)*(aux2*by(i,j)*bx(i,j)-aux1*bz(i,j))

           lzxy =
     &      dgy_esp_2(i,j,iesp)*(aux2*bz(i,j)*by(i,j)-aux1*bx(i,j)) -
     &      dgz_esp_2(i,j,iesp)*(1+aux2*by(i,j)**2)

           lzxz =
     &      dgy_esp_2(i,j,iesp)*(1+aux2*bz(i,j)**2) -
     &      dgz_esp_2(i,j,iesp)*(aux2*by(i,j)*bz(i,j)+aux1*bx(i,j))

 !
           lzyx =
     &      dgz_esp_2(i,j,iesp)*(1+aux2*bx(i,j)**2) -
     &      dgx_esp_2(i,j,iesp)*(aux2*bz(i,j)*bx(i,j)+aux1*by(i,j))

           lzyy =
     &      dgz_esp_2(i,j,iesp)*(aux2*bx(i,j)*by(i,j)+aux1*bz(i,j)) -
     &      dgx_esp_2(i,j,iesp)*(aux2*bz(i,j)*by(i,j)-aux1*bx(i,j))

           lzyz =
     &      dgz_esp_2(i,j,iesp)*(aux2*bx(i,j)*bz(i,j)-aux1*by(i,j)) -
     &      dgx_esp_2(i,j,iesp)*(1+aux2*bz(i,j)**2)
 !
           lzzx =
     &      dgx_esp_2(i,j,iesp)*(aux2*by(i,j)*bx(i,j)-aux1*bz(i,j)) -
     &      dgy_esp_2(i,j,iesp)*(1+aux2*bx(i,j)**2)

           lzzy =
     &      dgx_esp_2(i,j,iesp)*(1+aux2*by(i,j)**2) -
     &      dgy_esp_2(i,j,iesp)*(aux2*bx(i,j)*by(i,j)+aux1*bz(i,j))

           lzzz =
     &      dgx_esp_2(i,j,iesp)*(aux2*by(i,j)*bz(i,j)+aux1*bx(i,j)) -
     &      dgy_esp_2(i,j,iesp)*(aux2*bx(i,j)*bz(i,j)-aux1*by(i,j))

 !
           zxx(i,j)= zxx(i,j) + rzeta1*lzxx
           zxy(i,j)= zxy(i,j) + rzeta1*lzxy
           zxz(i,j)= zxz(i,j) + rzeta1*lzxz
 !
           zyx(i,j)= zyx(i,j) + rzeta1*lzyx
           zyy(i,j)= zyy(i,j) + rzeta1*lzyy
           zyz(i,j)= zyz(i,j) + rzeta1*lzyz
 !
           zzx(i,j)= zzx(i,j) + rzeta1*lzzx
           zzy(i,j)= zzy(i,j) + rzeta1*lzzy
           zzz(i,j)= zzz(i,j) + rzeta1*lzzz

           ENDDO
         ENDDO
! ----- fin de la boucle sur les esp�ces
       ENDDO

 !
 !     calcul des valeurs moyennes
 !

       rhomn(:)=1.e38
       rhomx(:)=0.
       rhoaux(:)=0.
       xxx0(:)=0.
       xxy0(:)=0.
       xxz0(:)=0.
       xyx0(:)=0.
       xyy0(:)=0.
       xyz0(:)=0.
       xzx0(:)=0.
       xzy0(:)=0.
       xzz0(:)=0.
       zxx0(:)=0.
       zxy0(:)=0.
       zxz0(:)=0.
       zyx0(:)=0.
       zyy0(:)=0.
       zyz0(:)=0.
       zzx0(:)=0.
       zzy0(:)=0.
       zzz0(:)=0.


       DO j=1,ny
         DO i=0,n1x
       rhomn(i)=min(rhomn(i),rho_esp_2(i,j,iref))
       rhomx(i)=max(rhomx(i),rho_esp_2(i,j,iref))
       xxx0(i)=xxx0(i)+xxx(i,j)
       xxy0(i)=xxy0(i)+xxy(i,j)
       xxz0(i)=xxz0(i)+xxz(i,j)
       xyx0(i)=xyx0(i)+xyx(i,j)
       xyy0(i)=xyy0(i)+xyy(i,j)
       xyz0(i)=xyz0(i)+xyz(i,j)
       xzx0(i)=xzx0(i)+xzx(i,j)
       xzy0(i)=xzy0(i)+xzy(i,j)
       xzz0(i)=xzz0(i)+xzz(i,j)
 !
       zxx0(i)=zxx0(i)+zxx(i,j)
       zxy0(i)=zxy0(i)+zxy(i,j)
       zxz0(i)=zxz0(i)+zxz(i,j)
       zyx0(i)=zyx0(i)+zyx(i,j)
       zyy0(i)=zyy0(i)+zyy(i,j)
       zyz0(i)=zyz0(i)+zyz(i,j)
       zzx0(i)=zzx0(i)+zzx(i,j)
       zzy0(i)=zzy0(i)+zzy(i,j)
       zzz0(i)=zzz0(i)+zzz(i,j)
       rhoaux(i)=rhoaux(i)+rho_esp_2(i,j,iref)
 !
         ENDDO
       ENDDO

       xaux = 1./ny

       DO i=0,n1x
        if(rhoaux(i).ne.0.) then
!         xaux=0.5*(rhomn(i)+rhomx(i))/rhoaux(i)
         xxx0(i)=xaux*xxx0(i)
         xxy0(i)=xaux*xxy0(i)
         xxz0(i)=xaux*xxz0(i)
         xyx0(i)=xaux*xyx0(i)
         xyy0(i)=xaux*xyy0(i)
         xyz0(i)=xaux*xyz0(i)
         xzx0(i)=xaux*xzx0(i)
         xzy0(i)=xaux*xzy0(i)
         xzz0(i)=xaux*xzz0(i)
 !
         zxx0(i)=xaux*zxx0(i)
         zxy0(i)=xaux*zxy0(i)
         zxz0(i)=xaux*zxz0(i)
         zyx0(i)=xaux*zyx0(i)
         zyy0(i)=xaux*zyy0(i)
         zyz0(i)=xaux*zyz0(i)
         zzx0(i)=xaux*zzx0(i)
         zzy0(i)=xaux*zzy0(i)
         zzz0(i)=xaux*zzz0(i)
        else
         xxx0(i)=0.
         xxy0(i)=0.
         xxz0(i)=0.
         xyx0(i)=0.
         xyy0(i)=0.
         xyz0(i)=0.
         xzx0(i)=0.
         xzy0(i)=0.
         xzz0(i)=0.
 !
         zxx0(i)=0.
         zxy0(i)=0.
         zxz0(i)=0.
         zyx0(i)=0.
         zyy0(i)=0.
         zyz0(i)=0.
         zzx0(i)=0.
         zzy0(i)=0.
         zzz0(i)=0.
        endif
       ENDDO
 !
 !
 !     on va positionner les ki et zeta a leur position
 !     pour la resolution de maxwell
 !
 !     xxx,zyx,zzx en (i+1/2,j).
 !     -------------------------
       DO i=n1x,1,-1
           xxx(i,:)=0.5*(xxx(i,:)+xxx(i-1,:))
           zxx(i,:)=0.5*(zxx(i,:)+zxx(i-1,:))
           zyx(i,:)=0.5*(zyx(i,:)+zyx(i-1,:))
           zzx(i,:)=0.5*(zzx(i,:)+zzx(i-1,:))
       ENDDO


         xxx(0,:)=0.
         zxx(0,:)=0.
         zyx(0,:)=0.
         zzx(0,:)=0.


       DO i=n1x,1,-1
         xxx0(i)=0.5*(xxx0(i)+xxx0(i-1))
         zxx0(i)=0.5*(zxx0(i)+zxx0(i-1))
         zyx0(i)=0.5*(zyx0(i)+zyx0(i-1))
         zzx0(i)=0.5*(zzx0(i)+zzx0(i-1))
       ENDDO

       xxx0(0)=0.
       zxx0(0)=0.
       zxy0(0)=0.
       zxz0(0)=0.
 !
 !     xyy,zxy,zzy en (i,j+1/2)
 !     ------------------------
       DO j=n1y,1,-1
         xyy(:,j)=0.5*(xyy(:,j)+xyy(:,j-1))
         zxy(:,j)=0.5*(zxy(:,j)+zxy(:,j-1))
         zyy(:,j)=0.5*(zyy(:,j)+zyy(:,j-1))
         zzy(:,j)=0.5*(zzy(:,j)+zzy(:,j-1))
       ENDDO

         xyy(:,0)=xyy(:,ny)
         zxy(:,0)=zxy(:,ny)
         zyy(:,0)=zyy(:,ny)
         zzy(:,0)=zzy(:,ny)
 !
       return
!      _____________________
       END SUBROUTINE kizeta



       SUBROUTINE kizeta_components(s1, s2, s3, s4, ipmx, ipmy,
     &            chir1, chir2, zetar1, iesp)
! ======================================================================
! Dans cette routine on calcule la discr�tisation de chaque
! composante des tenseurs ki et zeta sur la grille 2d,
! et on calcul les composantes des tenseurs ki et zeta sur la grille 1d (suivant x)
! apr�s avoir moyenn� suivant y
!
! ======================================================================
       USE domaines
       USE champs
       USE particules
       USE particules_klder
       USE temps
       USE erreurs

       IMPLICIT NONE

       INTEGER, INTENT(IN)   ::   ipmx, ipmy, iesp

       REAL(kind=kr), INTENT(IN)  ::  s1, s2, s3, s4
       REAL(kind=kr), DIMENSION(1:3,1:3), INTENT(IN)  ::
     &    chir1, chir2, zetar1

       REAL(kind=kr)     ::  sd1, sd2, sd3, sd4

       INTEGER   ::  i, j


       sd1 = s1*dxi*dyi*abs(charge(iesp))
       sd2 = s2*dxi*dyi*abs(charge(iesp))
       sd3 = s3*dxi*dyi*abs(charge(iesp))
       sd4 = s4*dxi*dyi*abs(charge(iesp))
!       sd1 = s1
!       sd2 = s2
!       sd3 = s3
!       sd4 = s4

       i=ipmx ; j=ipmy

       xxxr1(i  ,j  ) = xxxr1(i  ,j  ) + sd1*chir1(1,1)
       xxxr1(i+1,j  ) = xxxr1(i+1,j  ) + sd2*chir1(1,1)
       xxxr1(i  ,j+1) = xxxr1(i  ,j+1) + sd3*chir1(1,1)
       xxxr1(i+1,j+1) = xxxr1(i+1,j+1) + sd4*chir1(1,1)

       xxyr1(i  ,j  ) = xxyr1(i  ,j  ) + sd1*chir1(1,2)
       xxyr1(i+1,j  ) = xxyr1(i+1,j  ) + sd2*chir1(1,2)
       xxyr1(i  ,j+1) = xxyr1(i  ,j+1) + sd3*chir1(1,2)
       xxyr1(i+1,j+1) = xxyr1(i+1,j+1) + sd4*chir1(1,2)

       xxzr1(i  ,j  ) = xxzr1(i  ,j  ) + sd1*chir1(1,3)
       xxzr1(i+1,j  ) = xxzr1(i+1,j  ) + sd2*chir1(1,3)
       xxzr1(i  ,j+1) = xxzr1(i  ,j+1) + sd3*chir1(1,3)
       xxzr1(i+1,j+1) = xxzr1(i+1,j+1) + sd4*chir1(1,3)

       xyxr1(i  ,j  ) = xyxr1(i  ,j  ) + sd1*chir1(2,1)
       xyxr1(i+1,j  ) = xyxr1(i+1,j  ) + sd2*chir1(2,1)
       xyxr1(i  ,j+1) = xyxr1(i  ,j+1) + sd3*chir1(2,1)
       xyxr1(i+1,j+1) = xyxr1(i+1,j+1) + sd4*chir1(2,1)

       xyyr1(i  ,j  ) = xyyr1(i  ,j  ) + sd1*chir1(2,2)
       xyyr1(i+1,j  ) = xyyr1(i+1,j  ) + sd2*chir1(2,2)
       xyyr1(i  ,j+1) = xyyr1(i  ,j+1) + sd3*chir1(2,2)
       xyyr1(i+1,j+1) = xyyr1(i+1,j+1) + sd4*chir1(2,2)

       xyzr1(i  ,j  ) = xyzr1(i  ,j  ) + sd1*chir1(2,3)
       xyzr1(i+1,j  ) = xyzr1(i+1,j  ) + sd2*chir1(2,3)
       xyzr1(i  ,j+1) = xyzr1(i  ,j+1) + sd3*chir1(2,3)
       xyzr1(i+1,j+1) = xyzr1(i+1,j+1) + sd4*chir1(2,3)

       xzxr1(i  ,j  ) = xzxr1(i  ,j  ) + sd1*chir1(3,1)
       xzxr1(i+1,j  ) = xzxr1(i+1,j  ) + sd2*chir1(3,1)
       xzxr1(i  ,j+1) = xzxr1(i  ,j+1) + sd3*chir1(3,1)
       xzxr1(i+1,j+1) = xzxr1(i+1,j+1) + sd4*chir1(3,1)

       xzyr1(i  ,j  ) = xzyr1(i  ,j  ) + sd1*chir1(3,2)
       xzyr1(i+1,j  ) = xzyr1(i+1,j  ) + sd2*chir1(3,2)
       xzyr1(i  ,j+1) = xzyr1(i  ,j+1) + sd3*chir1(3,2)
       xzyr1(i+1,j+1) = xzyr1(i+1,j+1) + sd4*chir1(3,2)

       xzzr1(i  ,j  ) = xzzr1(i  ,j  ) + sd1*chir1(3,3)
       xzzr1(i+1,j  ) = xzzr1(i+1,j  ) + sd2*chir1(3,3)
       xzzr1(i  ,j+1) = xzzr1(i  ,j+1) + sd3*chir1(3,3)
       xzzr1(i+1,j+1) = xzzr1(i+1,j+1) + sd4*chir1(3,3)


       xxxr2(i  ,j  ) = xxxr2(i  ,j  ) + sd1*chir2(1,1)
       xxxr2(i+1,j  ) = xxxr2(i+1,j  ) + sd2*chir2(1,1)
       xxxr2(i  ,j+1) = xxxr2(i  ,j+1) + sd3*chir2(1,1)
       xxxr2(i+1,j+1) = xxxr2(i+1,j+1) + sd4*chir2(1,1)

       xxyr2(i  ,j  ) = xxyr2(i  ,j  ) + sd1*chir2(1,2)
       xxyr2(i+1,j  ) = xxyr2(i+1,j  ) + sd2*chir2(1,2)
       xxyr2(i  ,j+1) = xxyr2(i  ,j+1) + sd3*chir2(1,2)
       xxyr2(i+1,j+1) = xxyr2(i+1,j+1) + sd4*chir2(1,2)

       xxzr2(i  ,j  ) = xxzr2(i  ,j  ) + sd1*chir2(1,3)
       xxzr2(i+1,j  ) = xxzr2(i+1,j  ) + sd2*chir2(1,3)
       xxzr2(i  ,j+1) = xxzr2(i  ,j+1) + sd3*chir2(1,3)
       xxzr2(i+1,j+1) = xxzr2(i+1,j+1) + sd4*chir2(1,3)

       xyxr2(i  ,j  ) = xyxr2(i  ,j  ) + sd1*chir2(2,1)
       xyxr2(i+1,j  ) = xyxr2(i+1,j  ) + sd2*chir2(2,1)
       xyxr2(i  ,j+1) = xyxr2(i  ,j+1) + sd3*chir2(2,1)
       xyxr2(i+1,j+1) = xyxr2(i+1,j+1) + sd4*chir2(2,1)

       xyyr2(i  ,j  ) = xyyr2(i  ,j  ) + sd1*chir2(2,2)
       xyyr2(i+1,j  ) = xyyr2(i+1,j  ) + sd2*chir2(2,2)
       xyyr2(i  ,j+1) = xyyr2(i  ,j+1) + sd3*chir2(2,2)
       xyyr2(i+1,j+1) = xyyr2(i+1,j+1) + sd4*chir2(2,2)

       xyzr2(i  ,j  ) = xyzr2(i  ,j  ) + sd1*chir2(2,3)
       xyzr2(i+1,j  ) = xyzr2(i+1,j  ) + sd2*chir2(2,3)
       xyzr2(i  ,j+1) = xyzr2(i  ,j+1) + sd3*chir2(2,3)
       xyzr2(i+1,j+1) = xyzr2(i+1,j+1) + sd4*chir2(2,3)

       xzxr2(i  ,j  ) = xzxr2(i  ,j  ) + sd1*chir2(3,1)
       xzxr2(i+1,j  ) = xzxr2(i+1,j  ) + sd2*chir2(3,1)
       xzxr2(i  ,j+1) = xzxr2(i  ,j+1) + sd3*chir2(3,1)
       xzxr2(i+1,j+1) = xzxr2(i+1,j+1) + sd4*chir2(3,1)

       xzyr2(i  ,j  ) = xzyr2(i  ,j  ) + sd1*chir2(3,2)
       xzyr2(i+1,j  ) = xzyr2(i+1,j  ) + sd2*chir2(3,2)
       xzyr2(i  ,j+1) = xzyr2(i  ,j+1) + sd3*chir2(3,2)
       xzyr2(i+1,j+1) = xzyr2(i+1,j+1) + sd4*chir2(3,2)

       xzzr2(i  ,j  ) = xzzr2(i  ,j  ) + sd1*chir2(3,3)
       xzzr2(i+1,j  ) = xzzr2(i+1,j  ) + sd2*chir2(3,3)
       xzzr2(i  ,j+1) = xzzr2(i  ,j+1) + sd3*chir2(3,3)
       xzzr2(i+1,j+1) = xzzr2(i+1,j+1) + sd4*chir2(3,3)


       zxxr1(i  ,j  ) = zxxr1(i  ,j  ) + sd1*zetar1(1,1)
       zxxr1(i+1,j  ) = zxxr1(i+1,j  ) + sd2*zetar1(1,1)
       zxxr1(i  ,j+1) = zxxr1(i  ,j+1) + sd3*zetar1(1,1)
       zxxr1(i+1,j+1) = zxxr1(i+1,j+1) + sd4*zetar1(1,1)

       zxyr1(i  ,j  ) = zxyr1(i  ,j  ) + sd1*zetar1(1,2)
       zxyr1(i+1,j  ) = zxyr1(i+1,j  ) + sd2*zetar1(1,2)
       zxyr1(i  ,j+1) = zxyr1(i  ,j+1) + sd3*zetar1(1,2)
       zxyr1(i+1,j+1) = zxyr1(i+1,j+1) + sd4*zetar1(1,2)

       zxzr1(i  ,j  ) = zxzr1(i  ,j  ) + sd1*zetar1(1,3)
       zxzr1(i+1,j  ) = zxzr1(i+1,j  ) + sd2*zetar1(1,3)
       zxzr1(i  ,j+1) = zxzr1(i  ,j+1) + sd3*zetar1(1,3)
       zxzr1(i+1,j+1) = zxzr1(i+1,j+1) + sd4*zetar1(1,3)

       zyxr1(i  ,j  ) = zyxr1(i  ,j  ) + sd1*zetar1(2,1)
       zyxr1(i+1,j  ) = zyxr1(i+1,j  ) + sd2*zetar1(2,1)
       zyxr1(i  ,j+1) = zyxr1(i  ,j+1) + sd3*zetar1(2,1)
       zyxr1(i+1,j+1) = zyxr1(i+1,j+1) + sd4*zetar1(2,1)

       zyyr1(i  ,j  ) = zyyr1(i  ,j  ) + sd1*zetar1(2,2)
       zyyr1(i+1,j  ) = zyyr1(i+1,j  ) + sd2*zetar1(2,2)
       zyyr1(i  ,j+1) = zyyr1(i  ,j+1) + sd3*zetar1(2,2)
       zyyr1(i+1,j+1) = zyyr1(i+1,j+1) + sd4*zetar1(2,2)

       zyzr1(i  ,j  ) = zyzr1(i  ,j  ) + sd1*zetar1(2,3)
       zyzr1(i+1,j  ) = zyzr1(i+1,j  ) + sd2*zetar1(2,3)
       zyzr1(i  ,j+1) = zyzr1(i  ,j+1) + sd3*zetar1(2,3)
       zyzr1(i+1,j+1) = zyzr1(i+1,j+1) + sd4*zetar1(2,3)

       zzxr1(i  ,j  ) = zzxr1(i  ,j  ) + sd1*zetar1(3,1)
       zzxr1(i+1,j  ) = zzxr1(i+1,j  ) + sd2*zetar1(3,1)
       zzxr1(i  ,j+1) = zzxr1(i  ,j+1) + sd3*zetar1(3,1)
       zzxr1(i+1,j+1) = zzxr1(i+1,j+1) + sd4*zetar1(3,1)

       zzyr1(i  ,j  ) = zzyr1(i  ,j  ) + sd1*zetar1(3,2)
       zzyr1(i+1,j  ) = zzyr1(i+1,j  ) + sd2*zetar1(3,2)
       zzyr1(i  ,j+1) = zzyr1(i  ,j+1) + sd3*zetar1(3,2)
       zzyr1(i+1,j+1) = zzyr1(i+1,j+1) + sd4*zetar1(3,2)

       zzzr1(i  ,j  ) = zzzr1(i  ,j  ) + sd1*zetar1(3,3)
       zzzr1(i+1,j  ) = zzzr1(i+1,j  ) + sd2*zetar1(3,3)
       zzzr1(i  ,j+1) = zzzr1(i  ,j+1) + sd3*zetar1(3,3)
       zzzr1(i+1,j+1) = zzzr1(i+1,j+1) + sd4*zetar1(3,3)


 !
       return
!      _____________________
       END SUBROUTINE kizeta_components



       SUBROUTINE kizeta_mean_components
! ======================================================================
! Dans cette routine on calcule la discr�tisation de chaque
! composante des tenseurs ki et zeta sur la grille 2d,
! et on calcul les composantes des tenseurs ki et zeta sur la grille 1d (suivant x)
! apr�s avoir moyenn� suivant y
!
! ======================================================================
       USE domaines
       USE champs
       USE particules
       USE particules_klder
       USE temps
       USE erreurs

       IMPLICIT NONE

       INTEGER   ::  i, j



       REAL(kind=kr)                      ::
     &    xaux


 !
 !     calcul des valeurs moyennes
 !
       xxx0r1(:)=0.
       xxy0r1(:)=0.
       xxz0r1(:)=0.
       xyx0r1(:)=0.
       xyy0r1(:)=0.
       xyz0r1(:)=0.
       xzx0r1(:)=0.
       xzy0r1(:)=0.
       xzz0r1(:)=0.

       xxx0r2(:)=0.
       xxy0r2(:)=0.
       xxz0r2(:)=0.
       xyx0r2(:)=0.
       xyy0r2(:)=0.
       xyz0r2(:)=0.
       xzx0r2(:)=0.
       xzy0r2(:)=0.
       xzz0r2(:)=0.

       zxx0r1(:)=0.
       zxy0r1(:)=0.
       zxz0r1(:)=0.
       zyx0r1(:)=0.
       zyy0r1(:)=0.
       zyz0r1(:)=0.
       zzx0r1(:)=0.
       zzy0r1(:)=0.
       zzz0r1(:)=0.


       DO j=1,ny
         DO i=0,n1x
         xxx0r1(i)=xxx0r1(i)+xxxr1(i,j)
         xxy0r1(i)=xxy0r1(i)+xxyr1(i,j)
         xxz0r1(i)=xxz0r1(i)+xxzr1(i,j)
         xyx0r1(i)=xyx0r1(i)+xyxr1(i,j)
         xyy0r1(i)=xyy0r1(i)+xyyr1(i,j)
         xyz0r1(i)=xyz0r1(i)+xyzr1(i,j)
         xzx0r1(i)=xzx0r1(i)+xzxr1(i,j)
         xzy0r1(i)=xzy0r1(i)+xzyr1(i,j)
         xzz0r1(i)=xzz0r1(i)+xzzr1(i,j)
 !
         xxx0r2(i)=xxx0r2(i)+xxxr2(i,j)
         xxy0r2(i)=xxy0r2(i)+xxyr2(i,j)
         xxz0r2(i)=xxz0r2(i)+xxzr2(i,j)
         xyx0r2(i)=xyx0r2(i)+xyxr2(i,j)
         xyy0r2(i)=xyy0r2(i)+xyyr2(i,j)
         xyz0r2(i)=xyz0r2(i)+xyzr2(i,j)
         xzx0r2(i)=xzx0r2(i)+xzxr2(i,j)
         xzy0r2(i)=xzy0r2(i)+xzyr2(i,j)
         xzz0r2(i)=xzz0r2(i)+xzzr2(i,j)
!
         zxx0r1(i)=zxx0r1(i)+zxxr1(i,j)
         zxy0r1(i)=zxy0r1(i)+zxyr1(i,j)
         zxz0r1(i)=zxz0r1(i)+zxzr1(i,j)
         zyx0r1(i)=zyx0r1(i)+zyxr1(i,j)
         zyy0r1(i)=zyy0r1(i)+zyyr1(i,j)
         zyz0r1(i)=zyz0r1(i)+zyzr1(i,j)
         zzx0r1(i)=zzx0r1(i)+zzxr1(i,j)
         zzy0r1(i)=zzy0r1(i)+zzyr1(i,j)
         zzz0r1(i)=zzz0r1(i)+zzzr1(i,j)
 !
         ENDDO
       ENDDO

       xaux = 1./ny

       DO i=0,n1x
         xxx0r1(i)=xaux*xxx0r1(i)
         xxy0r1(i)=xaux*xxy0r1(i)
         xxz0r1(i)=xaux*xxz0r1(i)
         xyx0r1(i)=xaux*xyx0r1(i)
         xyy0r1(i)=xaux*xyy0r1(i)
         xyz0r1(i)=xaux*xyz0r1(i)
         xzx0r1(i)=xaux*xzx0r1(i)
         xzy0r1(i)=xaux*xzy0r1(i)
         xzz0r1(i)=xaux*xzz0r1(i)
 !
         xxx0r2(i)=xaux*xxx0r2(i)
         xxy0r2(i)=xaux*xxy0r2(i)
         xxz0r2(i)=xaux*xxz0r2(i)
         xyx0r2(i)=xaux*xyx0r2(i)
         xyy0r2(i)=xaux*xyy0r2(i)
         xyz0r2(i)=xaux*xyz0r2(i)
         xzx0r2(i)=xaux*xzx0r2(i)
         xzy0r2(i)=xaux*xzy0r2(i)
         xzz0r2(i)=xaux*xzz0r2(i)
!
         zxx0r1(i)=xaux*zxx0r1(i)
         zxy0r1(i)=xaux*zxy0r1(i)
         zxz0r1(i)=xaux*zxz0r1(i)
         zyx0r1(i)=xaux*zyx0r1(i)
         zyy0r1(i)=xaux*zyy0r1(i)
         zyz0r1(i)=xaux*zyz0r1(i)
         zzx0r1(i)=xaux*zzx0r1(i)
         zzy0r1(i)=xaux*zzy0r1(i)
         zzz0r1(i)=xaux*zzz0r1(i)
       ENDDO

 !
 !
 !     on va positionner les ki et zeta a leur position
 !     pour la resolution de maxwell
 !
 !     xxx,zyx,zzx en (i+1/2,j).
 !     -------------------------
       DO i=n1x,1,-1
         xxxr1(i,:)=0.5*(xxxr1(i,:)+xxxr1(i-1,:))
         xxxr2(i,:)=0.5*(xxxr2(i,:)+xxxr2(i-1,:))

         zxxr1(i,:)=0.5*(zxxr1(i,:)+zxxr1(i-1,:))
         zyxr1(i,:)=0.5*(zyxr1(i,:)+zyxr1(i-1,:))
         zzxr1(i,:)=0.5*(zzxr1(i,:)+zzxr1(i-1,:))
       ENDDO

         xxxr1(0,:)=0.
         xxxr2(0,:)=0.
         zxxr1(0,:)=0.
         zyxr1(0,:)=0.
         zzxr1(0,:)=0.

       DO i=n1x,1,-1
         xxx0r1(i)=0.5*(xxx0r1(i)+xxx0r1(i-1))
         xxx0r2(i)=0.5*(xxx0r2(i)+xxx0r2(i-1))

         zxx0r1(i)=0.5*(zxx0r1(i)+zxx0r1(i-1))
         zyx0r1(i)=0.5*(zyx0r1(i)+zyx0r1(i-1))
         zzx0r1(i)=0.5*(zzx0r1(i)+zzx0r1(i-1))
       ENDDO

       xxx0r1(0)=0.
       xxx0r2(0)=0.
       zxx0r1(0)=0.
       zxy0r1(0)=0.
       zxz0r1(0)=0.
 !
 !     xyy,zxy,zzy en (i,j+1/2)
 !     ------------------------
       DO j=n1y,1,-1
         xyyr1(:,j)=0.5*(xyyr1(:,j)+xyyr1(:,j-1))
         xyyr2(:,j)=0.5*(xyyr2(:,j)+xyyr2(:,j-1))
         zxyr1(:,j)=0.5*(zxyr1(:,j)+zxyr1(:,j-1))
         zyyr1(:,j)=0.5*(zyyr1(:,j)+zyyr1(:,j-1))
         zzyr1(:,j)=0.5*(zzyr1(:,j)+zzyr1(:,j-1))
       ENDDO

       xyyr1(:,0)=xyyr1(:,ny)
       xyyr2(:,0)=xyyr2(:,ny)
       zxyr1(:,0)=zxyr1(:,ny)
       zyyr1(:,0)=zyyr1(:,ny)
       zzyr1(:,0)=zzyr1(:,ny)

 !
       return
!      _____________________
       END SUBROUTINE kizeta_mean_components
