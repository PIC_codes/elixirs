! ======================================================================
! maxlim.f
! --------
!
!     SUBROUTINE maxlim_2d1s2
!     FUNCTION calc_profil
!     FUNCTION sqrt_gauss
!     FUNCTION plateau
!     FUNCTION sqrt_cos_car
!
! ======================================================================




      SUBROUTINE maxlim_2d1s2_nEB_implicit_huygens(tps)
! ======================================================================
! Application des conditions aux limites pour les 6 champs [EB][xyz]
! a l'instant reel temps
! Retourne le flux de Poynting entre (source)
! Le type de condition est code dans les variables ibndc(:)
!   ibndc >  0 : champs echanges avec un PE voisin
!   ibndc = -1 : conditions periodiques locales a ce PE
!   ibndc = -2 : absorption - condition de Silver-Muller
!   ibndc = -3 : conducteur parfait
! ======================================================================
      USE precisions
      USE domaines
      USE temps
      USE champs
      USE laser



      IMPLICIT NONE

      REAL(KIND=kr), INTENT(IN)     :: tps

      
!      REAL(KIND=kr) :: coefy_ez, coefy_bx0,coefy_bx1,coefy_by, coefy_sr,
!     &                 coefz_ey, coefz_bx0,coefz_bx1,coefz_bz 
     
      REAL(KIND=kr) ::  eylaserqx, eylaserqy, ezlaserqz 
     
      REAL(KIND=kr) :: coef_qx1, coef_qy1, coef_qz1 
      REAL(KIND=kr) :: coef_qx2, coef_qy2, coef_qz2
    
      REAL(KIND=kr) :: coef_t, coef_y, phase_y, phase_z
      REAL(KIND=kr) :: posy, auxpy_sr 
      REAL(KIND=kr) :: tps_eff, tretard, bordy

      REAL(KIND=kr) :: posy2, coef_y2, tretard2, tps_eff2, 
     &                  coef_t2, phase_y2, phase_z2 

      REAL(KIND=kr) :: tretard3, tps_eff3, coef_t3, phase_z3 
     
      REAL(KIND=kr) :: tretard4, tps_eff4, coef_t4, phase_z4 

      INTEGER       :: j, ilas 

      INTERFACE
        FUNCTION calc_profil(x, type, ordre, xlarg, xdeb, xcentre)
          USE precisions
          REAL(KIND=kr), INTENT(IN)     :: x
          CHARACTER(LEN=5), INTENT(IN)  :: type
          INTEGER,       INTENT(IN)     :: ordre
          REAL(KIND=kr), INTENT(IN)     :: xlarg, xdeb, xcentre
          REAL(KIND=kr)                 :: calc_profil
        END FUNCTION calc_profil
      END INTERFACE



      fpsr = 0._8
! --- On fait rentrer les sources l'une apres l'autre
      DO ilas=1,3
        IF(inj_laser(ilas).NE.0) THEN
          IF(inj_laser(ilas).EQ.1) nulx_inj = nx
          IF(    (y_prof(ilas).EQ.'gauss')
     &       .OR.(y_prof(ilas).EQ.'hyper')
     &       .OR.(y_prof(ilas).EQ.'cos_2')) THEN
            bordy = y_max(ilas) - y_lrg(ilas)
          ELSEIF(y_prof(ilas).EQ.'plato') THEN
            bordy = y_max(ilas) - 0.5*y_lrg(ilas) - y_deb(ilas)
          ELSE 
            bordy = ymin_gl
          ENDIF


! ---- 
!      On suppose que l angle du laser inject� et l angle de l'onde 
!      diffus�e sont �gaux
! ------------
          coef_qx1 = -0.5*d3*keb_a*COS(scat_angle)*dtsdx
	  coef_qx2 = -0.25*d3*keb_a
	  
          coef_qy1 = dtsdx2*keb_a*COS(scat_angle)*dtsdx
	  coef_qy2 = 0.5*dtsdx2*keb_a
	  
          coef_qz1 = 0.5*dtsdx2*keb_b
	  coef_qz2 = dtsdx2*keb_b*dtsdx/COS(scat_angle)

! ---- 
!  Mise � jour du champ By, il faut prendre en compte bx incident et 
!  by incident
! ---------------------------
          DO j=1,ny
            posy = ymin_gl + j*pasdy
! --- + attention si conditions transverses periodiques
! ---   A FAIRE !!!!!! ??????
! --- + facteur spatial (y)
            coef_y =  calc_profil(posy,y_prof(ilas),y_ord(ilas), 
     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
     
! --- + facteur temporel et phase
            tretard = SIN(y_ang(ilas))*(posy-y_max(ilas))
     &              + COS(y_ang(ilas))*(xmin_gl)
            tps_eff = tps - tretard
            coef_t =  calc_profil(tps_eff,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_y   = COS(tps_eff/lambda(ilas))

! --- + facteur temporel et phase
            tps_eff2 = tps - tretard - pasdt
            coef_t2 =  calc_profil(tps_eff2,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_y2   = COS(tps_eff2/lambda(ilas))
 
! ----- polarisation 1     
            ezlaserqz = a_0(ilas)*y_pol(ilas)*coef_y
     &        *( coef_qz1*coef_t*phase_y + coef_qz2*coef_t2*phase_y2 )       
        
	    qz(1,j) = qz(1,j) + ezlaserqz
   
            auxpy_sr = 1._8
            IF((j.EQ.nulmy).OR.(j.EQ.nulpy)) auxpy_sr = 0.5_8
            fpsr =  fpsr 
     &            + auxpy_sr*(a_0(ilas)*phase_y*y_pol(ilas)
     &                                 *coef_t*coef_y      )**2
          ENDDO


! ---- 
!  Mise � jour du champ Bz, 
!  il faut prendre en compte bz incident 
! ---------------------------
          DO j=1,ny
            posy = ymin_gl + (j-0.5_8)*pasdy
! --- + attention si conditions transverses periodiques
            IF( j.EQ.nulmy .AND. ibndc(3).EQ.-1 )
     &          posy = ymax_gl - 0.5_8*pasdy
            IF( j.EQ.nulpy+1 .AND. ibndc(4).EQ.-1 )
     &          posy = ymin_gl + 0.5_8*pasdy
     
            posy2 = ymin_gl + (j+0.5_8)*pasdy
    
! --- + facteur spatial (y)
            coef_y =  calc_profil(posy,y_prof(ilas),y_ord(ilas), 
     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
     
            coef_y2 =  calc_profil(posy2,y_prof(ilas),y_ord(ilas), 
     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))        
    
! --- + facteur temporel et phase
            tretard = SIN(y_ang(ilas))*(posy-y_max(ilas))
     &              + COS(y_ang(ilas))*xmin_gl
            tps_eff = tps - tretard - pasdt
            coef_t =  calc_profil(tps_eff,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_z = COS(tps_eff/lambda(ilas))
  
! --- + facteur temporel et phase 2
            tretard2 = SIN(y_ang(ilas))*(posy2-y_max(ilas))
     &              + COS(y_ang(ilas))*xmin_gl
            tps_eff2 = tps - tretard2 - pasdt
            coef_t2 =  calc_profil(tps_eff2,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_z2 = COS(tps_eff2/lambda(ilas))

! --- + facteur temporel et phase 3
            tretard3 = SIN(y_ang(ilas))*(posy-y_max(ilas))
     &              + COS(y_ang(ilas))*xmin_gl
            tps_eff3 = tps - tretard3
            coef_t3 =  calc_profil(tps_eff3,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_z3 = COS(tps_eff3/lambda(ilas))
	    
! --- + facteur temporel et phase 4
            tretard4 = SIN(y_ang(ilas))*(posy2-y_max(ilas))
     &              + COS(y_ang(ilas))*xmin_gl
            tps_eff4 = tps - tretard4
            coef_t4 =  calc_profil(tps_eff4,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_z4 = COS(tps_eff4/lambda(ilas))
  
! ----- polarisation 2           
	    eylaserqx = a_0(ilas)*z_pol(ilas)
     &            *(coef_qx1*(phase_z*coef_t*coef_y 
     &                      - phase_z2*coef_t2*coef_y2)
     &            + coef_qx2*(phase_z3*coef_t3*coef_y 
     &                      - phase_z4*coef_t4*coef_y2)) 
     
	    	    
	    eylaserqy = a_0(ilas)*z_pol(ilas)
     &            *(coef_qy1*(phase_z2*coef_t2*coef_y2)
     &            + coef_qy2*(phase_z4*coef_t4*coef_y2)) 
	    
            qx(1,j) = qx(1,j) + eylaserqx
            qy(1,j) = qy(1,j) + eylaserqy
   

            auxpy_sr = 1._8
            IF((j.EQ.nulmy).OR.(j.EQ.nulpy+1)) auxpy_sr = 0._8
            fpsr =  fpsr 
     &            + auxpy_sr*(a_0(ilas)*phase_z*z_pol(ilas)
     &                                 *coef_t*coef_y      )**2
          ENDDO
	  
        ENDIF
      ENDDO


!     ___________________________
      END SUBROUTINE maxlim_2d1s2_nEB_implicit_huygens




      SUBROUTINE maxlim_2d1s2_nEB_implicit_huygens_step2(tps)
! ======================================================================
! Application des conditions aux limites pour les 6 champs [EB][xyz]
! a l'instant reel temps
! Retourne le flux de Poynting entre (source)
! Le type de condition est code dans les variables ibndc(:)
!   ibndc >  0 : champs echanges avec un PE voisin
!   ibndc = -1 : conditions periodiques locales a ce PE
!   ibndc = -2 : absorption - condition de Silver-Muller
!   ibndc = -3 : conducteur parfait
! ======================================================================
      USE precisions
      USE domaines
      USE temps
      USE champs
      USE laser



      IMPLICIT NONE

      REAL(KIND=kr), INTENT(IN)     :: tps

      
!      REAL(KIND=kr) :: coefy_ez, coefy_bx0,coefy_bx1,coefy_by, coefy_sr,
!     &                 coefz_ey, coefz_bx0,coefz_bx1,coefz_bz 
     
      REAL(KIND=kr) :: eylaser, ezlaser 
     
      REAL(KIND=kr) :: coef_ey1, coef_ez1 
      REAL(KIND=kr) :: coef_ey2, coef_ez2 
    
      REAL(KIND=kr) :: coef_t, coef_y, phase_y, phase_z
      REAL(KIND=kr) :: coef_t2, phase_y2, phase_z2
      REAL(KIND=kr) :: posy, auxpy_sr 
      REAL(KIND=kr) :: tps_eff, tretard, bordy
      REAL(KIND=kr) :: tps_eff2, tretard2 

      INTEGER       :: j, ilas 

      INTERFACE
        FUNCTION calc_profil(x, type, ordre, xlarg, xdeb, xcentre)
          USE precisions
          REAL(KIND=kr), INTENT(IN)     :: x
          CHARACTER(LEN=5), INTENT(IN)  :: type
          INTEGER,       INTENT(IN)     :: ordre
          REAL(KIND=kr), INTENT(IN)     :: xlarg, xdeb, xcentre
          REAL(KIND=kr)                 :: calc_profil
        END FUNCTION calc_profil
      END INTERFACE



      fpsr = 0._8
! --- On fait rentrer les sources l'une apres l'autre
      DO ilas=1,3
        IF(inj_laser(ilas).NE.0) THEN
          IF(inj_laser(ilas).EQ.1) nulx_inj = nx
          IF(    (y_prof(ilas).EQ.'gauss')
     &       .OR.(y_prof(ilas).EQ.'hyper')
     &       .OR.(y_prof(ilas).EQ.'cos_2')) THEN
            bordy = y_max(ilas) - y_lrg(ilas)
          ELSEIF(y_prof(ilas).EQ.'plato') THEN
            bordy = y_max(ilas) - 0.5*y_lrg(ilas) - y_deb(ilas)
          ELSE 
            bordy = ymin_gl
          ENDIF


! ---- 
!      On suppose que l angle du laser inject� et l angle de l'onde 
!      diffus�e sont �gaux
! ------------
          coef_ey1 = 2.*keb_a*COS(scat_angle)*dtsdx 
          coef_ey2 = keb_a	  
	  
          coef_ez1 = 2.*keb_b*dtsdx/COS(scat_angle)
	  coef_ez2 = keb_b

! ---- 
!  Mise � jour du champ By, il faut prendre en compte bx incident et 
!  by incident
! ---------------------------
          DO j=1,ny
            posy = ymin_gl + j*pasdy
! --- + attention si conditions transverses periodiques
! ---   A FAIRE !!!!!! ??????
! --- + facteur spatial (y)
            coef_y =  calc_profil(posy,y_prof(ilas),y_ord(ilas), 
     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
     
! --- + facteur temporel et phase
            tretard = SIN(y_ang(ilas))*(posy-y_max(ilas))
     &              + COS(y_ang(ilas))*xmin_gl 
            tps_eff = tps - tretard - pasdt
            coef_t =  calc_profil(tps_eff,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_y   = COS(tps_eff/lambda(ilas))

! --- + facteur temporel et phase 2
            tretard2 = SIN(y_ang(ilas))*(posy-y_max(ilas))
     &              + COS(y_ang(ilas))*xmin_gl 
            tps_eff2 = tps - tretard2
            coef_t2 =  calc_profil(tps_eff2,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_y2   = COS(tps_eff2/lambda(ilas))
 
! ----- polarisation 1     
            ezlaser = a_0(ilas)*coef_y*y_pol(ilas)
     &        *(coef_ez1*coef_t*phase_y + coef_ez2*coef_t2*phase_y2)
        
	    ez(nulx_inj,j) = ez(nulx_inj,j) + ezlaser 
   
            auxpy_sr = 1._8
            IF((j.EQ.nulmy).OR.(j.EQ.nulpy)) auxpy_sr = 0.5_8
            fpsr =  fpsr 
     &            + auxpy_sr*(a_0(ilas)*phase_y*y_pol(ilas)
     &                                 *coef_t*coef_y      )**2
          ENDDO


! ---- 
!  Mise � jour du champ Bz, 
!  il faut prendre en compte bz incident 
! ---------------------------
          DO j=1,ny
            posy = ymin_gl + (j-0.5_8)*pasdy
! --- + attention si conditions transverses periodiques
            IF( j.EQ.nulmy .AND. ibndc(3).EQ.-1 )
     &          posy = ymax_gl - 0.5_8*pasdy
            IF( j.EQ.nulpy+1 .AND. ibndc(4).EQ.-1 )
     &          posy = ymin_gl + 0.5_8*pasdy    
    
! --- + facteur spatial (y)
            coef_y =  calc_profil(posy,y_prof(ilas),y_ord(ilas), 
     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
   
! --- + facteur temporel et phase
            tretard = SIN(y_ang(ilas))*(posy-y_max(ilas))
     &              + COS(y_ang(ilas))*xmin_gl 
            tps_eff = tps - tretard - pasdt
            coef_t =  calc_profil(tps_eff,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_z = COS(tps_eff/lambda(ilas))
  
! --- + facteur temporel et phase 2
            tretard2 = tretard 
            tps_eff2 = tps - tretard2
            coef_t2 =  calc_profil(tps_eff2,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_z2   = COS(tps_eff2/lambda(ilas))
  
! ----- polarisation 2            	    
	    eylaser = a_0(ilas)*z_pol(ilas)*coef_y
     &        *(phase_z*coef_t*coef_ey1 + phase_z2*coef_t2*coef_ey2) 
          
	    
            ey(nulx_inj,j) = ey(nulx_inj,j) + eylaser 	       

            auxpy_sr = 1._8
            IF((j.EQ.nulmy).OR.(j.EQ.nulpy+1)) auxpy_sr = 0._8
            fpsr =  fpsr 
     &            + auxpy_sr*(a_0(ilas)*phase_z*z_pol(ilas)
     &                                 *coef_t*coef_y      )**2
          ENDDO
	  
        ENDIF
      ENDDO


!     ___________________________
      END SUBROUTINE maxlim_2d1s2_nEB_implicit_huygens_step2



      SUBROUTINE maxlim_2d1s2(tps)
! ======================================================================
! Application des conditions aux limites pour les 6 champs [EB][xyz]
! a l'instant reel temps
! Retourne le flux de Poynting entre (source)
! Le type de condition est code dans les variables ibndc(:)
!   ibndc >  0 : champs echanges avec un PE voisin
!   ibndc = -1 : conditions periodiques locales a ce PE
!   ibndc = -2 : absorption - condition de Silver-Muller
!   ibndc = -3 : conducteur parfait
! ======================================================================
      USE precisions
      USE domaines
      USE temps
      USE champs
      USE laser



      IMPLICIT NONE

      REAL(KIND=kr), INTENT(IN)     :: tps

      
!      REAL(KIND=kr) :: coefy_ez, coefy_bx0,coefy_bx1,coefy_by, coefy_sr,
!     &                 coefz_ey, coefz_bx0,coefz_bx1,coefz_bz 
     
      REAL(KIND=kr) :: coefp1_bx, coefp1_by 
      REAL(KIND=kr) :: coefp2_bx, coefp2_by, coefp2_bz
      REAL(KIND=kr) :: bx_inc, by_inc, bz_inc
      
      REAL(KIND=kr) :: coef_t, coef_y, phase_y, phase_z
      REAL(KIND=kr) :: posy, auxpy_sr 
      REAL(KIND=kr) :: tps_eff, tretard, bordy
      INTEGER       :: j, ilas 

      INTERFACE
        FUNCTION calc_profil(x, type, ordre, xlarg, xdeb, xcentre)
          USE precisions
          REAL(KIND=kr), INTENT(IN)     :: x
          CHARACTER(LEN=5), INTENT(IN)  :: type
          INTEGER,       INTENT(IN)     :: ordre
          REAL(KIND=kr), INTENT(IN)     :: xlarg, xdeb, xcentre
          REAL(KIND=kr)                 :: calc_profil
        END FUNCTION calc_profil
      END INTERFACE



      fpsr = 0._8
! --- On fait rentrer les sources l'une apres l'autre
      DO ilas=1,3
        IF(inj_laser(ilas).NE.0) THEN

          IF(inj_laser(ilas).EQ.1) nulx_inj = nx
          IF(    (y_prof(ilas).EQ.'gauss')
     &       .OR.(y_prof(ilas).EQ.'hyper')
     &       .OR.(y_prof(ilas).EQ.'cos_2')) THEN
            bordy = y_max(ilas) - y_lrg(ilas)
          ELSEIF(y_prof(ilas).EQ.'plato') THEN
            bordy = y_max(ilas) - 0.5*y_lrg(ilas) - y_deb(ilas)
          ELSE 
            bordy = ymin_gl
          ENDIF

          coefp1_bx = a_0(ilas)*SIN(y_ang(ilas))
          coefp1_by = a_0(ilas)
!          coefp1_by = -a_0(ilas)*COS(y_ang(ilas))

          coefp2_bx = a_0(ilas)*COS(y_ang(ilas))*SIN(z_ang(ilas))
          coefp2_by = a_0(ilas)*SIN(y_ang(ilas))*SIN(z_ang(ilas))
          coefp2_bz = a_0(ilas)*COS(z_ang(ilas))	  	  

! ---- 
!  Mise � jour du champ By, il faut prendre en compte bx incident et 
!  by incident
! ---------------------------
          DO j=1,ny
            posy = ymin_gl + j*pasdy
! --- + attention si conditions transverses periodiques
! ---   A FAIRE !!!!!! ??????
! --- + facteur spatial (y)
            coef_y =  calc_profil(posy,y_prof(ilas),y_ord(ilas), 
     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
     
! --- + facteur temporel et phase
            tretard = SIN(y_ang(ilas))*(posy-y_max(ilas))
            tps_eff = tps - tretard
            coef_t =  calc_profil(tps_eff,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_y = SIN(tps_eff/lambda(ilas))
 
! ----- polarisation 1     
            by_inc = coefp1_by*phase_y*y_pol(ilas)
     &                 *coef_t *coef_y
     
! ----- polarisation 2              
            by_inc = by_inc + coefp2_by*phase_y*z_pol(ilas)
     &                 *coef_t *coef_y
 
            by(nulx_inj,j) = by(nulx_inj,j) - dts2dx*by_inc   

            auxpy_sr = 1._8
            IF((j.EQ.nulmy).OR.(j.EQ.nulpy)) auxpy_sr = 0.5_8
            fpsr =  fpsr 
     &            + auxpy_sr*(a_0(ilas)*phase_y*y_pol(ilas)
     &                                 *coef_t*coef_y      )**2
          ENDDO

! ---- 
!  Mise � jour du champ Bz, 
!  il faut prendre en compte bz incident 
! ---------------------------
          DO j=1,ny
            posy = ymin_gl + (j-0.5_8)*pasdy
! --- + attention si conditions transverses periodiques
            IF( j.EQ.nulmy .AND. ibndc(3).EQ.-1 )
     &          posy = ymax_gl - 0.5_8*pasdy
            IF( j.EQ.nulpy+1 .AND. ibndc(4).EQ.-1 )
     &          posy = ymin_gl + 0.5_8*pasdy
! --- + facteur spatial (y)
            coef_y =  calc_profil(posy,y_prof(ilas),y_ord(ilas), 
     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
! --- + facteur temporel et phase
            tretard = SIN(y_ang(ilas))*(posy-y_max(ilas))
            tps_eff = tps - tretard
            coef_t =  calc_profil(tps_eff,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_z = SIN(tps_eff/lambda(ilas))
   
! ----- polarisation 1 
            bx_inc = coefp1_bx*phase_z*y_pol(ilas)
     &                  *coef_t *coef_y
   
! ----- polarisation 2        
            bx_inc = bx_inc + coefp2_bx*phase_z*z_pol(ilas)
     &                  *coef_t *coef_y
 
            bz_inc = coefp2_bz*phase_z*z_pol(ilas)
     &                 *coef_t *coef_y
 
!            bx(nulx_inj-1,j) = bx_tn(nulx_inj-1,j) - bx_inc   	    
	    
	    bz(nulx_inj,j) = bz(nulx_inj,j) - dtsdx*bz_inc

            auxpy_sr = 1._8
            IF((j.EQ.nulmy).OR.(j.EQ.nulpy+1)) auxpy_sr = 0._8
            fpsr =  fpsr 
     &            + auxpy_sr*(a_0(ilas)*phase_z*z_pol(ilas)
     &                                 *coef_t*coef_y      )**2
          ENDDO
	  
        ENDIF
      ENDDO


!     ___________________________
      END SUBROUTINE maxlim_2d1s2



      FUNCTION calc_profil(x, type, ordre, xlarg, xdeb, xcentre)
! ======================================================================
! Valeur (entre 0 et 1) en x de la fonction correspondant au profil
! "type" pour les parametres donnes ensuite
!
! - Exposant 0.25 en 2D car on est en 2D...
! - Exposant 0.25 en 3D car on passe dans ce calcul pour les deux
!   dimensions transverses, et c'est le produit qui compte...
! ======================================================================
      USE precisions
      IMPLICIT NONE
      REAL(KIND=kr), INTENT(IN)     :: x
      CHARACTER(LEN=5), INTENT(IN)  :: type
      INTEGER,       INTENT(IN)     :: ordre
      REAL(KIND=kr), INTENT(IN)     :: xlarg, xdeb, xcentre
      REAL(KIND=kr)                 :: calc_profil

      REAL(KIND=kr)                 :: x_sur_Zr_2

      INTERFACE
        FUNCTION sqrt_gauss(x, ordre, fwhm, xcentre)
          USE precisions
          REAL(KIND=kr), INTENT(IN) :: x, fwhm, xcentre
          INTEGER,       INTENT(IN) :: ordre
          REAL(KIND=kr)             :: sqrt_gauss
        END FUNCTION sqrt_gauss
        FUNCTION sqrt_skew(x,skew,fwhm,xcentre)
          USE precisions
          REAL(KIND=kr), INTENT(IN) :: x, fwhm, xcentre, skew
          REAL(KIND=kr)             :: sqrt_skew
        END FUNCTION sqrt_skew  
        FUNCTION plateau(x, xramp, xplat, xcentre)
          USE precisions
          REAL(KIND=kr), INTENT(IN) :: x, xramp, xplat, xcentre
          REAL(KIND=kr)             :: plateau
        END FUNCTION plateau
        FUNCTION sqrt_cos_car(x, xlarg, xcentre)
          USE precisions
          REAL(KIND=kr), INTENT(IN) :: x, xlarg, xcentre
          REAL(KIND=kr)             :: sqrt_cos_car
        END FUNCTION sqrt_cos_car
      END INTERFACE


      SELECT CASE(type)
      CASE('plato')
        calc_profil = plateau(x, xdeb, xlarg, xcentre)
      CASE('cos_2')
        calc_profil = sqrt_cos_car(x, xlarg, xcentre)
      CASE('gauss')
        calc_profil = sqrt_gauss(x,     2, xlarg, xcentre)
      CASE('skewd')
        calc_profil = sqrt_skew(x, xdeb, xlarg, xcentre)
      CASE('hyper')
        calc_profil = sqrt_gauss(x, ordre, xlarg, xcentre)
      CASE('foc2d')
        x_sur_Zr_2 = 16._8*LOG(2._8)*LOG(2._8)*xdeb*xdeb
     &              /(xlarg**4)
        calc_profil = EXP(-2.*LOG(2._8)*((x-xcentre)/xlarg)**2
     &                       /(1._8+x_sur_Zr_2)               )
     .               /(1._8+x_sur_Zr_2)**0.25_8
      CASE('foc3d')
        x_sur_Zr_2 = 16._8*LOG(2._8)*LOG(2._8)*xdeb*xdeb
     &              /(xlarg**4)
        calc_profil = EXP(-2.*LOG(2._8)*((x-xcentre)/xlarg)**2
     &                       /(1._8+x_sur_Zr_2)               )
     .               /(1._8+x_sur_Zr_2)**0.25_8
      CASE DEFAULT
        calc_profil = 1._8
      END SELECT
      RETURN
!     ________________________
      END FUNCTION calc_profil


      FUNCTION sqrt_gauss(x, ordre, fwhm, xcentre)
! ======================================================================
! Valeur (entre 0 et 1) en x de la fonction qui, ELEVEE AU CARRE, 
! donne une Gaussienne d'ordre ordre, de largeur a mi-hauteur fwhm 
! et centree en xcentre
! ======================================================================
      USE precisions
      IMPLICIT NONE
      REAL(KIND=kr), INTENT(IN) :: x, fwhm, xcentre
      INTEGER,       INTENT(IN) :: ordre
      REAL(KIND=kr)             :: sqrt_gauss

      sqrt_gauss = EXP( -0.5*LOG(2._8)*(2._8*(x-xcentre)/fwhm)**ordre )
      RETURN
!     _______________________
      END FUNCTION sqrt_gauss


      FUNCTION sqrt_skew(x, skew, fwhm, xcentre)
! ======================================================================
! Valeur (entre 0 et 1) en x de la fonction qui, ELEVEE AU CARRE, 
! donne une Gaussienne d'ordre 2, de largeur a mi-hauteur fwhm 
! et centree en xcentre, de skew skew
! ======================================================================
      USE precisions
      IMPLICIT NONE
      REAL(KIND=kr), INTENT(IN) :: x, fwhm, xcentre, skew
      REAL(KIND=kr)             :: twhm, hwhm
      REAL(KIND=kr)             :: sqrt_skew

      hwhm = fwhm/(skew+2)
      twhm = fwhm - fwhm/(skew+2)
      IF(x.LE.xcentre) THEN
        sqrt_skew = EXP( -0.5*LOG(2._8)*((x-xcentre)/hwhm)**2 )
      ELSE
        sqrt_skew = EXP( -0.5*LOG(2._8)*((x-xcentre)/twhm)**2 )
      ENDIF
      RETURN
!     _______________________
      END FUNCTION sqrt_skew


      FUNCTION plateau(x, xramp, xplat, xcentre)
! ======================================================================
! Valeur (entre 0 et 1) en x de la fonction plateau centree en xcentre,
! constante a 1 pendant xplat et encadree de rampes lineaires de
! longueurs xramp 
! ======================================================================
      USE precisions
      IMPLICIT NONE
      REAL(KIND=kr), INTENT(IN) :: x, xramp, xplat, xcentre
      REAL(KIND=kr)             :: plateau
      REAL(KIND=kr)             :: aux, aux_plat

      aux = ABS(x-xcentre)
      aux_plat = 0.5*xplat
      IF(aux.LE.aux_plat) THEN
        plateau = 1.
      ELSEIF(aux.LE.(aux_plat+xramp)) THEN
        plateau = 1.-(aux-aux_plat)/xramp
      ELSE
        plateau = 0.
      ENDIF
      RETURN
!     ____________________
      END FUNCTION plateau


      FUNCTION sqrt_cos_car(x, xlarg, xcentre)
! ======================================================================
! Valeur (entre 0 et 1) en x de la fonction qui, ELEVEE AU CARRE, 
! donne un cosinus-carre de largeur totale xlarg et centre en xcentre
! ======================================================================
      USE precisions
      IMPLICIT NONE
      REAL(KIND=kr), INTENT(IN) :: x, xlarg, xcentre
      REAL(KIND=kr)             :: sqrt_cos_car
      REAL(KIND=8), PARAMETER   :: pi = 3.1415926536
      REAL(KIND=kr)             :: aux

      aux = ABS((x-xcentre)/xlarg)
      IF(aux.LE.0.5) THEN
        sqrt_cos_car = COS( pi*aux )
      ELSE
        sqrt_cos_car = 0.
      ENDIF
      RETURN
!     _________________________
      END FUNCTION sqrt_cos_car
