! ======================================================================
! calc_donn.f
! -----------
!
!     SUBROUTINE calc_donn
!  
!  Modif diag_phase juillet 2005
!  Modif typ_dist[xyz],vder[xyz] octobre 2005 
!
! ======================================================================

      SUBROUTINE calc_donn
! ======================================================================
! Premier traitement (avant allocation de la plupart des tableaux)
! des donnees lues dans le fichier data
! ======================================================================

!      USE champs
!      USE champs_sub      
      
      USE domaines
      USE domaines_klder      
      USE temps
      USE particules_klder      
      USE champs      
      USE laser
      USE diagnostics_klder
      USE diag_ptest
      USE diag_moyen      
      USE erreurs

      IMPLICIT NONE
!!      INCLUDE 'mpif.h'

      INTEGER                    :: iesp
!      INTEGER                    :: iproc, iprocx, iprocy, iprocz
!      INTEGER                    :: dist_x, dist_y, dist_z
      INTEGER, DIMENSION(3)      :: dsp_pas
      INTEGER                    :: dsp_i, dph_i, iax, ix
      REAL(KIND=8)               :: ox,oxmax,gder,vthder

! Nombre de processeurs et position dans la partition
! ----------------------------------------------------------------------
      IF((nprocx*nprocy*nprocz).NE.nproc) THEN
        IF(numproc.EQ.1) THEN
          WRITE(fmsgo,"('Subroutine calc_donn : K_lder a ete lance')")
          WRITE(fmsgo,"('avec ',I3,' PEs, mais la partition')") nproc
        WRITE(fmsgo,"('deduite du fichier de donnees est ',3(I3,1X))")
     &             nprocx, nprocy, nprocz
        ENDIF

      ENDIF

      IF(debug.GE.1) THEN
        WRITE(fdbg,"('Le PE ',I3,' a pour coordonnees dans la ',
     &               'partition :',3(1X,I3))")
     &        numproc,numprocx,numprocy,numprocz
      ENDIF

! Dimensionnalite du cas
! ----------------------------------------------------------------------
      SELECT CASE(dim_cas)
      CASE(6,5,3)
        CONTINUE
      CASE(4)
        d_ez  = 0 ; d_bx  = 0 ; d_by  = 0
        d_ezg = 0 ; d_bxg = 0 ; d_byg = 0
      CASE(2)
        d_ez  = 0 ; d_bx  = 0 ; d_by  = 0
        d_ezg = 0 ; d_bxg = 0 ; d_byg = 0
      CASE(1)
        d_ey  = 0 ; d_ez  = 0 ; d_bx  = 0 ; d_by  = 0 ; d_bz  = 0
        d_eyg = 0 ; d_ezg = 0 ; d_bxg = 0 ; d_byg = 0 ; d_bzg = 0
      END SELECT

! CFL et pas spatiaux
! ----------------------------------------------------------------------
      SELECT CASE(dim_cas)
      CASE(6)
        IF((pasdx.NE.pasdy).OR.(pasdx.NE.pasdz).OR.(pasdy.NE.pasdz)) 
     &  THEN
          WRITE(fmsgo,"('Note : les trois pas spatiaux '
     &                  'ne sont pas egaux')")
        ENDIF
        IF((pasdx**2+pasdy**2+pasdz**2)/pasdt**2.LT.1.) THEN
          WRITE(fmsgo,"('Attention : CFL n''est pas verifie !')")
        ENDIF
      CASE(5,4)
        IF((pasdx.NE.pasdy)) THEN
          WRITE(fmsgo,"('Note : les deux pas spatiaux '
     &                  'ne sont pas egaux')")
        ENDIF
	write(fmsgo,*) 'pasdx,pasdy,pasdt=',pasdx,pasdy,pasdt
        IF((pasdx**2+pasdy**2)/pasdt**2.LT.1.) THEN
          WRITE(fmsgo,"('Attention : CFL n''est pas verifie !')")
        ENDIF
      CASE(3,2)
        IF(pasdx/pasdt.LT.1.) THEN
          WRITE(fmsgo,"('Attention : CFL n''est pas verifie !')")
        ENDIF
      CASE(1)
        CONTINUE
      END SELECT

! Initialisation des variables donnant le nombre de mailles de d�passement en fonction 
! de l'ordre : l_oset, r_oset, c_oset
!
! Nombre de mailles de d�passement, on utilise c_oset pour les grandeurs centr�es
! sur les mailles primales telles que la densit� de charge
! et l_oset, r_oset pour les grandeurs calcul�es sur des mailles duales
!---------------------------------------------------------------------

!    left_offset
      l_oset  = INT( (ordre_interp-1)/2 ) 
!    right_offset
      r_oset  = INT( (ordre_interp+1)/2 ) 
      
!    center_offset      
      c_oset = INT(ordre_interp/2)


! Petits calculs betes
! ----------------------------------------------------------------------
      pasdxi  = 1._8/pasdx
      pasdyi  = 1._8/pasdy
      pasdzi  = 1._8/pasdz
      pasdts2 = 0.5_8*pasdt

! Injection de sources laser aux bords gauche ou droit
! Conversion angle d'injection : deg -> rad
! Coefficients de polarisation (rapportes au champ B)
! ----------------------------------------------------------------------
      inj_laser(:) = 0
      y_ang(:) = y_ang(:) / 45._8 * ATAN(1._8)
      z_ang(:) = z_ang(:) / 45._8 * ATAN(1._8)
      
      IF((numprocx.EQ.1)) THEN
        WHERE((t_prof(:).NE.'nolas').AND.(lambda(:).NE.0.)
     &                              .AND.(COS(y_ang(:)).GT.0.))
     &  inj_laser(:) = -1
      ENDIF
      IF((numprocx.EQ.nprocx)) THEN
        WHERE((t_prof(:).NE.'nolas').AND.(lambda(:).NE.0.)
     &                              .AND.(COS(y_ang(:)).LT.0.))
     &  inj_laser(:) = +1

      ENDIF
      IF(ANY(inj_laser(:).EQ.-1)) THEN
        ibndc(1) = 0
        IF(ibndc(2).EQ.-1) THEN
          WRITE(fmsgo,"('Subroutine calc_donn : '
     &                  'le bord droit ne peut')")
          WRITE(fmsgo,"('pas etre une condition periodique pour')")
          WRITE(fmsgo,"('les champs si on fait entrer une source')")
          WRITE(fmsgo,"('laser a gauche. On force ibndc(2)=-2')")
          ibndc(2) = -2
        ENDIF
      ENDIF
      IF(ANY(inj_laser(:).EQ.+1)) THEN
        ibndc(2) = 0
        IF(ibndc(1).EQ.-1) THEN
          WRITE(fmsgo,"('Subroutine calc_donn : '
     &                  'le bord gauche ne peut')")
          WRITE(fmsgo,"('pas etre une condition periodique pour')")
          WRITE(fmsgo,"('les champs si on fait entrer une source')")
          WRITE(fmsgo,"('laser a droite. On force ibndc(1)=-2')")
          ibndc(1) = -2
        ENDIF
      ENDIF

      usak(:) = SQRT(1._8 + (SIN(y_ang(:))*SIN(z_ang(:)))**2)
      z_pol(:) = polar(:)
      y_pol(:) = SQRT(1._8 - z_pol(:)**2)
! Dimensions du faisceau donnees sur section droite,
! a convertir en dimension au bord d'injection
! ----------------------------------------------------------------------
      y_lrg(:) = y_lrg(:)/COS(y_ang(:))
      z_lrg(:) = z_lrg(:)/COS(z_ang(:))
      a_0  (:) = a_0  (:)*COS(y_ang(:))*COS(z_ang(:))

! Cas particulier d une source laser definie en combinant deux modes  
! de hermite
! mode 0 : H_0 = 1.
! mode 2 : H_2(x) = 4* x**2 - 2
! The physicists' Hermite polynomials
! http://en.wikipedia.org/wiki/Hermite_polynomials
! ----------------------------------------------------------------------
      y_dpeak(:) = y_dpeak(:)/y_lrg(:)
      z_dpeak(:) = z_dpeak(:)/z_lrg(:) 

      a2_her_y(:) = a_0(:)*log(2.)*exp(0.5*log(2.)*(y_dpeak(:))**2)
      a2_her_z(:) = a_0(:)*log(2.)*exp(0.5*log(2.)*(z_dpeak(:))**2)
      
      a1_her_y(:) = 2*a2_her_y(:)*(1./(2.*log(2.))+1.
     &                             -0.25*(y_dpeak(:))**2)
      a1_her_z(:) = 2*a2_her_z(:)*(1./(2.*log(2.))+1.
     &                             -0.25*(z_dpeak(:))**2)

      write(fmsgo,*) 'a2_her_y(1) =',a2_her_y(1) 
      write(fmsgo,*) 'a1_her_y(1) =',a1_her_y(1) 
      

! Distribution en impulsions (si dynam. relativiste)
! ou vitesses (si dynam. classique) des particules 
!------------------------------------------------------------------------                     
      IF(dim_cas.EQ.1) THEN
        typ_disty(:) = 0;vthy(:) = 0.;vdery(:) = 0.;
	typ_distz(:) = 0;vthz(:) = 0.;vderz(:) = 0.; 
      ENDIF
      IF((dim_cas.EQ.2).OR.(dim_cas.EQ.4)) THEN
        typ_distz(:) = 0;vthz(:) = 0.;vderz(:) = 0.
      ENDIF

! La Maxwell-Juttner 2d n'est prise en compte que si typ_dist[xyz] = (2,2,*)
! La Maxwell-Juttner 3d n'est prise en compte que si typ_dist[xyz] = (3,3,*)
! Autrement, typ_dist = (2,3) -> typ_dist = 1
! Le waterbag oblique xy n'est pris en compte que si typ_dist[xyz] = (5,5,*)
! Atutrement typ_dist = (5,3) -> typ_distx = 4
      DO iesp=1,nesp_p        
        SELECT CASE(typ_distx(iesp))
	CASE(2)
	  IF (typ_disty(iesp).NE.2) typ_distx(iesp) = 1 ;
	CASE(3)
	  IF((typ_disty(iesp).NE.3).OR.(typ_distz(iesp).NE.3)) THEN
	    typ_distx(iesp) = 1
	  ENDIF
	CASE(5)
	  IF (typ_disty(iesp).NE.5) typ_distx(iesp) = 4 ;  
	END SELECT
        SELECT CASE(typ_disty(iesp))
	CASE(2)
	  IF (typ_distx(iesp).NE.2) typ_disty(iesp) = 1 ;
	CASE(3)
	  IF((typ_distx(iesp).NE.3).OR.(typ_distz(iesp).NE.3)) THEN
	    typ_disty(iesp) = 1
	  ENDIF
	CASE(5)
	  IF (typ_distx(iesp).NE.5) typ_disty(iesp) = 4 ;  
	END SELECT
	SELECT CASE(typ_distz(iesp))
	CASE(2)
	  typ_distz(iesp) = 1
	CASE(3)
	  IF((typ_distx(iesp).NE.3).OR.(typ_disty(iesp).NE.3)) THEN
	    typ_distz(iesp) = 1
	  ENDIF
	END SELECT
      ENDDO
	    
! La fd de Maxwell-Juttner impose une dynamique relativiste
      DO iesp=1,nesp_p
        SELECT CASE(typ_distx(iesp))
        CASE(1,2,3)
	  dynam(iesp) = 2
	END SELECT
	SELECT CASE(typ_disty(iesp))
        CASE(1,2,3)
	  dynam(iesp) = 2
	END SELECT
	SELECT CASE(typ_distz(iesp))
        CASE(1,2,3)
	  dynam(iesp) = 2
	END SELECT
      ENDDO

      ALLOCATE(inj_distx(2000,nesp_p),inj_disty(2000,nesp_p),
     & inj_distz(2000,nesp_p))

      DO iesp=1,nesp_p
        WRITE(fdbg,*) 'Distribution en impulsions de l''espece', iesp
        SELECT CASE(typ_distx(iesp))
	CASE(0)
! Maxwellienne : on convertit la temperature en vitesse/impulsion thermique           
          vthx(iesp) = SQRT(vthx(iesp)/(masse(iesp)*511._8))
          WRITE(fdbg,*) 'En x : Maxwellienne avec vthx, vderx = ',
     &         vthx(iesp),vderx(iesp) 
	CASE(1)
! Maxwell-Juttner 1d	
          WRITE(fdbg,*) 'En x : Maxwell-Juttner avec vthx, vderx = ',
     &        vthx(iesp),vderx(iesp)    
	  gder=1._8/SQRT(1-vderx(iesp)**2)
	  vthder = gder*vthx(iesp)/(masse(iesp)*511._8)
! Fonction de repartition du facteur isotrope exp(-gamma/gder*vth) dans
! le repere mobile. Discretisation sur 2000 points jusqu'a Ecin = 10.kT
          oxmax = SQRT(20._8*vthder*(1._8+5._8*vthder))
          ox=oxmax* 1._8/2000._8
	  IF (vthder .LE. 0.05) THEN
	    inj_distx(1,iesp) = EXP(-0.5*ox*ox/vthder)
	  ELSE
            inj_distx(1,iesp) = EXP(-SQRT(1.+ox*ox)/vthder)
          ENDIF
          DO ix=2,2000
            ox = oxmax*REAL(ix)/2000._8
	    IF (vthder .LE. 0.05) THEN
	      inj_distx(ix,iesp) = inj_distx(ix-1,iesp)
     &                         + EXP(-0.5*ox*ox/vthder)
            ELSE
              inj_distx(ix,iesp) = inj_distx(ix-1,iesp)
     &                         + EXP(-SQRT(1.+ox*ox)/vthder)
            ENDIF
          ENDDO
          inj_distx(:,iesp) = inj_distx(:,iesp)/inj_distx(2000,iesp)
	CASE(2)
! Maxwell-Juttner 2d	
          WRITE(fdbg,*) 'En xy : Maxwell-Juttner avec vth, vder[xy] =',
     &         vthx(iesp),vderx(iesp),vdery(iesp)
	  gder=1._8/SQRT(1-vderx(iesp)**2-vdery(iesp)**2)
	  vthder = gder*vthx(iesp)/(masse(iesp)*511._8)
! Fonction de repartition du facteur isotrope exp(-gamma/gder*vth) dans
! le repere mobile. Discretisation sur 2000 points jusqu'a Ecin = 10.kT
          oxmax = SQRT(20._8*vthder*(1._8+5._8*vthder))
          ox=oxmax* 1._8/2000._8
	  IF (vthder .LE. 0.05) THEN
	    inj_distx(1,iesp) = EXP(-0.5*ox*ox/vthder)*ox
	  ELSE
            inj_distx(1,iesp) = EXP(-SQRT(1.+ox*ox)/vthder)*ox
          ENDIF
          DO ix=2,2000
            ox = oxmax*REAL(ix)/2000._8
	    IF (vthder .LE. 0.05) THEN
	      inj_distx(ix,iesp) = inj_distx(ix-1,iesp)
     &                         + EXP(-0.5*ox*ox/vthder)*ox
            ELSE
              inj_distx(ix,iesp) = inj_distx(ix-1,iesp)
     &                         + EXP(-SQRT(1.+ox*ox)/vthder)*ox
            ENDIF
          ENDDO
          inj_distx(:,iesp) = inj_distx(:,iesp)/inj_distx(2000,iesp)
	CASE(3)
! Maxwell-Juttner 3d
          WRITE(fdbg,*) 'En xyz : Maxwell-Juttner avec vth, vder[xyz]=',
     &         vthx(iesp),vderx(iesp),vdery(iesp),vderz(iesp) 	
	  gder=1._8/SQRT(1-vderx(iesp)**2-vdery(iesp)**2-vderz(iesp)**2)
	  vthder = gder*vthx(iesp)/(masse(iesp)*511._8)
! Fonction de repartition du facteur isotrope exp(-gamma/gder*vth) dans
! le repere mobile. Discretisation sur 2000 points jusqu'a Ecin = 10.kT
          oxmax = SQRT(20._8*vthder*(1._8+5._8*vthder))
          ox=oxmax* 1._8/2000._8
	  IF (vthder .LE. 0.05) THEN
	    inj_distx(1,iesp) = EXP(-0.5*ox*ox/vthder)*ox**2
	  ELSE
            inj_distx(1,iesp) = EXP(-SQRT(1.+ox*ox)/vthder)*ox**2
          ENDIF
          DO ix=2,2000
            ox = oxmax*REAL(ix)/2000._8
	    IF (vthder .LE. 0.05) THEN
	      inj_distx(ix,iesp) = inj_distx(ix-1,iesp)
     &                         + EXP(-0.5*ox*ox/vthder)*ox**2
            ELSE
              inj_distx(ix,iesp) = inj_distx(ix-1,iesp)
     &                         + EXP(-SQRT(1.+ox*ox)/vthder)*ox**2
            ENDIF
          ENDDO
          inj_distx(:,iesp) = inj_distx(:,iesp)/inj_distx(2000,iesp)
	CASE(4)
! Waterbag anisotrope
          WRITE(fdbg,*) 'En x : Waterbag avec vthx, vderx =',
     &        vthx(iesp),vderx(iesp) 
	CASE(5)
! Waterbag anisotrope oblique xy
          WRITE(fdbg,*) 'En xy : Waterbag oblique vth[xy], vder[xy] =',
     &        vthx(iesp),vthy(iesp),vderx(iesp),vdery(iesp)    	    	 
	END SELECT
	
	SELECT CASE(typ_disty(iesp))
	CASE(0)
! Maxwellienne : on convertit la temperature en vitesse/impulsion thermique             
          vthy(iesp) = SQRT(vthy(iesp)/(masse(iesp)*511._8))		
          WRITE(fdbg,*) 'En y : Maxwellienne avec vthy, vdery = ',
     &      vthy(iesp),vdery(iesp)   
	CASE(1)
! Maxwell-Juttner 1d		
          WRITE(fdbg,*) 'En y : Maxwell-Juttner avec vthy, vdery = ',
     &      vthy(iesp),vdery(iesp)          
	  gder=1._8/SQRT(1-vdery(iesp)**2)
	  vthder = gder*vthy(iesp)/(masse(iesp)*511._8)
! Fonction de repartition du facteur isotrope exp(-gamma/gder*vth) dans
! le repere mobile. Discretisation sur 2000 points jusqu'a Ecin=10.kT
          oxmax = SQRT(20._8*vthder*(1._8+5._8*vthder))
          ox=oxmax* 1._8/2000._8
          inj_disty(1,iesp)=EXP(-SQRT(1.+ox*ox)/vthder)
          DO ix=2,2000
            ox = oxmax*REAL(ix)/2000._8
            inj_disty(ix,iesp)= inj_disty(ix-1,iesp)
     &                        +EXP(-SQRT(1.+ox*ox)/vthder)
          ENDDO
          inj_disty(:,iesp) = inj_disty(:,iesp)/inj_disty(2000,iesp)	
	CASE(4)
! Waterbag anisotrope
          WRITE(fdbg,*) 'En y : Waterbag avec vthx, vderx = ',
     &        vthy(iesp),vdery(iesp)
	END SELECT
	
	SELECT CASE(typ_distz(iesp))
	CASE(0)
! Maxwellienne : on convertit la temperature en vitesse/impulsion thermique             
          vthz(iesp) = SQRT(vthz(iesp)/(masse(iesp)*511._8))	
          WRITE(fdbg,*) 'En z : Maxwellienne avec vthz, vderz = ',
     &        vthz(iesp),vderz(iesp)	   
	CASE(1)
! Maxwell-Juttner 1d			
          WRITE(fdbg,*) 'En z : Maxwell-Juttner avec vthz, vderz = ',
     &        vthz(iesp),vderz(iesp) 
	  gder=1._8/SQRT(1-vderz(iesp)**2)
	  vthder = gder*vthz(iesp)/(masse(iesp)*511._8)
! Fonction de repartition du facteur isotrope exp(-gamma/gder*vth) dans
! le repere mobile. Discretisation sur 2000 points jusqu'a Ecin=10.kT
          oxmax = SQRT(20._8*vthder*(1._8+5._8*vthder))
          ox=oxmax* 1._8/2000._8
          inj_distz(1,iesp)=EXP(-SQRT(1.+ox*ox)/vthder)
          DO ix=2,2000
            ox = oxmax*REAL(ix)/2000._8
            inj_distz(ix,iesp)= inj_distz(ix-1,iesp)
     &                        +EXP(-SQRT(1.+ox*ox)/vthder)
          ENDDO
          inj_distz(:,iesp) = inj_distz(:,iesp)/inj_distz(2000,iesp)
	CASE(4)
! Waterbag anisotrope
          WRITE(fdbg,*) 'En z : Waterbag avec vthz, vderz = ',
     &        vthz(iesp),vderz(iesp) 	
	END SELECT		

      ENDDO


! Verification des diagnostics "spatiaux" demandes
! ----------------------------------------------------------------------
! --- Compatibilite avec la dimension du cas
      DO dsp_i=1,dsp_nb
        SELECT CASE(dsp(dsp_i)%typ)
        CASE(1,7,10,11)            ! --- ex, cx, rh, ta
          CONTINUE
        CASE(2,6,8)             ! --- ey, bz, cy
          IF(dim_cas.EQ.1) dsp(dsp_i)%typ = 0
        CASE(3,5,9)             ! --- ez, by, cz
          IF(dim_cas.EQ.1) dsp(dsp_i)%typ = 0
          IF(dim_cas.EQ.2) dsp(dsp_i)%typ = 0
          IF(dim_cas.EQ.4) dsp(dsp_i)%typ = 0
        CASE(4)                 ! --- bx
          IF(dim_cas.EQ.1) dsp(dsp_i)%typ = 0
          IF(dim_cas.EQ.2) dsp(dsp_i)%typ = 0
          IF(dim_cas.EQ.3) dsp(dsp_i)%typ = 0
          IF(dim_cas.EQ.4) dsp(dsp_i)%typ = 0
        CASE DEFAULT
         dsp(dsp_i)%typ = 0
        END SELECT
      ENDDO
! ---
      DO dsp_i=1,dsp_nb
        IF(dsp(dsp_i)%typ.EQ.0) CYCLE
        IF(dsp(dsp_i)%min(1).LT.xmin_gl) dsp(dsp_i)%min(1) = xmin_gl
        IF(dsp(dsp_i)%min(2).LT.ymin_gl) dsp(dsp_i)%min(2) = ymin_gl
        IF(dsp(dsp_i)%min(3).LT.zmin_gl) dsp(dsp_i)%min(3) = zmin_gl
        IF(dsp(dsp_i)%max(1).GT.xmax_gl) dsp(dsp_i)%max(1) = xmax_gl
        IF(dsp(dsp_i)%max(2).GT.ymax_gl) dsp(dsp_i)%max(2) = ymax_gl
        IF(dsp(dsp_i)%max(3).GT.zmax_gl) dsp(dsp_i)%max(3) = zmax_gl
! --- conversion positions -> numeros de noeuds
        SELECT CASE(dsp(dsp_i)%typ)
        CASE(1,7)               ! --- du pr pr : ex, cx
         dsp(dsp_i)%imn(1) = INT((dsp(dsp_i)%min(1)-xmin_gl)*pasdxi+0.5)
         dsp(dsp_i)%imx(1) = INT((dsp(dsp_i)%max(1)-xmin_gl)*pasdxi+1.5)
         dsp(dsp_i)%imn(2) = INT((dsp(dsp_i)%min(2)-ymin_gl)*pasdyi    )
         dsp(dsp_i)%imx(2) = INT((dsp(dsp_i)%max(2)-ymin_gl)*pasdyi+1. )
         dsp(dsp_i)%imn(3) = INT((dsp(dsp_i)%min(3)-zmin_gl)*pasdzi    )
         dsp(dsp_i)%imx(3) = INT((dsp(dsp_i)%max(3)-zmin_gl)*pasdzi+1. )
        CASE(2,8)               ! --- pr du pr : ey, cy
         dsp(dsp_i)%imn(1) = INT((dsp(dsp_i)%min(1)-xmin_gl)*pasdxi    )
         dsp(dsp_i)%imx(1) = INT((dsp(dsp_i)%max(1)-xmin_gl)*pasdxi+1. )
         dsp(dsp_i)%imn(2) = INT((dsp(dsp_i)%min(2)-ymin_gl)*pasdyi+0.5)
         dsp(dsp_i)%imx(2) = INT((dsp(dsp_i)%max(2)-ymin_gl)*pasdyi+1.5)
         dsp(dsp_i)%imn(3) = INT((dsp(dsp_i)%min(3)-zmin_gl)*pasdzi    )
         dsp(dsp_i)%imx(3) = INT((dsp(dsp_i)%max(3)-zmin_gl)*pasdzi+1. )
        CASE(3,9)               ! --- pr pr du : ez, cz
         dsp(dsp_i)%imn(1) = INT((dsp(dsp_i)%min(1)-xmin_gl)*pasdxi    )
         dsp(dsp_i)%imx(1) = INT((dsp(dsp_i)%max(1)-xmin_gl)*pasdxi+1. )
         dsp(dsp_i)%imn(2) = INT((dsp(dsp_i)%min(2)-ymin_gl)*pasdyi    )
         dsp(dsp_i)%imx(2) = INT((dsp(dsp_i)%max(2)-ymin_gl)*pasdyi+1. )
         dsp(dsp_i)%imn(3) = INT((dsp(dsp_i)%min(3)-zmin_gl)*pasdzi+0.5)
         dsp(dsp_i)%imx(3) = INT((dsp(dsp_i)%max(3)-zmin_gl)*pasdzi+1.5)
        CASE(4)                 ! --- pr du du : bx
         dsp(dsp_i)%imn(1) = INT((dsp(dsp_i)%min(1)-xmin_gl)*pasdxi    )
         dsp(dsp_i)%imx(1) = INT((dsp(dsp_i)%max(1)-xmin_gl)*pasdxi+1. )
         dsp(dsp_i)%imn(2) = INT((dsp(dsp_i)%min(2)-ymin_gl)*pasdyi+0.5)
         dsp(dsp_i)%imx(2) = INT((dsp(dsp_i)%max(2)-ymin_gl)*pasdyi+1.5)
         dsp(dsp_i)%imn(3) = INT((dsp(dsp_i)%min(3)-zmin_gl)*pasdzi+0.5)
         dsp(dsp_i)%imx(3) = INT((dsp(dsp_i)%max(3)-zmin_gl)*pasdzi+1.5)
        CASE(5)                 ! --- du pr du : by
         dsp(dsp_i)%imn(1) = INT((dsp(dsp_i)%min(1)-xmin_gl)*pasdxi+0.5)
         dsp(dsp_i)%imx(1) = INT((dsp(dsp_i)%max(1)-xmin_gl)*pasdxi+1.5)
         dsp(dsp_i)%imn(2) = INT((dsp(dsp_i)%min(2)-ymin_gl)*pasdyi    )
         dsp(dsp_i)%imx(2) = INT((dsp(dsp_i)%max(2)-ymin_gl)*pasdyi+1. )
         dsp(dsp_i)%imn(3) = INT((dsp(dsp_i)%min(3)-zmin_gl)*pasdzi+0.5)
         dsp(dsp_i)%imx(3) = INT((dsp(dsp_i)%max(3)-zmin_gl)*pasdzi+1.5)
        CASE(6)                 ! --- du du pr : bz
         dsp(dsp_i)%imn(1) = INT((dsp(dsp_i)%min(1)-xmin_gl)*pasdxi+0.5)
         dsp(dsp_i)%imx(1) = INT((dsp(dsp_i)%max(1)-xmin_gl)*pasdxi+1.5)
         dsp(dsp_i)%imn(2) = INT((dsp(dsp_i)%min(2)-ymin_gl)*pasdyi+0.5)
         dsp(dsp_i)%imx(2) = INT((dsp(dsp_i)%max(2)-ymin_gl)*pasdyi+1.5)
         dsp(dsp_i)%imn(3) = INT((dsp(dsp_i)%min(3)-zmin_gl)*pasdzi    )
         dsp(dsp_i)%imx(3) = INT((dsp(dsp_i)%max(3)-zmin_gl)*pasdzi+1. )
        CASE(10)                ! --- pr pr pr : rh
         dsp(dsp_i)%imn(1) = INT((dsp(dsp_i)%min(1)-xmin_gl)*pasdxi    )
         dsp(dsp_i)%imx(1) = INT((dsp(dsp_i)%max(1)-xmin_gl)*pasdxi+1. )
         dsp(dsp_i)%imn(2) = INT((dsp(dsp_i)%min(2)-ymin_gl)*pasdyi    )
         dsp(dsp_i)%imx(2) = INT((dsp(dsp_i)%max(2)-ymin_gl)*pasdyi+1. )
         dsp(dsp_i)%imn(3) = INT((dsp(dsp_i)%min(3)-zmin_gl)*pasdzi    )
         dsp(dsp_i)%imx(3) = INT((dsp(dsp_i)%max(3)-zmin_gl)*pasdzi+1. )
        CASE(11)                ! --- pr pr pr : ta
         dsp(dsp_i)%imn(1) = INT((dsp(dsp_i)%min(1)-xmin_gl)*pasdxi    )
         dsp(dsp_i)%imx(1) = INT((dsp(dsp_i)%max(1)-xmin_gl)*pasdxi+1. )
         dsp(dsp_i)%imn(2) = INT((dsp(dsp_i)%min(2)-ymin_gl)*pasdyi    )
         dsp(dsp_i)%imx(2) = INT((dsp(dsp_i)%max(2)-ymin_gl)*pasdyi+1. )
         dsp(dsp_i)%imn(3) = INT((dsp(dsp_i)%min(3)-zmin_gl)*pasdzi    )
         dsp(dsp_i)%imx(3) = INT((dsp(dsp_i)%max(3)-zmin_gl)*pasdzi+1. )
        CASE DEFAULT
         dsp(dsp_i)%typ = 0
        END SELECT

! --- coherence des grilles de calcul et de visualisation
        WHERE(dsp(dsp_i)%siz(:).GT.1)
          dsp_pas(:) = INT( (dsp(dsp_i)%imx(:)-dsp(dsp_i)%imn(:))
     &                     /(dsp(dsp_i)%siz(:)-1.) + 0.5         )
          dsp(dsp_i)%siz(:) =  (dsp(dsp_i)%imx(:)-dsp(dsp_i)%imn(:))
     &                        / dsp_pas(:) + 1.99
          dsp(dsp_i)%imx(:) =   dsp(dsp_i)%imn(:) 
     &                        +(dsp(dsp_i)%siz(:)-1)*dsp_pas(:)
        ENDWHERE

! --- limites 
! --- (un peu rapide pour l'instant : on limite tout le monde au primal)
        WHERE(dsp(dsp_i)%imn(:).LT.0) dsp(dsp_i)%imn(:) = 0
        IF(dsp(dsp_i)%imx(1).GT.(nprocx*nbmx)) THEN
          dsp(dsp_i)%imx(1) = dsp(dsp_i)%imx(1) - dsp_pas(1)
          dsp(dsp_i)%siz(1) = dsp(dsp_i)%siz(1) - 1
        ENDIF
        IF(dsp(dsp_i)%imx(2).GT.(nprocy*nbmy)) THEN
          dsp(dsp_i)%imx(2) = dsp(dsp_i)%imx(2) - dsp_pas(2)
          dsp(dsp_i)%siz(2) = dsp(dsp_i)%siz(2) - 1
        ENDIF
        IF(dsp(dsp_i)%imx(3).GT.(nprocz*nbmz)) THEN
          dsp(dsp_i)%imx(3) = dsp(dsp_i)%imx(3) - dsp_pas(3)
          dsp(dsp_i)%siz(3) = dsp(dsp_i)%siz(3) - 1
        ENDIF

        WHERE(dsp(dsp_i)%siz(:).LE.1)
          dsp(dsp_i)%siz(:) = 1
          dsp(dsp_i)%imx(:) = dsp(dsp_i)%imn(:)
        ENDWHERE

! --- affichage
      IF(debug.GE.1) THEN
        WRITE(fdbg,"('Le diag. spatial ',I3,' a pour min et max',
     &               6(1X,I5),' et prend',3(1X,I5),' mailles')")
     &        dsp_i, dsp(dsp_i)%imn(:), dsp(dsp_i)%imx(:),
     &               dsp(dsp_i)%siz(:)
      ENDIF
      ENDDO

! Verification des diagnostics "espaces des phases" demandes
!  - on met a '00' les axes non coherents avec la dimensionnalite du cas
!  - on place en troisieme axe un eventuel axe interdit '00'
!  - on rejette les diags dont deux axes sont identiques
!    (sauf ceux ayant deux axes interdits '00')
! ----------------------------------------------------------------------
      DO dph_i=1,dph_nb
        IF(.NOT.dph(dph_i)%ok) CYCLE
        DO iax=1,3
          SELECT CASE(dph(dph_i)%axe(iax))
          CASE('qx','ix','rx','gx')
            CONTINUE
          CASE('qy','iy','jy','ry','sy','gy')
            IF(dim_cas.LT.4) dph(dph_i)%axe(iax) = '00'
          CASE('qz','iz','jz','kz','rz','sz','tz','gz')
            IF(dim_cas.LT.6) dph(dph_i)%axe(iax) = '00'
          CASE('px')
            CONTINUE
          CASE('py','jx','sx')
            IF(dim_cas.LT.2) dph(dph_i)%axe(iax) = '00'
          CASE('pz','kx','tx')
            IF((dim_cas.LT.5).AND.(dim_cas.NE.3))
     &                       dph(dph_i)%axe(iax) = '00'
          CASE('ky','ty')
            IF(dim_cas.LT.5) dph(dph_i)%axe(iax) = '00'
          CASE('wx')
            CONTINUE
          CASE('wy')
            IF(dim_cas.LT.4) dph(dph_i)%axe(iax) = '00'
          CASE('wz')
            IF((dim_cas.LT.5).AND.(dim_cas.NE.3))
     &                       dph(dph_i)%axe(iax) = '00'
          CASE('ex')
            CONTINUE
          CASE('ey')
            IF(dim_cas.LT.4) dph(dph_i)%axe(iax) = '00'
          CASE('ez')
            IF((dim_cas.LT.5).AND.(dim_cas.NE.3))
     &                       dph(dph_i)%axe(iax) = '00'
          CASE('fx')
            dph(dph_i)%axe(1) = 'fx'
            dph(dph_i)%axe(2) = '00'
            dph(dph_i)%axe(3) = '00'
          CASE DEFAULT
            dph(dph_i)%axe(iax) = '00'
          END SELECT
        ENDDO
        DO iax=1,2
          if(dph(dph_i)%axe(iax).EQ.'00') then
            dph(dph_i)%axe(iax) = dph(dph_i)%axe(  3)
            dph(dph_i)%min(iax) = dph(dph_i)%min(  3)
            dph(dph_i)%max(iax) = dph(dph_i)%max(  3)
            dph(dph_i)%siz(iax) = dph(dph_i)%siz(  3)
            dph(dph_i)%axe(  3) = '00'
          endif
        ENDDO
        IF(    (     (dph(dph_i)%axe(1).EQ.dph(dph_i)%axe(2))
     &          .AND.(dph(dph_i)%axe(2).NE.'00')              )
     &     .OR.(     (dph(dph_i)%axe(2).EQ.dph(dph_i)%axe(3))
     &          .AND.(dph(dph_i)%axe(3).NE.'00')              )
     &     .OR.(     (dph(dph_i)%axe(1).EQ.dph(dph_i)%axe(3))
     &          .AND.(dph(dph_i)%axe(3).NE.'00')              )
     &     .OR.(     (dph(dph_i)%axe(1).EQ.dph(dph_i)%axe(2))
     &          .AND.(dph(dph_i)%axe(2).EQ.dph(dph_i)%axe(3)) )   )
     &     dph(dph_i)%ok = .false.
      ENDDO

! Diagnostics moyens demandes
! ----------------------------------------------------------------------
! --- affichage
      IF(debug.GE.1) THEN
        WRITE(fdbg,"('Les moyennes sont faites sur ',I4,' pas de ',
     &               ' temps pour les diags spatiaux')") dsp_moy
        WRITE(fdbg,"('et sur ',I4,' pas de temps pour les diags',
     &               '  du types espace des phases 1D')") dph_moy
      ENDIF

      RETURN
!     ________________________
      END SUBROUTINE calc_donn
