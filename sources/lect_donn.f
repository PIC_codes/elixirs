! ======================================================================
! lect_donn.f
! -----------
!
!     SUBROUTINE init_donn
!     SUBROUTINE lect_donn
!  
!  Modif diag_phase juillet 2005
!  Modif typ_dist[xyz],vder[xyz] octobre 2005 
!
! ======================================================================

      SUBROUTINE init_donn 
! ======================================================================
! Reprise ou creation du plasma
! ======================================================================
      USE temps
      USE domaines
      USE erreurs

      IMPLICIT NONE

      INTEGER                                  :: idummy, i
      INTEGER, DIMENSION(:), ALLOCATABLE       :: graine


! --- Decorrelation des generateurs aleatoires
!     (eventuellement reinitialises si reprise d'un cas)
! ----------------------------------------------------------------------
      CALL RANDOM_SEED(SIZE=idummy)
      ALLOCATE (graine(idummy))
      graine(:) = (/ ( numproc**i, i=1, idummy) /)
      CALL RANDOM_SEED(PUT=graine(:))
      WRITE(fdbg,*) 'Initialisation du generateur avec seed= ',graine(:)
      DEALLOCATE(graine)

! Lecture des donnees d'un cas
! ----------------------------------------------------------------------
      CALL lect_donn

      CALL part_dom


      RETURN
!     ________________________
      END SUBROUTINE init_donn



      SUBROUTINE lect_donn
! ======================================================================
! Lecture du fichier de donnees sur le PE 1
! Controles de coherence sommaires (sur pasdx,dy,dz)
! Broadcast des parametres
! ======================================================================

      USE domaines
      USE domaines_klder      
      USE temps
      USE particules_klder      
      USE champs      
      USE champs_iter   
      USE laser
      USE collisions
      USE diagnostics_klder
      USE diag_ptest
      USE diag_moyen      
      USE erreurs

      IMPLICIT NONE
!!      INCLUDE 'mpif.h'

      CHARACTER(LEN=8)         :: h_bloc
      CHARACTER(LEN=2)         :: dsp_map
!      CHARACTER, DIMENSION(:), ALLOCATABLE  :: msg_buff
!      INTEGER                               :: msg_taille

      INTEGER                  :: version = 1
      INTEGER                  :: iprof, iesp_p, iesp_f, dpt_ibloc,
     &                            dsp_i, dph_i, i, j, itta
     
      REAL(KIND=kr)            :: aux1, aux2




! Lecture du fichier de donnees 
! ----------------------------------------------------------------------

        itesterr(1) = 0
	ordre_interp = 1                   ! --- valeur par defaut
	esir_ok = 0
	thetad = 0.
	ttapushs2 = 0.
	harmonicfield = 0
	friction_tta = 1
	iter_es = 0
	
        nb_vidx = 5 ; nb_vidy = 0 ; nb_vidz = 0
	
	x_damp1 = -1
	x_damp2 = -1	
	
! ---- couplage implicite complet par d�faut	
	boundaries = 1    

	nprof = 1                         ! --- valeurs par defaut
	iprof = 0

! ----- initialisation sans recours au profil thetad_2
        tta_def = 0

        nesp_f = 0                         ! --- valeurs par defaut
        nesp_p = 0 ; nbpamax = 0           ! --- valeurs par defaut
        iesp_p = 0
	iesp_f = 0
        nesp = 0
	max_cycles = 1 

        der_ok    = 0                      ! --- valeurs par defaut
        der_x(:)  = 0.
        der_vx(:) = 0. ; der_vy(:) = 0. ; der_vz(:) = 0.
        dsp_nb = 4 ; dph_nb = 4            ! --- valeurs par defaut
        dsp_i = 0  ; dph_i = 0
        ALLOCATE( dsp(dsp_nb) ) ; dsp(:)%typ = 0
        ALLOCATE( dph(dph_nb) ) ; dph(:)%ok = .FALSE.

        clo_nb = 0                         ! --- valeurs par defaut
        cmx_nb = 0                         ! --- valeurs par defaut
        dpt_ok = 0                         ! --- valeurs par defaut
        dpt_nblocs = 0                     ! --- valeurs par defaut
        dpt_ibloc = 0        ! --- compteur de blocs de partic-test
	b0x = 0. ; b0y = 0. ; b0z = 0.     ! --- valeurs par defaut
	mag_ok = 0                        ! --- valeur par defaut
	
        y_dpeak(:) = 0. ; z_dpeak(:) = 0. 
	
        diag_typ = 'txt'

        OPEN(unit=20, file='data', status='old')
        READ(20,*) 
        READ(20,*) 
        READ(20,'(A8)') h_bloc

        DO WHILE((h_bloc.NE.'fin_fich').AND.(itesterr(1).EQ.0))
        READ(20,*)
        SELECT CASE(h_bloc)

        CASE('version_')
          READ(20,*,ERR=10) version

        CASE('dimensio')
          READ(20,*,ERR=10) dim_cas
	  dim_cas = 5
          IF((dim_cas.LT.1).OR.(dim_cas.GT.6)) THEN
            WRITE(fmsgo,"('Subroutine lect_donn : '
     &                    'une dimension du cas')") 
            WRITE(fmsgo,"('egale a ',I2,' n''est pas acceptee')")  
     &            dim_cas
            itesterr(1) = 1
          ENDIF

        CASE('ordre_in')
          READ(20,*,ERR=10) ordre_interp
          IF((ordre_interp.LT.1).OR.(ordre_interp.GT.4)) THEN
            WRITE(fmsgo,"('Subroutine lect_donn :', 
     &                'on doit avoir 1 <= ordre_interp <= 4')")  
            itesterr(1) = 1
          ENDIF

        CASE('theta_dp')
          READ(20,*,ERR=10) thetad, x_damp1, x_damp2	  
          IF((thetad.LT.0).OR.(thetad.GT.1)) THEN
            WRITE(fmsgo,"('Subroutine lect_donn :', 
     &                'on doit avoir 0 < theta < 1')")  
            itesterr(1) = 1
          ENDIF

        CASE('tta_push')
          READ(20,*,ERR=10) ttapushs2
	  ttapushs2 = 0.5*ttapushs2
          IF((ttapushs2.LT.0).OR.(ttapushs2.GT.0.5)) THEN
            WRITE(fmsgo,"('Subroutine lect_donn :', 
     &                'on doit avoir 0 < tta_push < 1')")  
            itesterr(1) = 1
          ENDIF

        CASE('friction')
          READ(20,*,ERR=10) friction_tta
          IF((friction_tta.NE.0).AND.(friction_tta.NE.1)) THEN
            WRITE(fmsgo,"('Subroutine lect_donn :', 
     &                'on doit avoir 0 < tta_push < 1')")  
            itesterr(1) = 1
          ENDIF

        CASE('E_well')
          READ(20,*,ERR=10) harmonicfield
          IF((harmonicfield.NE.0).AND.(harmonicfield.NE.1)) THEN
            WRITE(fmsgo,"('Subroutine lect_donn :', 
     &                'champ harmonique forc�, E_well vaut 0 ou 1')")  
            itesterr(1) = 1
          ENDIF

        CASE('esirkepo')
          READ(20,*,ERR=10) esir_ok
	  esir_ok = 0
          IF((esir_ok.LT.0).OR.(esir_ok.GT.1)) THEN
            WRITE(fmsgo,"('Subroutine lect_donn : ', 
     &                  'esirkepov vaut 0 ou 1 ')") 
            WRITE(fmsgo,"('egale a ',I2,' n''est pas acceptee')")  
     &         esir_ok
            itesterr(1) = 1
          ENDIF

        CASE('iter_es')
          READ(20,*,ERR=10) iter_es
          IF((iter_es.LT.0).OR.(iter_es.GT.1)) THEN
            WRITE(fmsgo,"('Subroutine lect_donn : ', 
     &                  'iter_es vaut 0 ou 1 ')") 
            WRITE(fmsgo,"('egale a ',I2,' n''est pas acceptee')")  
     &         iter_es
            itesterr(1) = 1
          ENDIF

        CASE('boundari')
          READ(20,*,ERR=10) boundaries
          IF((boundaries.NE.1).AND.(boundaries.NE.2) 
     &        .AND.(boundaries.NE.3)) THEN
            WRITE(fmsgo,"('Subroutine lect_donn :', 
     &                'on doit avoir boundaries �gale 1 ou 3',
     &      'pour le couplage implicite ou explicite des cond limites',
     &          'sur les champs E et B')")  
            itesterr(1) = 1
          ENDIF
	  
        CASE('cond_lim')
          READ(20,*,ERR=10) ibndp(1:6,1)
          READ(20,*) 
          READ(20,*,ERR=10) ibndc(1:6)

        CASE('maillage')
          READ(20,*,ERR=10) pasdx, pasdy, pasdz
          READ(20,*) 
          READ(20,*,ERR=10) nbmx_gl,  nbmy_gl,  nbmz_gl
          nbmx = nbmx_gl   ;  nbmy = nbmy_gl    ;  nbmz = nbmz_gl  
	  IF(nbmx_gl/2. .EQ. FLOOR(nbmx_gl/2.)) THEN
	  
            WRITE(fmsgo,"('Subroutine lect_donn :', 
     &      'il est fortement recommand� d utiliser un nombre de ',
     &      'mailles impaire en x, pour �viter les valeurs orphelines',
     &      ' sur le bord droit du domaine dues aux appels de tridia ', 
     &      'via la routine pot2di de correction du champ ', 
     &      'electrostatique')")  
            itesterr(1) = 1	  
	  ENDIF
	
	CASE('nb_profi')
	  READ(20,*,ERR=10) nprof
	  	   
          IF (ALLOCATED(centrex)) THEN
            DEALLOCATE(centrex, centrey, centrez,
     &                 x_profn, largx, lgrmx, lgrpx, nrmx, nrpx,
     &                 y_profn, largy, lgrmy, lgrpy, nrmy, nrpy,
     &                 z_profn, largz, lgrmz, lgrpz, nrmz, nrpz)
          ENDIF
          ALLOCATE(centrex(nprof), centrey(nprof), centrez(nprof),
     &             x_profn(nprof), largx(nprof),   lgrmx(nprof),
     &             lgrpx(nprof),   nrmx(nprof),    nrpx(nprof),
     &             y_profn(nprof), largy(nprof),   lgrmy(nprof),
     &             lgrpy(nprof),   nrmy(nprof),    nrpy(nprof),
     &             z_profn(nprof), largz(nprof),   lgrmz(nprof),
     &             lgrpz(nprof),   nrmz(nprof),    nrpz(nprof))

        CASE('taille_p')
          iprof = iprof + 1
          IF(iprof.GT.nprof) THEN  
            WRITE(fmsgo,"('Subroutine lect_donn : le nombre ou')")
            WRITE(fmsgo,"('l''emplacement des blocs taille_plasma')")
            WRITE(fmsgo,"('est incompatible avec '
     &           'le nombre de profils')")
            WRITE(fmsgo,"('lu par ailleurs dans le fichier', 
     &           'de parametres')")
            itesterr(1) = 1
          ELSE            
            READ(20,*,ERR=10) centrex(iprof),
     &                        centrey(iprof),
     &                        centrez(iprof)
            READ(20,*) 
            READ(20,*,ERR=10) x_profn(iprof), largx(iprof),
     &                        lgrmx(iprof),   lgrpx(iprof),
     &                        nrmx(iprof),    nrpx(iprof)
            READ(20,*,ERR=10) y_profn(iprof), largy(iprof),
     &                        lgrmy(iprof),   lgrpy(iprof),
     &                        nrmy(iprof),    nrpy(iprof)
            READ(20,*,ERR=10) z_profn(iprof), largz(iprof),
     &                        lgrmz(iprof),   lgrpz(iprof),
     &                        nrmz(iprof),    nrpz(iprof)
            WRITE(fdbg,"('Parametres du profil ',I2)") iprof 
	    WRITE(fdbg,*) ' centrex, centrey, centrez = ',
     &        centrex(iprof), centrey(iprof), centrez(iprof)
            WRITE(fdbg,*) ' x_profn, y_profn, z_profn = ',
     &        x_profn(iprof),' ',y_profn(iprof),' ',z_profn(iprof) 
	  ENDIF
	  
        CASE('theta_de')
	  itta = 1 ; tta_def=1
	  x_damp1 = -1. ;  x_damp2 = -1.
          ALLOCATE(tta_centrex(itta), tta_centrey(itta), 
     &      tta_centrez(itta),
     &             tta_x_profn(itta), tta_largx(itta), 
     &      tta_lgrmx(itta),
     &             tta_lgrpx(itta), tta_nrmx(itta), 
     &      tta_nrpx(itta),
     &             tta_y_profn(itta), tta_largy(itta), 
     &      tta_lgrmy(itta),
     &             tta_lgrpy(itta), tta_nrmy(itta), 
     &      tta_nrpy(itta),
     &             tta_z_profn(itta), tta_largz(itta), 
     &      tta_lgrmz(itta),
     &             tta_lgrpz(itta), tta_nrmz(itta), 
     &      tta_nrpz(itta))	  
	  
            READ(20,*,ERR=10) tta_centrex(itta),
     &                        tta_centrey(itta),
     &                        tta_centrez(itta)
            READ(20,*) 
            READ(20,*,ERR=10) tta_x_profn(itta), tta_largx(itta),
     &                        tta_lgrmx(itta) , tta_lgrpx(itta),
     &                        tta_nrmx(itta)  , tta_nrpx(itta)
            READ(20,*,ERR=10) tta_y_profn(itta), tta_largy(itta),
     &                        tta_lgrmy(itta) , tta_lgrpy(itta),
     &                        tta_nrmy(itta)  , tta_nrpy(itta)
            READ(20,*,ERR=10) tta_z_profn(itta), tta_largz(itta),
     &                        tta_lgrmz(itta) , tta_lgrpz(itta),
     &                        tta_nrmz(itta)  , tta_nrpz(itta)
            WRITE(fdbg,"('Profil de l amortissement ')")  
	    WRITE(fdbg,*) 'tta_centrex, tta_centrey, tta_centrez = ',
     &            tta_centrex(itta),' ',tta_centrey(itta),' ', 
     &            tta_centrez(itta)
            WRITE(fdbg,*) 'tta_x_profn, tta_y_profn, tta_z_profn = ',
     &        tta_x_profn(itta),' ', tta_y_profn(itta),' ',
     &        tta_z_profn(itta) 
     
! ------ d�sactivation theta port� par les particules     
            friction_tta=0
	  
	
        CASE('temps_si')
          READ(20,*) pasdt, itfin
! --- valeurs par defaut 
            dsp_moy = 6.28/pasdt        
	    
        CASE('src_lase')
            READ(20,*,ERR=10) lambda(1), a_0(1), y_ang(1), z_ang(1),
     &                        polar(1)
            READ(20,*) 
            READ(20,*,ERR=10) t_prof(1), t_ord(1), t_lrg(1), t_deb(1),
     &                        t_max(1)
            READ(20,*) 
            READ(20,*,ERR=10) y_prof(1), y_ord(1), y_lrg(1), y_deb(1),
     &                        y_max(1)
            READ(20,*) 
            READ(20,*,ERR=10) z_prof(1), z_ord(1), z_lrg(1), z_deb(1),
     &                        z_max(1)
            IF(y_prof(1).EQ.'foc3d') THEN
              z_prof(1) = 'foc3d'
              z_lrg(1) = y_lrg(1) ; z_max(1) = y_max(1)
            ENDIF
            IF(z_prof(1).EQ.'foc3d') THEN
              y_prof(1) = 'foc3d'
              y_lrg(1) = z_lrg(1) ; y_max(1) = z_max(1)
            ENDIF

        CASE('las_her1')	
            READ(20,*,ERR=10) lambda(1), a_0(1), y_ang(1), z_ang(1),
     &                        polar(1)
            READ(20,*) 
            READ(20,*,ERR=10) t_prof(1), t_ord(1), t_lrg(1), t_deb(1),
     &                        t_max(1) 
            READ(20,*) 
            READ(20,*,ERR=10) y_prof(1), y_ord(1), y_lrg(1), y_deb(1),
     &                        y_max(1), y_dpeak(1)
            READ(20,*) 
            READ(20,*,ERR=10) z_prof(1), z_ord(1), z_lrg(1), z_deb(1),
     &                        z_max(1), z_dpeak(1)
            IF(y_prof(1).EQ.'foc3d') THEN
              z_prof(1) = 'foc3d'
              z_lrg(1) = y_lrg(1) ; z_max(1) = y_max(1)
            ENDIF
            IF(z_prof(1).EQ.'foc3d') THEN
              y_prof(1) = 'foc3d'
              y_lrg(1) = z_lrg(1) ; y_max(1) = z_max(1)
            ENDIF

        CASE('src_las2')
            READ(20,*,ERR=10) lambda(2), a_0(2), y_ang(2), z_ang(2),
     &                        polar(2)
            READ(20,*) 
            READ(20,*,ERR=10) t_prof(2), t_ord(2), t_lrg(2), t_deb(2),
     &                        t_max(2)
            READ(20,*) 
            READ(20,*,ERR=10) y_prof(2), y_ord(2), y_lrg(2), y_deb(2),
     &                        y_max(2)
            READ(20,*) 
            READ(20,*,ERR=10) z_prof(2), z_ord(2), z_lrg(2), z_deb(2),
     &                        z_max(2)
            IF(y_prof(2).EQ.'foc3d') THEN
              z_prof(2) = 'foc3d'
              z_lrg(2) = y_lrg(2) ; z_max(2) = y_max(2)
            ENDIF
            IF(z_prof(2).EQ.'foc3d') THEN
              y_prof(2) = 'foc3d'
              y_lrg(2) = z_lrg(2) ; y_max(2) = z_max(2)
            ENDIF
	    
        CASE('las_her2')	
            READ(20,*,ERR=10) lambda(2), a_0(2), y_ang(2), z_ang(2),
     &                        polar(2)
            READ(20,*) 
            READ(20,*,ERR=10) t_prof(2), t_ord(2), t_lrg(2), t_deb(2),
     &                        t_max(2) 
            READ(20,*) 
            READ(20,*,ERR=10) y_prof(2), y_ord(2), y_lrg(2), y_deb(2),
     &                        y_max(2), y_dpeak(2)
            READ(20,*) 
            READ(20,*,ERR=10) z_prof(2), z_ord(2), z_lrg(2), z_deb(2),
     &                        z_max(2), z_dpeak(2)
            IF(y_prof(2).EQ.'foc3d') THEN
              z_prof(2) = 'foc3d'
              z_lrg(2) = y_lrg(2) ; z_max(2) = y_max(2)
            ENDIF
            IF(z_prof(2).EQ.'foc3d') THEN
              y_prof(2) = 'foc3d'
              y_lrg(2) = z_lrg(2) ; y_max(2) = z_max(2)
            ENDIF

        CASE('src_las3')
            READ(20,*,ERR=10) lambda(3), a_0(3), y_ang(3), z_ang(3),
     &                        polar(3)
            READ(20,*) 
            READ(20,*,ERR=10) t_prof(3), t_ord(3), t_lrg(3), t_deb(3),
     &                        t_max(3)
            READ(20,*) 
            READ(20,*,ERR=10) y_prof(3), y_ord(3), y_lrg(3), y_deb(3),
     &                        y_max(3)
            READ(20,*) 
            READ(20,*,ERR=10) z_prof(3), z_ord(3), z_lrg(3), z_deb(3),
     &                        z_max(3)
            IF(y_prof(3).EQ.'foc3d') THEN
              z_prof(3) = 'foc3d'
              z_lrg(3) = y_lrg(3) ; z_max(3) = y_max(3)
            ENDIF
            IF(z_prof(3).EQ.'foc3d') THEN
              y_prof(3) = 'foc3d'
              y_lrg(3) = z_lrg(3) ; y_max(3) = z_max(3)
            ENDIF

        CASE('chmp_mag')
	  mag_ok = 1
	  READ(20,*,ERR=10) b0x, b0y, b0z
          WRITE(fdbg,"('b0x, b0y, b0z =',3(1PE10.4,1X))") b0x,b0y,b0z

        CASE('especes_')
          READ(20,*,ERR=10) nesp_f, nesp_p, nbpamax
          IF(iesp_f+iesp_p.NE.0) THEN
            WRITE(fmsgo,"('Subroutine lect_donn : la '
     &           'declaration nombre')")
            WRITE(fmsgo,"('d''especes (dans le bloc '
     &           'particules) doit')")
            WRITE(fmsgo,"('preceder la declaration des '
     &           'especes dans les')")
            WRITE(fmsgo,"('blocs esp_particule et esp_fluide')")
            itesterr(1) = 1
          ENDIF
          IF (ALLOCATED(meth_inj)) THEN
            DEALLOCATE(meth_inj, nbppm, dynam, nbscycles, per_tri,
     &                 odml,
     &                 charge, masse, densite, vthx, vthy, vthz,
     &                 polyt,  limmx, limpx, freeze)
          ENDIF
!            nesp = nesp_p + nesp_f
            nesp = nesp_p 
            iesp_f = nesp_p
          ALLOCATE(meth_inj(nesp),   nbppm(nesp),     dynam(nesp), 
     &             nbscycles(nesp),  per_tri(nesp),   odml(nesp), 
     &             charge(nesp),     masse(nesp),     densite(nesp), 
     &             typ_distx(nesp),  typ_disty(nesp), typ_distz(nesp),
     &             vthx(nesp),       vthy(nesp),      vthz(nesp),
     &             vderx(nesp),      vdery(nesp),     vderz(nesp),
     &             polyt(nesp),      limmx(nesp),     limpx(nesp), 
     &             freeze(nesp))
     
          meth_inj(nesp)  =0 ; nbppm(nesp)    =0  ; dynam(nesp)    =0
          nbscycles(nesp) =0 ; per_tri(nesp)  =0  ; odml(nesp)     =0
          charge(nesp)    =0.; masse(nesp)    =0. ; densite(nesp)  =0.
          typ_distx(nesp) =0.; typ_disty(nesp)=0. ; typ_distz(nesp)=0.
          vthx(nesp)      =0.; vthy(nesp)     =0. ; vthz(nesp)     =0.
          vderx(nesp)     =0.; vdery(nesp)    =0. ; vderz(nesp)    =0.
          polyt(nesp)     =0.; limmx(nesp)    =0. ; limpx(nesp)    =0.
          freeze(nesp)    =0.

        CASE('esp_part')
          iesp_p = iesp_p + 1
          IF(iesp_p.GT.nesp_p) THEN
            WRITE(fmsgo,"('iesp_p : ', (I8,1X))") iesp_p
            WRITE(fmsgo,"('nesp_p : ', (I8,1X))") nesp_p
	    
            WRITE(fmsgo,"('Subroutine lect_donn : le nombre ou')")
            WRITE(fmsgo,"('l''emplacement des blocs esp_particule')")
            WRITE(fmsgo,"('est incompatible avec '
     &           'le nombre d''especes')")
            WRITE(fmsgo,"('lu par ailleurs dans '
     &           'le fichier de parametres')")
            itesterr(1) = 1
          ELSE
            READ(20,*,ERR=10) meth_inj(iesp_p), nbppm(iesp_p), 
     &                        dynam(iesp_p),    nbscycles(iesp_p)
            WRITE(fdbg,*)
     &       'nbscycles = ',  nbscycles(iesp_p)
            IF(nbscycles(iesp_p).GT.max_cycles) THEN 
	      max_cycles= nbscycles(iesp_p)	    
	    ENDIF     
     
            READ(20,*) 
            READ(20,*,ERR=10) per_tri(iesp_p)
            READ(20,*) 
            READ(20,*,ERR=10) charge(iesp_p), masse(iesp_p),
     &                        densite(iesp_p)
            READ(20,*) 
            READ(20,*,ERR=10) typ_distx(iesp_p), typ_disty(iesp_p),
     &                        typ_distz(iesp_p)
            WRITE(fdbg,*)
     &       'espece, typ_distx, typ_disty, typ_distz = ',iesp_p,
     &        typ_distx(iesp_p), typ_disty(iesp_p), typ_distz(iesp_p)
	    READ(20,*) 
            READ(20,*,ERR=10) vthx(iesp_p), vthy(iesp_p),
     &                        vthz(iesp_p), odml(iesp_p)
            WRITE(fdbg,*)
     &       'espece, vthx, vthy, vthz = ', iesp_p,
     &        vthx(iesp_p), vthy(iesp_p), vthz(iesp_p)     
            READ(20,*) 
            READ(20,*,ERR=10) vderx(iesp_p), vdery(iesp_p),
     &                        vderz(iesp_p)
            WRITE(fdbg,*)
     &       'espece, vderx, vdery, vderz = ', iesp_p,
     &        vderx(iesp_p), vdery(iesp_p), vderz(iesp_p)
            READ(20,*) 
            READ(20,*,ERR=10) limmx(iesp_p), limpx(iesp_p)
            polyt(iesp_p) = 0.
          ENDIF

        CASE('collisio')
! --- collisions classiques (=0) ou relativistes (=1)
            READ(20,*,ERR=10) col_regime
            READ(20,*) 
! --- pour les tests : calculer seulement les collisions =1 sinon =0
            READ(20,*,ERR=10) col_test
            READ(20,*) 
! --- nombre de types de collisions
            READ(20,*,ERR=10) col_nb
            READ(20,*) 
            IF(col_nb.GT.0) THEN
              ALLOCATE(col_p1(col_nb), col_p2(col_nb), 
     &                 col_per(col_nb),col_rnd(col_nb),
     &                 col_log(col_nb),
     &                 col_s0(col_nb), col_s1(col_nb),
     &                 col_dlt(col_nb))
!       initialisation des collisions
              DO i=1,col_nb
                READ(20,*,ERR=10) col_p1(i),  col_p2(i), 
     &                            col_per(i), col_rnd(i),
     &                            col_log(i)
		
                IF (col_regime.EQ.0) THEN 
		  CALL coll_init(i)
		ELSE
		  CALL coll_relat_init(i)
		ENDIF
              ENDDO
            ENDIF

        CASE('freeze_t')
          READ(20,*,ERR=10) freeze(1:nesp_p)

        CASE('corr_poi')
            READ(20,*,ERR=10) i, j
            pois_cor = (i.GT.0)
            pois_dia = (j.GT.0)

        CASE('diag_typ')
            READ(20,*,ERR=10) diag_typ

        CASE('diag_tem')
            READ(20,*,ERR=10) d_ex, d_ey, d_ez, d_bx, d_by, d_bz

        CASE('champ_lo')
            READ(20,*,ERR=10) clo_nb
            IF(clo_nb.NE.0) THEN
              ALLOCATE(clo_ch(clo_nb), clo_pos(3,clo_nb),
     &                                 clo_ind(3,clo_nb))
              READ(20,*) 
              DO i=1,clo_nb
                READ(20,*,ERR=10) clo_ch(i), clo_pos(1:3,i)
              ENDDO
            ENDIF

        CASE('champ_ma')
            READ(20,*,ERR=10) cmx_nb
            IF(cmx_nb.NE.0) THEN
              ALLOCATE(cmx_ch(cmx_nb))
              READ(20,*) 
              DO i=1,cmx_nb
                READ(20,*,ERR=10) cmx_ch(i)
              ENDDO
            ENDIF

        CASE('diag_pte')
            READ(20,*,ERR=10) dpt_ok, dpt_nblocs
	    print*,'dpt_ok, dpt_nblocs = ', dpt_ok, dpt_nblocs
            IF(nesp.EQ.0) THEN
              WRITE(fmsgo,"('Subroutine lect_donn : '
     &             'la definition du')")
              WRITE(fmsgo,"('nombre d''especes doit '
     &             'preceder l''appel au')")
              WRITE(fmsgo,"('diagnostic de particules-test')")
              itesterr(1) = 1
            ENDIF
            IF((dpt_ok.GT.0).AND.(nesp.GT.0)) THEN
              ALLOCATE(dpt_nbpt(nesp))
              dpt_nbpt(:) = 0
            ENDIF
            IF(dpt_nblocs.NE.0) THEN
              ALLOCATE(dpt_bl_esp(dpt_nblocs),dpt_bl_proba(dpt_nblocs))
              ALLOCATE(dpt_bl_xcn(dpt_nblocs),dpt_bl_ycn(dpt_nblocs),
     &                 dpt_bl_zcn(dpt_nblocs),dpt_bl_xrn(dpt_nblocs),
     &                 dpt_bl_yrn(dpt_nblocs),dpt_bl_zrn(dpt_nblocs),
     &                 dpt_bl_emn(dpt_nblocs),dpt_bl_emx(dpt_nblocs) )
            ENDIF

        CASE('def_ptes')
            dpt_ibloc = dpt_ibloc+1
            IF(dpt_ibloc.GT.dpt_nblocs) THEN
              WRITE(fmsgo,"('Subroutine lect_donn : '
     &             'la definition du')")
              WRITE(fmsgo,"('nombre d''especes doit '
     &             'preceder l''appel au')")
              WRITE(fmsgo,"('diagnostic de particules-test')")
              itesterr(1) = 1
            ENDIF
            READ(20,*,ERR=10) dpt_bl_esp(dpt_ibloc),
     &                        dpt_bl_proba(dpt_ibloc)
            READ(20,*) 
            READ(20,*,ERR=10) dpt_bl_xcn(dpt_ibloc),
     &                        dpt_bl_xrn(dpt_ibloc)
            READ(20,*,ERR=10) dpt_bl_ycn(dpt_ibloc),
     &                        dpt_bl_yrn(dpt_ibloc)
            READ(20,*,ERR=10) dpt_bl_zcn(dpt_ibloc),
     &                        dpt_bl_zrn(dpt_ibloc)
            READ(20,*) 
            READ(20,*,ERR=10) dpt_bl_emn(dpt_ibloc),
     &                        dpt_bl_emx(dpt_ibloc)
            print*, 'dpt_bl_esp(dpt_ibloc),dpt_bl_proba(dpt_ibloc) =',
     &        dpt_bl_esp(dpt_ibloc),dpt_bl_proba(dpt_ibloc)
            print*, 'dpt_bl_xcn(dpt_ibloc),dpt_bl_xrn(dpt_ibloc) =',
     &        dpt_bl_xcn(dpt_ibloc),dpt_bl_xrn(dpt_ibloc)
            print*, 'dpt_bl_emn(dpt_ibloc),dpt_bl_emx(dpt_ibloc) =',
     &        dpt_bl_emn(dpt_ibloc),dpt_bl_emx(dpt_ibloc)
     
        CASE('nb_diag_')
          READ(20,*,ERR=10) dsp_nb, dph_nb
          IF((dph_i+dsp_i).NE.0) THEN
          WRITE(fmsgo,"('Subroutine lect_donn : la declaration des')")
          WRITE(fmsgo,"('  nb maxi de diagnostics differents (bloc')")
          WRITE(fmsgo,"('  nb_diag_) doit preceder la declaration ')")
          WRITE(fmsgo,"('  de ces diagnostics dans les blocs     ')")
          WRITE(fmsgo,"('  diag_spa et diag_pha')")
            itesterr(1) = 1
          ENDIF
          IF( ALLOCATED(dsp) )  DEALLOCATE(dsp)
          ALLOCATE( dsp(dsp_nb) )
          dsp(:)%typ = 0
          IF( ALLOCATED(dph) )  DEALLOCATE(dph)
          ALLOCATE( dph(dph_nb) )
          dph(:)%ok = .false.

        CASE('diag_moy')
          READ(20,*,ERR=10) aux1, aux2
          dsp_moy   = aux1/pasdt
          dph_moy = aux2/pasdt

        CASE('diag_spa')
          dsp_i = dsp_i + 1
          READ(20,*,ERR=10) dsp_map, dsp(dsp_i)%moy, dsp(dsp_i)%per
	  WRITE(fmsgo,*) dsp_map, dsp(dsp_i)%moy, dsp(dsp_i)%per
          READ(20,*,ERR=10) dsp(dsp_i)%min(1), dsp(dsp_i)%max(1),
     &                      dsp(dsp_i)%siz(1)
          WRITE(fmsgo,*) dsp(dsp_i)%min(1), dsp(dsp_i)%max(1),
     &                      dsp(dsp_i)%siz(1)
          READ(20,*,ERR=10) dsp(dsp_i)%min(2), dsp(dsp_i)%max(2),
     &                      dsp(dsp_i)%siz(2)
          WRITE(fmsgo,*) dsp(dsp_i)%min(2), dsp(dsp_i)%max(2),
     &                      dsp(dsp_i)%siz(2)
          READ(20,*,ERR=10) dsp(dsp_i)%min(3), dsp(dsp_i)%max(3),
     &                      dsp(dsp_i)%siz(3)
          WRITE(fmsgo,*) dsp(dsp_i)%min(3), dsp(dsp_i)%max(3),
     &                      dsp(dsp_i)%siz(3)
          dsp(dsp_i)%typ = 0
          DO i=1,dsp_typ_nb
            IF(dsp_map.EQ.dsp_typ_map(i)) dsp(dsp_i)%typ = i
          ENDDO

        CASE('diag_pha')
          dph_i = dph_i + 1
          dph(dph_i)%ok = .true.
          READ(20,*,ERR=10) dph(dph_i)%per
	  WRITE(fmsgo,*) dph(dph_i)%per
	  READ(20,*,ERR=10) dph(dph_i)%ener(1),dph(dph_i)%ener(2)
	  WRITE(fmsgo,*) dph(dph_i)%ener(1),dph(dph_i)%ener(2)
          READ(20,*,ERR=10) dph(dph_i)%axe(1), dph(dph_i)%min(1),
     &                      dph(dph_i)%max(1), dph(dph_i)%siz(1)
          WRITE(fmsgo,*) dph(dph_i)%axe(1), dph(dph_i)%min(1),
     &                      dph(dph_i)%max(1), dph(dph_i)%siz(1)
          READ(20,*,ERR=10) dph(dph_i)%axe(2), dph(dph_i)%min(2),
     &                      dph(dph_i)%max(2), dph(dph_i)%siz(2)
          WRITE(fmsgo,*) dph(dph_i)%axe(2), dph(dph_i)%min(2),
     &                      dph(dph_i)%max(2), dph(dph_i)%siz(2)
          READ(20,*,ERR=10) dph(dph_i)%axe(3), dph(dph_i)%min(3),
     &                      dph(dph_i)%max(3), dph(dph_i)%siz(3)
          WRITE(fmsgo,*) dph(dph_i)%axe(3), dph(dph_i)%min(3),
     &                      dph(dph_i)%max(3), dph(dph_i)%siz(3)

        CASE('aide_deb')
          READ(20,*,ERR=10) debug

        CASE DEFAULT
          WRITE(fmsgo,"('Subroutine lect_donn :')")
          WRITE(fmsgo,"('bloc de donnees non reconnu (',A8,')')") h_bloc
          itesterr(1) = 1

        END SELECT
        READ(20,*)
        READ(20,'(A8)') h_bloc

        ENDDO


        dsp_nb = dsp_i
        dph_nb = dph_i
! Controles de coherence sommaires
! ----------------------------------------------------------------------
! La presence d'un champ magnetique statique impose 3 dimensions en impulsions         
	IF(mag_ok.EQ.1) THEN
	  IF((dim_cas.EQ.1).OR.(dim_cas.EQ.2)) dim_cas = 3
	  IF(dim_cas.EQ.4) dim_cas = 5
	ENDIF

        SELECT CASE(dim_cas)
        CASE(6)
          CONTINUE
        CASE(5,4)
!          nprocz = 1
          pasdz = 1._8 ; nbmz_gl = 1 ;
          centrez = 0._8 ; largz = 1._8 ; lgrmz = 0._8 ; lgrpz = 0._8
        CASE(3,2,1)
!          nprocy = 1 ; nprocz = 1
          pasdy = 1._8 ; nbmy_gl = 1 ; 
          pasdz = 1._8 ; nbmz_gl = 1 ; 
          centrey = 0._8 ; largy = 1._8 ; lgrmy = 0._8 ; lgrpy = 0._8
          centrez = 0._8 ; largz = 1._8 ; lgrmz = 0._8 ; lgrpz = 0._8
        END SELECT
! Arrondi interne des parametres
! ----------------------------------------------------------------------
        WRITE(fmsgo,"('pasdt     ', 1E25.20    )") pasdt
        WRITE(fmsgo,"('pasdx,y,z ',3(E25.20,1X))") pasdx, pasdy, pasdz
        WRITE(fmsgo,"('precision ',  I3        )") PRECISION(pasdt)



      RETURN
!
10    WRITE(fmsgo,*) 'Probleme de lecture dans le bloc ',h_bloc
      itesterr(1) = 1
      RETURN
!     ________________________
      END SUBROUTINE lect_donn
      
      
