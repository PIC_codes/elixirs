! ======================================================================
! fourier.f
! ----------
!
!     SUBROUTINE cpftv
!     SUBROUTINE rpf2iv
!     SUBROUTINE rpft2v
!
! ======================================================================



       SUBROUTINE cpftv(x,y,np,inc,sign,incv,nv,c)
! ======================================================================
!
!
!
! ======================================================================
       USE precisions
       
       IMPLICIT NONE
       
       INTEGER     ::    iflg=0
       
       REAL(kind=kr)                   :: x, y
       REAL(kind=kr), DIMENSION(2)     :: c

       REAL(kind=kr)    ::   tp, th, cc, ss, sign,
     &    xa, ya

       INTEGER     ::   np, nph, nex, npaux, 
     &    n1, n2, nd, ndh, mi, k, jv, l, inc, i, j, nbr, m, 
     &    ntv, nv, nvv, iv, incv, irc, ji, ij, iv, it, ircc 

       
	
	if(iflg.eq.np) goto 100
 !     calcul de nex par division
 !     attention modif......??????
	iflg=np
	nph=np/2
	tp=2.*3.1415926/float(np)
	th=0.0
 !
       nex=0
       npaux=np
       DO i=1,99
         npaux=npaux/2
         nex=nex+1
         if(npaux.eq.1) goto 6
       ENDDO
  
6      continue
 !
       DO i=1,np,2
         c(i)=cos(tp*th)
         c(i+1)=sin(tp*th)
         th=th+1.
       ENDDO
 
100    continue
       nd=np*inc
       ndh=nd/2
       nbr=nd-inc
       mi=2
       n2=nd
       n1=ndh
       DO k=1,nex
 !
 !   loop on nex levels.
 !
         m=1
         DO i=1,n1,inc
 !
 !   outer loop
 !
           cc=c(m)
           ss=c(m+1)*sign
           m=m+mi
           DO j=i,nd,n2
 !
 !   inner loop
 !
           l=j+n1
           jv=0
           ntv=nv
86         nvv=64
           if(ntv.lt.nvv)nvv=ntv
           ntv=ntv-nvv
 !dir$ ivdep
             DO iv=1,nvv
              xa=x(j+jv)-x(l+jv)
              ya=y(j+jv)-y(l+jv)
              x(j+jv)=x(j+jv)+x(l+jv)
              y(j+jv)=y(j+jv)+y(l+jv)
              x(l+jv)=xa*cc-ya*ss
              y(l+jv)=ya*cc+xa*ss
              jv=jv+incv
             ENDDO
 
            if(ntv.gt.0)go to 86
	    
           ENDDO
         ENDDO
         n2=n1
         n1=n1/2
         mi=mi+mi
	 
       ENDDO
 
       n1=nbr
       n2=(n1+inc)/2
       irc=0
       ji=0
       ij=0
       go to 22
 !
 ! even
 !
21     ij=n1-ij
       ji=n1-ji
       jv=1
       ntv=nv
87     nvv=64
       if(ntv.lt.nvv)nvv=ntv
       ntv=ntv-nvv
 !dir$ ivdep
 
       DO iv=1,nvv
        xa=x(ij+jv)
        x(ij+jv)=x(ji+jv)
        x(ji+jv)=xa
        ya=y(ij+jv)
        y(ij+jv)=y(ji+jv)
        y(ji+jv)=ya
        jv=jv+incv
       ENDDO
 
       if(ntv.gt.0)go to 87
       if(ij.gt.n2)go to 21
 !
 !  odd
 !
22     ij=ij+inc
       ji=ji+n2
       jv=1
       ntv=nv
88     nvv=64
       if(ntv.lt.nvv)nvv=ntv
       ntv=ntv-nvv
 !dir$ ivdep
       DO iv=1,nvv
        xa=x(ij+jv)
        x(ij+jv)=x(ji+jv)
        x(ji+jv)=xa
        ya=y(ij+jv)
        y(ij+jv)=y(ji+jv)
        y(ji+jv)=ya
        jv=jv+incv
       ENDDO
 
       if(ntv.gt.0)go to 88
       it=n2
23    it=it/2
       irc=irc-it
       ircc=irc
       if(ircc.ge.0)go to 23
       irc=irc+it+it
       ji=irc
       ij=ij+inc
       if(ij.lt.ji)go to 21
       if(ij.lt.n2)go to 22
       return
       
!      ____________________
       END SUBROUTINE cpftv
       
       
       SUBROUTINE rpf2iv(x,y,np,inc,incv,nv)
! ======================================================================
!
!
!
! ======================================================================
       USE precisions       
       
       IMPLICIT NONE
       
       REAL(kind=kr)     :: x, y 
       REAL(kind=kr)     :: xa, ya
       
       INTEGER     ::     i, ntv, nhm, np, i1, i2, inc, 
     &     nv, jv, nvv, iv
       
       
 !ccc  np=shiftl(1,nex)
       nhm=np/2-1
       i1=inc+1
       i2=(np-1)*inc+1
       
       DO i=1,nhm
         ntv=nv
         jv=0
80       nvv=64
         if(ntv.lt.nvv)nvv=ntv
         ntv=ntv-nvv
 !dir$ ivdep
         DO iv=1,nvv
           ya=y(i1+jv)-x(i2+jv)
           xa=y(i1+jv)+x(i2+jv)
           x(i2+jv)=x(i1+jv)+y(i2+jv)
           x(i1+jv)=x(i1+jv)-y(i2+jv)
           y(i2+jv)=ya
           y(i1+jv)=xa
           jv=jv+incv
         ENDDO
	 
         if(ntv.gt.0)go to 80
         i1=i1+inc
         i2=i2-inc
       ENDDO
 
       return       
!      _____________________
       END SUBROUTINE rpf2iv
       
       
       SUBROUTINE rpft2v(x,y,np,inc,incv,nv)
! ======================================================================
!
!
!
! ======================================================================
       USE precisions       
       
       IMPLICIT NONE
       

       REAL(kind=kr)     :: x, y
       REAL(kind=kr)     :: xa, ya
       
       INTEGER      ::  i1, i2, inc, incv, np, nv
     &           iv, jv
       
 !cccc np=shiftl(1,nex)
       nhm=np/2-1
       i1=inc+1
       i2=(np-1)*inc+1
       
       DO i=1,nhm
         ntv=nv
         jv=0
 80      nvv=64
         if(ntv.lt.nvv)nvv=ntv
         ntv=ntv-nvv
 !dir$ ivdep
           DO iv=1,nvv
             xa=x(i1+jv)+x(i2+jv)
             ya=y(i1+jv)-y(i2+jv)
             y(i1+jv)=y(i1+jv)+y(i2+jv)
             y(i2+jv)=x(i2+jv)-x(i1+jv)
             x(i1+jv)=xa
             x(i2+jv)=ya
             jv=jv+incv
           ENDDO
 
         if(ntv.gt.0)go to 80
	 
         i1=i1+inc
         i2=i2-inc
       ENDDO
 
       jv=0
 !dir$ ivdep
 
       DO iv=1,nv
         x(1+jv)=2.*x(1+jv)
         y(1+jv)=2.*y(1+jv)
         x(i1+jv)=2.*x(i1+jv)
         y(i1+jv)=2.*y(i1+jv)
         jv=jv+incv
       ENDDO
  
  
       return       
!      _____________________       
       END SUBROUTINE rpft2v
       
            
              
