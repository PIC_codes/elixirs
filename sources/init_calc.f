! ======================================================================
! init_calc.f
! -----------
!
!      SUBROUTINE init_calc
!      SUBROUTINE cree_plasma
!      SUBROUTINE cree_cell_p
!      SUBROUTINE calcul_poids
!      SUBROUTINE calcul_theta
!      SUBROUTINE aleat_dml2d
!      FUNCTION prof_dens
!      SUBROUTINE init_poids_front
!      SUBROUTINE aleat_unif
!      SUBROUTINE aleat_gauss
!      FUNCTION suite_equidistribuee
!
! ======================================================================

      SUBROUTINE init_calc
! ======================================================================
! Reprise ou creation du plasma
! ======================================================================
      USE temps
      USE domaines

      USE erreurs

      IMPLICIT NONE

      INTEGER    ::   ix, iy, iz
      

      INTERFACE
        SUBROUTINE calcul_theta(ix,iy,iz)
          USE precisions
          INTEGER, INTENT(IN)         :: ix, iy, iz 
        END SUBROUTINE calcul_theta
      END INTERFACE

! Creation du plasma ou reprise d'un cas
! ----------------------------------------------------------------------
      CALL cree_plasma
      itdeb=1

      IF(tta_def.EQ.1) THEN
      thetad_2 = 0.
      
! --- Boucle sur les mailles
        DO iz=nulmz+1, nulpz
          DO iy=nulmy+1, nulpy
            DO ix=nulmx+1, nulpx	 
! --- Initialisation d une maille du tableau des amortissements
              CALL calcul_theta(ix,iy,iz)
	    ENDDO
          ENDDO
        ENDDO
	
      thetad_2(:,nulmy) = thetad_2(:,nulpy)
      thetad_2(:,nulpy+1) = thetad_2(:,nulmy+1)	
      ENDIF

      CALL init_poids_front

      RETURN
!     ________________________
      END SUBROUTINE init_calc



      SUBROUTINE cree_plasma
! ======================================================================
! Creation du plasma dans le domaine local
! ======================================================================

      USE domaines
      USE domaines_klder
      USE particules_klder
      USE erreurs

      IMPLICIT NONE

      REAL(KIND=kr), DIMENSION(:), ALLOCATABLE :: moisson
      REAL(KIND=kr)   :: poids_df
      REAL(KIND=kr)   :: gm, vxm, vym, vzm, tx, ty, tz
      INTEGER         :: iesp, nbp, methode, nbm, ix, iy, iz

      INTERFACE
        SUBROUTINE cree_cell_p(ix,iy,iz,iesp,nbm,nbp,poids_df,moisson)
          USE precisions
          INTEGER, INTENT(IN)         :: ix, iy, iz, iesp
          INTEGER, INTENT(INOUT)      :: nbm, nbp
          REAL(KIND=kr), INTENT(IN)   :: poids_df
          REAL(KIND=kr), DIMENSION(:) :: moisson
        END SUBROUTINE cree_cell_p
        SUBROUTINE cree_cell_f(ix,iy,iz,iesp,nbm,poids_df)
          USE precisions
          INTEGER, INTENT(IN)         :: ix, iy, iz, iesp
          INTEGER, INTENT(INOUT)      :: nbm
          REAL(KIND=kr), INTENT(IN)   :: poids_df
        END SUBROUTINE cree_cell_f
      END INTERFACE


! Boucle sur les especes de particules
! ----------------------------------------------------------------------
      DO iesp=1,nesp_p
      
      methode = meth_inj(iesp)
      nbpa(iesp) = 0
      IF(methode.EQ.0) CYCLE

      nbp = 0
      nbm = 0
! --- poids d'une cellule a la densite maximale
      IF(charge(iesp).NE.0.) THEN
        poids_df = densite(iesp) * pasdx*pasdy*pasdz / ABS(charge(iesp))
      ELSE
        poids_df = densite(iesp) * pasdx*pasdy*pasdz
      ENDIF

      IF(methode.EQ.5) THEN
! --- initialisation avec fonctions (isotropes, 2D) de DML
        ALLOCATE(moisson(2*nbppm(iesp)))
      ELSE
        ALLOCATE(moisson(  nbppm(iesp)))
      ENDIF           
      
! --- Boucle sur les mailles
      DO iz=nulmz+1, nulpz
        DO iy=nulmy+1, nulpy
          DO ix=nulmx+1, nulpx	  
! ------    Initialisation d une cellule de plasma	  
            CALL cree_cell_p(ix,iy,iz,iesp,nbm,nbp,poids_df,moisson)
          ENDDO
        ENDDO
      ENDDO

      DEALLOCATE(moisson)

! --- On sauve le nombre de maille qui contiennent du plasma
      nbm_plas = FLOAT( nbm )

      IF(debug.GE.1) THEN
        WRITE(fdbg,*) 'Subroutine cree_plasma :'
        WRITE(fdbg,*) 'Creation de ',nbp,' particules d''espece ',iesp
        WRITE(fdbg,*) nbm,' mailles trouvees dans le plasma'
        WRITE(fdbg,*) 'X min : ', MINVAL(partic(a_qx, 1:nbp, iesp))
        WRITE(fdbg,*) 'X max : ', MAXVAL(partic(a_qx, 1:nbp, iesp))
        IF(a_qy.NE.0) THEN
          WRITE(fdbg,*) 'Y min : ', MINVAL(partic(a_qy, 1:nbp, iesp))
          WRITE(fdbg,*) 'Y max : ', MAXVAL(partic(a_qy, 1:nbp, iesp))
        ENDIF
        IF(a_qz.NE.0) THEN
          WRITE(fdbg,*) 'Z min : ', MINVAL(partic(a_qz, 1:nbp, iesp))
          WRITE(fdbg,*) 'Z max : ', MAXVAL(partic(a_qz, 1:nbp, iesp))
        ENDIF
      ENDIF

      nbpa(iesp) = nbp

      IF(nbp.GT.0) THEN
      tx = 0.
      ty = 0.
      tz = 0.
      
! Calcul de quelques moments
        SELECT CASE(dim_cas)
        CASE(1)
          IF (dynam(iesp).EQ.1) THEN
	    vxm = SUM(partic(a_px,1:nbp,iesp))/REAL(nbp)
            gm = 0.5_8*SUM(partic(a_px,1:nbp,iesp)**2)/REAL(nbp)
          ELSE
            vxm = SUM(partic(a_px,1:nbp,iesp)
     &      / SQRT(1._8+partic(a_px,1:nbp,iesp)**2))/REAL(nbp)
            gm = SUM(SQRT(1._8+partic(a_px,1:nbp,iesp)**2))/REAL(nbp)
            tx = SUM(partic(a_px,1:nbp,iesp)**2
     &     / SQRT(1._8+partic(a_px,1:nbp,iesp)**2))/REAL(nbp)
          ENDIF     
          WRITE(fdbg,*) 'vxm = ',vxm
          WRITE(fdbg,*) 'gm = ', gm
          WRITE(fdbg,*) 'tx = ',tx
        CASE(2,4)
          IF (dynam(iesp).EQ.1) THEN  
	    vxm = SUM(partic(a_px,1:nbp,iesp))/REAL(nbp)
	    vym = SUM(partic(a_py,1:nbp,iesp))/REAL(nbp)
	    gm = 0.5_8*SUM(partic(a_px,1:nbp,iesp)**2
     &       + partic(a_py,1:nbp,iesp)**2)/REAL(nbp)
          ELSE
            vxm = SUM(partic(a_px,1:nbp,iesp)
     &      / SQRT(1._8+partic(a_px,1:nbp,iesp)**2
     &      + partic(a_py,1:nbp,iesp)**2))/REAL(nbp)
            vym = SUM(partic(a_py,1:nbp,iesp)
     &      / SQRT(1._8+partic(a_px,1:nbp,iesp)**2
     &      + partic(a_py,1:nbp,iesp)**2))/REAL(nbp)
            gm = SUM(SQRT(1._8+partic(a_px,1:nbp,iesp)**2
     &     + partic(a_py,1:nbp,iesp)**2))/REAL(nbp)
            tx = SUM(partic(a_px,1:nbp,iesp)**2
     &     / SQRT(1._8+partic(a_px,1:nbp,iesp)**2
     &     + partic(a_py,1:nbp,iesp)**2))/REAL(nbp)
            ty = SUM(partic(a_py,1:nbp,iesp)**2
     &     / SQRT(1._8+partic(a_px,1:nbp,iesp)**2
     &     + partic(a_py,1:nbp,iesp)**2))/REAL(nbp)  
          ENDIF      
          WRITE(fdbg,*) 'vxm, vym = ',vxm, vym
          WRITE(fdbg,*) 'gm = ', gm
          WRITE(fdbg,*) 'tx, ty = ',tx, ty	                
        CASE(3,5,6)
          IF (dynam(iesp).EQ.1) THEN  
	    vxm = SUM(partic(a_px,1:nbp,iesp))/REAL(nbp)
	    vym = SUM(partic(a_py,1:nbp,iesp))/REAL(nbp)
	    vzm = SUM(partic(a_pz,1:nbp,iesp))/REAL(nbp)
	    gm = 0.5_8*SUM(partic(a_px,1:nbp,iesp)**2
     &       + partic(a_py,1:nbp,iesp)**2
     &       + partic(a_pz,1:nbp,iesp)**2)/REAL(nbp) 
	  ELSE     
            vxm = SUM(partic(a_px,1:nbp,iesp)
     &      / SQRT(1._8+partic(a_px,1:nbp,iesp)**2
     &      + partic(a_py,1:nbp,iesp)**2
     &	    + partic(a_pz,1:nbp,iesp)**2))/REAL(nbp)
            vym = SUM(partic(a_py,1:nbp,iesp)
     &      / SQRT(1._8+partic(a_px,1:nbp,iesp)**2
     &      + partic(a_py,1:nbp,iesp)**2
     &	    + partic(a_pz,1:nbp,iesp)**2))/REAL(nbp)
            vzm = SUM(partic(a_pz,1:nbp,iesp)
     &      / SQRT(1._8+partic(a_px,1:nbp,iesp)**2
     &      + partic(a_py,1:nbp,iesp)**2
     &	    + partic(a_pz,1:nbp,iesp)**2))/REAL(nbp)       
            gm = SUM(SQRT(1._8+partic(a_px,1:nbp,iesp)**2
     &     + partic(a_py,1:nbp,iesp)**2
     &     + partic(a_pz,1:nbp,iesp)**2))/REAL(nbp)
            tx = SUM(partic(a_px,1:nbp,iesp)**2
     &     / SQRT(1._8+partic(a_px,1:nbp,iesp)**2
     &     + partic(a_py,1:nbp,iesp)**2
     &     + partic(a_pz,1:nbp,iesp)**2))/REAL(nbp)
            ty = SUM(partic(a_py,1:nbp,iesp)**2
     &     / SQRT(1._8+partic(a_px,1:nbp,iesp)**2
     &     + partic(a_py,1:nbp,iesp)**2
     &     + partic(a_pz,1:nbp,iesp)**2))/REAL(nbp)
            tz = SUM(partic(a_pz,1:nbp,iesp)**2
     &     / SQRT(1._8+partic(a_px,1:nbp,iesp)**2
     &     + partic(a_py,1:nbp,iesp)**2
     &     + partic(a_pz,1:nbp,iesp)**2))/REAL(nbp)            
          ENDIF
          WRITE(fdbg,*) 'vxm, vym, vzm = ',vxm, vym, vzm
          WRITE(fdbg,*) 'gm = ', gm
          WRITE(fdbg,*) 'tx, ty, tz = ',tx, ty, tz	
        END SELECT
      ENDIF
            
! --- Fin de la boucle sur les especes de particules
      ENDDO


      IF(debug.GE.1) WRITE(fdbg,"(' ')")

      RETURN
!     __________________________
      END SUBROUTINE cree_plasma



      SUBROUTINE cree_cell_p(ix,iy,iz,iesp,nbm,nbp,poids_df,moisson)
! ======================================================================
! Tirage des positions et vitesses des particules de l'espece iesp
! dans la cellule (ix,iy,iz)
! Note : on met 2 fois moins de particules/maille sous 0.1*n_max
! ======================================================================

      USE domaines
      USE domaines_klder
      USE particules_klder
      USE temps
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)         :: ix,  iy,  iz, iesp
      INTEGER, INTENT(INOUT)      :: nbm, nbp
      REAL(KIND=kr), INTENT(IN)   :: poids_df
      REAL(KIND=kr), DIMENSION(:) :: moisson
      REAL(KIND=kr), DIMENSION(2) :: angles
      REAL(KIND=kr), DIMENSION(:),ALLOCATABLE :: ppar,pper

      REAL(KIND=8)    :: depi
      REAL(KIND=kr)   :: v_der, vder, gder, vthder, oxmax
      REAL(KIND=kr)   :: gamma, pu, pv, pw
      REAL(KIND=kr)   :: ctetad, stetad, cphid, sphid
      REAL(KIND=kr)   :: poids_rel, poids
!      REAL(KIND=kr)   :: poids_r1, poids_r2
      REAL(KIND=kr)   :: qx, qy, qz, aux
      INTEGER         :: nbp_cell, methode, n1, n2, ip
      INTEGER(KIND=8) :: graine

      INTERFACE
        SUBROUTINE calcul_poids(ix,iy,iz, iesp, qx,qy,qz, poids_rel)
          USE precisions
          INTEGER         , INTENT(IN)  :: ix, iy, iz, iesp
          REAL(KIND=kr)   , INTENT(OUT) :: qx, qy, qz
          REAL(KIND=kr)   , INTENT(OUT) :: poids_rel
        END SUBROUTINE calcul_poids
        SUBROUTINE aleat_unif(gerbe,methode,graine)
          USE precisions
          REAL(KIND=kr), DIMENSION(:), INTENT(OUT)   :: gerbe
          INTEGER                    , INTENT(IN)    :: methode
          INTEGER(KIND=8)            , INTENT(INOUT) :: graine
        END SUBROUTINE aleat_unif
        SUBROUTINE aleat_gaus(gerbe,methode,graine)
          USE precisions
          REAL(KIND=kr), DIMENSION(:), INTENT(OUT)   :: gerbe
          INTEGER                    , INTENT(IN)    :: methode
          INTEGER(KIND=8)            , INTENT(INOUT) :: graine
        END SUBROUTINE aleat_gaus
        SUBROUTINE aleat_dml2d(gerbe,methode,graine,vth,ordre)
          USE precisions
          REAL(KIND=kr), DIMENSION(:), INTENT(OUT)   :: gerbe
          INTEGER                    , INTENT(IN)    :: methode, ordre
          INTEGER(KIND=8)            , INTENT(INOUT) :: graine
          REAL(KIND=kr)              , INTENT(IN)    :: vth
        END SUBROUTINE aleat_dml2d
      END INTERFACE
      
!      IF(debug.GE.1) THEN
!        WRITE(fdbg,*) 'Subroutine cree_cell :'
!        WRITE(fdbg,*) 'Creation des particules de l''espece ',iesp
!        WRITE(fdbg,*) nbm,' mailles trouvees dans le plasma'
!      ENDIF
      

      depi = 2._8*pi
      graine = 0
      methode = meth_inj(iesp)
      IF(methode.EQ.0) RETURN

! --- Poids associe ?
      CALL calcul_poids(ix,iy,iz,iesp,qx,qy,qz,poids_rel)
      poids = poids_df*poids_rel
!      WRITE(fdbg,"('poids_df  : ', (F8.2,1X))"), poids_df
!      WRITE(fdbg,"('poids_rel : ', (F8.2,1X))"), poids_rel
!      write(fdbg,"('avant RETURN si poids nul')")
      IF(poids.EQ.0) RETURN
!      write(fdbg,"('poids est non nul')")

! --- On affecte le poids de la maille sur les particles de la cellule
! ----------------------------------------------------------------------
      IF(methode.EQ.4) graine=-(ix+nbmx_gl*(iy+nbmy_gl*iz))
      nbp_cell = nbppm(iesp)
      IF(poids_rel.LT.0.1) nbp_cell = (nbp_cell+1)/2
      n1 = nbp + 1
      n2 = nbp + nbp_cell

! --- On tire nbppm(iesp) nombres aleatoires par coordonnee Q...
      IF(a_qx.NE.0) THEN
        CALL aleat_unif(moisson(:),methode,graine)
        partic(a_qx, n1:n2, iesp) = (moisson(1:nbp_cell)-0.5)*pasdx+qx
      ENDIF
      IF(a_qy.NE.0) THEN
        CALL aleat_unif(moisson(:),methode,graine)
        partic(a_qy, n1:n2, iesp) = (moisson(1:nbp_cell)-0.5)*pasdy+qy
      ENDIF
      IF(a_qz.NE.0) THEN
        CALL aleat_unif(moisson(:),methode,graine)
        partic(a_qz, n1:n2, iesp) = (moisson(1:nbp_cell)-0.5)*pasdz+qz
      ENDIF

! --- On tire nbppm(iesp) nombres aleatoires par coordonnee V
! distribution DML non relativiste
      IF((methode.EQ.5).AND.(typ_distx(iesp).EQ.0)
     &                 .AND.(typ_disty(iesp).EQ.0)) THEN   
        CALL aleat_dml2d(moisson(:),methode,graine,vthx(iesp),
     &                   odml(iesp))
        partic(a_px, n1:n2, iesp) =
     &                       vthx(iesp)*moisson(         1:nbp_cell  )
        partic(a_py, n1:n2, iesp) =
     &                       vthy(iesp)*moisson(nbp_cell+1:nbp_cell*2)
      ELSE
        SELECT CASE(typ_distx(iesp))
	CASE(0)
! Maxwelienne
!          WRITE(fdbg,*) 'En x : Maxwellienne avec vthx, vderx = ',
!     &         vthx(iesp),vderx(iesp) 
          CALL aleat_gaus(moisson(:),methode,graine)
          IF(a_px.NE.0) partic(a_px, n1:n2, iesp) = 
     &      vderx(iesp) + vthx(iesp)*moisson(1:nbp_cell)
	CASE(1)
! Maxwell-Juttner 1d
!          WRITE(fdbg,*) 'En x : Maxwell-Juttner avec vthx, vderx = ',
!     &        vthx(iesp),vderx(iesp)          	
          vder=vderx(iesp)
	  gder=1._8/SQRT(1._8-vder**2)
	  vthder = gder*vthx(iesp)/(masse(iesp)*511._8)
          oxmax = SQRT(20._8*vthder*(1._8+5._8*vthder))	      	  
	  IF(vder.EQ.0.) THEN
c Inversion directe de la fd isotrope	  
            CALL aleat_unif(moisson(:),methode,graine)
	    DO ip=n1,n2
              CALL inverse(inj_distx(:,iesp),2000,oxmax,
     &                     moisson(ip-n1+1),aux)
              CALL aleat_unif(angles(1:1),methode,graine)
	      partic(a_px, ip, iesp) = SIGN(aux,angles(1)-0.5_8)
	    ENDDO
	  ELSE
! On applique une methode mixte d'inversion/rejet	  
            DO ip=n1,n2
              DO
! Inversion directe du facteur isotrope dans le repere mobile	      
                CALL RANDOM_NUMBER(moisson(1:1))
                CALL inverse(inj_distx(:,iesp),2000,oxmax,
     &                       moisson(1:1),aux)
                gamma=SQRT(1._8+aux**2) 
	        CALL aleat_unif(angles(1:1),methode,graine)
	        pu = SIGN(aux,angles(1)-0.5_8)
! On echantillonne le facteur (1+vder*pu/gamma)/(1+abs(vder))	      
	        CALL RANDOM_NUMBER(moisson(1:1))
	        IF(moisson(1).LT.(1._8+vder*pu/gamma)
     &                         /(1._8+ABS(vder))) EXIT
              ENDDO
! Transformee de Lorentz inverse	    
	      partic(a_px, ip, iesp) = gder*(pu+vder*gamma)
	    ENDDO
	  ENDIF
	CASE(2)
! Maxwell-Juttner 2d
!          WRITE(fdbg,*) 'En xy : Maxwell-Juttner avec vth, vder[xy] =',
!     &         vthx(iesp),vderx(iesp),vdery(iesp)  
          vder=SQRT(vderx(iesp)**2+vdery(iesp)**2)
	  gder=1._8/SQRT(1._8-vder**2)
	  vthder = gder*vthx(iesp)/(masse(iesp)*511._8)
          oxmax = SQRT(20._8*vthder*(1._8+5._8*vthder))	      	  
	  IF(vder.EQ.0.) THEN
c Inversion directe de la fd isotrope	  
            CALL aleat_unif(moisson(:),methode,graine)
	    DO ip=n1,n2
              CALL inverse(inj_distx(:,iesp),2000,oxmax,
     &                     moisson(ip-n1+1),aux)
              CALL aleat_unif(angles(1:1),methode,graine)
	      partic(a_px, ip, iesp) = aux*COS(depi*angles(1))
              partic(a_py, ip, iesp) = aux*SIN(depi*angles(1))
	    ENDDO
	  ELSE
! On se place dans le referentiel dont l'un des axes est // (vderx,vdery)     
! On effectue une transformee de Lorentz dans le repere mobile et on
! applique une methode mixte d'inversion/rejet	  
            ctetad=vderx(iesp)/vder
            stetad=vdery(iesp)/vder
            DO ip=n1,n2
              DO
! Inversion directe du facteur isotrope dans le repere mobile	      
                CALL RANDOM_NUMBER(moisson(1:1))
                CALL inverse(inj_distx(:,iesp),2000,oxmax,
     &                       moisson(1:1),aux)
                gamma=SQRT(1._8+aux**2) 
	        CALL aleat_unif(angles(1:1),methode,graine)
                pu = aux*COS(depi*angles(1))
                pv = aux*SIN(depi*angles(1))
! On echantillonne le facteur (1+vder*pu/gamma)/(1+abs(vder))	      
	        CALL RANDOM_NUMBER(moisson(1:1))
	        IF(moisson(1).LT.(1._8+vder*pu/gamma)
     &                         /(1._8+ABS(vder))) EXIT
              ENDDO
! Transformee de Lorentz inverse	    
	      pu = gder*(pu+vder*gamma)
! Retour dans le referentiel du labo
              partic(a_px, ip, iesp) = ctetad*pu-stetad*pv
              partic(a_py, ip, iesp) = stetad*pu+ctetad*pv	      	  	  
	    ENDDO
	  ENDIF            
	CASE(3)
! Maxwell-Juttner 3d
!          WRITE(fdbg,*) 'En xyz : Maxwell-Juttner avec vth, vder[xyz]=',
!     &         vthx(iesp),vderx(iesp),vdery(iesp),vderz(iesp)  
          vder=SQRT(vderx(iesp)**2+vdery(iesp)**2+vderz(iesp)**2)
	  gder=1._8/SQRT(1._8-vder**2)
	  vthder = gder*vthx(iesp)/(masse(iesp)*511._8)
          oxmax = SQRT(20._8*vthder*(1._8+5._8*vthder))	      	  
	  IF(vder.EQ.0.) THEN
c Inversion directe de la fd isotrope	  
            CALL aleat_unif(moisson(:),methode,graine)
	    DO ip=n1,n2
              CALL inverse(inj_distx(:,iesp),2000,oxmax,
     &                     moisson(ip-n1+1),aux)
              CALL aleat_unif(angles(1:2),methode,graine)
              angles(1)=ACOS(1._8-2._8*angles(1))
              angles(2)=depi*angles(2)
              partic(a_px, ip, iesp) = aux*COS(angles(1))
              partic(a_py, ip, iesp) = aux*SIN(angles(1))
     &                               * COS(angles(2))
              partic(a_pz, ip, iesp) = aux*SIN(angles(1))
     &                               * SIN(angles(2))
	    ENDDO
	  ELSE
! On se place dans le referentiel dont l'un des axes est // ,(vderx,vdery,vderz)     
! On effectue une transformee de Lorentz dans le repere mobile et on
! applique une methode mixte d'inversion/rejet	
            ctetad=vderx(iesp)/vder
            stetad=SQRT(1._8-ctetad**2)
            IF(stetad.EQ.0.) THEN
              cphid = 1._8
              sphid = 0.
            ELSE
              cphid=vdery(iesp)/(vder*stetad)
              sphid=vderz(iesp)/(vder*stetad)
            ENDIF	  	  
            DO ip=n1,n2
              DO
! Inversion directe du facteur isotrope dans le repere mobile	      
                CALL RANDOM_NUMBER(moisson(1:1))
                call inverse(inj_distx(:,iesp),2000,oxmax,
     &                       moisson(1:1),aux)
                gamma=SQRT(1._8+aux**2) 
	        CALL aleat_unif(angles(1:2),methode,graine)
                angles(1)=ACOS(1._8-2._8*angles(1))
                angles(2)=depi*angles(2)
                pu = aux*COS(angles(1))
                pv = aux*SIN(angles(1))*COS(angles(2))
                pw = aux*SIN(angles(1))*SIN(angles(2))
! On echantillonne le facteur (1+vder*pu/gamma)/(1+abs(vder))	      
	        CALL RANDOM_NUMBER(moisson(1:1))
	        IF(moisson(1).LT.(1._8+vder*pu/gamma)
     &                         /(1._8+ABS(vder))) EXIT
              ENDDO
! Transformee de Lorentz inverse	    
	      pu = gder*(pu+vder*gamma)	    
! Retour dans le referentiel du labo
              partic(a_px, ip, iesp) = ctetad*pu-stetad*pv
              partic(a_py, ip, iesp) = stetad*cphid*pu
     &                               + ctetad*cphid*pv-sphid*pw
	      partic(a_pz, ip, iesp) = stetad*sphid*pu
     &                               + ctetad*sphid*pv+cphid*pw
	    ENDDO
	  ENDIF	
	CASE(4)
! Waterbag anisotrope
!          WRITE(fdbg,*) 'En x : waterbag avec vthx, vderx = ',
!     &        vthx(iesp),vderx(iesp)  
          CALL aleat_unif(moisson(:),methode,graine)  
	  IF(a_px.NE.0) partic(a_px, n1:n2, iesp) = 
     &      vderx(iesp)+vthx(iesp)*(2._8*moisson(1:nbp_cell)-1._8)
     
	CASE(5)
! Waterbag anisotrope oblique 2d 
!          WRITE(fdbg,*) 'En xy : waterbag oblique avec vth, vder[xy] = ',
!     &        vthx(iesp),vthy(iesp),vderx(iesp),vdery(iesp)
! Impulsions paralleles et perpendiculaires a la derive
          ALLOCATE(ppar(n1:n2),pper(n1:n2))
	  IF ((vderx(iesp).EQ.0.).AND.(vdery(iesp).EQ.0.)) THEN
	    ctetad=1._8
	    stetad=0.
	  ELSE
	    vder=SQRT(vderx(iesp)**2+vdery(iesp)**2)
	    ctetad=vderx(iesp)/vder
            stetad=vdery(iesp)/vder
	  ENDIF
          CALL aleat_unif(moisson(:),methode,graine)  
	  IF(a_px.NE.0) ppar(n1:n2) = 
     &      vthx(iesp)*(2._8*moisson(1:nbp_cell)-1._8)
          CALL aleat_unif(moisson(:),methode,graine)  
	  IF(a_py.NE.0) pper(n1:n2) = 
     &      vthy(iesp)*(2._8*moisson(1:nbp_cell)-1._8)
! Impulsions dans le repere du labo
          partic(a_px, n1:n2, iesp) = vderx(iesp)+ppar(n1:n2)*ctetad - 
     &      pper(n1:n2)*stetad
          partic(a_py, n1:n2, iesp) = vdery(iesp)+ppar(n1:n2)*stetad + 
     &      pper(n1:n2)*ctetad     
	  DEALLOCATE(ppar,pper)  
	  
	END SELECT
	
        SELECT CASE(typ_disty(iesp))
	CASE(0)
! Maxwelienne	
!          WRITE(fdbg,*) 'En y : Maxwellienne avec vthy, vdery = ',
!     &      vthy(iesp),vdery(iesp)         
          CALL aleat_gaus(moisson(:),methode,graine)
          IF(a_py.NE.0) partic(a_py, n1:n2, iesp) = 
     &      vdery(iesp) + vthy(iesp)*moisson(1:nbp_cell)
	CASE(1)
! Maxwell-Juttner 1d	
!          WRITE(fdbg,*) 'En y : Maxwell-Juttner avec vthy, vdery = ',
!     &      vthy(iesp),vdery(iesp)          	
          vder=vdery(iesp)
	  gder=1._8/SQRT(1._8-vder**2)
	  vthder = gder*vthy(iesp)/(masse(iesp)*511._8)
          oxmax = SQRT(20._8*vthder*(1._8+5._8*vthder))	      	  
	  IF(vder.EQ.0.) THEN
c Inversion directe de la fd isotrope	  
            CALL aleat_unif(moisson(:),methode,graine)
	    DO ip=n1,n2
              CALL inverse(inj_disty(:,iesp),2000,oxmax,
     &                     moisson(ip-n1+1),aux)
              CALL aleat_unif(angles(1:1),methode,graine)
	      partic(a_py, ip, iesp) = SIGN(aux,angles(1)-0.5_8)
	    ENDDO
	  ELSE
! On applique une methode mixte d'inversion/rejet	  
            DO ip=n1,n2
              DO
! Inversion directe du facteur isotrope dans le repere mobile	      
                CALL RANDOM_NUMBER(moisson(1:1))
                call inverse(inj_disty(:,iesp),2000,oxmax,
     &                       moisson(1:1),aux)
                gamma=SQRT(1._8+aux**2) 
	        CALL aleat_unif(angles(1:1),methode,graine)
	        pu = SIGN(aux,angles(1)-0.5_8)
! On echantillonne le facteur (1+vder*pu/gamma)/(1+abs(vder))	      
	        CALL RANDOM_NUMBER(moisson(1:1))
	        IF(moisson(1).LT.(1._8+vder*pu/gamma)
     &                         /(1._8+ABS(vder))) EXIT
              ENDDO
! Transformee de Lorentz inverse	    
	      partic(a_py, ip, iesp) = gder*(pu+vder*gamma)
	    ENDDO
	  ENDIF	 	
	CASE(4)
! Waterbag anisotrope
!          WRITE(fdbg,*) 'En y : waterbag avec vthy, vdery =',
!     &        vthy(iesp),vdery(iesp)       
          CALL aleat_unif(moisson(:),methode,graine)  
	  IF(a_py.NE.0) partic(a_py, n1:n2, iesp) = 
     &      vdery(iesp)+vthy(iesp)*(2._8*moisson(1:nbp_cell)-1._8) 
	END SELECT
	
        SELECT CASE(typ_distz(iesp))
	CASE(0)
! Maxwelienne	
!          WRITE(fdbg,*) 'En z : Maxwellienne avec vthz, vderz = ',
!     &        vthz(iesp),vderz(iesp)          	
          CALL aleat_gaus(moisson(:),methode,graine)
          IF(a_pz.NE.0) partic(a_pz, n1:n2, iesp) = 
     &      vderz(iesp) + vthz(iesp)*moisson(1:nbp_cell)
	CASE(1)
! Maxwell-Juttner 1d		
!          WRITE(fdbg,*) 'En z : Maxwell-Juttner avec vthz, vderz = ',
!     &        vthz(iesp),vderz(iesp)          	
          vder=vderz(iesp)
	  gder=1._8/SQRT(1._8-vder**2)
	  vthder = gder*vthz(iesp)/(masse(iesp)*511._8)
          oxmax = SQRT(20._8*vthder*(1._8+5._8*vthder))	      	  
	  IF(vder.EQ.0.) THEN
c Inversion directe de la fd isotrope	  
            CALL aleat_unif(moisson(:),methode,graine)
	    DO ip=n1,n2
              CALL inverse(inj_distz(:,iesp),2000,oxmax,
     &                     moisson(ip-n1+1),aux)
              CALL aleat_unif(angles(1:1),methode,graine)
	      partic(a_pz, ip, iesp) = SIGN(aux,angles(1)-0.5_8)
	    ENDDO
	  ELSE
! On applique une methode mixte d'inversion/rejet	  
            DO ip=n1,n2
              DO
! Inversion directe du facteur isotrope dans le repere mobile	      
                CALL RANDOM_NUMBER(moisson(1:1))
                call inverse(inj_distz(:,iesp),2000,oxmax,
     &                       moisson(1:1),aux)
                gamma=SQRT(1._8+aux**2) 
	        CALL aleat_unif(angles(1:1),methode,graine)
	        pu = SIGN(aux,angles(1)-0.5_8)
! On echantillonne le facteur (1+vder*pu/gamma)/(1+abs(vder))	      
	        CALL RANDOM_NUMBER(moisson(1:1))
	        IF(moisson(1).LT.(1._8+vder*pu/gamma)
     &                         /(1._8+ABS(vder))) EXIT
              ENDDO
! Transformee de Lorentz inverse	    
	      partic(a_pz, ip, iesp) = gder*(pu+vder*gamma)
	    ENDDO
	  ENDIF	   	
	CASE(4)
! Waterbag anisotrope
!          WRITE(fdbg,*) 'En z : gwaterbag avec vthz, vderz = ',
!     &        vthz(iesp),vderz(iesp)          	
          CALL aleat_unif(moisson(:),methode,graine)  
	  IF(a_pz.NE.0) partic(a_pz, n1:n2, iesp) = 
     &      vderz(iesp)+vthz(iesp)*(2._8*moisson(1:nbp_cell)-1._8) 	  
	END SELECT
      ENDIF		            

! --- On distribue le poids de la maille sur les nbp_cell particules
      partic(a_po, n1:n2, iesp) = poids / nbp_cell

! --- Si derive du plasma, on change les vitesses individuelles
!     (Addition de vitesse uniquement classique pour l'instant)
      IF((der_ok.EQ.1).AND.(a_qx.NE.0.)) THEN
        DO ip=n1,n2
          qx = partic(a_qx, ip, iesp)
          IF((qx.LT.der_x(1)).OR.(qx.GT.der_x(2))) CYCLE
          qx = (qx-der_x(1))/(der_x(2)-der_x(1))
          IF(a_px.NE.0) THEN
            v_der = der_vx(1) + (der_vx(2)-der_vx(1))*qx
            partic(a_px, ip, iesp) = partic(a_px, ip, iesp) + v_der
          ENDIF
          IF(a_py.NE.0) THEN
            v_der = der_vy(1) + (der_vy(2)-der_vy(1))*qx
            partic(a_py, ip, iesp) = partic(a_py, ip, iesp) + v_der
          ENDIF
          IF(a_pz.NE.0) THEN
            v_der = der_vz(1) + (der_vz(2)-der_vz(1))*qx
            partic(a_pz, ip, iesp) = partic(a_pz, ip, iesp) + v_der
          ENDIF
        ENDDO
      ENDIF


! --- Pour chaque particule, on calcule l'energie (cinetique ou totale)
      partic(a_ga, n1:n2, iesp) = 0._8
      IF(a_px.NE.0) partic(a_ga, n1:n2,iesp) 
     &            = partic(a_ga, n1:n2,iesp)+partic(a_px, n1:n2,iesp)**2
      IF(a_py.NE.0) partic(a_ga, n1:n2,iesp) 
     &            = partic(a_ga, n1:n2,iesp)+partic(a_py, n1:n2,iesp)**2
      IF(a_pz.NE.0) partic(a_ga, n1:n2,iesp) 
     &            = partic(a_ga, n1:n2,iesp)+partic(a_pz, n1:n2,iesp)**2
      IF(dynam(iesp).EQ.2) THEN
        partic(a_ga, n1:n2, iesp) = SQRT(1._8+partic(a_ga, n1:n2, iesp))
      ELSE
        partic(a_ga, n1:n2, iesp) = 0.5*partic(a_ga, n1:n2, iesp)
      ENDIF


! --- Si meth_inj=2, on place iesp aux positions de l'espece 1
      IF(methode.EQ.2) THEN
        IF(a_qx.NE.0) partic(a_qx, n1:n2, iesp) = partic(a_qx, n1:n2, 1)
        IF(a_qy.NE.0) partic(a_qy, n1:n2, iesp) = partic(a_qy, n1:n2, 1)
        IF(a_qz.NE.0) partic(a_qz, n1:n2, iesp) = partic(a_qz, n1:n2, 1)
      ENDIF
      
! --- Si meth_inj=6, on place iesp aux positions de l'espece iesp-1      
      IF((methode.EQ.6).and.(iesp.GE.2)) THEN
        IF(a_qx.NE.0) partic(a_qx,n1:n2,iesp)=partic(a_qx,n1:n2,iesp-1)
        IF(a_qy.NE.0) partic(a_qy,n1:n2,iesp)=partic(a_qy,n1:n2,iesp-1)
        IF(a_qz.NE.0) partic(a_qz,n1:n2,iesp)=partic(a_qz,n1:n2,iesp-1)
      ENDIF      

! --- Si diagnostic de particules sondees, on initialise l'ancienne
!     position des particules a leur position initiale
      IF(a_qxm1.NE.0) partic(a_qxm1,n1:n2,iesp)=partic(a_qx,n1:n2,iesp)
      IF(a_qym1.NE.0) partic(a_qym1,n1:n2,iesp)=partic(a_qy,n1:n2,iesp)
      IF(a_qzm1.NE.0) partic(a_qzm1,n1:n2,iesp)=partic(a_qz,n1:n2,iesp)
! --- Si diagnostic de particules-test, on initialise leur numero a 0
      IF(a_pt.NE.0) partic(a_pt,n1:n2,iesp)=0.


! --- On incremente enfin le nombre de mailles et de particules
      IF(poids.GT.0.) THEN
        nbm = nbm + 1
        nbp = nbp + nbp_cell
      ENDIF

      RETURN
!     __________________________
      END SUBROUTINE cree_cell_p



      SUBROUTINE calcul_poids(ix,iy,iz,iesp,qx,qy,qz,poids_rel)
! ======================================================================
! Localisation du point (ix,iy,iz) dans le plasma et calcul du poids
! relatif associe (lie au profil, independamment de la densite abolue)
! -- Commun aux initialisations fluide et particulaires
! -- E.L. septembre 2003 / avril 2004
! ======================================================================
 
      USE domaines
      USE domaines_klder
      USE particules_klder
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)         :: ix, iy, iz, iesp
      REAL(KIND=kr), INTENT(OUT)  :: qx, qy, qz, poids_rel

      REAL(KIND=8)    :: log2
      REAL(KIND=kr)   :: boutmx, homomx, homopx, boutpx,
     &                   boutmy, homomy, homopy, boutpy,
     &                   boutmz, homomz, homopz, boutpz
      REAL(KIND=kr)   :: poids_x, poids_y, poids_z
      REAL(KIND=kr)   :: val, aux
      REAL(KIND=kr)   :: angle, epaisx, epaisx2, epaisy, dqx
      INTEGER         :: methode, iprof
!      LOGICAL         :: m_c

      INTERFACE
        FUNCTION prof_dens(profn, izo, pos, long, dm, dp)
          USE precisions
          CHARACTER(LEN=5), INTENT(IN)  :: profn
          INTEGER         , INTENT(IN)  :: izo
          REAL(KIND=kr)   , INTENT(IN)  :: pos, long, dm, dp
          REAL(KIND=kr)                 :: prof_dens
        END FUNCTION prof_dens
      END INTERFACE


      log2 = LOG(2._8)
      methode = meth_inj(iesp)
      IF(methode.EQ.0) RETURN

! Coordonnees de la maille (ix,iy,iz)
! ----------------------------------------------------------------------
      qx = 0.  ;  qy = 0.  ;  qz = 0.
      IF(a_qx.NE.0) qx=(ix-0.5)*pasdx+xmin_gl
      IF(a_qy.NE.0) qy=(iy-0.5)*pasdy+ymin_gl
      IF(a_qz.NE.0) qz=(iz-0.5)*pasdz+zmin_gl
      
      poids_rel = 0._8
      
      DO iprof=1,nprof
 
! Rotation ?
! ----------------------------------------------------------------------
! --- parametres de rotation : centre et angle
!      xrot=53.
!      yrot=0.
!      arot=-45./90*ASIN(1.)
!      arot=0.
! --- rotation
!      qyp=qy
!      qxp=qx
!      qx =xrot+COS(arot)*(qxp-xrot)-SIN(arot)*(qyp-yrot)
!      qy =yrot+SIN(arot)*(qxp-xrot)+COS(arot)*(qyp-yrot)

! Dimensions du plasma
! ----------------------------------------------------------------------
      homomx = centrex(iprof) - 0.5_8*largx(iprof)
      boutmx = homomx - lgrmx(iprof)
      homopx = centrex(iprof) + 0.5_8*largx(iprof)
      boutpx = homopx + lgrpx(iprof)
      homomy = centrey(iprof) - 0.5_8*largy(iprof)
      boutmy = homomy - lgrmy(iprof)
      homopy = centrey(iprof) + 0.5_8*largy(iprof)
      boutpy = homopy + lgrpy(iprof)
      homomz = centrez(iprof) - 0.5_8*largz(iprof)
      boutmz = homomz - lgrmz(iprof)
      homopz = centrez(iprof) + 0.5_8*largz(iprof)
      boutpz = homopz + lgrpz(iprof)

      boutmx = MIN(boutmx, xmax_gl-nb_vidx*pasdx)
      homomx = MIN(homomx, xmax_gl-nb_vidx*pasdx)
      homopx = MIN(homopx, xmax_gl-nb_vidx*pasdx)
      boutpx = MIN(boutpx, xmax_gl-nb_vidx*pasdx)
      boutmx = MAX(boutmx, xmin_gl+nb_vidx*pasdx)
      homomx = MAX(homomx, xmin_gl+nb_vidx*pasdx)
      homopx = MAX(homopx, xmin_gl+nb_vidx*pasdx)
      boutpx = MAX(boutpx, xmin_gl+nb_vidx*pasdx)
      
      boutmy = MIN(boutmy, ymax_gl-nb_vidy*pasdy)
      homomy = MIN(homomy, ymax_gl-nb_vidy*pasdy)
      homopy = MIN(homopy, ymax_gl-nb_vidy*pasdy)
      boutpy = MIN(boutpy, ymax_gl-nb_vidy*pasdy)
      boutmy = MAX(boutmy, ymin_gl+nb_vidy*pasdy)
      homomy = MAX(homomy, ymin_gl+nb_vidy*pasdy)
      homopy = MAX(homopy, ymin_gl+nb_vidy*pasdy)
      boutpy = MAX(boutpy, ymin_gl+nb_vidy*pasdy)
      
      boutmz = MIN(boutmz, zmax_gl-nb_vidz*pasdz)
      homomz = MIN(homomz, zmax_gl-nb_vidz*pasdz)
      homopz = MIN(homopz, zmax_gl-nb_vidz*pasdz)
      boutpz = MIN(boutpz, zmax_gl-nb_vidz*pasdz)
      boutmz = MAX(boutmz, zmin_gl+nb_vidz*pasdz)
      homomz = MAX(homomz, zmin_gl+nb_vidz*pasdz)
      homopz = MAX(homopz, zmin_gl+nb_vidz*pasdz)
      boutpz = MAX(boutpz, zmin_gl+nb_vidz*pasdz)

! Maille (qx,qy,qz) ?
! ----------------------------------------------------------------------
      IF(a_qz.NE.0) THEN
        IF(qz.GT.boutpz) THEN
          val = 0._8          
        ELSEIF(qz.GT.homopz) THEN
          val = prof_dens(z_profn(iprof), 3, qz-homopz, lgrpz(iprof),
     &                    1._8, nrpz(iprof))
        ELSEIF(qz.GE.homomz) THEN
          val = prof_dens(z_profn(iprof), 2, qz-homomz, largz(iprof),
     &                    nrmz(iprof), nrpz(iprof))
        ELSEIF(qz.GE.boutmz) THEN
          val = prof_dens(z_profn(iprof),1, qz-boutmz, lgrmz(iprof),
     &                    nrmz(iprof), 1._8)
        ELSE
          val = 0._8
        ENDIF
        poids_z = val
      ELSE
        poids_z = 1.
      ENDIF

      IF((a_qy.NE.0).AND.(y_profn(iprof)(1:4).NE.'cone')) THEN
        IF(qy.GT.boutpy) THEN
          val = 0._8
        ELSEIF(qy.GT.homopy) THEN
          val = prof_dens(y_profn(iprof), 3, qy-homopy, lgrpy(iprof),
     &                    1._8, nrpy(iprof))
        ELSEIF(qy.GE.homomy) THEN
          val = prof_dens(y_profn(iprof), 2, qy-homomy, largy(iprof),
     &                    nrmy(iprof), nrpy(iprof))
        ELSEIF(qy.GE.boutmy) THEN
          val = prof_dens(y_profn(iprof), 1, qy-boutmy, lgrmy(iprof),
     &                    nrmy(iprof), 1._8)
        ELSE
          val = 0._8
        ENDIF
        poids_y = poids_z*val
      ELSE
        poids_y = poids_z
      ENDIF

!      WRITE(fdbg,"('poids_y : ', (F8.2,1X))") poids_y
!      WRITE(fdbg,"('poids_z : ', (F8.2,1X))") poids_z
!      WRITE(fdbg,"('limmx(iesp) : ', (F8.2,1X))") limmx(iesp)
!      WRITE(fdbg,"('limpx(iesp) : ', (F8.2,1X))") limpx(iesp)
      
!      WRITE(fdbg,"('a_qx : ', (I6,1X))") a_qx
      IF(a_qx.NE.0) THEN
        IF((qx.GE.limmx(iesp)).AND.(qx.LE.limpx(iesp))) THEN

          IF(y_profn(iprof)(1:5).EQ.'cones') THEN
! ------ profil en cone
! ---------------------
! Ancienne version : cone construit par translation (=> epaisseur variable)
!          val = 0._8
!          IF(ABS(qy-centrey).GT.0.5*lgrmy) THEN
!! --------- hors du cone
!            val = 0._8
!          ELSE
!            IF(ABS(qy-centrey).LE.0.5*lgrpy) THEN
!! --------- dans la pointe
!              CONTINUE
!            ELSE
!! --------- dans le cone : transformation des coordonnees
!              dqx=qx+largy*2.*(ABS(qy-centrey)-0.5*lgrpy)/(lgrmy-lgrpy)
!            ENDIF
!! --------- profil correspondant selon x
!            IF(dqx.GE.boutpx) THEN
!              val = 0._8
!            ELSEIF(dqx.GE.homopx) THEN
!              val = prof_dens(x_profn, 3, dqx-homopx, lgrpx, 1._8, nrpx)
!            ELSEIF(dqx.GE.homomx) THEN
!              val = prof_dens(x_profn,2,dqx-homomx, largx, nrmx, nrpx)
!            ELSEIF(dqx.GE.boutmx) THEN
!              val = prof_dens(x_profn,1,dqx-boutmx, lgrmx, nrmx, 1._8)
!            ELSE
!              val = 0._8
!            ENDIF
!          ENDIF

! Nouvelle version : cone d'epaisseur constante
! L. G., janvier 2006
          val = 0._8
	  angle = atan(0.5*(lgrmy(iprof)-lgrpy(iprof))/largy(iprof))
	  epaisx = largx(iprof)/sin(angle)
	  epaisy=largx(iprof)/cos(angle)	  
          IF  ((ABS(qy-centrey(iprof)).GT.0.5*lgrmy(iprof)
     &             + epaisy+lgrpx(iprof)*tan(angle))
     &      .OR.(qx.LT.homomx-largy(iprof))) THEN
! --------- hors du cone
            val = 0._8
          ELSE
            IF(ABS(qy-centrey(iprof)).LE.0.5*lgrpy(iprof)) THEN
! --------- dans la pointe
! --------- profil selon x
              IF(qx.GE.boutpx) THEN
	        val = 0._8
              ELSEIF(qx.GE.homopx) THEN
	        val = prof_dens(x_profn(iprof),3,qx-homopx,lgrpx(iprof),
     &                          1._8,nrpx(iprof))
	      ELSEIF(qx.GE.homomx) THEN
                val = prof_dens(x_profn(iprof),2,qx-homomx,largx(iprof),
     &                          nrmx(iprof),nrpx(iprof))
              ELSEIF(qx.GE.boutmx) THEN
                val = prof_dens(x_profn(iprof),1,qx-boutmx,lgrmx(iprof),
     &                          nrmx(iprof),1._8)
              ELSE
                val = 0._8
	      ENDIF	      
            ELSEIF(ABS(qy-centrey(iprof)).LE.0.5*lgrpy(iprof) + 
     &             largx(iprof)*(1._8-sin(angle))/cos(angle)) THEN
              IF(qx.GE.boutpx) THEN
	        val = 0._8
	      ELSEIF(qx.GE.homopx) THEN
	        val = prof_dens(x_profn(iprof),3,qx-homopx,
     &                        lgrpx(iprof), 1._8,nrpx(iprof))
	      ELSE    		
! --------- dans le cone : transformation des coordonnees
                dqx = qx+2._8*largy(iprof)*(ABS(qy-centrey(iprof))
     &              - 0.5*lgrpy(iprof))/(lgrmy(iprof)-lgrpy(iprof))
		epaisx2 = largx(iprof)+(ABS(qy-centrey(iprof))
     &                  - 0.5*lgrpy(iprof))/tan(angle)
	        IF(dqx.GE.homomx) THEN
		  val = prof_dens(x_profn(iprof),2,dqx-homomx,
     &                        epaisx2, nrmx(iprof),nrpx(iprof))
		ELSEIF(dqx.GE.boutmx) THEN
		  val = prof_dens(x_profn(iprof),1,dqx-boutmx,
     &                           lgrmx(iprof), nrmx(iprof),1._8)
		ELSE
		  val = 0._8
		ENDIF
	      ENDIF
	    ELSE
! --------- dans le cone : transformation des coordonnees
              dqx = qx+largy(iprof)*2.*(ABS(qy-centrey(iprof))
     &            - 0.5*lgrpy(iprof)) / (lgrmy(iprof)-lgrpy(iprof))
              IF(dqx.GE.homomx+epaisx+lgrpx(iprof)) THEN
	        val = 0._8
              ELSEIF(dqx.GE.homomx+epaisx) THEN
	        val = prof_dens(x_profn(iprof),3,dqx-homomx-epaisx,
     &                          lgrpx(iprof),1._8,nrpx(iprof))
	      ELSEIF(dqx.GE.homomx) THEN
                val = prof_dens(x_profn(iprof),2,dqx-homomx,
     &                          epaisx,nrmx(iprof),nrpx(iprof))
              ELSEIF(dqx.GE.boutmx) THEN
                val = prof_dens(x_profn(iprof),1,dqx-boutmx,
     &                          lgrmx(iprof),nrmx(iprof),1._8)
              ELSE
                val = 0._8
	      ENDIF	      
	    ENDIF
	  ENDIF	
	  
          ELSEIF(x_profn(iprof)(1:4).EQ.'coqu') THEN
! ------ profil en coquille
! -------------------------
          val = 0._8
          IF(ABS(qy-centrey(iprof)).GT.lgrmx(iprof)
     &                    +0.5*largx(iprof)) THEN
! --------- hors de la coquille, sur le raccord plan
            IF((qx.GE.homomx).AND.(qx.LE.homopx)) val = 1._8
          ELSE
! --------- dans la zone de la coquille
            aux = SQRT((qx-centrex(iprof))**2+(qy-centrey(iprof))**2)
            IF((x_profn(iprof)(5:5).EQ.'2')) THEN
! --------- si 'coqu2' : coquille complete
              IF(ABS(aux-lgrmx(iprof)).LE.0.5*largx(iprof)) val = 1._8
            ELSE
! --------- par defaut demi-coquille
              IF((ABS(aux-lgrmx(iprof)).LE.0.5*largx(iprof))
     &          .AND.(qx.LE.homopx))
     &        val = 1._8
            ENDIF
          ENDIF

          ELSEIF(    (x_profn(iprof)(1:5).EQ.'sphex')
     &           .OR.(x_profn(iprof)(1:5).EQ.'sphpr')) THEN
! --- profil spherique
! --------------------
          IF(qx.GT.boutpx) THEN
            val = 0._8
          ELSEIF(qx.GT.homopx) THEN
            val = prof_dens(x_profn(iprof),3,qx-homopx,
     &                   lgrpx(iprof),1._8,nrpx(iprof))
          ELSEIF(qx.GE.homomx) THEN
            val = prof_dens(x_profn(iprof),2,qx-homomx,
     &                 largx(iprof),nrmx(iprof),nrpx(iprof))
          ELSEIF(qx.GE.boutmx) THEN
            aux = SQRT((qx-homomx)**2+(qy-centrey(iprof))**2)
            IF(aux.GT.lgrmx(iprof)) THEN
              val = 0.
            ELSE
              val = prof_dens(x_profn(iprof),1,aux,
     &                  lgrmx(iprof),nrmx(iprof),nrpx(iprof))
            ENDIF
          ELSE
            val = 0._8
          ENDIF

          ELSE
! ------ profil cartesien ou autre
! --------------------------------
!          WRITE(fdbg,"('profil cartesien ou autre')") 

!         WRITE(fdbg,"('qx : ', (F8.2,1X))") qx
!         WRITE(fdbg,"('boutpx : ', (F8.2,1X))") boutpx
!         WRITE(fdbg,"('homomx : ', (F8.2,1X))") homomx
!         WRITE(fdbg,"('homopx : ', (F8.2,1X))") homopx	  

          IF(qx.GT.boutpx) THEN
            val = 0._8
          ELSEIF(qx.GT.homopx) THEN
            val = prof_dens(x_profn(iprof),3,qx-homopx,lgrpx(iprof),
     &                      1._8,nrpx(iprof))
          ELSEIF(qx.GE.homomx) THEN
            val = prof_dens(x_profn(iprof),2,qx-homomx,largx(iprof),
     &                      nrmx(iprof),nrpx(iprof))
          ELSEIF(qx.GE.boutmx) THEN
            val = prof_dens(x_profn(iprof),1,qx-boutmx,lgrmx(iprof),
     &                      nrmx(iprof),1._8)
          ELSE
            val = 0._8
          ENDIF

          ENDIF

        ELSE
          val = 0._8
        ENDIF
        poids_x = poids_y*val
      ELSE
        poids_x = poids_y
      ENDIF


      poids_rel = MAX(poids_rel,poids_x)
      
      ENDDO

      RETURN
!     ___________________________
      END SUBROUTINE calcul_poids



      SUBROUTINE calcul_theta(ix,iy,iz)
! ======================================================================
! Localisation du point (ix,iy,iz) dans le plasma et calcul du poids
! relatif associe (lie au profil, independamment de la densite abolue)
! -- Commun aux initialisations fluide et particulaires
! -- E.L. septembre 2003 / avril 2004
! ======================================================================
 
      USE domaines
      USE domaines_klder
      USE particules_klder
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)         :: ix, iy, iz 
      REAL(KIND=kr)    :: qx, qy, qz 

      REAL(KIND=8)    :: log2
      REAL(KIND=kr)   :: boutmx, homomx, homopx, boutpx,
     &                   boutmy, homomy, homopy, boutpy,
     &                   boutmz, homomz, homopz, boutpz
      REAL(KIND=kr)   :: poids_x, poids_y, poids_z
      REAL(KIND=kr)   :: val, aux
      REAL(KIND=kr)   :: angle, epaisx, epaisx2, epaisy, dqx
      INTEGER         :: methode, iprof
!      LOGICAL         :: m_c

      INTERFACE
        FUNCTION prof_dens(profn, izo, pos, long, dm, dp)
          USE precisions
          CHARACTER(LEN=5), INTENT(IN)  :: profn
          INTEGER         , INTENT(IN)  :: izo
          REAL(KIND=kr)   , INTENT(IN)  :: pos, long, dm, dp
          REAL(KIND=kr)                 :: prof_dens
        END FUNCTION prof_dens
      END INTERFACE


      log2 = LOG(2._8)


! Coordonnees de la maille (ix,iy,iz)
! ----------------------------------------------------------------------
      qx = 0.  ;  qy = 0.  ;  qz = 0.
      IF(a_qx.NE.0) qx=(ix-0.5)*pasdx+xmin_gl
      IF(a_qy.NE.0) qy=(iy-0.5)*pasdy+ymin_gl
      IF(a_qz.NE.0) qz=(iz-0.5)*pasdz+zmin_gl
      
      thetad_2(ix, iy) = 0._8
      
      iprof=1
 
! Rotation ?
! ----------------------------------------------------------------------
! --- parametres de rotation : centre et angle
!      xrot=53.
!      yrot=0.
!      arot=-45./90*ASIN(1.)
!      arot=0.
! --- rotation
!      qyp=qy
!      qxp=qx
!      qx =xrot+COS(arot)*(qxp-xrot)-SIN(arot)*(qyp-yrot)
!      qy =yrot+SIN(arot)*(qxp-xrot)+COS(arot)*(qyp-yrot)

! Dimensions du plasma
! ----------------------------------------------------------------------
      homomx = tta_centrex(iprof) - 0.5_8*tta_largx(iprof)
      boutmx = homomx - tta_lgrmx(iprof)
      homopx = tta_centrex(iprof) + 0.5_8*tta_largx(iprof)
      boutpx = homopx + tta_lgrpx(iprof)
      homomy = tta_centrey(iprof) - 0.5_8*tta_largy(iprof)
      boutmy = homomy - tta_lgrmy(iprof)
      homopy = tta_centrey(iprof) + 0.5_8*tta_largy(iprof)
      boutpy = homopy + tta_lgrpy(iprof)
      homomz = tta_centrez(iprof) - 0.5_8*tta_largz(iprof)
      boutmz = homomz - tta_lgrmz(iprof)
      homopz = tta_centrez(iprof) + 0.5_8*tta_largz(iprof)
      boutpz = homopz + tta_lgrpz(iprof)

      boutmx = MIN(boutmx, xmax_gl-nb_vidx*pasdx)
      homomx = MIN(homomx, xmax_gl-nb_vidx*pasdx)
      homopx = MIN(homopx, xmax_gl-nb_vidx*pasdx)
      boutpx = MIN(boutpx, xmax_gl-nb_vidx*pasdx)
      boutmx = MAX(boutmx, xmin_gl+nb_vidx*pasdx)
      homomx = MAX(homomx, xmin_gl+nb_vidx*pasdx)
      homopx = MAX(homopx, xmin_gl+nb_vidx*pasdx)
      boutpx = MAX(boutpx, xmin_gl+nb_vidx*pasdx)
      
      boutmy = MIN(boutmy, ymax_gl-nb_vidy*pasdy)
      homomy = MIN(homomy, ymax_gl-nb_vidy*pasdy)
      homopy = MIN(homopy, ymax_gl-nb_vidy*pasdy)
      boutpy = MIN(boutpy, ymax_gl-nb_vidy*pasdy)
      boutmy = MAX(boutmy, ymin_gl+nb_vidy*pasdy)
      homomy = MAX(homomy, ymin_gl+nb_vidy*pasdy)
      homopy = MAX(homopy, ymin_gl+nb_vidy*pasdy)
      boutpy = MAX(boutpy, ymin_gl+nb_vidy*pasdy)
      
      boutmz = MIN(boutmz, zmax_gl-nb_vidz*pasdz)
      homomz = MIN(homomz, zmax_gl-nb_vidz*pasdz)
      homopz = MIN(homopz, zmax_gl-nb_vidz*pasdz)
      boutpz = MIN(boutpz, zmax_gl-nb_vidz*pasdz)
      boutmz = MAX(boutmz, zmin_gl+nb_vidz*pasdz)
      homomz = MAX(homomz, zmin_gl+nb_vidz*pasdz)
      homopz = MAX(homopz, zmin_gl+nb_vidz*pasdz)
      boutpz = MAX(boutpz, zmin_gl+nb_vidz*pasdz)
      
      IF(ix.EQ.1 .AND. iy.EQ.1) THEN
        WRITE(fmsgo,*) 'Subroutine calcul_theta ' 
        WRITE(fdbg,*) 'ymin_gl = ', ymin_gl
        WRITE(fdbg,*) 'ymax_gl = ', ymax_gl

        WRITE(fdbg,*) 'boutmy = ', boutmy
        WRITE(fdbg,*) 'boutpy = ', boutpy
        WRITE(fdbg,*) 'homomy = ', homomy
        WRITE(fdbg,*) 'homopy = ', homopy
      ENDIF

     

! Maille (qx,qy,qz) ?
! ----------------------------------------------------------------------
      IF(a_qz.NE.0) THEN
        IF(qz.GT.boutpz) THEN
          val = 0._8          
        ELSEIF(qz.GT.homopz) THEN
          val = prof_dens(tta_z_profn(iprof), 3, qz-homopz, 
     &        tta_lgrpz(iprof), 1._8, tta_nrpz(iprof))
        ELSEIF(qz.GE.homomz) THEN
          val = prof_dens(tta_z_profn(iprof), 2, qz-homomz, 
     &        tta_largz(iprof), tta_nrmz(iprof), tta_nrpz(iprof))
        ELSEIF(qz.GE.boutmz) THEN
          val = prof_dens(tta_z_profn(iprof),1, qz-boutmz, 
     &        tta_lgrmz(iprof), tta_nrmz(iprof), 1._8)
        ELSE
          val = 0._8
        ENDIF
        poids_z = val
      ELSE
        poids_z = 1.
      ENDIF

      IF((a_qy.NE.0).AND.(tta_y_profn(iprof)(1:4).NE.'cone')) THEN
        IF(qy.GT.boutpy) THEN
          val = 0._8
        ELSEIF(qy.GT.homopy) THEN
          val = prof_dens(tta_y_profn(iprof), 3, qy-homopy, 
     &       tta_lgrpy(iprof), 1._8, tta_nrpy(iprof))
        ELSEIF(qy.GE.homomy) THEN
          val = prof_dens(tta_y_profn(iprof), 2, qy-homomy, 
     &       tta_largy(iprof), tta_nrmy(iprof), tta_nrpy(iprof))
        ELSEIF(qy.GE.boutmy) THEN
          val = prof_dens(tta_y_profn(iprof), 1, qy-boutmy, 
     &       tta_lgrmy(iprof), tta_nrmy(iprof), 1._8)
        ELSE
          val = 0._8
        ENDIF
        poids_y = poids_z*val
      ELSE
        poids_y = poids_z
      ENDIF

!      WRITE(fdbg,"('poids_y : ', (F8.2,1X))") poids_y
!      WRITE(fdbg,"('poids_z : ', (F8.2,1X))") poids_z
!      WRITE(fdbg,"('limmx(iesp) : ', (F8.2,1X))") limmx(iesp)
!      WRITE(fdbg,"('limpx(iesp) : ', (F8.2,1X))") limpx(iesp)
      
!      WRITE(fdbg,"('a_qx : ', (I6,1X))") a_qx
      IF(a_qx.NE.0) THEN

          IF(tta_y_profn(iprof)(1:5).EQ.'cones') THEN
! ------ profil en cone
! ---------------------
! Ancienne version : cone construit par translation (=> epaisseur variable)
!          val = 0._8
!          IF(ABS(qy-centrey).GT.0.5*lgrmy) THEN
!! --------- hors du cone
!            val = 0._8
!          ELSE
!            IF(ABS(qy-centrey).LE.0.5*lgrpy) THEN
!! --------- dans la pointe
!              CONTINUE
!            ELSE
!! --------- dans le cone : transformation des coordonnees
!              dqx=qx+largy*2.*(ABS(qy-centrey)-0.5*lgrpy)/(lgrmy-lgrpy)
!            ENDIF
!! --------- profil correspondant selon x
!            IF(dqx.GE.boutpx) THEN
!              val = 0._8
!            ELSEIF(dqx.GE.homopx) THEN
!              val = prof_dens(x_profn, 3, dqx-homopx, lgrpx, 1._8, nrpx)
!            ELSEIF(dqx.GE.homomx) THEN
!              val = prof_dens(x_profn,2,dqx-homomx, largx, nrmx, nrpx)
!            ELSEIF(dqx.GE.boutmx) THEN
!              val = prof_dens(x_profn,1,dqx-boutmx, lgrmx, nrmx, 1._8)
!            ELSE
!              val = 0._8
!            ENDIF
!          ENDIF

! Nouvelle version : cone d'epaisseur constante
! L. G., janvier 2006
          val = 0._8
	  angle = atan(0.5*(tta_lgrmy(iprof)
     &              -tta_lgrpy(iprof))/tta_largy(iprof))
	  epaisx = tta_largx(iprof)/sin(angle)
	  epaisy=tta_largx(iprof)/cos(angle)	  
          IF  ((ABS(qy-tta_centrey(iprof)).GT.0.5*tta_lgrmy(iprof)
     &             + epaisy+tta_lgrpx(iprof)*tan(angle))
     &      .OR.(qx.LT.homomx-tta_largy(iprof))) THEN
! --------- hors du cone
            val = 0._8
          ELSE
            IF(ABS(qy-tta_centrey(iprof)).LE.0.5*tta_lgrpy(iprof)) THEN
! --------- dans la pointe
! --------- profil selon x
              IF(qx.GE.boutpx) THEN
	        val = 0._8
              ELSEIF(qx.GE.homopx) THEN
	        val = prof_dens(tta_x_profn(iprof),3,qx-homopx,
     &             tta_lgrpx(iprof), 1._8,tta_nrpx(iprof))
	      ELSEIF(qx.GE.homomx) THEN
                val = prof_dens(tta_x_profn(iprof),2,qx-homomx, 
     &             tta_largx(iprof), tta_nrmx(iprof),tta_nrpx(iprof))
              ELSEIF(qx.GE.boutmx) THEN
                val = prof_dens(tta_x_profn(iprof),1,qx-boutmx,
     &            tta_lgrmx(iprof), tta_nrmx(iprof),1._8)
              ELSE
                val = 0._8
	      ENDIF	      
            ELSEIF(ABS(qy-tta_centrey(iprof)).LE.0.5*tta_lgrpy(iprof) + 
     &             tta_largx(iprof)*(1._8-sin(angle))/cos(angle)) THEN
              IF(qx.GE.boutpx) THEN
	        val = 0._8
	      ELSEIF(qx.GE.homopx) THEN
	        val = prof_dens(tta_x_profn(iprof),3,qx-homopx,
     &                        tta_lgrpx(iprof), 1._8,tta_nrpx(iprof))
	      ELSE    		
! --------- dans le cone : transformation des coordonnees
                dqx = 
     &           qx+2._8*tta_largy(iprof)*(ABS(qy-tta_centrey(iprof))
     &           - 0.5*tta_lgrpy(iprof))
     &           /(tta_lgrmy(iprof)-tta_lgrpy(iprof))
		epaisx2 = tta_largx(iprof)+(ABS(qy-tta_centrey(iprof))
     &                  - 0.5*tta_lgrpy(iprof))/tan(angle)
	        IF(dqx.GE.homomx) THEN
		  val = prof_dens(tta_x_profn(iprof),2,dqx-homomx,
     &                       epaisx2, tta_nrmx(iprof),tta_nrpx(iprof))
		ELSEIF(dqx.GE.boutmx) THEN
		  val = prof_dens(tta_x_profn(iprof),1,dqx-boutmx,
     &                         tta_lgrmx(iprof), tta_nrmx(iprof),1._8)
		ELSE
		  val = 0._8
		ENDIF
	      ENDIF
	    ELSE
! --------- dans le cone : transformation des coordonnees
              dqx = qx+tta_largy(iprof)*2.*(ABS(qy-tta_centrey(iprof))
     &            - 0.5*tta_lgrpy(iprof)) 
     &            / (tta_lgrmy(iprof)-tta_lgrpy(iprof))
              IF(dqx.GE.homomx+epaisx+tta_lgrpx(iprof)) THEN
	        val = 0._8
              ELSEIF(dqx.GE.homomx+epaisx) THEN
	        val = prof_dens(tta_x_profn(iprof),3,dqx-homomx-epaisx,
     &                          tta_lgrpx(iprof),1._8,tta_nrpx(iprof))
	      ELSEIF(dqx.GE.homomx) THEN
                val = prof_dens(tta_x_profn(iprof),2,dqx-homomx,
     &                          epaisx,tta_nrmx(iprof),tta_nrpx(iprof))
              ELSEIF(dqx.GE.boutmx) THEN
                val = prof_dens(tta_x_profn(iprof),1,dqx-boutmx,
     &                          tta_lgrmx(iprof),tta_nrmx(iprof),1._8)
              ELSE
                val = 0._8
	      ENDIF	      
	    ENDIF
	  ENDIF	
! ------ fin nouvelle version LG	  
	  
	  
          ELSEIF(tta_x_profn(iprof)(1:4).EQ.'coqu') THEN
! ------ profil en coquille
! -------------------------
          val = 0._8
          IF(ABS(qy-tta_centrey(iprof)).GT.tta_lgrmx(iprof)
     &                    +0.5*tta_largx(iprof)) THEN
! --------- hors de la coquille, sur le raccord plan
            IF((qx.GE.homomx).AND.(qx.LE.homopx)) val = 1._8
          ELSE
! --------- dans la zone de la coquille
            aux = SQRT((qx-tta_centrex(iprof))**2
     &           +(qy-tta_centrey(iprof))**2)
            IF((tta_x_profn(iprof)(5:5).EQ.'2')) THEN
! --------- si 'coqu2' : coquille complete
              IF(ABS(aux-tta_lgrmx(iprof)).LE.0.5*tta_largx(iprof)) 
     &                       val = 1._8
            ELSE
! --------- par defaut demi-coquille
              IF((ABS(aux-tta_lgrmx(iprof)).LE.0.5*tta_largx(iprof))
     &          .AND.(qx.LE.homopx))
     &        val = 1._8
            ENDIF
          ENDIF

          ELSEIF(    (tta_x_profn(iprof)(1:5).EQ.'sphex')
     &           .OR.(tta_x_profn(iprof)(1:5).EQ.'sphpr')) THEN
! --- profil spherique
! --------------------
          IF(qx.GT.boutpx) THEN
            val = 0._8
          ELSEIF(qx.GT.homopx) THEN
            val = prof_dens(tta_x_profn(iprof),3,qx-homopx,
     &                   tta_lgrpx(iprof),1._8,tta_nrpx(iprof))
          ELSEIF(qx.GE.homomx) THEN
            val = prof_dens(tta_x_profn(iprof),2,qx-homomx,
     &                 tta_largx(iprof),tta_nrmx(iprof),tta_nrpx(iprof))
          ELSEIF(qx.GE.boutmx) THEN
            aux = SQRT((qx-homomx)**2+(qy-tta_centrey(iprof))**2)
            IF(aux.GT.tta_lgrmx(iprof)) THEN
              val = 0.
            ELSE
              val = prof_dens(tta_x_profn(iprof),1,aux,
     &               tta_lgrmx(iprof),tta_nrmx(iprof),tta_nrpx(iprof))
            ENDIF
          ELSE
            val = 0._8
          ENDIF

          ELSE
! ------ profil cartesien ou autre
! --------------------------------
!          WRITE(fdbg,"('profil cartesien ou autre')") 

!         WRITE(fdbg,"('qx : ', (F8.2,1X))") qx
!         WRITE(fdbg,"('boutpx : ', (F8.2,1X))") boutpx
!         WRITE(fdbg,"('homomx : ', (F8.2,1X))") homomx
!         WRITE(fdbg,"('homopx : ', (F8.2,1X))") homopx	  

          IF(qx.GT.boutpx) THEN
            val = 0._8
          ELSEIF(qx.GT.homopx) THEN
            val = prof_dens(tta_x_profn(iprof),3,qx-homopx,
     &        tta_lgrpx(iprof), 1._8,tta_nrpx(iprof))
          ELSEIF(qx.GE.homomx) THEN
            val = prof_dens(tta_x_profn(iprof),2,qx-homomx,
     &    tta_largx(iprof), tta_nrmx(iprof),tta_nrpx(iprof))
          ELSEIF(qx.GE.boutmx) THEN
            val = prof_dens(tta_x_profn(iprof),1,qx-boutmx,
     &    tta_lgrmx(iprof), tta_nrmx(iprof),1._8)
          ELSE
            val = 0._8
          ENDIF

          ENDIF
! ------ fin s�lection du profil	  


        poids_x = poids_y*val
      ELSE
        poids_x = poids_y
      ENDIF


      thetad_2(ix, iy) = 0.5*poids_x*thetad
      
      

      RETURN
!     ___________________________
      END SUBROUTINE calcul_theta



      SUBROUTINE aleat_dml2d(gerbe,methode,graine,vth,ordre)
! ======================================================================
! Tirage d'un tableau de nombres aleatoires repartis selon une
! fonction de Dum-Matte-Langdon (hyper-gaussienne d'ordre 2 a 5)
! (Reference : B. Afeyan)
! Pour ce cas particulier, gerbe est dimensionne a 2*nb de particules
! (on retourne d'un seul coup vx et vy)
! ======================================================================
      USE precisions
      IMPLICIT NONE

      REAL(KIND=kr), DIMENSION(:), INTENT(OUT)   :: gerbe
      INTEGER                    , INTENT(IN)    :: methode, ordre
      INTEGER(KIND=8)            , INTENT(INOUT) :: graine
      REAL(KIND=kr)              , INTENT(IN)    :: vth

      REAL(KIND=kr), DIMENSION(SIZE(gerbe))     :: gerbe_u
      INTEGER, SAVE                             :: ordre_prec = -1
      INTEGER                                   :: i, j, nbp
      REAL(KIND=kr), DIMENSION(0:100), SAVE     :: frep
      REAL(KIND=kr)                             :: deuxpi, vmax, v

      INTERFACE
        FUNCTION suite_equidistribuee(mseed)
          USE precisions
          REAL(KIND=kr)                  :: suite_equidistribuee
          INTEGER(KIND=8), INTENT(INOUT) :: mseed
        END FUNCTION suite_equidistribuee
      END INTERFACE

      deuxpi = 4.*ASIN(1._8)
      vmax = 4.*vth
      IF(ordre_prec.NE.ordre) THEN
! --- construction de la fonction de repartition (1D en vitesse)
        frep(0) = 0.
        DO i=1, 100
          v = i*vmax/100.
          IF(ordre.EQ.2) frep(i)=frep(i-1)+v*EXP(-0.500*(v/vth)**ordre)
          IF(ordre.EQ.4) frep(i)=frep(i-1)+v*EXP(-0.100*(v/vth)**ordre)
          IF(ordre.EQ.5) frep(i)=frep(i-1)+v*EXP(-0.036*(v/vth)**ordre)
        ENDDO
        frep(1:100) = frep(1:100)/frep(100)
        ordre_prec = ordre
      ENDIF

! --- tirages plus ou moins aleatoires
      SELECT CASE(methode)
      CASE(4)
        DO i=1,SIZE(gerbe)
          gerbe_u(i) = suite_equidistribuee(graine)
        ENDDO
      CASE DEFAULT
        CALL RANDOM_NUMBER(gerbe_u)
      END SELECT

! --- construction des vitesses a partir de frep et de gerbe_u
      nbp = SIZE(gerbe)/2
      DO i=1,nbp
        DO j=1,100
          IF(frep(j).GT.gerbe_u(i)) EXIT
        ENDDO
        v = 4./100. * (j-(frep(j)-gerbe_u(i))/(frep(j)-frep(j-1)))
        gerbe(i    ) = v * COS(deuxpi*gerbe_u(i+nbp))
        gerbe(i+nbp) = v * SIN(deuxpi*gerbe_u(i+nbp))
      ENDDO

      RETURN
!     __________________________
      END SUBROUTINE aleat_dml2d



      FUNCTION prof_dens(profn, izo, pos, long, dm, dp)
! ======================================================================
! Calcul des rampes du profil de densite, en valeur relative (max=1),
! a la position "pos" sur une longueur totale "long"
! Le profil est decoupe en 3 zones dont les variations
! spatiales sont fixees, selon le type de profil
!
! 'gauss'          : zone 1 lineaire de 0 a dm
!                    zone 2 gaussien de dm a dp
!                    zone 3 lineaire de dp a 0
!
! 'parab'          : zone 1 quadratique de dm a 1
!                    zone 2 constante a 1
!                    zone 3 quadratique de 1 a dp
!
! 'prepl'          : zone 1 constante a dm
!                    zone 2 constante a 1
!                    zone 3 lineaire de 1 a dp
!
! 'bipla'          : zone 1 constante a dm
!                    zone 2 nulle
!                    zone 3 constante a dp
!
! 'expon'          : zone 1 lineaire de 0 a dm
!                    zone 2 exponentielle de dm a dp
!                    zone 3 lineaire de dp a 0
!
! 'expo2'          : zone 1 lineaire de 0 a dm
!                    zone 2 exponentielle de dm a dp
!                    zone 3 constante a dp
!
! 'expo3'          : expo2 bidouill�
!
! 'sphex'          : zone 1 exponentielle de dm a dp en spherique
!                    zone 2 constante a dp
!                    zone 3 lineaire de dp a 0
!
! 'sphpr'          : zone 1 constante a dm en spherique
!                    zone 2 constante a 1
!                    zone 3 lineaire de 1 a dp
!
! 'cones'          : a commenter...
!
! 'coqu?'          : coquille a densite constante
!                    cas traite sans appel a prof_dens
!
! 'trapz' = DEFAUT : zone 1 lineaire de dm a 1
!                    zone 2 constante a 1
!                    zone 3 lineaire de 1 a dp
! ======================================================================
      USE precisions
      IMPLICIT NONE

      CHARACTER(LEN=5), INTENT(IN)  :: profn
      INTEGER         , INTENT(IN)  :: izo
      REAL(KIND=kr)   , INTENT(IN)  :: pos, long, dm, dp
      REAL(KIND=kr)                 :: prof_dens

      REAL(KIND=kr)                 :: aux, auxpos

      IF(long.EQ.0.) THEN
        aux = dm
      ELSE
        auxpos = pos/long
        SELECT CASE(profn)

        CASE('gauss')
          IF(izo.EQ.1) THEN
            aux = dm*auxpos
          ELSEIF(izo.EQ.2) THEN
            aux = EXP(-(  auxpos    *SQRT(-LOG(dp))
     &                  +(auxpos-1.)*SQRT(-LOG(dm)) )**2)
          ELSEIF(izo.EQ.3) THEN
            aux = dp*(1.-auxpos)
          ENDIF

        CASE('parab')
          IF(izo.EQ.1) THEN
            aux = 1. + (dm-1.)*(1.-auxpos)**2
          ELSEIF(izo.EQ.2) THEN
            aux = 1.
          ELSEIF(izo.EQ.3) THEN
            aux = 1. + (dp-1.)*auxpos**2
          ENDIF

        CASE('prepl')
          IF(izo.EQ.1) THEN
            aux = dm
          ELSEIF(izo.EQ.2) THEN
            aux = 1.
          ELSEIF(izo.EQ.3) THEN
            aux = 1. + (dp-1.)*auxpos
          ENDIF

        CASE('bipla')
          IF(izo.EQ.1) THEN
            aux = dm 
          ELSEIF(izo.EQ.2) THEN
            aux = 0.
          ELSEIF(izo.EQ.3) THEN
            aux = dp
          ENDIF

        CASE('expon')
          IF(izo.EQ.1) THEN
            aux = dm*auxpos
          ELSEIF(izo.EQ.2) THEN
            IF((dm.NE.0.).AND.(dp.NE.0.)) THEN
              aux = dm * EXP(LOG(dp/dm)*auxpos)
            ELSE
              aux = 0.
            ENDIF
          ELSEIF(izo.EQ.3) THEN
            aux = dp*(1.-auxpos)
          ENDIF

        CASE('expo2')
          IF(izo.EQ.1) THEN
            aux = dm*auxpos
          ELSEIF(izo.EQ.2) THEN
            IF((dm.NE.0.).AND.(dp.NE.0.)) THEN
              aux = dm * EXP(LOG(dp/dm)*auxpos)
            ELSE
              aux = 0.
            ENDIF
          ELSEIF(izo.EQ.3) THEN
            aux = dp
          ENDIF	  
	  
        CASE('expo3')
          IF(izo.EQ.1) THEN
	    IF(dm.NE.0.) THEN
              aux = dm * EXP(LOG(1._8/dm)*auxpos)
	    ELSE
	      aux = 0.
	    ENDIF
          ELSEIF(izo.EQ.2) THEN
            aux = 1._8
          ELSEIF(izo.EQ.3) THEN
            IF(dp.NE.0) THEN
              aux = EXP(LOG(dp)*auxpos)
	    ELSE
	      aux = 0.
	    ENDIF
          ENDIF

!        CASE('expo3')
!          IF(izo.EQ.1) THEN
!            aux = dm*auxpos
!          ELSEIF(izo.EQ.2) THEN
!            IF((dm.NE.0.).AND.(dp.NE.0.)) THEN
!              aux = dm * EXP(LOG(dp/dm)*auxpos)
!            ELSE
!              aux = 0.
!            ENDIF
!          ELSEIF(izo.EQ.3) THEN
!            aux = 16.4*dp
!          ENDIF

        CASE('sphex')
          IF(izo.EQ.1) THEN
            IF((dm.NE.0.).AND.(dp.NE.0.)) THEN
              aux = dp * EXP(LOG(dm/dp)*auxpos)
            ELSE
              aux = 0.
            ENDIF
          ELSEIF(izo.EQ.2) THEN
            aux = dp
          ELSEIF(izo.EQ.3) THEN
            aux = dp*(1.-auxpos)
          ENDIF

        CASE('sphpr')
          IF(izo.EQ.1) THEN
            aux = dm
          ELSEIF(izo.EQ.2) THEN
            aux = 1.
          ELSEIF(izo.EQ.3) THEN
            aux = 1. + (dp-1.)*auxpos
          ENDIF

        CASE('cones')
          IF(izo.EQ.1) THEN
            aux = 0
          ELSEIF(izo.EQ.2) THEN
            aux = 1.
          ELSEIF(izo.EQ.3) THEN
            aux = 0.
          ENDIF

        CASE DEFAULT
          IF(izo.EQ.1) THEN
            aux = dm + (1.-dm)*auxpos
          ELSEIF(izo.EQ.2) THEN
            aux = 1.
          ELSEIF(izo.EQ.3) THEN
            aux = 1. + (dp-1.)*auxpos
          ENDIF

        END SELECT
      ENDIF
      prof_dens = aux
      RETURN
!     ______________________
      END FUNCTION prof_dens
      
      
      SUBROUTINE init_poids_front
! ======================================================================
! Calcul des poids aux frontieres (utile si indp = -6)
! Creation ---  04-07 --- L. Gremillet
! ======================================================================

      USE domaines
      USE domaines_klder
      USE particules_klder
      USE erreurs

      IMPLICIT NONE

      REAL(KIND=kr)   :: poids_rel
      REAL(KIND=kr)   :: qx, qy, qz
      INTEGER         :: iesp 
      INTEGER         :: i, j, k, ix, iy, iz, iproc
      
! Calcul des poids aux frontieres pour indp = -6.
      SELECT CASE(dim_cas)
      CASE(6)      
        DO  k=0,nprocz-1
          DO j=0,nprocy-1  
	    iproc=(k*nprocy+j)*nprocx+1
	    IF (numproc.EQ.iproc) THEN      
	      ALLOCATE(poids3_bordmx(nbmy_gl,nbmz_gl))
	      write(fdbg,*) 'Calcul de poids3_bordmx'
	      poids3_bordmx=0.
              DO iz=1,nbmz_gl
	        DO iy=1,nbmy_gl
	          DO iesp=1,nesp_p
	            CALL calcul_poids(nb_vidx+1,iy,iz,iesp,
     &                     qx,qy,qz,poids_rel)
		    poids3_bordmx(iy,iz)=
     &		          MAX(poids3_bordmx(iy,iz),poids_rel)	      
	          ENDDO                    
!	          write(fdbg,"(I4,2X,I4,2X,PE10.4)") iy,iz,poids3_bordmx(iy,iz)
                ENDDO
	      ENDDO 
	    ENDIF
	    
	    iproc=(k*nprocy+j+1)*nprocx
	    IF (numproc.EQ.iproc) THEN      
	      ALLOCATE(poids3_bordpx(nbmy_gl,nbmz_gl))
	      write(fdbg,*) 'Calcul de poids3_bordpx'
	      poids3_bordpx=0.
              DO iz=1,nbmz_gl
	        DO iy=1,nbmy_gl
	          DO iesp=1,nesp_p
	            CALL calcul_poids(nbmx_gl-nb_vidx,iy,iz,iesp,
     &                     qx,qy,qz,poids_rel)
		    poids3_bordpx(iy,iz)=
     &		          MAX(poids3_bordpx(iy,iz),poids_rel)	      
	          ENDDO              
!	          write(fdbg,"(I4,2X,I3,2X,PE10.4)") iy,iz,poids3_bordpx(iy,iz)  
                ENDDO
	      ENDDO 
	    ENDIF	    	
	  ENDDO
	ENDDO 
	         	      
	DO  k=0,nprocz-1
          DO i=0,nprocx-1
	    iproc=k*nprocx*nprocy+i+1
	    IF (numproc.EQ.iproc) THEN		
	      ALLOCATE(poids3_bordmy(nbmx_gl,nbmz_gl))
	      write(fdbg,*) 'Calcul de poids3_bordmy'
	      poids3_bordmy=0.
              DO iz=1,nbmz_gl
	        DO ix=1,nbmx_gl
	          DO iesp=1,nesp_p
	            CALL calcul_poids(ix,nb_vidy+1,iz,iesp,
     &                     qx,qy,qz,poids_rel)
                    poids3_bordmy(ix,iz)=
     &                    MAX(poids3_bordmy(ix,iz),poids_rel)
	          ENDDO        
!	          write(fdbg,"(I4,2X,I3,2X,PE10.4)") ix,iz,poids3_bordmy(ix,iz)  
                ENDDO
	      ENDDO 
	    ENDIF
	
	    iproc=(k+1)*nprocx*nprocy-nprocx+i+1
	    IF (numproc.EQ.iproc) THEN		
	      ALLOCATE(poids3_bordpy(nbmx_gl,nbmz_gl))
	    write(fdbg,*) 'Calcul de poids3_bordpy'
	      poids3_bordpy=0.
              DO iz=1,nbmz_gl
	        DO ix=1,nbmx_gl
	          DO iesp=1,nesp_p
	            CALL calcul_poids(ix,nbmy_gl-nb_vidy,iz,iesp,
     &                     qx,qy,qz,poids_rel)
                    poids3_bordpy(ix,iz)=
     &                    MAX(poids3_bordpy(ix,iz),poids_rel)
	          ENDDO    
!	          write(fdbg,"(I4,2X,I3,2X,PE10.4)") ix,iz,poids3_bordpy(ix,iz)    
                ENDDO
	      ENDDO 
	    ENDIF
	  ENDDO
	ENDDO 	
	
	DO  j=0,nprocy-1
          DO i=0,nprocx-1
	    iproc=j*nprocx+i+1
	    IF (numproc.EQ.iproc) THEN		
	      ALLOCATE(poids3_bordmz(nbmx_gl,nbmy_gl))
	      write(fdbg,*) 'Calcul de poids3_bordmz'
	      poids3_bordmx=0.
              DO iy=1,nbmy_gl
	        DO ix=1,nbmx_gl
	          DO iesp=1,nesp_p
	            CALL calcul_poids(ix,iy,nb_vidz+1,iesp,
     &                qx,qy,qz,poids_rel)
		    poids3_bordmz(ix,iy)=
     &                    MAX(poids3_bordmz(ix,iy),poids_rel)
	          ENDDO   
!	          write(fdbg,"(I4,2X,I3,2X,PE10.4)") ix,iy,poids3_bordmz(ix,iy)  
                ENDDO
	      ENDDO 
	    ENDIF
	    	
	    iproc=j*nprocx+(nprocz-1)*nprocx*nprocy+i+1	
	    IF (numproc.EQ.iproc) THEN		
	      ALLOCATE(poids3_bordpz(nbmx_gl,nbmy_gl))
	      write(fdbg,*) 'Calcul de poids3_bordpz'
	      poids3_bordpx=0.
              DO iy=1,nbmy_gl
	        DO ix=1,nbmx_gl
	          DO iesp=1,nesp_p
	            CALL calcul_poids(ix,iy,nbmz_gl-nb_vidz,iesp,
     &                qx,qy,qz,poids_rel)
		    poids3_bordpz(ix,iy)=
     &                    MAX(poids3_bordpz(ix,iy),poids_rel)
	          ENDDO   
!	          write(fdbg,"(I4,2X,I3,2X,PE10.4)") ix,iy,poids3_bordpz(ix,iy)       
                ENDDO
	      ENDDO 
	    ENDIF
	  ENDDO
	ENDDO 		
	 
      CASE(5,4)	
	DO j=0,nprocy-1
	  iproc=j*nprocx+1 
	  IF(numproc.EQ.iproc) THEN
	    ALLOCATE(poids2_bordmx(nbmy_gl))       
            poids2_bordmx=0.
	    write(fdbg,*) 'Calcul de poids2_bordmx'
	    DO iy=1,nbmy_gl
	      DO iesp=1,nesp_p
	        CALL calcul_poids(nb_vidx+1,iy,1,iesp,
     &            qx,qy,qz,poids_rel)
                poids2_bordmx(iy)=MAX(poids2_bordmx(iy),poids_rel)
	      ENDDO
!	      write(fdbg,"(I4,2X,PE10.4)") iy,poids2_bordmx(iy)
	    ENDDO
	  ENDIF
     	  
	  iproc=(j+1)*nprocx 
	  IF(numproc.EQ.iproc) THEN 
	    ALLOCATE(poids2_bordpx(nbmy_gl))       
            poids2_bordpx=0.
	    write(fdbg,*) 'Calcul de poids2_bordpx'
	    DO iy=1,nbmy_gl
	      DO iesp=1,nesp_p
	        CALL calcul_poids(nbmx_gl-nb_vidx,iy,1,iesp,
     &            qx,qy,qz,poids_rel)
                poids2_bordpx(iy)=MAX(poids2_bordpx(iy),poids_rel)
	      ENDDO
!	      write(fdbg,"(I4,2X,PE10.4)") iy,poids2_bordpx(iy)
	    ENDDO
	  ENDIF	    
	ENDDO	
	
	DO i=0,nprocx-1
	  iproc=i+1 	  
	  IF(numproc.EQ.iproc) THEN
	    ALLOCATE(poids2_bordmy(nbmx_gl))       
            poids2_bordmy=0.
	    write(fdbg,*) 'Calcul de poids2_bordmy'
	    DO ix=1,nbmx_gl
	      DO iesp=1,nesp_p
	        CALL calcul_poids(ix,nb_vidy+1,1,iesp,
     &            qx,qy,qz,poids_rel)
                poids2_bordmy(ix)=MAX(poids2_bordmy(ix),poids_rel)
	      ENDDO
!	      write(fdbg,"(I4,2X,PE10.4)") ix,poids2_bordmy(ix)
	    ENDDO
	  ENDIF
	  
	  iproc=nprocx*(nprocy-1)+i+1 	  
	  IF(numproc.EQ.iproc) THEN
	    ALLOCATE(poids2_bordpy(nbmx_gl))       
            poids2_bordpy=0.
	    write(fdbg,*) 'Calcul de poids2_bordpy'
	    DO ix=1,nbmx_gl
	      DO iesp=1,nesp_p
	        CALL calcul_poids(ix,nbmy_gl-nb_vidy,1,iesp,
     &            qx,qy,qz,poids_rel)
                poids2_bordpy(ix)=MAX(poids2_bordpy(ix),poids_rel)
	      ENDDO
!	      write(fdbg,"(I4,2X,PE10.4)") ix,poids2_bordpy(ix)
	    ENDDO
	  ENDIF
	ENDDO
	  
      CASE(3,2,1)
        IF(numproc.EQ.1) THEN
	  poids1_bordmx=0.
	  DO iesp=1,nesp_p
	    CALL calcul_poids(nb_vidx+1,1,1,iesp,
     &        qx,qy,qz,poids_rel)
	    poids1_bordmx=MAX(poids1_bordmx,poids_rel)
	  ENDDO
!	  write(fdbg,*) 'poids2_bordmx =',poids2_bordmx
	ENDIF  
        
        IF(numproc.EQ.nproc) THEN  
	  poids1_bordpx=0.
	  DO iesp=1,nesp_p
	    CALL calcul_poids(nbmx_gl-nb_vidx,1,1,iesp,
     &        qx,qy,qz,poids_rel)
	    poids1_bordpx=MAX(poids1_bordpx,poids_rel)
	  ENDDO
!	  write(fdbg,*) 'poids2_bordpx =',poids2_bordpx
	ENDIF	
      
      END SELECT
      RETURN
!     ______________________
      END SUBROUTINE init_poids_front
      
        
      
      SUBROUTINE aleat_unif(gerbe,methode,graine)
! ======================================================================
! Tirage d'un tableau de nombres aleatoires repartis uniformement
! ======================================================================
      USE precisions
      IMPLICIT NONE

      REAL(KIND=kr), DIMENSION(:), INTENT(OUT)   :: gerbe
      INTEGER                    , INTENT(IN)    :: methode
      INTEGER(KIND=8)            , INTENT(INOUT) :: graine
      INTEGER       :: i

      INTERFACE
        FUNCTION suite_equidistribuee(mseed)
          USE precisions
          REAL(KIND=kr)                  :: suite_equidistribuee
          INTEGER(KIND=8), INTENT(INOUT) :: mseed
        END FUNCTION suite_equidistribuee
      END INTERFACE

      SELECT CASE(methode)
      CASE(4)
        DO i=1,SIZE(gerbe)
          gerbe(i) = suite_equidistribuee(graine)
        ENDDO
      CASE DEFAULT
        CALL RANDOM_NUMBER(gerbe)
      END SELECT
      RETURN
!     _________________________
      END SUBROUTINE aleat_unif




      SUBROUTINE aleat_gaus(gerbe,methode,graine)
! ======================================================================
! Tirage d'un tableau de nombres aleatoires repartis selon une
! gaussienne de largeur a mi-hauteur egale a SQRT(8.*ln(2))
! c.a.d : exp(-0.5*(x**2))
! Le terme multiplicatif est SQRT(12/N)
! (Reference : Abramowitz et Stegun, pp 952-953)
! ======================================================================
      USE precisions
      IMPLICIT NONE

      REAL(KIND=kr), DIMENSION(:), INTENT(OUT)   :: gerbe
      INTEGER                    , INTENT(IN)    :: methode
      INTEGER(KIND=8)            , INTENT(INOUT) :: graine
!      REAL(KIND=kr), DIMENSION(SIZE(gerbe),10)   :: gerbe_u
      REAL(KIND=kr), DIMENSION(SIZE(gerbe),100)  :: gerbe_u
      INTEGER       :: i,j

      INTERFACE
        FUNCTION suite_equidistribuee(mseed)
          USE precisions
          REAL(KIND=kr)                  :: suite_equidistribuee
          INTEGER(KIND=8), INTENT(INOUT) :: mseed
        END FUNCTION suite_equidistribuee
      END INTERFACE

      SELECT CASE(methode)
      CASE(4)
        DO i=1,SIZE(gerbe)
!          DO j=1,10
          DO j=1,100
            gerbe_u(i,j) = suite_equidistribuee(graine)
          ENDDO
        ENDDO
      CASE DEFAULT
        CALL RANDOM_NUMBER(gerbe_u)
      END SELECT
!      gerbe(:) = (SUM(gerbe_u(:,1:10), DIM=2) - 5.) * 1.0954
      gerbe(:) = (SUM(gerbe_u(:,1:100), DIM=2) - 50.) * 0.34641
      RETURN
!     _________________________
      END SUBROUTINE aleat_gaus




      FUNCTION suite_equidistribuee(mseed)
! ======================================================================
!    generateur de suite equidistribuee a la Van Der Corput
!    version double precision, non optimisee
!    B. Meltz    11/08/98
! ======================================================================
      USE precisions
      IMPLICIT NONE
      REAL(KIND=kr)         :: suite_equidistribuee
      INTEGER(KIND=8), INTENT(INOUT)     :: mseed
      INTEGER(KIND=8), SAVE :: seed = 4
      INTEGER(KIND=8)       :: numerateur, denominateur, rang, 
     &                         decalage, temp

      if(mseed.LT.0) then
        seed = -mseed
      endif
! --- recherche du denominateur
      denominateur = 2
      temp = seed
      do
        if ( temp <= 1 ) exit
        temp = temp/2
        denominateur = denominateur*2
      enddo

! --- ordre dans la suite des numerateurs
      rang = seed - denominateur/2
! --- decalage, pour brouiller la suite des numerateurs
      decalage = denominateur/2 + 2
! --- numerateur
      numerateur = mod( 1 + rang*decalage, denominateur)

      IF( (float(numerateur)/float(denominateur).GE.1.).OR.
     &    (float(numerateur)/float(denominateur).LE.0.)    ) THEN
        print*, 'Erreur dans suite_equidistribuee ', seed, numerateur,
     &          denominateur, float(numerateur)/float(denominateur)
      ENDIF
      suite_equidistribuee = float(numerateur)/float(denominateur)

      seed = seed + 1
      mseed = seed
!     _________________________________
      END FUNCTION suite_equidistribuee
      
      
      
