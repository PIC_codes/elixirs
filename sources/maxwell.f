! ======================================================================
! maxwell.f
! ----------
!
!     SUBROUTINE harmonic
!
!     SUBROUTINE gradE
!
!     SUBROUTINE maxwel
!     SUBROUTINE pot2di
!     SUBROUTINE calq
!
!     SUBROUTINE pot2d_cflout
!
!
! ======================================================================



       SUBROUTINE harmonic
! ======================================================================
!      Routine pour test du pousseur dans un puit de potentiel
! 
!
! ======================================================================
       USE domaines
       USE champs
       USE particules
       USE temps
       USE erreurs
       USE laser
      
      
       IMPLICIT NONE


       INTEGER      ::   ix, jy
       INTEGER      ::   i0, j0

       INTEGER     ::  countx, county

       REAL(kind=kr)   ::   x, y
       REAL(kind=kr)   ::   x0, y0



       x0 = 0.5*(xmin_gl + xmax_gl)
       y0 = 0.5*(ymin_gl + ymax_gl)

       i0 = FLOOR(0.5*nbmx)+1
       j0 = FLOOR(0.5*nbmy)

       countx = nulpx - i0
       county = nulpy - j0

! ---- 1er quart du domaine
!      x>x0, y>0 (x0 milieu de xmin et xmax)
! -------
       DO ix=nulmx,nulpx
         x= (ix - i0)*dx 
         DO jy=nulmy,nulpy
           y= (jy - j0)*dy 
	   
           ex(ix,jy) = -x*omega1**2 
!           ey(ix,jy) = y
         ENDDO
       ENDDO
       
       ey(:,:) = 0.
       
! ---- 2� quart
!      x<x0, y>0
!      on sym�trise en partant du centre
! -------       
!       DO ix=1,countx
!         ex(i0-ix,j0:nulpy) = ex(i0+ix-1,j0:nulpy)
!         ey(i0-ix,j0:nulpy) = ey(i0+ix-1,j0:nulpy)
!       ENDDO
       
! ---- 2� moiti�
!      xmin<x<xmax, y<0
!      on sym�trise le haut du domaine
! --------       
!         DO jy=1,county
!           ex(nulmx:nulpx,j0-jy) = ex(nulmx:nulpx,j0+jy-1) 
!           ey(nulmx:nulpx,j0-jy) = ey(nulmx:nulpx,j0+jy-1)        
!         ENDDO
       
       
       ez(:,:) = 0.
       bx(:,:) = 0.
       by(:,:) = 0.
       bz(:,:) = 0.
       
       
       RETURN
!      ______________________       
       END SUBROUTINE harmonic



       SUBROUTINE calcgradE
! ======================================================================
! 
! 
!
!
! ======================================================================
       USE domaines
       USE champs

       USE temps
       USE erreurs
       USE laser
      
      
       IMPLICIT NONE
  
       INTEGER    ::    i, j



! ----- calcul de dEx/dx
       DO i=1,nx
           dexdx(i,1:ny)=ex(i+1,1:ny)-ex(i,1:ny)
       ENDDO
       dexdx(1:nx,1:ny)= dexdx(1:nx,1:ny)*dxi

! ----- Conditions aux limites p�riodiques
       dexdx(1:nx,0)  =dexdx(1:nx,ny)
       dexdx(1:nx,n1y)=dexdx(1:nx,1)

! ----- calcul de dEy/dx
       DO i=1,nx
           deydx(i,1:ny)= 0.5*(ey(i+1,2:n1y)+ey(i+1,1:ny)) -
     &                    0.5*(ey(i-1,2:n1y)+ey(i-1,1:ny))
       ENDDO
       deydx(1:nx,1:ny)= 0.5*deydx(1:nx,1:ny)*dxi

! ----- Conditions aux limites p�riodiques
       deydx(1:nx,0)  =deydx(1:nx,ny)
       deydx(1:nx,n1y)=deydx(1:nx,1)

! ----- calcul de dEz/dx
       DO i=1,nx
           dezdx(i,1:ny)= ez(i+1,1:ny) - ez(i-1,1:ny)
       ENDDO
       dezdx(1:nx,1:ny)= 0.5*dezdx(1:nx,1:ny)*dxi

! ----- Conditions aux limites p�riodiques
       dezdx(1:nx,0)  =dezdx(1:nx,ny)
       dezdx(1:nx,n1y)=dezdx(1:nx,1)


! ----- calcul de dEx/dy
       DO j=1,ny
           dexdy(1:nx,j)=0.5*(ex(2:n1x,j+1)+ex(1:nx,j+1)) - 
     &                   0.5*(ex(2:n1x,j-1)+ex(1:nx,j-1))
       ENDDO
       dexdy(1:nx,1:ny)= 0.5*dexdy(1:nx,1:ny)*dyi

! ----- Conditions aux limites p�riodiques
       dexdy(1:nx,0)  =dexdy(1:nx,ny)
       dexdy(1:nx,n1y)=dexdy(1:nx,1)

! ----- calcul de dEy/dy
       DO j=1,ny
           deydy(1:nx,j)=ey(1:nx,j+1) - ey(1:nx,j) 
       ENDDO
       deydy(1:nx,1:ny)= deydy(1:nx,1:ny)*dyi

! ----- Conditions aux limites p�riodiques
       deydy(1:nx,0)  =deydy(1:nx,ny)
       deydy(1:nx,n1y)=deydy(1:nx,1)

! ----- calcul de dEz/dy
       DO j=1,ny
           dezdy(1:nx,j)=ez(1:nx,j+1) - ez(1:nx,j-1) 
       ENDDO
       dezdy(1:nx,1:ny)= 0.5*dezdy(1:nx,1:ny)*dyi

! ----- Conditions aux limites p�riodiques
       dezdy(1:nx,0)  =dezdy(1:nx,ny)
       dezdy(1:nx,n1y)=dezdy(1:nx,1)





       RETURN
!      ______________________       
       END SUBROUTINE calcgradE



       SUBROUTINE maxwel
! ======================================================================
! 
! 
!
!
!
! ======================================================================
       USE domaines
       USE champs
       USE particules
       USE temps
       USE erreurs
       USE laser
      
      
       IMPLICIT NONE
  
       INTEGER    ::    i, j
  
       REAL(kind=kr), DIMENSION(0:n1x) :: 
     &    ct1, ct2, ct3, ct4, ct5
       
       REAL(kind=kr), DIMENSION(19,ndima) :: a
       REAL(kind=kr), DIMENSION(9,ndima)  :: al

       REAL(kind=kr), DIMENSION(9,ndimam) :: am
       REAL(kind=kr), DIMENSION(4,ndimam) :: alm
            
       REAL(kind=kr), DIMENSION(ndimam) ::   axx     

       REAL(kind=kr), DIMENSION(ndima) ::   aux      
       
       INTEGER, DIMENSION(1:ndima) ::   in(ndima)

       INTEGER, DIMENSION(1:ndimam) ::   inm(ndimam)
   
       REAL(kind=kr)       ::   dband_am, dband_a, 
     &      amqx, amqy, amqz, amqmax       

       REAL(kind=kr)   ::   amx, amy, amz
 
       INTEGER    ::  npx, iter_golub, ky, jf, jk, 
     &    il, ik, m1, m2, i9, i19
     
       REAL(kind=kr), DIMENSION(0:n1x,0:n1y)  :: 
     &    bxold, byold, bzold 
    
       REAL(kind=4), DIMENSION(0:n1x,0:n1y)  ::   worktab1, worktab2
       
!       REAL(kind=kr)    ::   maxdivE
              
       
!       REAL(kind=kr), DIMENSION(1:ny) ::  abs_ey_0, abs_ey_nx
      
 
 !     calcul du maximun du terme source q
 !
       amqx=0.
       amqy=0.
       amqz=0.
       
       DO j=1,ny
         DO i=1,nx
           amqx=max(amqx,abs(qx(i,j)))
           amqy=max(amqy,abs(qy(i,j)))
           amqz=max(amqz,abs(qz(i,j)))
         ENDDO
       ENDDO
     
       amqmax=max(amqx,amqy,amqz)
       write(fmsgo,*) 'maxwel : amqmax,amqx,amqy,amqz = ',
     &     amqmax,amqx,amqy,amqz          
  
 
! ---- On assure la p�riodicit� e[yz](0,:), e[yz](nx,:)
! ---- conditions aux limites p�riodiques      
       ex(0:2,0)  =ex(0:2,ny)
       ex(0:2,n1y)=ex(0:2,1)
       ey(0:2,0)  =ey(0:2,ny)
       ey(0:2,n1y)=ey(0:2,1)
       ez(0:2,0)  =ez(0:2,ny)
       ez(0:2,n1y)=ez(0:2,1)  

       ex(nx-2:nx,0)  =ex(nx-2:nx,ny)
       ex(nx-2:nx,n1y)=ex(nx-2:nx,1)
       ey(nx-2:nx,0)  =ey(nx-2:nx,ny)
       ey(nx-2:nx,n1y)=ey(nx-2:nx,1)
       ez(nx-2:nx,0)  =ez(nx-2:nx,ny)
       ez(nx-2:nx,n1y)=ez(nx-2:nx,1)  
 
! ---- on sauve E(tn) 
       extm1 = ex  
       eytm1 = ey 
       eztm1 = ez  
 

       IF(boundaries.EQ.1) THEN
! ---- condition absorbante e tangentiel en xmax       
         IF(ibndc(2).EQ.-2) THEN       
          qxnx(1:ny) = kebqxr1*(eytm1(nx,1:ny)+eytm1(nx-1,1:ny)
     &                       -eytm1(nx,2:n1y)-eytm1(nx-1,2:n1y))
     &    + kebqxr2*(bz(nx,2:n1y)-bz(nx,1:ny))
     &    + kebqxr3*(ebby(nx,2:n1y)-ebby(nx-1,2:n1y)
     &               -ebby(nx,1:ny)+ebby(nx-1,1:ny))
     &    + kebqxr4*(ebbx(nx,2:n1y)-2.*ebbx(nx,1:ny)+ebbx(nx,0:ny-1))
       
         qynxm1(1:ny) = kebqyr1*(eytm1(nx,2:n1y)+eytm1(nxm1,2:n1y))
     &    + kebqyr2*bz(nx,2:n1y)
     &    + kebqyr3*(ebby(nx,2:n1y)-ebby(nxm1,2:n1y))
     &    + kebqyr4*(ebbx(nx,2:n1y)-ebbx(nx,1:ny))
       
         qznxm1(1:ny) = kebqzr1*(ebbz(nx,1:ny)-ebbz(nxm1,1:ny))
     &    + kebqzr2*(eztm1(nx,1:ny)+eztm1(nxm1,1:ny))
     &    + kebqzr3*by(nx,1:ny)     
              
         ENDIF
	 
       ELSEIF(boundaries.EQ.2) THEN	 
! ---- condition absorbante e tangentiel en xmax       
         IF(ibndc(2).EQ.-2) THEN       	 
          qxnx(1:ny) = d11*(ebby(nx,2:n1y) - ebby(nx,1:ny))
     &     - vqxr1tild*(ebby(nx-1,2:n1y) - ebby(nx-1,1:ny))
     &    - vqxr2tild*(ebbx(nx,2:n1y)-2.*ebbx(nx,1:ny)+ebbx(nx,0:ny-1))
     &    - vqxr3tild*(bz(nx,2:n1y)-bz(nx,1:ny))	 
	 
          qynxm1(1:ny) = -0.5*dtsdx2*ebby(nx,2:n1y)
     &     - vqyr1tild*ebby(nxm1,2:n1y)
     &     - vqyr2tild*(ebbx(nx,2:n1y)-ebbx(nx,1:ny))
     &     - vqyr3tild*bz(nx, 2:n1y)	 
	 
          qznxm1(1:ny) = -0.5*dtsdx2*ebbz(nx,1:ny)
     &     - vqzr1tild*ebbz(nxm1,1:ny)
     &     - vqzr2tild*by(nx,1:ny)	 
	 
	 ENDIF
	 
       ELSEIF(boundaries.EQ.3) THEN
! ---- condition absorbante e tangentiel en xmax       
         IF(ibndc(2).EQ.-2) THEN
! ---- d�termination Ey(nx,:) et Ez(nx,:) � l'instant tn+1  
         ey(nx,2:n1y)=eytm1(nx,2:n1y)
     &     - dtsdx*(eytm1(nx,2:n1y)-eytm1(nxm1,2:n1y))
     &     + cb_45*dtsdy*(extm1(nx,2:n1y)-extm1(nx,1:ny))
    
         ez(nx,1:ny)=eztm1(nx,1:ny)
     &     - dtsdx*(eztm1(nx,1:ny)-eztm1(nxm1,1:ny))
     &     - ce_45*dtsdy*(bx_tn(nx,2:n1y)-bx_tn(nx,1:ny))     

           ey(nx,0) =ey(nx,ny)
           ey(nx,1) =ey(nx,n1y)

          qxnx(1:ny)  = -d11*ey(nx,2:n1y)+d11*ey(nx,1:ny)
          qynxm1(1:ny)= d13*ey(nx,1:ny)
          qznxm1(1:ny)= d13*ez(nx,1:ny)
         ENDIF
       ENDIF
       
       
       IF(boundaries.EQ.1) THEN
! ---- condition au bord implicite
         IF(ibndc(1).EQ.-2 .OR. ibndc(1).EQ.0) THEN
          qxbound(1:ny)= kebqx1*(bz(1,2:n1y)-bz(1,1:ny))
     &             + kebqx2*(ebby(1,1:ny)-ebby(0,1:ny)
     &                      -ebby(1,2:n1y)+ebby(0,2:n1y))
     &        + kebqx3*(2.*ebbx(1,1:ny)-ebbx(1,0:ny-1)-ebbx(1,2:n1y)) 
     &    + kebqx4*(eytm1(1,2:n1y)+eytm1(0,2:n1y)
     &             -eytm1(1,1:ny) -eytm1(0,1:ny))
	 
	  qybound(1:ny)= kebqy1*bz(1,2:n1y) 
     &       + kebqy2*(ebby(1,2:n1y)-ebby(0,2:n1y))
     &       + kebqy3*(eytm1(1,2:n1y)+eytm1(0,2:n1y))
     &       + kebqy4*(ebbx(1,2:n1y)-ebbx(1,1:ny))
     
          qzbound(1:ny)= kebqz1*by(1,1:ny)
     &           + kebqz2*(ebbz(1,1:ny)-ebbz(0,1:ny))
     &           + kebqz3*(eztm1(0,1:ny)+eztm1(1,1:ny))

         ENDIF
       
! ---- Injection laser implicite
         IF(ibndc(1).EQ.0) THEN
! ---- Injection du laser en E[yz]      
           CALL maxlim_2d1s2_nEB_implicit(pasdt*(iter+0.5)) 
!         CALL maxlim_2d1s2_nEB_implicit_huygens(pasdt*iter) 
         ENDIF
       
       ELSEIF(boundaries.EQ.2) THEN

! ---- condition au bord implicite
         IF(ibndc(1).EQ.-2 .OR. ibndc(1).EQ.0) THEN
          qxbound(1:ny)= -vqx1tild*(ebby(1,2:n1y)-ebby(1,1:ny))
     &     -d11*(ebby(0,2:n1y)-ebby(0,1:ny))
     &     -vqx2tild*(ebbx(1,2:n1y)-2.*ebbx(1,1:ny)+ebbx(1,0:ny-1))
     &     -vqx3tild*(bz(1,2:n1y)-bz(1,1:ny))
	 
	  qybound(1:ny)= -vqy1tild*ebby(1,2:n1y)
     &     - 0.5*dtsdx2*ebby(0,2:n1y)
     &     - vqy2tild*(ebbx(1,2:n1y)-ebbx(1,1:ny))
     &     - vqy3tild*bz(1,2:n1y)     
    
          qzbound(1:ny)= vkebz2tild*(ebbz(1,1:ny)-ebbz(0,1:ny))
     &    + vkebz3tild*(ebbz(0,1:ny)+ebbz(1,1:ny))
     &    + vkebz4tild*by(1,1:ny)
     
!         qzbound(1:ny)= fkebz2tild*by(1,1:ny)
!     &    + fkebz3tild*(ebbz(1,1:ny)-ebbz(0,1:ny))

!         qzbound(1:ny)= ffkebz2tild*by(1,1:ny)
!     &    + ffkebz3tild*(ebbz(1,1:ny)-ebbz(0,1:ny))
!     &    + ffkebz4tild*(eztm1(1,1:ny)-eztm1(0,1:ny))

         ENDIF      
	
! ---- Injection laser implicite
         IF(ibndc(1).EQ.0) THEN
! ---- Injection du laser en E[yz]      
           CALL maxlim_2d1s2_nEB_implicit_var(pasdt*iter) 
         ENDIF
	 
       ELSEIF(boundaries.EQ.3) THEN
! ---- condition absorbante e tangentiel en xmin
         IF(ibndc(1).EQ.-2 .OR. ibndc(1).EQ.0) THEN
! ---- d�termination Ey(0,:) et Ez(0,:) � l'instant tn+1 
!         ey(0,2:n1y)=ey0_tm1(2:n1y)
!     &     + dtsdx*(ey1_tm1(2:n1y)-ey0_tm1(2:n1y))
!     &     - cb_45*dtsdy*(ex1_tm1(2:n1y)-ex1_tm1(1:ny))
!      
!         ez(0,1:ny)=ez0_tm1(1:ny)
!     &     + dtsdx*(ez1_tm1(1:ny)-ez0_tm1(1:ny))
!     &     - ce_45*dtsdy*(bx_tn(0,2:n1y)-bx_tn(0,1:ny))
!
!         ey(0,0)  =ey(0,ny)
!         ey(0,1)  =ey(0,n1y)
	 
!         ey(0,2:n1y)=eytm1(0,2:n1y)
!     &     + dts2dx*(-eytm1(2,2:n1y)+4*eytm1(1,2:n1y)-3*eytm1(0,2:n1y))
!     &     - cb_45*dtsdy*(extm1(1,2:n1y)-extm1(1,1:ny))
!      
!         ez(0,1:ny)=eztm1(0,1:ny)
!     &     + dts2dx*(-eztm1(2,1:ny)+4*eztm1(1,1:ny)-3*eztm1(0,1:ny))
!     &     - ce_45*dtsdy*(bx_tn(0,2:n1y)-bx_tn(0,1:ny))
!
!         ey(0,0)  =ey(0,ny)
!         ey(0,1)  =ey(0,n1y)

          ey(0,0:n1y)= -COS(y_ang(1))*bz_tn(1,0:n1y)
          ez(0,0:n1y)=  by_tn(1,0:n1y)/COS(y_ang(1))
         ENDIF
 
! ---- conditions aux limites p�riodiques en y           
         ez(0,0)  =ez(0,ny)
         ez(0,n1y)=ez(0,1)  
       
! ---- couplage implicite/explicite
         IF(ibndc(1).EQ.0) THEN
! ---- Injection du laser en E[yz]      
           CALL maxlim_2d1s2_nEB(pasdt*(iter)) 
!         CALL maxlim_2d1s2_E(pasdt*(iter)) 	
!         CALL maxlim_huygens_E(pasdt*(iter)) 	
! ---- p�riodicit� Ey, Ez     
           ey(nulx_inj,n1y)=ey(nulx_inj,1)
           ey(nulx_inj,0)  =ey(nulx_inj,ny)
           ez(nulx_inj,n1y)=ez(nulx_inj,1)
           ez(nulx_inj,0)  =ez(nulx_inj,ny)
         ENDIF
       
! ---- couplage implicite/explicite       
         IF(ibndc(1).EQ.-2 .OR. ibndc(1).EQ.0) THEN
          qxbound(1:ny) = d11*ey(0,2:n1y)-d11*ey(0,1:ny)
          qybound(1:ny) = d13*ey(0,1:ny)
          qzbound(1:ny) = d13*ez(0,1:ny)
         ENDIF 
       ENDIF

 
 !     calcul du terme source pour le demarrage de la methode iterative
 !
       DO j=1,ny
         DO i=1,nx
          zrx(i,j)=qx(i,j)+(xxx0(i)-xxx(i,j))*ex(i,j)                        
     &         +0.25*((xxy0(i-1)-xxy(i-1,j))*(ey(i-1,j+1)+ey(i-1,j))      
     &                +(xxy0(i)-xxy(i,j))*(ey(i,j+1)+ey(i,j)))             
     &         +0.5*((xxz0(i-1)-xxz(i-1,j))*ez(i-1,j)                      
     &              +(xxz0(i)-xxz(i,j))*ez(i,j))                           
     &      -dts2dy*((zzx0(i)-zzx(i,j+1))*ex(i,j+1)                       
     &               -(zzx0(i)-zzx(i,j-1))*ex(i,j-1)                       
     &               +(zzy0(i)-zzy(i,j+1))*ey(i,j+1)                       
     &               -(zzy0(i)-zzy(i,j))*ey(i,j)                           
     &               +(zzy0(i-1)-zzy(i-1,j+1))*ey(i-1,j+1)                 
     &               -(zzy0(i-1)-zzy(i-1,j))*ey(i-1,j))                    
     &       -dts4dy*((zzz0(i)-zzz(i,j+1))*ez(i,j+1)                       
     &               -(zzz0(i)-zzz(i,j-1))*ez(i,j-1)                       
     &               +(zzz0(i-1)-zzz(i-1,j+1))*ez(i-1,j+1)                 
     &               -(zzz0(i-1)-zzz(i-1,j-1))*ez(i-1,j-1))
         ENDDO
	 zrx(1,j) = zrx(1,j) +qxbound(j)
	 zrx(nx,j)= zrx(nx,j)+qxnx(j)
       ENDDO
       
       zrx(n1x,1:ny)=0.


       DO j=1,ny
         DO i=1,nxm1
       zry(i,j)=qy(i,j)+(xyy0(i  )-xyy(i  ,j  ))*ey(i,j)                  
     &       +0.25*((xyx0(i  )-xyx(i  ,j-1))*(ex(i+1,j-1)+ex(i,j-1))  
     &                +(xyx0(i  )-xyx(i  ,j  ))*(ex(i+1,j)+ex(i,j)))     
     &             +0.5*((xyz0(i  )-xyz(i  ,j  ))*ez(i,j)                  
     &                  +(xyz0(i  )-xyz(i  ,j-1))*ez(i,j-1))               
     &         +dts2dx *((zzx0(i+1)-zzx(i+1,j  ))*ex(i+1,j)                
     &                  -(zzx0(i  )-zzx(i  ,j  ))*ex(i,j)                  
     &                  +(zzx0(i+1)-zzx(i+1,j-1))*ex(i+1,j-1)              
     &                  -(zzx0(i  )-zzx(i,j-1  ))*ex(i,j-1)                
     &                  +(zzy0(i+1)-zzy(i+1,j  ))*ey(i+1,j)                
     &                  -(zzy0(i-1)-zzy(i-1,j  ))*ey(i-1,j))               
     &          +dts4dx*((zzz0(i+1)-zzz(i+1,j  ))*ez(i+1,j)                
     &                  -(zzz0(i-1)-zzz(i-1,j  ))*ez(i-1,j)                
     &                  +(zzz0(i+1)-zzz(i+1,j-1))*ez(i+1,j-1)              
     &                  -(zzz0(i-1)-zzz(i-1,j-1))*ez(i-1,j-1))
 !
       zrz(i,j)=qz(i,j)+(xzz0(i  )-xzz(i  ,j  ))*ez(i,j)                  
     &           +0.5*((xzx0(i  )-xzx(i  ,j  ))*(ex(i+1,j)+ex(i,j))      
     &               +(xzy0(i  )-xzy(i  ,j  ))*(ey(i,j)+ey(i,j+1)))     
     &          -dtsdx *((zyx0(i+1)-zyx(i+1,j  ))*ex(i+1,j)                
     &                  -(zyx0(i  )-zyx(i  ,j  ))*ex(i,j))                 
     &          -dts4dx*((zyy0(i+1)-zyy(i+1,j+1))*ey(i+1,j+1)              
     &                  -(zyy0(i-1)-zyy(i-1,j+1))*ey(i-1,j+1)              
     &                  +(zyy0(i+1)-zyy(i+1,j  ))*ey(i+1,j)                
     &                  -(zyy0(i-1)-zyy(i-1,j  ))*ey(i-1,j))               
     &          -dts2dx*((zyz0(i+1)-zyz(i+1,j  ))*ez(i+1,j)                
     &                  -(zyz0(i-1)-zyz(i-1,j  ))*ez(i-1,j))
       zrz(i,j)=zrz(i,j)                                                  
     &          +dts4dy*((zxx0(i+1)-zxx(i+1,j+1))*ex(i+1,j+1)              
     &                  -(zxx0(i+1)-zxx(i+1,j-1))*ex(i+1,j-1)              
     &                  +(zxx0(i  )-zxx(i  ,j+1))*ex(i,j+1)                
     &                  -(zxx0(i  )-zxx(i  ,j-1))*ex(i,j-1))               
     &           +dtsdy*((zxy0(i  )-zxy(i  ,j+1))*ey(i,j+1)                
     &                  -(zxy0(i  )-zxy(i  ,j  ))*ey(i,j))                 
     &          +dts2dy*((zxz0(i  )-zxz(i  ,j+1))*ez(i,j+1)                
     &                  -(zxz0(i  )-zxz(i  ,j-1))*ez(i,j-1))
         ENDDO
	 zry(1,j) = zry(1,j)+qybound(j)
	 zrz(1,j) = zrz(1,j)+qzbound(j)

	 zry(nxm1,j) = zry(nxm1,j)+qynxm1(j)
	 zrz(nxm1,j) = zrz(nxm1,j)+qznxm1(j)
       ENDDO
      
          
 !
 !
 !     transformees de fourier du terme source dans la direction y
 !
       npx=nxm1s2+1
       
!       call cpftv(zrx(1,1),zrx(npx+1,1),ny,n2x,-1.,1,npx,cwk)
!       call rpft2v(zrx(1,1),zrx(npx+1,1),ny,n2x,1,npx)
!       call cpftv(zry(1,1),zrz(1,1),ny,n2x,-1.,1,nxm1,cwk)
!       call rpft2v(zry(1,1),zrz(1,1),ny,n2x,1,nxm1)

!       write(fmsgo,*) 'Avant FFT zrx' 
       worktab1=real(zrx,4)
       call cpftv(worktab1(1,1),worktab1(npx+1,1),ny,
     &                       n2x,-1.,1,npx,cwk)
       call rpft2v(worktab1(1,1),worktab1(npx+1,1),ny,n2x,1,npx)
       zrx = 0.
       zrx=real(worktab1,8)
!       write(fmsgo,*) 'Apres FFT zrx'
       
       worktab1=real(zry,4)
       worktab2=real(zrz,4)
       call cpftv(worktab1(1,1),worktab2(1,1),ny,n2x,-1.,1,nxm1,cwk)
       call rpft2v(worktab1(1,1),worktab2(1,1),ny,n2x,1,nxm1)
       zry = 0.
       zrz = 0.
       zry=real(worktab1,8)
       zrz=real(worktab2,8)
       
 !
 !     normalisation des x..0 et z..0
 !
       DO i=0,nx
         ct4(i)=0.25*xxy0(i)
         ct5(i)=0.25*xyx0(i)
         xxz0(i)=0.5*xxz0(i)
         xyz0(i)=0.5*xyz0(i)
         xzx0(i)=0.5*xzx0(i)
         xzy0(i)=0.5*xzy0(i)
 !
         ct1(i)=dts2dy*zzy0(i)
         zzy0(i)=dts2dx*zzy0(i)
         ct2(i)=dts2dy*zzz0(i)
         zzz0(i)=dts4dx*zzz0(i)
         ct3(i)=dtsdy*zzx0(i)
         zzx0(i)=dts2dx*zzx0(i)
 !
         zxx0(i)=dts2dy*zxx0(i)
         zxy0(i)=dtsdy*zxy0(i)
         zxz0(i)=dtsdy*zxz0(i)
         zyx0(i)=dtsdx*zyx0(i)
         zyy0(i)=dts4dx*zyy0(i)
         zyz0(i)=dts2dx*zyz0(i)
       ENDDO
 
       write(fmsgo,*) 'Avant boucle golub'

       DO iter_golub=1,nbmax
 
       ky=1   !  mode 0   et nys2

       DO  jf=1,2
       am=0.
       alm=0.
 
 !     ecriture des lignes de la matrice
       il=0
       DO  i=1,nxm1f3,3
         il=il+1
 
         am(3,i)=(-(d11+ct1(il-1))*csym1(ky)+ct4(il-1)*csyp1(ky))
         am(4,i)=(xxz0(il-1))
         am(5,i)=(1.+xxx0(il)-dtsdy2*csym1(ky))
         am(6,i)=((d11-ct1(il))*csym1(ky)+ct4(il)*csyp1(ky))
         am(7,i)=(xxz0(il))
 
         am(2,i+1)=(-d13-zzy0(il-1))
         am(3,i+1)=(-zzz0(il-1)*csyp1(ky))
         am(4,i+1)=(d11*csym1(ky)+(ct5(il)-zzx0(il))*csyp1(ky))
         am(5,i+1)=(1.+xyy0(il)+dtsdx2)
         am(6,i+1)=(xyz0(il)*csyp1(ky))
         am(7,i+1)=(-d11*csym1(ky)+(ct5(il)+zzx0(il+1))*csyp1(ky))
         am(8,i+1)=(-d13+zzy0(il+1))
         am(9,i+1)=(zzz0(il+1)*csyp1(ky))
 !
         am(1,i+2)=(zyy0(il-1)*csyp1(ky))
         am(2,i+2)=(-d13+zyz0(il-1))
         am(3,i+2)=(xzx0(il)+zyx0(il))
         am(4,i+2)=(xzy0(il)*csyp1(ky)+zxy0(il)*csym1(ky))
         am(5,i+2)=(1.+xzz0(il)+dtsdx2-dtsdy2*csym1(ky))
         am(6,i+2)=(xzx0(il)-zyx0(il+1))
         am(7,i+2)=(-zyy0(il+1)*csyp1(ky))
         am(8,i+2)=(-d13-zyz0(il+1)) 
       ENDDO
    
       i=nxm1f3+1
       il=nx
       am(3,i)=(-(d11+ct1(il-1))*csym1(ky)+ct4(il-1)*csyp1(ky))
       am(4,i)=(xxz0(il-1))
       am(5,i)=(1.+xxx0(il)-dtsdy2*csym1(ky))
       
    
       IF(boundaries.EQ.1) THEN
       
         IF(ibndc(1).EQ.-2 .OR. ibndc(1).EQ.0) THEN              
! ---- partie r�elle ex
	 am(5,1)= am(5,1) + 2*keb2*csym1(ky)
	 am(6,1)= am(6,1) - keb1*csym1(ky)

! ---- partie r�elle ey       
         am(4,2)= am(4,2) - keb4*csym1(ky)
         am(5,2)= am(5,2) + keb3

! ---- partie r�elle ez    
         am(5,3)= am(5,3) + keb5
         ENDIF
	 
	 IF(ibndc(2).EQ.-2) THEN
! ---- partie r�elle ex
         am(3,nxm1f3+1)= am(3,nxm1f3+1) - kebrex1*csym1(ky)
         am(5,nxm1f3+1)= am(5,nxm1f3+1) + kebrex2*csym1(ky)

! ---- partie r�elle ey
         am(5,nxm1f3-1)= am(5,nxm1f3-1) + kebrey1
         am(7,nxm1f3-1)= am(7,nxm1f3-1) + kebrey2*csym1(ky)

! ---- partie r�elle ez	 
         am(5,nxm1f3)= am(5,nxm1f3) + kebrez1
 
	 ENDIF

       ELSEIF(boundaries.EQ.2) THEN
       
         IF(ibndc(1).EQ.-2 .OR. ibndc(1).EQ.0) THEN              
! ---- partie r�elle ex
	 am(5,1)= am(5,1) + 2.*vex2tild*csym1(ky) 
	 am(6,1)= am(6,1) + vex1tild*csym1(ky)

! ---- partie r�elle ey       
         am(4,2)= am(4,2) - vey2tild*csym1(ky)
         am(5,2)= am(5,2) + vey1tild

! ---- partie r�elle ez    
         am(5,3)= am(5,3) + vkebz1tild
!         am(5,3)= am(5,3) + fkebz1tild
!         am(5,3)= am(5,3) + ffkebz1tild	 
         ENDIF
	 
	 IF(ibndc(2).EQ.-2) THEN	 
! ---- partie r�elle ex
         am(3,nxm1f3+1)= am(3,nxm1f3+1) + vexr1tild*csym1(ky)
         am(5,nxm1f3+1)= am(5,nxm1f3+1) + 2.*vexr2tild*csym1(ky)

! ---- partie r�elle ey
         am(5,nxm1f3-1)= am(5,nxm1f3-1) + veyr1tild
         am(7,nxm1f3-1)= am(7,nxm1f3-1) - veyr2tild*csym1(ky)

! ---- partie r�elle ez	 
         am(5,nxm1f3)= am(5,nxm1f3) + vezr1tild 
	 
	 ENDIF	 
       
       ENDIF
      
 

  
       ik=0
       
       DO  i=1,nxm1f3,3
         ik=ik+1
         axx(i  )=(zrx(ik,ky))
         axx(i+1)=(zry(ik,ky))
         axx(i+2)=(zrz(ik,ky))
       ENDDO
 
       axx(nxm1f3+1)=zrx(nx,ky)       
        
       m1=4
       m2=4
       i9=9
       
        DO i=1,m1
          DO j=1,5-i
            am(j,i)=0.
          ENDDO
        ENDDO
       
        call bandec(am,ndimam,m1,m2,i9,alm,m1,inm,dband_am)
        call banbks(am,ndimam,m1,m2,i9,alm,m1,inm,axx)
	
 !
 !     on met la solution dans e.
 
       ik=0
       DO i=1,nxm1f3,3
         ik=ik+1
         ex(ik,ky)=axx(i)
         ey(ik,ky)=axx(i+1)
         ez(ik,ky)=axx(i+2)
       ENDDO

       ex(nx,ky)=axx(nxm1f3+1)
 
       ky=nys2p1    ! mode nys2

       ENDDO
 
       write(fmsgo,*) 'avant boucle sur les modes ky'
     
       jk=n1y 
       DO ky=2,nys2   !     on traite les lignes principales
       
       a=0.  ! reinitialisation de la matrice
       al=0.
 
       jk=jk-1
 
       il=1
         DO  i=7,nxm1f6,6
          il=il+1
 
 !     ligne ex reelle
 
          a( 6,i)=(-(d11+ct1(il-1))*csym1(ky)+ct4(il-1)*csyp1(ky))
          a( 7,i)=((d11+ct1(il-1)-ct4(il-1))*sny(ky))
          a( 8,i)=(xxz0(il-1))
          a( 9,i)=(ct2(il-1)*sny(ky))
          a(10,i)=(1.+xxx0(il)-dtsdy2*csym1(ky))
          a(11,i)=(ct3(il)*sny(ky))
          a(12,i)=((d11-ct1(il))*csym1(ky)+ct4(il)*csyp1(ky))
          a(13,i)=((-d11-ct4(il)+ct1(il))*sny(ky))
          a(14,i)=(xxz0(il))
          a(15,i)=(ct2(il)*sny(ky))
 
 !     ligne ex imaginaire
 
          a( 5,i+1)=-a( 7,i)
          a( 6,i+1)= a( 6,i)
          a( 7,i+1)=-a( 9,i)
          a( 8,i+1)= a( 8,i)
          a( 9,i+1)=-a(11,i)
          a(10,i+1)= a(10,i)
          a(11,i+1)=-a(13,i)
          a(12,i+1)= a(12,i)
          a(13,i+1)=-a(15,i)
          a(14,i+1)= a(14,i)
 
          a( 4,i+2)=(-d13-zzy0(il-1))
          a( 6,i+2)=(-zzz0(il-1)*csyp1(ky))
          a( 7,i+2)=(-zzz0(il-1)*sny(ky))
          a( 8,i+2)=(d11*csym1(ky)+(ct5(il)-zzx0(il))*csyp1(ky))
          a(9, i+2)=((d11+ct5(il)-zzx0(il))*sny(ky))
          a(10,i+2)=(1.+xyy0(il)+dtsdx2)
          a(12,i+2)=(xyz0(il)*csyp1(ky))
          a(13,i+2)=(xyz0(il)*sny(ky))
          a(14,i+2)=(-d11*csym1(ky)+(ct5(il)+zzx0(il+1))*csyp1(ky))
          a(15,i+2)=((-d11+ct5(il)+zzx0(il+1))*sny(ky))
          a(16,i+2)=(-d13+zzy0(il+1))
          a(18,i+2)=(zzz0(il+1)*csyp1(ky))
          a(19,i+2)=(zzz0(il+1)*sny(ky))
         ENDDO

       il=1
       DO  i=7,nxm1f6,6
         il=il+1
 
 !     ligne ey imaginaire
 
         a( 4,i+3)= a( 4,i+2)
         a( 5,i+3)=-a( 7,i+2)
         a( 6,i+3)= a( 6,i+2)
         a( 7,i+3)=-a( 9,i+2)
         a( 8,i+3)= a( 8,i+2)
!         a( 9,i+3)=-a(11,i+2)
         a(10,i+3)= a(10,i+2)
         a(11,i+3)=-a(13,i+2)
         a(12,i+3)= a(12,i+2)
         a(13,i+3)=-a(15,i+2)
         a(14,i+3)= a(14,i+2)
         a(16,i+3)= a(16,i+2)
         a(17,i+3)=-a(19,i+2)
         a(18,i+3)= a(18,i+2)
 
 !     ligne ez reelle
 
         a( 2,i+4)=(zyy0(il-1)*csyp1(ky))
         a( 3,i+4)=(-zyy0(il-1)*sny(ky))
         a( 4,i+4)=(-d13+zyz0(il-1))
         a( 6,i+4)=(xzx0(il)+zyx0(il))
         a( 7,i+4)=(-zxx0(il)*sny(ky))
         a( 8,i+4)=(xzy0(il)*csyp1(ky)+zxy0(il)*csym1(ky))
         a( 9,i+4)=(-(xzy0(il)+zxy0(il))*sny(ky))
         a(10,i+4)=(1.+xzz0(il)+dtsdx2-dtsdy2*csym1(ky))
         a(11,i+4)=(-zxz0(il)*sny(ky))
         a(12,i+4)=(xzx0(il)-zyx0(il+1))
         a(13,i+4)=(-zxx0(il+1)*sny(ky))
         a(14,i+4)=(-zyy0(il+1)*csyp1(ky))
         a(15,i+4)=(zyy0(il+1)*sny(ky))
         a(16,i+4)=(-d13-zyz0(il+1))
 
 !     ligne ez imaginaire
 
         a( 1,i+5)=-a( 3,i+4)
         a( 2,i+5)= a( 2,i+4)
         a( 4,i+5)= a( 4,i+4)
         a( 5,i+5)=-a( 7,i+4)
         a( 6,i+5)= a( 6,i+4)
         a( 7,i+5)=-a( 9,i+4)
         a( 8,i+5)= a( 8,i+4)
         a( 9,i+5)=-a(11,i+4)
         a(10,i+5)= a(10,i+4)
         a(11,i+5)=-a(13,i+4)
         a(12,i+5)= a(12,i+4)
         a(13,i+5)=-a(15,i+4)
         a(14,i+5)= a(14,i+4)
         a(16,i+5)= a(16,i+4)
       ENDDO

 !     on traite les lignes correspondant a ex en nx et ex,ey,ez en 1

 
       i=nxm1f6+1
       il=nx
   
 !     ligne ex reelle 
       a( 6,i)=(-(d11+ct1(il-1))*csym1(ky)+ct4(il-1)*csyp1(ky))
       a( 7,i)=((d11+ct1(il-1)-ct4(il-1))*sny(ky))
       a( 8,i)=(xxz0(il-1))
       a( 9,i)=(ct2(il-1)*sny(ky))
       a(10,i)=(1.+xxx0(il)-dtsdy2*csym1(ky))
       a(11,i)=(ct3(il)*sny(ky))
 
 !     ligne ex imaginaire 
       a( 5,i+1)=-a( 7,i)
       a( 6,i+1)= a( 6,i)
       a( 7,i+1)=-a( 9,i)
       a( 8,i+1)= a( 8,i)
       a( 9,i+1)=-a(11,i)
       a(10,i+1)= a(10,i)
 
 
! ---- Ces coefficients ne servent pas pour la r�solution de la matrice bande
! ----  ligne ey r�elle
!       a( 16,nxm1f6-3)=0
!       a( 18,nxm1f6-3)=0
!       a( 19,nxm1f6-3)=0       
! ----  ligne ey imaginaire
!       a( 16,nxm1f6-2)=0
!       a( 17,nxm1f6-2)=0
!       a( 18,nxm1f6-2)=0       
! ---- ligne ez r�elle
!       a( 14,nxm1f6-1)=0
!       a( 15,nxm1f6-1)=0
!       a( 16,nxm1f6-1)=0       
! ----  ligne ez imaginaire
!       a( 13,nxm1f6)=0
!       a( 14,nxm1f6)=0
!       a( 16,nxm1f6)=0       
 
 
 
       i=1
       il=1

!       ligne ex reelle

       a(10,1)=(1.+xxx0(il)-dtsdy2*csym1(ky))
       a(11,1)=(ct3(il)*sny(ky))
       a(12,1)=((d11-ct1(il))*csym1(ky)+ct4(il)*csyp1(ky))
       a(13,1)=((-d11-ct4(il)+ct1(il))*sny(ky))
       a(14,1)=(xxz0(il))
       a(15,1)=(ct2(il)*sny(ky))

!       ligne ex imaginaire
 
       a( 9,2)=-a(11,1)
       a(10,2)= a(10,1)
       a(11,2)=-a(13,1)
       a(12,2)= a(12,1)
       a(13,2)=-a(15,1)
       a(14,2)= a(14,1)

       a( 8,3)=(d11*csym1(ky)+(ct5(il)-zzx0(il))*csyp1(ky))
       a( 9,3)=((d11+ct5(il)-zzx0(il))*sny(ky))
       a(10,3)=(1.+xyy0(il)+dtsdx2)
       a(12,3)=(xyz0(il)*csyp1(ky))
       a(13,3)=(xyz0(il)*sny(ky))
       a(14,3)=(-d11*csym1(ky)+(ct5(il)+zzx0(il+1))*csyp1(ky))
       a(15,3)=((-d11+ct5(il)+zzx0(il+1))*sny(ky))
       a(16,3)=(-d13+zzy0(il+1))
       a(18,3)=(zzz0(il+1)*csyp1(ky))
       a(19,3)=(zzz0(il+1)*sny(ky))

!     ligne ey imaginaire
 
       a( 7,4)=-a( 9,3)
       a( 8,4)= a( 8,3)
!       a(9,4)=-a(11,3)
       a(10,4)= a(10,3)
       a(11,4)=-a(13,3)
       a(12,4)= a(12,3)
       a(13,4)=-a(15,3)
       a(14,4)= a(14,3)
       a(16,4)= a(16,3)
       a(17,4)=-a(19,3)
       a(18,4)= a(18,3)
 
 !     ligne ez reelle
 
       a( 6,5)=(xzx0(il)+zyx0(il))
       a( 7,5)=(-zxx0(il)*sny(ky))
       a( 8,5)=(xzy0(il)*csyp1(ky)+zxy0(il)*csym1(ky))
       a( 9,5)=(-(xzy0(il)+zxy0(il))*sny(ky))
       a(10,5)=(1.+xzz0(il)+dtsdx2-dtsdy2*csym1(ky))
       a(11,5)=(-zxz0(il)*sny(ky))
       a(12,5)=(xzx0(il)-zyx0(il+1))
       a(13,5)=(-zxx0(il+1)*sny(ky))
       a(14,5)=(-zyy0(il+1)*csyp1(ky))
       a(15,5)=(zyy0(il+1)*sny(ky))
       a(16,5)=(-d13-zyz0(il+1))
 
 !     ligne ez imaginaire
 
       a( 5,6)=-a( 7,5)
       a( 6,6)= a( 6,5)
       a( 7,6)=-a( 9,5)
       a( 8,6)= a( 8,5)
       a( 9,6)=-a(11,5)
       a(10,6)= a(10,5)
       a(11,6)=-a(13,5)
       a(12,6)= a(12,5)
       a(13,6)=-a(15,5)
       a(14,6)= a(14,5)
       a(16,6)= a(16,5)


       IF(boundaries.EQ.1) THEN 
! ---- coefficients modifi�s pour condition d onde sortante
!      de type k x E = B
! ---- 
         IF(ibndc(1).EQ.-2 .OR. ibndc(1).EQ.0) THEN
         a(10,1)= a(10,1) + 2.*keb2*csym1(ky)
         a(12,1)= a(12,1) - keb1*csym1(ky)
         a(13,1)= a(13,1) + keb1*sny(ky)
       
         a(10,2)= a(10,2) + 2.*keb2*csym1(ky)
         a(11,2)= a(11,2) - keb1*sny(ky)
         a(12,2)= a(12,2) - keb1*csym1(ky)
       
         a(8,3) = a(8,3) - keb4*csym1(ky)
         a(9,3) = a(9,3) - keb4*sny(ky)
         a(10,3)= a(10,3) + keb3
       
         a(7,4) = a(7,4) + keb4*sny(ky)
         a(8,4) = a(8,4) - keb4*csym1(ky)
         a(10,4)= a(10,4) + keb3
       
         a(10,5)= a(10,5) + keb5

         a(10,6)= a(10,6) + keb5        
         ENDIF       
	 
	 IF(ibndc(2).EQ.-2) THEN
	 a(6,nxm1f6+1)  = a(6,nxm1f6+1) - kebrex1*csym1(ky)
	 a(7,nxm1f6+1)  = a(7,nxm1f6+1) + kebrex1*sny(ky)
	 a(10,nxm1f6+1) = a(10,nxm1f6+1) + kebrex2*csym1(ky)
	 
	 a(5,nxm1f6+2)  = a(5,nxm1f6+2) - kebrex1*sny(ky)
	 a(6,nxm1f6+2)  = a(6,nxm1f6+2)	- kebrex1*csym1(ky)	 
	 a(10,nxm1f6+2) = a(10,nxm1f6+2) + kebrex2*csym1(ky)
	 
	 a(10,nxm1f6-3) = a(10,nxm1f6-3) + kebrey1
	 a(14,nxm1f6-3) = a(14,nxm1f6-3) + kebrey2*csym1(ky)
	 a(15,nxm1f6-3) = a(15,nxm1f6-3) + kebrey2*sny(ky)
	 
	 a(10,nxm1f6-2) = a(10,nxm1f6-2) + kebrey1
	 a(13,nxm1f6-2) = a(13,nxm1f6-2) - kebrey2*sny(ky)
	 a(14,nxm1f6-2) = a(14,nxm1f6-2) + kebrey2*csym1(ky) 
	 
	 a(10,nxm1f6-1) = a(10,nxm1f6-1) + kebrez1

	 a(10,nxm1f6) = a(10,nxm1f6) + kebrez1 
	 
	 ENDIF
	 
       ELSEIF(boundaries.EQ.2) THEN 
! ---- coefficients modifi�s pour condition d onde sortante
!      de type k x E = B
! ---- 
         IF(ibndc(1).EQ.-2 .OR. ibndc(1).EQ.0) THEN
         a(10,1)= a(10,1) + 2.*vex2tild*csym1(ky)
         a(12,1)= a(12,1) + vex1tild*csym1(ky)
         a(13,1)= a(13,1) - vex1tild*sny(ky)
       
         a(10,2)= a(10,2) + 2.*vex2tild*csym1(ky)
         a(11,2)= a(11,2) + vex1tild*sny(ky)
         a(12,2)= a(12,2) + vex1tild*csym1(ky)
       
         a(8,3) = a(8,3) - vey2tild*csym1(ky)
         a(9,3) = a(9,3) - vey2tild*sny(ky)
         a(10,3)= a(10,3) + vey1tild
       
         a(7,4) = a(7,4) + vey2tild*sny(ky)
         a(8,4) = a(8,4) - vey2tild*csym1(ky)
         a(10,4)= a(10,4) + vey1tild
       
         a(10,5)= a(10,5) + vkebz1tild
         a(10,6)= a(10,6) + vkebz1tild
	 
!         a(10,5)= a(10,5) + fkebz1tild
!         a(10,6)= a(10,6) + fkebz1tild
	 
!         a(10,5)= a(10,5) + ffkebz1tild
!         a(10,6)= a(10,6) + ffkebz1tild
         ENDIF 
	 
	 IF(ibndc(2).EQ.-2) THEN	 
	 a(6,nxm1f6+1)  = a(6,nxm1f6+1) + vexr1tild*csym1(ky)
	 a(7,nxm1f6+1)  = a(7,nxm1f6+1) - vexr1tild*sny(ky)
	 a(10,nxm1f6+1) = a(10,nxm1f6+1) + 2.*vexr2tild*csym1(ky)
	 
	 a(5,nxm1f6+2)  = a(5,nxm1f6+2) + vexr1tild*sny(ky)
	 a(6,nxm1f6+2)  = a(6,nxm1f6+2)	+ vexr1tild*csym1(ky)
	 a(10,nxm1f6+2) = a(10,nxm1f6+2) + 2.*vexr2tild*csym1(ky)
	 
	 a(10,nxm1f6-3) = a(10,nxm1f6-3) + veyr1tild
	 a(14,nxm1f6-3) = a(14,nxm1f6-3) - veyr2tild*csym1(ky)
	 a(15,nxm1f6-3) = a(15,nxm1f6-3) - veyr2tild*sny(ky)
	 
	 a(10,nxm1f6-2) = a(10,nxm1f6-2) + veyr1tild
	 a(13,nxm1f6-2) = a(13,nxm1f6-2) + veyr2tild*sny(ky)
	 a(14,nxm1f6-2) = a(14,nxm1f6-2) - veyr2tild*csym1(ky)
	 
	 a(10,nxm1f6-1) = a(10,nxm1f6-1) + vezr1tild

	 a(10,nxm1f6) = a(10,nxm1f6) + vezr1tild
	 ENDIF	 
       
       ENDIF

 !     transfert du terme  source dans aux

       ik=0
       DO  i=1,nxm1f6,6
         ik=ik+1
         aux(i  )=(zrx(ik,ky))
         aux(i+1)=(zrx(ik,jk))
         aux(i+2)=(zry(ik,ky))
         aux(i+3)=(zry(ik,jk))
         aux(i+4)=(zrz(ik,ky))
         aux(i+5)=(zrz(ik,jk))
       ENDDO

       aux(nxm1f6+1) = zrx(nx,ky)
       aux(nxm1f6+2) = zrx(nx,jk)

 !     appel du sous programmes d inversion
 
       m1=9
       m2=9
       i19=19
             
        call bandec(a,ndima,m1,m2,i19,al,m1,in,dband_a)
        call banbks(a,ndima,m1,m2,i19,al,m1,in,aux)

!     transfert de la solution dans e.
 
       ik=0

       DO i=1,nxm1f6,6
         ik=ik+1
         ex(ik,ky)=(aux(i))
         ex(ik,jk)=(aux(i+1))
         ey(ik,ky)=(aux(i+2))
         ey(ik,jk)=(aux(i+3))
         ez(ik,ky)=(aux(i+4))
         ez(ik,jk)=(aux(i+5))
       ENDDO

       ex(nx,ky)=(aux(nxm1f6+1))
       ex(nx,jk)=(aux(nxm1f6+2))
       
       ENDDO     !  fin de la  boucle sur les ky
 

 !     transformees de fourier inverses pour obtenir le champ
!       call rpf2iv(ex(1,1),ex(npx+1,1),ny,n2x,1,npx)
!       call cpftv(ex(1,1),ex(npx+1,1),ny,n2x,1.,1,npx,cwk)
!       call rpf2iv(ey(1,1),ez(1,1),ny,n2x,1,nxm1)
!       call cpftv(ey(1,1),ez(1,1),ny,n2x,1.,1,nxm1,cwk)
      
       worktab1=real(ex,4)
       call rpf2iv(worktab1(1,1),worktab1(npx+1,1),ny,n2x,1,npx)
       call cpftv(worktab1(1,1),worktab1(npx+1,1),ny,n2x,1.,1,npx,cwk)
       ex= 0.
       ex=real(worktab1,8)
       
       worktab1=real(ey,4)
       worktab2=real(ez,4)
       call rpf2iv(worktab1(1,1),worktab2(1,1),ny,n2x,1,nxm1)
       call cpftv(worktab1(1,1),worktab2(1,1),ny,n2x,1.,1,nxm1,cwk)
       ey= 0.
       ez= 0.
       ey=real(worktab1,8)
       ez=real(worktab2,8)

 !     normalisation de e
 ! --- Simple terminaison de la transform�e de fourier inverse
       ex(1:nx,0:n1y)=ex(1:nx,0:n1y)*anmax
       ey(1:nxm1,0:n1y)=ey(1:nxm1,0:n1y)*anmax
       ez(1:nxm1,0:n1y)=ez(1:nxm1,0:n1y)*anmax

! ---  application des conditions aux limites r�fl�chissantes 
!       pour le champ e
! ---- Les champs tangentiels sont nuls
       IF( ibndc(1).EQ.-3 ) THEN
         DO  j=0,n1y
          ey(0,j)=0.
          ez(0,j)=0.
         ENDDO   
       ENDIF
 
       IF( ibndc(2).EQ.-3 ) THEN
         DO  j=0,n1y
          ey(nx,j)=0.
          ez(nx,j)=0.
         ENDDO   
       ENDIF    
       
       
       DO  i=1,nx
         ex(i,0)=ex(i,ny)
         ex(i,n1y)=ex(i,1)
         ey(i,0)=ey(i,ny)
         ey(i,n1y)=ey(i,1)
         ez(i,0)=ez(i,ny)
         ez(i,n1y)=ez(i,1)
       ENDDO
          
      
       IF(boundaries.EQ.1) THEN
         IF(ibndc(1).EQ.-2 .OR. ibndc(1).EQ.0) THEN
! ---- Calcul de ey(0,j+1/2), ez(0,j) au temps n+1      
          ey(0,2:n1y)= kebey1*ey(1,2:n1y) 
     &     + kebey2*(ex(1,2:n1y)-ex(1,1:ny))
     &     + kebey3*bz(1,2:n1y) + kebey4*(ebby(1,2:n1y)-ebby(0,2:n1y))
     &     + kebey2*(ebbx(1,2:n1y)-ebbx(1,1:ny))
     &     - keb_a*(eytm1(1,2:n1y)+eytm1(0,2:n1y))      
      
          ez(0,1:ny)= kebez1*ez(1,1:ny) 
     &     - keb_b*(eztm1(0,1:ny)+eztm1(1,1:ny))  
     &     + kebez2*by(1,1:ny) + kebez3*(ebbz(1,1:ny)-ebbz(0,1:ny))
      
! ---- p�riodicit� du champ
         ey(0,0)  =ey(0,ny)
         ey(0,1)  =ey(0,n1y)
         ez(0,0)  =ez(0,ny)      
         ez(0,n1y)=ez(0,1)
      
! ---- Injection laser implicite
         IF(ibndc(1).EQ.0) THEN
! ---- Injection du laser en E[yz]      
           CALL maxlim_2d1s2_nEB_implicit_step2(pasdt*(iter+0.5)) 
         ENDIF      
      
! ---- p�riodicit� du champ
         ey(0,0)  =ey(0,ny)
         ey(0,n1y)=ey(0,1)
         ez(0,0)  =ez(0,ny)      
         ez(0,n1y)=ez(0,1)
	ENDIF    
	
      ELSEIF(boundaries.EQ.2) THEN
      
         IF(ibndc(1).EQ.-2 .OR. ibndc(1).EQ.0) THEN
! ---- Calcul de ey(0,j+1/2), ez(0,j) au temps n+1      
          ey(0,2:n1y)= vex1*ey(1,2:n1y) 
     &     + vex2*(ex(1,2:n1y) - ex(1,1:ny))
     &     - vqx1*ebby(1,2:n1y) - ebby(0,2:n1y)
     &     - vqx2*(ebbx(1,2:n1y) - ebbx(1,1:ny))
     &     - vqx3*bz(1,2:n1y)  
       
          ez(0,1:ny)= vkebz1*ez(1,1:ny) 
     &     - vkebz2*(ebbz(1,1:ny) - ebbz(0,1:ny))   
     &     - vkebz3*(ebbz(0,1:ny) + ebbz(1,1:ny))
     &     - vkebz4*by(1,1:ny)
     
!          ez(0,1:ny)= fkebz1*ez(1,1:ny) 
!     &     - fkebz2*by(1,1:ny)
!     &     - fkebz3*(ebbz(1,1:ny)-ebbz(0,1:ny))

!          ez(0,1:ny)= ffkebz1*ez(1,1:ny) 
!     &     - ffkebz2*by(1,1:ny)
!     &     - ffkebz3*(ebbz(1,1:ny)-ebbz(0,1:ny))     
!     &     - ffkebz4*(eztm1(1,1:ny)-eztm1(0,1:ny))     
		
! ---- p�riodicit� du champ
         ey(0,0)  =ey(0,ny)
         ey(0,1)  =ey(0,n1y)
         ez(0,0)  =ez(0,ny)      
         ez(0,n1y)=ez(0,1)
      
! ---- Injection laser implicite
         IF(ibndc(1).EQ.0) THEN
! ---- Injection du laser en E[yz]      
           CALL maxlim_2d1s2_nEB_implicit_step2_var(pasdt*iter) 
         ENDIF      
      
! ---- p�riodicit� du champ
         ey(0,0)  =ey(0,ny)
         ey(0,n1y)=ey(0,1)
         ez(0,0)  =ez(0,ny)      
         ez(0,n1y)=ez(0,1)
	ENDIF  	
	
       ELSEIF(boundaries.EQ.3) THEN       
         IF(ibndc(1).EQ.-2 .OR. ibndc(1).EQ.0) THEN
           ey(0,0:n1y)= -COS(y_ang(1))*bz_tn(1,0:n1y)
           ez(0,0:n1y)=  by_tn(1,0:n1y)/COS(y_ang(1))
         ENDIF
	 
! ---- couplage implicite/explicite
         IF(ibndc(1).EQ.0) THEN
! ---- Injection du laser en E[yz]      
           CALL maxlim_2d1s2_nEB(pasdt*(iter)) 
! ---- p�riodicit� Ey, Ez     
           ey(nulx_inj,n1y)=ey(nulx_inj,1)
           ey(nulx_inj,0)  =ey(nulx_inj,ny)
           ez(nulx_inj,n1y)=ez(nulx_inj,1)
           ez(nulx_inj,0)  =ez(nulx_inj,ny)
         ENDIF	 
       ENDIF
      
      
      IF(boundaries.EQ.1) THEN
        IF(ibndc(2).eq.-2) THEN
! ---- conditions absorbantes au bord droit       
         ey(nx,2:n1y) = - kebr1*ey(nxm1,2:n1y) 
     &      + 2.*kebr1*COS(scat_angle)*dtsdx*ey(nxm1,2:n1y)
     &    + 2.*kebr1*COS(scat_angle)*dtsdy*(ex(nx,2:n1y)-ex(nx,1:ny))
     &    - kebr1*(eytm1(nx,2:n1y)+eytm1(nxm1,2:n1y))
     &    + 4.*kebr1*COS(scat_angle)*bz(nx,2:n1y)
     &    - 2.*kebr1*COS(scat_angle)*dtsdx*
     &                  (ebby(nx,2:n1y)-ebby(nxm1,2:n1y))
     &    + 2.*kebr1*COS(scat_angle)*dtsdy*
     &                  (ebbx(nx,2:n1y)-ebbx(nx,1:ny))
     
        ez(nx,1:ny) = 
     &      kebr2*(2.*dtsdx/COS(scat_angle) -1.)*ez(nxm1,1:ny)
     &    - kebr2*2.*dtsdx/COS(scat_angle)*
     &                   (ebbz(nx,1:ny)-ebbz(nxm1,1:ny))
     &    - kebr2*(eztm1(nx,1:ny)+eztm1(nxm1,1:ny))
     &    - kebr2*4./COS(scat_angle)*by(nx,1:ny) 
           
! ---- p�riodicit� du champ
         ey(nx,0)  =ey(nx,ny)
         ey(nx,1)  =ey(nx,n1y)
         ez(nx,0)  =ez(nx,ny)      
         ez(nx,n1y)=ez(nx,1)      
        ENDIF
	
      ELSEIF(boundaries.EQ.2) THEN
        IF(ibndc(2).eq.-2) THEN
! ---- conditions absorbantes au bord droit    	
	ey(nx,2:n1y) = veyr1*ey(nxm1,2:n1y)
     &   + veyr2*(ex(nx,2:n1y) - ex(nx,1:ny))
     &   - ebby(nx,2:n1y)
     &   + vqxr1*ebby(nxm1,2:n1y)
     &   + vqxr2*(ebbx(nx,2:n1y) - ebbx(nx,1:ny))
     &   + vqxr3*bz(nx,2:n1y)

        ez(nx,1:ny) = vezr1*ez(nxm1,1:ny)
     &   - ebbz(nx,1:ny)
     &   + vqzr1*ebbz(nxm1,1:ny)
     &   + vqzr2*by(nx,1:ny)
     
! ---- p�riodicit� du champ
         ey(nx,0)  =ey(nx,ny)
         ey(nx,1)  =ey(nx,n1y)
         ez(nx,0)  =ez(nx,ny)      
         ez(nx,n1y)=ez(nx,1)	
	ENDIF
      ENDIF
      

       DO  i=1,nx
         ex(i,0)=ex(i,ny)
         ex(i,n1y)=ex(i,1)
         ey(i,0)=ey(i,ny)
         ey(i,n1y)=ey(i,1)
         ez(i,0)=ez(i,ny)
         ez(i,n1y)=ez(i,1)
       ENDDO

 !     calcul du residu
 !
       DO  j=1,ny
         DO  i=1,nx
       zetax(i,j)=qx(i,j)-ex(i,j)-xxx(i,j)*ex(i,j)                        
     &                -d11*((ey(i,j+1)-ey(i-1,j+1))-(ey(i,j)-ey(i-1,j)))   
     &                +d12*(ex(i,j+1)-2.*ex(i,j)+ex(i,j-1))                
     &          -0.25*(xxy(i-1,j)*(ey(i-1,j+1)+ey(i-1,j))                  
     &                +xxy(i,j)*(ey(i,j+1)+ey(i,j)))                       
     &         -0.5*(xxz(i-1,j)*ez(i-1,j)+xxz(i,j)*ez(i,j))                
     &       +dts2dy*(zzx(i,j+1)*ex(i,j+1)-zzx(i,j-1)*ex(i,j-1)            
     &               +zzy(i,j+1)*ey(i,j+1)-zzy(i,j)*ey(i,j)                
     &               +zzy(i-1,j+1)*ey(i-1,j+1)-zzy(i-1,j)*ey(i-1,j))       
     &       +dts4dy*(zzz(i,j+1)*ez(i,j+1)-zzz(i,j-1)*ez(i,j-1)            
     &               +zzz(i-1,j+1)*ez(i-1,j+1)-zzz(i-1,j-1)*ez(i-1,j-1))
        ENDDO
      ENDDO
 
       zetax(n1x,1:ny)=0.

 
       DO  j=1,ny
         DO  i=1,nxm1
       zetay(i,j)=qy(i,j)-ey(i,j)-xyy(i,j)*ey(i,j)                        
     &                +d13*(ey(i+1,j)-2.*ey(i,j)+ey(i-1,j))               
     &              -d11*((ex(i+1,j)-ex(i+1,j-1))-(ex(i,j)-ex(i,j-1)))  
     &           -0.25*(xyx(i,j)*(ex(i+1,j)+ex(i,j))                      
     &                  +xyx(i,j-1)*(ex(i,j-1)+ex(i+1,j-1)))               
     &             -0.5*(xyz(i,j)*ez(i,j)+xyz(i,j-1)*ez(i,j-1))            
     &         -dts2dx *(zzx(i+1,j)*ex(i+1,j)-zzx(i,j)*ex(i,j)             
     &                  +zzx(i+1,j-1)*ex(i+1,j-1)-zzx(i,j-1)*ex(i,j-1)     
     &                  +zzy(i+1,j)*ey(i+1,j)-zzy(i-1,j)*ey(i-1,j))        
     &          -dts4dx*(zzz(i+1,j)*ez(i+1,j)-zzz(i-1,j)*ez(i-1,j)         
     &             +zzz(i+1,j-1)*ez(i+1,j-1)-zzz(i-1,j-1)*ez(i-1,j-1))
 !
       zetaz(i,j)=qz(i,j)-ez(i,j)-xzz(i,j)*ez(i,j)                        
     &                 +d13*(ez(i+1,j)-2.*ez(i,j)+ez(i-1,j))               
     &                 +d12*(ez(i,j+1)-2.*ez(i,j)+ez(i,j-1))               
     &             -0.5*(xzx(i,j)*(ex(i+1,j)+ex(i,j))                      
     &                  +xzy(i,j)*(ey(i,j+1)+ey(i,j)))                     
     &          +dtsdx *(zyx(i+1,j)*ex(i+1,j)-zyx(i,j)*ex(i,j))            
     &      +dts4dx*(zyy(i+1,j+1)*ey(i+1,j+1)-zyy(i-1,j+1)*ey(i-1,j+1) 
     &                  +zyy(i+1,j)*ey(i+1,j)-zyy(i-1,j)*ey(i-1,j))        
     &          +dts2dx*(zyz(i+1,j)*ez(i+1,j)-zyz(i-1,j)*ez(i-1,j))        
     &      -dts4dy*(zxx(i+1,j+1)*ex(i+1,j+1)-zxx(i+1,j-1)*ex(i+1,j-1) 
     &                  +zxx(i,j+1)*ex(i,j+1)-zxx(i,j-1)*ex(i,j-1))        
     &           -dtsdy*(zxy(i,j+1)*ey(i,j+1)-zxy(i,j)*ey(i,j))            
     &          -dts2dy*(zxz(i,j+1)*ez(i,j+1)-zxz(i,j-1)*ez(i,j-1))
         ENDDO
       ENDDO
 !
 !     test de convergence
 !
       amx=0.
       amy=0.
       amz=0.
       
       DO j=1,ny
         DO  i=1,nxm1
         amx=max(amx,abs(zetax(i,j)))
         amy=max(amy,abs(zetay(i,j)))
         amz=max(amz,abs(zetaz(i,j)))
         ENDDO
       amx=max(amx,abs(zetax(nx,j)))
       ENDDO
  
      if(amqx.gt.1e-8) amx=amx/amqx
      if(amqy.gt.1e-8) amy=amy/amqy
      if(amqz.gt.1e-3) amz=amz/amqz
!       amx=amx/amqmax
!       amy=amy/amqmax
!       amz=amz/amqmax
       write(fmsgo,*) ' iter_golub = ',iter_golub,' amx = ',amx, 
     &            ' amy = ',amy, ' amz = ',amz
 !     if(amx.lt.epsmax.and.amy.lt.epsmax.and.amz.lt.epsmax) goto 2000
       if(amx.lt.epsmax) then
       if(amy.lt.epsmax) then
       if(amz.lt.epsmax) then
       goto 2000
       
       endif
       endif
       endif
       
       if(iter_golub.eq.nbmax) goto 1010
 
 !     preparation de l iteration suivante 
!       call cpftv(zetax(1,1),zetax(npx+1,1),ny,n2x,-1.,1,npx,cwk)
!       call rpft2v(zetax(1,1),zetax(npx+1,1),ny,n2x,1,npx)
!       call cpftv(zetay(1,1),zetaz(1,1),ny,n2x,-1.,1,nxm1,cwk)
!       call rpft2v(zetay(1,1),zetaz(1,1),ny,n2x,1,nxm1)
       
       
       worktab1=real(zetax,4)
       call cpftv(worktab1(1,1),worktab1(npx+1,1),ny,n2x,-1.,1,npx,cwk)
       call rpft2v(worktab1(1,1),worktab1(npx+1,1),ny,n2x,1,npx)
       zetax=0.
       zetax=real(worktab1,8)
       
       worktab1=real(zetay,4)
       worktab2=real(zetaz,4)
       call cpftv(worktab1(1,1),worktab2(1,1),ny,n2x,-1.,1,nxm1,cwk)
       call rpft2v(worktab1(1,1),worktab2(1,1),ny,n2x,1,nxm1)
       zetay=0.
       zetaz=0.
       zetay=real(worktab1,8)
       zetaz=real(worktab2,8)
       
 
       DO j=1,ny
         DO  i=1,nxm1
         zrx(i,j)=zrx(i,j)+zetax(i,j)
         zry(i,j)=zry(i,j)+zetay(i,j)
         zrz(i,j)=zrz(i,j)+zetaz(i,j)
         ENDDO
       
       zrx(nx,j)=zrx(nx,j)+zetax(nx,j)
       ENDDO
 
       ENDDO      !  fin de la boucle iterative
  
1010   write(fmsgo,*) ' nbmax = ',nbmax,' amx = ',amx,' amy = ',amy,           
     &             ' amz = ',amz
       write(fmsgo,*) ' maxwell : pas de convergence  en nbmax ',
     &    'iterations = ',nbmax
       write(fmsgo,*) '         *** stop ***'
       goto 2010
       
! ---- l'instruction continue est facultative       
2000   continue
       write(fmsgo,*) 'maxwell : convergence en ',iter_golub,       
     &      ' iterations ','pour eps = ',epsmax
     
2010   continue


!       IF(ibndc(1).EQ.0) THEN
! ---- Injection du laser en E[yz]      
!!         CALL maxlim_2d1s2_nEB(pasdt*(iter)) 	
!         CALL maxlim_2d1s2_E(pasdt*(iter)) 	
!!         CALL maxlim_huygens_E(pasdt*(iter)) 	
! ---- p�riodicit� Ey, Ez     
!         ey(nulx_inj,n1y)=ey(nulx_inj,1)
!         ey(nulx_inj,0)  =ey(nulx_inj,ny)
!         ez(nulx_inj,n1y)=ez(nulx_inj,1)
!         ez(nulx_inj,0)  =ez(nulx_inj,ny)
!       ENDIF
    
      
 !     calcul du champ b au temps n+1/2 
       emoyx=0.5_8*(ex+ebbx)
       emoyy=0.5_8*(ey+ebby)
       emoyz=0.5_8*(ez+ebbz)

       bxold(:,:)=bx(:,:)
       byold(:,:)=by(:,:)
       bzold(:,:)=bz(:,:)       

       DO j=1,ny
         DO i=1,nx
          bx(i,j)=bx(i,j)-dtsdy*(emoyz(i,j)-emoyz(i,j-1))
         ENDDO
       ENDDO	

       DO j=1,ny
         DO i=1,nx
          by(i,j)=by(i,j)+dtsdx*(emoyz(i,j)-emoyz(i-1,j))
          bz(i,j)=bz(i,j)-dtsdx*(emoyy(i,j)-emoyy(i-1,j))                        
     &               +dtsdy*(emoyx(i,j)-emoyx(i,j-1))
         ENDDO
       ENDDO       
      
! ---- Conditions au limite sur B[yz] en xmin
       SELECT CASE(ibndc(1))       
       CASE(0)
        DO j=1,ny
          bx(0,j)=bx(0,j)-dtsdy*(emoyz(0,j)-emoyz(0,j-1))
        ENDDO
     
! ---- p�riodicit� Bx 
         bx(0,n1y)=bx(0,1)
         bx(0,0)  =bx(0,ny)
    
       CASE(-2)
        DO j=1,ny
          bx(0,j)=bx(0,j)-dtsdy*(emoyz(0,j)-emoyz(0,j-1))
        ENDDO
       
! ---- p�riodicit� Bx 
         bx(0,n1y)=bx(0,1)
         bx(0,0)  =bx(0,ny)
       
       CASE(-3)       
        DO j=0,n1y
          bx(0,j)=0.
        ENDDO      
       
       CASE DEFAULT

       END SELECT
 
 !     conditions aux limites de b 
       DO i=1,nx
         bx(i,0)  =bx(i,ny)
         bx(i,n1y)=bx(i,1)
         by(i,0)  =by(i,ny)
         by(i,n1y)=by(i,1)
         bz(i,0)  =bz(i,ny)
         bz(i,n1y)=bz(i,1)
       ENDDO       
     
! --- + Diagnostics poynting a chaque pas de temps : diag_poynting
        CALL diag_poynting(pasdt,0.5*(bxold(:,:)+bx(:,:)),
     &                           0.5*(byold(:,:)+by(:,:)),
     &                           0.5*(bzold(:,:)+bz(:,:)) )
     
 !
 !     on sauve b au temps n+1/2 dans zeta

       zetax=bx
       zetay=by
       zetaz=bz

 !     on extrapole b au temps n+1
 !
       DO  j=1,ny
         DO  i=1,nx
         bx(i,j)=bx(i,j)-dts2dy*(ez(i,j)-ez(i,j-1))
         by(i,j)=by(i,j)+dts2dx*(ez(i,j)-ez(i-1,j))
         bz(i,j)=bz(i,j)-dts2dx*(ey(i,j)-ey(i-1,j))                         
     &                +dts2dy*(ex(i,j)-ex(i,j-1))
         ENDDO
       ENDDO
  
 !
 !     conditions aux limites de b
 !
 !dir$ ivdep
       DO  i=0,n1x
         bx(i,0)=bx(i,ny)
         bx(i,n1y)=bx(i,1)
         by(i,0)=by(i,ny)
         by(i,n1y)=by(i,1)
         bz(i,0)=bz(i,ny)
         bz(i,n1y)=bz(i,1)
       ENDDO      
      
 !
       return
       
!      ______________________       
       END SUBROUTINE maxwel



       
       SUBROUTINE pot2di
! ======================================================================
! 
! 
!
!
!
! ======================================================================       
       
       USE champs
       USE domaines
       USE temps
       USE erreurs
 
 
       IMPLICIT NONE
 
 !     version avec des conditions aux limites
 !     ouverte en x periodique en y
 !     en x phi(0)=phi(nx)=0.(phi=0. sur les parois)
 

       REAL(kind=kr), DIMENSION(nxm1f2)    ::  aux
       REAL(kind=kr), DIMENSION(7,nxm1f2)  ::  a
       REAL(kind=kr), DIMENSION(3,nxm1f2)  ::  al
       REAL(kind=kr), DIMENSION(nx,2)      ::  axx
       REAL(kind=kr), DIMENSION(nxm1f2)    ::  in
      
       REAL(kind=kr), DIMENSION(1:n1x)    ::   c1, c2, c3, c4 

       REAL(kind=4), DIMENSION(0:n1x,0:n1y)  ::   worktab1

!       REAL(kind=4), DIMENSION(1:n1x)   ::  wkin1 
!       REAL(kind=4), DIMENSION(1:nx,2)  ::  wkin2
!       REAL(kind=4), DIMENSION(0:n1x, 0:n1y)  ::  wkin3, wkout

       REAL(kind=kr)     :: dband_a, sr, tz
 
       INTEGER    :: i, j, ky, iter_golub, jk, il, ik, m1, m2, i7
!       INTEGER    ::  npx
              
       write(fmsgo,*) ' Maxima de : xxx0,xxy0,xyx0,xyy0 = ',       
     &       MAXVAL(xxx0(:)), MAXVAL(xxy0(:)),
     &       MAXVAL(xyx0(:)), MAXVAL(xyy0(:))
 
       write(fmsgo,*) ' Maxima de : zxx0,zxy0,zxz0 = ',       
     &       MAXVAL(zxx0(:)), MAXVAL(zxy0(:)), MAXVAL(zxz0(:))

       write(fmsgo,*) ' Maxima de : zyx0,zyy0,zyz0 = ',       
     &       MAXVAL(zyx0(:)), MAXVAL(zyy0(:)), MAXVAL(zyz0(:))
     
       write(fmsgo,*) ' Maxima de : zzx0,zzy0,zzz0 = ',       
     &       MAXVAL(zzx0(:)), MAXVAL(zzy0(:)), MAXVAL(zzz0(:)) 
 
!       npx=nxm1s2+1
 
       phi=0.
       write(fmsgo,*) 'pot : nxm1f2,nxm1s2 : ',nxm1f2,nxm1s2
 !     calcul des coefficients de la matrice
 !
       DO i=1,nx
         c1(i)=(1.+xxx0(i))*dx2i
         c2(i)=(1.+xyy0(i))*dy2i*2
         c3(i)=(xxy0(i+1)+xyx0(i))*dxdyi4*2.
         c4(i)=(xxy0(i-1)+xyx0(i))*dxdyi4*2.
       ENDDO
 !
 !     calcul du maximun de s
 !
       sr=0.
       DO j=1,ny
         DO i=1,nxm1
         sr=max(sr,abs(s(i,j)))
         ENDDO
       ENDDO
 !
 !     calcul du terme source de la methode iterative pour le demarrage
 
       DO  j=1,ny
         DO  i=1,nxm1
        z(i,j)=+dx2i*((xxx(i+1,j)-xxx0(i+1))*(phi(i+1,j)-phi(i,j))         
     &               -(xxx(i,j)-xxx0(i))*(phi(i,j)-phi(i-1,j)))            
     &         +dy2i*((xyy(i,j+1)-xyy0(i))*(phi(i,j+1)-phi(i,j))           
     &               -(xyy(i,j)-xyy0(i))*(phi(i,j)-phi(i,j-1)))            
     &    +dxdyi4*((xxy(i+1,j)-xxy0(i+1))*(phi(i+1,j+1)-phi(i+1,j-1))      
     &            -(xxy(i-1,j)-xxy0(i-1))*(phi(i-1,j+1)-phi(i-1,j-1))      
     &            +(xyx(i,j+1)-xyx0(i))*(phi(i+1,j+1)-phi(i-1,j+1))        
     &            -(xyx(i,j-1)-xyx0(i))*(phi(i+1,j-1)-phi(i-1,j-1)))       
     &         +s(i,j)
         ENDDO
       ENDDO

!     transformee de fourier du z
!       call cpftv(z(1,1),z(2,1),ny,n2x,-1.,2,nxm1s2,cwk)
!       call rpft2v(z(1,1),z(2,1),ny,n2x,2,nxm1s2)

       worktab1=real(z,4) 
       call cpftv(worktab1(1,1),worktab1(2,1),ny,n2x,-1.,2,nxm1s2,cwk)
       call rpft2v(worktab1(1,1),worktab1(2,1),ny,n2x,2,nxm1s2)
       z=0.
       z=real(worktab1,8)
       
       DO iter_golub=1,nbpot  !     boucle sur les iterations
      
 !     calcul du mode 0 et nys2 par resolution d'une matrice
 !     tridiagonale

       DO  i=1,nxm1
         axx(i,1)=c1(i)+c1(i+1)
         axx(i,2)=c1(i)+c1(i+1)-c2(i)*csym1(nys2p1)
       ENDDO

! --- affichage phi mode 0, phi mode nys2 avant tridia
       call tridia(c1,axx(1,1),phi(1,1),z(1,1))
       call tridia(c1,axx(1,2),phi(1,nys2p1),z(1,nys2p1))
       
 !     on range les coefficients de la matrice dans a sous forme bande
 
 !     la premiere inconnue est phi(1)
 !     les quatres premieres lignes sont differentes des autres
  
       jk=n1y

       DO ky=2,nys2   
         jk=jk-1
         a=0.
         al=0.
 
 !     les deux premieres lignes (phi(0)=0.)
         il=1
         a(4,1)= c1(il+1)+c1(il)-c2(il)*csym1(ky)
         a(6,1)=-c1(il+1)
         a(7,1)= c3(il)*sny(ky)
         a(4,2)= a(4,1)
         a(5,2)=-a(7,1)
         a(6,2)= a(6,1)
 !
 !     les deux dernieres lignes (phi(nx)=0.))
         il=nxm1
         a(2,nxm1f2-1)=-c1(il)
         a(3,nxm1f2-1)=-c4(il)*sny(ky)
         a(4,nxm1f2-1)= c1(il)+c1(il+1)-c2(il)*csym1(ky)
         a(1,nxm1f2  )=-a(3,nxm1f2-1)
         a(2,nxm1f2  )=-c1(il)
         a(4,nxm1f2  )= a(4,nxm1f2-1)
 
         il=1
         DO i=3,nxm1f2-2,2    !     lignes generales
           il=il+1
           a(2,i)=-c1(il)
           a(3,i)=-c4(il)*sny(ky)
           a(4,i)= c1(il)+c1(il+1)-c2(il)*csym1(ky)
           a(6,i)=-c1(il+1)
           a(7,i)= c3(il)*sny(ky)
 
           a(1,i+1)=-a(3,i)
           a(2,i+1)=-c1(il)
           a(4,i+1)= a(4,i)
           a(5,i+1)=-a(7,i)
           a(6,i+1)=-c1(il+1)     
         ENDDO

 !     terme source dans aux
 !     --------------------
         ik=-1
         DO  i=1,nxm1
           ik=ik+2
           aux(ik)=z(i,ky)
           aux(ik+1)=z(i,jk)
         ENDDO

         m1=3
         m2=3
         i7=7
	 
         call bandec(a,nxm1f2,m1,m2,i7,al,m1,in,dband_a)
         call banbks(a,nxm1f2,m1,m2,i7,al,m1,in,aux)
      
 !     on range phi au bon point 
         ik=-1
         DO i=1,nxm1
           ik=ik+2
           phi(i,ky)=aux(ik)
           phi(i,jk)=aux(ik+1)
         ENDDO
 
 ! --- fin de la boucle sur les modes ky
       ENDDO

 !     transformee fourier inverse de phi
!       call rpf2iv(phi(1,1),phi(2,1),ny,n2x,2,nxm1s2)
!       call cpftv(phi(1,1),phi(2,1),ny,n2x,1.,2,nxm1s2,cwk)
 
       worktab1=real(phi,4)
       call rpf2iv(worktab1(1,1),worktab1(2,1),ny,n2x,2,nxm1s2)
       call cpftv(worktab1(1,1),worktab1(2,1),ny,n2x,1.,2,nxm1s2,cwk)
       phi= 0.
       phi=real(worktab1,8)
 
 !     normalisation du phi et applications des conditions aux limites

       phi=phi*anpot

 !     conditions aux limites
 
       phi(0,1:ny)=0.
       phi(nx,1:ny)=0.

 
 !     periodicite du potentiel en y
 !     ------------------------
       phi(0:nx,0)=phi(0:nx,ny)
       phi(0:nx,n1y)=phi(0:nx,1)

 !     calcul du residu
 !
       DO j=1,ny
         DO i=1,nxm1
         zeta(i,j)=+dx2i*((1.+xxx(i+1,j))*(phi(i+1,j)-phi(i,j))           
     &                 -(1. +xxx(i  ,j))*(phi(i,j)-phi(i-1,j)))            
     &            +dy2i*((1.+xyy(i,j+1))*(phi(i,j+1)-phi(i,j))            
     &                 -(1. +xyy(i,j  ))*(phi(i,j)-phi(i,j-1)))           
     &            +dxdyi4*(  xxy(i+1,j )*(phi(i+1,j+1)-phi(i+1,j-1))      
     &                      -xxy(i-1,j )*(phi(i-1,j+1)-phi(i-1,j-1))      
     &                      +xyx(i ,j+1)*(phi(i+1,j+1)-phi(i-1,j+1))      
     &                     -xyx(i ,j-1)*(phi(i+1,j-1)-phi(i-1,j-1))) 
     &         +s(i,j)
         ENDDO
       ENDDO


       tz=maxval(abs(zeta(1:nxm1,1:ny)))
       IF(sr.GT.1e-8) tz=tz/sr

       if(tz.lt.epspot) goto 2000
       write(fmsgo,*) iter_golub,'   tz = ',tz,'   sr = ',sr
 
 !     calcul du terme source pour l iteration suivante
 !
 !
 !     transformee de fourier du z
 !
!       call cpftv (zeta(1,1),zeta(2,1),ny,n2x,-1.,2,nxm1s2,cwk)
!       call rpft2v(zeta(1,1),zeta(2,1),ny,n2x,2,nxm1s2)

       worktab1=real(zeta,4)
       call cpftv (worktab1(1,1),worktab1(2,1),ny,n2x,-1.,2,nxm1s2,cwk)
       call rpft2v(worktab1(1,1),worktab1(2,1),ny,n2x,2,nxm1s2)
       zeta= 0.
       zeta=real(worktab1,8)
 
       DO ky=1,ny
         z(1:nxm1,ky)=z(1:nxm1,ky)+zeta(1:nxm1,ky)
       ENDDO

! --- fin de la boucle it�rative Concus et Golub   
       ENDDO
 !
       write(fmsgo,*) 'pas convergence en ',nbpot,
     &     ' iterations, eps=',tz
       return
       
2000   continue
       write(fmsgo,*) ' '
       write(fmsgo,*) 'convergence en ',iter_golub,
     &     ' iterations, eps=',tz
       write(fmsgo,*) ' '
       
       return       
!      _____________________       
       END SUBROUTINE pot2di
       
       
       
       
       SUBROUTINE pot2d_cflout
! ======================================================================
!  A deplacer apres Final Push !
!  Cette deuxieme correction ne fonctionne pas !
! 
!! --- + 2e correction de Poisson si particules tq c*Dt>Dx
!       IF(cfl_out.EQ.1) THEN       
!        rho_esp_2(:,:,iesp) = 0.
!	
!! ----- calcul de rho_esp_2(:,:,iesp) effectif
!        DO ip=1,nbpa(iesp)  
!          SELECT CASE(ordre_interp)
!           CASE(1)
!           CALL ponder_ordre1(ip, iesp, ponpx, ponpy, ipx, ipy)
!
!           CASE(2)
!           CALL ponder_ordre2(ip, iesp, ponpx, ponpy, ipx, ipy)
!
!           CASE(3)
!           CALL ponder_ordre3(ip, iesp, ponpx, ponpy, ipx, ipy)
!
!           CASE(4)
!           CALL ponder_ordre4(ip, iesp, ponpx, ponpy, ipx, ipy)
!
!           CASE DEFAULT
!           write(fmsgo,*) 'pousseurs.f : valeur de ordre_interp ',
!     &       'incorrecte '
!          END SELECT 
!	 
!         aux_rh = partic(a_po,ip,iesp)
!
!         CALL projpa_2dx3dv_rho(iesp, ponpx, ponpy,   
!     &                ipx, ipy, aux_rh)         
!        ENDDO
! ------- normalisation des sources � cause de partic(a_po_ip,iesp)	 
!        rho_esp_2(:,:,iesp) = rho_esp_2(:,:,iesp)*dxi*dyi*charge(iesp)
!       
!         IF(iesp.EQ.nesp_p) THEN    
!           CALL grimax        
!           CALL pot2d_cflout	 
!           CALL griuni	 
!	 ENDIF	 
!       ENDIF
!
!
!
! ======================================================================       
       
       USE champs
       USE domaines
       USE particules_klder       
       USE temps
       USE erreurs
 
 
       IMPLICIT NONE
 
 !     version avec des conditions aux limites
 !     ouverte en x periodique en y
 !     en x phi(0)=phi(nx)=0.(phi=0. sur les parois)
 

       REAL(kind=kr), DIMENSION(nxm1f2)    ::  aux
       REAL(kind=kr), DIMENSION(7,nxm1f2)  ::  a
       REAL(kind=kr), DIMENSION(3,nxm1f2)  ::  al
       REAL(kind=kr), DIMENSION(nx,2)      ::  axx
       REAL(kind=kr), DIMENSION(nxm1f2)    ::  in
       
       REAL(kind=kr), DIMENSION(1:n1x)    ::   c1  
      
       REAL(kind=4), DIMENSION(0:n1x,0:n1y)  ::   worktab1

       REAL(kind=kr), DIMENSION(0:n1x, 0:n1y)  ::  rho, phico, 
     &    zco, zetaco  

       REAL(kind=kr)     :: dband_a, sr, tz
 
       INTEGER    :: i, j, iesp 
       INTEGER    :: ky, iter_golub, jk, il, ik, m1, m2, i7
       
!       INTEGER    ::  npx


       write(fmsgo,*) ' -----------------------------------------' 
       write(fmsgo,*) '     D�but 2� correction de Poisson       '
       write(fmsgo,*) ' -----------------------------------------'
       write(fmsgo,*) ' pot : xxx0,xxy0,xyx0,xyy0 = ',       
     &       xxx0(nxs2),xxy0(nxs2),xyx0(nxs2),xyy0(nxs2)
 
 
!       npx=nxm1s2+1
 
       phico(:,:) = 0. ; zco(:,:) = 0.
       zetaco(:,:) = 0.
       
       
       write(fmsgo,*) 'pot : nxm1f2,nxm1s2 : ',nxm1f2,nxm1s2
 !     calcul des coefficients de la matrice
 !

        rho(:,:)=0.
        DO  j=0,n1y
          DO  i=0,n1x
            DO iesp=1,nesp_p 
             rho(i,j)= rho(i,j) + rho_esp_2(i,j,iesp)
            ENDDO
	  ENDDO
        ENDDO   
	
       DO i=1,nx
         c1(i)=dx2i
       ENDDO 

 !
 !     calcul du maximun de rho
 !
       sr=0.
       DO j=1,ny
         DO i=1,nxm1
         sr=max(sr,abs(rho(i,j)))
         ENDDO
       ENDDO
 !
 !     calcul du terme source de la methode iterative pour le demarrage
 
       DO  j=1,ny
         DO  i=1,nxm1
          zco(i,j)= dxi*(ex(i+1,j)-ex(i,j))         
     &        + dyi*(ey(i,j+1)-ey(i,j))            
     &        - rho(i,j)
         ENDDO
       ENDDO

!     transformee de fourier du z
!       call cpftv(z(1,1),z(2,1),ny,n2x,-1.,2,nxm1s2,cwk)
!       call rpft2v(z(1,1),z(2,1),ny,n2x,2,nxm1s2)

       worktab1=real(zco,4) 
       call cpftv(worktab1(1,1),worktab1(2,1),ny,n2x,-1.,2,nxm1s2,cwk)
       call rpft2v(worktab1(1,1),worktab1(2,1),ny,n2x,2,nxm1s2)
       zco=0.
       zco=real(worktab1,8)
       
       DO iter_golub=1,nbpot  !     boucle sur les iterations
      
 !     calcul du mode 0 et nys2 par resolution d'une matrice
 !     tridiagonale

       DO  i=1,nxm1
! -----  terme diagonal mode 0       
         axx(i,1)=-2.*dx2i
! -----  terme diagonal mode ny/2	 
         axx(i,2)=2.*dy2i*csym1(nys2p1)-2.*dx2i
       ENDDO

! --- affichage phi mode 0, phi mode nys2 avant tridia
       call tridia(c1,axx(1,1),phico(1:nxm1,1),zco(1,1))
       call tridia(c1,axx(1,2),phico(1:nxm1,nys2p1),zco(1,nys2p1))
       
 !     on range les coefficients de la matrice dans a sous forme bande
 
 !     la premiere inconnue est phi(1)
 !     les quatres premieres lignes sont differentes des autres
  
       jk=n1y

       DO ky=2,nys2   
         jk=jk-1
         a=0.
         al=0.
 
 !     les deux premieres lignes (phi(0)=0.)
         il=1
         a(4,1)= 2.*dy2i*csym1(ky) - 2.*dx2i
         a(6,1)= dx2i
	 
         a(4,2)= a(4,1)
         a(6,2)= a(6,1)
 !
 !     les deux dernieres lignes (phi(nx)=0.))
         il=nxm1
         a(2,nxm1f2-1)= dx2i
         a(4,nxm1f2-1)= 2.*dy2i*csym1(ky) - 2.*dx2i
	 
         a(2,nxm1f2  )= a(2,nxm1f2-1)
         a(4,nxm1f2  )= a(4,nxm1f2-1)
 
         il=1
         DO i=3,nxm1f2-2,2    !     lignes generales
           il=il+1
           a(2,i)= dx2i
           a(4,i)= 2.*dy2i*csym1(ky) - 2.*dx2i
           a(6,i)= dx2i
 
           a(2,i+1)= dx2i
           a(4,i+1)= 2.*dy2i*csym1(ky) - 2.*dx2i
           a(6,i+1)= dx2i     
         ENDDO

 !     terme source dans aux
 !     --------------------
         ik=-1
         DO  i=1,nxm1
           ik=ik+2
           aux(ik)=zco(i,ky)
           aux(ik+1)=zco(i,jk)
         ENDDO

         m1=3
         m2=3
         i7=7
	 
         call bandec(a,nxm1f2,m1,m2,i7,al,m1,in,dband_a)
         call banbks(a,nxm1f2,m1,m2,i7,al,m1,in,aux)
      
 !     on range phi au bon point 
         ik=-1
         DO i=1,nxm1
           ik=ik+2
           phico(i,ky)=aux(ik)
           phico(i,jk)=aux(ik+1)
         ENDDO
 
 ! --- fin de la boucle sur les modes ky
       ENDDO

 !     transformee fourier inverse de phi
!       call rpf2iv(phi(1,1),phi(2,1),ny,n2x,2,nxm1s2)
!       call cpftv(phi(1,1),phi(2,1),ny,n2x,1.,2,nxm1s2,cwk)
 
       worktab1=real(phico,4)
       call rpf2iv(worktab1(1,1),worktab1(2,1),ny,n2x,2,nxm1s2)
       call cpftv(worktab1(1,1),worktab1(2,1),ny,n2x,1.,2,nxm1s2,cwk)
       phico= 0.
       phico=real(worktab1,8)
 
 !     normalisation du phi et applications des conditions aux limites

       phico=phico*anpot

 !     conditions aux limites
 
       phico(0,1:ny)=0.
       phico(nx,1:ny)=0.

 
 !     periodicite du potentiel en y
 !     ------------------------
       phico(0:nx,0)=phico(0:nx,ny)
       phico(0:nx,n1y)=phico(0:nx,1)

 !     calcul du residu
 !
       DO j=1,ny
         DO i=1,nxm1
         zetaco(i,j)= 
     &      dx2i*(2.*phico(i,j)-phico(i+1,j)-phico(i-1,j)) +
     &      dy2i*(2.*phico(i,j)-phico(i,j+1)-phico(i,j-1)) +
     &      dxi*(ex(i+1,j)-ex(i,j)) + dyi*(ey(i,j+1)-ey(i,j))
     &      - rho(i,j)
         ENDDO
       ENDDO


       tz=maxval(abs(zetaco(1:nxm1,1:ny)))
       IF(sr.GT.1e-8) tz=tz/sr

       if(tz.lt.epspot) goto 2000
       write(fmsgo,*) iter_golub,'   tz = ',tz,'   sr = ',sr
 
 !     calcul du terme source pour l iteration suivante
 !
 !
 !     transformee de fourier du z
 !
!       call cpftv (zeta(1,1),zeta(2,1),ny,n2x,-1.,2,nxm1s2,cwk)
!       call rpft2v(zeta(1,1),zeta(2,1),ny,n2x,2,nxm1s2)

       worktab1=real(zetaco,4)
       call cpftv (worktab1(1,1),worktab1(2,1),ny,n2x,-1.,2,nxm1s2,cwk)
       call rpft2v(worktab1(1,1),worktab1(2,1),ny,n2x,2,nxm1s2)
       zetaco= 0.
       zetaco=real(worktab1,8)
 
       DO ky=1,ny
         zco(1:nxm1,ky)=zco(1:nxm1,ky)+zetaco(1:nxm1,ky)
       ENDDO

! --- fin de la boucle it�rative Concus et Golub   
       ENDDO
 !
       write(fmsgo,*) 'pas convergence en ',nbpot,
     &     ' iterations, eps=',tz
     
       write(fmsgo,*) ' -----------------------------------------' 
       write(fmsgo,*) '     Fin 2� correction de Poisson       '
       write(fmsgo,*) ' -----------------------------------------'
     
       return
       
2000   continue
       write(fmsgo,*) ' '
       write(fmsgo,*) 'convergence en ',iter_golub,
     &     ' iterations, eps=',tz
       write(fmsgo,*) ' '
       
       DO j=1,ny
         DO i=1,nx
           ex(i,j)=ex(i,j)-dxi*(phico(i,j)-phico(i-1,j))
           ey(i,j)=ey(i,j)-dyi*(phico(i,j)-phico(i,j-1))
         ENDDO
       ENDDO            
       
       write(fmsgo,*) ' -----------------------------------------' 
       write(fmsgo,*) '     Fin 2� correction de Poisson       '
       write(fmsgo,*) ' -----------------------------------------'
       
       return       
!      _____________________       
       END SUBROUTINE pot2d_cflout
       
       
       
       
       SUBROUTINE calq
! ======================================================================
! 
! 
!
!
!
! ======================================================================       
       
       USE champs
       USE domaines
       USE temps
 
       IMPLICIT NONE
       
       INTEGER    :: i, j, ix, jy
  


         bx_tn = bx
         by_tn = by
         bz_tn = bz
 !     on met dans b le champ magnetique au temps n-1/2
 !
         bx=zetax
         by=zetay
         bz=zetaz
         zetax=0.
         zetay=0.
         zetaz=0.


       IF(tta_def.EQ.1) THEN       
         DO jy=0,n1y
           DO ix=0,n1x
! -------- mise � jour Eb(n-2)  
            ebx(ix,jy) = (1-thetad_2(ix,jy))*extm1(ix,jy)  
     &                   + thetad_2(ix,jy)*ebx(ix,jy)
            eby(ix,jy) = (1-thetad_2(ix,jy))*eytm1(ix,jy) 
     &                   + thetad_2(ix,jy)*eby(ix,jy)
            ebz(ix,jy) = (1-thetad_2(ix,jy))*eztm1(ix,jy) 
     &                   + thetad_2(ix,jy)*ebz(ix,jy)    

! -------- mise � jour de Ebb(n-1)
            ebbx(ix,jy) = thetad_2(ix,jy)*ex(ix,jy) 
     &                   + (1-thetad_2(ix,jy))*ebx(ix,jy)
            ebby(ix,jy) = thetad_2(ix,jy)*ey(ix,jy) 
     &                   + (1-thetad_2(ix,jy))*eby(ix,jy)
            ebbz(ix,jy) = thetad_2(ix,jy)*ez(ix,jy) 
     &                   + (1-thetad_2(ix,jy))*ebz(ix,jy)
	   ENDDO
         ENDDO

       ELSEIF(i_damp1.GT.0) THEN     
! ---- mise � jour Eb(n-2)  zone 1
        ebx(0:i_damp1,:) = (1-thetads2)*extm1(0:i_damp1,:) 
     &                + thetads2*ebx(0:i_damp1,:)
        eby(0:i_damp1,:) = (1-thetads2)*eytm1(0:i_damp1,:) 
     &                + thetads2*eby(0:i_damp1,:)
        ebz(0:i_damp1,:) = (1-thetads2)*eztm1(0:i_damp1,:) 
     &                + thetads2*ebz(0:i_damp1,:)    

! ---- mise � jour de Ebb(n-1) zone 1
        ebbx(0:i_damp1,:) = thetads2*ex(0:i_damp1,:) 
     &               + (1-thetads2)*ebx(0:i_damp1,:)
        ebby(0:i_damp1,:) = thetads2*ey(0:i_damp1,:) 
     &               + (1-thetads2)*eby(0:i_damp1,:)
        ebbz(0:i_damp1,:) = thetads2*ez(0:i_damp1,:) 
     &               + (1-thetads2)*ebz(0:i_damp1,:)
     
! ---- mise � jour Eb(n-2)  zone 2
        ebx(i_damp1+1:i_damp2,:) = 
     &     (1-ttapushs2)*extm1(i_damp1+1:i_damp2,:) 
     &                + ttapushs2*ebx(i_damp1+1:i_damp2,:)
        eby(i_damp1+1:i_damp2,:) = 
     &     (1-ttapushs2)*eytm1(i_damp1+1:i_damp2,:) 
     &                + ttapushs2*eby(i_damp1+1:i_damp2,:)
        ebz(i_damp1+1:i_damp2,:) = 
     &     (1-ttapushs2)*eztm1(i_damp1+1:i_damp2,:) 
     &                + ttapushs2*ebz(i_damp1+1:i_damp2,:)    

! ---- mise � jour de Ebb(n-1) zone 2
        ebbx(i_damp1+1:i_damp2,:) = ttapushs2*ex(i_damp1+1:i_damp2,:) 
     &               + (1-ttapushs2)*ebx(i_damp1+1:i_damp2,:)
        ebby(i_damp1+1:i_damp2,:) = ttapushs2*ey(i_damp1+1:i_damp2,:) 
     &               + (1-ttapushs2)*eby(i_damp1+1:i_damp2,:)
        ebbz(i_damp1+1:i_damp2,:) = ttapushs2*ez(i_damp1+1:i_damp2,:) 
     &               + (1-ttapushs2)*ebz(i_damp1+1:i_damp2,:)
     
! ---- mise � jour Eb(n-2) zone 3 
        ebx(i_damp2+1:n1x,:) = (1-thetads2)*extm1(i_damp2+1:n1x,:) 
     &                + thetads2*ebx(i_damp2+1:n1x,:)
        eby(i_damp2+1:n1x,:) = (1-thetads2)*eytm1(i_damp2+1:n1x,:) 
     &                + thetads2*eby(i_damp2+1:n1x,:)
        ebz(i_damp2+1:n1x,:) = (1-thetads2)*eztm1(i_damp2+1:n1x,:) 
     &                + thetads2*ebz(i_damp2+1:n1x,:)    

! ---- mise � jour de Ebb(n-1) zone 3
        ebbx(i_damp2+1:n1x,:) = thetads2*ex(i_damp2+1:n1x,:) 
     &               + (1-thetads2)*ebx(i_damp2+1:n1x,:)
        ebby(i_damp2+1:n1x,:) = thetads2*ey(i_damp2+1:n1x,:) 
     &               + (1-thetads2)*eby(i_damp2+1:n1x,:)
        ebbz(i_damp2+1:n1x,:) = thetads2*ez(i_damp2+1:n1x,:) 
     &               + (1-thetads2)*ebz(i_damp2+1:n1x,:)
       ELSE

! ---- mise � jour Eb(n-2)  
        ebx(:,:) = (1-thetads2)*extm1(:,:) + thetads2*ebx(:,:)
        eby(:,:) = (1-thetads2)*eytm1(:,:) + thetads2*eby(:,:)
        ebz(:,:) = (1-thetads2)*eztm1(:,:) + thetads2*ebz(:,:)    

! ---- mise � jour de Ebb(n-1)
        ebbx(:,:) = thetads2*ex(:,:) + (1-thetads2)*ebx(:,:)
        ebby(:,:) = thetads2*ey(:,:) + (1-thetads2)*eby(:,:)
        ebbz(:,:) = thetads2*ez(:,:) + (1-thetads2)*ebz(:,:)
       ENDIF


 !
 !     calcul du terme  source du systeme donnant (ex,ey,ez)
 !
       DO  j=1,ny
         DO  i=1,nx
         qx(i,j)=-dt*jxtot(i,j)+ex(i,j)+dtsdy*(bz(i,j+1)-bz(i,j))                  
     &       -d11*((ebby(i,j+1)-ebby(i-1,j+1))-(ebby(i,j)-ebby(i-1,j)))           
     &          +d12*(ebbx(i,j+1)-2.*ebbx(i,j)+ebbx(i,j-1))
         ENDDO
       ENDDO
 !
       DO  j=1,n1y
         DO  i=1,nxm1
         qy(i,j)=-dt*jytot(i,j)+ey(i,j)-dtsdx*(bz(i+1,j)-bz(i,j))                 
     &          +d13*(ebby(i+1,j)-2.*ebby(i,j)+ebby(i-1,j))                         
     &      -d11*((ebbx(i+1,j)-ebbx(i+1,j-1))-(ebbx(i,j)-ebbx(i,j-1)))
         ENDDO
       ENDDO           
!
       DO  j=1,ny
         DO  i=1,nxm1
         qz(i,j)=-dt*jztot(i,j)+ez(i,j)                                            
     &          +dtsdx*(by(i+1,j)-by(i,j))-dtsdy*(bx(i,j+1)-bx(i,j))             
     &          +d13*(ebbz(i+1,j)-2.*ebbz(i,j)+ebbz(i-1,j))                        
     &          +d12*(ebbz(i,j+1)-2.*ebbz(i,j)+ebbz(i,j-1)) 
         ENDDO 
       ENDDO
      
   
       return       
!      ___________________       
       END SUBROUTINE calq
       
                   
       
