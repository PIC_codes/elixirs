! ======================================================================
! dom_dynam.f
! -----------
!
!     SUBROUTINE part_dom
!     SUBROUTINE cree_dom
!
! ======================================================================

      SUBROUTINE part_dom
! ======================================================================
! ======================================================================
      USE precisions
      USE domaines
      USE erreurs

! cald     USE particules


      IMPLICIT NONE
!!      INCLUDE 'mpif.h'


      INTEGER                    :: iproc, iprocx, iprocy, iprocz
!      INTEGER                    :: dist_x, dist_y, dist_z

! Limites globales du domaine
! ----------------------------------------------------------------------
      xmin_gl =  0.
      ymin_gl = -0.5_8*nbmy_gl*pasdy
      zmin_gl = -0.5_8*nbmz_gl*pasdz  
      
! Pas de zones de vide si CL periodiques sur les particules (modif 08/2006)
      write(fdbg,*) 'ibndp(1:2,1) = ',ibndp(1:2,1)
      write(fdbg,*) 'ibndp(3:4,1) = ',ibndp(3:4,1)
      write(fdbg,*) 'ibndp(5:6,1) = ',ibndp(5:6,1)
      
      SELECT CASE(dim_cas)
      CASE(6)
        IF (ANY(ibndp(1:2,1).EQ.-1)) nb_vidx = 0
        IF (ANY(ibndp(3:4,1).EQ.-1)) nb_vidy = 0
        IF (ANY(ibndp(5:6,1).EQ.-1)) nb_vidz = 0   
      CASE(4,5)
        IF (ANY(ibndp(1:2,1).EQ.-1)) nb_vidx = 0
        IF (ANY(ibndp(3:4,1).EQ.-1)) nb_vidy = 0
        nb_vidz = 0   
      CASE(1,2,3)
        IF (ANY(ibndp(1:2,1).EQ.-1)) nb_vidx = 0
        nb_vidy = 0
        nb_vidz = 0
      END SELECT   
      write(fdbg,*) 'nb_vid[xyz] = ',nb_vidx,nb_vidy,nb_vidz 
                   
! Partition du domaine entre les processeurs
! Ulterieurement, adapter pour 
! 1/ tolerer une taille non-multiple,
! 2/ adapter les tailles de domaines a la charge de travail attendue
! ----------------------------------------------------------------------
      IF( (MOD(nbmx_gl,nprocx).NE.0).OR.(MOD(nbmy_gl,nprocy).NE.0).OR.
     &    (MOD(nbmz_gl,nprocz).NE.0) ) THEN
        IF(numproc.EQ.1) THEN
          WRITE(6,"('Subroutine calc_donn : la taille totale')")
          WRITE(6,"('du plasma n''est pas un multiple de')")
          WRITE(6,"('la partition entre les processeurs')")
        ENDIF
!        CALL MPI_ABORT(MPI_COMM_WORLD,0,info)
      ENDIF
      nbmx = nbmx_gl/nprocx
      nbmy = nbmy_gl/nprocy
      nbmz = nbmz_gl/nprocz

!  numproc[xyz]=1 d�fini comme PARAMETER dans modules.f

! Positions des frontieres entre processeurs                (ibord(:,:))
! ----------------------------------------------------------------------
      DO iproc=1,nproc
        iprocx = MOD((iproc-1), nprocx) + 1
        iprocy = MOD((iproc-1)/nprocx, nprocy) + 1
        iprocz = (iproc-1)/(nprocx*nprocy) + 1
!
        ibord(1,numproc) = (nprocx-1)*nbmx
        ibord(2,numproc) =  nprocx   *nbmx
        ibord(3,numproc) = (nprocy-1)*nbmy
        ibord(4,numproc) =  nprocy   *nbmy
        ibord(5,numproc) = (nprocz-1)*nbmz
        ibord(6,numproc) =  nprocz   *nbmz
      ENDDO

! Limites diverses du domaine locale, et taille du buffer de particules
! ----------------------------------------------------------------------
      CALL cree_dom      
      IF(debug.GE.1) WRITE(fdbg,"('Buffers de ',2I8,' particules')")
     &                             sizeabs,sizebuf

! Coherence des conditions aux limites periodiques pour les champs
! ----------------------------------------------------------------------
      IF( ANY( ibndc(1:2).EQ.-1 ) ) ibndc(1:2)=-1
      IF( ANY( ibndc(3:4).EQ.-1 ) ) ibndc(3:4)=-1
      IF( ANY( ibndc(5:6).EQ.-1 ) ) ibndc(5:6)=-1

! Conditions aux limites pour les particules                (ibndp(:,:))
! Connections aux autres PEs pour le routage des messages   (connect(:))
!   note : en cas de conditions periodiques + 2 PE dans
!          une dimension, on privilegie la connection
!          interieure au domaine
! Reconnaissance des voisins                                (voisin(6))
! Conditions aux limites pour les champs                    (ibndc(:))
! ----------------------------------------------------------------------
!      DO iproc=1,nproc
!        ibndp(1:6,iproc) = ibndp(1:6,1)
!      ENDDO
      
!     _______________________
      END SUBROUTINE part_dom



      SUBROUTINE cree_dom
! ======================================================================
! Calcul des differentes frontieres (entieres, reelles...) des domaines
! a partir du tableau ibord et, pour les frontieres absolues, de
! l'origine du domaine (xmin_gl, ymin_gl, zmin_gl)
!
! Calcul des tailles des buffers pour les echanges de particules
! ======================================================================

      USE particules_klder
      USE domaines
      USE diagnostics_klder
      USE erreurs


      IMPLICIT NONE


      INTEGER                    :: i, j, k, ok_x, ok_y, ok_z
      INTEGER, DIMENSION(:), ALLOCATABLE  :: nbppm_aux

      INTEGER     ::     gliss_ok



      gliss_ok = 0
      sizeabs = 0
      sizebuf = 0

      
! Origine du domaine : [xyz]min_gl : variables reelles absolues
! ----------------------------------------------------------------------
! Positions des bords : ibord(:,:) : variables entieres relatives
! ----------------------------------------------------------------------
! Positions des bords : nul[mp][xyz] : variables entieres locales
! ----------------------------------------------------------------------
      nulmx = ibord(1,numproc)
      nulpx = ibord(2,numproc)
      nulmy = ibord(3,numproc)
      nulpy = ibord(4,numproc)
      nulmz = ibord(5,numproc)
      nulpz = ibord(6,numproc)
      
      WRITE(fdbg,"('nulmx  : ', (I8  ,1X))") nulmx
      WRITE(fdbg,"('nulpx  : ', (I8  ,1X))") nulpx
      WRITE(fdbg,"('nulmy  : ', (I8  ,1X))") nulmy
      WRITE(fdbg,"('nulpy  : ', (I8  ,1X))") nulpy
      WRITE(fdbg,"('nulmz  : ', (I8  ,1X))") nulmz
      WRITE(fdbg,"('nulpz  : ', (I8  ,1X))") nulpz
      
! Taille du domaine : nbm[xyz] : variables entieres relatives
! ----------------------------------------------------------------------
      nbmx = ibord(2,numproc) - ibord(1,numproc)
      nbmy = ibord(4,numproc) - ibord(3,numproc)
      nbmz = ibord(6,numproc) - ibord(5,numproc)
! Positions des bords : bord(:,:) : variables reelles absolues
! ----------------------------------------------------------------------
      bord(1:2,numproc) = xmin_gl + ibord(1:2,numproc)*pasdx
      bord(3:4,numproc) = ymin_gl + ibord(3:4,numproc)*pasdy
      bord(5:6,numproc) = zmin_gl + ibord(5:6,numproc)*pasdz

! Fin du domaine : [xyz]max_gl : variables reelles absolues
! ----------------------------------------------------------------------
      xmax_gl = bord(2,nproc)
      ymax_gl = bord(4,nproc)
      zmax_gl = bord(6,nproc)
! Frontieres du domaine local : [xyz]minmax : variables reelles absolues
! ----------------------------------------------------------------------
      xmin = bord(1,numproc)
      xmax = bord(2,numproc)
      ymin = bord(3,numproc)
      ymax = bord(4,numproc)
      zmin = bord(5,numproc)
      zmax = bord(6,numproc)
      
! On reserve nb_vid[xyz] mailles de vide aux extremites du
! domaine global dans chaque direction [xyz] sauf en en cas
! de conditions periodiques.
! Cela implique donc nbmx > nb_vid[xyz] (modif 08/2006)
! ----------------------------------------------------------------------
      write(fdbg,"('ibndp(1:2,1) = ',2(I2,1X))") ibndp(1:2,1)
      write(fdbg,"('ibndp(3:4,1) = ',2(I2,1X))") ibndp(3:4,1)
      write(fdbg,"('ibndp(5:6,1) = ',2(I2,1X))") ibndp(5:6,1)
      
      SELECT CASE(dim_cas)
      CASE(6)
        IF (ANY(ibndp(1:2,1).EQ.-1)) nb_vidx = 0
        IF (ANY(ibndp(3:4,1).EQ.-1)) nb_vidy = 0
        IF (ANY(ibndp(5:6,1).EQ.-1)) nb_vidz = 0   
      CASE(4,5)
        IF (ANY(ibndp(1:2,1).EQ.-1)) nb_vidx = 0
        IF (ANY(ibndp(3:4,1).EQ.-1)) nb_vidy = 0
        nb_vidz = 0   
      CASE(1,2,3)
        IF (ANY(ibndp(1:2,1).EQ.-1)) nb_vidx = 0
        nb_vidy = 0
        nb_vidz = 0
      END SELECT     
      write(fdbg,"('nb_vid[xyz] = ',3(I2,1X))") 
     &      nb_vidx,nb_vidy,nb_vidz   

       DO k=0,nprocz-1
        DO j=0,nprocy-1
	  bord(1,(k*nprocy+j)*nprocx+1)
     &      = bord(1,(k*nprocy+j)*nprocx+1) + nb_vidx*pasdx
          bord(2,(k*nprocy+j+1)*nprocx)
     &      = bord(2,(k*nprocy+j+1)*nprocx) - nb_vidx*pasdx
        ENDDO
      ENDDO
      
      DO k=0,nprocz-1
        DO i=0,nprocx-1
	  bord(3,k*nprocx*nprocy+i+1)
     &  = bord(3,k*nprocx*nprocy+i+1) + nb_vidy*pasdy
          bord(4,(k+1)*nprocx*nprocy-nprocx+i+1)
     &  = bord(4,(k+1)*nprocx*nprocy-nprocx+i+1) - nb_vidy*pasdy
        ENDDO
      ENDDO	
            
      DO j=0,nprocy-1
        DO i=0,nprocx-1
	  bord(5,j*nprocx+i+1)
     &  = bord(5,j*nprocx+i+1) + nb_vidz*pasdz
          bord(6,j*nprocx+(nprocz-1)*nprocx*nprocy+i+1)
     &  = bord(6,j*nprocx+(nprocz-1)*nprocx*nprocy+i+1)
     &  - nb_vidz*pasdz
        ENDDO	
      ENDDO
 
      write(fdbg,"('bord(1:6) =',6(1PE11.4,1X))") bord(1:6,numproc)

! Nombre max de partic. que l'on prevoit d'echanger avec les PE voisins
! ----------------------------------------------------------------------
      SELECT CASE(dim_cas)
      CASE(6)
        sizebuf = nbmx*nbmy + nbmx*nbmz + (gliss_ok+1)*nbmy*nbmz
        sizeabs = 2*sizebuf
        IF (nprocx.EQ.1) sizebuf = sizebuf - (gliss_ok+1)*nbmy*nbmz
        IF (nprocy.EQ.1) sizebuf = sizebuf - nbmx*nbmz
        IF (nprocz.EQ.1) sizebuf = sizebuf - nbmx*nbmy
      CASE(5,4)
        sizebuf = nbmx + (gliss_ok+1)*nbmy
        sizeabs = 2*sizebuf
        IF (nprocx.EQ.1) sizebuf = sizebuf - (gliss_ok+1)*nbmy
!        IF (nprocy.EQ.1) sizebuf = sizebuf - nbmx
      CASE(3,2,1)
        sizebuf = (gliss_ok+1)*2 + 2                    ! + 2 = bonus...
        sizeabs = 2*sizebuf
      CASE DEFAULT
        sizebuf = 0
        sizeabs = 0
      END SELECT
      IF(ALLOCATED(nbppm)) THEN
        ALLOCATE(nbppm_aux(nesp_p))
        nbppm_aux(1:nesp_p) = nbppm(1:nesp_p)
!        DO j=1,nesp_p
!          IF(meth_ioni(j).NE.0) THEN
!            nbppm_aux(ioni_vers(j)) =   nbppm_aux(ioni_vers(j))
!     &                                + nbppm_aux(j)
!            nbppm_aux(nesp_p)       =   nbppm_aux(nesp_p)
!     &                                + nbppm_aux(j)
!          ENDIF
!        ENDDO
        sizeabs = sizeabs*MAXVAL(nbppm_aux(:))
        sizebuf = sizebuf*MAXVAL(nbppm_aux(:))
!        write(fdbg,"('on multiplie sizeabs et sizebuf par 10')")
!        sizeabs = 10*sizeabs*MAXVAL(nbppm_aux(:))   ! test 1d
!        sizebuf = 10*sizebuf*MAXVAL(nbppm_aux(:))
        DEALLOCATE(nbppm_aux)
      ELSE
        sizeabs = 0
        sizebuf = 0
      ENDIF

      write(fdbg,"('sizeabs = ', I10)") sizeabs
      write(fdbg,"('sizebuf = ', I10)") sizebuf


! Diagnostics
! ----------------------------------------------------------------------
      IF(clo_nb.GT.0) THEN
        DO j=1,clo_nb
          WRITE(clo_ch(j)(3:3),"(A1)") 0
          ok_x = 0 ; ok_y = 0 ; ok_z = 0
          IF(pasdx.NE.0.) THEN
            SELECT CASE(clo_ch(j)(1:2))
            CASE('ex','by','bz')
              clo_ind(1,j) = (clo_pos(1,j)-xmin_gl)/pasdx + 1.0
              IF(       (clo_ind(1,j).GE.nulmx)
     &            .AND.((clo_ind(1,j).LT.nulpx)
     &                  .OR.(     (clo_ind(1,j).LE.nulpx+1)
     &                       .AND.(numprocx.EQ.nprocx)     )) ) ok_x=1
            CASE DEFAULT
              clo_ind(1,j) = (clo_pos(1,j)-xmin_gl)/pasdx + 0.5
              IF(       (clo_ind(1,j).GE.nulmx)
     &            .AND.((clo_ind(1,j).LT.nulpx)
     &                  .OR.(     (clo_ind(1,j).LE.nulpx)
     &                       .AND.(numprocx.EQ.nprocx)     )) ) ok_x=1
            END SELECT
          ENDIF

          IF(pasdy.NE.0.) THEN
            SELECT CASE(clo_ch(j)(1:2))
            CASE('ey','bx','bz')
              clo_ind(2,j) = (clo_pos(2,j)-ymin_gl)/pasdy + 1.0
              IF(       (clo_ind(2,j).GE.nulmy)
     &            .AND.((clo_ind(2,j).LT.nulpy)
     &                  .OR.(     (clo_ind(2,j).LE.nulpy+1)
     &                       .AND.(numprocy.EQ.nprocy)     )) ) ok_y=1
            CASE DEFAULT
              clo_ind(2,j) = (clo_pos(2,j)-ymin_gl)/pasdy + 0.5
              IF(       (clo_ind(2,j).GE.nulmy)
     &            .AND.((clo_ind(2,j).LT.nulpy)
     &                  .OR.(     (clo_ind(2,j).LE.nulpy)
     &                       .AND.(numprocy.EQ.nprocy)     )) ) ok_y=1
            END SELECT
          ENDIF

          IF(pasdz.NE.0.) THEN
          SELECT CASE(clo_ch(j)(1:2))
            CASE('ez','bx','by')
              clo_ind(3,j) = (clo_pos(3,j)-zmin_gl)/pasdz + 1.0
              IF(       (clo_ind(3,j).GE.nulmz)
     &            .AND.((clo_ind(3,j).LT.nulpz)
     &                  .OR.(     (clo_ind(3,j).LE.nulpz+1)
     &                       .AND.(numprocz.EQ.nprocz)     )) ) ok_z=1
            CASE DEFAULT
              clo_ind(3,j) = (clo_pos(3,j)-zmin_gl)/pasdz + 0.5
              IF(       (clo_ind(3,j).GE.nulmz)
     &            .AND.((clo_ind(3,j).LT.nulpz)
     &                  .OR.(     (clo_ind(3,j).LE.nulpz)
     &                       .AND.(numprocz.EQ.nprocz)     )) ) ok_z=1
            END SELECT
          ENDIF

          SELECT CASE(dim_cas)
          CASE(6)
            IF( (ok_x.EQ.1).AND.(ok_y.EQ.1).AND.(ok_z.EQ.1) )
     &      WRITE(clo_ch(j)(3:3),"(I1)") 3
          CASE(5,4)
            IF( (ok_x.EQ.1).AND.(ok_y.EQ.1) )
     &      WRITE(clo_ch(j)(3:3),"(I1)") 2
          CASE(3,2,1)
            IF( (ok_x.EQ.1) )
     &      WRITE(clo_ch(j)(3:3),"(I1)") 1
          END SELECT
        ENDDO
      ENDIF

      IF(cmx_nb.GT.0) THEN
        DO j=1,cmx_nb
          WRITE(cmx_ch(j)(3:3),"(A1)") 0
          SELECT CASE(cmx_ch(j)(1:2))
          CASE('ex','cx','rh')
            IF(    (dim_cas.EQ.6))       WRITE(cmx_ch(j)(3:3),"(I1)") 3
            IF(    (dim_cas.EQ.5)
     &         .OR.(dim_cas.EQ.4))       WRITE(cmx_ch(j)(3:3),"(I1)") 2
            IF(    (dim_cas.EQ.3).OR.(dim_cas.EQ.2)
     &         .OR.(dim_cas.EQ.1))       WRITE(cmx_ch(j)(3:3),"(I1)") 1
          CASE('ey','bz','cy')
            IF(    (dim_cas.EQ.6))       WRITE(cmx_ch(j)(3:3),"(I1)") 3
            IF(    (dim_cas.EQ.5)
     &         .OR.(dim_cas.EQ.4))       WRITE(cmx_ch(j)(3:3),"(I1)") 2
            IF(    (dim_cas.EQ.3)
     &         .OR.(dim_cas.EQ.2))       WRITE(cmx_ch(j)(3:3),"(I1)") 1
          CASE('ez','bx','by','cz')
            IF(    (dim_cas.EQ.6))       WRITE(cmx_ch(j)(3:3),"(I1)") 3
            IF(    (dim_cas.EQ.5))       WRITE(cmx_ch(j)(3:3),"(I1)") 2
            IF(    (dim_cas.EQ.3))       WRITE(cmx_ch(j)(3:3),"(I1)") 1
          END SELECT
        ENDDO
      ENDIF

! Messages
! ----------------------------------------------------------------------
      IF(debug.GE.1) THEN
        WRITE(fdbg,"('  en x : de ',1PE10.3,' a ',1PE10.3)") xmin,xmax
        WRITE(fdbg,"('  en y : de ',1PE10.3,' a ',1PE10.3)") ymin,ymax
        WRITE(fdbg,"('  en z : de ',1PE10.3,' a ',1PE10.3)") zmin,zmax
      ENDIF

      RETURN
!     _______________________
      END SUBROUTINE cree_dom


      
