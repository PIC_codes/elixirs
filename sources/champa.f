! ======================================================================
! champa.f
! -------------
!
!    SUBROUTINE  invmat33
!
!    SUBROUTINE  ponder_ordre1
!    SUBROUTINE  ponder_ordre2
!    SUBROUTINE  ponder_ordre3
!    SUBROUTINE  ponder_ordre4
!    SUBROUTINE  ponder_ordre1_ik
!    SUBROUTINE  ponder_ordre2_ik
!    SUBROUTINE  ponder_ordre3_ik
!    SUBROUTINE  ponder_ordre4_ik
!
!    SUBROUTINE  ponder_ordre1_dual
!    SUBROUTINE  ponder_ordre2_dual
!    SUBROUTINE  ponder_ordre3_dual
!    SUBROUTINE  ponder_ordre4_dual
!    SUBROUTINE  ponder_ordre1_ik_dual
!    SUBROUTINE  ponder_ordre2_ik_dual
!    SUBROUTINE  ponder_ordre3_ik_dual
!    SUBROUTINE  ponder_ordre4_ik_dual
!
!    SUBROUTINE  champa_2d_gradE
!
!    SUBROUTINE  projpa_2d_rho_ik
!
!    SUBROUTINE  projpa_2dx3dv_rjxyz
!    SUBROUTINE  projpa_2dx3dv_dgxyz
!
!    SUBROUTINE  projpa_2dx3dv_rho
!
!    SUBROUTINE  champa_2dx3dv_bxyz
!    SUBROUTINE  champa_2d_exyz1
!
!    SUBROUTINE  champa_2d_exyztm1
!    SUBROUTINE  champa_2d_epart_ik
!
!    SUBROUTINE  champa_2d_exyz1_gridmax
!    SUBROUTINE  champa_2d_exyztm1_gridmax
!    SUBROUTINE champa_2d_epart_ik_gridmax
!
!
! ======================================================================



      SUBROUTINE invmat33(Am33)
! ======================================================================
! Routine d inversion d une matrice 3*3
! Méthode commune de la commatrice
! inv(Am33)=t(Com(Am33))/det(Am33)
! ======================================================================

      USE domaines
      USE particules
      USE particules_klder
      USE champs
      USE temps
      USE erreurs

      IMPLICIT NONE

      REAL(kind=kr), DIMENSION(1:3,1:3), INTENT(INOUT)  ::  Am33

      REAL(kind=kr), DIMENSION(1:3,1:3)  ::  adjM

      REAL(kind=kr)  ::   a1, a2, a3,
     &                    b1, b2, b3,
     &                    c1, c2, c3

      REAL(kind=kr)  ::   detM


      a1=Am33(1,1); a2=Am33(1,2); a3=Am33(1,3)
      b1=Am33(2,1); b2=Am33(2,2); b3=Am33(2,3)
      c1=Am33(3,1); c2=Am33(3,2); c3=Am33(3,3)

      detM= a1*(b2*c3 - c2*b3) - a2*(b1*c3 - c1*b3) + a3*(b1*c2 - c1*b2)

      adjM(1,1)=  (b2*c3 - c2*b3)/detM;
      adjM(1,2)= -(a2*c3 - c2*a3)/detM;
      adjM(1,3)=  (a2*b3 - b2*a3)/detM;

      adjM(2,1)= -(b1*c3 - c1*b3)/detM;
      adjM(2,2)=  (a1*c3 - c1*a3)/detM;
      adjM(2,3)= -(a1*b3 - b1*a3)/detM;

      adjM(3,1)=  (b1*c2 - c1*b2)/detM;
      adjM(3,2)= -(a1*c2 - c1*a2)/detM;
      adjM(3,3)=  (a1*b2 - b1*a2)/detM;

! ---- l inverse est stockee dans Am33
      Am33= adjM;


      RETURN
!     ________________________________
      END SUBROUTINE invmat33



      SUBROUTINE ponder_ordre1(ip, iesp, ponpx, ponpy, ipx, ipy)
! ======================================================================
! Projection sur la grille a 2 dimensions
! des charge et courants particulaires de l'espece iesp
! On calcule les quatre champs source Cx, Cy, Cz, Rh
! ======================================================================

      USE domaines
      USE particules
      USE particules_klder
      USE champs
      USE temps
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)      :: ip, iesp

      REAL(KIND=kr)    :: rpx, rpy

      INTEGER          :: ipmx, ippx, ipmy, ippy
      REAL(KIND=kr)    :: ponpmx, ponppx,
     &                    ponpmy, ponppy

      INTEGER(KIND=kr), DIMENSION(1:2), INTENT(OUT)  :: ipx, ipy
      REAL(KIND=kr), DIMENSION(1:2), INTENT(OUT)  :: ponpx, ponpy


! ----- on détermine ds quelle maille est la particule
        rpx = (partic(a_qx,ip,iesp)-xmin_gl)*dxi
        rpy = (partic(a_qy,ip,iesp)-ymin_gl)*dyi
! ----- coordonnées entières, lignes encadrant la particule
        ipmx = INT(rpx)
        ippx = ipmx+1
        ipmy = INT(rpy)
        ippy = ipmy+1

        ponpmx = REAl(ippx,8) - rpx
        ponppx = 1._8 - ponpmx
        ponpmy = REAl(ippy,8) - rpy
        ponppy = 1._8 - ponpmy

! --- Initialisation des tableaux pour calculs
! --- simplifies des projections sur les noeuds du maillage
        ipx(1)  = ipmx ; ipx(2)  = ippx
	ipy(1)  = ipmy ; ipy(2)  = ippy

        ponpx(1) = ponpmx ; ponpx(2) = ponppx
        ponpy(1) = ponpmy ; ponpy(2) = ponppy


      RETURN
!     ________________________________
      END SUBROUTINE ponder_ordre1



      SUBROUTINE ponder_ordre2(ip, iesp, ponpx, ponpy, ipx, ipy)
! ======================================================================
! Projection sur la grille a 2 dimensions
! des charge et courants particulaires de l'espece iesp
! On calcule les quatre champs source Cx, Cy, Cz, Rh
! ======================================================================

      USE domaines
      USE particules
      USE particules_klder
      USE champs
      USE temps
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)      :: ip, iesp

      REAL(KIND=kr)    :: rpx, rpy
      INTEGER          :: ipmx, ipctx, ippx, ipmy, ipcty, ippy
      REAL(KIND=kr)    :: ponpmx, ponpctx, ponppx,
     &                    ponpmy, ponpcty, ponppy

      INTEGER(KIND=kr), DIMENSION(1:3), INTENT(OUT)  :: ipx, ipy
      REAL(KIND=kr), DIMENSION(1:3), INTENT(OUT)  :: ponpx, ponpy

!      INTEGER         :: i, j


! --- Coordonnees reelles en numero de maille primale
        rpx = (partic(a_qx,ip,iesp)-xmin_gl)*dxi
        rpy = (partic(a_qy,ip,iesp)-ymin_gl)*dyi
! --- Coordonnees entieres primales : lignes encadrant la particule
        ipmx = FLOOR(rpx-0.5_8)
	ipctx = ipmx+1
        ippx = ipmx+2
        ipmy = FLOOR(rpy-0.5_8)
	ipcty = ipmy+1
        ippy = ipmy+2

! --- Ponderations quadratiques des differents sommets primaux
        ponpmx  = 0.5*( 0.5 + (REAL(ipctx,8) - rpx ) )**2
	ponpctx = 0.75 - ( REAL(ipctx,8) - rpx )**2
	ponppx  = 0.5*( 0.5 - (REAL(ipctx,8) - rpx ) )**2

        ponpmy  = 0.5*( 0.5 + (REAL(ipcty,8) - rpy ) )**2
	ponpcty = 0.75 - ( REAL(ipcty,8) - rpy )**2
	ponppy  = 0.5*( 0.5 - (REAL(ipcty,8) - rpy ) )**2

! --- Initialisation des tableaux pour calculs
! --- simplifies des projections sur les noeuds du maillage
        ipx(1)  = ipmx ; ipx(2)  = ipctx ; ipx(3)  = ippx ;
	ipy(1)  = ipmy ; ipy(2)  = ipcty ; ipy(3)  = ippy ;
        IF(ipy(1).EQ.-1) ipy(1)=ny-1

        ponpx(1) = ponpmx ; ponpx(2) = ponpctx ; ponpx(3) = ponppx ;
        ponpy(1) = ponpmy ; ponpy(2) = ponpcty ; ponpy(3) = ponppy ;

      RETURN
!     ________________________________
      END SUBROUTINE ponder_ordre2


      SUBROUTINE ponder_ordre3(ip, iesp, ponpx, ponpy, ipx, ipy)
! ======================================================================
! Projection sur la grille a 2 dimensions
! des charge et courants particulaires de l'espece iesp
! On calcule les quatre champs source Cx, Cy, Cz, Rh
! ======================================================================

      USE domaines
      USE particules
      USE particules_klder
      USE champs
      USE temps
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)      :: ip, iesp

      REAL(KIND=kr)    :: rpx, rpy
      INTEGER          :: ipmmx, ipmx, ippx, ipppx,
     &                    ipmmy, ipmy, ippy, ipppy
      REAL(KIND=kr)    :: ponpmmx, ponpmx, ponppx, ponpppx,
     &                    ponpmmy, ponpmy, ponppy, ponpppy

      INTEGER(KIND=kr), DIMENSION(1:4), INTENT(OUT)  :: ipx, ipy
      REAL(KIND=kr), DIMENSION(1:4), INTENT(OUT)  :: ponpx, ponpy

!      INTEGER         :: i, j


! --- Coordonnees reelles en numero de maille primale
        rpx = (partic(a_qx,ip,iesp)-xmin_gl)*dxi
        rpy = (partic(a_qy,ip,iesp)-ymin_gl)*dyi
! --- Coordonnees entieres primales : lignes encadrant la particule
	ipmmx = FLOOR(rpx - 1)
	ipmx  = ipmmx + 1
	ippx  = ipmmx + 2
	ipppx = ipmmx + 3
	ipmmy = FLOOR(rpy - 1)
	ipmy  = ipmmy + 1
	ippy  = ipmmy + 2
	ipppy = ipmmy + 3

! --- Ponderations cubiques des differents sommets primaux
	ponpmmx = (4.0/3.0)*(1 + (REAL(ipmmx,8) - rpx)/2 )**3
	ponpmx  = 2.0/3.0 - (REAL(ipmx,8) - rpx)**2 -
     &  (REAL(ipmx,8) - rpx)**3/2
	ponppx  = 2.0/3.0 - (REAL(ippx,8) - rpx)**2 +
     &  (REAL(ippx,8) - rpx)**3/2
	ponpppx = (4.0/3.0)*(1 - (REAL(ipppx,8) - rpx)/2 )**3

	ponpmmy = (4.0/3.0)*(1 + (REAL(ipmmy,8) - rpy)/2 )**3
	ponpmy  = 2.0/3.0 - (REAL(ipmy,8) - rpy)**2 -
     &  (REAL(ipmy,8) - rpy)**3/2
	ponppy  = 2.0/3.0 - (REAL(ippy,8) - rpy)**2 +
     &  (REAL(ippy,8) - rpy)**3/2
	ponpppy = (4.0/3.0)*(1 - (REAL(ipppy,8) - rpy)/2 )**3

! --- Initialisation des tableaux pour calculs
! --- simplifies des projections sur les noeuds du maillage
! --- Initialisation des tableaux pour calculs
! --- simplifies des projections sur les noeuds du maillage
        ipx(1)  = ipmmx ; ipx(2)  = ipmx ;
	ipx(3)  = ippx ; ipx(4)  = ipppx ;
	ipy(1)  = ipmmy ; ipy(2)  = ipmy ;
	ipy(3)  = ippy ; ipy(4)  = ipppy ;
        ponpx(1) = ponpmmx ; ponpx(2) = ponpmx ;
	ponpx(3) = ponppx  ; ponpx(4) = ponpppx ;

        ponpy(1) = ponpmmy ; ponpy(2) = ponpmy ;
	ponpy(3) = ponppy  ; ponpy(4) = ponpppy ;

        IF(ipy(1).EQ.-1) ipy(1)=ny-1

        ponpx(1) = ponpmmx ; ponpx(2) = ponpmx ;
	ponpx(3) = ponppx ; ponpx(4) = ponpppx ;
        ponpy(1) = ponpmmy ; ponpy(2) = ponpmy ;
	ponpy(3) = ponppy ; ponpy(4) = ponpppy ;

      RETURN
!     ________________________________
      END SUBROUTINE ponder_ordre3


      SUBROUTINE ponder_ordre4(ip, iesp, ponpx, ponpy, ipx, ipy)
! ======================================================================
! Projection sur la grille a 2 dimensions
! des charge et courants particulaires de l'espece iesp
! On calcule les quatre champs source Cx, Cy, Cz, Rh
! ======================================================================

      USE domaines
      USE particules
      USE particules_klder
      USE champs
      USE temps
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)      :: ip, iesp

      REAL(KIND=kr)    :: rpx, rpy

      INTEGER(KIND=kr), DIMENSION(1:5), INTENT(OUT)  :: ipx, ipy
      REAL(KIND=kr), DIMENSION(1:5), INTENT(OUT)  :: ponpx, ponpy

!      INTEGER         :: i, j


! --- Coordonnees reelles en numero de maille primale
        rpx = (partic(a_qx,ip,iesp)-xmin_gl)*dxi
        rpy = (partic(a_qy,ip,iesp)-ymin_gl)*dyi
! --- Coordonnees entieres primales : lignes encadrant la particule
	ipx(1) = FLOOR(rpx - 1.5_8)
	ipx(2) = ipx(1) + 1
	ipx(3) = ipx(1) + 2
	ipx(4) = ipx(1) + 3
	ipx(5) = ipx(1) + 4

	ipy(1) = FLOOR(rpy - 1.5_8)
	ipy(2) = ipy(1) + 1
	ipy(3) = ipy(1) + 2
	ipy(4) = ipy(1) + 3
	ipy(5) = ipy(1) + 4

! --- Ponderations cubiques des differents sommets primaux
	ponpx(1) = (1.0/24.0)*(5.0/2.0 + (REAL(ipx(1),8) - rpx) )**4

	ponpx(2) = (1.0/96.0)*(55. - 20.*(REAL(ipx(2),8) - rpx)
     &   - 120.*(REAL(ipx(2),8) - rpx)**2
     &   - 80.*(REAL(ipx(2),8) - rpx)**3
     &   - 16.*(REAL(ipx(2),8) - rpx)**4)

	ponpx(3) = (1.0/192.0)*(115. - 120*(REAL(ipx(3),8) - rpx)**2
     &   + 48.*(REAL(ipx(3),8) - rpx)**4 )

	ponpx(4) = (1.0/96.0)*(55. + 20.*(REAL(ipx(4),8) - rpx)
     &   - 120.*(REAL(ipx(4),8) - rpx)**2
     &   + 80.*(REAL(ipx(4),8) - rpx)**3
     &   - 16.*(REAL(ipx(4),8) - rpx)**4)

	ponpx(5) = (1.0/24.0)*(5.0/2.0 - (REAL(ipx(5),8) - rpx) )**4


	ponpy(1) = (1.0/24.0)*(5.0/2.0 + (REAL(ipy(1),8) - rpy) )**4

	ponpy(2) = (1.0/96.0)*(55. - 20.*(REAL(ipy(2),8) - rpy)
     &   - 120.*(REAL(ipy(2),8) - rpy)**2
     &   - 80.*(REAL(ipy(2),8) - rpy)**3
     &   - 16.*(REAL(ipy(2),8) - rpy)**4)

	ponpy(3) = (1.0/192.0)*(115. - 120*(REAL(ipy(3),8) - rpy)**2
     &   + 48.*(REAL(ipy(3),8) - rpy)**4 )

	ponpy(4) = (1.0/96.0)*(55. + 20.*(REAL(ipy(4),8) - rpy)
     &   - 120.*(REAL(ipy(4),8) - rpy)**2
     &   + 80.*(REAL(ipy(4),8) - rpy)**3
     &   - 16.*(REAL(ipy(4),8) - rpy)**4)

	ponpy(5) = (1.0/24.0)*(5.0/2.0 - (REAL(ipy(5),8) - rpy) )**4


	WHERE(ipy(:).EQ.-1) ipy(:)=ny-1
	WHERE(ipy(:).EQ.-2) ipy(:)=ny-2

	IF(ipy(5).EQ.(ny+2)) ipy(5)=2



      RETURN
!     ________________________________
      END SUBROUTINE ponder_ordre4



      SUBROUTINE ponder_ordre1_ik(ip, iesp, ponpx, ponpy, ipx, ipy)
! ======================================================================
! Projection sur la grille a 2 dimensions
! des charge et courants particulaires de l'espece iesp
! On calcule les quatre champs source Cx, Cy, Cz, Rh
! ======================================================================

      USE domaines
      USE champs_iter
      USE temps
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)      :: ip, iesp

      REAL(KIND=kr)    :: rpx, rpy

      INTEGER          :: ipmx, ippx, ipmy, ippy
      REAL(KIND=kr)    :: ponpmx, ponppx,
     &                    ponpmy, ponppy

      INTEGER(KIND=kr), DIMENSION(1:2), INTENT(OUT)  :: ipx, ipy
      REAL(KIND=kr), DIMENSION(1:2), INTENT(OUT)  :: ponpx, ponpy


! ----- on détermine ds quelle maille est la particule
        rpx = (partic_ik(a_qx_ik,ip,iesp)-xmin_gl)*dxi
        rpy = (partic_ik(a_qy_ik,ip,iesp)-ymin_gl)*dyi
! ----- coordonnées entières, lignes encadrant la particule
        ipmx = INT(rpx)
        ippx = ipmx+1
        ipmy = INT(rpy)
        ippy = ipmy+1

        ponpmx = REAl(ippx,8) - rpx
        ponppx = 1._8 - ponpmx
        ponpmy = REAl(ippy,8) - rpy
        ponppy = 1._8 - ponpmy

! --- Initialisation des tableaux pour calculs
! --- simplifies des projections sur les noeuds du maillage
        ipx(1)  = ipmx ; ipx(2)  = ippx
	ipy(1)  = ipmy ; ipy(2)  = ippy

        ponpx(1) = ponpmx ; ponpx(2) = ponppx
        ponpy(1) = ponpmy ; ponpy(2) = ponppy


      RETURN
!     ________________________________
      END SUBROUTINE ponder_ordre1_ik



      SUBROUTINE ponder_ordre2_ik(ip, iesp, ponpx, ponpy, ipx, ipy)
! ======================================================================
! Projection sur la grille a 2 dimensions
! des charge et courants particulaires de l'espece iesp
! On calcule les quatre champs source Cx, Cy, Cz, Rh
! ======================================================================

      USE domaines
      USE champs_iter
      USE temps
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)      :: ip, iesp

      REAL(KIND=kr)    :: rpx, rpy
      INTEGER          :: ipmx, ipctx, ippx, ipmy, ipcty, ippy
      REAL(KIND=kr)    :: ponpmx, ponpctx, ponppx,
     &                    ponpmy, ponpcty, ponppy

      INTEGER(KIND=kr), DIMENSION(1:3), INTENT(OUT)  :: ipx, ipy
      REAL(KIND=kr), DIMENSION(1:3), INTENT(OUT)  :: ponpx, ponpy

!      INTEGER         :: i, j


! --- Coordonnees reelles en numero de maille primale
        rpx = (partic_ik(a_qx_ik,ip,iesp)-xmin_gl)*dxi
        rpy = (partic_ik(a_qy_ik,ip,iesp)-ymin_gl)*dyi
! --- Coordonnees entieres primales : lignes encadrant la particule
        ipmx = FLOOR(rpx-0.5_8)
	ipctx = ipmx+1
        ippx = ipmx+2
        ipmy = FLOOR(rpy-0.5_8)
	ipcty = ipmy+1
        ippy = ipmy+2
! --- Ponderations quadratiques des differents sommets primaux
        ponpmx  = 0.5*( 0.5 + (REAL(ipctx,8) - rpx ) )**2
	ponpctx = 0.75 - ( REAL(ipctx,8) - rpx )**2
	ponppx  = 0.5*( 0.5 - (REAL(ipctx,8) - rpx ) )**2

        ponpmy  = 0.5*( 0.5 + (REAL(ipcty,8) - rpy ) )**2
	ponpcty = 0.75 - ( REAL(ipcty,8) - rpy )**2
	ponppy  = 0.5*( 0.5 - (REAL(ipcty,8) - rpy ) )**2

! --- Initialisation des tableaux pour calculs
! --- simplifies des projections sur les noeuds du maillage
        ipx(1)  = ipmx ; ipx(2)  = ipctx ; ipx(3)  = ippx ;
	ipy(1)  = ipmy ; ipy(2)  = ipcty ; ipy(3)  = ippy ;

        IF(ipy(1).EQ.-1) ipy(1)=ny-1

        ponpx(1) = ponpmx ; ponpx(2) = ponpctx ; ponpx(3) = ponppx ;
        ponpy(1) = ponpmy ; ponpy(2) = ponpcty ; ponpy(3) = ponppy ;


      RETURN

!     ________________________________
      END SUBROUTINE ponder_ordre2_ik


      SUBROUTINE ponder_ordre3_ik(ip, iesp, ponpx, ponpy, ipx, ipy)
! ======================================================================
! Projection sur la grille a 2 dimensions
! des charge et courants particulaires de l'espece iesp
! On calcule les quatre champs source Cx, Cy, Cz, Rh
! ======================================================================

      USE domaines
      USE champs_iter
      USE temps
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)      :: ip, iesp

      REAL(KIND=kr)    :: rpx, rpy
      INTEGER          :: ipmmx, ipmx, ippx, ipppx,
     &                    ipmmy, ipmy, ippy, ipppy
      REAL(KIND=kr)    :: ponpmmx, ponpmx, ponppx, ponpppx,
     &                    ponpmmy, ponpmy, ponppy, ponpppy

      INTEGER(KIND=kr), DIMENSION(1:4), INTENT(OUT)  :: ipx, ipy
      REAL(KIND=kr), DIMENSION(1:4), INTENT(OUT)  :: ponpx, ponpy

!      INTEGER         :: i, j


! --- Coordonnees reelles en numero de maille primale
        rpx = (partic_ik(a_qx_ik,ip,iesp)-xmin_gl)*dxi
        rpy = (partic_ik(a_qy_ik,ip,iesp)-ymin_gl)*dyi
! --- Coordonnees entieres primales : lignes encadrant la particule
	ipmmx = FLOOR(rpx - 1)
	ipmx  = ipmmx + 1
	ippx  = ipmmx + 2
	ipppx = ipmmx + 3
	ipmmy = FLOOR(rpy - 1)
	ipmy  = ipmmy + 1
	ippy  = ipmmy + 2
	ipppy = ipmmy + 3

! --- Ponderations cubiques des differents sommets primaux
	ponpmmx = (4.0/3.0)*(1 + (REAL(ipmmx,8) - rpx)/2 )**3
	ponpmx  = 2.0/3.0 - (REAL(ipmx,8) - rpx)**2 -
     &  (REAL(ipmx,8) - rpx)**3/2
	ponppx  = 2.0/3.0 - (REAL(ippx,8) - rpx)**2 +
     &  (REAL(ippx,8) - rpx)**3/2
	ponpppx = (4.0/3.0)*(1 - (REAL(ipppx,8) - rpx)/2 )**3

	ponpmmy = (4.0/3.0)*(1 + (REAL(ipmmy,8) - rpy)/2 )**3
	ponpmy  = 2.0/3.0 - (REAL(ipmy,8) - rpy)**2 -
     &  (REAL(ipmy,8) - rpy)**3/2
	ponppy  = 2.0/3.0 - (REAL(ippy,8) - rpy)**2 +
     &  (REAL(ippy,8) - rpy)**3/2
	ponpppy = (4.0/3.0)*(1 - (REAL(ipppy,8) - rpy)/2 )**3

! --- Initialisation des tableaux pour calculs
! --- simplifies des projections sur les noeuds du maillage
! --- Initialisation des tableaux pour calculs
! --- simplifies des projections sur les noeuds du maillage
        ipx(1)  = ipmmx ; ipx(2)  = ipmx ;
	ipx(3)  = ippx ; ipx(4)  = ipppx ;
	ipy(1)  = ipmmy ; ipy(2)  = ipmy ;
	ipy(3)  = ippy ; ipy(4)  = ipppy ;
        ponpx(1) = ponpmmx ; ponpx(2) = ponpmx ;
	ponpx(3) = ponppx  ; ponpx(4) = ponpppx ;

        ponpy(1) = ponpmmy ; ponpy(2) = ponpmy ;
	ponpy(3) = ponppy  ; ponpy(4) = ponpppy ;

        IF(ipy(1).EQ.-1) ipy(1)=ny-1

        ponpx(1) = ponpmmx ; ponpx(2) = ponpmx ;
	ponpx(3) = ponppx ; ponpx(4) = ponpppx ;
        ponpy(1) = ponpmmy ; ponpy(2) = ponpmy ;
	ponpy(3) = ponppy ; ponpy(4) = ponpppy ;

      RETURN
!     ________________________________
      END SUBROUTINE ponder_ordre3_ik


      SUBROUTINE ponder_ordre4_ik(ip, iesp, ponpx, ponpy, ipx, ipy)
! ======================================================================
! Projection sur la grille a 2 dimensions
! des charge et courants particulaires de l'espece iesp
! On calcule les quatre champs source Cx, Cy, Cz, Rh
! ======================================================================

      USE domaines
      USE champs_iter
      USE temps
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)      :: ip, iesp

      REAL(KIND=kr)    :: rpx, rpy

      INTEGER(KIND=kr), DIMENSION(1:5), INTENT(OUT)  :: ipx, ipy
      REAL(KIND=kr), DIMENSION(1:5), INTENT(OUT)  :: ponpx, ponpy

!      INTEGER         :: i, j


! --- Coordonnees reelles en numero de maille primale
        rpx = (partic_ik(a_qx_ik,ip,iesp)-xmin_gl)*dxi
        rpy = (partic_ik(a_qy_ik,ip,iesp)-ymin_gl)*dyi
! --- Coordonnees entieres primales : lignes encadrant la particule
	ipx(1) = FLOOR(rpx - 1.5_8)
	ipx(2) = ipx(1) + 1
	ipx(3) = ipx(1) + 2
	ipx(4) = ipx(1) + 3
	ipx(5) = ipx(1) + 4

	ipy(1) = FLOOR(rpy - 1.5_8)
	ipy(2) = ipy(1) + 1
	ipy(3) = ipy(1) + 2
	ipy(4) = ipy(1) + 3
	ipy(5) = ipy(1) + 4

! --- Ponderations cubiques des differents sommets primaux
	ponpx(1) = (1.0/24.0)*(5.0/2.0 + (REAL(ipx(1),8) - rpx) )**4

	ponpx(2) = (1.0/96.0)*(55. - 20.*(REAL(ipx(2),8) - rpx)
     &   - 120.*(REAL(ipx(2),8) - rpx)**2
     &   - 80.*(REAL(ipx(2),8) - rpx)**3
     &   - 16.*(REAL(ipx(2),8) - rpx)**4)

	ponpx(3) = (1.0/192.0)*(115. - 120*(REAL(ipx(3),8) - rpx)**2
     &   + 48.*(REAL(ipx(3),8) - rpx)**4 )

	ponpx(4) = (1.0/96.0)*(55. + 20.*(REAL(ipx(4),8) - rpx)
     &   - 120.*(REAL(ipx(4),8) - rpx)**2
     &   + 80.*(REAL(ipx(4),8) - rpx)**3
     &   - 16.*(REAL(ipx(4),8) - rpx)**4)

	ponpx(5) = (1.0/24.0)*(5.0/2.0 - (REAL(ipx(5),8) - rpx) )**4


	ponpy(1) = (1.0/24.0)*(5.0/2.0 + (REAL(ipy(1),8) - rpy) )**4

	ponpy(2) = (1.0/96.0)*(55. - 20.*(REAL(ipy(2),8) - rpy)
     &   - 120.*(REAL(ipy(2),8) - rpy)**2
     &   - 80.*(REAL(ipy(2),8) - rpy)**3
     &   - 16.*(REAL(ipy(2),8) - rpy)**4)

	ponpy(3) = (1.0/192.0)*(115. - 120*(REAL(ipy(3),8) - rpy)**2
     &   + 48.*(REAL(ipy(3),8) - rpy)**4 )

	ponpy(4) = (1.0/96.0)*(55. + 20.*(REAL(ipy(4),8) - rpy)
     &   - 120.*(REAL(ipy(4),8) - rpy)**2
     &   + 80.*(REAL(ipy(4),8) - rpy)**3
     &   - 16.*(REAL(ipy(4),8) - rpy)**4)

	ponpy(5) = (1.0/24.0)*(5.0/2.0 - (REAL(ipy(5),8) - rpy) )**4


	WHERE(ipy(:).EQ.-1) ipy(:)=ny-1
	WHERE(ipy(:).EQ.-2) ipy(:)=ny-2

	IF(ipy(5).EQ.(ny+2)) ipy(5)=2



      RETURN
!     ________________________________
      END SUBROUTINE ponder_ordre4_ik



      SUBROUTINE ponder_ordre1_dual(ip, iesp, pondx, pondy, idx, idy)
! ======================================================================
! Projection sur la grille a 2 dimensions
! des charge et courants particulaires de l'espece iesp
! On calcule les quatre champs source Cx, Cy, Cz, Rh
! ======================================================================

      USE domaines
      USE particules
      USE particules_klder
      USE champs
      USE temps
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)      :: ip, iesp

      REAL(KIND=kr)    :: rdx, rdy

      INTEGER          :: idmx, idpx, idmy, idpy
      REAL(KIND=kr)    :: pondmx, pondpx,
     &                    pondmy, pondpy

      INTEGER(KIND=kr), DIMENSION(1:2), INTENT(OUT)  :: idx, idy
      REAL(KIND=kr), DIMENSION(1:2), INTENT(OUT)  :: pondx, pondy


! ----- on détermine ds quelle maille est la particule
        rdx = (partic(a_qx,ip,iesp)-xmin_gl)*dxi +0.5_8
        rdy = (partic(a_qy,ip,iesp)-ymin_gl)*dyi +0.5_8
! ----- coordonnées entières, lignes encadrant la particule
        idmx = INT(rdx)
        idpx = idmx+1
        idmy = INT(rdy)
        idpy = idmy+1

        pondmx = REAl(idpx,8) - rdx
        pondpx = 1._8 - pondmx
        pondmy = REAl(idpy,8) - rdy
        pondpy = 1._8 - pondmy

! --- Initialisation des tableaux pour calculs
! --- simplifies des projections sur les noeuds du maillage
        idx(1)  = idmx ; idx(2)  = idpx
	idy(1)  = idmy ; idy(2)  = idpy

        pondx(1) = pondmx ; pondx(2) = pondpx
        pondy(1) = pondmy ; pondy(2) = pondpy


      RETURN
!     ________________________________
      END SUBROUTINE ponder_ordre1_dual



      SUBROUTINE ponder_ordre2_dual(ip, iesp, pondx, pondy, idx, idy)
! ======================================================================
! Projection sur la grille a 2 dimensions
! des charge et courants particulaires de l'espece iesp
! On calcule les quatre champs source Cx, Cy, Cz, Rh
! ======================================================================

      USE domaines
      USE particules
      USE particules_klder
      USE champs
      USE temps
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)      :: ip, iesp

      REAL(KIND=kr)    :: rdx, rdy
      INTEGER          :: idmx, idctx, idpx, idmy, idcty, idpy
      REAL(KIND=kr)    :: pondmx, pondctx, pondpx,
     &                    pondmy, pondcty, pondpy

      INTEGER(KIND=kr), DIMENSION(1:3), INTENT(OUT)  :: idx, idy
      REAL(KIND=kr), DIMENSION(1:3), INTENT(OUT)  :: pondx, pondy

!      INTEGER         :: i, j


! --- Coordonnees reelles en numero de maille primale
        rdx = (partic(a_qx,ip,iesp)-xmin_gl)*dxi +0.5_8
        rdy = (partic(a_qy,ip,iesp)-ymin_gl)*dyi +0.5_8
! --- Coordonnees entieres primales : lignes encadrant la particule
        idmx = FLOOR(rdx-0.5_8)
	idctx = idmx+1
        idpx = idmx+2
        idmy = FLOOR(rdy-0.5_8)
	idcty = idmy+1
        idpy = idmy+2
! --- Ponderations quadratiques des differents sommets primaux
        pondmx  = 0.5*( 0.5 + (REAL(idctx,8) - rdx ) )**2
	pondctx = 0.75 - ( REAL(idctx,8) - rdx )**2
	pondpx  = 0.5*( 0.5 - (REAL(idctx,8) - rdx ) )**2

        pondmy  = 0.5*( 0.5 + (REAL(idcty,8) - rdy ) )**2
	pondcty = 0.75 - ( REAL(idcty,8) - rdy )**2
	pondpy  = 0.5*( 0.5 - (REAL(idcty,8) - rdy ) )**2

! --- Initialisation des tableaux pour calculs
! --- simplifies des projections sur les noeuds du maillage
        idx(1)  = idmx ; idx(2)  = idctx ; idx(3)  = idpx ;
	idy(1)  = idmy ; idy(2)  = idcty ; idy(3)  = idpy ;

        pondx(1) = pondmx ; pondx(2) = pondctx ; pondx(3) = pondpx ;
        pondy(1) = pondmy ; pondy(2) = pondcty ; pondy(3) = pondpy ;


      RETURN

!     ________________________________
      END SUBROUTINE ponder_ordre2_dual


      SUBROUTINE ponder_ordre3_dual(ip, iesp, pondx, pondy, idx, idy)
! ======================================================================
! Projection sur la grille a 2 dimensions
! des charge et courants particulaires de l'espece iesp
! On calcule les quatre champs source Cx, Cy, Cz, Rh
! ======================================================================

      USE domaines
      USE particules
      USE particules_klder
      USE champs
      USE temps
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)      :: ip, iesp

      REAL(KIND=kr)    :: rdx, rdy
      INTEGER          :: idmmx, idmx, idpx, idppx,
     &                    idmmy, idmy, idpy, idppy
      REAL(KIND=kr)    :: pondmmx, pondmx, pondpx, pondppx,
     &                    pondmmy, pondmy, pondpy, pondppy

      INTEGER(KIND=kr), DIMENSION(1:4), INTENT(OUT)  :: idx, idy
      REAL(KIND=kr), DIMENSION(1:4), INTENT(OUT)  :: pondx, pondy

!      INTEGER         :: i, j


! --- Coordonnees reelles en numero de maille primale
        rdx = (partic(a_qx,ip,iesp)-xmin_gl)*dxi +0.5_8
        rdy = (partic(a_qy,ip,iesp)-ymin_gl)*dyi +0.5_8
! --- Coordonnees entieres duales : lignes encadrant la particule
	idmmx = FLOOR(rdx - 1)
	idmx  = idmmx + 1
	idpx  = idmmx + 2
	idppx = idmmx + 3
	idmmy = FLOOR(rdy - 1)
	idmy  = idmmy + 1
	idpy  = idmmy + 2
	idppy = idmmy + 3
! --- Ponderations quadratiques des differents sommets duaux
	pondmmx = (4.0/3.0)*(1 + (REAL(idmmx,8) - rdx)/2 )**3
	pondmx  = 2.0/3.0 - (REAL(idmx,8) - rdx)**2 -
     &  (REAL(idmx,8) - rdx)**3/2
	pondpx  = 2.0/3.0 - (REAL(idpx,8) - rdx)**2 +
     &  (REAL(idpx,8) - rdx)**3/2
	pondppx = (4.0/3.0)*(1 - (REAL(idppx,8) - rdx)/2 )**3

	pondmmy = (4.0/3.0)*(1 + (REAL(idmmy,8) - rdy)/2 )**3
	pondmy  = 2.0/3.0 - (REAL(idmy,8) - rdy)**2 -
     &  (REAL(idmy,8) - rdy)**3/2
	pondpy  = 2.0/3.0 - (REAL(idpy,8) - rdy)**2 +
     &  (REAL(idpy,8) - rdy)**3/2
	pondppy = (4.0/3.0)*(1 - (REAL(idppy,8) - rdy)/2 )**3


! --- Initialisation des tableaux pour calculs
! --- simplifies des projections sur les noeuds du maillage
        idx(1)  = idmmx ; idx(2)  = idmx ;
	idx(3)  = idpx ; idx(4)  = idppx ;
	idy(1)  = idmmy ; idy(2)  = idmy ;
	idy(3)  = idpy ; idy(4)  = idppy ;

        IF(idy(1).EQ.-1) idy(1)=ny-1
        IF(idy(4).EQ.(ny+2)) idy(4)=2

	pondx(1) = pondmmx ; pondx(2) = pondmx ;
	pondx(3) = pondpx  ; pondx(4) = pondppx ;

	pondy(1) = pondmmy ; pondy(2) = pondmy ;
	pondy(3) = pondpy  ; pondy(4) = pondppy ;


      RETURN
!     ________________________________
      END SUBROUTINE ponder_ordre3_dual


      SUBROUTINE ponder_ordre4_dual(ip, iesp, pondx, pondy, idx, idy)
! ======================================================================
! Projection sur la grille a 2 dimensions
! des charge et courants particulaires de l'espece iesp
! On calcule les quatre champs source Cx, Cy, Cz, Rh
! ======================================================================

      USE domaines
      USE particules
      USE particules_klder
      USE champs
      USE temps
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)      :: ip, iesp

      REAL(KIND=kr)    :: rdx, rdy

      INTEGER(KIND=kr), DIMENSION(1:5), INTENT(OUT)  :: idx, idy
      REAL(KIND=kr), DIMENSION(1:5), INTENT(OUT)  :: pondx, pondy

!      INTEGER         :: i, j


! --- Coordonnees reelles en numero de maille primale
        rdx = (partic(a_qx,ip,iesp)-xmin_gl)*dxi +0.5_8
        rdy = (partic(a_qy,ip,iesp)-ymin_gl)*dyi +0.5_8
! --- Coordonnees entieres primales : lignes encadrant la particule
	idx(1) = FLOOR(rdx - 1.5_8)
	idx(2) = idx(1) + 1
	idx(3) = idx(1) + 2
	idx(4) = idx(1) + 3
	idx(5) = idx(1) + 4

	idy(1) = FLOOR(rdy - 1.5_8)
	idy(2) = idy(1) + 1
	idy(3) = idy(1) + 2
	idy(4) = idy(1) + 3
	idy(5) = idy(1) + 4

! --- Ponderations cubiques des differents sommets primaux
	pondx(1) = (1.0/24.0)*(5.0/2.0 + (REAL(idx(1),8) - rdx) )**4

	pondx(2) = (1.0/96.0)*(55. - 20.*(REAL(idx(2),8) - rdx)
     &   - 120.*(REAL(idx(2),8) - rdx)**2
     &   - 80.*(REAL(idx(2),8) - rdx)**3
     &   - 16.*(REAL(idx(2),8) - rdx)**4)

	pondx(3) = (1.0/192.0)*(115. - 120*(REAL(idx(3),8) - rdx)**2
     &   + 48.*(REAL(idx(3),8) - rdx)**4 )

	pondx(4) = (1.0/96.0)*(55. + 20.*(REAL(idx(4),8) - rdx)
     &   - 120.*(REAL(idx(4),8) - rdx)**2
     &   + 80.*(REAL(idx(4),8) - rdx)**3
     &   - 16.*(REAL(idx(4),8) - rdx)**4)

	pondx(5) = (1.0/24.0)*(5.0/2.0 - (REAL(idx(5),8) - rdx) )**4


	pondy(1) = (1.0/24.0)*(5.0/2.0 + (REAL(idy(1),8) - rdy) )**4

	pondy(2) = (1.0/96.0)*(55. - 20.*(REAL(idy(2),8) - rdy)
     &   - 120.*(REAL(idy(2),8) - rdy)**2
     &   - 80.*(REAL(idy(2),8) - rdy)**3
     &   - 16.*(REAL(idy(2),8) - rdy)**4)

	pondy(3) = (1.0/192.0)*(115. - 120*(REAL(idy(3),8) - rdy)**2
     &   + 48.*(REAL(idy(3),8) - rdy)**4 )

	pondy(4) = (1.0/96.0)*(55. + 20.*(REAL(idy(4),8) - rdy)
     &   - 120.*(REAL(idy(4),8) - rdy)**2
     &   + 80.*(REAL(idy(4),8) - rdy)**3
     &   - 16.*(REAL(idy(4),8) - rdy)**4)

	pondy(5) = (1.0/24.0)*(5.0/2.0 - (REAL(idy(5),8) - rdy) )**4


        IF(idy(1).EQ.-1) idy(1)=ny-1
        IF(idy(5).EQ.(ny+2)) idy(5)=2



      RETURN
!     ________________________________
      END SUBROUTINE ponder_ordre4_dual



      SUBROUTINE ponder_ordre1_ik_dual(ip, iesp, pondx, pondy, idx, idy)
! ======================================================================
! Projection sur la grille a 2 dimensions
! des charge et courants particulaires de l'espece iesp
! On calcule les quatre champs source Cx, Cy, Cz, Rh
! ======================================================================

      USE domaines
      USE champs_iter
      USE temps
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)      :: ip, iesp

      REAL(KIND=kr)    :: rdx, rdy

      INTEGER          :: idmx, idpx, idmy, idpy
      REAL(KIND=kr)    :: pondmx, pondpx,
     &                    pondmy, pondpy

      INTEGER(KIND=kr), DIMENSION(1:2), INTENT(OUT)  :: idx, idy
      REAL(KIND=kr), DIMENSION(1:2), INTENT(OUT)  :: pondx, pondy


! ----- on détermine ds quelle maille est la particule
        rdx = (partic_ik(a_qx_ik,ip,iesp)-xmin_gl)*dxi +0.5_8
        rdy = (partic_ik(a_qy_ik,ip,iesp)-ymin_gl)*dyi +0.5_8
! ----- coordonnées entières, lignes encadrant la particule
        idmx = INT(rdx)
        idpx = idmx+1
        idmy = INT(rdy)
        idpy = idmy+1

        pondmx = REAl(idpx,8) - rdx
        pondpx = 1._8 - pondmx
        pondmy = REAl(idpy,8) - rdy
        pondpy = 1._8 - pondmy

! --- Initialisation des tableaux pour calculs
! --- simplifies des projections sur les noeuds du maillage
        idx(1)  = idmx ; idx(2)  = idpx
	idy(1)  = idmy ; idy(2)  = idpy

        pondx(1) = pondmx ; pondx(2) = pondpx
        pondy(1) = pondmy ; pondy(2) = pondpy


      RETURN
!     ________________________________
      END SUBROUTINE ponder_ordre1_ik_dual



      SUBROUTINE ponder_ordre2_ik_dual(ip, iesp, pondx, pondy, idx, idy)
! ======================================================================
! Projection sur la grille a 2 dimensions
! des charge et courants particulaires de l'espece iesp
! On calcule les quatre champs source Cx, Cy, Cz, Rh
! ======================================================================

      USE domaines
      USE champs_iter
      USE temps
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)      :: ip, iesp

      REAL(KIND=kr)    :: rdx, rdy
      INTEGER          :: idmx, idctx, idpx, idmy, idcty, idpy
      REAL(KIND=kr)    :: pondmx, pondctx, pondpx,
     &                    pondmy, pondcty, pondpy

      INTEGER(KIND=kr), DIMENSION(1:3), INTENT(OUT)  :: idx, idy
      REAL(KIND=kr), DIMENSION(1:3), INTENT(OUT)  :: pondx, pondy

!      INTEGER         :: i, j


! --- Coordonnees reelles en numero de maille primale
        rdx = (partic_ik(a_qx_ik,ip,iesp)-xmin_gl)*dxi +0.5_8
        rdy = (partic_ik(a_qy_ik,ip,iesp)-ymin_gl)*dyi +0.5_8
! --- Coordonnees entieres primales : lignes encadrant la particule
        idmx = FLOOR(rdx-0.5_8)
	idctx = idmx+1
        idpx = idmx+2
        idmy = FLOOR(rdy-0.5_8)
	idcty = idmy+1
        idpy = idmy+2
! --- Ponderations quadratiques des differents sommets primaux
        pondmx  = 0.5*( 0.5 + (REAL(idctx,8) - rdx ) )**2
	pondctx = 0.75 - ( REAL(idctx,8) - rdx )**2
	pondpx  = 0.5*( 0.5 - (REAL(idctx,8) - rdx ) )**2

        pondmy  = 0.5*( 0.5 + (REAL(idcty,8) - rdy ) )**2
	pondcty = 0.75 - ( REAL(idcty,8) - rdy )**2
	pondpy  = 0.5*( 0.5 - (REAL(idcty,8) - rdy ) )**2

! --- Initialisation des tableaux pour calculs
! --- simplifies des projections sur les noeuds du maillage
        idx(1)  = idmx ; idx(2)  = idctx ; idx(3)  = idpx ;
	idy(1)  = idmy ; idy(2)  = idcty ; idy(3)  = idpy ;

        pondx(1) = pondmx ; pondx(2) = pondctx ; pondx(3) = pondpx ;
        pondy(1) = pondmy ; pondy(2) = pondcty ; pondy(3) = pondpy ;


      RETURN

!     ________________________________
      END SUBROUTINE ponder_ordre2_ik_dual


      SUBROUTINE ponder_ordre3_ik_dual(ip, iesp, pondx, pondy, idx, idy)
! ======================================================================
! Projection sur la grille a 2 dimensions
! des charge et courants particulaires de l'espece iesp
! On calcule les quatre champs source Cx, Cy, Cz, Rh
! ======================================================================

      USE domaines
      USE champs_iter
      USE temps
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)      :: ip, iesp

      REAL(KIND=kr)    :: rdx, rdy
      INTEGER          :: idmmx, idmx, idpx, idppx,
     &                    idmmy, idmy, idpy, idppy
      REAL(KIND=kr)    :: pondmmx, pondmx, pondpx, pondppx,
     &                    pondmmy, pondmy, pondpy, pondppy

      INTEGER(KIND=kr), DIMENSION(1:4), INTENT(OUT)  :: idx, idy
      REAL(KIND=kr), DIMENSION(1:4), INTENT(OUT)  :: pondx, pondy

!      INTEGER         :: i, j



! --- Coordonnees reelles en numero de maille primale
        rdx = (partic_ik(a_qx_ik,ip,iesp)-xmin_gl)*dxi +0.5_8
        rdy = (partic_ik(a_qy_ik,ip,iesp)-ymin_gl)*dyi +0.5_8
! --- Coordonnees entieres duales : lignes encadrant la particule
	idmmx = FLOOR(rdx - 1)
	idmx  = idmmx + 1
	idpx  = idmmx + 2
	idppx = idmmx + 3
	idmmy = FLOOR(rdy - 1)
	idmy  = idmmy + 1
	idpy  = idmmy + 2
	idppy = idmmy + 3
! --- Ponderations quadratiques des differents sommets duaux
	pondmmx = (4.0/3.0)*(1 + (REAL(idmmx,8) - rdx)/2 )**3
	pondmx  = 2.0/3.0 - (REAL(idmx,8) - rdx)**2 -
     &  (REAL(idmx,8) - rdx)**3/2
	pondpx  = 2.0/3.0 - (REAL(idpx,8) - rdx)**2 +
     &  (REAL(idpx,8) - rdx)**3/2
	pondppx = (4.0/3.0)*(1 - (REAL(idppx,8) - rdx)/2 )**3

	pondmmy = (4.0/3.0)*(1 + (REAL(idmmy,8) - rdy)/2 )**3
	pondmy  = 2.0/3.0 - (REAL(idmy,8) - rdy)**2 -
     &  (REAL(idmy,8) - rdy)**3/2
	pondpy  = 2.0/3.0 - (REAL(idpy,8) - rdy)**2 +
     &  (REAL(idpy,8) - rdy)**3/2
	pondppy = (4.0/3.0)*(1 - (REAL(idppy,8) - rdy)/2 )**3


! --- Initialisation des tableaux pour calculs
! --- simplifies des projections sur les noeuds du maillage
        idx(1)  = idmmx ; idx(2)  = idmx ;
	idx(3)  = idpx ; idx(4)  = idppx ;
	idy(1)  = idmmy ; idy(2)  = idmy ;
	idy(3)  = idpy ; idy(4)  = idppy ;

        IF(idy(1).EQ.-1) idy(1)=ny-1
        IF(idy(4).EQ.(ny+1)) idy(4)=1

	pondx(1) = pondmmx ; pondx(2) = pondmx ;
	pondx(3) = pondpx  ; pondx(4) = pondppx ;

	pondy(1) = pondmmy ; pondy(2) = pondmy ;
	pondy(3) = pondpy  ; pondy(4) = pondppy ;


      RETURN
!     ________________________________
      END SUBROUTINE ponder_ordre3_ik_dual


      SUBROUTINE ponder_ordre4_ik_dual(ip, iesp, pondx, pondy, idx, idy)
! ======================================================================
! Projection sur la grille a 2 dimensions
! des charge et courants particulaires de l'espece iesp
! On calcule les quatre champs source Cx, Cy, Cz, Rh
! ======================================================================

      USE domaines
      USE champs_iter
      USE temps
      USE erreurs


      IMPLICIT NONE

      INTEGER, INTENT(IN)      :: ip, iesp

      REAL(KIND=kr)    :: rdx, rdy

      INTEGER(KIND=kr), DIMENSION(1:5), INTENT(OUT)  :: idx, idy
      REAL(KIND=kr), DIMENSION(1:5), INTENT(OUT)  :: pondx, pondy

!      INTEGER         :: i, j


! --- Coordonnees reelles en numero de maille primale
        rdx = (partic_ik(a_qx_ik,ip,iesp)-xmin_gl)*dxi +0.5_8
        rdy = (partic_ik(a_qy_ik,ip,iesp)-ymin_gl)*dyi +0.5_8
! --- Coordonnees entieres primales : lignes encadrant la particule
	idx(1) = FLOOR(rdx - 1.5_8)
	idx(2) = idx(1) + 1
	idx(3) = idx(1) + 2
	idx(4) = idx(1) + 3
	idx(5) = idx(1) + 4

	idy(1) = FLOOR(rdy - 1.5_8)
	idy(2) = idy(1) + 1
	idy(3) = idy(1) + 2
	idy(4) = idy(1) + 3
	idy(5) = idy(1) + 4

! --- Ponderations cubiques des differents sommets primaux
	pondx(1) = (1.0/24.0)*(5.0/2.0 + (REAL(idx(1),8) - rdx) )**4

	pondx(2) = (1.0/96.0)*(55. - 20.*(REAL(idx(2),8) - rdx)
     &   - 120.*(REAL(idx(2),8) - rdx)**2
     &   - 80.*(REAL(idx(2),8) - rdx)**3
     &   - 16.*(REAL(idx(2),8) - rdx)**4)

	pondx(3) = (1.0/192.0)*(115. - 120*(REAL(idx(3),8) - rdx)**2
     &   + 48.*(REAL(idx(3),8) - rdx)**4 )

	pondx(4) = (1.0/96.0)*(55. + 20.*(REAL(idx(4),8) - rdx)
     &   - 120.*(REAL(idx(4),8) - rdx)**2
     &   + 80.*(REAL(idx(4),8) - rdx)**3
     &   - 16.*(REAL(idx(4),8) - rdx)**4)

	pondx(5) = (1.0/24.0)*(5.0/2.0 - (REAL(idx(5),8) - rdx) )**4


	pondy(1) = (1.0/24.0)*(5.0/2.0 + (REAL(idy(1),8) - rdy) )**4

	pondy(2) = (1.0/96.0)*(55. - 20.*(REAL(idy(2),8) - rdy)
     &   - 120.*(REAL(idy(2),8) - rdy)**2
     &   - 80.*(REAL(idy(2),8) - rdy)**3
     &   - 16.*(REAL(idy(2),8) - rdy)**4)

	pondy(3) = (1.0/192.0)*(115. - 120*(REAL(idy(3),8) - rdy)**2
     &   + 48.*(REAL(idy(3),8) - rdy)**4 )

	pondy(4) = (1.0/96.0)*(55. + 20.*(REAL(idy(4),8) - rdy)
     &   - 120.*(REAL(idy(4),8) - rdy)**2
     &   + 80.*(REAL(idy(4),8) - rdy)**3
     &   - 16.*(REAL(idy(4),8) - rdy)**4)

	pondy(5) = (1.0/24.0)*(5.0/2.0 - (REAL(idy(5),8) - rdy) )**4


        IF(idy(1).EQ.-1) idy(1)=ny-1
        IF(idy(5).EQ.(ny+2)) idy(5)=2



      RETURN
!     ________________________________
      END SUBROUTINE ponder_ordre4_ik_dual



      SUBROUTINE champa_2d_gradE(gradE, ponpx, ponpy, ipx, ipy)
! ======================================================================
! Calcul des champs necessaires, aux positions des particules
! Geometrie 2d1/2, on calcule le champ field
! ======================================================================

      USE domaines
      USE champs
      USE temps
      USE particules
      USE erreurs

      IMPLICIT NONE

      INTEGER(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN)::
     &                 ipx, ipy
      REAL(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN)   ::
     &                 ponpx, ponpy

      REAL(KIND=kr), DIMENSION(1:3,1:3), INTENT(OUT) ::  gradE

      REAL(KIND=kr)   ::    aux_i, aux_j

      INTEGER         :: i, j, ord


      ord= ordre_interp+1

! --- Champ dexdx : (primal, primal)
        aux_j = 0
	DO j=1,ord
          aux_i = 0
	  DO i=1,ord
	    aux_i = aux_i + dexdx(ipx(i), ipy(j))*ponpx(i)
	  ENDDO
	  aux_j = aux_j + aux_i*ponpy(j)
	ENDDO
        gradE(1,1) = aux_j

! --- Champ dexdy : (primal, primal)
        aux_j = 0
	DO j=1,ord
          aux_i = 0
	  DO i=1,ord
	    aux_i = aux_i + dexdy(ipx(i), ipy(j))*ponpx(i)
	  ENDDO
	  aux_j = aux_j + aux_i*ponpy(j)
	ENDDO
        gradE(2,1) = aux_j
! --- Champ dexdx : (primal, primal)
        gradE(3,1) = 0.

! --- Champ deydx : (primal, primal)
        aux_j = 0
	DO j=1,ord
          aux_i = 0
	  DO i=1,ord
	    aux_i = aux_i + deydx(ipx(i), ipy(j))*ponpx(i)
	  ENDDO
	  aux_j = aux_j + aux_i*ponpy(j)
	ENDDO
        gradE(1,2) = aux_j

! --- Champ deydy : (primal, primal)
        aux_j = 0
	DO j=1,ord
          aux_i = 0
	  DO i=1,ord
	    aux_i = aux_i + deydy(ipx(i), ipy(j))*ponpx(i)
	  ENDDO
	  aux_j = aux_j + aux_i*ponpy(j)
	ENDDO
        gradE(2,2) = aux_j
! --- Champ deydz : (primal, primal)
        gradE(3,2) = 0.

! --- Champ dezdx : (primal, primal)
        aux_j = 0
	DO j=1,ord
          aux_i = 0
	  DO i=1,ord
	    aux_i = aux_i + dezdx(ipx(i), ipy(j))*ponpx(i)
	  ENDDO
	  aux_j = aux_j + aux_i*ponpy(j)
	ENDDO
        gradE(1,3) = aux_j

! --- Champ dezdy : (primal, primal)
        aux_j = 0
	DO j=1,ord
          aux_i = 0
	  DO i=1,ord
	    aux_i = aux_i + dezdy(ipx(i), ipy(j))*ponpx(i)
	  ENDDO
	  aux_j = aux_j + aux_i*ponpy(j)
	ENDDO
        gradE(2,3) = aux_j
! --- Champ dezdz : (primal, primal)
        gradE(3,3) = 0.


      RETURN
!     ___________________________
      END SUBROUTINE champa_2d_gradE



      SUBROUTINE projpa_2d_rho_ik(iesp,
     &                ponpx, ponpy, ipx, ipy, aux_rh)
! ======================================================================
! Projection sur la grille a 2 dimensions
! des charge et courants particulaires de l'espece iesp
! On calcule les quatre champs source Cx, Cy, Cz, Rh
! ======================================================================

      USE domaines
      USE champs_iter
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)          :: iesp

      INTEGER(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN) ::
     &       ipx, ipy

      REAL(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN) ::
     &       ponpx, ponpy

      REAL(KIND=kr), INTENT(IN)    ::  aux_rh

      INTEGER         :: i, j, ord

      ord= ordre_interp+1


! --- Champ Rh :
          DO j=1,ord
            DO i=1,ord
               rho_esp2_ik(ipx(i),ipy(j),iesp) =
     &         rho_esp2_ik(ipx(i),ipy(j),iesp) +
     &         (ponpx(i)*ponpy(j))*aux_rh
            ENDDO
          ENDDO


      RETURN
!     ________________________________
      END SUBROUTINE  projpa_2d_rho_ik



      SUBROUTINE projpa_2dx3dv_rjxyz(iesp,
     &                ponpx, ponpy, ipx, ipy, vxe, vye, vze, aux_rh)
! ======================================================================
! Projection sur la grille a 2 dimensions
! des charge et courants particulaires de l'espece iesp
! On calcule les quatre champs source Cx, Cy, Cz, Rh
! ======================================================================

      USE domaines
      USE particules
      USE champs
      USE temps
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)          :: iesp

      INTEGER(KIND=kr), DIMENSION(1:ordre_interp +1), INTENT(IN) ::
     &      ipx, ipy
      REAL(KIND=kr), DIMENSION(1:ordre_interp +1), INTENT(IN) ::
     &      ponpx, ponpy

      REAL(KIND=kr), INTENT(IN)    :: vxe, vye, vze, aux_rh

      INTEGER         :: i, j, ord


      ord= ordre_interp +1

! --- Champ Cx :
          DO j=1,ord
            DO i=1,ord
               cjx_esp_2(ipx(i),ipy(j),iesp) =
     &         cjx_esp_2(ipx(i),ipy(j),iesp) +
     &         (ponpx(i)*ponpy(j))*vxe*aux_rh
            ENDDO
          ENDDO

! --- Champ Cy :
          DO j=1,ord
            DO i=1,ord
               cjy_esp_2(ipx(i),ipy(j),iesp) =
     &         cjy_esp_2(ipx(i),ipy(j),iesp) +
     &         (ponpx(i)*ponpy(j))*vye*aux_rh
            ENDDO
          ENDDO

! --- Champ Cz :
          DO j=1,ord
            DO i=1,ord
               cjz_esp_2(ipx(i),ipy(j),iesp) =
     &         cjz_esp_2(ipx(i),ipy(j),iesp) +
     &         (ponpx(i)*ponpy(j))*vze*aux_rh
            ENDDO
          ENDDO


      RETURN
!     ________________________________
      END SUBROUTINE projpa_2dx3dv_rjxyz



      SUBROUTINE projpa_2dx3dv_rho(iesp, ponpx, ponpy,
     &                ipx, ipy, aux_rh)
! ======================================================================
! Projection sur la grille a 2 dimensions
! des charge et courants particulaires de l'espece iesp
! On calcule les quatre champs source Cx, Cy, Cz, Rh
! ======================================================================

      USE domaines
      USE particules
      USE champs
      USE temps
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)             :: iesp

      INTEGER(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN) ::
     &     ipx, ipy
      REAL(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN) ::
     &     ponpx, ponpy

      REAL(KIND=kr), INTENT(IN)    ::  aux_rh

      INTEGER         :: i, j, ord

      ord=ordre_interp+1

! --- Champ Rh :
          DO j=1,ord
            DO i=1,ord
               rho_esp_2(ipx(i),ipy(j),iesp) =
     &         rho_esp_2(ipx(i),ipy(j),iesp) +
     &         (ponpx(i)*ponpy(j))*aux_rh
            ENDDO
          ENDDO


      RETURN
!     ________________________________
      END SUBROUTINE projpa_2dx3dv_rho



      SUBROUTINE projpa_2dx3dv_dgxyz(iesp, ponpx, ponpy,
     &                ipx, ipy, vxe, vye, vze, aux_rh)
! ======================================================================
! Projection sur la grille a 2 dimensions
! des charge et courants particulaires de l'espece iesp
! On calcule les quatre champs source Cx, Cy, Cz, Rh
! ======================================================================

      USE domaines
      USE particules
      USE champs
      USE temps
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)             :: iesp

      INTEGER(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN) ::
     &     ipx, ipy
      REAL(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN) ::
     &     ponpx, ponpy

      REAL(KIND=kr), INTENT(IN)    :: vxe, vye, vze, aux_rh

      INTEGER         :: i, j, ord

      ord=ordre_interp+1

! --- Champ Rh :
          DO j=1,ord
            DO i=1,ord
               rho_esp_2(ipx(i),ipy(j),iesp) =
     &         rho_esp_2(ipx(i),ipy(j),iesp) +
     &         (ponpx(i)*ponpy(j))*aux_rh
            ENDDO
          ENDDO

! --- Champ Cx :
          DO j=1,ord
            DO i=1,ord
               dgx_esp_2(ipx(i),ipy(j),iesp) =
     &         dgx_esp_2(ipx(i),ipy(j),iesp) +
     &         (ponpx(i)*ponpy(j))*vxe*aux_rh
            ENDDO
          ENDDO

! --- Champ Cy :
          DO j=1,ord
            DO i=1,ord
               dgy_esp_2(ipx(i),ipy(j),iesp) =
     &         dgy_esp_2(ipx(i),ipy(j),iesp) +
     &         (ponpx(i)*ponpy(j))*vye*aux_rh
            ENDDO
          ENDDO

! --- Champ Cz :
          DO j=1,ord
            DO i=1,ord
               dgz_esp_2(ipx(i),ipy(j),iesp) =
     &         dgz_esp_2(ipx(i),ipy(j),iesp) +
     &         (ponpx(i)*ponpy(j))*vze*aux_rh
            ENDDO
          ENDDO


      RETURN
!     ________________________________
      END SUBROUTINE projpa_2dx3dv_dgxyz



      SUBROUTINE champa_2dx3dv_bxyz(ip, iesp,
     &                ponpx, ponpy, ipx, ipy)
! ======================================================================
! Calcul des champs necessaires, aux positions des particules
! Geometrie 2d1/2, on calcule les six champs Ex, Ey, Ez, Bx, By, Bz
! ======================================================================

      USE domaines
      USE champs
      USE temps
      USE particules
      USE particules_klder
      USE erreurs

      IMPLICIT NONE

      INTEGER, INTENT(IN)   ::  ip, iesp

      INTEGER(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN)::
     &          ipx, ipy
      REAL(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN)   ::
     &          ponpx, ponpy

      REAL(KIND=kr)   ::    aux_i, aux_j

      INTEGER         :: i, j, ord


      ord= ordre_interp+1

! --- Champ Bx : (primal)
          aux_j = 0
	  DO j=1,ord
            aux_i = 0
	    DO i=1,ord
	      aux_i = aux_i + bx(ipx(i), ipy(j))*ponpx(i)
	    ENDDO
	    aux_j = aux_j + aux_i*ponpy(j)
	  ENDDO

        partic(a_bxp, ip, iesp) = aux_j

! --- Champ By : (primal)
          aux_j = 0
	  DO j=1,ord
            aux_i = 0
	    DO i=1,ord
	      aux_i = aux_i + by(ipx(i), ipy(j))*ponpx(i)
	    ENDDO
	    aux_j = aux_j + aux_i*ponpy(j)
	  ENDDO

        partic(a_byp, ip, iesp) = aux_j

! --- Champ Bz : (primal)
          aux_j = 0
	  DO j=1,ord
            aux_i = 0
	    DO i=1,ord
	      aux_i = aux_i + bz(ipx(i), ipy(j))*ponpx(i)
	    ENDDO
	    aux_j = aux_j + aux_i*ponpy(j)
	  ENDDO

        partic(a_bzp, ip, iesp) = aux_j

      RETURN
!     ___________________________
      END SUBROUTINE champa_2dx3dv_bxyz



      SUBROUTINE champa_2d_exyz1(ex1, ey1, ez1,
     &                ponpx, ponpy, ipx, ipy)
! ======================================================================
! Calcul des champs necessaires, aux positions des particules
! Geometrie 2d1/2, on calcule les six champs Ex, Ey, Ez, Bx, By, Bz
! ======================================================================

      USE domaines
      USE champs
      USE temps
      USE particules
      USE erreurs

      IMPLICIT NONE

      INTEGER(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN)::
     &                 ipx, ipy
      REAL(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN)   ::
     &                 ponpx, ponpy

      REAL(KIND=kr), INTENT(OUT)   :: ex1, ey1, ez1

      REAL(KIND=kr)   ::    aux_i, aux_j

      INTEGER         :: i, j, ord

      ord=ordre_interp+1

! --- Champ Ex : (dual, primal, primal)
          aux_j = 0
	  DO j=1,ord
            aux_i = 0
	    DO i=1,ord
	      aux_i = aux_i + ex(ipx(i), ipy(j))*ponpx(i)
	    ENDDO
	    aux_j = aux_j + aux_i*ponpy(j)
	  ENDDO

        ex1 = aux_j

! --- Champ Ey : (primal, dual, primal)
	  aux_j = 0
	  DO j=1,ord
	    aux_i = 0
	    DO i=1,ord
	      aux_i = aux_i + ey(ipx(i), ipy(j))*ponpx(i)
	    ENDDO
	    aux_j = aux_j + aux_i*ponpy(j)
	  ENDDO

        ey1 = aux_j

! --- Champ Ez : (primal, primal, dual)
	  aux_j = 0
	  DO j=1,ord
	    aux_i = 0
	    DO i=1,ord
	      aux_i = aux_i + ez(ipx(i), ipy(j))*ponpx(i)
	    ENDDO
	    aux_j = aux_j + aux_i*ponpy(j)
	  ENDDO

        ez1 = aux_j

      RETURN
!     ___________________________
      END SUBROUTINE champa_2d_exyz1



      SUBROUTINE champa_2d_exyztm1(ex1, ey1, ez1,
     &                ponpx, ponpy, ipx, ipy)
! ======================================================================
! Calcul des champs necessaires, aux positions des particules
! Geometrie 2d1/2, on calcule les six champs Ex, Ey, Ez, Bx, By, Bz
! ======================================================================

      USE domaines
      USE champs
      USE temps
      USE particules
      USE erreurs

      IMPLICIT NONE

      INTEGER(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN)::
     &                 ipx, ipy
      REAL(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN)   ::
     &                 ponpx, ponpy

      REAL(KIND=kr), INTENT(OUT)   :: ex1, ey1, ez1

      REAL(KIND=kr)   ::    aux_i, aux_j

      INTEGER         :: i, j, ord

      ord=ordre_interp+1

! --- Champ Ex : (dual, primal, primal)
          aux_j = 0
	  DO j=1,ord
            aux_i = 0
	    DO i=1,ord
	      aux_i = aux_i + extm1(ipx(i), ipy(j))*ponpx(i)
	    ENDDO
	    aux_j = aux_j + aux_i*ponpy(j)
	  ENDDO

        ex1 = aux_j

! --- Champ Ey : (primal, dual, primal)
	  aux_j = 0
	  DO j=1,ord
	    aux_i = 0
	    DO i=1,ord
	      aux_i = aux_i + eytm1(ipx(i), ipy(j))*ponpx(i)
	    ENDDO
	    aux_j = aux_j + aux_i*ponpy(j)
	  ENDDO

        ey1 = aux_j

! --- Champ Ez : (primal, primal, dual)
	  aux_j = 0
	  DO j=1,ord
	    aux_i = 0
	    DO i=1,ord
	      aux_i = aux_i + eztm1(ipx(i), ipy(j))*ponpx(i)
	    ENDDO
	    aux_j = aux_j + aux_i*ponpy(j)
	  ENDDO

        ez1 = aux_j

      RETURN
!     ___________________________
      END SUBROUTINE champa_2d_exyztm1


      SUBROUTINE champa_2d_epart_ik(ex1, ey1, ez1,
     &                ponpx, ponpy, ipx, ipy)
! ======================================================================
! Calcul des champs necessaires, aux positions des particules
! Geometrie 2d1/2, on calcule les six champs Ex, Ey, Ez, Bx, By, Bz
!
! On ajoute contribution �lectromagn�tique � l instant (n+1), et contribution
! �lectrostatique apr�s k it�rations
! nota bene :
! - pas de composante �lectrostatique ez2_ik en 2d espace
!
! ======================================================================

      USE domaines
      USE champs
      USE champs_iter
      USE temps
      USE particules
      USE erreurs

      IMPLICIT NONE

      INTEGER(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN)::
     &                 ipx, ipy
      REAL(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN)   ::
     &                 ponpx, ponpy

      REAL(KIND=kr), INTENT(OUT)   :: ex1, ey1, ez1

      REAL(KIND=kr)   ::    aux_i, aux_j

      INTEGER         :: i, j, ord

      ord=ordre_interp+1

! --- Champ Ex : (dual, primal, primal)
          aux_j = 0
	  DO j=1,ord
            aux_i = 0
	    DO i=1,ord
	      aux_i = aux_i + (ex(ipx(i), ipy(j))
     &                       + ex2_ik(ipx(i), ipy(j)))*ponpx(i)
	    ENDDO
	    aux_j = aux_j + aux_i*ponpy(j)
	  ENDDO

        ex1 = aux_j

! --- Champ Ey : (primal, dual, primal)
	  aux_j = 0
	  DO j=1,ord
	    aux_i = 0
	    DO i=1,ord
	      aux_i = aux_i + (ey(ipx(i), ipy(j))
     &                       + ey2_ik(ipx(i), ipy(j)))*ponpx(i)
	    ENDDO
	    aux_j = aux_j + aux_i*ponpy(j)
	  ENDDO

        ey1 = aux_j

! --- Champ Ez : (primal, primal, dual)
	  aux_j = 0
	  DO j=1,ord
	    aux_i = 0
	    DO i=1,ord
	      aux_i = aux_i + ez(ipx(i), ipy(j))*ponpx(i)
	    ENDDO
	    aux_j = aux_j + aux_i*ponpy(j)
	  ENDDO

        ez1 = aux_j

      RETURN
!     ___________________________
      END SUBROUTINE champa_2d_epart_ik



      SUBROUTINE champa_2d_exyz1_gridmax(ex1, ey1, ez1,
     &           ponpx, ponpy, pondx, pondy, ipx, ipy, idx, idy)
! ======================================================================
! Calcul des champs necessaires, aux positions des particules
! Geometrie 2d1/2, on calcule les six champs Ex, Ey, Ez, Bx, By, Bz
! ======================================================================

      USE domaines
      USE champs
      USE temps
      USE particules
      USE erreurs

      IMPLICIT NONE

      INTEGER(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN)::
     &                 ipx, ipy, idx, idy
      REAL(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN)   ::
     &                 ponpx, ponpy, pondx, pondy

      REAL(KIND=kr), INTENT(OUT)   :: ex1, ey1, ez1

      REAL(KIND=kr)   ::    aux_i, aux_j

      INTEGER         :: i, j, ord

      ord=ordre_interp+1

! --- Champ Ex : (dual, primal, primal)
          aux_j = 0
	  DO j=1,ord
            aux_i = 0
	    DO i=1,ord
	      aux_i = aux_i + ex(idx(i), ipy(j))*pondx(i)
	    ENDDO
	    aux_j = aux_j + aux_i*ponpy(j)
	  ENDDO

        ex1 = aux_j

! --- Champ Ey : (primal, dual, primal)
	  aux_j = 0
	  DO j=1,ord
	    aux_i = 0
	    DO i=1,ord
	      aux_i = aux_i + ey(ipx(i), idy(j))*ponpx(i)
	    ENDDO
	    aux_j = aux_j + aux_i*pondy(j)
	  ENDDO

        ey1 = aux_j

! --- Champ Ez : (primal, primal, dual)
	  aux_j = 0
	  DO j=1,ord
	    aux_i = 0
	    DO i=1,ord
	      aux_i = aux_i + ez(ipx(i), ipy(j))*ponpx(i)
	    ENDDO
	    aux_j = aux_j + aux_i*ponpy(j)
	  ENDDO

        ez1 = aux_j

      RETURN
!     ___________________________
      END SUBROUTINE champa_2d_exyz1_gridmax



      SUBROUTINE champa_2d_exyztm1_gridmax(ex1, ey1, ez1,
     &          ponpx, ponpy, pondx, pondy, ipx, ipy, idx, idy)
! ======================================================================
! Calcul des champs necessaires, aux positions des particules
! Geometrie 2d1/2, on calcule les six champs Ex, Ey, Ez, Bx, By, Bz
! ======================================================================

      USE domaines
      USE champs
      USE temps
      USE particules
      USE erreurs

      IMPLICIT NONE

      INTEGER(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN)::
     &                 ipx, ipy, idx, idy
      REAL(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN)   ::
     &                 ponpx, ponpy, pondx, pondy

      REAL(KIND=kr), INTENT(OUT)   :: ex1, ey1, ez1

      REAL(KIND=kr)   ::    aux_i, aux_j

      INTEGER         :: i, j, ord

      ord=ordre_interp+1

! --- Champ Ex : (dual, primal, primal)
          aux_j = 0
	  DO j=1,ord
            aux_i = 0
	    DO i=1,ord
	      aux_i = aux_i + extm1(idx(i), ipy(j))*pondx(i)
	    ENDDO
	    aux_j = aux_j + aux_i*ponpy(j)
	  ENDDO

        ex1 = aux_j

! --- Champ Ey : (primal, dual, primal)
	  aux_j = 0
	  DO j=1,ord
	    aux_i = 0
	    DO i=1,ord
	      aux_i = aux_i + eytm1(ipx(i), idy(j))*ponpx(i)
	    ENDDO
	    aux_j = aux_j + aux_i*pondy(j)
	  ENDDO

        ey1 = aux_j

! --- Champ Ez : (primal, primal, dual)
	  aux_j = 0
	  DO j=1,ord
	    aux_i = 0
	    DO i=1,ord
	      aux_i = aux_i + eztm1(ipx(i), ipy(j))*ponpx(i)
	    ENDDO
	    aux_j = aux_j + aux_i*ponpy(j)
	  ENDDO

        ez1 = aux_j

      RETURN
!     ___________________________
      END SUBROUTINE champa_2d_exyztm1_gridmax


      SUBROUTINE champa_2d_epart_ik_gridmax(ex1, ey1, ez1,
     &          ponpx, ponpy, pondx, pondy, ipx, ipy, idx, idy)
! ======================================================================
! Calcul des champs necessaires, aux positions des particules
! Geometrie 2d1/2, on calcule les six champs Ex, Ey, Ez, Bx, By, Bz
!
! On ajoute contribution �lectromagn�tique � l instant (n+1), et contribution
! �lectrostatique apr�s k it�rations
! nota bene :
! - pas de composante �lectrostatique ez2_ik en 2d espace
!
! ======================================================================

      USE domaines
      USE champs
      USE champs_iter
      USE temps
      USE particules
      USE erreurs

      IMPLICIT NONE

      INTEGER(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN)::
     &                 ipx, ipy, idx, idy
      REAL(KIND=kr), DIMENSION(1:ordre_interp+1), INTENT(IN)   ::
     &                 ponpx, ponpy, pondx, pondy

      REAL(KIND=kr), INTENT(OUT)   :: ex1, ey1, ez1

      REAL(KIND=kr)   ::    aux_i, aux_j

      INTEGER         :: i, j, ord

      ord=ordre_interp+1

! --- Champ Ex : (dual, primal, primal)
          aux_j = 0
	  DO j=1,ord
            aux_i = 0
	    DO i=1,ord
	      aux_i = aux_i + (ex(idx(i), ipy(j))
     &                       + ex2_ik(idx(i), ipy(j)))*pondx(i)
	    ENDDO
	    aux_j = aux_j + aux_i*ponpy(j)
	  ENDDO

        ex1 = aux_j

! --- Champ Ey : (primal, dual, primal)
	  aux_j = 0
	  DO j=1,ord
	    aux_i = 0
	    DO i=1,ord
	      aux_i = aux_i + (ey(ipx(i), idy(j))
     &                       + ey2_ik(ipx(i), idy(j)))*ponpx(i)
	    ENDDO
	    aux_j = aux_j + aux_i*pondy(j)
	  ENDDO

        ey1 = aux_j

! --- Champ Ez : (primal, primal, dual)
	  aux_j = 0
	  DO j=1,ord
	    aux_i = 0
	    DO i=1,ord
	      aux_i = aux_i + ez(ipx(i), ipy(j))*ponpx(i)
	    ENDDO
	    aux_j = aux_j + aux_i*ponpy(j)
	  ENDDO

        ez1 = aux_j

      RETURN
!     ___________________________
      END SUBROUTINE champa_2d_epart_ik_gridmax
