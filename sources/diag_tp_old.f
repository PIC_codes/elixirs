

      CASE(5)
        vol_s2 = 0.5_8*pasdx*pasdy
        ALLOCATE(tp_2(nulmx:nulpx-2, nulmy:nulpy-1))
        IF(d_ex.NE.0) THEN
          tp_2(:,:) = ex_2(nulmx:nulpx-2, nulmy:nulpy-1)
!          tp_2(:,:) = ex_2(nulmx+1:nulpx, nulmy:nulpy-1)
!     &              + ex_2(nulmx+1:nulpx, nulmy+1:nulpy)
!          d1d_loc(d_ex) = 0.25_8 * SUM( tp_2(:,:)**2 ) * vol_s2
          d1d_loc(d_ex) = SUM( tp_2(:,:)**2 ) * vol_s2
        ENDIF
        IF(d_ey.NE.0) THEN
          tp_2(:,:) = ey_2(nulmx:nulpx-2, nulmy:nulpy-1)
!          tp_2(:,:) = ey_2(nulmx:nulpx-1, nulmy+1:nulpy)
!     &              + ey_2(nulmx+1:nulpx, nulmy+1:nulpy)
!          d1d_loc(d_ey) = 0.25_8 * SUM( tp_2(:,:)**2 ) * vol_s2
          d1d_loc(d_ey) = SUM( tp_2(:,:)**2 ) * vol_s2
        ENDIF
        IF(d_ez.NE.0) THEN
          tp_2(:,:) = ez_2(nulmx:nulpx-2, nulmy:nulpy-1)
!          tp_2(:,:) = ez_2(nulmx:nulpx-1, nulmy:nulpy-1)
!     &              + ez_2(nulmx+1:nulpx, nulmy:nulpy-1)
!     &              + ez_2(nulmx:nulpx-1, nulmy+1:nulpy)
!     &              + ez_2(nulmx+1:nulpx, nulmy+1:nulpy)
!          d1d_loc(d_ez) = 0.0625_8 * SUM( tp_2(:,:)**2 ) * vol_s2
          d1d_loc(d_ez) = SUM( tp_2(:,:)**2 ) * vol_s2
        ENDIF
        IF(d_bx.NE.0) THEN
          tp_2(:,:) = bx_2(nulmx:nulpx-2, nulmy:nulpy-1)
!          tp_2(:,:) = bx_2(nulmx:nulpx-1, nulmy+1:nulpy)
!     &              + bx_2(nulmx+1:nulpx, nulmy+1:nulpy)
!          d1d_loc(d_bx) = 0.25_8 * SUM( tp_2(:,:)**2 ) * vol_s2
          d1d_loc(d_bx) = SUM( tp_2(:,:)**2 ) * vol_s2
        ENDIF
        IF(d_by.NE.0) THEN
         tp_2(:,:) = by_2(nulmx:nulpx-2, nulmy:nulpy-1)
!          tp_2(:,:) = by_2(nulmx+1:nulpx, nulmy:nulpy-1)
!     &              + by_2(nulmx+1:nulpx, nulmy+1:nulpy)
!          d1d_loc(d_by) = 0.25_8 * SUM( tp_2(:,:)**2 ) * vol_s2
          d1d_loc(d_by) = SUM( tp_2(:,:)**2 ) * vol_s2
        ENDIF
        IF(d_bz.NE.0) THEN
          d1d_loc(d_bz) = SUM( bz_2(nulmx:nulpx-2, nulmy:nulpy-1)**2 )
     &                                                 * vol_s2
        ENDIF
        DEALLOCATE(tp_2)
        IF(d_psr.NE.0) d1d_loc(d_psr) =  d1d_loc(d_psr)
     &                                 + fpsr*pasdt*pasdy


!        IF((ibndc(1).LT.-1).OR.(ibndc(1).EQ.0)) d1d_loc(d_pmx)
!     &  = d1d_loc(d_pmx) + pasdt*pasdy
!     &   *SUM(   ez_2(nulmx  ,nulmy:nulpy-1)
!     &          *by_2(nulmx  ,nulmy:nulpy-1)
!     &       -   ey_2(nulmx  ,nulmy:nulpy-1)
!     &          *bz_2(nulmx  ,nulmy:nulpy-1)
!     &       )

        IF((ibndc(1).LT.-1).OR.(ibndc(1).EQ.0)) d1d_loc(d_pmx)
     &  = d1d_loc(d_pmx) + pasdt*pasdy
     &   *SUM( -.25*eytm1(nulmx  ,nulmy:nulpy-1)
     &        *( bz_tn(nulmx  ,nulmy:nulpy-1)
     &          +bz_tn(nulmx  ,nulmy+1:nulpy)
     &          +bz_tn(nulmx+1,nulmy:nulpy-1) 
     &          +bz_tn(nulmx+1,nulmy+1:nulpy) )
     &      +0.5*eztm1(nulmx  ,nulmy:nulpy-1)
     &        *( by_tn(nulmx  ,nulmy:nulpy-1)
     &          +by_tn(nulmx+1,nulmy:nulpy-1) )
     &       )

!        IF((ibndc(2).LT.-1).OR.(ibndc(2).EQ.0)) d1d_loc(d_ppx)
!     &  = d1d_loc(d_ppx) + pasdt*pasdy
!     &   *SUM(-  ez_2(nulpx  ,nulmy:nulpy-1)
!     &          *by_2(nulpx  ,nulmy:nulpy-1)
!     &       +   ey_2(nulpx  ,nulmy:nulpy-1)
!     &          *bz_2(nulpx  ,nulmy:nulpy-1)
!     &       )

        IF((ibndc(2).LT.-1).OR.(ibndc(2).EQ.0)) d1d_loc(d_ppx)
     &  = d1d_loc(d_ppx) + pasdt*pasdy
     &   *SUM( .25*eytm1(nulpx  ,nulmy:nulpy-1)
     &        *( bz_tn(nulpx  ,nulmy:nulpy-1)
     &          +bz_tn(nulpx  ,nulmy+1:nulpy)
     &          +bz_tn(nulpx+1,nulmy:nulpy-1) 
     &          +bz_tn(nulpx+1,nulmy+1:nulpy) )
     &      -0.5*eztm1(nulpx  ,nulmy:nulpy-1)
     &        *( by_tn(nulpx  ,nulmy:nulpy-1)
     &          +by_tn(nulpx+1,nulmy:nulpy-1) )
     &       )

!        IF(ibndc(3).LT.-1)                      d1d_loc(d_pmy)
!     &  = d1d_loc(d_pmy) + pasdt*pasdx
!     &   *SUM(   ex_2(nulmx:nulpx-1,nulmy  )
!     &          *bz_2(nulmx:nulpx-1,nulmy  )
!     &        -  ez_2(nulmx:nulpx-1,nulmy  )
!     &          *bx_2(nulmx:nulpx-1,nulmy  ) 
!     &       )

        IF(ibndc(3).LT.-1)                      d1d_loc(d_pmy)
     &  = d1d_loc(d_pmy) + pasdt*pasdx
     &  *SUM(-0.5*eztm1(nulmx  :nulpx-1,nulmy  )
     &        *( bx_tn(nulmx  :nulpx-1,nulmy  )
     &          +bx_tn(nulmx  :nulpx-1,nulmy+1) )
     &      +.25*extm1(nulmx:nulpx-1  ,nulmy  )
     &        *( bz_tn(nulmx:nulpx-1  ,nulmy  )
     &          +bz_tn(nulmx:nulpx-1  ,nulmy+1)
     &          +bz_tn(nulmx+1:nulpx  ,nulmy  )
     &          +bz_tn(nulmx+1:nulpx  ,nulmy+1) )
     &       )

!        IF(ibndc(4).LT.-1)                      d1d_loc(d_ppy)
!     &  = d1d_loc(d_ppy) + pasdt*pasdx
!     &   *SUM(-   ex_2(nulmx:nulpx-1,nulpy  )
!     &           *bz_2(nulmx:nulpx-1,nulpy  )
!     &         +  ez_2(nulmx:nulpx-1,nulpy  )
!     &           *bx_2(nulmx:nulpx-1,nulpy  )
!     &       )

        IF(ibndc(4).LT.-1)                      d1d_loc(d_ppy)
     &  = d1d_loc(d_ppy) + pasdt*pasdx
     &  *SUM( 0.5*eztm1(nulmx  :nulpx-1,nulpy  )
     &        *( bx_tn(nulmx  :nulpx-1,nulpy  )
     &          +bx_tn(nulmx  :nulpx-1,nulpy+1) )
     &      -.25*extm1(nulmx:nulpx-1  ,nulpy  )
     &        *( bz_tn(nulmx:nulpx-1  ,nulpy  )
     &          +bz_tn(nulmx:nulpx-1  ,nulpy+1)
     &          +bz_tn(nulmx+1:nulpx  ,nulpy  )
     &          +bz_tn(nulmx+1:nulpx  ,nulpy+1) )
     &       )
     
     
     
