! ======================================================================
! init_vars.f
! ----------
!
!     SUBROUTINE zero
!     SUBROUTINE cstes
!
!     SUBROUTINE init_implicite
!     SUBROUTINE init_partic
!     SUBROUTINE update_fields
!     SUBROUTINE update_moments
!
!
! ======================================================================



       SUBROUTINE zero
! ======================================================================
!
!
!
! ======================================================================

       USE champs
       USE champs_iter
       USE particules
       USE particules_klder
       USE erreurs


       IMPLICIT NONE

       INTEGER    ::   ip, iesp



!     tableaux de type champ
       xxx=0. ; xxy=0. ; xxz=0.
       xyx=0. ; xyy=0. ; xyz=0.
       xzx=0. ; xzy=0. ; xzz=0.
 !
       zxx=0. ; zxy=0. ; zxz=0.
       zyx=0. ; zyy=0. ; zyz=0.
       zzx=0. ; zzy=0. ; zzz=0.
 !
       IF(debug.GE.4) THEN
         xxxr1=0. ; xxyr1=0. ; xxzr1=0.
         xyxr1=0. ; xyyr1=0. ; xyzr1=0.
         xzxr1=0. ; xzyr1=0. ; xzzr1=0.

         xxxr2=0. ; xxyr2=0. ; xxzr2=0.
         xyxr2=0. ; xyyr2=0. ; xyzr2=0.
         xzxr2=0. ; xzyr2=0. ; xzzr2=0.

         zxxr1=0. ; zxyr1=0. ; zxzr1=0.
         zyxr1=0. ; zyyr1=0. ; zyzr1=0.
         zzxr1=0. ; zzyr1=0. ; zzzr1=0.
       ENDIF

       IF(max_cycles.GT.1) THEN
         xxx_esp=0. ; xxy_esp=0. ; xxz_esp=0.
         xyx_esp=0. ; xyy_esp=0. ; xyz_esp=0.
         xzx_esp=0. ; xzy_esp=0. ; xzz_esp=0.
 !
         zxx_esp=0. ; zxy_esp=0. ; zxz_esp=0.
         zyx_esp=0. ; zyy_esp=0. ; zyz_esp=0.
         zzx_esp=0. ; zzy_esp=0. ; zzz_esp=0.
       ENDIF

       IF(ANY(dynam(1:nesp_p).NE.2) .AND. max_cycles.GT.1) THEN
          WRITE(fmsgo,"('init_vars.f : le sub_cycling n est pas'
     &         'pris en charge pour les esp�ces de particules '
     &         'classiques ERREUR FATALE !' )")
          STOP
       ENDIF

       IF(iter_es.EQ.1) THEN
         src_max=0.

         xxx_ik=0. ; xxy_ik=0. ; xxz_ik=0.
         xyx_ik=0. ; xyy_ik=0. ; xyz_ik=0.
         xzx_ik=0. ; xzy_ik=0. ; xzz_ik=0.

         xxx0_ik=0. ; xxy0_ik=0. ; xxz0_ik=0.
         xyx0_ik=0. ; xyy0_ik=0. ; xyz0_ik=0.
         xzx0_ik=0. ; xzy0_ik=0. ; xzz0_ik=0.
       ENDIF

       qx(:,:) = 0.
       qy(:,:) = 0.
       qz(:,:) = 0.

       qxbound(:) = 0.
       qybound(:) = 0.
       qzbound(:) = 0.

       qxnx(:)   = 0.
       qynxm1(:) = 0.
       qznxm1(:) = 0.
 !
       bx =0.
       by =0.
       bz =0.
       emoyx=0.
       emoyy=0.
       emoyz=0.
       ex =0.
       ey =0.
       ez =0.
       phi=0.
       s = 0.

       IF(simplified.EQ.2) THEN
         dexdx(:,:) =0.; dexdy(:,:) =0.
         deydx(:,:) =0.; deydy(:,:) =0.
         dezdx(:,:) =0.; dezdy(:,:) =0.
       ENDIF

       ebx=0.
       eby=0.
       ebz=0.
       ebbx=0.
       ebby=0.
       ebbz=0.

       exm(:)  =0.
       bym(:)  =0.
       bzm1(:) =0.
       eym(:)  =0.
       bxm(:)  =0.
       bzm2(:) =0.

       extm1m(:) =0.
       eytm1m(:) =0.

! ---- initialisation de bx_tn, by_tn, bz_tn
       bx_tn =0.
       by_tn =0.
       bz_tn =0.

! ---- tableaux moyenn�s suivant y
       bx_1d =0.
       by_1d =0.
       bz_1d =0.
       ex_1d =0.
       ey_1d =0.
       ez_1d =0.

       extm1=0.
       eytm1=0.
       eztm1=0.


! ---- initialisation en dur pour une particule
       IF(nbpa(iref).EQ.1) THEN
         ip = 1 ; iesp = 1

         partic(a_qx,ip,iesp) = 0.101d0
         partic(a_qy,ip,iesp) = 0.d0

         partic(a_px,ip,iesp) = 0.d0
         partic(a_py,ip,iesp) = 0.d0
         partic(a_pz,ip,iesp) = 0.d0

!       ELSEIF(nbpa(iref).EQ.-1) THEN
         partic(a_pxtld,ip,iesp) = 0.d0
         partic(a_pytld,ip,iesp) = 0.d0
         partic(a_pztld,ip,iesp) = 0.d0

         partic(a_gatld,ip,iesp)   = 1.d0
         partic(a_gatld12,ip,iesp) = 1.d0

         iesp = 2
         partic(a_qx,ip,iesp) = 0.101d0
         partic(a_qy,ip,iesp) = 0.d0

         partic(a_px,ip,iesp) = 0.d0
         partic(a_py,ip,iesp) = 0.d0
         partic(a_pz,ip,iesp) = 0.d0

         partic(a_pxtld,ip,iesp) = 0.d0
         partic(a_pytld,ip,iesp) = 0.d0
         partic(a_pztld,ip,iesp) = 0.d0

         partic(a_gatld,ip,iesp)   = 1.d0
         partic(a_gatld12,ip,iesp) = 1.d0
       ENDIF

!      ___________________
       END SUBROUTINE zero



       SUBROUTINE init_implicite
! ======================================================================
!
!
!
! ======================================================================

       USE domaines
       USE domaines_klder
       USE diagnostics
       USE particules
       USE particules_klder
       USE champs
       USE champs_iter
       USE temps
       USE erreurs

       IMPLICIT NONE

!       INTEGER    ::  ip

       write(fdbg,"('Debut init_implicite')")
       xlong     = pasdx*nbmx
       ylong     = pasdy*nbmy
       dt        = pasdt

! ----- Activation expressions simplifi�es des corrections
       simplified = 0

! ---- Crit�res de convergence m�thode de Concus et Golub
!    prendre 8 pour nbpot et nbmax
       nbpot     = 15
       epspot    = 1.e-6
       nbmax     = 5
       epsmax    = 5.e-5

       epsik0 = 1e-4
       nik0   = 6

       Efast = 10.


!       IF(ordre_interp.EQ.2) THEN
!         nb_vidx = 1
!         nb_vidy = 1
!       ENDIF
       write(fdbg,"('Vitesses moyennes : Init ok')")
       xvide     = pasdx*nb_vidx
       write(fdbg,"('xvide  : ', (F8.2  ,1X))") xvide
       yvide     = pasdy*nb_vidy
       write(fdbg,"('yvide  : ', (F8.2  ,1X))") yvide

! --- electrons : iesp=1, ions : iesp=2
       nx     = nbmx
       write(fdbg,"('nx : Init ok', (I8,1X))") nx
       ny     = nbmy
       write(fdbg,"('ny : Init ok', (I8,1X))") ny
       nvect  = MIN(INT(float(nx)/2.), INT(float(ny)/2.))
       write(fdbg,"('nvect : Init ok', (I8,1X))") nvect

       nfreq  = itfin
       write(fdbg,"('nfreq : Init ok', (I8,1X))") nfreq

       write(fdbg,"('Nombre de mailles & densites : Init ok')")

       n1x=nx+1; n1y=ny+1
       nxm1=nx-1; n2x=nx+2; n2y=ny+2; n2xf2=n2x*2
       nxs2=nx/2; nys2=ny/2; nxm1s2=nxm1/2
       nxm1f2=nxm1*2; nxm1f3=nxm1*3
       nys2m1=nys2-1; nys2p1=nys2+1; nys2p2=nys2p1+1

       nvf28=nvect*28
       nxm1f6=nxm1*6; ndima=nxm1f6+2; nxny=nx*ny

       ngmax=n2x*n2y


       nda1d = nys2m1*nxm1f2*7
       ndimam = nxm1f3+1; ndim1d = ndima*19*nys2m1
       ngf4=ngmax*4; ngf7=ngmax*7; ngf14=ngmax*14

       write(fdbg,"('Variables secondaires : Init ok')")

       RETURN
!      _____________________________
       END SUBROUTINE init_implicite


       SUBROUTINE init_partic
! ======================================================================
!
! On shinte la routine "setrep" pour utiliser � la place les routines de Klder,
! ce qui est fait par les appels "init_donn", "calc_donn", "init_calc",
!  "init_diag" dans calcul.f.
! C'est pourquoi on utilise le tableau partic pour mettre
! � jour les variables originelles de impl2d.
!
! ======================================================================

       USE domaines
       USE temps
       USE champs
       USE diagnostics
       USE particules
       USE particules_klder
       USE erreurs


       IMPLICIT NONE

       INTEGER     ::    iesp, ip
       INTEGER     ::    ix, iy

       INTEGER     ::    imin, imax, indx

       REAL(kind=kr)  ::   of7x, of7y


! ----- Initialisation de partic(a_gatld12,ip,iesp) et partic(a_gatld,ip,iesp)
       DO iesp=1,nesp_p
         partic(a_gatld12,1:nbpa(iesp),iesp)
     &               = partic(a_ga,1:nbpa(iesp),iesp)
         partic(a_gatld,1:nbpa(iesp),iesp)
     &               = partic(a_ga,1:nbpa(iesp),iesp)

         partic(a_pxtld,1:nbpa(iesp),iesp)
     &               = partic(a_px,1:nbpa(iesp),iesp)
         partic(a_pytld,1:nbpa(iesp),iesp)
     &               = partic(a_py,1:nbpa(iesp),iesp)
         partic(a_pztld,1:nbpa(iesp),iesp)
     &               = partic(a_pz,1:nbpa(iesp),iesp)
       ENDDO

       partic(a_bxp,1:nbpamax,1:nesp_p)= 0.d0
       partic(a_byp,1:nbpamax,1:nesp_p)= 0.d0
       partic(a_bzp,1:nbpamax,1:nesp_p)= 0.d0

       partic(a_gaxe,1:nbpamax,1:nesp_p)= 0.d0
       partic(a_gaye,1:nbpamax,1:nesp_p)= 0.d0
       partic(a_gaze,1:nbpamax,1:nesp_p)= 0.d0

       partic(a_paxe,1:nbpamax,1:nesp_p)= 0.d0
       partic(a_paye,1:nbpamax,1:nesp_p)= 0.d0
       partic(a_paze,1:nbpamax,1:nesp_p)= 0.d0


! ----- Initialisation du champ a_ttapushs2 de partic(:,:,:)
!       uniquement en mode friction_tta.EQ.1 ce qui implique
!       a_ttapushs2.NE.0
!       Dans ce cas chaque particule porte son propre theta pour le pousseur
!      Crit�re arbitraire pour les particules rapides : (\gamma -1)> Efast
!      Avec E_fast de l'ordre de quelques m_e c^2
!
       IF(a_ttapushs2.NE.0) THEN
         cnt_fast(1:nesp_p) = 0.

         DO iesp=1,nesp_p
           IF(dynam(iesp).EQ.2) THEN
             DO ip=1, nbpa(iesp)
               IF(partic(a_qx,ip,iesp).LE.x_damp1 .OR.
     &            partic(a_qx,ip,iesp).GT.x_damp2 .OR.
     &           (partic(a_ga,ip,iesp)-1).GE.Efast) THEN
                 partic(a_ttapushs2,ip,iesp)= 0.5*thetad
! --------- particule rapide +1
	         cnt_fast(iesp)= cnt_fast(iesp)+1
	       ELSE
                 partic(a_ttapushs2,ip,iesp)= ttapushs2
	       ENDIF
             ENDDO
	   ELSE
             DO ip=1, nbpa(iesp)
               IF(partic(a_qx,ip,iesp).LE.x_damp1 .OR.
     &            partic(a_qx,ip,iesp).GT.x_damp2 .OR.
     &           partic(a_ga,ip,iesp).GE.Efast) THEN
                 partic(a_ttapushs2,ip,iesp)= 0.5*thetad
! --------- particule rapide +1
	         cnt_fast(iesp)= cnt_fast(iesp)+1
	       ELSE
                 partic(a_ttapushs2,ip,iesp)= ttapushs2
	       ENDIF
             ENDDO
	   ENDIF
         ENDDO
       ENDIF


! ----- Initialisation des �lectrons et des ions
!       test du potentiel �lectrostatique
!       IF(nbpa(1).EQ.80 .AND. nbpa(2).EQ.80) THEN
!       imin = 23
!       imax = 42
!       indx = imax - imin +1
!       of7x = indx*pasdx
!       of7y = -0.5*ny*pasdy
!
!         DO ix=1,indx
!           DO iy=1,ny
!             partic(a_qx,(iy-1)*indx + ix,1) = ix*pasdx + of7x
!             partic(a_qy,(iy-1)*indx + ix,1) = iy*pasdy + of7y
!
!             partic(a_qx,(iy-1)*indx + ix,2) = (ix+1.2)*pasdx + of7x
!             partic(a_qy,(iy-1)*indx + ix,2) = iy*pasdy + of7y
!           ENDDO
!         ENDDO
!       ENDIF


!      __________________________
       END SUBROUTINE init_partic




       SUBROUTINE update_fields
! ======================================================================
!
!
!
! ======================================================================

       USE domaines
       USE champs
       USE particules
       USE particules_klder


       IMPLICIT NONE



       SELECT CASE(dim_cas)

! ---- 3dx3dv
       CASE (6)

! ---- 2dx3dv
       CASE (5)
         ex_2(nulmx:nulpx, nulmy:nulpy) =
     &     ex(0:nx,0:ny)
         ey_2(nulmx:nulpx, nulmy:nulpy) =
     &     ey(0:nx,0:ny)
         ez_2(nulmx:nulpx, nulmy:nulpy) =
     &     ez(0:nx,0:ny)
         bx_2(nulmx:nulpx, nulmy:nulpy) =
     &     bx(0:nx,0:ny)
         by_2(nulmx:nulpx, nulmy:nulpy) =
     &     by(0:nx,0:ny)
         bz_2(nulmx:nulpx, nulmy:nulpy) =
     &     bz(0:nx,0:ny)

!         ex_2(nulmx:nulpx+1, nulmy:nulpy+1) =
!     &     ex(0:nx+1,0:ny+1)
!         ey_2(nulmx:nulpx+1, nulmy:nulpy+1) =
!     &     ey(0:nx+1,0:ny+1)
!         ez_2(nulmx:nulpx+1, nulmy:nulpy+1) =
!     &     ez(0:nx+1,0:ny+1)
!         bx_2(nulmx:nulpx+1, nulmy:nulpy+1) =
!     &     bx(0:nx+1,0:ny+1)
!         by_2(nulmx:nulpx+1, nulmy:nulpy+1) =
!     &     by(0:nx+1,0:ny+1)
!         bz_2(nulmx:nulpx+1, nulmy:nulpy+1) =
!     &     bz(0:nx+1,0:ny+1)

! ---- 2dx2dv
       CASE (4)
         ex_2(nulmx:nulpx, nulmy:nulpy) =
     &     ex(0:nx,0:ny)
         ey_2(nulmx:nulpx, nulmy:nulpy) =
     &     ey(0:nx,0:ny)
         bz_2(nulmx:nulpx, nulmy:nulpy) =
     &     bz(0:nx,0:ny)


! ---- 1dx3dv
       CASE (3)

! ---- 1dx�dv
       CASE (2)

! ---- 1dx1dv
       CASE (1)

! ---- erreur
       CASE DEFAULT



       END SELECT


       RETURN
!      _____________________________
       END SUBROUTINE update_fields



       SUBROUTINE update_moments
! ======================================================================
!
!
!
! ======================================================================

       USE domaines
       USE champs
       USE particules
       USE particules_klder


       IMPLICIT NONE



       SELECT CASE(dim_cas)

! ---- 3dx3dv
       CASE (6)

! ---- 2dx3dv
       CASE (5)
         rh_2(nulmx:nulpx, nulmy:nulpy) =  s(0:nx,0:ny)
         cx_2(nulmx:nulpx, nulmy:nulpy) =  jxtot(0:nx,0:ny)
         cy_2(nulmx:nulpx, nulmy:nulpy) =  jytot(0:nx,0:ny)
         cz_2(nulmx:nulpx, nulmy:nulpy) =  jztot(0:nx,0:ny)


! ---- 2dx2dv
       CASE (4)
         rh_2(nulmx:nulpx, nulmy:nulpy) =  s(0:nx,0:ny)
         cx_2(nulmx:nulpx, nulmy:nulpy) =  jxtot(0:nx,0:ny)
         cy_2(nulmx:nulpx, nulmy:nulpy) =  jytot(0:nx,0:ny)


! ---- 1dx3dv
       CASE (3)

! ---- 1dx�dv
       CASE (2)

! ---- 1dx1dv
       CASE (1)

! ---- erreur
       CASE DEFAULT



       END SELECT


       RETURN
!      _____________________________
       END SUBROUTINE update_moments



       SUBROUTINE verif_update_fields
! ======================================================================
!
!
!
! ======================================================================

       USE domaines
       USE champs
       USE particules
       USE particules_klder


       IMPLICIT NONE





!      __________________________________
       END SUBROUTINE verif_update_fields



       SUBROUTINE cstes
! ======================================================================
!
!
!
! ======================================================================

       USE domaines
       USE diagnostics
       USE particules
       USE particules_klder
       USE champs
       USE temps
       USE laser
       USE erreurs


       IMPLICIT NONE

       REAL(kind=kr)       ::   csy, aky

       INTEGER      ::    ky

 !

       aky=2.*pi/float(ny)

       DO ky=1,ny
         csy=cos(aky*(ky-1))
	 cosy(ky)=csy
         sny(ky)=sin(aky*(ky-1))
         csym1(ky)=csy-1.
         csyp1(ky)=csy+1.
       ENDDO
 !
       dx=pasdx
       dy=pasdy

       xmin=xmin_gl+xvide
       xmax=xmax_gl-xvide
       ymin=ymin_gl+yvide
       ymax=ymax_gl-yvide
 !
       dxi=1./dx
       dyi=1./dy
       dxf2i=0.5*dxi
       dxf4i=0.25*dxi
       dyf2i=0.5*dyi
       dyf4i=0.25*dyi
 !
       dtsdx=dt/dx
       dtsdy=dt/dy
       dts2dx=0.5*dtsdx
       dts2dy=0.5*dtsdy
       dts4dx=0.5*dts2dx
       dts4dy=0.5*dts2dy
 !
       dts2=dt/2.
       dts4=dt/4.
 !
       dt2=dt**2
       dt2s2=0.5*dt2
       dt2s4=0.5*dt2s2
       dt2s8=0.5*dt2s4
 !
       dx2=dx**2
       dy2=dy**2
       dxdy=dx*dy
       dx2i=1./dx2
       dy2i=1./dy2
       dxdyi=1./dxdy
       dxdyi4=0.25/dxdy
 !
       dtsdx2=dt2/dx2
       dtsdy2=dt2/dy2
 !
!       d1=2.*dtsdy2
!       d2=2.*dtsdy

       d3=2.*dt2/dxdy

!       d4=2.*dtsdx
!       d5=2.*dtsdx2
!       d6=dt2s4*pmesmi
!       d7=dt2s4*pmas2
!       d8=dt2s2*pmas2
!       d9=dt*pmesmi
!       d10=dt2s8*pmesmi

       d11=dt2/(2.*dxdy)
       d12=0.5*dtsdy2
       d13=0.5*dtsdx2

!       d14=0.25*dxdyi
!       d15=2.*dx2i
!       d16=2.*dy2i
!       d18=dts2*pmesmi
!       d19=dts4*pmesmi

! ---- angle par d�faut du champ diffus�
       in_angle   = y_ang(1)
       scat_angle = -in_angle

! ---- Constantes li�es aux conditions aux limites
! ---- au bord gauche
       keb_a =  1./(1. + 2.*COS(scat_angle)*dtsdx )
       keb_b =  1./(1. + 2.*dtsdx/COS(scat_angle) )

       keb1 = 0.25*d3*keb_a*(2.*COS(scat_angle)*dtsdx-1.)
       keb2 = 0.5*d3*keb_a*COS(scat_angle)*dtsdy
       keb3 = -0.5*(dt2/dx2)*keb_a*(2.*COS(scat_angle)*dtsdx-1.)
       keb4 = (dt2/dx2)*keb_a*COS(scat_angle)*dtsdy
       keb5 = -0.5*(dt2/dx2)*keb_b*(2.*dtsdx/COS(scat_angle)-1.)

       kebqx1 = -d3*keb_a*COS(scat_angle)
       kebqx2 = -0.5*d3*keb_a*COS(scat_angle)*dtsdx
       kebqx3 = 0.5*d3*keb_a*COS(scat_angle)*dtsdy
       kebqx4 = -0.25*d3*keb_a

       kebqy1 = -dtsdx2*2*keb_a*COS(scat_angle)
       kebqy2 = dtsdx2*keb_a*COS(scat_angle)*dtsdx
       kebqy3 = -0.5*dtsdx2*keb_a
       kebqy4 = -dtsdx2*keb_a*COS(scat_angle)*dtsdy

       kebqz1 = dtsdx2*2*keb_b/COS(scat_angle)
       kebqz2 = dtsdx2*keb_b*dtsdx/COS(scat_angle)
       kebqz3 = -0.5*dtsdx2*keb_b

       kebey1 = keb_a*(2.*COS(scat_angle)*dtsdx -1.)
       kebey2 = -2.*keb_a*COS(scat_angle)*dtsdy
       kebey3 = -4.*keb_a*COS(scat_angle)
       kebey4 = 2.*keb_a*COS(scat_angle)*dtsdx

       kebez1 = keb_b*(2.*dtsdx/COS(scat_angle) -1.)
       kebez2 = 4.*keb_b/COS(scat_angle)
       kebez3 = 2.*keb_b*dtsdx/COS(scat_angle)

! ---- Sch�ma � la Klder (version 1) au bord gauche

       vex1 = (dtsdx*COS(scat_angle) -1.)/(1. +dtsdx*COS(scat_angle))
       vex2 = -dtsdy*COS(scat_angle)/(1.+ dtsdx*COS(scat_angle))
       vqx1 = -vex1
       vqx2 = -vex2
       vqx3 = 4*COS(scat_angle)/(1. +dtsdx*COS(scat_angle))

       vex1tild = -d11*vex1
       vex2tild = -d11*vex2

       vqx1tild = d11*vqx1
       vqx2tild = d11*vqx2
       vqx3tild = d11*vqx3
       vqx4tild = -d11*8./(1+dtsdx*COS(scat_angle))

       vey1tild = -0.5*dtsdx2*vex1
       vey2tild = -0.5*dtsdx2*vex2

       vqy1tild = 0.5*dtsdx2*vqx1
       vqy2tild = 0.5*dtsdx2*vqx2
       vqy3tild = 0.5*dtsdx2*vqx3
       vqy4tild = -0.5*dtsdx2*8./(1+dtsdx*COS(scat_angle))

       deltakeb = 1./(0.5*COS(scat_angle) + 0.5*dtsdx)
       vkebz1 = deltakeb*(0.5*dtsdx - 0.5*COS(scat_angle))
       vkebz2 = -deltakeb*0.5*dtsdx
       vkebz3 = 0.5*COS(scat_angle)*deltakeb
       vkebz4 = -2.*deltakeb

       vkebz1tild = vkebz1*(-0.5)*dtsdx2
       vkebz2tild = vkebz2*(-0.5)*dtsdx2
       vkebz3tild = vkebz3*(-0.5)*dtsdx2
       vkebz4tild = vkebz4*(-0.5)*dtsdx2

! ---- Sch�ma � la Klder (version 1) au bord droit

       veyr1= (dtsdx*COS(scat_angle) -1.)/(1. +dtsdx*COS(scat_angle))
       veyr2= dtsdy*COS(scat_angle)/(1.+dtsdx*COS(scat_angle))
       vqxr1= veyr1
       vqxr2= dtsdy*COS(scat_angle)/(1. +dtsdx*COS(scat_angle))
       vqxr3= 4.*COS(scat_angle)/(1. +dtsdx*COS(scat_angle))

       vexr1tild = d11*veyr1
       vexr2tild = d11*veyr2
       vqxr1tild = d11*vqxr1
       vqxr2tild = d11*vqxr2
       vqxr3tild = d11*vqxr3

       veyr1tild = -0.5*dtsdx2*veyr1
       veyr2tild = -0.5*dtsdx2*veyr2
       vqyr1tild = -0.5*dtsdx2*vqxr1
       vqyr2tild = -0.5*dtsdx2*vqxr2
       vqyr3tild = -0.5*dtsdx2*vqxr3

       vezr1 = (dtsdx/COS(scat_angle) -1.)/(1. +dtsdx/COS(scat_angle))
       vqzr1 = vezr1
       vqzr2 = (-4./COS(scat_angle))/(1. +dtsdx/COS(scat_angle))

       vezr1tild = -0.5*dtsdx2*vezr1
       vqzr1tild = -0.5*dtsdx2*vqzr1
       vqzr2tild = -0.5*dtsdx2*vqzr2




! ---- Sch�ma � la Klder (version 2) au bord gauche

       deltaf = 1./(1.+ 2.*dtsdx/COS(scat_angle))
       fkebz1 = deltaf*(2.*dtsdx/COS(scat_angle) -1.)
       fkebz2 = -2.*deltaf/COS(scat_angle)
       fkebz3 = -dtsdx*deltaf/COS(scat_angle)

       fkebz1tild = fkebz1*(-0.5)*dtsdx2
       fkebz2tild = fkebz2*(-0.5)*dtsdx2
       fkebz3tild = fkebz3*(-0.5)*dtsdx2


       deltaff = 1./(1.+ 1.5*dtsdx/COS(scat_angle))
       ffkebz1 = deltaff*(1.5*dtsdx/COS(scat_angle) -1.)
       ffkebz2 = -2.*deltaff/COS(scat_angle)
       ffkebz3 = -dtsdx*deltaff/COS(scat_angle)
       ffkebz4 = -0.5*dtsdx*deltaff/COS(scat_angle)

       ffkebz1tild = ffkebz1*(-0.5)*dtsdx2
       ffkebz2tild = ffkebz2*(-0.5)*dtsdx2
       ffkebz3tild = ffkebz3*(-0.5)*dtsdx2
       ffkebz4tild = ffkebz4*(-0.5)*dtsdx2

! ---- Constantes li�es aux conditions aux limites
! ---- au bord droit
       kebr1 = 1./(1.+2.*COS(scat_angle)*dtsdx)
       kebr2 = 1./(1.+2.*dtsdx/COS(scat_angle))

       kebqxr1 = -d11*kebr1
       kebqxr2 = -d11*kebr1*4.*COS(scat_angle)
       kebqxr3 =  d11*kebr1*2.*COS(scat_angle)*dtsdx
       kebqxr4 = -d11*kebr1*2.*COS(scat_angle)*dtsdy

       kebqyr1 = -0.5*dtsdx2*kebr1
       kebqyr2 =  0.5*dtsdx2*kebr1*4.*COS(scat_angle)
       kebqyr3 = -0.5*dtsdx2*kebr1*2.*COS(scat_angle)*dtsdx
       kebqyr4 =  0.5*dtsdx2*kebr1*2.*COS(scat_angle)*dtsdy

       kebqzr1 = -0.5*dtsdx2*kebr2*2.*dtsdx/COS(scat_angle)
       kebqzr2 = -0.5*dtsdx2*kebr2
       kebqzr3 = -0.5*dtsdx2*kebr2*4./COS(scat_angle)

       kebrex1 = kebr1*d11*(1.-2.*COS(scat_angle)*dtsdx)
       kebrex2 = kebr1*d11*4.*COS(scat_angle)*dtsdy

       kebrey1 = -0.5*kebr1*dtsdx2*(2.*COS(scat_angle)*dtsdx-1.)
       kebrey2 = kebr1*dtsdx2*COS(scat_angle)*dtsdy

       kebrez1 = 0.5*kebr2*dtsdx2*(1.-2.*dtsdx/COS(scat_angle))

! ---- initialisation des constantes pour conditions aux limites
!      absorbantes, bonne r�solution pour les angles compris entre 0 et 45�
       cb_45 = 0.414213562
       ce_45 = 0.585786437

       omega1 = 0.5
       boost = 0.25*a_0(1)**2/(1.+ 0.25*a_0(1)**2)

!       x_damp1 = 0.25*(xmax_gl-xmin_gl)/dx
       i_damp1 = FLOOR(x_damp1/dx)

!       x_damp2 = 0.75*(xmax_gl-xmin_gl)/dx
       i_damp2 = FLOOR(x_damp2/dx)

!       i_damp1 = -1
!       i_damp2 = -1

       thetads2 = 0.5*thetad
       ones2 = 0.5


       WRITE(fmsgo,*) 'thetad = ', thetad
       WRITE(fmsgo,*) 'i_damp1 = ', i_damp1
       WRITE(fmsgo,*) 'i_damp2 = ', i_damp2
       WRITE(fmsgo,*) 'ttapushs2 = ', ttapushs2
       WRITE(fmsgo,*) 'friction_tta = ', friction_tta

       nulx_inj = 0

! ---- choix arbitraire d une espece de reference
! ---- pour determiner la densite particulaire
       iref = 1

       SELECT CASE(ordre_interp)
         CASE(1)
           iptarf=1

         CASE(2)
           iptarf=2

         CASE(3)
           iptarf=2

         CASE(4)
           iptarf=3

         CASE DEFAULT
           WRITE(fdbg,*) 'init_vars.f : routines cstes, ip_tta_ref'
        END SELECT


!       cb_45 = 0.5
!       ce_45 = 0.5

!
       return
!      ____________________
       END SUBROUTINE cstes
