! ======================================================================
! maxlim.f
! --------
!
!     SUBROUTINE maxlim_2d1s2
!     FUNCTION calc_profil
!     FUNCTION calc_profil_y
!     FUNCTION sqrt_gauss
!     FUNCTION sqrt_hermite
!     FUNCTION plateau
!     FUNCTION sqrt_cos_car
!
! --- sch�mas k x E = B implicites
!     SUBROUTINE maxlim_2d1s2_nEB_implicit(tps)
!     SUBROUTINE maxlim_2d1s2_nEB_implicit_step2(tps)
!
!     SUBROUTINE maxlim_2d1s2_nEB_implicit_var(tps)
!     SUBROUTINE maxlim_2d1s2_nEB_implicit_step2_var(tps)
!      
! --- sch�ma k x E = B explicite
!     SUBROUTINE maxlim_2d1s2_nEB(tps)
!
! --- sch�ma de type diff�rentiel � valider
! --- Condition de Mur d'ordre 1
! --- voir la ref de Engquist et Majda, ou le bouquin de Taflove
!     SUBROUTINE maxlim_2d1s2_E(tps)
!      
!
! --- m�thodes de huygens non valid�es (d�conseill�es)
!     SUBROUTINE maxlim_huygens_E
!     SUBROUTINE maxlim_huygens_E_step2(tps)
!      
!     SUBROUTINE maxlim_huygens_B(tps)
!
!
!
!
! ======================================================================






      SUBROUTINE maxlim_2d1s2_nEB_implicit(tps)
! ======================================================================
! Application des conditions aux limites pour les 6 champs [EB][xyz]
! a l'instant reel temps
! Retourne le flux de Poynting entre (source)
! Le type de condition est code dans les variables ibndc(:)
!   ibndc >  0 : champs echanges avec un PE voisin
!   ibndc = -1 : conditions periodiques locales a ce PE
!   ibndc = -2 : absorption - condition de Silver-Muller
!   ibndc = -3 : conducteur parfait
! ======================================================================
      USE precisions
      USE domaines
      USE temps
      USE champs
      USE laser



      IMPLICIT NONE

      REAL(KIND=kr), INTENT(IN)     :: tps

      
!      REAL(KIND=kr) :: coefy_ez, coefy_bx0,coefy_bx1,coefy_by, coefy_sr,
!     &                 coefz_ey, coefz_bx0,coefz_bx1,coefz_bz 
     
      REAL(KIND=kr) ::  eylaserqx, eylaserqy, ezlaserqz 
     
      REAL(KIND=kr) :: coef_qx, coef_qy, coef_qz 
    
      REAL(KIND=kr) :: coef_t, coef_y, phase_y, phase_z
      REAL(KIND=kr) :: posy, auxpy_sr 
      REAL(KIND=kr) :: tps_eff, tretard, bordy

      REAL(KIND=kr) :: posy2, coef_y2, tretard2, tps_eff2, 
     &                  coef_t2, phase_z2 

      INTEGER       :: j, ilas 

      INTERFACE
        FUNCTION calc_profil(x, type, ordre, xlarg, xdeb, xcentre)
          USE precisions
          REAL(KIND=kr), INTENT(IN)     :: x
          CHARACTER(LEN=5), INTENT(IN)  :: type
          INTEGER,       INTENT(IN)     :: ordre
          REAL(KIND=kr), INTENT(IN)     :: xlarg, xdeb, xcentre
          REAL(KIND=kr)                 :: calc_profil
        END FUNCTION calc_profil
	
        FUNCTION calc_profil_y(x, ilas)
          USE precisions
          USE laser      
          IMPLICIT NONE	   
          REAL(KIND=kr), INTENT(IN)   :: x
          INTEGER, INTENT(IN)	      :: ilas
          REAL(KIND=kr)		      :: calc_profil_y
        END FUNCTION calc_profil_y	
      END INTERFACE



      fpsr = 0._8
! --- On fait rentrer les sources l'une apres l'autre
      DO ilas=1,3
        IF(inj_laser(ilas).NE.0) THEN
          IF(inj_laser(ilas).EQ.1) nulx_inj = nx
          IF(    (y_prof(ilas).EQ.'gauss')
     &       .OR.(y_prof(ilas).EQ.'hyper')
     &       .OR.(y_prof(ilas).EQ.'cos_2')) THEN
            bordy = y_max(ilas) - y_lrg(ilas)
          ELSEIF(y_prof(ilas).EQ.'plato') THEN
            bordy = y_max(ilas) - 0.5*y_lrg(ilas) - y_deb(ilas)
          ELSE 
            bordy = ymin_gl
          ENDIF


! ---- 
!      On suppose que l angle du laser inject� et l angle de l'onde 
!      diffus�e sont �gaux
! ------------
          coef_qx = -d3*keb_a*
     &           (1. + COS(scat_angle)/COS(in_angle))
          coef_qy = dtsdx2*2.*keb_a*
     &           (1. + COS(scat_angle)/COS(in_angle))
          coef_qz = dtsdx2*2.*keb_b*
     &           (1. + COS(scat_angle)/COS(in_angle))  

! ---- 
!  Mise � jour du champ By, il faut prendre en compte bx incident et 
!  by incident
! ---------------------------
          DO j=1,ny
            posy = ymin_gl + j*pasdy
! --- + attention si conditions transverses periodiques
! ---   A FAIRE !!!!!! ??????
! --- + facteur spatial (y)
c            coef_y =  calc_profil(posy,y_prof(ilas),y_ord(ilas), 
c     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
            coef_y =  calc_profil_y(posy, ilas)
     
! --- + facteur temporel et phase
            tretard = SIN(y_ang(ilas))*(posy-y_max(ilas))
     &              + COS(y_ang(ilas))*(xmin_gl + 0.5*pasdx)
            tps_eff = tps - tretard
            coef_t =  calc_profil(tps_eff,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_y   = COS(tps_eff/lambda(ilas))
 
! ----- polarisation 1     
            ezlaserqz = coef_qz*a_0(ilas)*phase_y*y_pol(ilas)
     &                 *coef_t *coef_y 
        
	    qzbound(j) = qzbound(j) + ezlaserqz
   
            auxpy_sr = 1._8
            IF((j.EQ.nulmy).OR.(j.EQ.nulpy)) auxpy_sr = 0.5_8
            fpsr =  fpsr 
     &            + auxpy_sr*(a_0(ilas)*phase_y*y_pol(ilas)
     &                                 *coef_t*coef_y      )**2
          ENDDO


! ---- 
!  Mise � jour du champ Bz, 
!  il faut prendre en compte bz incident 
! ---------------------------
          DO j=1,ny
            posy = ymin_gl + (j-0.5_8)*pasdy
! --- + attention si conditions transverses periodiques
            IF( j.EQ.nulmy .AND. ibndc(3).EQ.-1 )
     &          posy = ymax_gl - 0.5_8*pasdy
            IF( j.EQ.nulpy+1 .AND. ibndc(4).EQ.-1 )
     &          posy = ymin_gl + 0.5_8*pasdy
     
            posy2 = ymin_gl + (j+0.5_8)*pasdy
! --- + attention si conditions transverses periodiques
!            IF( (j+1).EQ.nulmy .AND. ibndc(3).EQ.-1 )
!     &          posy2 = ymax_gl - 0.5_8*pasdy
!            IF( (j+1).EQ.nulpy+1 .AND. ibndc(4).EQ.-1 )
!     &          posy2 = ymin_gl + 0.5_8*pasdy    
    
! --- + facteur spatial (y)
c            coef_y =  calc_profil(posy,y_prof(ilas),y_ord(ilas), 
c     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
            coef_y =  calc_profil_y(posy, ilas)
     
c            coef_y2 =  calc_profil(posy2,y_prof(ilas),y_ord(ilas), 
c     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
            coef_y2 =  calc_profil_y(posy2, ilas)
	    
! --- + facteur temporel et phase
            tretard = SIN(y_ang(ilas))*(posy-y_max(ilas))
     &              + COS(y_ang(ilas))*(xmin_gl + 0.5*pasdx)
            tps_eff = tps - tretard
            coef_t =  calc_profil(tps_eff,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_z = SIN(tps_eff/lambda(ilas))
  
! --- + facteur temporel et phase 2
            tretard2 = SIN(y_ang(ilas))*(posy2-y_max(ilas))
     &              + COS(y_ang(ilas))*(xmin_gl + 0.5*pasdx)
            tps_eff2 = tps - tretard2
            coef_t2 =  calc_profil(tps_eff2,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_z2 = SIN(tps_eff2/lambda(ilas))
  
! ----- polarisation 2           
	    eylaserqx = coef_qx*a_0(ilas)*z_pol(ilas)
     &            *(phase_z*coef_t*coef_y - phase_z2*coef_t2*coef_y2) 
	    	    
	    eylaserqy = coef_qy*a_0(ilas)*z_pol(ilas)
     &            *phase_z*coef_t*coef_y 
	    
            qxbound(j) = qxbound(j) + eylaserqx
            qybound(j) = qybound(j) + eylaserqy
   

!            auxpy_sr = 1._8
!            IF((j.EQ.nulmy).OR.(j.EQ.nulpy+1)) auxpy_sr = 0._8
!            fpsr =  fpsr 
!     &            + auxpy_sr*(a_0(ilas)*phase_z*z_pol(ilas)
!     &                                 *coef_t*coef_y      )**2
          ENDDO
	  
        ENDIF
      ENDDO


!     ___________________________
      END SUBROUTINE maxlim_2d1s2_nEB_implicit
      
         


      SUBROUTINE maxlim_2d1s2_nEB_implicit_var(tps)
! ======================================================================
! Application des conditions aux limites pour les 6 champs [EB][xyz]
! a l'instant reel temps
! Retourne le flux de Poynting entre (source)
! Le type de condition est code dans les variables ibndc(:)
!   ibndc >  0 : champs echanges avec un PE voisin
!   ibndc = -1 : conditions periodiques locales a ce PE
!   ibndc = -2 : absorption - condition de Silver-Muller
!   ibndc = -3 : conducteur parfait
! ======================================================================
      USE precisions
      USE domaines
      USE temps
      USE champs
      USE laser



      IMPLICIT NONE

      REAL(KIND=kr), INTENT(IN)     :: tps

      
!      REAL(KIND=kr) :: coefy_ez, coefy_bx0,coefy_bx1,coefy_by, coefy_sr,
!     &                 coefz_ey, coefz_bx0,coefz_bx1,coefz_bz 
     
      REAL(KIND=kr) ::  eylaserqx, eylaserqy, ezlaserqz 
     
      REAL(KIND=kr) :: coef_qx, coef_qy, coef_qz 
    
      REAL(KIND=kr) :: coef_t, coef_y, phase_y, phase_z
      REAL(KIND=kr) :: posy, auxpy_sr 
      REAL(KIND=kr) :: tps_eff, tretard, bordy

      REAL(KIND=kr) :: posy2, coef_y2, tretard2, tps_eff2, 
     &                  coef_t2, phase_z2 

      INTEGER       :: j, ilas 

      INTERFACE
        FUNCTION calc_profil(x, type, ordre, xlarg, xdeb, xcentre)
          USE precisions
          REAL(KIND=kr), INTENT(IN)     :: x
          CHARACTER(LEN=5), INTENT(IN)  :: type
          INTEGER,       INTENT(IN)     :: ordre
          REAL(KIND=kr), INTENT(IN)     :: xlarg, xdeb, xcentre
          REAL(KIND=kr)                 :: calc_profil
        END FUNCTION calc_profil
      END INTERFACE



      fpsr = 0._8
! --- On fait rentrer les sources l'une apres l'autre
      DO ilas=1,3
        IF(inj_laser(ilas).NE.0) THEN
          IF(inj_laser(ilas).EQ.1) nulx_inj = nx
          IF(    (y_prof(ilas).EQ.'gauss')
     &       .OR.(y_prof(ilas).EQ.'hyper')
     &       .OR.(y_prof(ilas).EQ.'cos_2')) THEN
            bordy = y_max(ilas) - y_lrg(ilas)
          ELSEIF(y_prof(ilas).EQ.'plato') THEN
            bordy = y_max(ilas) - 0.5*y_lrg(ilas) - y_deb(ilas)
          ELSE 
            bordy = ymin_gl
          ENDIF


! ---- 
!      On suppose que l angle du laser inject� et l angle de l'onde 
!      diffus�e sont �gaux
! ------------
          coef_qx = -vqx4tild
          coef_qy = -vqy4tild

          coef_qz = 2.*dtsdx2*deltakeb*COS(scat_angle)

!          coef_qz = 2.*dtsdx2*deltaf
!          coef_qz = 2.*dtsdx2*deltaff

! ---- 
!  Mise � jour du champ By, il faut prendre en compte bx incident et 
!  by incident
! ---------------------------
          DO j=1,ny
            posy = ymin_gl + j*pasdy
! --- + attention si conditions transverses periodiques
! ---   A FAIRE !!!!!! ??????
! --- + facteur spatial (y)
            coef_y =  calc_profil(posy,y_prof(ilas),y_ord(ilas), 
     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
     
! --- + facteur temporel et phase
            tretard = SIN(y_ang(ilas))*(posy-y_max(ilas))
     &              + COS(y_ang(ilas))*(xmin_gl + 0.5*pasdx)
            tps_eff = tps - tretard
            coef_t =  calc_profil(tps_eff,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_y   = COS(tps_eff/lambda(ilas))
 
! ----- polarisation 1     
            ezlaserqz = coef_qz*a_0(ilas)*phase_y*y_pol(ilas)
     &                 *coef_t *coef_y 
        
	    qzbound(j) = qzbound(j) + ezlaserqz
   
            auxpy_sr = 1._8
            IF((j.EQ.nulmy).OR.(j.EQ.nulpy)) auxpy_sr = 0.5_8
            fpsr =  fpsr 
     &            + auxpy_sr*(a_0(ilas)*phase_y*y_pol(ilas)
     &                                 *coef_t*coef_y      )**2
          ENDDO


! ---- 
!  Mise � jour du champ Bz, 
!  il faut prendre en compte bz incident 
! ---------------------------
          DO j=1,ny
            posy = ymin_gl + (j-0.5_8)*pasdy
! --- + attention si conditions transverses periodiques
            IF( j.EQ.nulmy .AND. ibndc(3).EQ.-1 )
     &          posy = ymax_gl - 0.5_8*pasdy
            IF( j.EQ.nulpy+1 .AND. ibndc(4).EQ.-1 )
     &          posy = ymin_gl + 0.5_8*pasdy
     
            posy2 = ymin_gl + (j+0.5_8)*pasdy
! --- + attention si conditions transverses periodiques
!            IF( (j+1).EQ.nulmy .AND. ibndc(3).EQ.-1 )
!     &          posy2 = ymax_gl - 0.5_8*pasdy
!            IF( (j+1).EQ.nulpy+1 .AND. ibndc(4).EQ.-1 )
!     &          posy2 = ymin_gl + 0.5_8*pasdy    
    
! --- + facteur spatial (y)
            coef_y =  calc_profil(posy,y_prof(ilas),y_ord(ilas), 
     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
     
            coef_y2 =  calc_profil(posy2,y_prof(ilas),y_ord(ilas), 
     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
    
! --- + facteur temporel et phase
            tretard = SIN(y_ang(ilas))*(posy-y_max(ilas))
     &              + COS(y_ang(ilas))*(xmin_gl + 0.5*pasdx)
            tps_eff = tps - tretard
            coef_t =  calc_profil(tps_eff,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_z = COS(tps_eff/lambda(ilas))
  
! --- + facteur temporel et phase 2
            tretard2 = SIN(y_ang(ilas))*(posy2-y_max(ilas))
     &              + COS(y_ang(ilas))*(xmin_gl + 0.5*pasdx)
            tps_eff2 = tps - tretard2
            coef_t2 =  calc_profil(tps_eff2,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_z2 = COS(tps_eff2/lambda(ilas))
  
! ----- polarisation 2           
	    eylaserqx = coef_qx*a_0(ilas)*z_pol(ilas)
     &            *(phase_z2*coef_t2*coef_y2 - phase_z*coef_t*coef_y) 
	    	    
	    eylaserqy = coef_qy*a_0(ilas)*z_pol(ilas)
     &            *phase_z*coef_t*coef_y 
	    
            qxbound(j) = qxbound(j) + eylaserqx
            qybound(j) = qybound(j) + eylaserqy
   

            auxpy_sr = 1._8
            IF((j.EQ.nulmy).OR.(j.EQ.nulpy+1)) auxpy_sr = 0._8
            fpsr =  fpsr 
     &            + auxpy_sr*(a_0(ilas)*phase_z*z_pol(ilas)
     &                                 *coef_t*coef_y      )**2
          ENDDO
	  
        ENDIF
      ENDDO


!     ___________________________
      END SUBROUTINE maxlim_2d1s2_nEB_implicit_var




      SUBROUTINE maxlim_2d1s2_nEB_implicit_step2(tps)
! ======================================================================
! Application des conditions aux limites pour les 6 champs [EB][xyz]
! a l'instant reel temps
! Retourne le flux de Poynting entre (source)
! Le type de condition est code dans les variables ibndc(:)
!   ibndc >  0 : champs echanges avec un PE voisin
!   ibndc = -1 : conditions periodiques locales a ce PE
!   ibndc = -2 : absorption - condition de Silver-Muller
!   ibndc = -3 : conducteur parfait
! ======================================================================
      USE precisions
      USE domaines
      USE temps
      USE champs
      USE laser



      IMPLICIT NONE

      REAL(KIND=kr), INTENT(IN)     :: tps

      
!      REAL(KIND=kr) :: coefy_ez, coefy_bx0,coefy_bx1,coefy_by, coefy_sr,
!     &                 coefz_ey, coefz_bx0,coefz_bx1,coefz_bz 
     
      REAL(KIND=kr) :: eylaser, ezlaser 
     
      REAL(KIND=kr) :: coef_ey, coef_ez 
    
      REAL(KIND=kr) :: coef_t, coef_y, phase_y, phase_z
      REAL(KIND=kr) :: posy, auxpy_sr 
      REAL(KIND=kr) :: tps_eff, tretard, bordy

      INTEGER       :: j, ilas 

      INTERFACE
        FUNCTION calc_profil(x, type, ordre, xlarg, xdeb, xcentre)
          USE precisions
          REAL(KIND=kr), INTENT(IN)     :: x
          CHARACTER(LEN=5), INTENT(IN)  :: type
          INTEGER,       INTENT(IN)     :: ordre
          REAL(KIND=kr), INTENT(IN)     :: xlarg, xdeb, xcentre
          REAL(KIND=kr)                 :: calc_profil
        END FUNCTION calc_profil
	
        FUNCTION calc_profil_y(x, ilas)
          USE precisions
          USE laser      
          IMPLICIT NONE	   
          REAL(KIND=kr), INTENT(IN)   :: x
          INTEGER, INTENT(IN)	      :: ilas
          REAL(KIND=kr)		      :: calc_profil_y
        END FUNCTION calc_profil_y
      END INTERFACE



      fpsr = 0._8
! --- On fait rentrer les sources l'une apres l'autre
      DO ilas=1,3
        IF(inj_laser(ilas).NE.0) THEN
          IF(inj_laser(ilas).EQ.1) nulx_inj = nx
          IF(    (y_prof(ilas).EQ.'gauss')
     &       .OR.(y_prof(ilas).EQ.'hyper')
     &       .OR.(y_prof(ilas).EQ.'cos_2')) THEN
            bordy = y_max(ilas) - y_lrg(ilas)
          ELSEIF(y_prof(ilas).EQ.'plato') THEN
            bordy = y_max(ilas) - 0.5*y_lrg(ilas) - y_deb(ilas)
          ELSE 
            bordy = ymin_gl
          ENDIF


! ---- 
!      On suppose que l angle du laser inject� et l angle de l'onde 
!      diffus�e sont �gaux
! ------------
          coef_ey = 4.*keb_a*
     &           (1. + COS(scat_angle)/COS(in_angle)) 
          coef_ez = 4.*keb_b*
     &           (1. + COS(scat_angle)/COS(in_angle))

! ---- 
!  Mise � jour du champ By, il faut prendre en compte bx incident et 
!  by incident
! ---------------------------
          DO j=1,ny
            posy = ymin_gl + j*pasdy
! --- + attention si conditions transverses periodiques
! ---   A FAIRE !!!!!! ??????
! --- + facteur spatial (y)
c            coef_y =  calc_profil(posy,y_prof(ilas),y_ord(ilas), 
c     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
            coef_y =  calc_profil_y(posy, ilas)
     
! --- + facteur temporel et phase
            tretard = SIN(y_ang(ilas))*(posy-y_max(ilas))
     &              + COS(y_ang(ilas))*(xmin_gl + 0.5*pasdx)
            tps_eff = tps - tretard
            coef_t =  calc_profil(tps_eff,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_y   = COS(tps_eff/lambda(ilas))
 
! ----- polarisation 1     
            ezlaser = coef_ez*a_0(ilas)*phase_y*y_pol(ilas)
     &                 *coef_t *coef_y 
        
	    ez(nulx_inj,j) = ez(nulx_inj,j) + ezlaser 
   
            auxpy_sr = 1._8
            IF((j.EQ.nulmy).OR.(j.EQ.nulpy)) auxpy_sr = 0.5_8
            fpsr =  fpsr 
     &            + auxpy_sr*(a_0(ilas)*phase_y*y_pol(ilas)
     &                                 *coef_t*coef_y      )**2
          ENDDO


! ---- 
!  Mise � jour du champ Bz, 
!  il faut prendre en compte bz incident 
! ---------------------------
          DO j=1,ny
            posy = ymin_gl + (j-0.5_8)*pasdy
! --- + attention si conditions transverses periodiques
            IF( j.EQ.nulmy .AND. ibndc(3).EQ.-1 )
     &          posy = ymax_gl - 0.5_8*pasdy
            IF( j.EQ.nulpy+1 .AND. ibndc(4).EQ.-1 )
     &          posy = ymin_gl + 0.5_8*pasdy    
    
! --- + facteur spatial (y)
c            coef_y =  calc_profil(posy,y_prof(ilas),y_ord(ilas), 
c     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
            coef_y =  calc_profil_y(posy, ilas)
	    
! --- + facteur temporel et phase
            tretard = SIN(y_ang(ilas))*(posy-y_max(ilas))
     &              + COS(y_ang(ilas))*(xmin_gl + 0.5*pasdx)
            tps_eff = tps - tretard
            coef_t =  calc_profil(tps_eff,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_z = SIN(tps_eff/lambda(ilas))
  
! ----- polarisation 2            	    
	    eylaser = coef_ey*a_0(ilas)*z_pol(ilas)
     &            *phase_z*coef_t*coef_y 
	    
            ey(nulx_inj,j) = ey(nulx_inj,j) + eylaser
	       

            auxpy_sr = 1._8
            IF((j.EQ.nulmy).OR.(j.EQ.nulpy+1)) auxpy_sr = 0._8
! --- + facteur temporel et phase
            fpsr =  fpsr 
     &            + auxpy_sr*(a_0(ilas)*phase_z*z_pol(ilas)
     &                                 *coef_t*coef_y      )**2
          ENDDO
	  
        ENDIF
      ENDDO


!     ___________________________
      END SUBROUTINE maxlim_2d1s2_nEB_implicit_step2



      SUBROUTINE maxlim_2d1s2_nEB_implicit_step2_var(tps)
! ======================================================================
! Application des conditions aux limites pour les 6 champs [EB][xyz]
! a l'instant reel temps
! Retourne le flux de Poynting entre (source)
! Le type de condition est code dans les variables ibndc(:)
!   ibndc >  0 : champs echanges avec un PE voisin
!   ibndc = -1 : conditions periodiques locales a ce PE
!   ibndc = -2 : absorption - condition de Silver-Muller
!   ibndc = -3 : conducteur parfait
! ======================================================================
      USE precisions
      USE domaines
      USE temps
      USE champs
      USE laser



      IMPLICIT NONE

      REAL(KIND=kr), INTENT(IN)     :: tps

      
!      REAL(KIND=kr) :: coefy_ez, coefy_bx0,coefy_bx1,coefy_by, coefy_sr,
!     &                 coefz_ey, coefz_bx0,coefz_bx1,coefz_bz 
     
      REAL(KIND=kr) :: eylaser, ezlaser 
     
      REAL(KIND=kr) :: coef_ey, coef_ez 
    
      REAL(KIND=kr) :: coef_t, coef_y, phase_y, phase_z
      REAL(KIND=kr) :: posy, auxpy_sr 
      REAL(KIND=kr) :: tps_eff, tretard, bordy

      INTEGER       :: j, ilas 

      INTERFACE
        FUNCTION calc_profil(x, type, ordre, xlarg, xdeb, xcentre)
          USE precisions
          REAL(KIND=kr), INTENT(IN)     :: x
          CHARACTER(LEN=5), INTENT(IN)  :: type
          INTEGER,       INTENT(IN)     :: ordre
          REAL(KIND=kr), INTENT(IN)     :: xlarg, xdeb, xcentre
          REAL(KIND=kr)                 :: calc_profil
        END FUNCTION calc_profil
      END INTERFACE



      fpsr = 0._8
! --- On fait rentrer les sources l'une apres l'autre
      DO ilas=1,3
        IF(inj_laser(ilas).NE.0) THEN
          IF(inj_laser(ilas).EQ.1) nulx_inj = nx
          IF(    (y_prof(ilas).EQ.'gauss')
     &       .OR.(y_prof(ilas).EQ.'hyper')
     &       .OR.(y_prof(ilas).EQ.'cos_2')) THEN
            bordy = y_max(ilas) - y_lrg(ilas)
          ELSEIF(y_prof(ilas).EQ.'plato') THEN
            bordy = y_max(ilas) - 0.5*y_lrg(ilas) - y_deb(ilas)
          ELSE 
            bordy = ymin_gl
          ENDIF


! ---- 
!      On suppose que l angle du laser inject� et l angle de l'onde 
!      diffus�e sont �gaux
! ------------
          coef_ey = 8./(1. +dtsdx*COS(scat_angle))
	  
          coef_ez = 4.*deltakeb*COS(scat_angle)  
	  
!          coef_ez = 4.*deltaf 
!          coef_ez = 4.*deltaff 

! ---- 
!  Mise � jour du champ By, il faut prendre en compte bx incident et 
!  by incident
! ---------------------------
          DO j=1,ny
            posy = ymin_gl + j*pasdy
! --- + attention si conditions transverses periodiques
! ---   A FAIRE !!!!!! ??????
! --- + facteur spatial (y)
            coef_y =  calc_profil(posy,y_prof(ilas),y_ord(ilas), 
     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
     
! --- + facteur temporel et phase
            tretard = SIN(y_ang(ilas))*(posy-y_max(ilas))
     &              + COS(y_ang(ilas))*(xmin_gl + 0.5*pasdx)
            tps_eff = tps - tretard
            coef_t =  calc_profil(tps_eff,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_y   = SIN(tps_eff/lambda(ilas))
 
! ----- polarisation 1     
            ezlaser = coef_ez*a_0(ilas)*phase_y*y_pol(ilas)
     &                 *coef_t *coef_y 
        
	    ez(nulx_inj,j) = ez(nulx_inj,j) + ezlaser 
   
            auxpy_sr = 1._8
            IF((j.EQ.nulmy).OR.(j.EQ.nulpy)) auxpy_sr = 0.5_8
            fpsr =  fpsr 
     &            + auxpy_sr*(a_0(ilas)*phase_y*y_pol(ilas)
     &                                 *coef_t*coef_y      )**2
          ENDDO


! ---- 
!  Mise � jour du champ Bz, 
!  il faut prendre en compte bz incident 
! ---------------------------
          DO j=1,ny
            posy = ymin_gl + (j-0.5_8)*pasdy
! --- + attention si conditions transverses periodiques
            IF( j.EQ.nulmy .AND. ibndc(3).EQ.-1 )
     &          posy = ymax_gl - 0.5_8*pasdy
            IF( j.EQ.nulpy+1 .AND. ibndc(4).EQ.-1 )
     &          posy = ymin_gl + 0.5_8*pasdy    
    
! --- + facteur spatial (y)
            coef_y =  calc_profil(posy,y_prof(ilas),y_ord(ilas), 
     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
   
! --- + facteur temporel et phase
            tretard = SIN(y_ang(ilas))*(posy-y_max(ilas))
     &              + COS(y_ang(ilas))*(xmin_gl + 0.5*pasdx)
            tps_eff = tps - tretard
            coef_t =  calc_profil(tps_eff,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_z = SIN(tps_eff/lambda(ilas))
  
! ----- polarisation 2            	    
	    eylaser = coef_ey*a_0(ilas)*z_pol(ilas)
     &            *phase_z*coef_t*coef_y 
	    
            ey(nulx_inj,j) = ey(nulx_inj,j) + eylaser
	       

            auxpy_sr = 1._8
            IF((j.EQ.nulmy).OR.(j.EQ.nulpy+1)) auxpy_sr = 0._8
            fpsr =  fpsr 
     &            + auxpy_sr*(a_0(ilas)*phase_z*z_pol(ilas)
     &                                 *coef_t*coef_y      )**2
          ENDDO
	  
        ENDIF
      ENDDO


!     ___________________________
      END SUBROUTINE maxlim_2d1s2_nEB_implicit_step2_var


      SUBROUTINE maxlim_2d1s2_nEB(tps)
! ======================================================================
! Application des conditions aux limites pour les 6 champs [EB][xyz]
! a l'instant reel temps
! Retourne le flux de Poynting entre (source)
! Le type de condition est code dans les variables ibndc(:)
!   ibndc >  0 : champs echanges avec un PE voisin
!   ibndc = -1 : conditions periodiques locales a ce PE
!   ibndc = -2 : absorption - condition de Silver-Muller
!   ibndc = -3 : conducteur parfait
! ======================================================================
      USE precisions
      USE domaines
      USE temps
      USE champs
      USE laser



      IMPLICIT NONE

      REAL(KIND=kr), INTENT(IN)     :: tps

      
!      REAL(KIND=kr) :: coefy_ez, coefy_bx0,coefy_bx1,coefy_by, coefy_sr,
!     &                 coefz_ey, coefz_bx0,coefz_bx1,coefz_bz 
     
      REAL(KIND=kr) :: coef_ey, coef_ez 

      REAL(KIND=kr) :: ez_inc, ey_inc 
     
      REAL(KIND=kr) :: coef_t, coef_y, phase_y, phase_z
      REAL(KIND=kr) :: posy, auxpy_sr 
      REAL(KIND=kr) :: tps_eff, tretard, bordy
      INTEGER       :: j, ilas 

      INTERFACE
        FUNCTION calc_profil(x, type, ordre, xlarg, xdeb, xcentre)
          USE precisions
          REAL(KIND=kr), INTENT(IN)     :: x
          CHARACTER(LEN=5), INTENT(IN)  :: type
          INTEGER,       INTENT(IN)     :: ordre
          REAL(KIND=kr), INTENT(IN)     :: xlarg, xdeb, xcentre
          REAL(KIND=kr)                 :: calc_profil
        END FUNCTION calc_profil
	
        FUNCTION calc_profil_y(x, ilas)
          USE precisions
          USE laser      
          IMPLICIT NONE	   
          REAL(KIND=kr), INTENT(IN)   :: x
          INTEGER, INTENT(IN)	      :: ilas
          REAL(KIND=kr)		      :: calc_profil_y
        END FUNCTION calc_profil_y
      END INTERFACE



      fpsr = 0._8
! --- On fait rentrer les sources l'une apres l'autre
      DO ilas=1,3
        IF(inj_laser(ilas).NE.0) THEN
          IF(inj_laser(ilas).EQ.1) nulx_inj = nx
          IF(    (y_prof(ilas).EQ.'gauss')
     &       .OR.(y_prof(ilas).EQ.'hyper')
     &       .OR.(y_prof(ilas).EQ.'cos_2')) THEN
            bordy = y_max(ilas) - y_lrg(ilas)
          ELSEIF(y_prof(ilas).EQ.'plato') THEN
            bordy = y_max(ilas) - 0.5*y_lrg(ilas) - y_deb(ilas)
          ELSE 
            bordy = ymin_gl
          ENDIF


! ---- on suppose que l angle du laser inject� et l angle de l'onde 
!      diffus�e sont �gaux
! ------------
          coef_ey = 2.
          coef_ez = 2.

! ---- 
!  Mise � jour du champ By, il faut prendre en compte bx incident et 
!  by incident
! ---------------------------
          DO j=1,ny
            posy = ymin_gl + j*pasdy
! --- + attention si conditions transverses periodiques
! ---   A FAIRE !!!!!! ??????
! --- + facteur spatial (y)
c            coef_y =  calc_profil(posy,y_prof(ilas),y_ord(ilas), 
c     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
            coef_y =  calc_profil_y(posy, ilas)
	    
! --- + facteur temporel et phase
            tretard = SIN(y_ang(ilas))*(posy-y_max(ilas))
            tps_eff = tps - tretard
            coef_t =  calc_profil(tps_eff,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_y   = COS(tps_eff/lambda(ilas))
 
! ----- polarisation 1     
            ez_inc = coef_ez*a_0(ilas)*phase_y*y_pol(ilas)
     &                 *coef_t *coef_y  
     
            ez(nulx_inj,j) = ez(nulx_inj,j) + ez_inc 
   
            auxpy_sr = 1._8
            IF((j.EQ.nulmy).OR.(j.EQ.nulpy)) auxpy_sr = 0.5_8
            fpsr =  fpsr 
     &            + auxpy_sr*(a_0(ilas)*phase_y*y_pol(ilas)
     &                                 *coef_t*coef_y      )**2
          ENDDO


! ---- 
!  Mise � jour du champ Bz, 
!  il faut prendre en compte bz incident 
! ---------------------------
          DO j=1,ny
            posy = ymin_gl + (j-0.5_8)*pasdy
! --- + attention si conditions transverses periodiques
            IF( j.EQ.nulmy .AND. ibndc(3).EQ.-1 )
     &          posy = ymax_gl - 0.5_8*pasdy
            IF( j.EQ.nulpy+1 .AND. ibndc(4).EQ.-1 )
     &          posy = ymin_gl + 0.5_8*pasdy
! --- + facteur spatial (y)
c            coef_y =  calc_profil(posy,y_prof(ilas),y_ord(ilas), 
c     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
            coef_y =  calc_profil_y(posy, ilas)
	    
! --- + facteur temporel et phase
            tretard = SIN(y_ang(ilas))*(posy-y_max(ilas))
            tps_eff = tps - tretard
            coef_t =  calc_profil(tps_eff,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_z = COS(tps_eff/lambda(ilas))
  
! ----- polarisation 2        
            ey_inc = coef_ey*a_0(ilas)*phase_z*z_pol(ilas)
     &                  *coef_t *coef_y      
   
	    ey(nulx_inj,j) = ey(nulx_inj,j) + ey_inc 

            auxpy_sr = 1._8
            IF((j.EQ.nulmy).OR.(j.EQ.nulpy+1)) auxpy_sr = 0._8
            fpsr =  fpsr 
     &            + auxpy_sr*(a_0(ilas)*phase_z*z_pol(ilas)
     &                                 *coef_t*coef_y      )**2
          ENDDO
	  
        ENDIF
      ENDDO


!     ___________________________
      END SUBROUTINE maxlim_2d1s2_nEB



      SUBROUTINE maxlim_2d1s2_E(tps)
! ======================================================================
! Application des conditions aux limites pour les 6 champs [EB][xyz]
! a l'instant reel temps
! Retourne le flux de Poynting entre (source)
! Le type de condition est code dans les variables ibndc(:)
!   ibndc >  0 : champs echanges avec un PE voisin
!   ibndc = -1 : conditions periodiques locales a ce PE
!   ibndc = -2 : absorption - condition de Silver-Muller
!   ibndc = -3 : conducteur parfait
! ======================================================================
      USE precisions
      USE domaines
      USE temps
      USE champs
      USE laser



      IMPLICIT NONE

      REAL(KIND=kr), INTENT(IN)     :: tps

      
!      REAL(KIND=kr) :: coefy_ez, coefy_bx0,coefy_bx1,coefy_by, coefy_sr,
!     &                 coefz_ey, coefz_bx0,coefz_bx1,coefz_bz 
     

      REAL(KIND=kr) :: coefp11_ez
!  coefp12_ez
      REAL(KIND=kr) :: coefp2_ey
      REAL(KIND=kr) :: ez_inc, ey_inc
!  ez_inc2 
      REAL(KIND=kr) :: coefp1_bx, coefp2_ex
      REAL(KIND=kr) :: bx_inc, ex_inc 
!  bx_gauss 
      REAL(KIND=kr) :: coefp1_gauss 
      REAL(KIND=kr) :: gauss1, gauss2 
      
      REAL(KIND=kr) :: coef_t, coef_y, phase_y, phase_z 

      REAL(KIND=kr) :: phase_y_ci
      REAL(KIND=kr) :: phase_y0 
!      REAL(KIND=kr) :: phase_z1, phase_z2 
      REAL(KIND=kr) :: phase_y1, phase_y2
      REAL(KIND=kr) :: posy, auxpy_sr 
      REAL(KIND=kr) :: tps_eff, tretard, bordy
      INTEGER       :: j, ilas 

      INTERFACE
        FUNCTION calc_profil(x, type, ordre, xlarg, xdeb, xcentre)
          USE precisions
          REAL(KIND=kr), INTENT(IN)     :: x
          CHARACTER(LEN=5), INTENT(IN)  :: type
          INTEGER,       INTENT(IN)     :: ordre
          REAL(KIND=kr), INTENT(IN)     :: xlarg, xdeb, xcentre
          REAL(KIND=kr)                 :: calc_profil
        END FUNCTION calc_profil
      END INTERFACE



      fpsr = 0._8
! --- On fait rentrer les sources l'une apres l'autre
      DO ilas=1,3
        IF(inj_laser(ilas).NE.0) THEN
          IF(inj_laser(ilas).EQ.1) nulx_inj = nx
          IF(    (y_prof(ilas).EQ.'gauss')
     &       .OR.(y_prof(ilas).EQ.'hyper')
     &       .OR.(y_prof(ilas).EQ.'cos_2')) THEN
            bordy = y_max(ilas) - y_lrg(ilas)
          ELSEIF(y_prof(ilas).EQ.'plato') THEN
            bordy = y_max(ilas) - 0.5*y_lrg(ilas) - y_deb(ilas)
          ELSE 
            bordy = ymin_gl
          ENDIF


! ---- coefficients pour la m�thode alternative 
          coefp11_ez = a_0(ilas)*dt*(1+COS(y_ang(ilas)))
!          coefp12_ez = a_0(ilas)*dt*COS(y_ang(ilas))
	  
          coefp2_ey = a_0(ilas)*dt*COS(y_ang(ilas))
     &                          *(1+COS(y_ang(ilas)))	

          coefp2_ex =  cb_45*dt*a_0(ilas)*SIN(y_ang(ilas))**2
          coefp1_bx =  ce_45*dt*a_0(ilas)*SIN(y_ang(ilas))**2

          coefp1_gauss = ce_45*dt*a_0(ilas)
	  gauss1 = -4*LOG(2.)/y_lrg(ilas)**2
	  gauss2 = (4*LOG(2.)/y_lrg(ilas)**2)**2 


! ---- 
!  Mise � jour du champ By, il faut prendre en compte bx incident et 
!  by incident
! ---------------------------
          DO j=1,ny
            posy = ymin_gl + j*pasdy
! --- + attention si conditions transverses periodiques
! ---   A FAIRE !!!!!! ??????
! --- + facteur spatial (y)
            coef_y =  calc_profil(posy,y_prof(ilas),y_ord(ilas), 
     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
     
! --- + facteur temporel et phase
            tretard = SIN(y_ang(ilas))*(posy-y_max(ilas))
            tps_eff = tps - tretard
            coef_t =  calc_profil(tps_eff,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_y_ci = COS(tretard/lambda(ilas))
            phase_y0 = COS((tps_eff)/lambda(ilas))
            phase_y1 = COS((tps_eff+dts2)/lambda(ilas))
            phase_y2 = COS((tps_eff+dt)/lambda(ilas))
 
! ----- polarisation 1     
            ez_inc = coefp11_ez*y_pol(ilas)*coef_t*coef_y*phase_y0

!            ez_inc2 = a_0(ilas)*y_pol(ilas)*coef_t*coef_y*phase_y0 
     
! ----- onde plane     
            bx_inc = coefp1_bx*(phase_y_ci - phase_y0)
     &         *y_pol(ilas)*coef_t*coef_y            

! ----- faisceau gaussien
!            bx_inc = coefp1_gauss*(phase_y1+phase_y_ci)*y_pol(ilas)*
!     &         coef_t*coef_y*(gauss1 + gauss2*(posy-y_max(ilas))**2)

            ez(nulx_inj,j) = ez(nulx_inj,j) + ez_inc + bx_inc 

!            ez(nulx_inj,j) = ez_inc2
   
            auxpy_sr = 1._8
            IF((j.EQ.nulmy).OR.(j.EQ.nulpy)) auxpy_sr = 0.5_8
            fpsr =  fpsr 
     &            + auxpy_sr*(a_0(ilas)*phase_y*y_pol(ilas)
     &                                 *coef_t*coef_y      )**2
          ENDDO


! ---- 
!  Mise � jour du champ Bz, 
!  il faut prendre en compte bz incident 
! ---------------------------
          DO j=1,ny
            posy = ymin_gl + (j-0.5_8)*pasdy
! --- + attention si conditions transverses periodiques
            IF( j.EQ.nulmy .AND. ibndc(3).EQ.-1 )
     &          posy = ymax_gl - 0.5_8*pasdy
            IF( j.EQ.nulpy+1 .AND. ibndc(4).EQ.-1 )
     &          posy = ymin_gl + 0.5_8*pasdy
! --- + facteur spatial (y)
            coef_y =  calc_profil(posy,y_prof(ilas),y_ord(ilas), 
     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
! --- + facteur temporel et phase
            tretard = SIN(y_ang(ilas))*(posy-y_max(ilas))
            tps_eff = tps - tretard
            coef_t =  calc_profil(tps_eff,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_z = COS(tps_eff/lambda(ilas))
!            phase_z1 = SIN(tps_eff/lambda(ilas))
!            phase_z2 = SIN((tps_eff+pasdt)/lambda(ilas))
  
! ----- polarisation 2        
            ey_inc = coefp2_ey*phase_z*z_pol(ilas)
     &                  *coef_t *coef_y   

            ex_inc = coefp2_ex*phase_z*z_pol(ilas)
     &                  *coef_t *coef_y      
   
	    ey(nulx_inj,j) = ey(nulx_inj,j) + ey_inc 
     &         + ex_inc

            auxpy_sr = 1._8
            IF((j.EQ.nulmy).OR.(j.EQ.nulpy+1)) auxpy_sr = 0._8
            fpsr =  fpsr 
     &            + auxpy_sr*(a_0(ilas)*phase_z*z_pol(ilas)
     &                                 *coef_t*coef_y      )**2
          ENDDO
	  
        ENDIF
      ENDDO


!     ___________________________
      END SUBROUTINE maxlim_2d1s2_E
      
      
      
      SUBROUTINE maxlim_huygens_E(tps)
! ======================================================================
! Application des conditions aux limites pour les 6 champs [EB][xyz]
! a l'instant reel temps
! Retourne le flux de Poynting entre (source)
! Le type de condition est code dans les variables ibndc(:)
!   ibndc >  0 : champs echanges avec un PE voisin
!   ibndc = -1 : conditions periodiques locales a ce PE
!   ibndc = -2 : absorption - condition de Silver-Muller
!   ibndc = -3 : conducteur parfait
! ======================================================================
      USE precisions
      USE domaines
      USE temps
      USE champs
      USE laser



      IMPLICIT NONE

      REAL(KIND=kr), INTENT(IN)     :: tps

      
!      REAL(KIND=kr) :: coefy_ez, coefy_bx0,coefy_bx1,coefy_by, coefy_sr,
!     &                 coefz_ey, coefz_bx0,coefz_bx1,coefz_bz 
     
!      REAL(KIND=kr) :: coefp1_by, coefp2_bz
      REAL(KIND=kr) :: coefp1_ez, coefp2_ey
!      REAL(KIND=kr) :: coefp2_ex, coefp1_bx
      REAL(KIND=kr) :: ez_inc, ey_inc
!      REAL(KIND=kr) :: by_inc, bz_inc, bx_inc
      
      REAL(KIND=kr) :: coef_t, coef_y, phase_y, phase_z
      REAL(KIND=kr) :: posy, auxpy_sr 
      REAL(KIND=kr) :: tps_eff, tretard, bordy
      INTEGER       :: j, ilas 

      INTERFACE
        FUNCTION calc_profil(x, type, ordre, xlarg, xdeb, xcentre)
          USE precisions
          REAL(KIND=kr), INTENT(IN)     :: x
          CHARACTER(LEN=5), INTENT(IN)  :: type
          INTEGER,       INTENT(IN)     :: ordre
          REAL(KIND=kr), INTENT(IN)     :: xlarg, xdeb, xcentre
          REAL(KIND=kr)                 :: calc_profil
        END FUNCTION calc_profil
      END INTERFACE



      fpsr = 0._8
! --- On fait rentrer les sources l'une apres l'autre
      DO ilas=1,3
        IF(inj_laser(ilas).NE.0) THEN
          IF(inj_laser(ilas).EQ.1) nulx_inj = nx
          IF(    (y_prof(ilas).EQ.'gauss')
     &       .OR.(y_prof(ilas).EQ.'hyper')
     &       .OR.(y_prof(ilas).EQ.'cos_2')) THEN
            bordy = y_max(ilas) - y_lrg(ilas)
          ELSEIF(y_prof(ilas).EQ.'plato') THEN
            bordy = y_max(ilas) - 0.5*y_lrg(ilas) - y_deb(ilas)
          ELSE 
            bordy = ymin_gl
          ENDIF


! ---- coefficients pour la m�thode de Huygens
          coefp1_ez = -dtsdx*a_0(ilas)

          coefp2_ey = -dtsdx*a_0(ilas)*COS(y_ang(ilas))	


! ---- 
!  Mise � jour du champ By, il faut prendre en compte bx incident et 
!  by incident
! ---------------------------
          DO j=1,ny
            posy = ymin_gl + j*pasdy
! --- + attention si conditions transverses periodiques
! ---   A FAIRE !!!!!! ??????
! --- + facteur spatial (y)
            coef_y =  calc_profil(posy,y_prof(ilas),y_ord(ilas), 
     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
     
! --- + facteur temporel et phase
            tretard = SIN(y_ang(ilas))*(posy-y_max(ilas))
            tps_eff = tps - tretard
            coef_t =  calc_profil(tps_eff,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_y   = COS(tps_eff/lambda(ilas))
 
! ----- polarisation 1     
            ez_inc = coefp1_ez*phase_y*y_pol(ilas)
     &                 *coef_t *coef_y    	    

            ez(nulx_inj-1,j) = ez(nulx_inj-1,j) + ez_inc         
	    
    
            auxpy_sr = 1._8
            IF((j.EQ.nulmy).OR.(j.EQ.nulpy)) auxpy_sr = 0.5_8
            fpsr =  fpsr 
     &            + auxpy_sr*(a_0(ilas)*phase_y*y_pol(ilas)
     &                                 *coef_t*coef_y      )**2
          ENDDO


! ---- 
!  Mise � jour du champ Bz, 
!  il faut prendre en compte bz incident 
! ---------------------------
          DO j=1,ny
            posy = ymin_gl + (j-0.5_8)*pasdy
! --- + attention si conditions transverses periodiques
            IF( j.EQ.nulmy .AND. ibndc(3).EQ.-1 )
     &          posy = ymax_gl - 0.5_8*pasdy
            IF( j.EQ.nulpy+1 .AND. ibndc(4).EQ.-1 )
     &          posy = ymin_gl + 0.5_8*pasdy
! --- + facteur spatial (y)
            coef_y =  calc_profil(posy,y_prof(ilas),y_ord(ilas), 
     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
! --- + facteur temporel et phase
            tretard = SIN(y_ang(ilas))*(posy-y_max(ilas))
            tps_eff = tps - tretard
            coef_t =  calc_profil(tps_eff,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_z = COS(tps_eff/lambda(ilas))
  
! ----- polarisation 2        
            ey_inc = coefp2_ey*phase_z*z_pol(ilas)
     &                  *coef_t *coef_y  
    
	    ey(nulx_inj-1,j) = ey(nulx_inj-1,j) + ey_inc


            auxpy_sr = 1._8
            IF((j.EQ.nulmy).OR.(j.EQ.nulpy+1)) auxpy_sr = 0._8
            fpsr =  fpsr 
     &            + auxpy_sr*(a_0(ilas)*phase_z*z_pol(ilas)
     &                                 *coef_t*coef_y      )**2
          ENDDO
	  
        ENDIF
      ENDDO


!     ___________________________
      END SUBROUTINE maxlim_huygens_E
      
      
      
      SUBROUTINE maxlim_huygens_E_step2(tps)
! ======================================================================
! Application des conditions aux limites pour les 6 champs [EB][xyz]
! a l'instant reel temps
! Retourne le flux de Poynting entre (source)
! Le type de condition est code dans les variables ibndc(:)
!   ibndc >  0 : champs echanges avec un PE voisin
!   ibndc = -1 : conditions periodiques locales a ce PE
!   ibndc = -2 : absorption - condition de Silver-Muller
!   ibndc = -3 : conducteur parfait
! ======================================================================
      USE precisions
      USE domaines
      USE temps
      USE champs
      USE laser



      IMPLICIT NONE

      REAL(KIND=kr), INTENT(IN)     :: tps

      
!      REAL(KIND=kr) :: coefy_ez, coefy_bx0,coefy_bx1,coefy_by, coefy_sr,
!     &                 coefz_ey, coefz_bx0,coefz_bx1,coefz_bz 
     
      REAL(KIND=kr) :: coefp1_by, coefp2_bz
      REAL(KIND=kr) :: by_inc, bz_inc 
      
      REAL(KIND=kr) :: coef_t, coef_y, phase_y, phase_z
      REAL(KIND=kr) :: posy, auxpy_sr 
      REAL(KIND=kr) :: tps_eff, tretard, bordy
      INTEGER       :: j, ilas 

      INTERFACE
        FUNCTION calc_profil(x, type, ordre, xlarg, xdeb, xcentre)
          USE precisions
          REAL(KIND=kr), INTENT(IN)     :: x
          CHARACTER(LEN=5), INTENT(IN)  :: type
          INTEGER,       INTENT(IN)     :: ordre
          REAL(KIND=kr), INTENT(IN)     :: xlarg, xdeb, xcentre
          REAL(KIND=kr)                 :: calc_profil
        END FUNCTION calc_profil
      END INTERFACE



      fpsr = 0._8
! --- On fait rentrer les sources l'une apres l'autre
      DO ilas=1,3
        IF(inj_laser(ilas).NE.0) THEN
          IF(inj_laser(ilas).EQ.1) nulx_inj = nx
          IF(    (y_prof(ilas).EQ.'gauss')
     &       .OR.(y_prof(ilas).EQ.'hyper')
     &       .OR.(y_prof(ilas).EQ.'cos_2')) THEN
            bordy = y_max(ilas) - y_lrg(ilas)
          ELSEIF(y_prof(ilas).EQ.'plato') THEN
            bordy = y_max(ilas) - 0.5*y_lrg(ilas) - y_deb(ilas)
          ELSE 
            bordy = ymin_gl
          ENDIF


! ---- coefficients pour la m�thode de Huygens
          coefp1_by = dtsdx*a_0(ilas)*COS(y_ang(ilas))

          coefp2_bz = dtsdx*a_0(ilas)


! ---- 
!  Mise � jour du champ By, il faut prendre en compte bx incident et 
!  by incident
! ---------------------------
          DO j=1,ny
            posy = ymin_gl + j*pasdy
! --- + attention si conditions transverses periodiques
! ---   A FAIRE !!!!!! ??????
! --- + facteur spatial (y)
            coef_y =  calc_profil(posy,y_prof(ilas),y_ord(ilas), 
     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
     
! --- + facteur temporel et phase
            tretard = SIN(y_ang(ilas))*(posy-y_max(ilas))
            tps_eff = tps - tretard
            coef_t =  calc_profil(tps_eff,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_y   = COS(tps_eff/lambda(ilas))
 
! ----- polarisation 1     
            by_inc = coefp1_by*phase_y*y_pol(ilas)
     &                 *coef_t *coef_y    	    

            ez(nulx_inj,j) = ez(nulx_inj,j) + by_inc         
	    
    
            auxpy_sr = 1._8
            IF((j.EQ.nulmy).OR.(j.EQ.nulpy)) auxpy_sr = 0.5_8
            fpsr =  fpsr 
     &            + auxpy_sr*(a_0(ilas)*phase_y*y_pol(ilas)
     &                                 *coef_t*coef_y      )**2
          ENDDO


! ---- 
!  Mise � jour du champ Bz, 
!  il faut prendre en compte bz incident 
! ---------------------------
          DO j=1,ny
            posy = ymin_gl + (j-0.5_8)*pasdy
! --- + attention si conditions transverses periodiques
            IF( j.EQ.nulmy .AND. ibndc(3).EQ.-1 )
     &          posy = ymax_gl - 0.5_8*pasdy
            IF( j.EQ.nulpy+1 .AND. ibndc(4).EQ.-1 )
     &          posy = ymin_gl + 0.5_8*pasdy
! --- + facteur spatial (y)
            coef_y =  calc_profil(posy,y_prof(ilas),y_ord(ilas), 
     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
! --- + facteur temporel et phase
            tretard = SIN(y_ang(ilas))*(posy-y_max(ilas))
            tps_eff = tps - tretard
            coef_t =  calc_profil(tps_eff,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_z = COS(tps_eff/lambda(ilas))
  
! ----- polarisation 2        
            bz_inc = coefp2_bz*phase_z*z_pol(ilas)
     &                  *coef_t *coef_y  
    
	    ey(nulx_inj,j) = ey(nulx_inj,j) + bz_inc


            auxpy_sr = 1._8
            IF((j.EQ.nulmy).OR.(j.EQ.nulpy+1)) auxpy_sr = 0._8
            fpsr =  fpsr 
     &            + auxpy_sr*(a_0(ilas)*phase_z*z_pol(ilas)
     &                                 *coef_t*coef_y      )**2
          ENDDO
	  
        ENDIF
      ENDDO


!     ___________________________
      END SUBROUTINE maxlim_huygens_E_step2
      


      SUBROUTINE maxlim_huygens_B(tps)
! ======================================================================
! Application des conditions aux limites pour les 6 champs [EB][xyz]
! a l'instant reel temps
! Retourne le flux de Poynting entre (source)
! Le type de condition est code dans les variables ibndc(:)
!   ibndc >  0 : champs echanges avec un PE voisin
!   ibndc = -1 : conditions periodiques locales a ce PE
!   ibndc = -2 : absorption - condition de Silver-Muller
!   ibndc = -3 : conducteur parfait
! ======================================================================
      USE precisions
      USE domaines
      USE temps
      USE champs
      USE laser



      IMPLICIT NONE

      REAL(KIND=kr), INTENT(IN)     :: tps

      
!      REAL(KIND=kr) :: coefy_ez, coefy_bx0,coefy_bx1,coefy_by, coefy_sr,
!     &                 coefz_ey, coefz_bx0,coefz_bx1,coefz_bz 
     
!      REAL(KIND=kr) :: coefp1_by, coefp2_bz
      REAL(KIND=kr) :: coefp1_ez, coefp2_ey
!      REAL(KIND=kr) :: coefp2_ex, coefp1_bx
      REAL(KIND=kr) :: ez_inc, ey_inc
!      REAL(KIND=kr) :: by_inc, bz_inc, bx_inc
      
      REAL(KIND=kr) :: coef_t, coef_y, phase_y, phase_z
      REAL(KIND=kr) :: posy, auxpy_sr 
      REAL(KIND=kr) :: tps_eff, tretard, bordy
      INTEGER       :: j, ilas 

      INTERFACE
        FUNCTION calc_profil(x, type, ordre, xlarg, xdeb, xcentre)
          USE precisions
          REAL(KIND=kr), INTENT(IN)     :: x
          CHARACTER(LEN=5), INTENT(IN)  :: type
          INTEGER,       INTENT(IN)     :: ordre
          REAL(KIND=kr), INTENT(IN)     :: xlarg, xdeb, xcentre
          REAL(KIND=kr)                 :: calc_profil
        END FUNCTION calc_profil
      END INTERFACE



      fpsr = 0._8
! --- On fait rentrer les sources l'une apres l'autre
      DO ilas=1,3
        IF(inj_laser(ilas).NE.0) THEN
          IF(inj_laser(ilas).EQ.1) nulx_inj = nx
          IF(    (y_prof(ilas).EQ.'gauss')
     &       .OR.(y_prof(ilas).EQ.'hyper')
     &       .OR.(y_prof(ilas).EQ.'cos_2')) THEN
            bordy = y_max(ilas) - y_lrg(ilas)
          ELSEIF(y_prof(ilas).EQ.'plato') THEN
            bordy = y_max(ilas) - 0.5*y_lrg(ilas) - y_deb(ilas)
          ELSE 
            bordy = ymin_gl
          ENDIF


!          coefp1_by = -a_0(ilas)*COS(y_ang(ilas))
!     &                          *(1+COS(y_ang(ilas)))*dt
!          coefp2_bz = a_0(ilas)*(1+COS(y_ang(ilas)))*dt

! ---- coefficients pour la m�thode de Huygens
          coefp1_ez = -dtsdx*a_0(ilas)

!          coefp2_ex = -dtsdx*a_0(ilas)*SIN(y_ang(ilas))
          coefp2_ey =  dtsdx*a_0(ilas)*COS(y_ang(ilas))	

!          coefp1_bx = a_0(ilas)*SIN(y_ang(ilas))


! ---- 
!  Mise � jour du champ By, il faut prendre en compte bx incident et 
!  by incident
! ---------------------------
          DO j=1,ny
            posy = ymin_gl + j*pasdy
! --- + attention si conditions transverses periodiques
! ---   A FAIRE !!!!!! ??????
! --- + facteur spatial (y)
            coef_y =  calc_profil(posy,y_prof(ilas),y_ord(ilas), 
     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
     
! --- + facteur temporel et phase
            tretard = SIN(y_ang(ilas))*(posy-y_max(ilas))
            tps_eff = tps - tretard
            coef_t =  calc_profil(tps_eff,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_y   = COS(tps_eff/lambda(ilas))
 
! ----- polarisation 1     
            ez_inc = coefp1_ez*phase_y*y_pol(ilas)
     &                 *coef_t *coef_y    

!            by_inc = coefp1_by*phase_y*y_pol(ilas)
!     &                 *coef_t *coef_y    
	    

            by(nulx_inj,j) = by(nulx_inj,j) + ez_inc  
 
!            by(nulx_inj,j) = by(nulx_inj,j) + by_inc
 
    
            auxpy_sr = 1._8
            IF((j.EQ.nulmy).OR.(j.EQ.nulpy)) auxpy_sr = 0.5_8
            fpsr =  fpsr 
     &            + auxpy_sr*(a_0(ilas)*phase_y*y_pol(ilas)
     &                                 *coef_t*coef_y      )**2
          ENDDO


! ---- 
!  Mise � jour du champ Bz, 
!  il faut prendre en compte bz incident 
! ---------------------------
          DO j=1,ny
            posy = ymin_gl + (j-0.5_8)*pasdy
! --- + attention si conditions transverses periodiques
            IF( j.EQ.nulmy .AND. ibndc(3).EQ.-1 )
     &          posy = ymax_gl - 0.5_8*pasdy
            IF( j.EQ.nulpy+1 .AND. ibndc(4).EQ.-1 )
     &          posy = ymin_gl + 0.5_8*pasdy
! --- + facteur spatial (y)
            coef_y =  calc_profil(posy,y_prof(ilas),y_ord(ilas), 
     &                            y_lrg(ilas),y_deb(ilas),y_max(ilas))
! --- + facteur temporel et phase
            tretard = SIN(y_ang(ilas))*(posy-y_max(ilas))
            tps_eff = tps - tretard
            coef_t =  calc_profil(tps_eff,t_prof(ilas),t_ord(ilas),
     &                            t_lrg(ilas),t_deb(ilas),t_max(ilas))

            phase_z = COS(tps_eff/lambda(ilas))
  
! ----- polarisation 2        
            ey_inc = coefp2_ey*phase_z*z_pol(ilas)
     &                  *coef_t *coef_y
    
!            bz_inc = coefp2_bz*phase_z*z_pol(ilas)
!     &                  *coef_t *coef_y
    
	    bz(nulx_inj,j) = bz(nulx_inj,j) + ey_inc

!	    bz(nulx_inj,j) = bz(nulx_inj,j) + bz_inc	    

            auxpy_sr = 1._8
            IF((j.EQ.nulmy).OR.(j.EQ.nulpy+1)) auxpy_sr = 0._8
            fpsr =  fpsr 
     &            + auxpy_sr*(a_0(ilas)*phase_z*z_pol(ilas)
     &                                 *coef_t*coef_y      )**2
          ENDDO
	  
        ENDIF
      ENDDO


!     ___________________________
      END SUBROUTINE maxlim_huygens_B



      FUNCTION calc_profil(x, type, ordre, xlarg, xdeb, xcentre)
! ======================================================================
! Valeur (entre 0 et 1) en x de la fonction correspondant au profil
! "type" pour les parametres donnes ensuite
!
! - Exposant 0.25 en 2D car on est en 2D...
! - Exposant 0.25 en 3D car on passe dans ce calcul pour les deux
!   dimensions transverses, et c'est le produit qui compte...
! ======================================================================
      USE precisions
      IMPLICIT NONE
      REAL(KIND=kr), INTENT(IN)     :: x
      CHARACTER(LEN=5), INTENT(IN)  :: type
      INTEGER,       INTENT(IN)     :: ordre
      REAL(KIND=kr), INTENT(IN)     :: xlarg, xdeb, xcentre
      REAL(KIND=kr)                 :: calc_profil

      REAL(KIND=kr)                 :: x_sur_Zr_2

      INTERFACE
        FUNCTION sqrt_gauss(x, ordre, fwhm, xcentre)
          USE precisions
          REAL(KIND=kr), INTENT(IN) :: x, fwhm, xcentre
          INTEGER,       INTENT(IN) :: ordre
          REAL(KIND=kr)             :: sqrt_gauss
        END FUNCTION sqrt_gauss
        FUNCTION sqrt_skew(x,skew,fwhm,xcentre)
          USE precisions
          REAL(KIND=kr), INTENT(IN) :: x, fwhm, xcentre, skew
          REAL(KIND=kr)             :: sqrt_skew
        END FUNCTION sqrt_skew  
        FUNCTION plateau(x, xramp, xplat, xcentre)
          USE precisions
          REAL(KIND=kr), INTENT(IN) :: x, xramp, xplat, xcentre
          REAL(KIND=kr)             :: plateau
        END FUNCTION plateau
        FUNCTION sqrt_cos_car(x, xlarg, xcentre)
          USE precisions
          REAL(KIND=kr), INTENT(IN) :: x, xlarg, xcentre
          REAL(KIND=kr)             :: sqrt_cos_car
        END FUNCTION sqrt_cos_car
      END INTERFACE


      SELECT CASE(type)
      CASE('plato')
        calc_profil = plateau(x, xdeb, xlarg, xcentre)
      CASE('cos_2')
        calc_profil = sqrt_cos_car(x, xlarg, xcentre)
      CASE('gauss')
        calc_profil = sqrt_gauss(x,     2, xlarg, xcentre)
      CASE('skewd')
        calc_profil = sqrt_skew(x, xdeb, xlarg, xcentre)
      CASE('hyper')
        calc_profil = sqrt_gauss(x, ordre, xlarg, xcentre)
      CASE('foc2d')
        x_sur_Zr_2 = 16._8*LOG(2._8)*LOG(2._8)*xdeb*xdeb
     &              /(xlarg**4)
        calc_profil = EXP(-2.*LOG(2._8)*((x-xcentre)/xlarg)**2
     &                       /(1._8+x_sur_Zr_2)               )
     .               /(1._8+x_sur_Zr_2)**0.25_8
      CASE('foc3d')
        x_sur_Zr_2 = 16._8*LOG(2._8)*LOG(2._8)*xdeb*xdeb
     &              /(xlarg**4)
        calc_profil = EXP(-2.*LOG(2._8)*((x-xcentre)/xlarg)**2
     &                       /(1._8+x_sur_Zr_2)               )
     .               /(1._8+x_sur_Zr_2)**0.25_8
      CASE DEFAULT
        calc_profil = 1._8
      END SELECT
      RETURN
!     ________________________
      END FUNCTION calc_profil


      FUNCTION calc_profil_y(x, ilas)
! ======================================================================
! Valeur (entre 0 et 1) en x de la fonction correspondant au profil
! "type" pour les parametres donnes ensuite
!
! - Exposant 0.25 en 2D car on est en 2D...
! - Exposant 0.25 en 3D car on passe dans ce calcul pour les deux
!   dimensions transverses, et c'est le produit qui compte...
! ======================================================================
      USE precisions
      USE laser
      
      IMPLICIT NONE
      
      REAL(KIND=kr), INTENT(IN)     :: x
      INTEGER, INTENT(IN)           :: ilas
      
      CHARACTER(LEN=5)              :: type_f
      INTEGER                       :: ordre
      REAL(KIND=kr)                 :: xlarg, xdeb, xcentre
      
      REAL(KIND=kr)                 :: a1h, a2h
      REAL(KIND=kr)                 :: x_sur_Zr_2
      
      REAL(KIND=kr)                 :: calc_profil_y


      INTERFACE
        FUNCTION sqrt_gauss(x, ordre, fwhm, xcentre)
          USE precisions
          REAL(KIND=kr), INTENT(IN) :: x, fwhm, xcentre
          INTEGER,       INTENT(IN) :: ordre
          REAL(KIND=kr)             :: sqrt_gauss
        END FUNCTION sqrt_gauss
        FUNCTION sqrt_hermite(x, ordre, fwhm, xcentre, 
     &                        a1_her_y, a2_her_y, a0)
          USE precisions
          REAL(KIND=kr), INTENT(IN) :: x, fwhm, xcentre
          REAL(KIND=kr), INTENT(IN) :: a1_her_y, a2_her_y, a0
          INTEGER,       INTENT(IN) :: ordre
          REAL(KIND=kr)             :: sqrt_hermite
        END FUNCTION sqrt_hermite
        FUNCTION sqrt_skew(x,skew,fwhm,xcentre)
          USE precisions
          REAL(KIND=kr), INTENT(IN) :: x, fwhm, xcentre, skew
          REAL(KIND=kr)             :: sqrt_skew
        END FUNCTION sqrt_skew  
        FUNCTION plateau(x, xramp, xplat, xcentre)
          USE precisions
          REAL(KIND=kr), INTENT(IN) :: x, xramp, xplat, xcentre
          REAL(KIND=kr)             :: plateau
        END FUNCTION plateau
        FUNCTION sqrt_cos_car(x, xlarg, xcentre)
          USE precisions
          REAL(KIND=kr), INTENT(IN) :: x, xlarg, xcentre
          REAL(KIND=kr)             :: sqrt_cos_car
        END FUNCTION sqrt_cos_car
      END INTERFACE


      type_f  =  y_prof(ilas)
      ordre   =  y_ord(ilas)
      xlarg   =  y_lrg(ilas)
      xdeb    =  y_deb(ilas)
      xcentre =  y_max(ilas)
      

      SELECT CASE(type_f)
      CASE('plato')
        calc_profil_y = plateau(x, xdeb, xlarg, xcentre)
      CASE('cos_2')
        calc_profil_y = sqrt_cos_car(x, xlarg, xcentre)
      CASE('gauss')
        calc_profil_y = sqrt_gauss(x, 2, xlarg, xcentre)
      CASE('hermi')
        calc_profil_y = sqrt_hermite(x, 2, xlarg, xcentre, 
     &                   a1_her_y(ilas), a2_her_y(ilas), 
     &                   a_0(ilas))
      CASE('skewd')
        calc_profil_y = sqrt_skew(x, xdeb, xlarg, xcentre)
      CASE('hyper')
        calc_profil_y = sqrt_gauss(x, ordre, xlarg, xcentre)
      CASE('foc2d')
        x_sur_Zr_2 = 16._8*LOG(2._8)*LOG(2._8)*xdeb*xdeb
     &              /(xlarg**4)
        calc_profil_y = EXP(-2.*LOG(2._8)*((x-xcentre)/xlarg)**2
     &                       /(1._8+x_sur_Zr_2)               )
     .               /(1._8+x_sur_Zr_2)**0.25_8
      CASE('foc3d')
        x_sur_Zr_2 = 16._8*LOG(2._8)*LOG(2._8)*xdeb*xdeb
     &              /(xlarg**4)
        calc_profil_y = EXP(-2.*LOG(2._8)*((x-xcentre)/xlarg)**2
     &                       /(1._8+x_sur_Zr_2)               )
     .               /(1._8+x_sur_Zr_2)**0.25_8
      CASE DEFAULT
        calc_profil_y = 1._8
      END SELECT
      RETURN
!     ________________________
      END FUNCTION calc_profil_y


      FUNCTION sqrt_gauss(x, ordre, fwhm, xcentre)
! ======================================================================
! Valeur (entre 0 et 1) en x de la fonction qui, ELEVEE AU CARRE, 
! donne une Gaussienne d'ordre ordre, de largeur a mi-hauteur fwhm 
! et centree en xcentre
! ======================================================================
      USE precisions
      IMPLICIT NONE
      REAL(KIND=kr), INTENT(IN) :: x, fwhm, xcentre
      INTEGER,       INTENT(IN) :: ordre
      REAL(KIND=kr)             :: sqrt_gauss

      sqrt_gauss = EXP( -0.5*LOG(2._8)*(2._8*(x-xcentre)/fwhm)**ordre )
      RETURN
!     _______________________
      END FUNCTION sqrt_gauss


      FUNCTION sqrt_hermite(x, ordre, fwhm, xcentre, 
     &                      a1_her_y, a2_her_y, a0)
! ======================================================================
! Valeur (entre 0 et 1) en x de la fonction qui, ELEVEE AU CARRE, 
! donne une Gaussienne d'ordre ordre, de largeur a mi-hauteur fwhm 
! et centree en xcentre
! ======================================================================
      USE precisions
      IMPLICIT NONE
      REAL(KIND=kr), INTENT(IN) :: x, fwhm, xcentre
      REAL(KIND=kr), INTENT(IN) :: a1_her_y, a2_her_y, a0
      INTEGER,       INTENT(IN) :: ordre
      REAL(KIND=kr)             :: h0, h2, xnorm
      REAL(KIND=kr)             :: sqrt_hermite

c      xnorm = 2._8*(x-xcentre)/fwhm
      xnorm = (x-xcentre)/fwhm

c      sqrt_hermite = EXP( -0.5*LOG(2._8)* xnorm**ordre )
      sqrt_hermite = EXP( -2.*log(2.)*xnorm**ordre )
      
      h0 = 1.*a1_her_y
      h2 = (4.*(xnorm/sqrt(2.))**2 -2.)*a2_her_y
      
      sqrt_hermite = sqrt_hermite*(h0 + h2)/a0
      
      
      RETURN
!     _______________________
      END FUNCTION sqrt_hermite 


      FUNCTION sqrt_skew(x, skew, fwhm, xcentre)
! ======================================================================
! Valeur (entre 0 et 1) en x de la fonction qui, ELEVEE AU CARRE, 
! donne une Gaussienne d'ordre 2, de largeur a mi-hauteur fwhm 
! et centree en xcentre, de skew skew
! ======================================================================
      USE precisions
      IMPLICIT NONE
      REAL(KIND=kr), INTENT(IN) :: x, fwhm, xcentre, skew
      REAL(KIND=kr)             :: twhm, hwhm
      REAL(KIND=kr)             :: sqrt_skew

      hwhm = fwhm/(skew+2)
      twhm = fwhm - fwhm/(skew+2)
      IF(x.LE.xcentre) THEN
        sqrt_skew = EXP( -0.5*LOG(2._8)*((x-xcentre)/hwhm)**2 )
      ELSE
        sqrt_skew = EXP( -0.5*LOG(2._8)*((x-xcentre)/twhm)**2 )
      ENDIF
      RETURN
!     _______________________
      END FUNCTION sqrt_skew


      FUNCTION plateau(x, xramp, xplat, xcentre)
! ======================================================================
! Valeur (entre 0 et 1) en x de la fonction plateau centree en xcentre,
! constante a 1 pendant xplat et encadree de rampes lineaires de
! longueurs xramp 
! ======================================================================
      USE precisions
      IMPLICIT NONE
      REAL(KIND=kr), INTENT(IN) :: x, xramp, xplat, xcentre
      REAL(KIND=kr)             :: plateau
      REAL(KIND=kr)             :: aux, aux_plat

      aux = ABS(x-xcentre)
      aux_plat = 0.5*xplat
      IF(aux.LE.aux_plat) THEN
        plateau = 1.
      ELSEIF(aux.LE.(aux_plat+xramp)) THEN
        plateau = 1.-(aux-aux_plat)/xramp
      ELSE
        plateau = 0.
      ENDIF
      RETURN
!     ____________________
      END FUNCTION plateau


      FUNCTION sqrt_cos_car(x, xlarg, xcentre)
! ======================================================================
! Valeur (entre 0 et 1) en x de la fonction qui, ELEVEE AU CARRE, 
! donne un cosinus-carre de largeur totale xlarg et centre en xcentre
! ======================================================================
      USE precisions
      IMPLICIT NONE
      REAL(KIND=kr), INTENT(IN) :: x, xlarg, xcentre
      REAL(KIND=kr)             :: sqrt_cos_car
      REAL(KIND=8), PARAMETER   :: pi = 3.1415926536
      REAL(KIND=kr)             :: aux

      aux = ABS((x-xcentre)/xlarg)
      IF(aux.LE.0.5) THEN
        sqrt_cos_car = COS( pi*aux )
      ELSE
        sqrt_cos_car = 0.
      ENDIF
      RETURN
!     _________________________
      END FUNCTION sqrt_cos_car
