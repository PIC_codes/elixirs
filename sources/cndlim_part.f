! ======================================================================
! cndlim_p_sun.f
! --------------
!
!     SUBROUTINE cndlim_part
!     SUBROUTINE bord_sortie
!
!     SUBROUTINE cndl_period
!     SUBROUTINE cndl_reflex
!     SUBROUTINE cndl_reinjs
!     FUNCTION localise_rel
!     SUBROUTINE tasse_tableau
!
! ======================================================================





      SUBROUTINE cndlim_part(iesp)
! ======================================================================
! Applications des conditions de destruction et des conditions aux  
! limites pour les particules de l'espece iesp
! On parametre la taille des buffers avec 
!    + sizeabs : nombre maxi. de particules qui peuvent quitter le PE
!    + sizebuf : nombre maxi. de particules que l'on peut echanger avec
!                chaque voisin
! Les differentes etapes :
!    + Localisation des particules
!    + Traitement des particules a detruire (ionisees)
!    + Traitement des conditions aux limites
!    + Aiguillage des particules
!    + Tassement du tableau
!    | Envoi / Reception ou Reception / Envoi
!    | des messages, en x, y et z selon besoin
! 
! ======================================================================
      USE precisions
      USE domaines
      USE domaines_klder
      USE particules_klder
      USE diagnostics_klder
      USE erreurs

      IMPLICIT NONE
!!      INCLUDE 'mpif.h'

      INTEGER, INTENT(IN)         :: iesp 

      INTEGER                            :: nbp, ip, nb_perdu
      INTEGER                            :: dyn, p_proc, p_bord
      INTEGER                            :: p_poids_bord      
      INTEGER                            :: test=0
      INTEGER, DIMENSION(:), ALLOCATABLE :: quel_PE, pa_perdu
      REAL(KIND=kr)          :: vtx, vty, vtz
      REAL(KIND=kr)          :: p_qx, p_qy, p_qz, p_px, p_py, p_pz
      REAL(KIND=kr)          :: p_qx_prec, p_qy_prec, p_qz_prec

      REAL(KIND=kr)          :: p_vx, p_vy, p_vz
      REAL(KIND=kr)          :: p_ga, p_ga0
      REAL(KIND=kr)          :: xsorti, ysorti, zsorti, tsorti
      INTEGER                :: isorti, jsorti, ksorti

      INTEGER                        :: nb_depart 

      LOGICAL, SAVE                           :: premier = .TRUE.
      INTEGER,       DIMENSION(:),   ALLOCATABLE, SAVE :: b_depd
      REAL(KIND=kr), DIMENSION(:,:), ALLOCATABLE, SAVE :: b_depart

      INTEGER(KIND=8)                         :: dummy
      REAL(KIND=kr), DIMENSION(1)             :: moisson
      REAL(KIND=kr), DIMENSION(6)             :: def_prob, def_elec_tot

      INTERFACE
        FUNCTION localise_rel(p_qx, p_qy, p_qz, p_proc)
          USE precisions
          INTEGER                   :: localise_rel
          REAL(KIND=kr), INTENT(IN) :: p_qx, p_qy, p_qz
          INTEGER, INTENT(IN)       :: p_proc
        END FUNCTION localise_rel
        SUBROUTINE tasse_tableau(nb_perdu, nbp, pa_perdu, a_tasser)
          USE precisions
          INTEGER, INTENT(IN)                          :: nb_perdu
          INTEGER, INTENT(INOUT)                       :: nbp
          INTEGER,      DIMENSION(:), INTENT(IN)       :: pa_perdu
          REAL(KIND=kr), DIMENSION(:,:), INTENT(INOUT) :: a_tasser
        END SUBROUTINE tasse_tableau
        SUBROUTINE aleat_unif(gerbe,methode,graine)
          USE precisions
          REAL(KIND=kr), DIMENSION(:), INTENT(OUT)   :: gerbe
          INTEGER                    , INTENT(IN)    :: methode
          INTEGER(KIND=8)            , INTENT(INOUT) :: graine
        END SUBROUTINE aleat_unif
      END INTERFACE


      nbp = nbpa(iesp)
      IF(debug.GE.5) WRITE(fdbg,*) 'avant cndlim_p, ',nbpa(iesp),
     &                             ' partic. sur PE ',numproc

! ----- mise a jour de partic(a_ga,ip,iesp)
!      partic(a_ga,1:nbpa(iesp),iesp) = 
!     &                    partic(a_gatld12,1:nbpa(iesp),iesp)

      dyn = dynam(iesp)
      vtx = vthx(iesp)
      vty = vthy(iesp)
      vtz = vthz(iesp)
!      d1d_loc(d_pof(iesp)) = 0.
!      d1d_loc(d_kif(iesp)) = 0.
!      d1d_loc(d_kir(iesp)) = 0.
      nb_perdu = 0
      nb_depart = 0
      dummy = 0

! --- Si conditions mixtes sur un des bords (-5) et esp=electrons,
!     calculer la probabilite avec laquelle il faut reinjecter pour
!     minimiser la non-neutralite globale
      IF((ANY(ibndp(:,:).EQ.-5)).AND.(iesp.EQ.nesp_p)) THEN
        IF(numproc.EQ.1) def_charg(:) = def_charg(:)+def_charg_tot(:)
        IF(nproc.GT.1) THEN
!          CALL MPI_ALLREDUCE(def_charg(1), def_charg_tot(1),
!     &                       6, MPI_CALDER_REAL, MPI_SUM,
!     &                       MPI_COMM_WORLD, info)
!          CALL MPI_ALLREDUCE(def_elec(1), def_elec_tot(1),
!     &                       6, MPI_CALDER_REAL, MPI_SUM,
!     &                       MPI_COMM_WORLD, info)
        WRITE(fdbg,"('Erreur algorithmique dans cndlim_part : '
     &       'Impasse sur les appels MPI_ALLREDUCE !')")

        ELSE
          def_charg_tot(:) = def_charg(:)
          def_elec_tot(:)  = def_elec(:)
        ENDIF
        def_charg(:) = 0.
        def_elec(:)  = 0.
        WHERE(def_elec_tot(:).NE.0.)
          def_prob(:) = 1. - def_charg_tot(:)/def_elec_tot(:)
        ELSEWHERE
          def_prob(:) = 1.
        ENDWHERE

!
! on suppose que la reinjection est inhibee par la 
! collisionnalite du courant de retour...
!        def_prob(:) = 0.2 * def_prob(:)
!
!        print*, numproc,' : def_charg_tot = ',def_charg_tot(:)
!        print*, numproc,' : def_elec_tot  = ',def_elec_tot(:)
!        print*, numproc,' : proba.s de reinjection = ',def_prob(:)
      ENDIF

      IF(premier) THEN
        ALLOCATE(b_depart(SIZE(partic,1),2*sizebuf), b_depd(2*sizebuf))
        premier = .FALSE.
      ENDIF

      ALLOCATE(pa_perdu(sizeabs))

      IF(nbp.GT.0) THEN
! --- Localisation simultanee de toutes les particules -----------------
!     (seuls les bords inf. appartiennent au domaine : LT mais GE)
        ALLOCATE(quel_PE(nbp))
        quel_PE(:) = 0
        IF(a_qx.NE.0.) THEN
          WHERE(partic(a_qx, 1:nbp, iesp).LT.bord(1,numproc))
     &         quel_PE = quel_PE+ 1
          WHERE(partic(a_qx, 1:nbp, iesp).GE.bord(2,numproc))
     &         quel_PE = quel_PE+ 2
        ENDIF
        IF(a_qy.NE.0.) THEN
          WHERE(partic(a_qy, 1:nbp, iesp).LT.bord(3,numproc))
     &         quel_PE = quel_PE+ 3
          WHERE(partic(a_qy, 1:nbp, iesp).GE.bord(4,numproc))
     &         quel_PE = quel_PE+ 6
        ENDIF
        IF(a_qz.NE.0.) THEN
          WHERE(partic(a_qz, 1:nbp, iesp).LT.bord(5,numproc))
     &         quel_PE = quel_PE+ 9
          WHERE(partic(a_qz, 1:nbp, iesp).GE.bord(6,numproc))
     &         quel_PE = quel_PE+18
        ENDIF

! --- Traitement des particules detruites ou sorties -------------------
        DO ip=1,nbp
! --- + Traitement des particules detruites (ionisees) -----------------
          IF(partic(a_po,ip,iesp).EQ.0.) THEN
            nb_perdu = nb_perdu + 1
            pa_perdu(nb_perdu) = ip
            quel_PE(ip) = 0
          ENDIF
! --- + Traitement des particules sorties ------------------------------
          IF(quel_PE(ip).EQ.0) CYCLE
          p_proc = numproc
          p_qx = 0. ; IF (a_qx.NE.0) p_qx = partic(a_qx,ip,iesp)
          p_qy = 0. ; IF (a_qy.NE.0) p_qy = partic(a_qy,ip,iesp)
          p_qz = 0. ; IF (a_qz.NE.0) p_qz = partic(a_qz,ip,iesp)
          p_qx_prec = 0. ; IF (a_qxm1.NE.0) 
     &                p_qx_prec = partic(a_qxm1,ip,iesp)
          p_qy_prec = 0. ; IF (a_qym1.NE.0) 
     &                p_qy_prec = partic(a_qym1,ip,iesp)
          p_qz_prec = 0. ; IF (a_qzm1.NE.0) 
     &                p_qz_prec = partic(a_qzm1,ip,iesp)
          p_px = 0. ; IF (a_px.NE.0) p_px = partic(a_px,ip,iesp)
          p_py = 0. ; IF (a_py.NE.0) p_py = partic(a_py,ip,iesp)
          p_pz = 0. ; IF (a_pz.NE.0) p_pz = partic(a_pz,ip,iesp)

          
          DO WHILE ((quel_PE(ip).NE.0).AND.(test.NE.-1))
            CALL bord_sortie(p_qx,p_qy,p_qz, p_px,p_py,p_pz,
     &                       p_proc, quel_PE(ip), p_bord)

            IF(debug.GE.7) WRITE(fdbg,*) '-- Part. ',ip,p_proc,
     &                     quel_PE(ip),p_qx,p_qy,p_qz,' p_bord=',p_bord

            IF(ibndp(p_bord,p_proc).GT.0) THEN
! ------------------------------------
! --- La particule passe a un autre PE
! ------------------------------------
              p_proc = ibndp(p_bord,p_proc)
! --- ++ Calcul de la localisation de la particule par rapport a p_proc
              quel_PE(ip) = localise_rel(p_qx, p_qy, p_qz, p_proc)

            ELSEIF(ibndp(p_bord,p_proc).EQ.-1) THEN
! --------------------------
! --- Conditions periodiques
! --------------------------
              CALL cndl_period(p_bord, p_qx, p_qy, p_qz, p_proc)	      
     
! --- ++ Calcul de la localisation de la particule par rapport a p_proc
              quel_PE(ip) = localise_rel(p_qx, p_qy, p_qz, p_proc)

            ELSEIF(ibndp(p_bord,p_proc).EQ.-2) THEN
! --------------------------
! --- Conditions absorbantes
! --------------------------
              IF(dyn.EQ.2) THEN
                d1d_loc(d_kif(iesp)) = d1d_loc(d_kif(iesp)) + 
     &             (partic(a_ga,ip,iesp)-1._8)*partic(a_po,ip,iesp)
              ELSE
                d1d_loc(d_kif(iesp)) = d1d_loc(d_kif(iesp)) + 
     &              partic(a_ga,ip,iesp)      *partic(a_po,ip,iesp)
              ENDIF
              d1d_loc(d_pof(iesp)) =  d1d_loc(d_pof(iesp))
     &                              + partic(a_po,ip,iesp)
              p_proc = 0
              quel_PE(ip) = 0

            ELSEIF(ibndp(p_bord,p_proc).EQ.-3) THEN
! ------------------------------
! --- Conditions reflechissantes
! ------------------------------
              CALL cndl_reflex(p_bord, p_qx,p_qy,p_qz, p_px,p_py,p_pz)

! --- ++ Calcul de la localisation de la particule par rapport a p_proc
              quel_PE(ip) = localise_rel(p_qx, p_qy, p_qz, p_proc)

            ELSEIF(ibndp(p_bord,p_proc).EQ.-4) THEN
! -------------------------------------------------
! --- Conditions reinjectantes (au point de sortie)
! -------------------------------------------------
              p_ga = partic(a_ga,ip,iesp) ; p_ga0 = p_ga
              IF( esir_ok.EQ.1 ) THEN
                CALL cndl_reinjs(p_bord, p_qx,p_qy,p_qz, 
     &                  p_px,p_py,p_pz, p_ga, dyn, vtx,vty,vtz)	
              ELSE
                CALL cndl_reinjs2(p_bord, p_qx,p_qy,p_qz, 
     &                  p_px,p_py,p_pz, p_ga, dyn, vtx,vty,vtz )      
	      ENDIF
     
              partic(a_ga,ip,iesp) = p_ga
              IF(a_wx.NE.0) partic(a_wx,ip,iesp) = 0.
              IF(a_wy.NE.0) partic(a_wy,ip,iesp) = 0.
              d1d_loc(d_kir(iesp)) = d1d_loc(d_kir(iesp))
     &                              +(p_ga0 - p_ga)*partic(a_po,ip,iesp)
              p_proc = p_proc
! --- ++ Calcul de la localisation de la particule par rapport a p_proc
              quel_PE(ip) = localise_rel(p_qx, p_qy, p_qz, p_proc)

            ELSEIF(ibndp(p_bord,p_proc).EQ.-5) THEN
! --------------------------------------------------------
! --- Conditions absorbantes ion / reinjectantes electrons
! --------------------------------------------------------
              IF(iesp.LT.nesp_p) THEN
! --- ++ Absorption de la particule
                IF(dyn.EQ.2) THEN
                  d1d_loc(d_kif(iesp)) = d1d_loc(d_kif(iesp)) + 
     &               (partic(a_ga,ip,iesp)-1._8)*partic(a_po,ip,iesp)
                ELSE
                  d1d_loc(d_kif(iesp)) = d1d_loc(d_kif(iesp)) + 
     &                partic(a_ga,ip,iesp)      *partic(a_po,ip,iesp)
                ENDIF
                d1d_loc(d_pof(iesp)) =  d1d_loc(d_pof(iesp))
     &                                + partic(a_po,ip,iesp)
                p_proc = 0
                quel_PE(ip) = 0
                def_charg(p_bord) =  def_charg(p_bord)
     &                             + charge(iesp)*partic(a_po,ip,iesp)
              ELSE
! --- ++ Aborption ou reinjection de la particule
                def_elec(p_bord) =  def_elec(p_bord)
     &                            + partic(a_po,ip,iesp)
                CALL aleat_unif(moisson,0,dummy)
                IF(moisson(1).GT.def_prob(p_bord)) THEN
! --- ++++ Absorption
                IF(dyn.EQ.2) THEN
                  d1d_loc(d_kif(iesp)) = d1d_loc(d_kif(iesp)) + 
     &               (partic(a_ga,ip,iesp)-1._8)*partic(a_po,ip,iesp)
                ELSE
                  d1d_loc(d_kif(iesp)) = d1d_loc(d_kif(iesp)) + 
     &                partic(a_ga,ip,iesp)      *partic(a_po,ip,iesp)
                ENDIF
                d1d_loc(d_pof(iesp)) =  d1d_loc(d_pof(iesp))
     &                                + partic(a_po,ip,iesp)
                p_proc = 0
                quel_PE(ip) = 0
                def_charg(p_bord) =  def_charg(p_bord)
     &                             + charge(iesp)*partic(a_po,ip,iesp)
                ELSE
! --- ++++ Reinjection
                p_ga = partic(a_ga,ip,iesp) ; p_ga0 = p_ga
                CALL cndl_reinjs2(p_bord, p_qx,p_qy,p_qz, 
     &                 p_px,p_py,p_pz, p_ga, dyn, vtx,vty,vtz )
                partic(a_ga,ip,iesp) = p_ga
                IF(a_wx.NE.0) partic(a_wx,ip,iesp) = 0.
                IF(a_wy.NE.0) partic(a_wy,ip,iesp) = 0.
                d1d_loc(d_kir(iesp)) = d1d_loc(d_kir(iesp))
     &                              +(p_ga0 - p_ga)*partic(a_po,ip,iesp)
                p_proc = p_proc
! --- ++ Calcul de la localisation de la particule par rapport a p_proc
                quel_PE(ip) = localise_rel(p_qx, p_qy, p_qz, p_proc)
                ENDIF
              ENDIF

            ELSEIF(ibndp(p_bord,p_proc).EQ.-6) THEN
! ---------------------------------------------------------------------------------
! --- Conditions mixtes :
!     - reflechissantes si sortie dans le vide
!     - reinjectantes si sortie dans un plasma
! Ajout : L.G. 02/2007
! ---------------------------------------------------------------------------------
              p_ga = partic(a_ga,ip,iesp)
	      IF(dyn.EQ.2) THEN
		p_vx=p_px/p_ga;p_vy=p_py/p_ga;p_vz=p_pz/p_ga
              ELSE
                p_vx=p_px;p_vy=p_py;p_vz=p_pz
	      ENDIF
	      
	      SELECT CASE(p_bord)
	      CASE(1)
	        IF(p_vx.LT.0.) THEN;tsorti=(p_qx-bord(1,1))/p_vx
                ELSE ; tsorti = 0. ; ENDIF
                xsorti = bord(1,1);
	        ysorti = p_qy-tsorti*p_vy;jsorti=floor(ysorti/pasdy)+1
                zsorti = p_qz-tsorti*p_vz;ksorti=floor(zsorti/pasdz)+1
		SELECT CASE(dim_cas)
		CASE(6)
                  p_poids_bord = poids3_bordmx(jsorti,ksorti)
		CASE(5,4)
		  p_poids_bord = poids2_bordmx(jsorti)
		CASE(3,2,1)
		  p_poids_bord = poids1_bordmx;	        
		END SELECT
		  
	      CASE(2)
	        IF(p_vx.GT.0.) THEN;tsorti=(p_qx-bord(2,nproc))/p_vx
                ELSE ; tsorti = 0. ; ENDIF
                xsorti = bord(2,nproc);
	        ysorti = p_qy-tsorti*p_vy;jsorti=floor(ysorti/pasdy)+1
                zsorti = p_qz-tsorti*p_vz;ksorti=floor(zsorti/pasdz)+1
		SELECT CASE(dim_cas)
		CASE(6)
                  p_poids_bord = poids3_bordpx(jsorti,ksorti)
		CASE(5,4)
		  p_poids_bord = poids2_bordpx(jsorti)
		CASE(3,2,1)
		  p_poids_bord = poids1_bordpx;	        
		END SELECT
		  
	      CASE(3)
	        IF(p_vy.LT.0.) THEN;tsorti=(p_qy-bord(3,1))/p_vy
                ELSE ; tsorti = 0. ; ENDIF
                ysorti = bord(3,1);
	        xsorti = p_qx-tsorti*p_vx;isorti=floor(xsorti/pasdx)+1
                zsorti = p_qz-tsorti*p_vz;ksorti=floor(zsorti/pasdz)+1
		SELECT CASE(dim_cas)
		CASE(6)
                  p_poids_bord = poids3_bordmy(isorti,ksorti)
		CASE(5,4)
		  p_poids_bord = poids2_bordmy(isorti)
		END SELECT
		
	      CASE(4)
	        IF(p_vy.GT.0.) THEN;tsorti=(p_qy-bord(4,nproc))/p_vy
                ELSE ; tsorti = 0. ; ENDIF
                ysorti = bord(4,nproc);
	        xsorti = p_qx-tsorti*p_vx;isorti=floor(xsorti/pasdx)+1
                zsorti = p_qz-tsorti*p_vz;ksorti=floor(zsorti/pasdz)+1
		SELECT CASE(dim_cas)
		CASE(6)
                  p_poids_bord = poids3_bordpy(isorti,ksorti)
		CASE(5,4)
		  p_poids_bord = poids2_bordpy(isorti)
		END SELECT		
				  
	      CASE(5)
	        IF(p_vz.LT.0.) THEN;tsorti=(p_qz-bord(5,1))/p_vz
                ELSE ; tsorti = 0. ; ENDIF
                zsorti = bord(5,1);
	        xsorti = p_qx-tsorti*p_vx ;isorti=floor(xsorti/pasdx)+1
                ysorti = p_qy-tsorti*p_vy ;jsorti=floor(ysorti/pasdy)+1
		IF(dim_cas.EQ.6) p_poids_bord=poids3_bordmz(isorti,jsorti)
		
	      CASE(6)
	        IF(p_vz.GT.0.) THEN;tsorti=(p_qz-bord(6,nproc))/p_vz
                ELSE ; tsorti = 0. ; ENDIF
                zsorti = bord(6,nproc);
	        xsorti = p_qx-tsorti*p_vx;isorti=floor(xsorti/pasdx)+1
                ysorti = p_qy-tsorti*p_vy;jsorti=floor(ysorti/pasdy)+1
		IF(dim_cas.EQ.6) p_poids_bord=poids3_bordpz(isorti,jsorti)		
				
	      END SELECT
              
	      IF(p_poids_bord.EQ.0.) THEN
! -------------------------------
! --- Conditions reflechissantes
! -------------------------------
                CALL cndl_reflex(p_bord, p_qx,p_qy,p_qz, p_px,p_py,p_pz)
              ELSE
!---------------------------------
! --- Conditions reinjectantes 
! -------------------------------
                p_ga0 = p_ga
                CALL cndl_reinjs2(p_bord,p_qx,p_qy,p_qz,
     &               p_px,p_py,p_pz,p_ga,
     &               dyn,vtx,vty,vtz)
                partic(a_ga,ip,iesp) = p_ga
                IF(a_wx.NE.0) partic(a_wx,ip,iesp) = 0.
                IF(a_wy.NE.0) partic(a_wy,ip,iesp) = 0.
                d1d_loc(d_kir(iesp)) = d1d_loc(d_kir(iesp))
     &               +(p_ga0 - p_ga)*partic(a_po,ip,iesp) 
	      ENDIF
	      p_proc = p_proc
! --- ++ Calcul de la localisation de la particule par rapport a p_proc
	      quel_PE(ip) = localise_rel(p_qx, p_qy, p_qz, p_proc)

            ENDIF

            IF(debug.GE.7) WRITE(fdbg,*) '++ Part. ',ip,p_proc,
     &                     quel_PE(ip),p_qx,p_qy,p_qz,' p_bord=',p_bord
     
! ------- Fin de traitement de la particule ip      
          ENDDO

          IF (a_qx.NE.0) partic(a_qx,ip,iesp) = p_qx
          IF (a_qy.NE.0) partic(a_qy,ip,iesp) = p_qy
          IF (a_qz.NE.0) partic(a_qz,ip,iesp) = p_qz
          IF (a_qxm1.NE.0) partic(a_qxm1,ip,iesp) = p_qx_prec
          IF (a_qym1.NE.0) partic(a_qym1,ip,iesp) = p_qy_prec
          IF (a_qzm1.NE.0) partic(a_qzm1,ip,iesp) = p_qz_prec
          IF (a_px.NE.0) partic(a_px,ip,iesp) = p_px
          IF (a_py.NE.0) partic(a_py,ip,iesp) = p_py
          IF (a_pz.NE.0) partic(a_pz,ip,iesp) = p_pz
          IF(p_proc.NE.numproc) THEN
! --- La particule est transmise ou absorbee
            nb_perdu = nb_perdu + 1
            pa_perdu(nb_perdu) = ip
            IF(p_proc.NE.0) THEN
! --- La particule est transmise
              nb_depart = nb_depart + 1
              b_depart(:,nb_depart) = partic(:,ip,iesp)
              b_depd(nb_depart) = p_proc
            ENDIF
          ENDIF

! ----- Fin de traitement des particules detruites ou sorties -
        ENDDO

        DEALLOCATE(quel_PE)
      ENDIF

! ----- mise a jour de partic(a_gatld12,ip,iesp)
!      partic(a_gatld12,1:nbpa(iesp),iesp) = 
!     &                    partic(a_ga,1:nbpa(iesp),iesp)


      CALL tasse_tableau(nb_perdu,nbpa(iesp), pa_perdu,partic(:,:,iesp))
      IF(debug.GE.5) WRITE(fdbg,*) 'Tableau tasse'
      DEALLOCATE(pa_perdu)


      IF(nb_depart.GE.1) THEN
        print*, 'Il reste des particules a envoyer sur ',numproc
        WRITE(fdbg,"('+----------------------------------------+')")
        WRITE(fdbg,"('| Il reste ',I5,' part. a envoyer sur ',
     &               I3,' |')") nb_depart,numproc
        WRITE(fdbg,"('+----------------------------------------+')")
      ENDIF

      IF(debug.GE.5) THEN
       WRITE(fdbg,"('Apres cndlim_p, ',I6,' partic. sur PE ',I3)")
     &              nbpa(iesp),numproc
       IF(a_qx.NE.0) THEN
        WRITE(fdbg,*) 'X min : ', MINVAL(partic(a_qx,1:nbpa(iesp),iesp))
        WRITE(fdbg,*) 'X max : ', MAXVAL(partic(a_qx,1:nbpa(iesp),iesp))
       ENDIF
       IF(a_qy.NE.0) THEN
        WRITE(fdbg,*) 'Y min : ', MINVAL(partic(a_qy,1:nbpa(iesp),iesp))
        WRITE(fdbg,*) 'Y max : ', MAXVAL(partic(a_qy,1:nbpa(iesp),iesp))
       ENDIF
       IF(a_qz.NE.0) THEN
        WRITE(fdbg,*) 'Z min : ', MINVAL(partic(a_qz,1:nbpa(iesp),iesp))
        WRITE(fdbg,*) 'Z max : ', MAXVAL(partic(a_qz,1:nbpa(iesp),iesp))
       ENDIF
      ENDIF 

      RETURN
!     __________________________
      END SUBROUTINE cndlim_part




      SUBROUTINE bord_sortie(p_qx,p_qy,p_qz, p_px,p_py,p_pz,
     &                       p_proc, pos_rel, p_bord)
! ======================================================================
! Retourne dans p_bord le bord (entre 1 et 6) par lequel la particule
! qui se trouve en pos_rel (de 0 a 26) par rapport au domaine de p_proc
! est sortie de ce domaine
! Note : on calcule les temps en divisant par les impulsions... ce sont 
! donc en fait des temps propres, mais on peut bien les comparer
! ======================================================================
      USE precisions
      USE domaines
      USE domaines_klder
      IMPLICIT NONE
      REAL(KIND=kr), INTENT(IN)   :: p_qx,p_qy,p_qz,
     &                               p_px,p_py,p_pz
      INTEGER, INTENT(IN)         :: p_proc, pos_rel
      INTEGER, INTENT(OUT)        :: p_bord

      INTEGER                     :: bx, by, bz
      REAL(KIND=kr)               :: tx, ty, tz

!        print*, 'entree dans bordso, p_proc=', p_proc
!        print*, p_qx,p_qy,p_qz, p_px,p_py,p_pz, pos_rel
      IF(MOD(pos_rel,3).EQ.1) THEN        ! -------- bord -x
        bx = 1
        tx = (p_qx - bord(1, p_proc))/p_px
      ELSEIF(MOD(pos_rel,3).EQ.2) THEN    ! -------- bord +x
        bx = 2
        tx = (p_qx - bord(2, p_proc))/p_px
      ELSE
        bx = 0
        tx = 0.
      ENDIF
      IF(MOD(pos_rel/3,3).EQ.1) THEN      ! -------- bord -y
        by = 3
        ty = (p_qy - bord(3, p_proc))/p_py
      ELSEIF(MOD(pos_rel/3,3).EQ.2) THEN  ! -------- bord +y
        by = 4
        ty = (p_qy - bord(4, p_proc))/p_py
      ELSE
        by = 0
        ty = 0.
      ENDIF
      IF(MOD(pos_rel/9,3).EQ.1) THEN      ! -------- bord -z
        bz = 5
        tz = (p_qz - bord(5, p_proc))/p_pz
      ELSEIF(MOD(pos_rel/9,3).EQ.2) THEN  ! -------- bord +z
        bz = 6
        tz = (p_qz - bord(6, p_proc))/p_pz
      ELSE
        bz = 0
        tz = 0.
      ENDIF

! ----- Il est nécessaire de prendre les valeurs absolues
!      le prpush ne garantissant pas que les vitesse aient un signe
!       tel que tx,ty,tz >0
!     contrairement a un pousseur sans correction (Klder)     

      tx = abs(tx) ; ty = abs(ty) ; tz = abs(tz)

      IF((tx.GE.ty).AND.(tx.GE.tz)) p_bord = bx
      IF((ty.GE.tx).AND.(ty.GE.tz)) p_bord = by
      IF((tz.GE.tx).AND.(tz.GE.ty)) p_bord = bz

! --- special petite precision, pour les particules qui se mettent 
!     juste sur un bord superieur (25/11/2003)
!     En procedant ainsi (z, y, x), on evite les problemes dus aux
!     basses dimensionnalites (non 3D)
      IF(p_bord.EQ.0) THEN
        IF(p_qz.EQ.bord(6, p_proc)) p_bord = 6
        IF(p_qy.EQ.bord(4, p_proc)) p_bord = 4
        IF(p_qx.EQ.bord(2, p_proc)) p_bord = 2
      ENDIF

      IF((p_bord.EQ.0).AND.(gliss_ok.EQ.1)) p_bord = 1

      IF((p_bord.GT.6).OR.(p_bord.LE.0)) THEN
        print*, 'Erreur dans bord_sortie, p_bord=',p_bord,' p_proc=',
     &          p_proc,' pos_rel=',pos_rel,' x,y,z=',p_qx,p_qy,p_qz,
     &          ' px,py,pz=',p_px,p_py,p_pz,
     &          ' tx,ty,tz=',tx,ty,tz

        print*, 'Erreur bord(3, proc) bord(4, proc)=',
     &          bord(3, p_proc), bord(4, p_proc)
      ENDIF

      RETURN
!     __________________________
      END SUBROUTINE bord_sortie



      SUBROUTINE cndl_period(p_bord, p_qx, p_qy, p_qz, p_proc)
! ======================================================================
! Application des conditions aux limites periodiques pour la particule
! de coordonnees (p_qx, p_qy, p_qz) sortant par le bord p_bord
! Retourne le nouveau PE a priori de la particule, p_proc
! ======================================================================
      USE precisions
      USE domaines
      USE domaines_klder

      IMPLICIT NONE
      INTEGER, INTENT(IN)          :: p_bord
      REAL(KIND=kr), INTENT(INOUT) :: p_qx, p_qy, p_qz
      INTEGER, INTENT(INOUT)       :: p_proc

      SELECT CASE(p_bord)
      CASE(1)
        p_qx = p_qx - bord(1,1) + bord(2,nproc)
        p_proc = p_proc + nprocx - 1
      CASE(2)
        p_qx = p_qx - bord(2,nproc) + bord(1,1)
        p_proc = p_proc - nprocx + 1
      CASE(3)
        p_qy = p_qy - bord(3,1) + bord(4,nproc)
        p_proc = p_proc + nprocx*(nprocy-1)
      CASE(4)
        p_qy = p_qy - bord(4,nproc) + bord(3,1)
        p_proc = p_proc - nprocx*(nprocy-1)
      CASE(5)
        p_qz = p_qz - bord(5,1) + bord(6,nproc)
        p_proc = p_proc + nprocx*nprocy*(nprocz-1)
      CASE(6)
        p_qz = p_qz - bord(6,nproc) + bord(5,1)
        p_proc = p_proc - nprocx*nprocy*(nprocz-1)
      CASE DEFAULT
        print*, 'Erreur dans cndl_period... parametre p_bord =',p_bord
        STOP
      END SELECT
      RETURN
!     __________________________
      END SUBROUTINE cndl_period



      SUBROUTINE cndl_reflex(p_bord, p_qx, p_qy, p_qz, 
     &                       p_px, p_py, p_pz)
! ======================================================================
! Application des conditions aux limites reflechissantes pour la 
! particule de position (p_qx, p_qy, p_qz) et impulsion (p_px, p_py, 
! p_pz) sortant par le bord p_bord 
! ======================================================================
      USE precisions
      USE domaines
      USE domaines_klder

      IMPLICIT NONE
      INTEGER, INTENT(IN)          :: p_bord
      REAL(KIND=kr), INTENT(INOUT) :: p_qx, p_qy, p_qz, p_px, p_py, p_pz

      SELECT CASE(p_bord)
      CASE(1)
        p_qx = 2._8*bord(1,    1) - p_qx
        p_px = -p_px
      CASE(2)
        p_qx = 2._8*bord(2,nproc) - p_qx
        p_px = -p_px
      CASE(3)
        p_qy = 2._8*bord(3,    1) - p_qy
        p_py = -p_py
      CASE(4)
        p_qy = 2._8*bord(4,nproc) - p_qy
        p_py = -p_py
      CASE(5)
        p_qz = 2._8*bord(5,    1) - p_qz
        p_pz = -p_pz
      CASE(6)
        p_qz = 2._8*bord(6,nproc) - p_qz
        p_pz = -p_pz
      CASE DEFAULT
        print*, 'Erreur dans cndl_reflex...'
      END SELECT
      RETURN
!     __________________________
      END SUBROUTINE cndl_reflex



      SUBROUTINE cndl_reinjs(p_bord, p_qx,p_qy,p_qz, 
     &                       p_px,p_py,p_pz, p_ga, dyn, vtx,vty,vtz)
! ======================================================================
! Application des conditions aux limites reinjectantes pour la 
! particule de position (p_qx, p_qy, p_qz), impulsion (p_px, p_py, p_pz)
! et energie cinetique p_ga sortant par le bord p_bord
! La dynamique de la particule est fixee par dyn
! La reinjection est "locale", c.a.d. par le point de sortie du plasma
! Les parametres pour la nouvelle impulsion sont vtx, vty, vtz
! ======================================================================
      USE precisions
      USE domaines
      USE domaines_klder
      USE particules_klder

      IMPLICIT NONE
      INTEGER, INTENT(IN)          :: p_bord, dyn 
      REAL(KIND=kr), INTENT(INOUT) :: p_qx,p_qy,p_qz,
     &                                p_px,p_py,p_pz, p_ga
      REAL(KIND=kr), INTENT(IN)    :: vtx, vty, vtz

      INTEGER(KIND=8)              :: dummy
      REAL(KIND=kr), DIMENSION(1)  :: moisson
      REAL(KIND=kr)                :: tsorti, xsorti, ysorti, zsorti
      REAL(KIND=kr)                :: p_vx,p_vy,p_vz

      INTERFACE
        SUBROUTINE aleat_gaus(gerbe,methode,graine)
          USE precisions
          REAL(KIND=kr), DIMENSION(:), INTENT(OUT)   :: gerbe
          INTEGER                    , INTENT(IN)    :: methode
          INTEGER(KIND=8)            , INTENT(INOUT) :: graine
        END SUBROUTINE aleat_gaus
      END INTERFACE

      dummy = 0
      IF(dyn.EQ.2) THEN
        p_vx = p_px / p_ga ; p_vy = p_py / p_ga ; p_vz = p_pz / p_ga
      ELSE
        p_vx = p_px        ; p_vy = p_py        ; p_vz = p_pz
      ENDIF
      SELECT CASE(p_bord)
      CASE(1)
        IF(p_vx.LT.0.) THEN ; tsorti = (p_qx-bord(1,1))/p_vx
        ELSE ; tsorti = 0. ; ENDIF
        xsorti = bord(1,1)
        CALL aleat_gaus(moisson,0,dummy) ; p_vx =  vtx*ABS(moisson(1))
        IF(a_py.NE.0) THEN
          ysorti = p_qy - tsorti*p_vy
          CALL aleat_gaus(moisson,0,dummy) ; p_vy =  vty*moisson(1)
        ENDIF
        IF(a_pz.NE.0) THEN
          zsorti = p_qz - tsorti*p_vz
          CALL aleat_gaus(moisson,0,dummy) ; p_vz =  vtz*moisson(1)
        ENDIF
        
      CASE(2)
        IF(p_vx.GT.0.) THEN ; tsorti = (p_qx-bord(2,nproc))/p_vx
        ELSE ; tsorti = 0. ; ENDIF
        xsorti = bord(2,nproc)
        CALL aleat_gaus(moisson,0,dummy) ; p_vx = -vtx*ABS(moisson(1))
        IF(a_py.NE.0) THEN
          ysorti = p_qy - tsorti*p_vy
          CALL aleat_gaus(moisson,0,dummy) ; p_vy =  vty*moisson(1)
        ENDIF
        IF(a_pz.NE.0) THEN
          zsorti = p_qz - tsorti*p_vz
          CALL aleat_gaus(moisson,0,dummy) ; p_vz =  vtz*moisson(1)
        ENDIF

      CASE(3)
        IF(p_vy.LT.0.) THEN ; tsorti = (p_qy-bord(3,1))/p_vy
        ELSE ; tsorti = 0. ; ENDIF
        ysorti = bord(3,1)
        CALL aleat_gaus(moisson,0,dummy) ; p_vy =  vty*ABS(moisson(1))
        IF(a_px.NE.0) THEN
          xsorti = p_qx - tsorti*p_vx
          CALL aleat_gaus(moisson,0,dummy) ; p_vx =  vtx*moisson(1)
        ENDIF
        IF(a_pz.NE.0) THEN
          zsorti = p_qz - tsorti*p_vz
          CALL aleat_gaus(moisson,0,dummy) ; p_vz =  vtz*moisson(1)
        ENDIF
        
      CASE(4)
        IF(p_vy.GT.0.) THEN ; tsorti = (p_qy-bord(4,nproc))/p_vy
        ELSE ; tsorti = 0. ; ENDIF
        ysorti = bord(4,nproc)
        CALL aleat_gaus(moisson,0,dummy) ; p_vy = -vty*ABS(moisson(1))
        IF(a_px.NE.0) THEN
          xsorti = p_qx - tsorti*p_vx
          CALL aleat_gaus(moisson,0,dummy) ; p_vx =  vtx*moisson(1)
        ENDIF
        IF(a_pz.NE.0) THEN
          zsorti = p_qz - tsorti*p_vz
          CALL aleat_gaus(moisson,0,dummy) ; p_vz =  vtz*moisson(1)
        ENDIF

      CASE(5)
        IF(p_vz.LT.0.) THEN ; tsorti = (p_qz-bord(5,1))/p_vz
        ELSE ; tsorti = 0. ; ENDIF
        zsorti = bord(5,1)
        CALL aleat_gaus(moisson,0,dummy) ; p_vz =  vtz*ABS(moisson(1))
        IF(a_px.NE.0) THEN
          xsorti = p_qx - tsorti*p_vx
          CALL aleat_gaus(moisson,0,dummy) ; p_vx =  vtx*moisson(1)
        ENDIF
        IF(a_py.NE.0) THEN
          ysorti = p_qy - tsorti*p_vy
          CALL aleat_gaus(moisson,0,dummy) ; p_vy =  vty*moisson(1)
        ENDIF
        
      CASE(6)
        IF(p_vz.GT.0.) THEN ; tsorti = (p_qz-bord(6,nproc))/p_vz
        ELSE ; tsorti = 0. ; ENDIF
        zsorti = bord(6,nproc)
        CALL aleat_gaus(moisson,0,dummy) ; p_vz = -vtz*ABS(moisson(1))
        IF(a_px.NE.0) THEN
          xsorti = p_qx - tsorti*p_vx
          CALL aleat_gaus(moisson,0,dummy) ; p_vx =  vtx*moisson(1)
        ENDIF
        IF(a_py.NE.0) THEN
          ysorti = p_qy - tsorti*p_vy
          CALL aleat_gaus(moisson,0,dummy) ; p_vy =  vty*moisson(1)
        ENDIF

      CASE DEFAULT
        print*, 'Erreur dans cndl_reinjs...'

      END SELECT

      p_ga = 0.
      IF(a_px.NE.0) p_ga = p_ga + p_vx*p_vx
      IF(a_py.NE.0) p_ga = p_ga + p_vy*p_vy
      IF(a_pz.NE.0) p_ga = p_ga + p_vz*p_vz
      IF(dyn.EQ.2) THEN
        p_ga = SQRT(1._8 + p_ga)
        p_px = p_vx ; p_py = p_vy ; p_pz = p_vz
        IF(a_px.NE.0) p_qx = xsorti + tsorti*p_vx/p_ga
        IF(a_py.NE.0 .AND. a_qy.NE.0) p_qy = ysorti + tsorti*p_vy/p_ga
        IF(a_pz.NE.0 .AND. a_qz.NE.0) p_qz = zsorti + tsorti*p_vz/p_ga
      ELSE
        p_ga = 0.5_8*p_ga
        p_px = p_vx ; p_py = p_vy ; p_pz = p_vz
        IF(a_px.NE.0) p_qx = xsorti + tsorti*p_vx
        IF(a_py.NE.0 .AND. a_qy.NE.0) p_qy = ysorti + tsorti*p_vy
        IF(a_pz.NE.0 .AND. a_qz.NE.0) p_qz = zsorti + tsorti*p_vz
      ENDIF
!
      RETURN
!     __________________________
      END SUBROUTINE cndl_reinjs



      SUBROUTINE cndl_reinjs2(p_bord, p_qx,p_qy,p_qz, 
     &                        p_px,p_py,p_pz, p_ga, dyn, vtx,vty,vtz )
! ======================================================================
! Application des conditions aux limites reinjectantes pour la 
! particule de position (p_qx, p_qy, p_qz), impulsion (p_px, p_py, p_pz)
! et energie cinetique p_ga sortant par le bord p_bord
! La dynamique de la particule est fixee par dyn
! La reinjection est "locale", c.a.d. par le point de sortie du plasma
! Les parametres pour la nouvelle impulsion sont vthx, vthy, vthz
! ======================================================================
      USE precisions
      USE domaines
      USE domaines_klder
      USE particules_klder

      IMPLICIT NONE
      INTEGER, INTENT(IN)          :: p_bord, dyn 
      REAL(KIND=kr), INTENT(INOUT) :: p_qx,p_qy,p_qz,
     &                                p_px,p_py,p_pz, p_ga
      REAL(KIND=kr), INTENT(IN)    :: vtx, vty, vtz

      INTEGER(KIND=8)              :: dummy
      REAL(KIND=kr), DIMENSION(1)  :: moisson
      REAL(KIND=kr)                :: tsorti, xsorti, ysorti, zsorti
      REAL(KIND=kr)                :: p_vx,p_vy,p_vz

      INTERFACE
        SUBROUTINE aleat_unif(gerbe,methode,graine)
          USE precisions
          REAL(KIND=kr), DIMENSION(:), INTENT(OUT)   :: gerbe
          INTEGER                    , INTENT(IN)    :: methode
          INTEGER(KIND=8)            , INTENT(INOUT) :: graine
        END SUBROUTINE aleat_unif
        SUBROUTINE aleat_gaus(gerbe,methode,graine)
          USE precisions
          REAL(KIND=kr), DIMENSION(:), INTENT(OUT)   :: gerbe
          INTEGER                    , INTENT(IN)    :: methode
          INTEGER(KIND=8)            , INTENT(INOUT) :: graine
        END SUBROUTINE aleat_gaus
      END INTERFACE

      dummy = 0
      IF(dyn.EQ.2) THEN
        p_vx = p_px / p_ga ; p_vy = p_py / p_ga ; p_vz = p_pz / p_ga
      ELSE
        p_vx = p_px        ; p_vy = p_py        ; p_vz = p_pz
      ENDIF
      SELECT CASE(p_bord)
      CASE(1)
        IF(p_vx.LT.0.) THEN ; tsorti = (p_qx-bord(1,1))/p_vx
        ELSE ; tsorti = 0. ; ENDIF
        xsorti = bord(1,1)
! vt(xyz) sont des impulsions si dynam==2 et donc aussi pv(xyz)
        CALL aleat_gaus(moisson,0,dummy) ; p_vx =  vtx*ABS(moisson(1))  
        IF(a_py.NE.0) THEN
          ysorti = p_qy - tsorti*p_vy
          CALL aleat_gaus(moisson,0,dummy) ; p_vy =  vty*moisson(1)
        ENDIF
        IF(a_pz.NE.0) THEN
          zsorti = p_qz - tsorti*p_vz
          CALL aleat_gaus(moisson,0,dummy) ; p_vz =  vtz*moisson(1)
        ENDIF
        
      CASE(2)
        IF(p_vx.GT.0.) THEN ; tsorti = (p_qx-bord(2,nproc))/p_vx
        ELSE ; tsorti = 0. ; ENDIF
        xsorti = bord(2,nproc)
        CALL aleat_gaus(moisson,0,dummy) ; p_vx = -vtx*ABS(moisson(1))
        IF(a_py.NE.0) THEN
          ysorti = p_qy - tsorti*p_vy
          CALL aleat_gaus(moisson,0,dummy) ; p_vy =  vty*moisson(1)
        ENDIF
        IF(a_pz.NE.0) THEN
          zsorti = p_qz - tsorti*p_vz
          CALL aleat_gaus(moisson,0,dummy) ; p_vz =  vtz*moisson(1)
        ENDIF

      CASE(3)
        IF(p_vy.LT.0.) THEN ; tsorti = (p_qy-bord(3,1))/p_vy
        ELSE ; tsorti = 0. ; ENDIF
        ysorti = bord(3,1)
        CALL aleat_gaus(moisson,0,dummy) ; p_vy =  vty*ABS(moisson(1))
        IF(a_px.NE.0) THEN
          xsorti = p_qx - tsorti*p_vx
          CALL aleat_gaus(moisson,0,dummy) ; p_vx =  vtx*moisson(1)
        ENDIF
        IF(a_pz.NE.0) THEN
          zsorti = p_qz - tsorti*p_vz
          CALL aleat_gaus(moisson,0,dummy) ; p_vz =  vtz*moisson(1)
        ENDIF
        
      CASE(4)
        IF(p_vy.GT.0.) THEN ; tsorti = (p_qy-bord(4,nproc))/p_vy
        ELSE ; tsorti = 0. ; ENDIF
        ysorti = bord(4,nproc)
        CALL aleat_gaus(moisson,0,dummy) ; p_vy = -vty*ABS(moisson(1))
        IF(a_px.NE.0) THEN
          xsorti = p_qx - tsorti*p_vx
          CALL aleat_gaus(moisson,0,dummy) ; p_vx =  vtx*moisson(1)
        ENDIF
        IF(a_pz.NE.0) THEN
          zsorti = p_qz - tsorti*p_vz
          CALL aleat_gaus(moisson,0,dummy) ; p_vz =  vtz*moisson(1)
        ENDIF

      CASE(5)
        IF(p_vz.LT.0.) THEN ; tsorti = (p_qz-bord(5,1))/p_vz
        ELSE ; tsorti = 0. ; ENDIF
        zsorti = bord(5,1)
        CALL aleat_gaus(moisson,0,dummy) ; p_vz =  vtz*ABS(moisson(1))
        IF(a_px.NE.0) THEN
          xsorti = p_qx - tsorti*p_vx
          CALL aleat_gaus(moisson,0,dummy) ; p_vx =  vtx*moisson(1)
        ENDIF
        IF(a_py.NE.0) THEN
          ysorti = p_qy - tsorti*p_vy
          CALL aleat_gaus(moisson,0,dummy) ; p_vy =  vty*moisson(1)
        ENDIF
        
      CASE(6)
        IF(p_vz.GT.0.) THEN ; tsorti = (p_qz-bord(6,nproc))/p_vz
        ELSE ; tsorti = 0. ; ENDIF
        zsorti = bord(6,nproc)
        CALL aleat_gaus(moisson,0,dummy) ; p_vz = -vtz*ABS(moisson(1))
        IF(a_px.NE.0) THEN
          xsorti = p_qx - tsorti*p_vx
          CALL aleat_gaus(moisson,0,dummy) ; p_vx =  vtx*moisson(1)
        ENDIF
        IF(a_py.NE.0) THEN
          ysorti = p_qy - tsorti*p_vy
          CALL aleat_gaus(moisson,0,dummy) ; p_vy =  vty*moisson(1)
        ENDIF

      CASE DEFAULT
        print*, 'Erreur dans cndl_reinjs...'

      END SELECT

      p_ga = 0.
      IF(a_px.NE.0) p_ga = p_ga + p_vx*p_vx
      IF(a_py.NE.0) p_ga = p_ga + p_vy*p_vy
      IF(a_pz.NE.0) p_ga = p_ga + p_vz*p_vz
      IF(dyn.EQ.2) THEN
        p_ga = SQRT(1._8 + p_ga)
        p_px = p_vx ; p_py = p_vy ; p_pz = p_vz
        IF(a_px.NE.0) p_qx = xsorti + tsorti*p_vx/p_ga
        IF(a_py.NE.0 .AND. a_qy.NE.0) p_qy = ysorti + tsorti*p_vy/p_ga
        IF(a_pz.NE.0 .AND. a_qz.NE.0) p_qz = zsorti + tsorti*p_vz/p_ga
      ELSE
        p_ga = 0.5_8*p_ga
        p_px = p_vx ; p_py = p_vy ; p_pz = p_vz
        IF(a_px.NE.0) p_qx = xsorti + tsorti*p_vx
        IF(a_py.NE.0 .AND. a_qy.NE.0) p_qy = ysorti + tsorti*p_vy
        IF(a_pz.NE.0 .AND. a_qz.NE.0) p_qz = zsorti + tsorti*p_vz
      ENDIF

      CALL aleat_unif(moisson,0,dummy)
      
      SELECT CASE(p_bord)
      CASE(1)
        p_qx = p_qx + moisson(1)*pasdx
      CASE(2)
        p_qx = p_qx - moisson(1)*pasdx
      CASE(3)
        p_qy = p_qy + moisson(1)*pasdy
      CASE(4)
        p_qy = p_qy - moisson(1)*pasdy
      CASE(5)
        p_qz = p_qz + moisson(1)*pasdz
      CASE(6)
        p_qz = p_qz - moisson(1)*pasdz
      END SELECT
!
      RETURN
!     ___________________________
      END SUBROUTINE cndl_reinjs2



      FUNCTION localise_rel(p_qx, p_qy, p_qz, p_proc)
! ======================================================================
! Localisation de la particule de coordonnees p_qx, p_qy, p_qz
! relativement au domaine du PE p_proc
! Retourne un nombre compris entre 0 et 26 inclus, forme de la somme de
! ...  1  |  0  |   2  : sort en -x | dedans en x | sort en +x
! ...  3  |  0  |   6  : sort en -y | dedans en y | sort en +y
! ...  9  |  0  |  18  : sort en -z | dedans en z | sort en +z
! ======================================================================
      USE precisions
      USE domaines
      USE domaines_klder
      IMPLICIT NONE
      INTEGER                   :: localise_rel
      REAL(KIND=kr), INTENT(IN) :: p_qx, p_qy, p_qz
      INTEGER, INTENT(IN)       :: p_proc
      INTEGER                   :: result

      result = 0
      IF(p_qx.LT.bord(1,p_proc)) THEN
        result = result +  1
      ELSEIF(p_qx.GE.bord(2,p_proc)) THEN
        result = result +  2
      ENDIF
      IF(p_qy.LT.bord(3,p_proc)) THEN
        result = result +  3
      ELSEIF(p_qy.GE.bord(4,p_proc)) THEN
        result = result +  6
      ENDIF
      IF(p_qz.LT.bord(5,p_proc)) THEN
        result = result +  9
      ELSEIF(p_qz.GE.bord(6,p_proc)) THEN
        result = result + 18
      ENDIF
      localise_rel = result
      RETURN
!     _________________________
      END FUNCTION localise_rel



      SUBROUTINE tasse_tableau(nb_perdu, nbp, pa_perdu, a_tasser)
! ======================================================================
! Tasse le tableau de particules a_tasser (nbp elements dont des trous)
! en enlevant les numeros pa_perdu
! Retourne une valeur modifiee de nbp
! ======================================================================
      USE precisions
      USE erreurs
      IMPLICIT NONE
      INTEGER, INTENT(IN)                          :: nb_perdu
      INTEGER, INTENT(INOUT)                       :: nbp
      INTEGER,       DIMENSION(:), INTENT(IN)      :: pa_perdu
      REAL(KIND=kr), DIMENSION(:,:), INTENT(INOUT) :: a_tasser

      INTEGER                               :: nb_fin, i, j, istep
      INTEGER, DIMENSION(:), ALLOCATABLE    :: vec_ind

      IF(nb_perdu.GT.nbp) THEN
        print*, 'Erreur dans tasse_tableau'
      ELSEIF((nb_perdu.EQ.nbp).OR.(nb_perdu.EQ.0)) THEN
        nbp = nbp - nb_perdu
      ELSE
        nb_fin = nbp - nb_perdu
! --- Constitution du vecteur d'indices pour le tableau tasse
        ALLOCATE(vec_ind(nbp))
        vec_ind(:) = 1
        vec_ind(pa_perdu(1:nb_perdu)) = 0
        j = 1
        DO i = 1,nbp
          istep = vec_ind(i)
          vec_ind(j) = i
          j = j + istep
        ENDDO
c --- On decale pour boucher les trous
        a_tasser(:,1:nb_fin) = a_tasser(:,vec_ind(1:nb_fin))
        nbp = nb_fin
        DEALLOCATE(vec_ind)
      ENDIF
      RETURN
!     ____________________________
      END SUBROUTINE tasse_tableau


